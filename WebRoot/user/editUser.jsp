<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>注册</title>

<script type="text/javascript" src="<%=basePath%>/js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="<%=basePath%>/js/style.css" />
		<script type="text/javascript" src="<%=basePath%>/js/jquery.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>/js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=basePath%>/js/styles.css" />
        <link rel="stylesheet" type="text/css" href="<%=basePath%>/js/jquery.confirm.css" />
        
        <link rel="stylesheet" href="<%=basePath%>/css/mycss.css" type="text/css"/>
<script type="text/javascript"> 

$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
		</script>
<script type="text/javascript">

</script>
		
</head>
  
  <body>
    <div>
    	<s:form action="user_save" id="form" method="post">
    		<input type="hidden" value="${ctUser.UId}" name="ctUser.UId"/>
    		<table>
    			<tr>
    				<td>姓名：</td>
    				<td><input type="text" id="UUsername" name="ctUser.UUsername" value="${ctUser.UUsername }" /></td>
    			</tr>
    			<tr>
    				<td>Email：</td>
    				<td><input type="text" id="UEmail" name="ctUser.UEmail"  value="${ctUser.UEmail }"/></td>
    			</tr>
    			<tr>
    				<td>QQ账号：</td>
    				<td><input type="text" id="UQq" name="ctUser.UQq"  value="${ctUser.UQq }"/></td>
    			</tr>
    			<tr>
    				<td>微博账号：</td>
    				<td><input type="text" id="UWeibo" name="ctUser.UWeibo"  value="${ctUser.UWeibo }"/></td>
    			</tr>
    			<tr>
    				<td>淘宝账号：</td>
    				<td><input type="text" id="UTb" name="ctUser.UTb"  value="${ctUser.UTb }"/></td>
    			</tr>
    			<tr>
    				<td>性别：</td>
    				<td>
    					<s:if test="ctUser.USex=='男'">
    						<input type="radio" id="USex" name="ctUser.USex" value="男" checked="checked"/>男
    						<input type="radio" id="USex" name="ctUser.USex" value="女"/>女
    					</s:if>
    					<s:else>
    					<input type="radio" id="USex" name="ctUser.USex" value="男" />男
    						<input type="radio" id="USex" name="ctUser.USex" value="女" checked="checked"/>女
    					</s:else>
    				</td>
    			</tr>
    			<tr>
    				<td>生日：</td>
    				<td><input type="text" id="UBirthday" name="ctUser.UBirthday"  value="${ctUser.UBirthday }"/></td>
    			</tr>
    			<tr>
    				<td><input type="submit" value="保存" /></td>
    			</tr>
    		</table>
    	</s:form>
    </div>
  </body>
</html>
