<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>注册</title>

<script type="text/javascript" src="../js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="../js/style.css" />
		<script type="text/javascript" src="../js/jquery.min.js"></script>
		<script type="text/javascript" src="../js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../js/styles.css" />
        <link rel="stylesheet" type="text/css" href="../js/jquery.confirm.css" />
        
        <link rel="stylesheet" href="../css/mycss.css" type="text/css"/>
<script type="text/javascript"> 

$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
		</script>
<script type="text/javascript">
	//验证用户名
	function checkName(){
		var username=$("#username").val();
		if(username == ""){
			$("#usernametip").text("请输入用户名");
		}else{
			$("#usernametip").text("");
			$.post("<%=basePath%>login_checkName",{"ctUser.UUserid":username},function(data){
					if(data=="success"){
					}else {
						$("#usernametip").text("用户名已存在请重新输入");
						$("#username").val("");
					}
			});
		}
	}
	//验证用户密码
	function checkPasswd(){
		var passwd=$("#passwd").val();
		if(passwd.length<6){
			$("#passwdtip").text("密码长度至少6位");
			$("#passwd").val("");
		}else {
			$("#passwdtip").text("");
		}
	}
	//验证确认密码是否相同
	function checkPasswd2(){
		var passwd=$("#passwd").val();
		var passwd2=$("#passwd2").val();
		if(passwd != passwd2){
			$("#passwdtip2").text("两次输入的密码不一样");
			$("#passwd2").val("");
		}else{
			$("#passwdtip2").text("");
		}
	}
	//验证手机号码是否合法
	function checkPhone(){
		var phoneNum = $("#umb").val();
		if(!(/^1[3|5|8|9][0-9]\d{8}$/.test(phoneNum))){
			$("#umbtip").text("手机号不合法");
		}else{
			$("#umbtip").text("");
			$.post("<%=basePath%>login_checkPhone",{"ctUser.UMb":phoneNum},function(data){
				if(data=="success"){
					$("#umbtip").text("");
				}else {
					$("#umbtip").text("手机号已注册");
					$("#umb").val("");
				}
			});
		}
	}	//验证手机号码,获取手机验证码并保存验证码等信息
	function getPhoneYzm(){
		var phoneNum = $("#umb").val();
		if(phoneNum==""){
			$("#umbtip").text("必选填写手机号");
		}else{
			$("#umbtip").text("");
		}
		$.post("<%=basePath%>login_savePhoneYzm",{"ctSms.sms":"1111","ctUser.UMb":phoneNum});
	}
	function register(){
		
		var username=$("#username").val();
		var passwd=$("#passwd").val();
		var passwd2=$("#passwd2").val();
		var phoneNum = $("#umb").val();
		var yzm = $("#sms").val();
		
		if(username == ""){
			$("#usernametip").text("请输入用户名");
		}
		if(passwd == ""){
			$("#passwdtip").text("请输入密码");
		}
		if(passwd2 == ""){
			$("#passwdtip2").text("请输入确认密码");
		}
		if(phoneNum == ""){
			$("#umbtip").text("请输入手机号码");
		}
		if(yzm == ""){
			$("#smstip").text("请输入验证码");
		}else{
			//1.判断输入的验证码是否正确
			var phoneNum = $("#umb").val();
			var yzm = $("#sms").val();
			$("#smstip").text("");
			$.post("<%=basePath%>login_checkYzmIsEqual",{"ctSms.sms":yzm,"ctUser.UMb":phoneNum},function(data){
				if(data=="success"){
					//2.向数据库中写注册信息
					$("#form").submit();
				}else {
					$("#smstip").text("输入的验证码不对请重新输入");
					$("#sms").val("");
				}
			});
			
		}
		
		
		
		
//		var checkedNum = $("input[name='mid']:checked").length;
//		if(checkedNum == 0){
//			alert("mei选中");
			//$("#agreetip").text("必须选中同意注册协议的复选框");
//		}else {
			//$("#agreetip").text("");
//			alert("选中");
//		}
	}
</script>
		
</head>
  
  <body>
    <div style="margin-left: 550px;margin-top: 300px">
    	<s:form action="login_register" id="form" method="post">
    		<table>
    			<tr>
    				<td><span style="color: red;">*</span>用户名：</td>
    				<td><input type="text" id="username" name="ctUser.UUserid" onblur="checkName();"/></td>
    				<td colspan="2"><span style="color: red;" id="usernametip"></span></td>
    			</tr>
    			<tr>
    				<td><span style="color: red;">*</span>请设置密码：</td>
    				<td><input type="password" id="passwd" name="ctUser.UPassword" onblur="checkPasswd();"/></td>
    				<td colspan="2"><span style="color: red;" id="passwdtip"></span></td>
    			</tr>
    			<tr>
    				<td><span style="color: red;">*</span>请确认密码：</td>
    				<td><input type="password" id="passwd2" name="passwd2" onblur="checkPasswd2();"/></td>
    				<td colspan="2"><span style="color: red;" id="passwdtip2"></span></td>
    			</tr>
    			<tr>
    				<td><span style="color: red;">*</span>验证手机：</td>
    				<td><input type="text" id="umb" name="ctUser.UMb" onblur="checkPhone();"/></td>
    				<td colspan="2"><span style="color: red;" id="umbtip"></span></td>
    			</tr>
    			<tr>
    				<td><span style="color: red;">*</span>短信验证码：</td>
    				<td><input type="text" id="sms" name="ctSms.sms"/></td>
    				<td colspan="2"><input type="button" value="获取短信验证码" onclick="getPhoneYzm();"/></td>
    				<td colspan="2"><span style="color: red;" id="smstip"></span></td>
    			</tr>
    			<tr>
    				<td><input type="checkbox" id="agree" name="checkbox"/>我已阅读并同意</td>
    				<td colspan="3"><a href="#"><span style="color: red;">《CT_GO用户注册协议》</span></a></td>
    				<td colspan="2"><span style="color: red;" id="agreetip"></span></td>
    			</tr>
    			<tr>
    				<td colspan="4" align="center"><input type="button" onclick="register();" value="立即注册"/></td>
    			</tr>
    		</table>
    	</s:form>
    </div>
  </body>
</html>
