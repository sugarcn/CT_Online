<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>注册</title>

<script type="text/javascript" src="../js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="../js/style.css" />
		<script type="text/javascript" src="../js/jquery.min.js"></script>
		<script type="text/javascript" src="../js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../js/styles.css" />
        <link rel="stylesheet" type="text/css" href="../js/jquery.confirm.css" />
        
        <link rel="stylesheet" href="../css/mycss.css" type="text/css"/>
<script type="text/javascript"> 

$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
		</script>
<script type="text/javascript">
	function showYzm() {
		document.getElementById("realyzm").src="<%=basePath%>/common/image.jsp?id="+Math.random();
	}
</script>
<script type="text/javascript">
	function updatePasswd(){
		checkUserName();
		checkYzm();
		$("#form").submit();
	}
	function checkUserName(){
		var username=$("#UUserid").val();
		if(username == ""){
			$("#usernametip").text("请输入用户名");
		}else{
			$("#usernametip").text("");
			$.post("<%=basePath%>login_checkUserName",{"ctUser.UUserid":username},function(data){
					if(data=="success"){
						
					}else {
						$("#usernametip").text("用户名或者手机号不存在");
						$("#username").val("");
					}
			});
		}
	}
	
	function checkYzm(){
		var yzm = $("#yzm").val();
		if(yzm == ""){
			$("#yzmtip").text("请输入验证码");
		}else{
			$("#yzmtip").text("");
			$.post("<%=basePath%>login_checkYzm",{"yzm":yzm},function(data){
				if(data=="success"){
				}else {
					$("#yzmtip").text("输入的验证码不对");
					$("#yzm").val("");
				}
			});
		}
	}
</script>
</head>
  
  <body>
	<div style="margin-left: 550px;margin-top: 300px">
		<s:form action="login_findpwd1" id="form" method="post">
			<table>
				<tr>
					<td>账户名:</td>
					<td><input type="text" value="请输入用户名/已验证手机" onclick="this.value=''" 
					name="ctUser.UUserid" id="UUserid"/>
					</td>
					<td colspan="2"><span style="color: red;" id="usernametip"></span></td>
				</tr>
				<tr>
					<td>验证码:</td>
					<td><input type="text" id="yzm" name="yzm"/></td>
					<td><img id="realyzm" title="点我换下一张"
					  onclick="javascript:showYzm();"
					 src="<%=basePath%>common/image.jsp"/></td>
					 <td colspan="2"><span style="color: red;" id="yzmtip"></span></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="button" value="提交" onclick="updatePasswd();"/></td>
					<td></td>
				</tr>
				
			</table>
			
		</s:form>
	</div>
  </body>
</html>
