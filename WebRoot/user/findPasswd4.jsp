<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>注册</title>

<script type="text/javascript" src="<%=basePath %>js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="<%=basePath %>js/style.css" />
		<script type="text/javascript" src="<%=basePath %>js/jquery.min.js"></script>
		<script type="text/javascript" src="<%=basePath %>js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=basePath %>js/styles.css" />
        <link rel="stylesheet" type="text/css" href="<%=basePath %>js/jquery.confirm.css" />
        
        <link rel="stylesheet" href="<%=basePath %>css/mycss.css" type="text/css"/>
<script type="text/javascript"> 
//验证用户密码
function checkPasswd(){
	var passwd=$("#passwd").val();
	if(passwd.length<6){
		$("#passwdtip").text("密码长度至少6位");
		$("#passwd").val("");
	}else {
		$("#passwdtip").text("");
	}
}
//验证确认密码是否相同
function checkPasswd2(){
	var passwd=$("#passwd").val();
	var passwd2=$("#passwd2").val();
	if(passwd != passwd2){
		$("#passwdtip2").text("两次输入的密码不一样");
		$("#passwd2").val("");
	}else{
		$("#passwdtip2").text("");
	}
}

//修改密码
function updatePasswd(){
	var passwd=$("#passwd").val();
	var passwd2=$("#passwd2").val();
	if(passwd == ""){
		$("#passwdtip").text("请输入密码");
	}
	if(passwd2 == ""){
		$("#passwdtip2").text("请输入确认密码");
	}
	$("#form").submit();
}

</script>
</head>
<body>
	新密码设置成功
</body>
</html>
