<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>注册</title>

<script type="text/javascript" src="../js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="../js/style.css" />
		<script type="text/javascript" src="../js/jquery.min.js"></script>
		<script type="text/javascript" src="../js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../js/styles.css" />
        <link rel="stylesheet" type="text/css" href="../js/jquery.confirm.css" />
        
        <link rel="stylesheet" href="../css/mycss.css" type="text/css"/>
<script type="text/javascript"> 

$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
		</script>
<script type="text/javascript">
	$(function(){
		var UMb = $("#UMb").text();
		UMb = UMb.substring(0, 3) + "****" + UMb.substring(7, 11);
		$("#UMb").text(UMb);
		//$("#btn").onclick=function(){getPhoneYzm(this);};
	});
	//documnet.getElementById("btn").onclick=function(){getPhoneYzm(this);};
	function getPhoneYzm(){
		var phoneNum=$("#UMb2").val();
		$.post("<%=basePath%>login_savePhoneYzm",{"ctUser.UMb":phoneNum},function(data){
			if(data=="success"){
				document.getElementById("msgtip").style.display="";
			}else {
				$("#msptip2").text("获取验证码失败");
			}
		});
	}
	
	//提交获取的验证码
	function tijiao(){
		var phoneNum=$("#UMb2").val();
		var yzm = $("#sms").val();
		if(yzm == ""){
			$("#smstip").text("请输入验证码");
		}else{
			//1.判断输入的验证码是否正确
			$("#smstip").text("");
			$.post("<%=basePath%>login_checkYzmIsEqual",{"ctSms.sms":yzm,"ctUser.UMb":phoneNum},function(data){
				if(data=="success"){
					//2.跳转到找回密码的下一页
					//$.post("<%=basePath%>login_findpasswd2",{"ctUser.UMb":phoneNum});
					$("#form").submit();
				}else {
					$("#smstip").text("输入的验证码不对请重新输入");
					$("#sms").val("");
				}
			});
			
		}
	}
</script>

</head>
  
  <body>
	<div style="margin-left: 550px;margin-top: 200px">
		<s:form action="login_findpasswd2" id="form" method="post">
			<input type="hidden" id="UMb2" name="ctUser.UMb" value="${ctUser.UMb}"/>
			请点击获取验证码并在手机：<span id="UMb">${ctUser.UMb}</span>中查看短信,并填写验证码
			<table>
				<tr>
					<td>请填写手机验证码:</td>
					<td><input type="text" id="sms" name="ctSms.sms"/></td>
					<td><input type="button" value="获取短信验证码" id="btn" onclick="getPhoneYzm()"/></td>
					<td colspan="2"><span style="color: red;" id="smstip"></span></td>
				</tr>
				<tr>
					<td><input type="button" value="提交" onclick="tijiao()"/></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td rowspan="4">
						<div style="display: none" id="msgtip">
							<span id="msptip2">校验码已发出，请注意查收短信，如果没有收到，你可以在<span style="color: red;">95</span>秒后要求系统重新发送</span>
						</div>
					</td>
				</tr>
			</table>
			
		</s:form>
	</div>

  </body>
  
</html>
