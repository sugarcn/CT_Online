	//验证用户名
	function checkName(){
		var username=$("#username").val();
		if(username == ""){
			$("#usernametip").text("请输入用户名");
		}else{
			$("#usernametip").text("");
			$.post("login_checkName",{"ctUser.UUserid":username},function(data){
					if(data=="success"){
					}else {
						$("#usernametip").text("用户名已存在请重新输入");
						$("#username").val("");
					}
			});
		}
	}
	//验证用户密码
	function checkPasswd(){
		var passwd=$("#passwd").val();
		if(passwd.length<6){
			$("#passwdtip").text("密码长度至少6位");
			$("#passwd").val("");
		}else {
			$("#passwdtip").text("");
		}
	}
	//验证确认密码是否相同
	function checkPasswd2(){
		var passwd=$("#passwd").val();
		var passwd2=$("#passwd2").val();
		if(passwd != passwd2){
			$("#passwdtip2").text("两次输入的密码不一样");
			$("#passwd2").val("");
		}else{
			$("#passwdtip2").text("");
		}
	}
	//验证手机号码是否合法
	function checkPhone(){
		var phoneNum = $("#umb").val();
		if(!(/^1[3|5|8|9][0-9]\d{8}$/.test(phoneNum))){
			$("#umbtip").text("手机号不合法");
		}else{
			$("#umbtip").text("");
			$.post("login_checkPhone",{"ctUser.UMb":phoneNum},function(data){
				if(data=="success"){
					$("#umbtip").text("");
				}else {
					$("#umbtip").text("手机号已注册");
					$("#umb").val("");
				}
			});
		}
	}	//验证手机号码,获取手机验证码并保存验证码等信息
	function getPhoneYzm(){
		var phoneNum = $("#umb").val();
		if(phoneNum=="" || phoneNum == "请输入手机号码"){
			$("#umbtip").text("必选填写手机号");
		}else{
			$("#umbtip").text("");
		}
		$.post("login_savePhoneYzm",{"ctSms.sms":"1111","ctUser.UMb":phoneNum});
	}
	//检测手机验证码是否正确
	function checkUmbYzmIsEqual() {
		var phoneNum = $("#umb").val();
		var yzm = $("#sms").val();
		$("#smstip").text("");
		$.post("login_checkYzmIsEqual",{"ctSms.sms":yzm,"ctUser.UMb":phoneNum},function(data){
			if(data=="success"){
			}else {
				$("#smstip").text("输入的验证码不对请重新输入");
				$("#sms").val("");
			}
		});
	}
	function registerUmb(){
		
		var username=$("#username").val();
		var passwd=$("#passwd").val();
		var passwd2=$("#passwd2").val();
		var phoneNum = $("#umb").val();
		var yzm = $("#sms").val();
		
		if(username == ""){
			$("#usernametip").text("请输入用户名");
		}else
		if(phoneNum == "" || phoneNum=="请输入手机号码"){
			$("#umbtip").text("请输入手机号码");
		}else
		if(yzm == ""){
			$("#smstip").text("请输入验证码");
		}else
		if(passwd == ""){
			$("#passwdtip").text("请输入密码");
		}else
		if(passwd2 == ""){
			$("#passwdtip2").text("请输入确认密码");
		}else
		if(!($("#isAgree").attr("checked") == "checked")){
			$("#isAgreetip").text("请勾选长亭电子有限公司用户注册协议");
		}else {
			$("#form2").submit();
		}
	}
	
	//检查邮箱是否被注册
	function checkEmail() {
		var email = $("#email").val();
		if(email == ""){
			$("#emailtip").text("请输入邮箱");
		}else{
			$("#emailtip").text("");
			$.post("login_checkEmail",{"ctUser.UEmail":email},function(data){
				if(data=="success"){
					$("#emailtip").text("邮箱合法");
				}else {
					$("#emailtip").text("邮箱已存在请重新输入");
					$("#email").val("");
				}
		});
			
		}
	}
	//验证邮箱注册用户名
	function checkNameEmail(){
		var username=$("#usernameemail").val();
		if(username == ""){
			$("#usernametipemail").text("请输入用户名");
		}else{
			$("#usernametipemail").text("");
			$.post("login_checkName",{"ctUser.UUserid":username},function(data){
					if(data=="success"){
						$("#usernametipemail").text("用户名合法");
					}else {
						$("#usernametipemail").text("用户名已存在请重新输入");
						$("#usernameemail").val("");
					}
			});
		}
	}
	//验证邮箱注册用户密码
	function checkPasswdEmail(){
		var passwd=$("#passwdemail").val();
		if(passwd.length<6){
			$("#passwdemailtip").text("密码长度至少6位");
			$("#passwdemail").val("");
		}else {
			$("#passwdemailtip").text("");
		}
	}
	//验证邮箱注册确认密码是否相同
	function checkPasswd2Email(){
		var passwd=$("#passwdemail").val();
		var passwd2=$("#passwdemail2email").val();
		if(passwd != passwd2){
			$("#passwdemailtip2").text("两次输入的密码不一样");
			$("#passwdemail2email").val("");
		}else{
			$("#passwdemailtip2").text("");
		}
	}
	//验证码
	function showYzm() {
		document.getElementById("realyzm").src="../common/image.jsp?id="+Math.random();
	}
	//检验验证码是否相同
	function checkYzm(){
		var yzm = $("#yzm").val();
		if(yzm == ""){
			$("#yzmtip").text("请输入验证码");
		}else{
			$("#yzmtip").text("");
			$.post("login_checkYzm",{"yzm":yzm},function(data){
				if(data=="success"){
				}else {
					$("#yzmtip").text("输入的验证码不对");
					$("#yzm").val("");
				}
			});
		}
	}
	
	//邮箱注册
	function registerEmail() {
		var email = $("#email").val();
		var username=$("#usernameemail").val();
		var passwd=$("#passwdemail").val();
		var passwd2=$("#passwdemail2email").val();
		var yzm = $("#yzm").val();
		if(email == "" || email=="l.aiyu@163.com"){
			$("#emailtip").text("请输入邮箱");
		}else
		if(username == ""){
			$("#usernametipemail").text("请输入用户名");
		}else
		if(passwd == ""){
			$("#passwdemailtip").text("请输入密码");
		}else
		if(passwd2 == ""){
			$("#passwdemailtip2").text("请输入确认密码");
		}else
		if(yzm == ""){
			$("#yzmtip").text("请输入验证码");
		}else
		if(!($("#isAgree").attr("checked") == "checked")){
			$("#isAgreetip").text("请勾选长亭电子有限公司用户注册协议");
		}else {
			$("#form2").submit();
		}
	}