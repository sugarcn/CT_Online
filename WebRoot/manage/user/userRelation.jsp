<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>

<title>My JSP 'userRelation.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

</head>
<script type="text/javascript" src="js/jquery-latest.pack.js"></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery1.js"></script>
<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
<link rel="stylesheet" type="text/css" href="js/styles.css" />
<link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />

<link rel="stylesheet" href="css/mycss.css" type="text/css" />
<script type="text/javascript">

          function  searchThis(){
          var keyword=$("#key").val();
          var UGId=$("#UGId").val();
          location.href="search_user?keyword="+keyword+"&UGId="+UGId;
          }
          
        
          //右移
          function  toRight(){
		   var flag=true;
		   var selectRight=document.getElementById("selectRight");
		   $("#selectLeft option:selected").each(function(){
		    for(var i=0; i<selectRight.options.length; i++){
		       if($(this).val() ==selectRight.options[i].value ){
		       $(this).remove();
		       flag=false;
		       }
		    }
		    if(flag){
		   $("#selectRight").append("<option value=" + $(this).val() + ">" + $(this).html() + "</option>");
		   $(this).remove();
		   }
		  
		   });
		}
		
		//左移
		function toLeft(){
		
		 var flag=true;
		   var selectLeft=document.getElementById("selectLeft");
		   $("#selectRight option:selected").each(function(){
		   
		   
		      $.post();
		      
		      
		   
		    for(var i=0; i<selectLeft.options.length; i++){
		       if($(this).val() ==selectLeft.options[i].value ){
		       $(this).remove();
		       flag=false;
		       }
		    }
		    if(flag){
		   $("#selectLeft").append("<option value=" + $(this).val() + ">" + $(this).html() + "</option>");
		   $(this).remove();
		   }
		   });
		
		}
		
          //提交保存
          function submit1(){
          var UIDss="";
           var selectLeft=document.getElementById("selectRight");
		  for(var i=0; i<selectLeft.options.length; i++){
		   a =selectLeft.options[i].value;
		   UIDss = UIDss+ a+"@";
          }
           var UGId="${UGId}";
          $.post("<%=basePath%>add_userGroupRelation", {
			"UIds" : UIDss,
			"UGId" : UGId
		}, function(data) {
			if (data = "success") {
				alert("用户添加成功！");
			} else {
				alert("用户添加失败！");
			}

		});
	}

	//全部右移
	function allToRight() {
		var selectLeft = document.getElementById("selectLeft");
		for ( var i = 0; i < selectLeft.options.length; i++) {
			var select = selectLeft.options[i];
			select.selected = true;
		}
		var flag = true;
		var selectRight = document.getElementById("selectRight");
		$("#selectLeft option:selected").each(
				function() {
					for ( var i = 0; i < selectRight.options.length; i++) {
						if ($(this).val() == selectRight.options[i].value) {
							$(this).remove();
							flag = false;
						}
					}
					if (flag) {
						$("#selectRight").append(
								"<option value=" + $(this).val() + ">"
										+ $(this).html() + "</option>");
						$(this).remove();
					}
				});
	}

	//全部左移
	function allToLeft() {
		var selectRight = document.getElementById("selectRight");
		for ( var i = 0; i < selectRight.options.length; i++) {
			var select = selectRight.options[i];
			select.selected = true;
		}
		var flag = true;
		var selectLeft = document.getElementById("selectLeft");
		$("#selectRight option:selected").each(
				function() {
					for ( var i = 0; i < selectLeft.options.length; i++) {
						if ($(this).val() == selectLeft.options[i].value) {
							$(this).remove();
							flag = false;
						}
					}
					if (flag) {
						$("#selectLeft").append(
								"<option value=" + $(this).val() + ">"
										+ $(this).html() + "</option>");
						$(this).remove();
					}
				});

	}
</script>

<body>
	<input id="UGId" name="UGId" value="${UGId}" type="hidden" />
	<div align="center">

		<input type="button" value="查询" onclick="searchThis();"> <input
			id="key" name="key" type="text" />
		<div class="clear"></div>
	</div>
	<table>
		<tr>
			<td><select size='10' multiple id="selectLeft"
				style="width:200px">
					<s:iterator value="users" var="list">
						<option value="${list.UId}">
							<s:property value="#list.UUsername" />
						</option>
					</s:iterator>
			</select>
			</td>
			<td><input type="button" value=" >> " id="allToRight"
				onclick="allToRight()" /><br /> <br />
			<input type="button" value=" > " id="toRight" onclick="toRight()" /><br />
				<br /> <input type="button" value=" < " id="toLeft"
				onclick="toLeft()" /><br /> <br />
			<input type="button" value=" << " id="allToLeft"
				onclick="allToLeft()" />
			</td>
			<td><select size='10' multiple id="selectRight"
				style="width:200px">
					<s:iterator value="user" status="list">
						<option value="<s:property value="user[#list.index][0]" />">
							<s:property value="user[#list.index][1]" />
						</option>
					</s:iterator>
			</select>
			</td>
		</tr>
	</table>
	<input type="button" value="确定" onclick="submit1()" />
</body>
</html>
