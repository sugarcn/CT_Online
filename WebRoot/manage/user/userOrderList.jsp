﻿<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="frm" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/userRank.js" charset="utf-8"></script>
    <script type="text/javascript" src="<%=basePath %>js/jquery-ui.js"></script>
      <script type="text/javascript" 
src="<%=basePath%>manage/js/My97DatePicker/WdatePicker.js" 
<script type="text/javascript">
function  goTabForOrder(orderid){
	window.location.href="order_Tab?orderId="+orderid;

	}
</script>
</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/manage/list_order">订单管理</a></li>
    <li><a href="#">订单列表</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" id="form1" theme="simple">   
     <div class="rightinfo" style="text-align:right">

    <div class="rightinfo">
     <div class="tools">
    	<div class="ssk">
    		<form action="manage/findOrderSnUi" method="post" id="findOrderSnUiAction">
	     		<input type="hidden" name="orderSn" value="" id="vaFindInfo" />
			   <div class="ssbk">
			      <div style="float:left;padding-left:30px;padding-top:1px">
			     	 <input type="text" id="findOrderSn" name="keyword" class="wbdd" onfocus="if(this.value=='订单编号'){this.value=''}" onblur="if(this.value==''){this.value='订单编号'}"
			     	 value="<c:if test="${findOrderSnKey == null || findOrderSnKey == '' }">订单编号</c:if><c:if test="${findOrderSnKey != null && findOrderSnKey != '' }">${findOrderSnKey }</c:if>">
			      </div>
			      <div onclick="findOrderSnUiBtn1()" style="float:left; background:#fff; padding:0;"><img src="<%=basePath%>manage/images/ss.jpg"></div>
			      <div class="clear"></div>
			   </div>
    		</form>
  		</div>
		<div style="float: right;">
  		<input type="hidden" value="${firstDate }" id="firstDate" />
  			<form action="list_order" method="post" id="timeFrom">
				日期: 
				<input class="dfinputdate" name="stime" id="stime" placeholder="起始时间" value="${stime }" onfocus="javascript:WdatePicker({skin:'whyGreen',dateFmt:'yyyy-MM-dd'});" type="text" />
				-
				<input class="dfinputdate" placeholder="结束时间" id="etime" name="etime" value="${etime }" onfocus="WdatePicker({skin:'whyGreen',dateFmt:'yyyy-MM-dd'})" type="text" />
				<div onclick="findOrderByTime()" style="float:right;width:62px; background:#fff; padding:0;display: none;">
					<img style="height:30px;" src="<%=basePath%>manage/images/ss.jpg">
				</div>
				<input type="hidden" name="orderSn" id="orderSn" />
			</form>
  		</div>
  		<div style="float:right;margin-right:30px;">
	    	<img  src="../images/icon.png"/> 加急件&nbsp;<img src="../images/006.png"/> 已处理
		</div>
  	</div>
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>订单号</th>
        <th>下单时间</th>
        <th>付款时间</th>
        <th>收货人</th>
        <th>购货人</th>
        <th>总金额</th>
        <th>付款金额</th>
        <th>退款信息</th>
        <th>订单状态</th>
        <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${checks }" var="list">
	        <tr>
	        	<td width="228" height="40" align="center" class="modify">
	        	<c:if test="${list.urgentOrder != null && list.urgentOrder!='-1' }">
	        		<img class="jjddtb" src="../images/icon.png"/>
	        	</c:if>
	        	<c:if test="${list.orderStatus == 8 || list.orderStatus == 4 || list.orderStatus == 5 }">
		        	<img class="jjddtb" src="../images/006.png"/>
	        	</c:if>
	        	<a href="order_Tab?orderDTO.orderid=${list.orderId }">${list.orderSn }</a>
	        	</td>
	        	<td width="228" align="center" class="modify">${list.orderTime }</td>
	        	<td width="228" align="center" class="modify">
	        		<c:if test="${list.pay != 1 && list.xianPayDate != null }">
	        			${list.xianPayDate }
	        		</c:if>
	        		<c:if test="${list.pay == 1 && list.xiaPayDate != null }">
		        		${list.xiaPayDate }
	        		</c:if>
	        	</td>
	        	<td width="228" align="center" class="modify">${list.consignee }</td>
	        	<td width="228" align="center" class="modify">${list.ctUser.UUsername }</td>
	        	<td width="228" align="center" class="modify"><frm:formatNumber pattern="0.00">${list.total }</frm:formatNumber></td>
	        	<td width="228" align="center" class="modify">
	        		<c:if test="${list.pay != 1 }">
	        			<c:if test="${list.orderStatus != 0 && list.orderStatus != 6 && list.orderStatus != 7 }">
	        				<frm:formatNumber pattern="0.00">${list.total }</frm:formatNumber>
	        			</c:if>
	        		</c:if>
	        		<c:if test="${list.pay == 1 }">
		        		<frm:formatNumber pattern="0.00">${list.payPrice }</frm:formatNumber>
	        		</c:if>
	        	</td>
	        	<td width="228" align="center" class="modify">
	        		<c:if test="${list.isRefinfo == '1' }">
	        			存在退款信息
	        		</c:if>
	        		<c:if test="${list.isRefinfo == '0' }">
	        			已退款
	        		</c:if>
	        		<c:if test="${list.isRefinfo == '3' }">
	        			退款驳回
	        		</c:if>
	        	</td>
	        	<td width="228" align="center" class="modify">
	        		<c:if test="${list.orderStatus == 0 }">待付款</c:if>
	        		<c:if test="${list.orderStatus == 1 }">已付款</c:if>
	        		<c:if test="${list.orderStatus == 2 }">待审核 &nbsp;&nbsp;${list.retStatus }</c:if>
	        		<c:if test="${list.orderStatus == 3 }">配货中 &nbsp;&nbsp;${list.retStatus }</c:if>
	        		<c:if test="${list.orderStatus == 4 }">已发货 &nbsp;&nbsp;${list.retStatus }</c:if>
	        		<c:if test="${list.orderStatus == 5 }">已完成</c:if>
	        		<c:if test="${list.orderStatus == 6 }">取消</c:if>
	        		<c:if test="${list.orderStatus == 7 }">无效</c:if>
	        		<c:if test="${list.orderStatus == 8 }">已配货</c:if>
	        	</td>
	        	<td width="228" align="center" class="modify">
	        	<a href="order_Tab?orderDTO.orderid=${list.orderId }">查看订单</a>
	        	</td>
	        </tr> 
        </c:forEach>
        </tbody>
    </table>
    </div>
   </s:form>     
       ${pages.pageStr1}
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	
		function findOrderSnUiBtn1(){
			var va = $("#findOrderSn").val();
			if(va == "订单编号"){
				//location.href="list_order?page=1";
				findOrderByTime();
				return;
			}
			$("#vaFindInfo").val(va);
			va = escape(va);
			$("#orderSn").val(va);
			findOrderByTime();
			//location.href="list_order?page=1&orderSn="+va;
		}
		function pageTiaoZhuang(num){
			var stime = $("#stime").val();
			var etime = $("#etime").val();
			var aa = escape($("#findOrderSn").val()); 
			if(stime != "" && etime != ""){
				location.href="list_order?page="+num+"&orderSn="+aa + "&stime="+stime + "&etime=" + etime;
			} else {
				location.href="list_order?page="+num+"&orderSn="+aa;
			}
		}
		function findOrderByTime(){
			var stime = $("#stime").val();
			var etime = $("#etime").val();
			if(stime == "" && etime != ""){
				alert("请输入起始时间");
				$("#stime").val(getNowFormatDate());
				return;
			}
			if(etime == "" && stime != ""){
				alert("请输入结束时间");
		    	$("#etime").val(getNowFormatDate());
				return;
			}
			var oDate1 = new Date(stime);
		    var oDate2 = new Date(etime);
		    if(oDate1.getTime() > oDate2.getTime()){
		    	alert("起始时间不能小于结束时间");
		    	$("#stime").val(getNowFormatDate());
		    	$("#etime").val(getNowFormatDate());
		    	return;
		    }
		    $("#timeFrom").submit();
		}
		function setDefaultTime(){
			var stime = $("#stime").val()
			if(stime == ""){
				$("#stime").val($("#firstDate").val());
		    	$("#etime").val(getNowFormatDate());
			}
		}
		//setDefaultTime();
		//获取当前时间，格式YYYY-MM-DD
	    function getNowFormatDate() {
	        var date = new Date();
	        var seperator1 = "-";
	        var year = date.getFullYear();
	        var month = date.getMonth() + 1;
	        var strDate = date.getDate();
	        if (month >= 1 && month <= 9) {
	            month = "0" + month;
	        }
	        if (strDate >= 0 && strDate <= 9) {
	            strDate = "0" + strDate;
	        }
	        var currentdate = year + seperator1 + month + seperator1 + strDate;
	        return currentdate;
	    }
	</script>
</body>

</html>
