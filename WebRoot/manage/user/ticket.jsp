<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>增票管理</title>

<link href="<%=basePath%>manage/css/style.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript" src="<%=basePath%>manage/js/jquery.js"></script>
<script type="text/javascript"
	src="<%=basePath%>manage/js/jquery-latest.pack.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=basePath%>manage/js/style.css" />
<script type="text/javascript"
	src="<%=basePath%>manage/js/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>manage/js/jquery.leanModal.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=basePath%>manage/js/styles.css" />
<link rel="stylesheet" type="text/css"
	href="<%=basePath%>manage/js/jquery.confirm.css" />
<link rel="stylesheet" href="<%=basePath%>manage/css/mycss.css"
	type="text/css" />
<script type="text/javascript" src="<%=basePath%>manage/js/ct.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="<%=basePath%>manage/js/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css"
	href="<%=basePath%>manage/js/jquery.fancybox.css?v=2.1.5"
	media="screen" />
<script type="text/javascript"> 

$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
		</script>
<script type="text/javascript" src="<%=basePath%>manage/js/ticket.js"></script>
<script type="text/javascript">
		$(document).ready(function() {
			$('.fancybox').fancybox();
		});
		
		
	
		
		
	function showBigImg(imgpath) {
		$("img").attr("src", imgpath);
		$("#signup").show();
	}
</script>

</head>

<body>
	<div class="place">
		<span>位置：</span>
		<ul class="placeul">
			<li><a href="#">增票管理</a></li>
			<li><a href="#">增票列表</a></li>
		</ul>
	</div>
	<s:form method="post" name="form1" id="form1" theme="simple">
	 <input type="hidden" id="passA" value="ticket_pass">
	 <input type="hidden" id="notPassA" value="ticket_notPass">
	 
		<div class="rightinfo">
			<div class="tools">
				<ul class="toolbar">
					<li onclick="allPass();"><span><img
							src="<%=basePath%>manage/images/t07.png" width="24" height="24" />
					</span>所有审核通过</li>
					<li onclick="allNotPass();"><span><img
							src="<%=basePath%>manage/images/t08.png" width="24" height="24" />
					</span>所有审核不通过</li>
				</ul>
				<div class="ssk">
					<div class="ssbk">
						<div style="float:left;padding-left:30px;">
							<input type="text" id="keyword" name="keyword" class="wbdd"
								onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}"
								onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"
								value="请输入你要查询的关键词">
						</div>
						<div style="float:right">
							<img src="<%=basePath%>manage/images/ss.jpg" class="searchItem">
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>

			<table class="tablelist">
				<thead>
					<tr align="center" >
						<th><input type='checkbox' id='checkall' name='checkall' />
						</th>
						<th>用户名</th>
						<th>单位名称</th>
						<th>纳税人识别号</th>
						<th>注册地址</th>
						<th>注册电话</th>
						<th>开户银行</th>
						<th>银行账户</th>
						<th>营业执照副本</th>
						<th>税务登记证副本</th>
						<th>纳税人资格证书</th>
						<th>银行开户许可证</th>
						<th>是否通过审核</th>
					</tr>
				</thead>
				<tbody>
					<s:iterator value="addTickets" var="list">
						<tr id="sb">
							<td width="29" height="38" style="padding-left:10px">
							<input	type="checkbox"  name="mid" value=${list.ATId } />
							</td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.ctUser.UUsername" /></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.companyName" /></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.companySn" /></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.registeAddress" /></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.registeTel" /></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.depositBank" /></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.bankAccount" /></td>
							<td width="212" align="center" class="modify">
								<a class="fancybox" href="<%=basePath%>${list.lisence }"
								data-fancybox-group="gallery"> <img
									src="<%=basePath%><s:property value="#list.lisenceSmall"/>"
									style="width: 25px;height: 25px" /> </a>
							</td>
							<td width="212" align="center" class="modify"><a
								class="fancybox" href="<%=basePath%>${list.taxCertifi }"
								data-fancybox-group="gallery"> <img
									src="<%=basePath%><s:property value="#list.taxCertifiSmall"/>"
									style="width: 25px;height: 25px" /> </a>
							</td>
							<td width="212" align="center" class="modify"><a
								class="fancybox" href="<%=basePath%>${list.generalCertifi }"
								data-fancybox-group="gallery"> <img
									src="<%=basePath%><s:property value="#list.generalCertifiSmall"/>"
									style="width: 25px;height: 25px" /> </a>
							</td>
							<td width="212" align="center" class="modify"><a
								class="fancybox" href="<%=basePath%>${list.bankLisence }"
								data-fancybox-group="gallery"> <img
									src="<%=basePath%><s:property value="#list.bankLisenceSmall"/>"
									style="width: 25px;height: 25px" /> </a>
							</td>
							<s:if test="#list.isPass==0">
								<td width="212" align="center" class="tr_zp">不通过</td>
							</s:if>
							<s:else>
								<td width="212" align="center" class="modify">通过</td>
							</s:else>
						</tr>
					</s:iterator>
					
				</tbody>
			</table>
			<ul class="toolbar" style="margin-top:15px;">
					<li class="deleteItem" ><span><img
							src="<%=basePath%>manage/images/t07.png" width="24" height="24" />
					</span>审核通过</li>
					<li class="deleteItem1" ><span><img
							src="<%=basePath%>manage/images/t08.png" width="24" height="24" />
					</span>审核不通过</li>
				</ul>
			 ${pages.pageStr}
		</div>
	</s:form>
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	
	

</body>

</html>