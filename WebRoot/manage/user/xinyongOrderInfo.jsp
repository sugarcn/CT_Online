<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>My JSP 'userRelation.jsp' starting page</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/user.js" charset="utf-8"></script>
</head>

<body>
	<div class="place">
		<span>位置：</span>
		<ul class="placeul">
			<li><a href="#">用户管理</a></li>
			<li><a href="#">用户列表</a></li>
			<li><a href="#">信用支付列表</a></li>
		</ul>
	</div>
	<input type="hidden" id="searchA" value="search_user1">
	<input type="hidden" id="orderInfoUid" value="${orderInfoUid }">
	<s:form method="post" name="form1" id="form1" theme="simple">
		<div class="rightinfo">
			<ul class="toolbar">
				<li onclick="javascript:location.href='findXinYongOrderInfo?orderDTO.uid=${orderInfoUid }';"><span><img src="images/t07.png" width="24" height="24" /></span>全部</li>
				<li onclick="findType(1,${orderInfoUid });"><span><img src="images/t07.png" width="24" height="24" /></span>已还款</li>
				<li onclick="findType(2,${orderInfoUid });"><span><img src="images/t08.png" width="24" height="24" /></span>未还款</li>
				<li onclick="findOrderInfoByXianXia(3,${orderInfoUid });"><span><img src="images/t08.png" width="24" height="24" /></span>线下支付审核</li>
			</ul>
				<font style="position:absolute;top:56px;width:300px;left:450px;">剩余还款订单总金额<b>${shenCountPrice }</b></font>
			<table class="tablelist">
				<thead>
					<tr align="center">
						<th width="212" align="center" bgcolor="#909090">订单编号</th>
						<th width="262" align="center" bgcolor="#909090">订货人</th>
						<th width="212" align="center" bgcolor="#909090">下单时间</th>
						<th width="212" align="center" bgcolor="#909090">订单金额</th>
						<th width="212" align="center" bgcolor="#909090">支付时间</th>
						<th width="212" align="center" bgcolor="#909090">是否还款</th>
					</tr>
				</thead>
				<tbody>
					<s:iterator value="checks" var="list">
						<tr  class="user_tr">
							<td width="262" align="center" class="modify" id="uuserid"><a href="order_Tab?orderDTO.orderid=${list.orderId }"><s:property
									value="#list.orderSn" /></a></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.consignee" /></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.orderTime" /></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.total" /></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.payInterface.payDate" /></td>
							<td width="212" align="center" class="modify">
								<c:if test="${list.pay == 5 }">未还款</c:if>
								<c:if test="${list.pay == 6 }">已还款</c:if>
								<c:if test="${list.pay == 7 }">线下支付已提交申请</c:if>
								<c:if test="${list.pay == 8 }">已还款</c:if>
							</td>
						</tr>
					</s:iterator>
				</tbody>
			</table>
			
			  ${pages.pageStr1}
			  
	</s:form>
	
	
	<div id="signup1">
		<div id="signup-ct">
			<div id="signup-header">
				<h2>修改客户</h2>
				<a class="modal_close" href="#"></a>
			</div>
			<form>
				<div id="signupmodi"></div>

				<div class="btn-fld">
					<div id="tipHtmlModi" style="color:red;float:left"></div>
					<button class="usergroup_qd" type="submit"
						onclick="return checkThis(this.form,'update_user',1);">确定</button>
				</div>
			</form>
		</div>
	</div>

	<script src="js/jquery.confirm.js"></script>
	<script src="js/script.js"></script>
	
    </div>
    <form action="" method="post" id="userOrder">
    	<input type="hidden" name="uid" id="userId"  />
    	<input type="hidden" name="uname" id="userName"  />
    	<input type="hidden" name="keyword" id="keyword"  />
    </form>
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	function userOrderListInfo(uid, uname){
		$("#userId").val(uid);
		$("#userName").val(uname);
		$("#userOrder").attr("action","list_order");
		$("#userOrder").submit();
	}
	function findXinyongOrderInfo(uid, uname){
		$("#userId").val(uid);
		$("#userName").val(uname);
		$("#userOrder").attr("action","findXinYongOrderInfo");
		$("#userOrder").submit();
	}
	function findOrderInfoByXianXia(type, uid){
		$("#userId").val(uid);
		$("#userOrder").attr("action","findUserXianCre");
		$("#userOrder").submit();
	}
	function findType(type, uid){
		$("#keyword").val(type);
		$("#userId").val(uid);
		$("#userOrder").attr("action","findXinYongOrderInfo");
		$("#userOrder").submit();
	}
	
	function pageTiaoZhuang(num){
		var keyword = $("#keyword").val();
		if(keyword == "请输入你要查询的关键词" || keyword == ""){
			keyword = "";
		}
		location.href="findXinYongOrderInfo?page="+num+"&uid="+$("#orderInfoUid").val();
	}
	</script>
	

</body>

</html>
