<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>公告管理</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="../js/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.all.js"></script>
<script type="text/javascript" charset="utf-8" src="../js/ueditor/lang/zh-cn/zh-cn.js"></script>
<link type="text/css" src="../js/ueditor/themes/default/css/ueditor.css"/>
<script type="text/javascript" src="../js/ajaxfileupload.js" charset="utf-8"></script>
<script type="text/javascript" src="js/notice.js" charset="utf-8"></script>


</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">活动管理</a></li>
    <li><a href="list_draw">抽奖次数管理</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" id="form1" theme="simple">   
 
 <input type="hidden" id="deleteA" value="delete_notice">
 <input type="hidden" id="findA" value="find_notice">
 <input type="hidden" id="searchA" value="search_notice">
 <input type="hidden" id="updateA" value="update_notice"> 
 
    <div class="rightinfo">
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>用户姓名</th>
        <th>剩余抽奖次数</th>
        <th>操作</th>
        </tr>
        </thead>
        <tbody>
  <s:iterator value="drawsList" var="list">      
        <tr align="center">
        	<td width="400" align="center" class="modify"><s:property value="#list.user.UUsername"/></td>
        	<td width="400" align="center" class="modify"><s:property value="#list.drNum"/></td>
        	<td width="400" align="center" class="modify">
        		<a href="javascript:;" onclick="addOneDraw(${list.drId},${list.drNum })">加一次抽奖机会</a> 
        		<a href="javascript:;" onclick="clearOneDraw(${list.drId},${list.drNum })">清空抽奖次数</a>
        	</td>
        </tr> 
</s:iterator>       
        </tbody>
    </table>
   
     ${pages.pageStr}
     
   </s:form> 
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	

</body>
	<script type="text/javascript">
		function addOneDraw(drid, deNum){
			if(confirm("确定给当前用户添加一次抽奖机会？当前用户剩余抽奖机会为："+deNum)){
				window.location.href="addOneDrawNum?drId="+drid;
			}
		}
		function clearOneDraw(drid, deNum){
			if(confirm("确定给当前用户清空抽奖机会？当前用户剩余抽奖机会为："+deNum)){
				window.location.href="clearDrawNum?drId="+drid;
			}
		}
	</script>
</html>
