<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>增票管理</title>

<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
        
        <link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript"> 

$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
		</script>
</head>

<body>
	<div align="center">
			<s:form action="fileUpload" id="form" method="post" enctype="multipart/form-data">
			<table border="1" width="960" cellspacing="0" cellpadding="0" bordercolor="#D2CFCF" style="border-collapse: collapse">
				<tr style="font-weight:700;color:#fff;font-size:14px">
					<td width="30" align="left"><input type="hidden" id="UId" name="ctUser.UId"/></td>
				</tr>
				<tr style="font-weight:700;color:#fff;font-size:14px">
					<td width="50" align="center" bgcolor="gray">单位名称:</td>
					<td width="50" align="left"><input type="text" id="companyName" name="addTicket.companyName"/></td>
				</tr>
				<tr style="font-weight:700;color:#fff;font-size:14px">
					<td width="50" align="center" bgcolor="gray">纳税人识别号:</td>
					<td width="50" align="left"><input type="text" id="companySn" name="addTicket.companySn"/></td>
				</tr>
				<tr style="font-weight:700;color:#fff;font-size:14px">
					<td width="50" align="center" bgcolor="gray">注册地址:</td>
					<td width="50" align="left"><input type="text" id="registeAddress" name="addTicket.registeAddress"/></td>
				</tr>
				<tr style="font-weight:700;color:#fff;font-size:14px">
					<td width="50" align="center" bgcolor="gray">注册电话:</td>
					<td width="50" align="left"><input type="text" id="registeTel" name="addTicket.registeTel"/></td>
				</tr>
				<tr style="font-weight:700;color:#fff;font-size:14px">
					<td width="50" align="center" bgcolor="gray">开户银行:</td>
					<td width="50" align="left"><input type="text" id="depositBank" name="addTicket.depositBank"/></td>
				</tr>
				<tr style="font-weight:700;color:#fff;font-size:14px">
					<td width="50" align="center" bgcolor="gray">银行账户:</td>
					<td width="50" align="left"><input type="text" id="bankAccount" name="addTicket.bankAccount"/></td>
				</tr>
				<tr style="font-weight:700;color:#fff;font-size:14px">
					<td width="238" align="center" bgcolor="gray">营业执照副本:</td>
					<td width="50" align="left"><input type="file" name="file"/></td>
				</tr>
				<tr style="font-weight:700;color:#fff;font-size:14px">
					<td width="50" align="center" bgcolor="gray">税务登记证副本:</td>
					<td width="50" align="left"><input type="file"  name="file"/></td>
				</tr>
				<tr style="font-weight:700;color:#fff;font-size:14px">
					<td width="50" align="center" bgcolor="gray">一般纳税人资格证书:</td>
					<td width="50" align="left"><input type="file" name="file"/></td>
				</tr>
				<tr style="font-weight:700;color:#fff;font-size:14px">
					<td width="50" align="center" bgcolor="gray">银行开户许可证:</td>
					<td width="50" align="left"><input type="file" name="file"/></td>
				</tr>
				<tr align="center">
					<td align="left">
						<input type="submit" value="提交"/>
					</td>
				</tr>
			</table>
			</s:form>
	</div>
</body>
</html>