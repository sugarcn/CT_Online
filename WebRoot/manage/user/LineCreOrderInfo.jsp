﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>My JSP 'userRelation.jsp' starting page</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/user.js" charset="utf-8"></script>
</head>
<script type="text/javascript">

function goBomList(UId,UUser){
var flag = 1;

location.href="bom_goBomList?userDTO.UId="+UId+"&userDTO.Flag="+flag+"&userDTO.UUserid="+UUser;
}

</script>

<body>
	<div class="place">
		<span>位置：</span>
		<ul class="placeul">
			<li><a href="#">用户管理</a></li>
			<li><a href="#">用户列表</a></li>
			<li><a href="#">信用支付列表</a></li>
			<li><a href="#">线下还款列表</a></li>
		</ul>
	</div>
	<input type="hidden" id="searchA" value="search_user1">
			<ul class="toolbar" style="margin-top: 10px;">
				<li onclick="history.go(-1);"><span><img src="images/t06.png" width="24" height="24" /></span>返回</li>
			</ul>
	<s:form method="post" name="form1" id="form1" theme="simple">
			<table class="tablelist">
				<thead>
					<tr align="center">
						<th width="212" align="center" bgcolor="#909090">付款人</th>
						<th width="262" align="center" bgcolor="#909090">付款时间</th>
						<th width="212" align="center" bgcolor="#909090">付款金额</th>
						<th width="212" align="center" bgcolor="#909090">付款人电话</th>
						<th width="212" align="center" bgcolor="#909090">付款银行</th>
						<th width="212" align="center" bgcolor="#909090">汇入银行</th>
						
						<th width="212" align="center" bgcolor="#909090">付款说明</th>
						<th width="212" align="center" bgcolor="#909090">查看图片</th>
						
					</tr>
				</thead>
				<tbody>
					<s:iterator value="paysList" var="list">
						<tr class="user_tr" name="lineList" onclick="clickFindOrderInfo(this,${list.orid})">
							<td width="262" align="center" class="modify" id="uuserid"><s:property
									value="#list.CName" /></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.payDate" /></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.payAmount" /></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.CTel" /></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.CAName" /></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.bank.accountName" /></td>
							
							<td width="212" align="center"  class="modify">${list.payDesc }</td>
							<td width="212" align="center" class="modify">
								<a href="${list.payFile }" target="_black">点击查看</a>
							</td>
						</tr>
					</s:iterator>
				</tbody>
			</table>
			
			  ${pages.pageStr1}
			  
	</s:form>
	
	
	<table class="tablelist">
		<thead>
			<tr align="center">
				<th width="212" align="center" bgcolor="#909090">订单编号</th>
				<th width="262" align="center" bgcolor="#909090">订单时间</th>
				<th width="212" align="center" bgcolor="#909090">订单金额</th>
				<th width="212" align="center" bgcolor="#909090">下单人</th>
				<th width="212" align="center" bgcolor="#909090">联系电话</th>
			</tr>
		</thead>
		<tbody id="showOrderInfo">
		</tbody>
	</table>
	<input type="button" id="okCre"  class="go" style="width: 100px; margin-top:10px; margin-right:10px; float: right; display: none;" value="审核通过" />
	<script src="js/jquery.confirm.js"></script>
	<script src="js/script.js"></script>
	
    </div>
    <form action="" method="post" id="userOrder">
    	<input type="hidden" name="orid" id="orid"  />
    </form>
    <script type="text/javascript">
    
    
    function clickFindOrderInfo(y, orid){
    	var $yuan = $(y);
    	$("tbody").children("tr[class=user_tr]").each(function(){
    		$(this).attr("style","");
    	});
    	$yuan.attr("style","background:#e5ebee;");
    	
    	$.post(
    		"findOrderInfoByOrId",
    		{"orid":orid},
    		function(data, varstart){
    			$("#showOrderInfo tr").remove();
    			$("#okCre").attr("onclick","");
    			var orderInfoList = data.split("+");
    			var str = "";
    			if(orderInfoList != ""){
	    			for(var i=0; i < orderInfoList.length; i++){
	    				var orderInfo = orderInfoList[i].split("|");
	    				str += "<tr align=\"center\">";
	    				str += "<td width=\"262\" align=\"center\" class=\"modify\" id=\"uuserid\">"+orderInfo[0]+"</td>";
	    				str += "<td width=\"262\" align=\"center\" class=\"modify\" id=\"uuserid\">"+orderInfo[1]+"</td>";
	    				str += "<td width=\"262\" align=\"center\" class=\"modify\" id=\"uuserid\">"+orderInfo[2]+"</td>";
	    				str += "<td width=\"262\" align=\"center\" class=\"modify\" id=\"uuserid\">"+orderInfo[3]+"</td>";
	    				str += "<td width=\"262\" align=\"center\" class=\"modify\" id=\"uuserid\">"+orderInfo[4]+"</td>";
	    				str += "</tr>";
	    			}
	    			$("#okCre").show();
	    			$("#showOrderInfo").html(str);
	    			$("#okCre").attr("onclick","onCre("+orid+")");
    			} else {
	    			$("#okCre").hide();
    			}
    		}
    	);
    	
    	
    }
    function onCre(orid){
    	$("#orid").val(orid);
    	$.post(
    		"okCreLinePay",
    		{"orid":orid},
    		function(data, varstart){
    			location.reload();
    		}
    	);
    }
    
	function pageTiaoZhuang(num){
		location.href="findUserXianCre?page="+num;
	}
	function loadPage(){
		var lis = $("tr[name=lineList]");
		if(lis.length == 0){
			$(".pagin").hide();
		}
	}
	loadPage()
	</script>
	

</body>

</html>
