<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    
    <title>客户分配客户经理</title>
    <link href="<%=basePath%>manage/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=basePath%>manage/js/jquery.js"></script>
<script type="text/javascript" src="<%=basePath%>manage/js/jquery-latest.pack.js" ></script>
		<script type="text/javascript" src="<%=basePath%>manage/js/jquery.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>manage/js/jquery.leanModal.min.js"></script>
<script type="text/javascript" src="<%=basePath%>manage/js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="<%=basePath%>manage/js/user.js" charset="utf-8"></script>

<script type="text/javascript">
$(".user_search").live("click", function () {
		var keyword = $("#keyword").val();
		var utpye = $("#utpye").val();
		var UId = $("#UId").val(); 
		if(keyword == "" || keyword== "请输入你要查询的关键词"){
		alert("请输入你要查询的关键词");
		return false;
		}else{
		
		location.href="manageSearch?keyword="+keyword+"&UType ="+utpye+"&UId="+UId;
		
		}
    });


</script>
  </head>
  
  <body>
   <div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">客户管理</a></li>
    <li><a href="#">分配客户经理</a></li>
    <li><a href="#">用户：${UUser}</a></li>
    </ul>
    </div>
    <div class="formbody">
    
    <div class="formtitle"><span>基本信息</span></div>
    <div class="ssk">
     
		   <div class="ssbk">
		      <div style="float:left;padding-left:30px;padding-top:1px">
		        <input type="text" id="keyword" name="keyword" class="user_sousuo" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="${keyword}">
		        <a class="user_search"><img src="images/ss.jpg"/></a>
		      </div>
		      
		      <div class="clear"></div>
		   </div>
   
  </div>
  <form action="manageSearch"  id="form1" name="form1" method="post">
    <ul class="forminfo">
    <input type="hidden" id="utpye" value="${Utype}"/>
	<input type="hidden" id="UId" value="${UId}"/>
<!-- 	<input type="hidden" id="UId" value="${UUser}"/> -->
     <s:iterator value="cmAndCumr" status="st">
	<s:if test="cmAndCumr[#st.index][2] != null">
    <li><cite><input type="radio" id="MManagerId" name="MManagerId" checked="checked" value="<s:property value="cmAndCumr[#st.index][0]"/>"/><s:property value="cmAndCumr[#st.index][1]"/></cite></li>
    </s:if>
    <s:else>
    <li><cite><input type="radio" id="MManagerId" name="MManagerId" value="<s:property value="cmAndCumr[#st.index][0]"/>"/><s:property value="cmAndCumr[#st.index][1]"/></cite></li>
                                  </s:else>
                                   </s:iterator>
    <li><label>&nbsp;</label><input type="button" value="保存" onclick="save()" class="btn" /></li>
    </ul>
    
    </form>
    </div>
  </body>
</html>
