﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>My JSP 'userRelation.jsp' starting page</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/user.js" charset="utf-8"></script>
</head>
<script type="text/javascript">

function goBomList(UId,UUser){
var flag = 1;

location.href="bom_goBomList?userDTO.UId="+UId+"&userDTO.Flag="+flag+"&userDTO.UUserid="+UUser;
}
</script>

<body>
	<div class="place">
		<span>位置：</span>
		<ul class="placeul">
			<li><a href="#">用户管理</a></li>
			<li><a href="#">用户列表</a></li>
		</ul>
	</div>
	<input type="hidden" id="searchA" value="search_user1">
	<s:form method="post" name="form1" id="form1" theme="simple">
		<div class="rightinfo">
			<div class="tools">
			<ul class="toolbar">
				<li rel="leanModal1" class="modiItem" href="#signup1"><span><img src="images/t02.png" width="24" height="24" /></span>修改</li>
			</ul>
				<div class="ssk">
					<div class="ssbk">
						<div style="float:left;padding-top:1px; margin-left:30px;">
							<input type="text" id="keyword" name="keyword" class="wbdd"
								onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}"
								onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"
								value="${keyword}" style="height:34px;">
						</div>
						<div style="float:right">
							<img src="<%=basePath%>manage/images/ss.jpg" class="searchItem">
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>

			<table class="tablelist">
				<thead>
					<tr align="center">
						<th><input type='checkbox' id='checkall' name='checkall' />
						</th>
						<th width="212" align="center" bgcolor="#909090">用户名</th>
						<th width="262" align="center" bgcolor="#909090">姓名</th>
						<th width="212" align="center" bgcolor="#909090">手机号</th>
						<th width="212" align="center" bgcolor="#909090">Email</th>
						<th width="212" align="center" bgcolor="#909090">QQ账号</th>
						<th width="212" align="center" bgcolor="#909090">注册时间</th>
						<th width="212" align="center" bgcolor="#909090">性别</th>
						
						<th width="212" align="center" bgcolor="#909090">信用额度</th>
						<th width="212" align="center" bgcolor="#909090">剩余额度</th>
						
						<th width="212" align="center" bgcolor="#909090">操作</th>
					</tr>
				</thead>
				<tbody>
					<s:iterator value="users" var="list">
						<tr  class="user_tr">
							<td width="36" height="38" style="padding-left:10px"><input
								type="checkbox" id="mid" name="mid" value=${list.UId } />
							</td>
							<td width="262" align="center" class="modify" id="uuserid"><s:property
									value="#list.UUserid" /></td>
									<input type="hidden" id="uuid" name="uuserid" value="${list.UUserid }" />
							<td width="212" align="center" class="modify"><s:property
									value="#list.UUsername" /></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.UMb" /></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.UEmail" /></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.UQq" /></td>
							<td width="212" align="center" class="modify"><s:property
									value="#list.URegTime" /></td>
							<s:if test="#list.USex==男 ">
								<td width="212" align="center" class="modify">男</td>
							</s:if>
							<s:elseif test="#list.USex==女">
								<td width="212" align="center" class="modify">女</td>
							</s:elseif>
							<s:else>
							<td width="212" align="center" class="modify">  </td>
							</s:else>
							
							<td width="212" align="center" name="credit" id="${list.UId }_cre" class="modify">${list.UCreditLimit }</td>
							<td width="212" align="center" class="modify" id="${list.UId }_rem">
								<a href="javascript:;" onclick="findXinyongOrderInfo(${list.UId},'${list.UUsername }')">${list.URemainingAmount }</a>
							</td>
							
							
							<td width="400" align="center" class="modify">
								<c:if test="${list.UIsCustomer == 1 }">
									<a href="javascript:;" onclick="userCusList(${list.UId},'${list.UUserid }');">客户管理 </a>/
									<!-- <a href="javascript:;" onclick="customerMent(${list.UId});">客户管理 </a>/  -->
								</c:if>
								<c:if test="${list.UIsCustomer == null ||  list.UIsCustomer == 0}">
									<a href="javascript:;" onclick="updateUserCus(${list.UId},'${list.UUserid }');">升级客服账户 </a>/
								</c:if>

							<a href="javascript:;" onclick="userOrderListInfo(${list.UId},'${list.UUsername }');">订单详细 </a>/
							<a href="javascript:goBomList(<s:property value="#list.UId"/>,'<s:property value="#list.UUserid"/>')">BOM管理</a>
							</td>
						</tr>
					</s:iterator>
				</tbody>
			</table>
			
			  ${pages.pageStr1}
			  
	</s:form>
	
	
	<div id="signup1">
		<div id="signup-ct">
			<div id="signup-header">
				<h2>修改客户</h2>
				<a class="modal_close" href="#"></a>
			</div>
			<form>
				<div id="signupmodi"></div>

				<div class="btn-fld">
					<div id="tipHtmlModi" style="color:red;float:left"></div>
					<button class="usergroup_qd" type="submit"
						onclick="return checkThis(this.form,'update_user',1);">确定</button>
				</div>
			</form>
		</div>
	</div>

	<script src="js/jquery.confirm.js"></script>
	<script src="js/script.js"></script>
	
    </div>
    <form action="" method="post" id="userOrder">
    	<input type="hidden" name="uid" id="userId"  />
    	<input type="hidden" name="uname" id="userName"  />
    </form>
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	function userOrderListInfo(uid, uname){
		$("#userId").val(uid);
		$("#userName").val(uname);
		$("#userOrder").attr("action","list_order");
		$("#userOrder").submit();
	}
	function findXinyongOrderInfo(uid, uname){
		$("#userId").val(uid);
		$("#userName").val(uname);
		$("#userOrder").attr("action","findXinYongOrderInfo");
		$("#userOrder").submit();
	}
	function pageTiaoZhuang(num){
		var keyword = $("#keyword").val();
		if(keyword == "请输入你要查询的关键词" || keyword == ""){
			keyword = "";
		}
		location.href="search_user1?page="+num + "&keyword="+keyword;
	}
	function updateUserCus(uid, uuid){
		if(confirm("升级后只能管理客户地址，原有地址将无法使用，请及时备份！")){
			location.href="setUpUserCus?UId=" + uid + "&UUserid=" + uuid;
		}
	}
	function userCusList(uid, uuid){
		location.href="userCusList?UId="+ uid + "&UUserid=" + uuid;
	}
	</script>
	

</body>

</html>
