<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>My JSP 'userRelation.jsp' starting page</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/cusUserAddress.js" charset="utf-8"></script>
</head>

<body>
	<div class="place">
		<span>位置：</span>
		<ul class="placeul">
			<li><a href="#">用户管理</a></li>
			<li><a href="#">用户列表</a></li>
			<li><a href="#">客服${uuid }</a></li>
		</ul>
	</div>
	<input type="hidden" id="searchA" value="search_user1">
	<input type="hidden" id="orderInfoUid" value="${orderInfoUid }">
	<s:form method="post" name="form1" id="form1" theme="simple">
	
	<input type="hidden" id="clientId" name="clientId"  />
 <input type="hidden" id="deleteA" value="delete_CusAddress">
 <input type="hidden" id="findA" value="find_notice">
 <input type="hidden" id="searchA" value="search_notice">
 <input type="hidden" id="updateA" value="update_notice"> 
 
	
		<div class="rightinfo">
			<div class="tools">
			<input style="width:70px;height:35px;margin-left: 154px;position: absolute;" type="button" class="go" onclick="location.href='user_list'" value="返回">
			<ul class="toolbar" style="width: 90%;">
	        	<a href="
	        		<c:forEach items="${uesrClientsList }" var="l">
						<c:if test="${l.cname == '原账户地址' }">
							findCusAddress?clientId=${l.cid }&cusName=${l.cname }&UId=${l.uid }
						</c:if>
					</c:forEach>
	        	" style="position: absolute;left: 243px;color: blue;cursor: pointer;">原账户地址</a>
	        	<li name="signup" onclick="delSelCus()" style="float: right;position: absolute;"><span><img src="images/t03.png" width="24" height="24" /></span>删除选中客户</li>
			        <div class="txt-fld">
				        <div style="float: right;position: absolute;right: 175px;">
				        	输入客户姓名：<input id="cusName" type="text" style="border: 1px solid #bb9797; height: 24px;" />
				        </div>
			        </div>
	        	<li style="float: right;position: absolute;right: 33px;" onclick="addCus()" ><span><img src="images/t01.png" width="24" height="24" /></span>添加客户</li>
        	</ul>
        	</div>
			<table class="tablelist">
				<thead>
					<tr align="center">
						<th width="29"><input type='checkbox' style="margin-left:-10px;" id='checkall' name='checkall' /></th>
						<th width="262" align="center" bgcolor="#909090">客户名称</th>
						<th width="212" align="center" bgcolor="#909090">操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${uesrClientsList }" var="list">
						<c:if test="${list.cname != '原账户地址' }">
							<tr  class="user_tr">
								<td width="29" height="38" align="center" style="padding-left:0px">
									<input type="checkbox" name="mid" value="${list.cid }" />
					       	  	</td>
								<td width="212" align="center" class="modify">
									<a href="findCusAddress?clientId=${list.cid }&cusName=${list.cname }&UId=${list.uid }">${list.cname }</a>
								</td>
								<td width="212" align="center" class="modify">
									<c:if test="${list.cdefult == null || list.cdefult != 1 }">
										<a href="javascript:;" onclick="setupUserAddessDefult(${list.cid})">设置默认客户</a> /
									</c:if>
									<a href="findCusAddress?clientId=${list.cid }">编辑客户地址</a>
								</td>
							</tr>
						</c:if>
					</c:forEach>
				</tbody>
			</table>
			<table style="display: none;" class="tablelist">
				<thead>
					<tr align="center">
						<th><input type='checkbox' id='checkall' name='checkall' /></th>
						<th width="212" align="center" bgcolor="#909090">收货人姓名</th>
						<th width="262" align="center" bgcolor="#909090">收货地址</th>
						<th width="212" align="center" bgcolor="#909090">邮编</th>
						<th width="212" align="center" bgcolor="#909090">手机号码</th>
						<th width="212" align="center" bgcolor="#909090">固定电话</th>
						<th width="212" align="center" bgcolor="#909090">快递备注</th>
						<th width="212" align="center" bgcolor="#909090">所属客户</th>
						<th width="212" align="center" bgcolor="#909090">操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${userAddressesList }" var="list">
						<tr  class="user_tr">
							<td width="29" height="38" align="center" style="padding-left:0px">
								<input type="checkbox" name="mid" value="${list.AId }" />
				       	  	</td>
							<td width="262" align="center" class="modify">${list.AConsignee }</td>
							<td width="212" align="center" class="modify">${list.address }</td>
							<td width="212" align="center" class="modify">${list.zipcode }</td>
							<td width="212" align="center" class="modify">${list.tel }</td>
							<td width="212" align="center" class="modify">${list.mb }</td>
							<td width="212" align="center" class="modify">${list.adesc }</td>
							<td width="212" align="center" class="modify">${list.cname }</td>
							<td width="212" align="center" class="modify">
								<c:if test="${list.ACustomer == null || list.ACustomer == 1 }">
									<a href="javascript:;" onclick="setupUserAddessDefult(${list.cid})">设置当前客户为下单用户</a>
								</c:if>
								<c:if test="${list.ACustomer == 2 }">
									当前下单客户
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			
			  ${pages.pageStr1}
			  
	</s:form>
	
		<div id="signup" style="width:600px; height: 700px;">
			<div id="signup-ct">
				<div id="signup-header">
					<h2>新建客户地址</h2>
					<a class="modal_close" href="#"></a>
				</div>
				
				<form  id="form"  method="post">
				   <div class="txt-fld">
				    <label for="">客户姓名:</label>
				    <input id="" class="good_input" name="addessUserName" type="text" />
				  </div>
				  <div style="width: 550px; " class="txt-fld">
				    <label for="">请选择区县:</label>
				    	<div style="float: left;">
						    <select id="country" name="country" onchange="showProvince('<%=basePath%>',this.value)">
							    <option value="0">---请选择---</option>
							    <s:iterator value="regionsList" var="list">
							    	<option value="${list.regionId}">${list.regionName}</option>
							    </s:iterator>
						    </select>
						  	<select id="province" name="province" onchange="showCity('<%=basePath%>',this.value)">
						    	<option value="0">---请选择---</option>
						    </select>
						  	<select id="city" name="city" onchange="showDistrict('<%=basePath%>',this.value)">
						    	<option value="0">---请选择---</option>
						    </select>
						    <select id="district" name="district" >
						    	<option value="0">---请选择---</option>
						    </select>
					    </div>
				  </div>
				  <div class="txt-fld">
				    <label for="">详细地址:</label>
				    <input id="" class="good_input" name="address" type="text" />
				  </div>
				  <div class="txt-fld">
				    <label for="">手机号码:</label>
				    <input id="" class="good_input" name="phone" type="text" />
				  </div>
				   <div class="txt-fld" id="imgFile">
				    <label for="">固定电话:</label>
				   	<input id="" class="good_input" name="usermb" type="text" />
				   </div>
				  <div class="txt-fld">
				    <label for="">快递备注:</label>
				    <input id="" class="good_input" name="etcDesc" type="text" />
				  </div>
				  
				   <div class="txt-fld">
				    <div id="tipHtml" style="color:red;float:left">
                    </div>
				  <button type="submit" onclick="return checkThis(this.form,'saveCusAddress',0);">确定</button> 
				  </div>
				 </form>
			</div>
		</div>
		<div id="signup2" style="width:600px; height: 700px;">
			<div id="signup-ct">
				<div id="signup-header">
					<h2>新建客户地址2</h2>
					<a class="modal_close" href="#"></a>
				</div>
				
				<form  id="form"  method="post">
				   <div class="txt-fld">
				    <label for="">客户姓名:</label>
				    <input id="" class="good_input" name="addessUserName" type="text" />
				  </div>
				  <div style="width: 550px; " class="txt-fld">
				    <label for="">请选择区县:</label>
				    	<div style="float: left;">
						    <select id="country" name="country" onchange="showProvince('<%=basePath%>',this.value)">
							    <option value="0">---请选择---</option>
							    <s:iterator value="regionsList" var="list">
							    	<option value="${list.regionId}">${list.regionName}</option>
							    </s:iterator>
						    </select>
						  	<select id="province" name="province" onchange="showCity('<%=basePath%>',this.value)">
						    	<option value="0">---请选择---</option>
						    </select>
						  	<select id="city" name="city" onchange="showDistrict('<%=basePath%>',this.value)">
						    	<option value="0">---请选择---</option>
						    </select>
						    <select id="district" name="district" >
						    	<option value="0">---请选择---</option>
						    </select>
					    </div>
				  </div>
				  <div class="txt-fld">
				    <label for="">详细地址:</label>
				    <input id="" class="good_input" name="address" type="text" />
				  </div>
				  <div class="txt-fld">
				    <label for="">手机号码:</label>
				    <input id="" class="good_input" name="phone" type="text" />
				  </div>
				   <div class="txt-fld" id="imgFile">
				    <label for="">固定电话:</label>
				   	<input id="" class="good_input" name="usermb" type="text" />
				   </div>
				  <div class="txt-fld">
				    <label for="">快递备注:</label>
				    <input id="" class="good_input" name="etcDesc" type="text" />
				  </div>
				  
				   <div class="txt-fld">
				    <div id="tipHtml" style="color:red;float:left">
                    </div>
				  <button type="submit" onclick="return checkThis(this.form,'saveCusAddress',0);">确定</button> 
				  </div>
				 </form>
			</div>
		</div>
		
		
	<div id="signup1" style="width:600px; height: 700px;">
		<div id="signup-ct">
			<div id="signup-header">
				<h2>修改客户</h2>
				<a class="modal_close" href="#"></a>
			</div>
			<form  id="form"  method="post">
				<input type="hidden" id="userCusAddId" name="id"  />
				   <div class="txt-fld">
				    <label for="">客户姓名:</label>
				    <input id="AConsignee" class="good_input" name="addessUserName" type="text" />
				  </div>
				  <div style="width: 550px; " class="txt-fld">
				    <label for="">请选择区县:</label>
				    	<div style="float: left;">
						    <select id="country1" name="country" onchange="showProvince('<%=basePath%>',this.value)">
							    <option value="0">---请选择---</option>
							    <s:iterator value="regionsList" var="list">
							    	<option value="${list.regionId}">${list.regionName}</option>
							    </s:iterator>
						    </select>
						  	<select id="province1" name="province" onchange="showCity('<%=basePath%>',this.value)">
						    	<option value="0">---请选择---</option>
						    </select>
						  	<select id="city1" name="city" onchange="showDistrict('<%=basePath%>',this.value)">
						    	<option value="0">---请选择---</option>
						    </select>
						    <select id="district1" name="district" >
						    	<option value="0">---请选择---</option>
						    </select>
					    </div>
				  </div>
				  <div class="txt-fld">
				    <label for="">详细地址:</label>
				    <input id="address" class="good_input" name="address" type="text" />
				  </div>
				  <div class="txt-fld">
				    <label for="">手机号码:</label>
				    <input id="tel" class="good_input" name="phone" type="text" />
				  </div>
				   <div class="txt-fld" id="imgFile">
				    <label for="">固定电话:</label>
				   	<input id="bm" class="good_input" name="usermb" type="text" />
				   </div>
				  <div class="txt-fld">
				    <label for="">快递备注:</label>
				    <input id="adesc" class="good_input" name="etcDesc" type="text" />
				  </div>
				  
				   <div class="txt-fld">
				    <div id="tipHtml" style="color:red;float:left">
                    </div>
				  <button type="submit" onclick="return checkThis(this.form,'updateCusAddress',1);">确定</button> 
				  </div>
				 </form>
		</div>
	</div>

	<script src="js/jquery.confirm.js"></script>
	<script src="js/script.js"></script>
	
    </div>
    <form action="" method="post" id="formCus">
    	<input type="hidden" id="cusNameForm" name="cusName"  />
    	<input type="hidden" id="uid"  />
    	<input type="hidden" id="cid" name="clientId"  />
    	
    </form>
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	function userOrderListInfo(uid, uname){
		$("#userId").val(uid);
		$("#userName").val(uname);
		$("#userOrder").attr("action","list_order");
		$("#userOrder").submit();
	}
	function findXinyongOrderInfo(uid, uname){
		$("#userId").val(uid);
		$("#userName").val(uname);
		$("#userOrder").attr("action","findXinYongOrderInfo");
		$("#userOrder").submit();
	}
	function findOrderInfoByXianXia(type, uid){
		$("#userId").val(uid);
		$("#userOrder").attr("action","findUserXianCre");
		$("#userOrder").submit();
	}
	function findType(type, uid){
		$("#keyword").val(type);
		$("#userId").val(uid);
		$("#userOrder").attr("action","findXinYongOrderInfo");
		$("#userOrder").submit();
	}
	
	function pageTiaoZhuang(num){
		var keyword = $("#keyword").val();
		if(keyword == "请输入你要查询的关键词" || keyword == ""){
			keyword = "";
		}
		location.href="userCusList?page="+num;
	}
	function setupUserAddessDefult(aid){
		if(aid == null || aid == ""){
			alert("请先设置当前地址的所属客户");
			return;
		}
		if(confirm("确定要设置当前用户地址为下单地址嘛？")){
			location.href="setupUserCusAddess?aid=" + aid;
		}
	}
	
	
	
	 function showProvince(basePath,regionId){
         $.post(basePath+"manage/queryPro_region?regionId="+regionId,function(json){
         var list=json.plist;
         for(var i=0;i<list.length;i++){
         $("#province").append("<option value='"+list[i].regionId+"'>"+list[i].regionName+"</option>");
         }
		},"json");
    }
    
    function showCity(basePath,regionId){
   	 $("#city").empty();
        $("#city").append("<option value='0'>---请选择---</option>");
        $("#district").empty();
        $("#district").append("<option value='0'>---请选择---</option>");
        if(regionId != 0){
       	 if(regionId != 0){
       		 $.post(basePath+"manage/queryPro_region?regionId="+regionId,function(json){
       			 var list=json.plist;
       			 for(var i=0;i<list.length;i++){
       				 $("#city").append("<option value='"+list[i].regionId+"'>"+list[i].regionName+"</option>");
       			 }
       		 },"json");
       	 }
        }
    }
    function showDistrict(basePath,regionId){
   	 $("#district").empty();
        $("#district").append("<option value='0'>---请选择---</option>");
        if(regionId != 0){
       	 $.post(basePath+"manage/queryPro_region?regionId="+regionId,function(json){
       		 var list=json.plist;
       		 if(list.length!=0){
       			 for(var i=0;i<list.length;i++){
       				 $("#district").show();
       				 $("#district").append("<option value='"+list[i].regionId+"'>"+list[i].regionName+"</option>");
       			 }
       		 }else{
       			 $("#district").empty();
       			 $("#district").append("<option value='-1'>---请选择---</option>");
       			 $("#district").hide();
       		 }
       	 },"json");
        }
    }
	
	 function showProvince1(basePath,regionId){
         $.post(basePath+"manage/queryPro_region?regionId="+regionId,function(json){
         var list=json.plist;
         for(var i=0;i<list.length;i++){
         $("#province1").append("<option value='"+list[i].regionId+"'>"+list[i].regionName+"</option>");
         }
		},"json");
    }
    
    function showCity1(basePath,regionId){
   	 $("#city1").empty();
        $("#city1").append("<option value='0'>---请选择---</option>");
        $("#district1").empty();
        $("#district1").append("<option value='0'>---请选择---</option>");
        if(regionId != 0){
       	 if(regionId != 0){
       		 $.post(basePath+"manage/queryPro_region?regionId="+regionId,function(json){
       			 var list=json.plist;
       			 for(var i=0;i<list.length;i++){
       				 $("#city1").append("<option value='"+list[i].regionId+"'>"+list[i].regionName+"</option>");
       			 }
       		 },"json");
       	 }
        }
    }
    function showDistrict1(basePath,regionId){
   	 $("#district1").empty();
        $("#district1").append("<option value='0'>---请选择---</option>");
        if(regionId != 0){
       	 $.post(basePath+"manage/queryPro_region?regionId="+regionId,function(json){
       		 var list=json.plist;
       		 if(list.length!=0){
       			 for(var i=0;i<list.length;i++){
       				 $("#district1").show();
       				 $("#district1").append("<option value='"+list[i].regionId+"'>"+list[i].regionName+"</option>");
       			 }
       		 }else{
       			 $("#district1").empty();
       			 $("#district1").append("<option value='-1'>---请选择---</option>");
       			 $("#district1").hide();
       		 }
       	 },"json");
        }
    }
	
	function addAddressToCus(){
		var selValue = $("#cus").val();
		var checkedNum = $("input[name='mid']:checked").length; 
		if(checkedNum == 0) { 
			  $("p.tipp").html("请至少选择一项");
			  $("cite.tipc").html("");
			  $(".tip").fadeIn(200);
			  return;
		} else{
			if(selValue == "" || selValue == 0){
				alert("请选择客户");
				return;
			}
			$("#form1").attr("action", "updateCusClientAddress");
			$("#clientId").val(selValue);
			$("#form1").submit();
		}
		
	}
	
	function addCus(){
		var cusName = $("#cusName").val();
		if(cusName == null || cusName == ""){
			alert("请输入客户姓名");
			return;
		}
		$.post(
			"findClientName",
			{"cusName":cusName},
			function(data, varstart){
				if(data == "success"){
					$("#formCus").attr("action","saveCusName");
					$("#cusNameForm").val(cusName);
					$("#formCus").submit();
				} else {
					alert("输入的客户姓名重复");
					return;
				}
			}
		);
	}
	
	function delSelCus(){
		var checkedNum = $("input[name='mid']:checked").length; 
		if(checkedNum == 0) { 
			  $("p.tipp").html("请至少选择一项");
			  $("cite.tipc").html("");
			  $(".tip").fadeIn(200);
			  return;
		} else{
			//$("#form1").attr("action", "updateCusClientAddress");
			//$("#form1").submit();
			if(confirm("删除该客户后对应的收货地址也将被删除\n确定执行此操作吗？")){
				$("#form1").attr("action", "deleteCusByUser");
				$("#form1").submit();
			}
		}
		
		
	}
	
	</script>
	

</body>

</html>
