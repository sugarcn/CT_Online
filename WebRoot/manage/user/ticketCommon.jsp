<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>增票管理</title>

<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
        
        <link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript"> 

$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
		</script>
</head>

<body>
			<s:form action="user/ticket_del" method="post">
			<div style="width: 1000px;height: 40px">
				<div style="float: right;">
					<input type="submit" value="删除该增票" style="height: 35px;width: 100px"/>
				</div>
			</div>
			<table border="1" width="960" cellspacing="0" cellpadding="0" bordercolor="#D2CFCF" style="border-collapse: collapse">
				<tr style="font-weight:700;font-size:14px">
					<td width="50" align="center" bgcolor="gray">单位名称:</td>
					<td width="212">${addTicket.companyName}</td>
				</tr>
				<tr style="font-weight:700;font-size:14px">
					<td width="50" align="center" bgcolor="gray">纳税人识别号:</td>
					<td width="212" >${addTicket.companySn}</td>
				</tr>
				<tr style="font-weight:700;font-size:14px">
					<td width="50" align="center" bgcolor="gray">注册地址:</td>
					<td width="212" >${addTicket.registeAddress}</td>
				</tr>
				<tr style="font-weight:700;font-size:14px">
					<td width="50" align="center" bgcolor="gray">注册电话:</td>
					<td width="212" >${addTicket.registeTel}</td>
				</tr>
				<tr style="font-weight:700;font-size:14px">
					<td width="50" align="center" bgcolor="gray">开户银行:</td>
					<td width="212" >${addTicket.depositBank}</td>
				</tr>
				<tr style="font-weight:700;font-size:14px">
					<td width="50" align="center" bgcolor="gray">银行账户:</td>
					<td width="212" >${addTicket.bankAccount}</td>
				</tr>
				<tr style="font-weight:700;font-size:14px">
					<td width="238" align="center" bgcolor="gray">营业执照副本:</td>
					<td width="50" align="left">
						<img src="<%=basePath%>${addTicket.lisence}" style="width: 80px;height: 80px"/>
					</td>
				</tr>
				<tr style="font-weight:700;font-size:14px">
					<td width="50" align="center" bgcolor="gray">税务登记证副本:</td>
					<td width="50" align="left">
						<img src="<%=basePath%>${addTicket.taxCertifi}" style="width: 80px;height: 80px"/>
					</td>
				</tr>
				<tr style="font-weight:700;font-size:14px">
					<td width="50" align="center" bgcolor="gray">一般纳税人资格证书:</td>
					<td width="50" align="left">
						<img src="<%=basePath%>${addTicket.generalCertifi}" style="width: 80px;height: 80px"/>
					</td>
				</tr>
				<tr style="font-weight:700;font-size:14px">
					<td width="50" align="center" bgcolor="gray">银行开户许可证:</td>
					<td width="50" align="left">
						<img src="<%=basePath%>${addTicket.bankLisence}" style="width: 80px;height: 80px"/>
					</td>
				</tr>
		</table>
		</s:form>
</body>
</html>