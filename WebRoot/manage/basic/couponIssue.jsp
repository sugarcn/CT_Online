<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/jquery-ui.js"></script>
    <title>添加优惠劵</title>
    
  <link href="<%=basePath %>manage/css/style.css" rel="stylesheet" type="text/css" />
  <link href="<%=basePath %>js/jquery-ui.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" 
src="<%=basePath%>manage/js/My97DatePicker/WdatePicker.js" 
    charset="gb2312"  defer="defer"></script>
   <script type="text/javascript">
    
    $(document).ready(function(){
      $(".zhekou").hide();
    });
  //自动弹出下拉菜单
function getGoodsByGname(){

    var GType =$("#GType").val();
	var GName = $("#GName").val();
	$(".sbss").html("");
	
	$.post("goods_getGoodsByGname1",{"goodsDTO.GName":GName,"goodsDTO.GType":GType},function(data){
		//data = eval(data);
		var html = "";
		$(".sbss").css("display","block");
		for(var i=0;i<data.length;i++){
			html = html + "<li onclick=\"javascript:setGid("+data[i][0]+",\'"+data[i][1]+"\')\"><span >"+data[i][1]+"</span></li>";
			$(".sbss").html(html);
		}
	});
}
//设置下拉菜单的商品Gid
function setGid(Gid,Gname){
	$("#GId").val(Gid);
	$("#GName").val(Gname);
	
	$(".sbss").css("display","none");
}

function changeDiscount(){
 var CouponType = $("#CouponType").val();
 if(CouponType =="1"){
    $(".zhekou").hide();
   $(".youhui").show();
   $("#info").html(" ");
   $("#Amount").val(" ");
  }else if(CouponType =="2"){
   $(".zhekou").hide();
   $(".youhui").show();
   $("#info").html(" ");
   $("#Amount").val("0");
 }else if(CouponType =="3"){
   $(".zhekou").show();
   $(".youhui").hide();
    $("#info").html("（折扣范围在0和10之间）");
 }
}
    function checkAdd(){
    if($("#CouponName").val()==""){
            alert("优惠券名称不得为空！");
            return false;
    }
    if($("#Discount").val()==""){
            alert("优惠金额不得为空！");
            return false;
    }else if($("#CouponType").val()=="3"){
              if($("#Discount").val()==""){
              alert("折扣不得为空！");
               return false;
              }else if($("#Discount").val() <=0 || $("#Discount").val()>=10){
                alert("折扣范围应在0到10之间！");
               return false;
              }
    }
    
    if($("#Amount").val()==""){
            alert("所需消费金额不得为空！");
            return false;
    }
    if($("#STime").val()==""){
            alert("起始时间不得为空！");
            return false;
    }
    if($("#ETime").val()==""){
            alert("截止时间不得为空！");
            return false;
    }
    
     $("#addcoupon").submit();
     alert("优惠券添加成功！");
    }
    function showAddUser(){
    	$("#dangqian").show();
    	$("#tiajjia").show();
    	$("#fabubtn").attr("onclick","fabuyouhui()");
    	$("#fabubtn").attr("disabled",false);
    }
    function hideAddUser(){
    	$("#dangqian").hide();
    	$("#tiajjia").hide();
    	$("#fabubtn").attr("onclick","fabuquanyouhui()");
    	$("#fabubtn").attr("disabled",false);
    }
    function fabuquanyouhui(){
    	if(confirm("确定要给所有用户发布此优惠券嘛？")){
    		$("#message").show();
	    	$.post(
	    		"manage/fabuyouhuiquan",
	    		{"id":$("#couid").val()},
	    		function(data,varstart){
	    			alert(data);
	    			location.reload();
	    		}
	    	);
    	}
    }
    function fabuyouhui(){
    	if($("#addsuserList").val() == "" || $("#addsuserList").val() == null){
    		alert("您还没有添加会员呢");
    		return;
    	}
    	if(confirm("确定要给当前选中的用户发布此优惠券嘛？")){
    		$("#message").show();
	    	$.post(
	    		"manage/fabuyouhui",
	    		{"id":$("#couid").val(),"keyword":$("#addsuserList").val()},
	    		function(data,varstart){
	    			alert(data);
	    			location.reload();
	    		}
	    	);
    	}
    }
    $(function(){
	    $("#findUser").autocomplete({
	    	source:function(request,response){
	    		$.post("manage/findUserNameByPhoneAndName",{"keyword":$("#findUser").val()},function(data){
	      			response(data);
	      			//remandadd();
	      		});
	    	}
	    });
	    $("#addUser").click(function(){
	    	var $findUser = $("#findUser").val();
	    	if($findUser != "" && $findUser != null){
	    		var $addsuserList = $("#addsuserList").val();
	    		$findUser = $findUser.trim();
	    		$findUser += "  ";
	    		$addsuserList += $findUser;
	    		$("#addsuserList").val($addsuserList);
	    		$("#findUser").val("");
	    	} else {
	    		alert("您还没有添加会员呢");
	    	}
	    });
    });
    </script>
  </head>
  
  <body>
     <input type="hidden" id="couid" value="${couponId }" />
	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">优惠券管理</a></li>
    <li><a href="#">发行优惠券优惠券</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    <div class="formtitle"><span>发行优惠券</span></div>
    <form action="manage/add_coupon" id="addcoupon" name="addcoupon" method="post">
		<input type="hidden" id="CouponId" name="couponId" value="" />
    <ul class="forminfo1">
    	<li><label>总发行数量：<i>${allCount + weiYongCount }</i></label></li>
    	<li><label>已发行数量：<i>${allCount }</i></label></li>
    	<li><label>未发行数量：<i>${weiYongCount }</i></label></li>
    	<li><label>发行类型：
	    		<input type="radio" name="typeCoupon"
	    			<c:if test="${userCount > weiYongCount }">
	    			 	disabled="disabled"
	    			</c:if>
	    			<c:if test="${userCount <= weiYongCount }">
	    			 	onclick="hideAddUser()"
	    			</c:if>
	    		 class="input" /> <b>全部会员</b>
	    		<input type="radio" name="typeCoupon"
	    			<c:if test="${weiYongCount == 0 }">
	    				disabled="disabled"
	    			</c:if>
	    			<c:if test="${weiYongCount != 0 }">
	    				onclick="showAddUser()"
	    			</c:if>
	    		 class="input" /> <b>指定会员</b>
	    		<i>当前会员总数： ${userCount }</i>
    		</label>
    	</li>
    	<li style="display: none;" id="dangqian">
    		<lable>当前添加会员： &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" disabled="disabled" class="dfinput" id="addsuserList" style="width: 90%;" /></lable>
    	</li>
    	<li style="display: none;" id="tiajjia"><label>输入会员名称进行查找会员：<input type="text" id="findUser" class="dfinput" /><input type="button" id="addUser" class="btn" value="添加会员"/></label></li>
    	<li id="message" style="display: none;"><label><i>正在发布优惠券，请稍后。。。。。。。</i></label></li>
    	<li><input type="button" id="fabubtn" onclick="" disabled="disabled" class="btn" value="发行优惠券"/></li>
    <div style="clear:both"></div>
	</ul>
    </form>
    </div>

  </body>
</html>
