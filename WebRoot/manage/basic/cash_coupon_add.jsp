<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <script type="text/javascript" src="<%=basePath %>manage/js/jquery.js"></script>
    <title>添加现金劵</title>
    
  <link href="<%=basePath %>manage/css/style.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" 
src="<%=basePath%>manage/js/My97DatePicker/WdatePicker.js" 
    charset="gb2312"  defer="defer"></script>
   <script type="text/javascript">
    
    $(document).ready(function(){
      $(".zhekou").hide();
    });
  //自动弹出下拉菜单
function getGoodsByGname(){

    var GType =$("#GType").val();
	var GName = $("#GName").val();
	$(".sbss").html("");
	
	$.post("goods_getGoodsByGname1",{"goodsDTO.GName":GName,"goodsDTO.GType":GType},function(data){
		//data = eval(data);
		var html = "";
		$(".sbss").css("display","block");
		for(var i=0;i<data.length;i++){
			html = html + "<li onclick=\"javascript:setGid("+data[i][0]+",\'"+data[i][1]+"\')\"><span >"+data[i][1]+"</span></li>";
			$(".sbss").html(html);
		}
	});
}
//设置下拉菜单的商品Gid
function setGid(Gid,Gname){
	$("#GId").val(Gid);
	$("#GName").val(Gname);
	$(".sbss").css("display","none");
}

function changeDiscount(){
 var CouponType = $("#CouponType").val();
 if(CouponType =="1"){
   $(".youhui").text();
   $("#info").html(" ");
   $("#amount").val("");
   $("#amount").attr("readonly",false);
   $("#amount").attr("disabled",false);
  }else if(CouponType =="2"){
   $(".zhekou").hide();
   $(".youhui").show();
   $("#info").html(" ");
   $("#amount").val("0");
   $("#amount").attr("readonly",true);
   $("#amount").attr("disabled",true);
   
 }
}
    function checkAdd(){
    if($("#CouponName").val()==""){
            alert("优惠券名称不得为空！");
            return false;
    }
    if($("#Discount").val()==""){
            alert("优惠金额不得为空！");
            return false;
    }
    
    if($("#amount").val()==""){
           alert("所需消费金额");
            return false;
    }
    if($("#GName").val() == ""){
    	alert("请输入订单编号");
    	return false;
    }
    if(confirm("确定要给此订单发布优惠券？")){
	     $("#addcoupon").submit();
	     alert("优惠券添加成功！");
    }
    }
    function isOrderSn(v){
    	if(v.value != ""){
	    	$.post(
	    		"getOrderByOrderSn",
	    		{"orderSn":v.value},
	    		function(data,varstart){
	    			if(data == "请确认输入的订单"){
	    				alert(data);
	    				v.value="";
	    			} else {
	    				$("#infoOrderSn").html("");
	    			}
	    		}
	    	);
    	} else {
    		$("#infoOrderSn").html("请输入订单编号");
    	}
    }
    </script>
  </head>
  
  <body>
     
	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="/manage/list_coupon">现金劵管理</a></li>
    <li><a href="#">新增现金劵</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    <div class="formtitle"><span>基本信息</span></div>
    <form action="manage/add_cash_coupon" id="addcoupon" name="addcoupon" method="post">
		<input type="hidden" id="CouponId" name="couponId" value="" />
    <ul class="forminfo">
    <li><label>现金劵名称：</label><input id="CouponName" name="coupon.couponName" type="text" class="dfinput" /></li>
    <li class="infoer"><label>类型：</label><select id="CouponType" name="coupon.couponType" onchange="changeDiscount()">
						<option value="2">无限制</option>
						<option value="1">满减</option>
					</select></li>
    <li><label class="suoxu">所需消费金额：</label><input id="amount" disabled="disabled" readonly="readonly" name="coupon.amount" value="0" type="text" class="dfinput" /><span id="info"></span></li>
    <li><label class="youhui">优惠金额：</label><input id="Discount" name="coupon.discount" type="text" class="dfinput" /><span id="infodis"></span></li>
    <li class="infoer"><label>现金劵有效期：</label>
    	<select id="cashtime" name="coupon.stime">
    		<option value="15">15分钟</option>
    		<option value="20">20分钟</option>
    		<option value="30">30分钟</option>
    		<option value="60">60分钟</option>
    	</select>
    </li>
    <li><label>订单编号：</label><input id="GName" name="coupon.GName" onblur="isOrderSn(this)" type="text" class="dfinput" /><span id="infoOrderSn"></li>
    <li><label>&nbsp;</label><input id="subtn" onclick="checkAdd()" type="button" class="btn" value="新增"/></li>
    				<div style="clear:both"></div>
			</ul>
    </ul>
    
    </form>
    </div>

  </body>
</html>
