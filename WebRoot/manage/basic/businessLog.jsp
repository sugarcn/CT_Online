<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>公告管理</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="../js/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.all.js"></script>
<script type="text/javascript" charset="utf-8" src="../js/ueditor/lang/zh-cn/zh-cn.js"></script>
<link type="text/css" src="../js/ueditor/themes/default/css/ueditor.css"/>
<script type="text/javascript" src="../js/ajaxfileupload.js" charset="utf-8"></script>
<script type="text/javascript" src="js/notice.js" charset="utf-8"></script>


</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">系统管理</a></li>
    <li><a href="list_notice">业务日志</a></li>
    </ul>
    </div>
    
    
    <form action="business_log" id="form1" method="post">
		 <input type="hidden" id="deleteA" value="delete_notice">
		 <input type="hidden" id="findA" value="find_notice">
		 <input type="hidden" id="searchA" value="business_log">
		 <input type="hidden" id="updateA" value="update_notice"> 
		 
		    <div class="rightinfo">
		        
		    <div class="tools">
		    <ul class="toolbar">
			    <li onclick="findUser(2)"><span></span>全部</li>
			    
			    <li onclick="findUser(0)"><span></span>管理员</li>
		
				<li onclick="findUser(1)"><span></span>用户</li>
			</ul>
			<div class="ssk">
				   <div class="ssbk">
				      <div style="float:left;padding-left:30px;padding-top:1px"><input type="text" id="keyword" name="keyword" class="wbdd" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="${keyword}"></div>
				      <div style="float:right"><img src="images/ss.jpg" onclick="findKey()"></div>
				      <div class="clear"></div>
				   </div>
		  </div>
		    <input type="hidden" name="logType" id="logType" value="${logType }" />
		    </div>
		    
		    <table class="tablelist">
		    	<thead>
		    	<tr align="center">
		        <th>用户名</th>
		        <th>用户类型</th>
		        <th>操作模块</th>
		        <th>操作详细数据（JOSN）</th>
		        <th>操作状态</th>
		        <th>ip</th>
		        <th>操作时间</th>
		        </tr>
		        </thead>
		        <tbody>
			        <c:forEach items="${loginOperationsList }" var="list">
				        <tr align="center">
				        	<td width="400" align="center" class="modify">${list.adminName } ${list.userName }</td>
				        	<td width="400" align="center" class="modify">
				        		<c:if test="${list.logType == 1 }">
				        			用户
				        		</c:if>
				        		<c:if test="${list.logType == 0 }">
				        			管理员
				        		</c:if>
				        	</td>
				        	<td width="400" align="center" class="modify">${list.logModule }</td>
				        	<td width="400" align="center" class="modify">${list.logState }</td>
				        	<td width="400" height="100" align="center" class="modify">
				        		<a id="show" onclick="lookJson(this)">点击查看json数据</a>
					        	<textarea id="jsonDate" style="display: none;" rows="7" cols="65">
						        	${list.logDesc }
					        	</textarea>
				        		<a id="hide" style="display: none;" onclick="hideJson(this)">收起</a>
					        	
				        	</td>
				        	<td width="400" align="center" class="modify">${list.logIp }</td>
				        	<td width="400" align="center" class="modify">${list.logTime }</td>
				        </tr> 
			        </c:forEach>
		        </tbody>
		    </table>
		   <input type="hidden" id="page" name="page" />
		     ${pages.pageStr1}
		     
		      </div>
    	
    </form>
</div>
	
	
	
    
    
    
    
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
		function lookJson(yuan){
			$(yuan).next("#jsonDate").show();
			$(yuan).siblings("#hide").show();
			$(yuan).hide();
		}
		function hideJson(yuan){
			$(yuan).prev("#jsonDate").hide();
			$(yuan).siblings("#show").show();
			$(yuan).hide();
		}
		function pageTiaoZhuang(num){
			$("#page").val(num);
			$("#form1").submit();
		}
		function findKey(){
			var checkedNum = $("#keyword").attr("value"); 
			if(checkedNum == "请输入你要查询的关键词") { 
				  $("p.tipp").html("请输入你要查询的关键词");
				  $("cite.tipc").html("");
				  $(".tip").fadeIn(200);
			}else{
				var params={
					"keyword":checkedNum
				}
				$("#form1").submit();
			}
		}
		function findUser(num){
			$("#logType").val(num);
			$("#form1").submit();
		}
	</script>
	
	
	

</body>
</html>
