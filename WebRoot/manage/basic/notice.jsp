<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>公告管理</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="../js/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.all.js"></script>
<script type="text/javascript" charset="utf-8" src="../js/ueditor/lang/zh-cn/zh-cn.js"></script>
<link type="text/css" src="../js/ueditor/themes/default/css/ueditor.css"/>
<script type="text/javascript" src="../js/ajaxfileupload.js" charset="utf-8"></script>
<script type="text/javascript" src="js/notice.js" charset="utf-8"></script>


</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">系统管理</a></li>
    <li><a href="list_notice">公告管理</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" id="form1" theme="simple">   
 
 <input type="hidden" id="deleteA" value="delete_notice">
 <input type="hidden" id="findA" value="find_notice">
 <input type="hidden" id="searchA" value="search_notice">
 <input type="hidden" id="updateA" value="update_notice"> 
 
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
	        <li rel="leanModal" name="signup" href="#signup"><span><img src="images/t01.png" width="24" height="24" /></span>添加</li>
	
			<li rel="leanModal1" class="modiItem" href="#signup1"><span><img src="images/t02.png" width="24" height="24" /></span>修改</li>
	
	        <li class="deleteItem" ><span><img src="images/t03.png" /></span>删除</li>
	        
	        
        </ul>

	<div class="ssk">
     
		   <div class="ssbk">
		      <div style="float:left;padding-left:30px;padding-top:1px"><input type="text" id="keyword" name="keyword" class="wbdd" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="${keyword}"></div>
		      <div style="float:right"><img src="images/ss.jpg" class="searchItem"></div>
		      <div class="clear"></div>
		   </div>
   
  </div>
    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th><input type='checkbox' id='checkall' name='checkall' /></th>
        <th>公告标题</th>
        <th>链接地址</th>
        <th>公告类型</th>
        </tr>
        </thead>
        <tbody>
  <s:iterator value="notices" var="list">      
        <tr align="center">
			<td width="29" height="38" style="padding-left:0px">
			<input type="checkbox" name="mid" value=${list.noId } />
       	  </td>
        	<td width="400" align="center" class="modify"><s:property value="#list.noTitle"/></td>
        	<td width="400" align="center" class="modify"><s:property value="#list.noUrl"/></td>
        	<c:forEach items="${noticeTypesList }" var="listNoType">
        		<c:if test="${listNoType.noTypeId == list.noTypeId }">
		        	<td width="400" align="center" class="modify">${listNoType.noTypeName }</td>
        		</c:if>
        	</c:forEach>
        	
        </tr> 
</s:iterator>       
        </tbody>
    </table>
   
     ${pages.pageStr}
     
   </s:form> 
    
	
	<div id="signup" style="width:800px; height: 700px;">
			<div id="signup-ct">
				<div id="signup-header">
					<h2>新建公告</h2>
					<a class="modal_close" href="#"></a>
				</div>
				
				<form  id="form"  method="post"  enctype="multipart/form-data">
				   <div class="txt-fld">
				    <label for="">公告标题:</label>
				    <input id="" class="good_input" name="noTitle" type="text" />
				  </div>
				  <div class="txt-fld">
				    <label for="">链接地址:</label>
				    <input id="" class="good_input" name="noUrl" type="text" />
				  </div>
				  <div class="txt-fld">
				    <label for="">公告类型:</label>
				    <ul class="formbody">
				    	<li>
						    <select id="noticeType" onchange="sel();" style="width:230px;">
						    	<c:forEach items="${noticeTypesList }" var="list" varStatus="varstart">
						    		<c:if test="${varstart.index == 0 }">
								    	<option selected="selected" value="${list.noTypeId }">${list.noTypeName }</option>
						    		</c:if>
						    		<c:if test="${varstart.index != 0 }">
								    	<option value="${list.noTypeId }">${list.noTypeName }</option>
						    		</c:if>
						    	</c:forEach>
						    </select>
				    	</li>
				    </ul>
				    <input id="typeId" name="typeId" type="hidden" />
				  </div>
				   <div class="txt-fld" id="imgFile" style="display: none;">
				    <label for="">公告主图:</label>
				   	<input type="file"  onchange="updateNoFile()" id="file" name="file" />
				   	<input id="hidImgFile" name="imgUrl" type="hidden" />
				   </div>
				  <div class="ctn-fld">
				    <label for="">公告内容:</label>
				     <script  type="text/plain" id="edit" name="noContent" style="width:760px;padding-top:50px"></script>	   
				   <script type="text/javascript">
				   			var ue=UE.getEditor("edit");
				   </script>				
				  </div>
				   <div class="txt-fld">
				  <button type="submit" onclick="return checkThis(this.form,'add_notice',0);">确定</button> 
				  </div>
				 </form>
			</div>
		</div>
		
		<div id="signup1" style="width:800px; height: 700px;">
			<div id="signup-ct">
            <div id="signup-header">
					<h2>修改公告</h2>
					<a class="modal_close" href="#"></a>
			  </div>
              <form method="post">
              <div id="signupmodi">
                </div>
                    
              <div class="btn-fld">
              <div id="tipHtmlModi" style="color:red;float:left">
                </div>
				  <button type="submit" onclick="return checkThis(this.form,'update_notice',1);">确定</button>
</div>
			  </form>
      </div>
</div>
	
	
	
    
    
    
    
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	
	
	

</body>
	<script type="text/javascript">
	//实例化编辑器
		editor = UE.ui.Editor({
			maxFrameHeight:164
		});
		editor.render('container');
		editor.render('container1');
		
	</script>
	<script>
//编辑器资源文件根路径 最好在ueditor.config.js中配置
window.UEDITOR_HOME_URL = "/ueditor/";
//建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
var ue = UE.getEditor('editor',{initialFrameHeight: 500,initialFrameWidth:800,maximumWords:3000,elementPathEnabled:false});
//复写UEDITOR的getActionUrl 方法,定义自己的Action
UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;
UE.Editor.prototype.getActionUrl = function(action) {
    if (action == 'uploadimage' || action == 'uploadfile') {
        var id = $('#carInfoId').val();
    	   return '/CT_Online/manage/uploadimage';
    } else {
        return this._bkGetActionUrl.call(this, action);
    }
};
// 复写UEDITOR的getContentLength方法 解决富文本编辑器中一张图片或者一个文件只能算一个字符的问题,可跟数据库字符的长度配合使用
UE.Editor.prototype._bkGetContentLength = UE.Editor.prototype.getContentLength;
UE.Editor.prototype.getContentLength = function(){
	return this.getContent().length;
}
</script>
</html>
