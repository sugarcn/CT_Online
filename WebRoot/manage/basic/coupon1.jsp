<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'coupon.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <table border="1" width="960" cellspacing="0" cellpadding="0" bordercolor="#D2CFCF" style="border-collapse: collapse">
	<tr style="font-weight:700;color:#fff;font-size:14px">
		<td width="40" align="center" bgcolor="#909090">劵组ID</td>
		<td width="100" align="center" bgcolor="#909090">优惠劵名称</td>
		<td width="100" align="center" bgcolor="#909090">类型</td>
		<td width="100" align="center" bgcolor="#909090">所需消费金额</td>
		<td width="100" align="center" bgcolor="#909090">折扣</td>
		<td width="100" align="center" bgcolor="#909090">起始时间</td>
		<td width="100" align="center" bgcolor="#909090">截止时间</td>
		<td width="100" align="center" bgcolor="#909090">商品ID</td>
		<td width="100" align="center" bgcolor="#909090">商品类型标记</td>
		<td width="100" align="center" bgcolor="#909090">发行量</td>	
		<td width="100" align="center" bgcolor="#909090">操作</td>			
	</tr>
   <s:iterator value="querylist" var="list"> 
	<tr >
		<td width="40" align="center" class="modify"><s:property value="#list.CouponId"/></td>
		<td width="100" align="center" class="modify"><s:property value="#list.CouponName"/></td>
  		<td width="100" align="center" class="modify"><s:property value="#list.CouponType"/></td>
  		<td width="100" align="center" class="modify"><s:property value="#list.Amount"/></td>
  		<td width="100" align="center" class="modify"><s:property value="#list.Discount"/></td>  		
  		<td width="100" align="center" class="modify"><s:property value="#list.Stime"/></td>
  		<td width="100" align="center" class="modify"><s:property value="#list.Etime"/></td>
  		<td width="100" align="center" class="modify"><s:property value="#list.GId"/></td>
  		<td width="100" align="center" class="modify"><s:property value="#list.GType"/></td>
  		<td width="100" align="center" class="modify"><s:property value="#list.ReleaseNum"/></td>  	
  		<td width="80" align="center" class="modify"><a id="del" rel="leanModal"  href='manage/delete_coupon?id=<s:property value="#list.CouponId"/>'>   删除         </a><a id="detail" rel="leanModal"  href='manage/list_coupondetail?id=<s:property value="#list.CouponId"/>'>优惠券明细</a></td>  		
	</tr>
	</s:iterator>
	</table>
	
  <a id="go" rel="leanModal" name="signup" href="<%=request.getContextPath() %>/manage/basic/coupon_add.jsp">添加优惠劵</a>
  
  <a id="aaa" rel="leanModal" name="signup" href="<%=request.getContextPath() %>/manage/list_coupondetail?id=0">优惠券明细总表</a>
 </body>
</html>
