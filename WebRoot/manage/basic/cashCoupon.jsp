<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/userRank.js" charset="utf-8"></script>
</head>
  
  <body>
  	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">营销中心</a></li>
    <li><a href="#">现金劵管理</a></li>
    </ul>
    </div>
 
 <input type="hidden" id="deleteA" value="delete_coupon">
 <input type="hidden" id="searchA" value="search_coupon">
 
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li class="modiItem1" ><span><img src="images/t01.png" width="24" height="24" /></span><a href="<%=request.getContextPath() %>/manage/basic/cash_coupon_add.jsp">添加现金劵</a></li>
        </ul>

	<div class="ssk">
     
		   <div class="ssbk">
		      <div style="float:left;padding-left:30px;padding-top:1px"><input type="text" id="keyword" name="keyword" class="wbdd" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="${keyword}"></div>
		      <div style="float:right"><img src="images/ss.jpg" class="searchItem"></div>
		      <div class="clear"></div>
		   </div>
  </div>
    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
		<th>现金劵名称</th>
		<th>现金劵类型</th>
		<th>关联订单号</th>
		<th>消费金额</th>
		<th>优惠金额</th>
		<th>起始时间</th>
		<th>结束时间</th>
		<th>使用时间</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${cashCouponList }" var="list">
	        <tr>
				<td width="212" align="center" class="modify">${list.cashName }</td>
		  		<td width="212" align="center" class="modify">
		  			<c:if test="${list.cashType == 1 }">
		  				满减现金劵
		  			</c:if>
		  			<c:if test="${list.cashType == 2 }">
		  				无限制现金劵
		  			</c:if>
		  		</td>
		  		<td width="212" align="center" class="modify">${list.orderSn }</td>
		  		<td width="212" align="center" class="modify">${list.amount }</td>  		
		  		<td width="212" align="center" class="modify">${list.discount }</td>
		  		<td width="212" align="center" class="modify">${list.stime }</td>
		  		<td width="212" align="center" class="modify">${list.etime }</td>
		  		<td width="212" align="center" class="modify">${list.useTime }</td>
	        </tr> 
        </c:forEach>
        </tbody>
    </table>
    
     ${pages.pageStr}
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	
	

  </body>
</html>
