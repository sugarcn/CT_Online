<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="<%=basePath %>manage/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=basePath %>manage/js/jquery.js"></script>
<script type="text/javascript" src="<%=basePath %>manage/js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="<%=basePath %>manage/js/style.css" />
		<script type="text/javascript" src="<%=basePath %>manage/js/jquery.min.js"></script>
		<script type="text/javascript" src="<%=basePath %>manage/js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=basePath %>manage/js/styles.css" />
        <link rel="stylesheet" type="text/css" href="<%=basePath %>manage/js/jquery.confirm.css" />
		<link rel="stylesheet" href="<%=basePath %>manage/css/mycss.css" type="text/css"/>
<script type="text/javascript" src="<%=basePath %>manage/js/ct.js" charset="utf-8"></script>
<script type="text/javascript">
$("#pagego").click(function(){
			  var page = $("#getpage").val();
			  var total = $("#totalpage").val();
			  var couponId = "${couponId}"; 
			  
			  if(page<1||page>total){
				  $("p.tipp").html("输入的页码超出范围");
				  $("cite.tipc").html("");
				  $(".tip").fadeIn(200);
			  }else{
				 var couponId = "${couponId}"; 
				  if(checkedNum == "请输入你要查询的关键词") { 
					  window.location.href="?page="+page;
				  }else{
					  window.location.href="?page="+page+"&couponId="+couponId;
				  }
					  
			  }
			  
			});
</script>
</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">优惠券管理</a></li>
    <li><a href="#">优惠券明细</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" id="form1" theme="simple">   
 
    <div class="rightinfo">
    <div class="tools">
    	<ul class="toolbar">
     <li><a id="go" rel="leanModal" name="signup" href="javascript:history.go(-1);" ><span><img src="<%=basePath%>manage/images/t01.png" width="24" height="24" /></span>返回</a></li>   
    </ul>
    </div>
    <input type="hidden" name="couponId" id="couponId" value="${couponId}" />
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>ID</th>
        <th>劵组ID</th>
        <th>优惠劵号</th>
        <th>用户ID</th>
        <th>使用时间</th>
        <th>订单号</th>
        <th>优惠价格</th>
        <th>发放人ID</th>
        </tr>
        </thead>
        <tbody>
  <s:iterator value="coupondetailList" var="list">    
        <tr>
       	  <td width="228" align="center" class="modify"><s:property value="#list.CDetailId"/></td>
        	<td width="228" align="center" class="modify"><s:property value="#list.ctCoupon.couponId"/></td>
        	<td width="228" align="center" class="modify"><s:property value="#list.couponNumber"/></td>
        	<td width="228" align="center" class="modify"><s:property value="#list.UId"/></td>
        	<td width="228" align="center" class="modify"><s:property value="#list.useTime"/></td>
        	<td width="228" align="center" class="modify"><s:property value="#list.orderId"/></td>
        	<td width="228" align="center" class="modify"><s:property value="#list.prePrice"/></td>
        	<td width="228" align="center" class="modify">
        	<s:property value="#list.releaseUId"/>
        	</td>
        </tr> 
        
	
</s:iterator>       
        </tbody>
    </table>
    
     ${pages.pageStr1}
     
   </s:form> 
    
	
		
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
		function pageTiaoZhuang(num){
			var url = location.href;
			var index = url.indexOf("&");
			if(index != -1){
				var chai = url.split("&");
				url = chai[0]+"&page="+num;
			} else {
				url += "&page="+num;
			}
			location.href=url;
		}
	</script>
	
	

</body>

</html>
