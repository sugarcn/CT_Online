<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>My JSP 'regList.jsp' starting page</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/userRank.js" charset="utf-8"></script>

  </head>
  
  <body>
    
	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="javascript:;">营销中心</a></li>
    <li><a href="#">样品申请列表</a></li>
    </ul>
    </div>
 
 <input type="hidden" id="deleteA" value="delete_coupon">
 <input type="hidden" id="searchA" value="search_coupon">
 
    <div class="rightinfo">
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
			<th>客户姓名</th>
			<th>客户电话</th>
			<th>公司名称</th>
			<th>公司类型</th>
			<th>申请样品名称</th>
			<th>申请时间</th>
			<th>是否回复</th>
			<th>操作</th>
			
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${freeSampleRegList }" var="list">
	        <tr>
				<td width="212" align="center" class="modify">${list.samUsername }</td>
		  		<td width="212" align="center" class="modify">${list.samUserphone }</td>
		  		<td width="212" align="center" class="modify">${list.samCompanyname }</td>
		  		<td width="212" align="center" class="modify">${list.samCompanytype }</td>  		
		  		<td width="212" align="center" class="modify">${list.samGoodsName }</td>
		  		<td width="212" align="center" class="modify">${list.samTime }</td>
		  		<td width="212" align="center" class="modify">
		  			<c:if test="${list.samReply == 0 || list.samReply == null }">
		  				未回复
		  			</c:if>
		  			<c:if test="${list.samReply == 1 && list.samReply != null }">
		  				已回复
		  			</c:if>
		  		</td>
		  		<td width="212" align="center" class="modify">
			  		<c:if test="${list.samReply == 0 || list.samReply == null }">
				  		<a href="javascript:;" onclick="regInfo(${list.samId });">确认回复</a>
			  		</c:if>
		  		</td>
	        </tr> 
        </c:forEach>
        </tbody>
    </table>
    
     ${pages.pageStr}
     
    
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	function regInfo(samId){
		if(confirm("确定已经回复")){
			$.post(
					"manage/regListHui",
					{"freeSampleReg.samId":samId},
					function(data, varstart){
						location.href="sam_regList";
					}
			);
		}
	}
	</script>
	
	

  </body>
</html>
