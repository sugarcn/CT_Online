<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
        <script type="text/javascript"
	src="<%=basePath%>manage/js/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css"
	href="<%=basePath%>manage/js/jquery.fancybox.css?v=2.1.5"
	media="screen" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/ad.js" charset="utf-8"></script>
<script type="text/javascript" src="../js/ajaxfileupload.js" charset="utf-8"></script>
<script type="text/javascript">
		$(document).ready(function() {
			$('.fancybox').fancybox();
		});
	function showBigImg(imgpath) {
		$("img").attr("src", imgpath);
		$("#signup").show();
	}
</script>
<style type="text/css">
	.divIndexShow{float:left;line-height:40px;margin-right:50px;margin-left:30px;}
</style>
</head>


<body>
	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">系统管理</a></li>
    <li><a href="#">广告类型管理</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" id="form1" theme="simple">   
 
 <input type="hidden" id="deleteA" value="delete_ad">
 <input type="hidden" id="findA" value="find_ad">
 <input type="hidden" id="searchA" value="search_ad">
 <input type="hidden" id="updateA" value="update_ad"> 
 
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li rel="leanModal" name="signup" href="#signup"><span><img src="images/t01.png" width="24" height="24" /></span>添加</li>

		<li rel="leanModal1" class="modiItem" href="#signup1"><span><img src="images/t02.png" width="24" height="24" /></span>修改</li>

        <li class="deleteItem" ><span><img src="images/t03.png" /></span>删除</li>
        </ul>

    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th><input type='checkbox' id='checkall' name='checkall' /></th>
        <th>广告标题</th>
        <th>链接地址</th>
        <th>广告说明</th>
        <th>公告类型</th>
        <th>排序</th>
        <th>是否首页显示</th>
        
        </tr>
        </thead>
        <tbody>
  <s:iterator value="ads" var="list">      
        <tr>
			<td width="29" height="38" style="padding-left:10px">
			<input type="checkbox" name="mid" value=${list.adId } />
       	  </td>
        	<td width="212" align="center" class="modify"><s:property value="#list.adTitle"/></td>
        	<td width="212" align="center" class="modify"><s:property value="#list.adUrl"/></td>
        	<td width="212" align="center" class="modify"><s:property value="#list.adDesc"/></td>
        	
        	<td width="212" align="center" class="modify">
        		<c:forEach items="${adTypeList }" var="listType">
        			<c:if test="${listType.adTypeId == list.adType}">
        				${listType.adTypeName }
        			</c:if>
        		</c:forEach>
        	</td>
        	<td width="212" align="center" class="modify"><s:property value="#list.adSort"/></td>
        	<td width="212" align="center" class="modify"><s:property value="#list.adIsUp"/></td>
        	
        </tr> 
</s:iterator>       
        </tbody>
    </table>
   </s:form> 
   
    ${pages.pageStr}
    
	<div id="signup">
			<div id="signup-ct">
				<div id="signup-header">
					<h2>新建首页轮播</h2>
					<a class="modal_close" href="#"></a>
				</div>
				
				<s:form action="" id="form" method="post" enctype="multipart/form-data">
				  <div class="txt-fld">
				    <label for="">广告标题:</label>
				    <input id="" class="good_input" name="adTitle" type="text" />
				  </div>
				  <div class="txt-fld">
				    <label for="">链接地址:</label>
				    <input id="" class="good_input" name="adUrl" type="text" />
				  </div>
				  <div class="txt-fld">
				    <label for="">广告说明:</label>
				    <input id="" class="good_input" name="adDesc" type="text" />
				  </div>
				  <div class="txt-fld">
				    <label for="">图像地址:</label>
				    <input id="fileIndexImg" value="1" class="good_input" onchange="indexUp()" name="fileIndexImg" type="file" />
				    <input type="hidden" id="imgUrl" name="adImg" />
				  </div>
				  <div class="txt-fld">
				    <label for="">广告类型:</label>
				    <select style="position:absolute;left:126px;top:15px;width:150px;height:20px;" name="adType" id="adType">
				    	<c:forEach items="${adTypeList }" var="list">
					    	<option value="${list.adTypeId }">${list.adTypeName }</option>
				    	</c:forEach>
				    </select>
				  </div>
				  <div class="txt-fld">
				    <label for="">排序:</label>
				    <input id="" class="good_input" name="adSort" type="text" />
				  </div>
				  <div class="txt-fld">
				    <label for="">首页显示:</label>
				    <div class="divIndexShow">
					    <input style="height:15px; width: 13px;" type="radio" checked="checked" value="1" name="adIsUp"/> 是
				    </div>
				    <div class="divIndexShow">
					    <input style="height:15px; width: 13px;" type="radio" value="0" name="adIsUp"/> 否
				    </div>
				  </div>
				  
				  <div class="btn-fld">
	                  <div id="tipHtml" style="color:red;float:left">
	                    </div>
					  <button type="submit" onclick="return checkThis(this.form,'addAdInfo',0);">确定</button>
				  </div>
				 </s:form>
			</div>
		</div>
		
		<div id="signup1" class="ad_xiugai">
			<div id="signup-ct">
            <div id="signup-header">
					<h2>修改广告</h2>
					<a class="modal_close" href="#"></a>
				</div>
              <s:form id="form" method="post" enctype="multipart/form-data">
              <div id="signupmodi">
                    </div>
                    
              <div class="btn-fld">
              <div id="tipHtmlModi" style="color:red;float:left">
                    </div>
				  <!-- <button type="submit" onclick="return checkThis(this.form,'update_ad',1);">确定</button> -->
				  <button type="submit" onclick="return checkThis(this.form,'update_Ad',1);">确定</button>
</div>
				 </s:form>
                 </div>
		</div>
	
	
	
    
    
    
    
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	
	

</body>

</html>