<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>公告管理</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="../js/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.all.js"></script>
<script type="text/javascript" charset="utf-8" src="../js/ueditor/lang/zh-cn/zh-cn.js"></script>
<link type="text/css" src="../js/ueditor/themes/default/css/ueditor.css"/>
<script type="text/javascript" src="js/notice.js" charset="utf-8"></script>


</head>
  
  <body>
   
	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">系统管理</a></li>
    <li><a href="list_notice">公告管理</a></li>
    <li><a href="list_notice_type">公告类别</a></li>
    </ul>
    </div>
 <form method="post" name="form1" id="form1">
 	

 <input type="hidden" id="deleteA" value="delete_notice_type">
 <input type="hidden" id="findA" value="find_notice">
 <input type="hidden" id="searchA" value="search_notice">
 <input type="hidden" id="updateA" value="update_notice"> 
 
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li rel="leanModal" name="signup" href="#signup"><span><img src="images/t01.png" width="24" height="24" /></span>添加</li>

		<li rel="leanModal1" class="modiItemNoType" href="#signup1"><span><img src="images/t02.png" width="24" height="24" /></span>修改</li>

        <li class="deleteItem" ><span><img src="images/t03.png" /></span>删除</li>
        </ul>
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th><input type='checkbox' id='checkall' name='checkall' /></th>
        <th>类别编号</th>
        <th>类别名称</th>
        <th>类别说明</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${noticeTypesList }" var="list">
	        <tr align="center">
				<td width="29" height="38" style="padding-left:0px">
				<input type="checkbox" name="mid" value=${list.noTypeId } />
	       	  </td>
	        	<td width="100" align="center" class="modify">${list.noTypeId }</td>
	        	<td width="300" align="center" class="modify">${list.noTypeName }</td>
	        	<td width="400" align="center" class="modify">${list.noTypeDesc }</td>
	        </tr> 
        </c:forEach>
        </tbody>
    </table>
   
     ${pages.pageStr}
	 </form>
	<div id="signup" style="width:450px">
			<div id="signup-ct">
				<div id="signup-header">
					<h2>新建类别</h2>
					<a class="modal_close" href="#"></a>
				</div>
				
				<form  id="form"  method="post">
				   <div class="txt-fld">
				    <label for="">类别名称:</label>
				    <input id="" class="good_input" name="noTitle" type="text" />
				  </div>
				  <div class="txt-fld">
				    <label for="">类别说明:</label>
				    <input id="" class="good_input" name="noContent" type="text" />
				  </div>
				   <div class="txt-fld">
				  <button type="submit" onclick="return checkThisType(this.form,'add_notice_type',0);">确定</button> 
				  </div>
				 </form>
			</div>
		</div>
		
		<div id="signup1" style="width:450px; height: 250px;">
			<div id="signup-ct">
            <div id="signup-header">
					<h2>修改类别</h2>
					<a class="modal_close" href="#"></a>
			  </div>
              <form method="post">
              <div id="signupmodi">
                </div>
                    
              <div class="btn-fld">
              <div id="tipHtmlModi" style="color:red;float:left">
                </div>
				  <button type="submit" onclick="return checkThisType(this.form,'update_notice_type',1);">确定</button>
</div>
			  </form>
      </div>
</div>
	
	
	
    
    
    
    
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	
	
	

</body>
</html>
