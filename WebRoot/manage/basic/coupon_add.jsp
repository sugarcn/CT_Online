<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <script type="text/javascript" src="<%=basePath %>manage/js/jquery.js"></script>
    <title>添加优惠劵</title>
    
  <link href="<%=basePath %>manage/css/style.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" 
src="<%=basePath%>manage/js/My97DatePicker/WdatePicker.js" 
    charset="gb2312"  defer="defer"></script>
   <script type="text/javascript">
    
    $(document).ready(function(){
      $(".zhekou").hide();
    });
  //自动弹出下拉菜单
function getGoodsByGname(){

    var GType =$("#GType").val();
	var GName = $("#GName").val();
	$(".sbss").html("");
	
	$.post("goods_getGoodsByGname1",{"goodsDTO.GName":GName,"goodsDTO.GType":GType},function(data){
		//data = eval(data);
		var html = "";
		$(".sbss").css("display","block");
		for(var i=0;i<data.length;i++){
			html = html + "<li onclick=\"javascript:setGid("+data[i][0]+",\'"+data[i][1]+"\')\"><span >"+data[i][1]+"</span></li>";
			$(".sbss").html(html);
		}
	});
}
//设置下拉菜单的商品Gid
function setGid(Gid,Gname){
	$("#GId").val(Gid);
	$("#GName").val(Gname);
	
	$(".sbss").css("display","none");
}

function changeDiscount(){
 var CouponType = $("#CouponType").val();
 if(CouponType =="1"){
    $(".zhekou").hide();
   $(".youhui").show();
   $("#info").html(" ");
   $("#Amount").val(" ");
  }else if(CouponType =="2"){
   $(".zhekou").hide();
   $(".youhui").show();
   $("#info").html(" ");
   $("#Amount").val("0");
 }else if(CouponType =="3"){
   $(".zhekou").show();
   $(".youhui").hide();
    $("#info").html("（折扣范围在0和10之间）");
 }
}
    function checkAdd(){
    if($("#CouponName").val()==""){
            alert("优惠券名称不得为空！");
            return false;
    }
    if($("#Discount").val()==""){
            alert("优惠金额不得为空！");
            return false;
    }else if($("#CouponType").val()=="3"){
              if($("#Discount").val()==""){
              alert("折扣不得为空！");
               return false;
              }else if($("#Discount").val() <=0 || $("#Discount").val()>=10){
                alert("折扣范围应在0到10之间！");
               return false;
              }
    }
    
    if($("#Amount").val()==""){
            alert("所需消费金额不得为空！");
            return false;
    }
    if($("#STime").val()==""){
            alert("起始时间不得为空！");
            return false;
    }
    if($("#ETime").val()==""){
            alert("截止时间不得为空！");
            return false;
    }
    
     $("#addcoupon").submit();
     alert("优惠券添加成功！");
    }
    </script>
  </head>
  
  <body>
     
	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="/manage/list_coupon">优惠券管理</a></li>
    <li><a href="#">新增优惠券</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    <div class="formtitle"><span>基本信息</span></div>
    <form action="manage/add_coupon" id="addcoupon" name="addcoupon" method="post">
		<input type="hidden" id="CouponId" name="couponId" value="" />
    <ul class="forminfo">
    <li><label>优惠劵名称：</label><input id="CouponName" name="coupon.couponName" type="text" class="dfinput" /></li>
    <li class="infoer"><label>类型：</label><select id="CouponType" name="coupon.couponType"  onchange="changeDiscount()">
						<option value="1">满减</option>
						<option value="2">无限制</option>
						<option value="3">折扣</option>
						<option value="9">样品券</option>
					</select></li>
	<li class="infoer"><label>商品类型标志：</label><select id="GType" name="coupon.GType">
						<option value="9">全品类</option>
						<option value="0">商品</option>
						<option value="1">商品组</option>
						
					</select></li>				
    <li><label class="zhekou">折扣：</label ><label class="youhui">优惠金额：</label><input id="Discount" name="coupon.discount" type="text" class="dfinput" /><span id="info"></span></li>
    <li><label>所需消费金额：</label><input id="Amount" name="coupon.amount" type="text" class="dfinput" /></li>
    <li><label>起始时间(YYYYMMDD)：</label><input id="STime" name="coupon.stime"  onfocus="WdatePicker({skin:'whyGreen',dateFmt:'yyyy-MM-dd HH:mm:ss'})" type="text" class="dfinput" /></li>
    <li><label>截止时间(YYYYMMDD)：</label><input id="ETime" name="coupon.etime" onfocus="WdatePicker({skin:'whyGreen',dateFmt:'yyyy-MM-dd HH:mm:ss'})" type="text" class="dfinput"  /></li>
    
    <li><label>商品名称：</label><input id="GName" name="coupon.GName" onkeyup="getGoodsByGname();" type="text" class="dfinput" /></li>
                         <ul class="sbss" style="display:none;"></ul>
		                  <input type="hidden" id="GId" name="GId">
      <li><label>发行量：</label><input id="releaseNum" name="coupon.ReleaseNum" type="text" class="dfinput" /></li>
       <li><label>每人限领数：</label><input id="limitNum" name="coupon.limitNum" type="text" class="dfinput" /></li>
    <li><label>&nbsp;</label><input id="subtn" onclick="checkAdd()" type="button" class="btn" value="新增"/></li>
    				<div style="clear:both"></div>
			</ul>
    </ul>
    
    </form>
    </div>

  </body>
</html>
