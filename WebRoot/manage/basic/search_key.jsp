<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="../js/ueditor/ueditor.config.js"></script>
<link type="text/css" src="../js/ueditor/themes/default/css/ueditor.css"/>
<script type="text/javascript" src="js/searchReplace.js" charset="utf-8"></script>
<style>
    .tanchu{
            width:100%;
            height:100%;
            background:rgba(0,0,0,0.5);
            position:fixed;
            top:0;left:0;
        }
        div.t_content{
            width:400px;
            border-radius:5px;
            background:#fff;
            position:fixed;
            top:50%;
            left:50%;
            margin-left:-200px;
        }
        div.t_content>p{
            width:100%;
            height:40px;
            background:#DFDFDF;
            color:#333;
            padding-left:10px;
            line-height:40px;
            font-size:16px;
            border-radius:5px 5px 0 0;
        }
        div.t_content>div{
            width:100%;
            position:relative;
            padding:15px 100px 20px 15px;
        }
        div.t_content>div input{
            width:240px;
            height:30px;
            border-radius:3px;
            color:#333;
            outline:none;
            box-shadow:none;
            border:1px solid #ddd;
            margin-top:15px;
            padding-left:10px;
        }
        div.t_content>.btn1{
            position:absolute;
            top:70px;right:20px;
            width:100px;
            height:30px;
            border-radius:3px;
            border:0;
            font-size:14px;
            background:#108FC6;
            color:#fff;
            cursor:pointer;
        }
        div.t_contentt>.btn1:hover{
            background:#0579AA;
        }
        div.t_content>.btn2{
            width:80px;
            height:30px;
            border-radius:3px;
            background:#5AC96A;
            font-size:16px;
            border:0;
            color:#fff;
            margin:15px 0 15px 160px;
            cursor:pointer;
        }
        div.t_content>.btn2:hover{
            background:#3EA54B;
        }
        .addBtnClass{
        	margin-left:26px;
        	height: 31px;
        	background-color:#5AC96A;
        }
        .addBtnClass:hover{
        	margin-left:26px;
        	height: 31px;
        	 background:#3EA54B;
        }
</style>
</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">系统管理</a></li>
    <li><a href="#">替换词管理</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" id="form1" theme="simple">   
 
 <input type="hidden" id="deleteA" value="delete_searchKey_Replace">
 <input type="hidden" id="findA" value="find_userRank">
 <input type="hidden" id="searchA" value="search_replace">
 <input type="hidden" id="updateA" value="update_userRank"> 
 
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li rel="leanModal" class="modiItem1" name="signup" href="#signup" onclick=""><span><img src="images/t01.png" width="24" height="24" /></span>添加</li>

		<li rel="leanModal1" class="modiItem" href="#signup1"><span><img src="images/t02.png" width="24" height="24" /></span>修改</li>

        <li class="deleteItem" ><span><img src="images/t03.png" /></span>删除</li>
        </ul>

	<div class="ssk">
     
		   <div class="ssbk">
		      <div style="float:left;padding-left:30px;padding-top:1px"><input type="text" id="keyword" name="keyword" class="wbdd" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="${keyword}"></div>
		      <div style="float:right"><img src="images/ss.jpg" class="searchItem"></div>
		      <div class="clear"></div>
		   </div>
   
  </div>
    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th><input type='checkbox' id='checkall' name='checkall' /></th>
        <th align="center">查询词1</th>
        <th align="center">查询词2</th>
        <th align="center">查询词3</th>
        <th align="center">查询词4</th>
        <th align="center">查询词5</th>
        </tr>
        </thead>
        <tbody>
		<c:forEach items="${replacesList }" var="list" varStatus="indexvar">      
		   <tr align="center"
		   	<c:if test="${indexvar.index % 2 != 0 }">
		   		 class="odd"
		   	</c:if>
		   >
			<td width="29" height="38" style="padding-left:10px">
				<input type="checkbox" name="mid" value=${list.reId } />
			</td>
		   	<td width="452" align="center" class="modify">${list.re1 }</td>
		   	<td width="452" align="center" class="modify">${list.re2 }</td>
		   	<td width="452" align="center" class="modify">${list.re3 }</td>
		   	<td width="452" align="center" class="modify">${list.re4 }</td>
		   	<td width="452" align="center" class="modify">${list.re5 }</td>
		   </tr>
		</c:forEach>       
        </tbody>
    </table>
    
     ${pages.pageStr}
     
   </s:form> 
    
	 
	<div id="signup" style="width: 550px;">
			<div id="signup-ct">
				<div id="signup-header">
					<h2>新建查询词</h2>
					<a class="modal_close" href="#"></a>
				</div>
				
				<form>
				   	<div name="renum" style="width: 84%;" class="txt-fld">
				    	<label for="">查询词1:</label>
				    	<input id="" class="good_input" name="re1" type="text" />
				    	<button class="addBtnClass" id="addSreachKey" type="button">再加一行</button>
				  	</div>
				  	<div class="btn-fld">
                  		<div id="tipHtml" style="color:red;float:left">
                    	</div>
				  	<button type="submit" onclick="return checkThis(this.form,'add_searchKey_Replace',0);">确定</button>
					</div>
				 </form>
			</div>
		</div>
		
		<div id="signup1" style="width: 550px;">
			<div id="signup-ct">
            <div id="signup-header">
					<h2>修改查询词</h2>
					<a class="modal_close" href="#"></a>
				</div>
              <form>
              <div id="signupmodi">
                    </div>
                    
              <div class="btn-fld">
              	  <div id="tipHtmlModi" style="color:red;float:left">
                  </div>
				  <button type="submit" onclick="return checkThis(this.form,'update_searchKey_Replace',1);">确定</button>
			  </div>
				 </form>
                 </div>
		</div>
	
	
	
    
    
    
    
    </div>
    <div style="display: none;"  class="tanchu">
    </div>
<div style="display: none;" class="t_content">
    <p>添加</p>
    <div class="inp">
        <form>
            <input type="text">
            <input type="text">
        </form>
    </div>
    <button class="btn1">再加一行</button>
    <button class="btn2">确定</button>
</div>
    
    <script type="text/javascript">
    $(function(){
    	$("#addSreachKey").click(function(){
    		var num = $("div[name=renum]");
    		if(num.length < 5){
	    		var str = "<div name=\"renum\" style=\"width: 63%;\" class=\"txt-fld\">";
		    	str += "<label style=\"width:89px;\" for=\"\">查询词"+(num.length+1)+":</label>";
		    	str += "<input id=\"\" class=\"good_input\" name=\"re"+(num.length+1)+"\" type=\"text\" />";
		  		str += "</div>";
	    		$(this).parent("div[class=txt-fld]").siblings("div[class=btn-fld]").before(str);
    		} else {
    			alert("查询词替换最多五个，不能再多啦");
    		}
    	});
    });
	//$('.tablelist tbody tr:odd').addClass('odd');
	//$(document).ready(function(){
	//	var h=parseFloat($(".t_content").css("height"));
	//    console.log(h);
	//    $(".t_content").css("margin-top",-(h/2)+"px");
	//    $(".btn1").click(function(){
	//        var height=parseFloat($(".t_content").css("height"));
	//        $(".t_content .inp").children("form").append($("<input type='text'>"));
	//        $(".t_content").css("margin-top",-(height/2)+"px");
	//    })
	//    $(".tanchu").click(function(){
	//            $(this).fadeOut();
	//            $(".t_content").fadeOut();
	//    });
	//});
	</script>
	
	

</body>

</html>
