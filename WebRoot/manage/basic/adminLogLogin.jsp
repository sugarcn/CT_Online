<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>公告管理</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="../js/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.all.js"></script>
<script type="text/javascript" charset="utf-8" src="../js/ueditor/lang/zh-cn/zh-cn.js"></script>
<link type="text/css" src="../js/ueditor/themes/default/css/ueditor.css"/>
<script type="text/javascript" src="../js/ajaxfileupload.js" charset="utf-8"></script>
<script type="text/javascript" src="js/notice.js" charset="utf-8"></script>


</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">系统管理</a></li>
    <li><a href="list_notice">管理员登录日志</a></li>
    </ul>
    </div>
 <input type="hidden" id="deleteA" value="delete_notice">
 <input type="hidden" id="findA" value="find_notice">
 <input type="hidden" id="searchA" value="search_notice">
 <input type="hidden" id="updateA" value="update_notice"> 
 
    <div class="rightinfo">
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>序号</th>
        <th>时间</th>
        <th>用户名</th>
        <th>ip</th>
        </tr>
        </thead>
        <tbody>
  <s:iterator value="loginLogsList" var="list">      
        <tr align="center">
        	<td width="400" align="center" class="modify"><s:property value="#list.logId"/></td>
        	<td width="400" align="center" class="modify"><s:property value="#list.logTime"/></td>
        	<td width="400" align="center" class="modify"><s:property value="#list.manager.MManagerId"/></td>
        	<td width="400" align="center" class="modify"><s:property value="#list.logIp"/></td>
        </tr> 
</s:iterator>       
        </tbody>
    </table>
   
     ${pages.pageStr}
     
      </div>
</div>
	
	
	
    
    
    
    
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	
	
	

</body>
</html>
