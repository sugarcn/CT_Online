<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/userRank.js" charset="utf-8"></script>
</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">营销中心</a></li>
    <li><a href="#">优惠券管理</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" id="form1" theme="simple">   
 
 <input type="hidden" id="deleteA" value="delete_coupon">
 <input type="hidden" id="searchA" value="search_coupon">
 
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li rel="leanModal" class="modiItem1" name="signup" ><span><img src="images/t01.png" width="24" height="24" /></span><a href="<%=request.getContextPath() %>/manage/basic/coupon_add.jsp">添加优惠券</a></li>

		<li rel="leanModal1"  ><span><img src="images/t02.png" width="24" height="24" /></span><a href="<%=request.getContextPath() %>/manage/list_coupondetail">优惠券明细总表</a></li>

        </ul>

	<div class="ssk">
     
		   <div class="ssbk">
		      <div style="float:left;padding-left:30px;padding-top:1px"><input type="text" id="keyword" name="keyword" class="wbdd" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="${keyword}"></div>
		      <div style="float:right"><img src="images/ss.jpg" class="searchItem"></div>
		      <div class="clear"></div>
		   </div>
   
  </div>
    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
		<th>优惠劵名称</th>
		<th>类型</th>
		<th>所需消费金额</th>
		<th>折扣或优惠金额</th>
		<th>起始时间</th>
		<th>截止时间</th>
		<th>商品名称</th>
		<th>商品类型标记</th>
		<th>发行量</th>	
		<th>操作</th>
        </tr>
        </thead>
        <tbody>
  <s:iterator value="couponList" var="list">      
        <tr>
		<td width="212" align="center" class="modify"><s:property value="#list.CouponName"/></td>
  		<td width="212" align="center" class="modify"><s:if test="#list.CouponType==1">满减</s:if><s:elseif test="#list.CouponType==2">无限制</s:elseif><s:elseif test="#list.CouponType==3">折扣</s:elseif><s:else>样品券</s:else></td>
  		<td width="212" align="center" class="modify"><s:property value="#list.Amount"/></td>
  		<td width="212" align="center" class="modify"><s:property value="#list.Discount"/></td>  		
  		<td width="212" align="center" class="modify"><s:property value="#list.Stime"/></td>
  		<td width="212" align="center" class="modify"><s:property value="#list.Etime"/></td>
  		<td width="212" align="center" class="modify"><s:property value="#list.GName"/></td>
  		<td width="212" align="center" class="modify"><s:if test="#list.GType==0">商品</s:if><s:else>商品组</s:else> </td>
  		<td width="212" align="center" class="modify"><s:property value="#list.ReleaseNum"/></td>  	
  		<td width="212" align="center" class="modify">
  		<a id="del" rel="leanModal"  href='delete_coupon?id=<s:property value="#list.CouponId"/>'>   删除         </a>
  		 <a id="detail" rel="leanModal"  href='manage/list_oneCoupondetail?coupondetailDTO.coupondetail.couponId=<s:property value="#list.CouponId"/>'>优惠券明细</a>
  		 <a id="detailFaXing" rel="leanModal" href="manage/couponIssue?id=<s:property value="#list.CouponId"/>">发行</a>
  		  </td>  		
        </tr> 
</s:iterator>       
        </tbody>
    </table>
    
     ${pages.pageStr}
     
   </s:form> 
    
	
	
	
	
    
    
    
    
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	
	

</body>

</html>
