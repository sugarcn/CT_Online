<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>中心</title>

		<script type="text/javascript" src="manage/js/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=basePath %>manage/js/styles.css" />
        <link rel="stylesheet" type="text/css" href="<%=basePath %>manage/js/jquery.confirm.css" />
        <link rel="stylesheet" href="<%=basePath %>manage/css/mycss.css" type="text/css"/>
        
<script type="text/javascript"> 
$(function(){
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
			
			
			
function save(){
	var allinput = document.getElementsByTagName("input");
	var loopTime = allinput.length;
	var my_arrayradio = new Array();
	var my_arraytext = new Array();
	var count1=0;
	var count2=0;
	var countOfChecked=0;
	for(var i=0;i<loopTime;i++){
		if(allinput[i].type=="radio"){
			if(allinput[i].checked==true){
				my_arrayradio[count1++]=allinput[i].value;
				countOfChecked++;
			}
		}
	}
	for(var j=0;j<loopTime;j++){
		if(allinput[j].type=="hidden"){
				my_arraytext[count2++]=allinput[j].value;
			}
	}
	$.post(
			"${pageContext.request.contextPath}/role_saveOrUpdate",
	     {"roleModuleDTO.right":my_arrayradio.toString(),
	     "roleModuleDTO.moduleId":my_arraytext.toString()},
	     function(data,varstart){
	    if(varstart == "success"){
	    	alert("保存成功");
	    }
		window.location.href="${pageContext.request.contextPath}/role_list";
		},"json"
	);
}

</script>
</head>

<body>
	<s:form>
		<input type="hidden" value="${roleId}"/>
		<table border="1" width="960" cellspacing="0" cellpadding="0" bordercolor="#D2CFCF" style="border-collapse: collapse"> 
			<tr>
				<td>模块名称</td>
				<td>权限</td>
			</tr>
			
			<s:iterator value="modules" status="st">
			 	<tr>
			 		<td>
			 			<s:property value="modules[#st.index][1]"/>--<s:property value="modules[#st.index][2]"/>
			 			<input type="hidden" id="mid" name="mid" value="<s:property value="modules[#st.index][0]"/>"/>
			 		</td>
			 		<td>
			 			<s:if test="modules[#st.index][6]==null">
			 				<input type="radio"  name="<s:property value="modules[#st.index][0]"/>" value="0" checked="checked">无权限</input>
			 				<input type="radio"  name="<s:property value="modules[#st.index][0]"/>" value="1">浏览</input>
			 				<input type="radio" name="<s:property value="modules[#st.index][0]"/>"  value="2">编辑</input>	
			 			</s:if>
			 			<s:elseif test="modules[#st.index][6]=='0'">
			 				<input type="radio" name="<s:property value="modules[#st.index][0]"/>" value="0" checked="checked">无权限</input>
			 				<input type="radio" name="<s:property value="modules[#st.index][0]"/>" value="1">浏览</input>
			 				<input type="radio" name="<s:property value="modules[#st.index][0]"/>" value="2">编辑</input>	
			 			</s:elseif>
			 			<s:elseif test="modules[#st.index][6]=='1'">
			 				<input type="radio" name="<s:property value="modules[#st.index][0]"/>" value="0">无权限</input>
			 				<input type="radio" name="<s:property value="modules[#st.index][0]"/>"  value="1" checked="checked">浏览</input>
			 				<input type="radio" name="<s:property value="modules[#st.index][0]"/>"  value="2">编辑</input>	
			 			</s:elseif>
			 			<s:elseif test="modules[#st.index][6]=='2'">
			 				<input type="radio"  name="<s:property value="modules[#st.index][0]"/>" value="0">无权限</input>
			 				<input type="radio"  name="<s:property value="modules[#st.index][0]"/>" value="1">浏览</input>
			 				<input type="radio"  name="<s:property value="modules[#st.index][0]"/>" value="2" checked="checked">编辑</input>	
			 			</s:elseif>
			 			
			 		</td>
			 	</tr>
			</s:iterator>
			<tr><td><input type="button" value="保存" onclick="save();"/></td></tr>
		</table>
	</s:form>
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	
	

</body>

</html>