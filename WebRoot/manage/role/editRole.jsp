<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>中心</title>

<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
        
        <link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript"> 

$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
</script>
</head>

<body>
<s:form action="role_saveEdit" method="post">
	<input type="hidden" value="${role.roleId}" id="roleId" name="role.roleId"/>
	<table  border="1" width="960" cellspacing="0" cellpadding="0" bordercolor="#D2CFCF" style="border-collapse: collapse">
	   <tr>
	   	<td>角色名:</td>
	   	<td><input type="text" id ="roleName" name="role.roleName" value="${role.roleName }"/></td>
	   </tr>
	   <tr>
	   	<td>角色描述:</td>
	   	<td><input type="text" id ="roleDesc" name="role.roleDesc" value="${role.roleDesc }"/></td>
	   </tr>
	   <tr>
	   	<td colspan="2" align="center"><input type="submit" value="保存"/></td>
	   </tr>
	 </table> 
 </s:form>
</body>
</html>