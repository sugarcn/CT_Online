<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="<%=basePath%>manage/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=basePath%>manage/js/jquery.js"></script>
<script type="text/javascript" src="<%=basePath%>manage/js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="<%=basePath%>manage/js/style.css" />
		<script type="text/javascript" src="<%=basePath%>manage/js/jquery.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>manage/js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=basePath%>manage/js/styles.css" />
        <link rel="stylesheet" type="text/css" href="<%=basePath%>manage/js/jquery.confirm.css" />
		<link rel="stylesheet" href="<%=basePath%>manage/css/mycss.css" type="text/css"/>
<script type="text/javascript" src="<%=basePath%>manage/js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="<%=basePath%>manage/js/role.js" charset="utf-8"></script>
<script type="text/javascript">

function role(id){
	window.location.href="<%=basePath%>/role_role?roleId="+id;
}

</script>
</head>




<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">用户管理</a></li>
    <li><a href="#">角色管理</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" id="form1" theme="simple">   
 
 <input type="hidden" id="deleteA" value="role_delRole">
 <input type="hidden" id="findA" value="find_userRank">
 <input type="hidden" id="searchA" value="search_role">
 <input type="hidden" id="updateA" value="update_userRank"> 
 
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
	        <li rel="leanModal" name="signup" href="#signup" onclick="getMaxRName()"><span><img src="<%=basePath%>manage/images/t01.png" width="24" height="24" /></span>添加</li>
			<li rel="leanModal1" class="modiItem" href="#signup1"><span><img src="<%=basePath%>manage/images/t02.png" width="24" height="24" /></span>修改</li>
	        <li class="deleteItem" ><span><img src="<%=basePath%>manage/images/t03.png" /></span>删除</li>
        </ul>

	<div class="ssk">
     
		   <div class="ssbk">
		      <div style="float:left;padding-left:30px;padding-top:1px"><input type="text" id="keyword" name="keyword" class="wbdd" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="${keyword}"></div>
		      <div style="float:right"><img src="<%=basePath%>manage/images/ss.jpg" class="searchItem"></div>
		      <div class="clear"></div>
		   </div>
   
  </div>
    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th><input type='checkbox' id='checkall' name='checkall' /></th>
        <th>角色名</th>
        <th>角色描述</th>
        <th>操作</th>
        </tr>
        </thead>
        <tbody>
  <s:iterator value="roles" var="list">      
        <tr>
			<td width="29" height="38" style="padding-left:10px">
			<input type="checkbox" name="mid" value=${list.roleId } />
       	  </td>
        	<td width="228" align="center" class="modify"><s:property value="#list.roleName"/></td>
        	<td width="212" align="center" class="modify"><s:property value="#list.roleDesc"/></td>
        	<td width="212" align="center" class="modify"> <a href='javascript:role("<s:property value='roleId'/>")'>权限</a></td>
        </tr> 
</s:iterator>       
        </tbody>
    </table>
    
     ${pages.pageStr}
     
   </s:form> 
    
	
	<div id="signup">
			<div id="signup-ct">
				<div id="signup-header">
					<h2>新建角色</h2>
					<a class="modal_close" href="#"></a>
				</div>
				
				<form method="post">
				  <div class="txt-fld">
				    <label for="">角色名称:</label>
				    <input id="" class="good_input" name="roleName" type="text" />
				  </div>
				  <div class="txt-fld">
				    <label for="">角色描述:</label>
				    <input id="" class="good_input" name="roleDesc" type="text" />
				  </div>
				 

				  <div class="btn-fld">
                  <div id="tipHtml" style="color:red;float:left">
                    </div>
				  <button class="usergroup_qd" type="submit" onclick="return checkThis(this.form,'role_save',0);">确定</button>
</div>
				 </form>
			</div>
		</div>
		
		<div id="signup1">
			<div id="signup-ct">
            <div id="signup-header">
					<h2>修改角色</h2>
					<a class="modal_close" href="#"></a>
				</div>
              <form method="post">
              <div id="signupmodi">
                    </div>
                    
              <div class="btn-fld">
              <div id="tipHtmlModi" style="color:red;float:left">
                    </div>
				  <button class="usergroup_qd" type="submit" onclick="return checkThis(this.form,'update_role',1);">确定</button>
</div>
				 </form>
                 </div>
		</div>
	
	
	
    
    
    
    
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	
	

</body>

</html>
