<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
        <script type="text/javascript"
	src="<%=basePath%>manage/js/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css"
	href="<%=basePath%>manage/js/jquery.fancybox.css?v=2.1.5"
	media="screen" />
<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="../js/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.all.js"></script>
<script type="text/javascript" src="../../js/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="../../js/ueditor/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript" src="../../js/ueditor/third-party/jquery-1.10.2.min.js"></script>
<link rel="stylesheet" href="../../js/ueditor/themes/default/css/ueditor.css" type="text/css"></link>
<script type="text/javascript" src="js/help.js" charset="utf-8"></script>
<script type="text/javascript">
		$(document).ready(function() {
			$('.fancybox').fancybox();
		});
	function showBigImg(imgpath) {
		$("img").attr("src", imgpath);
		$("#signup").show();
	}
	
				
</script>
</head>


<body>
	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">帮助中心</a></li>
    <li><a href="#">帮助类型管理</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" id="form1" theme="simple">   
 
 <input type="hidden" id="deleteA" value="delete_help">
 <input type="hidden" id="findA" value="find_help">
 <input type="hidden" id="searchA" value="search_help">
 <input type="hidden" id="updateA" value="update_help"> 
 
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li rel="leanModal" name="signup" href="#signup"><span><img src="images/t01.png" width="24" height="24" /></span>添加</li>

		<li rel="leanModal1" class="modiItem" href="#signup1"><span><img  src="images/t02.png" width="24" height="24" /></span>修改</li>

        <li class="deleteItem" ><span><img src="images/t03.png" /></span>删除</li>
        </ul>

	<div class="ssk">
     
		   <div class="ssbk">
		      <div style="float:left;padding-left:30px;padding-top:1px"><input type="text" id="keyword" name="keyword" class="wbdd" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="请输入你要查询的关键词"></div>
		      <div style="float:right"><img src="images/ss.jpg" class="searchItem"></div>
		      <div class="clear"></div>
		   </div>
   
  </div>
    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th><input type='checkbox' id='checkall' name='checkall' /></th>
        <th>帮助标题</th>
        <th>帮助分类</th>   
        </tr>
        </thead>
        <tbody>
  <s:iterator value="helps" var="list">      
        <tr>
			<td width="29" height="38" style="padding-left:10px">
			<input type="checkbox" name="mid" value=${list.HId } />
       	  </td>
        	<td width="212" align="center" class="modify"><s:property value="#list.HTitle"/></td>
        	<td width="212" align="center" class="modify"><s:property value="#list.helptype.HTypeName"/></td>  
        </tr> 
</s:iterator>       
        </tbody>
    </table>
     ${pages.pageStr}
   </s:form> 
   
   
    
	<div id="signup" style="width:800px">
			<div id="signup-ct">
				<div id="signup-header">
					<h2>新建帮助</h2>
					<a class="modal_close" href="#"></a>
				</div>
				
				<form action="addhelp"  id="form"  method="post"  enctype="multipart/form-data">
				<div class="btn-fld">
              <div id="tipHtmlModi" style="color:red;float:left">
                    </div>            
				 <button type="submit" onclick="return checkThis(this.form,'add_help',0);">确定</button>
					</div>	
				  <div class="txt-fld">
				    <label for="">帮助标题:</label>
				    <input id="" class="good_input" name="HTitle" type="text" />
				  </div>
				  <div class="txt-fld">
				    <label for="">帮助分类:</label>
				      <select id="HTypeId" name="HTypeId" style="width:262px; height:34px;">
				      <option>--请选择类型--</option>
				       <s:iterator value="typeList" var="list">
						<option value="${list.HTypeId}">${list.HTypeName}</option>				
						</s:iterator>
				    </select>
				  </div>
				  <div class="ctn-fld">
				    <label for="">帮助内容:</label>	
				    <script  type="text/plain" id="editor" name="HDesc" style="width:760px;padding-top:50px"></script>	   
				   <script type="text/javascript">
				   			var ue=UE.getEditor("editor");
				   </script>				   
				  </div>				  
				  
				   								
				 </form>
			</div>
		</div>
		
		<div id="signup1" class="help_xiugai" style="width:800px">
		<form method="post">
			<div id="signup-ct">
			<div class="btn-fld">
              <div id="tipHtmlModi" style="color:red;float:left">
                    </div>
                   
				  <button type="submit" onclick="checkThis(this.form,'update_help',1);">确定</button>
</div>
            <div id="signup-header">
					<h2>修改帮助</h2>
					<a class="modal_close" href="#"></a>
				</div>
				<script type="text/javascript">
				</script>
                    <select id="HType" name="HTypeId" style="width:262px; height:34px;margin-top:12px;margin-left:120px;">        
                       <c:forEach items="${typeList }" var="tlist">
                        	<option 
	                        	 <c:if test="${tlist.HTypeId == help.HTypeId }">
	                        		 selected="selected"
	                        	</c:if> 
                        	 value="${tlist.HTypeId}"> ${tlist.HTypeName}</option>                
                       </c:forEach>
                    </select>       
             </div>
              <div id="signupmodi"> 
                    </div>              
              
				 </form>
                 </div>
		</div>
	
	
	
    
    
    
    
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	
	

</body>

</html>