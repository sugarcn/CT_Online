<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>欢迎登录后台管理系统</title>
<link href="<%=basePath %>manage/css/style.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="<%=basePath %>manage/js/jquery.js"></script>
<script src="<%=basePath %>manage/js/cloud.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath %>manage/js/jquery.min.js"></script>
		<script type="text/javascript" src="<%=basePath %>manage/js/jquery.leanModal.min.js"></script>
		<script type="text/javascript" src="<%=basePath %>manage/js/jquery-latest.pack.js" ></script>
<script language="javascript">

	$(function(){
    $('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
	$(window).resize(function(){  
    $('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
    })  
});  
function LoginSubmit()
{ 
          if (event.keyCode == 13) 
          { 
              event.keyCode = 9;
              event.returnValue = false;
              login();
          } 
   } 

	function login(){
		var MManagerId = $("#MManagerId").val();
		var MPassword = $("#MPassword").val();
		//alert(MManagerId);
		//alert(MPassword);
			if(MManagerId == ""){
		$("#usernametip").text("请输入用户名");
	}else {
		$("#usernametip").text("");
		if(MPassword.length<6){
			$("#passwdtip").text("密码长度至少6位");
			$("#MPassword").val("");
		}else {
			$("#passwdtip").text("");
			$("#form").submit();
		}
		}
	}
</script> 

</head>

<body style="background-color:#1c77ac; background-image:url(images/light.png); background-repeat:no-repeat; background-position:center top; overflow:hidden;"  onkeydown="LoginSubmit();">



    <div id="mainBody">
      <div id="cloud1" class="cloud"></div>
      <div id="cloud2" class="cloud"></div>
    </div>  


<div class="logintop">    
    <span>欢迎登录后台管理界面平台</span>    
    <ul>
    <li><a href="#">回首页</a></li>
    <li><a href="#">帮助</a></li>
    <li><a href="#">关于</a></li>
    </ul>    
    </div>
    
    <div class="loginbody">
    
    <span class="systemlogo"></span> 
     <form action="<%=basePath %>manage/admin_main" method="post" id="form">  
    <div class="loginbox">
    
    <ul>
    <li>
    <span style="color: red">${message}</span>
    <input id="MManagerId" name="manager.MManagerId" type="text" class="loginuser" value="admin" onclick="JavaScript:this.value=''"/>
    <span style="color: red;" id="usernametip"></span>
    </li>
    <li><input id="MPassword" name="manager.MPassword" type="password" class="loginpwd" value="" onclick="JavaScript:this.value=''"/>
    <span style="color: red;" id="passwdtip"></span>
    </li>
    <li><input name="" type="button" class="loginbtn" value="登录"  onclick="login();"  /><label><input name="" type="checkbox" value="" checked="checked" />记住密码</label><label><a href="#">忘记密码？</a></label></li>
    </ul>
    
    
    </div>
</form>
    
    </div>
    
    
    
    <div class="loginbm">版权所有  2013</div>
	
    
</body>

</html>
