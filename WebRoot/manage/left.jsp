<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="<%=basePath %>manage/css/style.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="<%=basePath %>manage/js/jquery.js"></script>

<script type="text/javascript">
$(function(){	
	//导航切换
	$(".menuson li").click(function(){
		$(".menuson li.active").removeClass("active")
		$(this).addClass("active");
	});
	
	$('.title').click(function(){
		var $ul = $(this).next('ul');
		$('dd').find('ul').slideUp();
		if($ul.is(':visible')){
			$(this).next('ul').slideUp();
		}else{
			$(this).next('ul').slideDown();
		}
	});
})	
</script>


</head>

<body style="background:#f0f9fd;">
	<dl class="leftmenu">
	<s:iterator value="leftlists" var="list">
	<dd>
    <div class="title">
    
    <span><img src="images/leftico02.png" /></span>${list.moduleName}
    </div>
    <s:if test=" null != #list.childs and #list.childs.size()>0">
					<ul class="menuson">
						<s:iterator value="#list.childs" var="chd">
							<li><cite></cite>
								<a href='<%=basePath %>${chd.moduleUrl}' target="rightFrame">${chd.moduleName}</a><i></i>
							</li>
						</s:iterator>		
					</ul>
	</s:if>    
    </dd> 
	</s:iterator>
</body>
</html>
