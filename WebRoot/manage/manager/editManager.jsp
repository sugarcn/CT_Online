<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>编辑管理员</title>
	<link href="<%=basePath %>manage/css/style.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<%=basePath %>manage/js/jquery.js"></script>
	<script type="text/javascript" src="<%=basePath %>manage/js/jquery-latest.pack.js" ></script>
	<link rel="stylesheet" type="text/css" href="<%=basePath %>manage/js/style.css" />
	<script type="text/javascript" src="<%=basePath %>manage/js/jquery.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>manage/js/jquery.leanModal.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<%=basePath %>manage/js/styles.css" />
    <link rel="stylesheet" type="text/css" href="<%=basePath %>manage/js/jquery.confirm.css" />
	<link rel="stylesheet" href="<%=basePath %>manage/css/mycss.css" type="text/css"/>
	<script type="text/javascript" src="<%=basePath %>manage/js/ct.js" charset="utf-8"></script>
	<script type="text/javascript" src="<%=basePath %>manage/js/editManager.js" charset="utf-8"></script>
<script type="text/javascript"> 

$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
</script>
<script type="text/javascript"> 
function resetPass(MManagerId){

if(confirm("确定重置密码吗？此操作不可撤销！")){
  
  $.post("resetPwd",
  {"MManagerId":MManagerId},
  function(data){
   if(data){
   alert("重置密码成功！");
   }else{
   
   alert("重置密码失败！");
   }
  }
  );
 


}

}
function checkThis(f,mAction,modi,yuan){
	var noTitle = f.checkPassword.value;
	//validate(maxPoints);
	if (!noTitle || noTitle==''){
		$("#tipHtml").html("请输入密码");
		$("#tipHtmlModi").html("请输入密码");
		return false;
	} else {
		$("#typeId").val($("#noticeType").val());
		$.post(
			"checkPassword",
			{"MPassword":noTitle},
			function(data, varstart){
				if(data){
					alert("密码验证成功");
					$("div[name='passInfo']").remove();
					var str = "";
					str += "<div name=\"passInfo\" style=\"width:420px;\" class=\"txt-fld\">";
					str += "<label style=\"width:110px;\" for=\"\">请输入新密码:</label>";
					str += "<input class=\"good_input\" id=\"pass\" name=\"password\" type=\"password\" />";
					str += "</div>";
					
					str += "<div name=\"passInfo\" style=\"width:420px;\" class=\"txt-fld\">";
					str += "<label style=\"width:110px;\" for=\"\">再次输入新密码:</label>";
					str += "<input class=\"good_input\" id=\"passAgain\" name=\"passwordAgain\" type=\"password\" />";
					str += "</div>";
					$("#form3").prepend(str);
					$("#messPass").text("修改密码");
					$("#btnPass").text("修改密码");
					$("#btnPass").attr("onclick","return editPassWord()");
					return true;
				} else {
					alert("密码认证失败");
					return false;
				}
			}
		);
	}
}
function editPassWord(){
	var $pass = $("#pass").val(); 
	var $passAgain = $("#passAgain").val(); 
	if($pass == "" || $pass.length < 6){
		alert("密码格式不正确");
		return false;
	}
	if($pass != $passAgain){
		alert("两次输入密码不一致");
		return false;
	}
	$.post(
		"editManagerPassword",
		{"MPassword":$pass,"MPasswordAgain":$passAgain},
		function(data, varstart){
			if(data){
				alert("密码修改成功，请重新登录");
				//window.href="manage/admin_main";
				location.reload();
			} else {
				alert("请确认您的密码输入正确");
				return false;
			}
		}
	);
}
</script>
  </head>
  
  <body>
  <s:form action="manage/manager_saveEdit" method="post" id="form">
<input type="hidden" id="MId" name="manager.MId" value="${manager.MId }"/>
  <div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">管理员</a></li>
    <li><a href="#">编辑管理员</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    <div class="formtitle"><span>基本信息</span></div>
    
    <ul class="forminfo">
    <li><label>管理员姓名：</label><input name="manager.MManagername" id="MManagername" type="text" class="dfinput" value="${manager.MManagername}"/></li>
    <li><label>管理员账号：</label><input id="MManagerId" name="manager.MManagerId"  readonly="readonly"  value="${manager.MManagerId}"  type="text" class="dfinput" /></li>
    <li><label>管理员权限：</label>
           <select id="roleId" name="roleDTO.roleId">
			<s:iterator value="roles" var="list">
				<c:if test="${list.roleId == manager.roleId }">
					<option selected="selected" value="${list.roleId }" >${list.roleName }</option>
				</c:if>
				<c:if test="${list.roleId != manager.roleId }">
					<option value="${list.roleId }" >${list.roleName }</option>
				</c:if>
			</s:iterator>
			</select></li>
    <li class="editmanager">
      <input name=""  class="btn" type="submit" value="保存"/>
      <input name=""  class="btn" value="重置密码" id="reset" onclick="resetPass('${manager.MManagerId}')"/>
      <input class="btn" value="修改密码" id="editPassword2"/>
    </li>
     <ul style="display: none;" class="toolbar">
	        <li rel="leanModal" id="b" name="signup" href="#signup"><span><img src="images/t02.png"/></span>添加</li>
    </ul>
    </ul>
    
    
    </div>
    </s:form>
    	
	<div id="signup" style="width:500px; height: 300px;">
			<div id="signup-ct">
				<div id="signup-header">
					<h2 id="messPass">身份认证</h2>
					<a class="modal_close" href="#"></a>
				</div>
				
				<form  id="form3"  method="post"  enctype="multipart/form-data">
				  <div name="passInfo" class="txt-fld">
				    <label for="">原密码:</label>
				    <input id="" class="good_input" name="checkPassword" type="text" />
				  </div>
				   <div class="txt-fld">
				  	<button type="button" id="btnPass" onclick="return checkThis(this.form,'checkPass',0,this);">验证密码</button> 
				  </div>
				 </form>
			</div>
		</div>
  </body>
  <script type="text/javascript">
  	$(function(){
  		$("#editPassword2").click(function(){
  			$("#b").click();
  		});
  	});
  </script>
</html>
