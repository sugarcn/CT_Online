<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>新增管理员</title>
    <link href="<%=basePath %>manage/css/style.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<%=basePath %>manage/js/jquery.js"></script>
        
  <script type="text/javascript"> 

$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});

//验证用户名
     var flag=false;
     $.ajaxSetup({
        async: false
     });
     
     
	function checkName(){
		var MManagerId=$("#MManagerId").val();
		if(MManagerId == ""){
			$("#usernametip").text("请输入用户名");
		}else{
			$("#usernametip").text("");
			$.post("<%=basePath%>manage/manager_checkName",{"MManagerId":MManagerId},function(data){
					if(data=="success"){
						$("#usernametip").text("用户名合法");
						flag=true;
					}else {
						$("#usernametip").text("用户名已存在请重新输入");
						$("#username").val("");
						flag=false;
					}
			});
		}
	}

function checkPasswd(){
	var passwd=$("#passwd").val();
		if(passwd.length<6){
			$("#passwdtip").text("密码长度至少6位");
			$("#passwd").val("");
		}else {
			$("#passwdtip").text("");
		}
}
//验证确认密码是否相同
	function checkPasswd2(){
		var passwd=$("#passwd").val();
		var passwd2=$("#passwd2").val();
		if(passwd != passwd2){
			$("#passwdtip2").text("两次输入的密码不一样");
			$("#passwd2").val("");
			return false;
		}else{
			$("#passwdtip2").text("");
			return true;
		}
	}
	
	function save(){
		var MManagerId=$("#MManagerId").val();
		var passwd=$("#passwd").val();
		var passwd2=$("#passwd2").val();
		if(MManagerId == ""){
			$("#usernametip").text("请输入用户名");
		}
		if(passwd == ""){
			$("#passwdtip").text("请输入密码");
		}
		if(passwd2 == ""){
			$("#passwdtip2").text("请输入确认密码");
		}else if(flag){
			$("#form").submit();
		}
	}
</script>
  </head>
  
  <body>
  <s:form action="manage/manager_save" method="post" id="form">
    <div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">管理员</a></li>
    <li><a href="#">新增管理员</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    <div class="formtitle"><span>基本信息</span></div>
    
    <ul class="forminfo">
    <li><label>管理员名称：</label><input name="manager.MManagername" id="MManagername"  type="text" class="dfinput" onblur="checkName1();"/></li>
    <li><label>管理员账号：</label><input name="manager.MManagerId" id="MManagerId" type="text" class="dfinput" onblur="checkName();"/><span style="color: red;" id="usernametip"></span></li>
    <li><label>管理员角色：</label>
    <select id="roleId" name="roleDTO.roleId">
				<option value="">--请选择--</option>
				<s:iterator value="roles" var="list">
					<option value="${list.roleId }">${list.roleName }</option>
				</s:iterator>
			</select>
    </li>
    <li><label>请设置密码：</label><input name="manager.MPassword" id="passwd"  type="password" class="dfinput" onblur="checkPasswd();"/><span style="color: red;" id="passwdtip"></span></li>
    <li><label>请确认密码：</label><input name="passwd2" id="passwd2"  type="password" class="dfinput" onblur="checkPasswd2();"/><span style="color: red;" id="passwdtip2"></span></li>
    <li><label>&nbsp;</label><input name="" type="button" class="btn" onclick="save();" value="保存"/></li>
    </ul>
    
    
    </div>
    </s:form>
  </body>
</html>
