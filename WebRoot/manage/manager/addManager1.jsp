<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>中心</title>

<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
        
        <link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript"> 

$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});

//验证用户名
     var flag=false;
     $.ajaxSetup({
        async: false
     });
     
     
	function checkName(){
		var MManagerId=$("#MManagerId").val();
		if(MManagerId == ""){
			$("#usernametip").text("请输入用户名");
		}else{
			$("#usernametip").text("");
			$.post("<%=basePath%>manage/manager_checkName",{"MManagerId":MManagerId},function(data){
					if(data=="success"){
						$("#usernametip").text("用户名合法");
						flag=true;
					}else {
						$("#usernametip").text("用户名已存在请重新输入");
						$("#username").val("");
						flag=false;
					}
			});
		}
	}

function checkPasswd(){
	var passwd=$("#passwd").val();
		if(passwd.length<6){
			$("#passwdtip").text("密码长度至少6位");
			$("#passwd").val("");
		}else {
			$("#passwdtip").text("");
		}
}
//验证确认密码是否相同
	function checkPasswd2(){
		var passwd=$("#passwd").val();
		var passwd2=$("#passwd2").val();
		if(passwd != passwd2){
			$("#passwdtip2").text("两次输入的密码不一样");
			$("#passwd2").val("");
			return false;
		}else{
			$("#passwdtip2").text("");
			return true;
		}
	}
	
	function save(){
		var MManagerId=$("#MManagerId").val();
		var passwd=$("#passwd").val();
		var passwd2=$("#passwd2").val();
		if(MManagerId == ""){
			$("#usernametip").text("请输入用户名");
		}
		if(passwd == ""){
			$("#passwdtip").text("请输入密码");
		}
		if(passwd2 == ""){
			$("#passwdtip2").text("请输入确认密码");
		}else if(flag){
			$("#form").submit();
		}
	}
</script>
</head>

<body>
<s:form action="manage/manager_save" method="post" id="form">
<table  border="1" widtd="960" cellspacing="0" cellpadding="0" bordercolor="#D2CFCF" style="border-collapse: collapse">
   
	<tr>
		<td><span style="color: red;">*</span>管理员名称：</td>
		<td><input type="text" id="MManagername" name="manager.MManagername" onblur="checkName1();"/></td>
		<!-- <td colspan="2"><span style="color: red;" id="usernametip"></span></td> -->
	</tr>
	<tr>
		<td><span style="color: red;">*</span>管理员账号：</td>
		<td><input type="text" id="MManagerId" name="manager.MManagerId" onblur="checkName();"/></td>
		<td colspan="2"><span style="color: red;" id="usernametip"></span></td>
	</tr>
	<tr>
		<td><span style="color: red;">*</span>管理员角色：</td>
		<td>
			<select id="roleId" name="roleDTO.roleId">
				<option value="">--请选择--</option>
				<s:iterator value="roles" var="list">
					<option value="${list.roleId }">${list.roleName }</option>
				</s:iterator>
			</select>
		</td>
	</tr>
	<tr>
		<td><span style="color: red;">*</span>请设置密码：</td>
		<td><input type="password" id="passwd" name="manager.MPassword" onblur="checkPasswd();"/></td>
		<td colspan="2"><span style="color: red;" id="passwdtip"></span></td>
	</tr>
	<tr>
		<td><span style="color: red;">*</span>请确认密码：</td>
		<td><input type="password" id="passwd2" name="passwd2" onblur="checkPasswd2();"/></td>
		<td colspan="2"><span style="color: red;" id="passwdtip2"></span></td>
	</tr>
	<tr>
	   	<td colspan="2" align="center"><input type="button" onclick="save();" value="保存"/></td>
	   </tr>
   </table> 
 </s:form>  
</body>
</html>