<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>中心</title>

<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
        
        <link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript"> 

$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
</script>
<script type="text/javascript"> 
function resetPass(MManagerId){

if(confirm("确定重置密码吗？此操作不可撤销！")){
  
  $.post("resetPwd",
  {"MManagerId":MManagerId},
  function(data){
   if(data){
   alert("重置密码成功！");
   }else{
   
   alert("重置密码失败！");
   }
  }
  );
 


}

}
</script>
</head>

<body>
<s:form action="manage/manager_saveEdit" method="post" id="form">
<input type="hidden" id="MId" name="manager.MId" value="${manager.MId }"/>
<table  border="1" widtd="960" cellspacing="0" cellpadding="0" bordercolor="#D2CFCF" style="border-collapse: collapse">
		<tr>
		<td><span style="color: red;">*</span>管理员姓名：</td>
		<td><input type="text" id="MManagername" name="manager.MManagername" value="${manager.MManagername}"/></td>
		
	</tr>
	<tr>
		<td><span style="color: red;">*</span>管理员账号：</td>
		<td><input type="text" id="MManagerId" name="manager.MManagerId"  readonly="readonly"  value="${manager.MManagerId}"/></td>
		
	</tr>
	<tr>
		<td><span style="color: red;">*</span>管理员权限：</td>
		<td>
			<select id="roleId" name="roleDTO.roleId">
			<s:iterator value="roles" var="list">
				<option value="${list.roleId }" >${list.roleName }</option>
			</s:iterator>
			</select>
		</td>
	</tr>
	<tr>
	   	<td colspan="2" align="center"><input type="submit" value="保存"/><input type="button" value="重置密码" id="reset" onclick="resetPass('${manager.MManagerId}')"/></td>
	   <%-- 	<td colspan="2" align="center"><input type="button" value="重置密码"  onclick="reset('${manager.MManagerId}')"/></td> --%>
	 </tr>
   </table> 
   </s:form>
</body>
</html>