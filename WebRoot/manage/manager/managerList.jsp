<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>中心</title>

<link href="<%=basePath%>manage/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=basePath%>manage/js/jquery.js"></script>
<script type="text/javascript" src="<%=basePath%>manage/js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="<%=basePath%>manage/js/style.css" />
		<script type="text/javascript" src="<%=basePath%>manage/js/jquery.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>manage/js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=basePath%>manage/js/styles.css" />
        <link rel="stylesheet" type="text/css" href="<%=basePath%>manage/js/jquery.confirm.css" />
		<link rel="stylesheet" href="<%=basePath%>manage/css/mycss.css" type="text/css"/>
<script type="text/javascript" src="<%=basePath%>manage/js/ct.js" charset="utf-8"></script>
</head>
<script type="text/javascript"> 

$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
//新增管理员
$(".modiItem1").live("click", function () {
	window.location.href="<%=basePath%>manage/manager_add";
});
//修改角色
$(".modiItem").live("click", function () {
	var checkedNum = $("input[name='mid']:checked").length;
	if(checkedNum == 0) { 
			alert("请选择一项！"); 
			return false; 
		} else if(checkedNum  > 1){
			alert("请选择一个进行修改");
			return false;
		}else{
			var MId = $("input[name='mid']:checked")[0].value;
			window.location.href="<%=basePath%>manage/manager_editManager?MId="+MId;
		}
	
});
/* function deleteThis(){

 	$(".deleteItem").live("click", function () {
		var checkedNum = $("input[name='mid']:checked").length; 
		if(checkedNum == 0) { 
			alert("请选择至少一项！"); 
			return; 
		} 
         if (!confirm("确认要删除么？")) {
              return false;
         }else{
			  document.forms[0].action = "manage/manager_del";
    		document.forms[0].submit();
		}
    })
	} */
	

 	$(".sb").live("click", function () {
		var checkedNum = $("#keyword").attr("value");; 
		if(checkedNum == "请输入你要查询的关键词") { 
			alert("请输入你要查询的关键词！"); 
			return; 
		}else{
			var params={
				"keyword":checkedNum
			};
			 document.forms[0].action = "manage/manager_search?page=1";
    		document.forms[0].submit();
		} 
		
    });
</script>
</head>

<body>
<input type="hidden" id="deleteA" value="manager_del">
    <div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">用户管理</a></li>
    <li><a href="#">管理员</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" id="form1" theme="simple">   
 
 
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
	        <li rel="leanModal" class="modiItem1"  ><span><img src="<%=basePath%>manage/images/t01.png" width="24" height="24" /></span>添加</li>
	
			<li rel="leanModal1" class="modiItem" ><span><img src="<%=basePath%>manage/images/t02.png" width="24" height="24" /></span><a href="javascript:editManager()">修改</a></li>
	
	        <li class="deleteItem" ><span><img src="<%=basePath%>manage/images/t03.png" /></span><a href="javascript:deleteThis()">删除1</a></li>
	        <s:iterator value="modules" status="st">
	        	<s:property value="modules[#st.index][1]"/>--<s:property value="modules[#st.index][2]"/>
	        </s:iterator>
        </ul>

	<div class="ssk">
     
		   <div class="ssbk">
		      <div style="float:left;padding-left:30px;padding-top:1px"><input type="text" id="keyword" name="keyword" class="wbdd" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="请输入你要查询的关键词"></div>
		      <div style="float:right"><img src="<%=basePath%>manage/images/ss.jpg" onclick="searchThis();" class="sb"></div>
		      <div class="clear"></div>
		   </div>
   
  </div>
    
    </div>

    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th><input type='checkbox' id='checkall' name='checkall' /></th>
        <th>管理员账号</th>
        <th>管理员名称</th>
        <th>最后一次登录时间</th>
        <th>最后一次登录IP</th>
        <th>角色名称</th>
        </tr>
        </thead>
        <tbody>
  <s:iterator value="managers" var="list">      
        <tr>
			<td width="29" height="38" style="padding-left:10px">
			<input type="checkbox" name="mid" value=${list.MId } />
       	  </td>
       	  <td width="228" align="center" class="modify"><s:property value="#list.MManagerId" /></td>
        	<td width="228" align="center" class="modify"><s:property value="#list.MManagername" /></td>
        	<td width="228" align="center" class="modify"><s:property value="#list.MLastTime" /></td>
        	<td width="228" align="center" class="modify"><s:property value="#list.MLastIp" /></td>
        	<td width="228" align="center" class="modify"><s:property value="#list.ctRole.roleName" /></td>
        </tr> 
</s:iterator>       
        </tbody>
    </table>
    
    ${pages.pageStr}
    
   </s:form> 
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	
	

</body>

</html>