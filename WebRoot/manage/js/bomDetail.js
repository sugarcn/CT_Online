//删除Bom详情中的某个商品
function deleteThis(){
var bomId = $("#bomId").val();
 	$(".deleteItem").die().live("click", function () {
		var checkedNum = $("input[name='bomGoodsDTO.mid']:checked").length; 
		if(checkedNum == 0) { 
			alert("请选择至少一项！"); 
			return; 
		} 
         if (!confirm("确认要删除么？")) {
              return false;
         }else{
			  document.forms[0].action = "bom_delGoodsOfBomDetail?bomDTO.bomId="+bomId;
			  document.forms[0].submit();
		}
    });
}

//删除Bom中心详情中的某个商品
function deleteThisBC(){
var bomId = $("#bomId").val();
 	$(".deleteItemBC").die().live("click", function () {
		var checkedNum = $("input[name='bomGoodsDTO.mid']:checked").length; 
		if(checkedNum == 0) { 
			alert("请选择至少一项！"); 
			return; 
		} 
         if (!confirm("确认要删除么？")) {
              return false;
         }else{
			  document.forms[0].action = "bom_delGoodsOfBomDetailBC?bomDTO.bomId="+bomId;
			  document.forms[0].submit();
		}
    });
}
//搜索
function searchThis(){
	var bomId = $("#bomId").val();
 	$(".searchItem").live("click", function () {
		var checkedNum = $("#keyword").attr("value");; 
		if(checkedNum == "") { 
			alert("请输入你要查询的关键词！"); 
			return; 
		}else{
			var params={
				"keyword":checkedNum
			}
			 document.forms[0].action = "bom_searchGoodsOfBomDetail?bomGoodsDTO.page=1&bomGoodsDTO.bomId="+bomId;
    		document.forms[0].submit();
		} 
		
    });
}

//BOM中心搜索
function searchThisBC(){
	var bomId = $("#bomId").val();
 	$(".searchItemBC").live("click", function () {
		var checkedNum = $("#keyword").attr("value");; 
		if(checkedNum == "") { 
			alert("请输入你要查询的关键词！"); 
			return; 
		}else{
			var params={
				"keyword":checkedNum
			}
			 document.forms[0].action = "bom_searchGoodsOfBomDetailBC?bomGoodsDTO.page=1&bomGoodsDTO.bomId="+bomId;
    		document.forms[0].submit();
		} 
		
    });
}

//更新Pack
function getPack(bomGoodsId){
	
	var pack = $("#"+bomGoodsId).val();
	//var GId = $("#GId").text();
	$.post("bom_savePack",{"bomGoodsDTO.pack":pack,"bomGoodsDTO.bomGoodsId":bomGoodsId});
}

//数量加1
function addNum(num){
	var goodsNum = $("#"+num).val();
	var newgoodsNum = Number(goodsNum) + 1;
	$.post("bom_updateGoodsNum",{"bomGoodsDTO.goodsNum":newgoodsNum,"bomGoodsDTO.GId":num},function(data){
		if(data == "success"){
			$("#"+num).val(newgoodsNum);
		}
		
	});
}
//数量减1
function subNum(num){
	var goodsNum = $("#"+num).val();
	if(goodsNum <= 1){
		alert("数量不能小于1");
		return ;
	}
	var newgoodsNum = Number(goodsNum) - 1;
	$.post("bom_updateGoodsNum",{"bomGoodsDTO.goodsNum":newgoodsNum,"bomGoodsDTO.GId":num},function(data){
		if(data == "success"){
			$("#"+num).val(newgoodsNum);
		}
		
	});
}
//更新数量
function updateNum(num){
	var goodsNum = $("#"+num).val();
	if(goodsNum < 1){
		alert("数量不能小于1");
		return ;
	}
	$.post("bom_updateGoodsNum",{"bomGoodsDTO.goodsNum":goodsNum,"bomGoodsDTO.GId":num},function(data){
		if(data == "success"){
			$("#"+num).val(goodsNum);
		}
		
	});
}
//详情新增商品
function add(){
	flag = setGidByGname();
	if(flag){
		var GId = $("#GId").val();
		var bomId = $("#bomId").val();
		document.forms[0].action = "bom_addGoodsOfBomDetail?bomGoodsDTO.bomId="+bomId+"&bomGoodsDTO.GId="+GId;
		document.forms[0].submit();
	}
	
}

//BOM中心详情新增商品
function addBC(){
	flag = setGidByGname();
	if(flag){
		var GId = $("#GId").val();
		var bomId = $("#bomId").val();
		document.forms[0].action = "bom_addGoodsOfBomDetailBC?bomGoodsDTO.bomId="+bomId+"&bomGoodsDTO.GId="+GId+"&bId="+bomId;
		document.forms[0].submit();
	}
	
}

//自动弹出下拉菜单
function getGoodsByGname(){
	var GName = $("#GName").val();
	$(".bomdetail_shpmch").html("");
	$.post("goods_getGoodsByGname",{"goodsDTO.GName":GName},function(data){
		//data = eval(data);
		var html = "";
		$(".bomdetail_shpmch").css("display","block");
		for(var i=0;i<data.length;i++){
			html = html + "<li onclick=\"javascript:setGid("+data[i][0]+",\'"+data[i][1]+"\')\"><span >"+data[i][1]+"</span></li>";
			$(".bomdetail_shpmch").html(html);
		}
	});
}
//设置下拉菜单的商品Gid
function setGid(Gid,Gname){
	$("#GId").val(Gid);
	$("#GName").val(Gname);
	
	$(".bomdetail_shpmch").css("display","none");
}
//直接通过Gname设置Gid
function setGidByGname(){
	flag = true;
	$(".bomdetail_shpmch").css("display","none");
	var GName = $("#GName").val();
	$.ajaxSetup({async:false});
	$.post("goods_getOneGoodsByGname",{"goodsDTO.GName":GName},function(data){
		if(data == "error"){
			alert("你输入的商品不存在！");
			flag = false;
		}else {
			$("#GId").val(data[0]);
		}
		
	});
	return flag;
}