//弹出新增页
function add(){
	var UId = $("#uid").val();
	window.location.href="manage/bom_add?userDTO.UId="+UId;
}
//Bom中心新增bom
function addBomCenter(){
	window.location.href="bom_addBomCenter";
}
//修改Bom
function editBom(){
	var UId = $("#uid").val();
	var checkedNum = $("input[name='mid']:checked").length;
	if(checkedNum == 0) { 
			alert("请选择一项！"); 
			return false; 
		} else if(checkedNum  > 1){
			alert("请选择一个进行修改");
			return false;
		}else{
			var MId = $("input[name='mid']:checked")[0].value;
			window.location.href="manage/bom_editBom?bomDTO.mid="+MId+"&userDTO.UId="+UId;
		}
	
}

//修改Bom中心
function editBomBomCenter(){
	var checkedNum = $("input[name='mid']:checked").length;
	if(checkedNum == 0) { 
			alert("请选择一项！"); 
			return false; 
		} else if(checkedNum  > 1){
			alert("请选择一个进行修改");
			return false;
		}else{
			var MId = $("input[name='mid']:checked")[0].value;
			window.location.href="manage/bom_editBomBomCenter?bomDTO.mid="+MId;
		}
	
}

//删除Bom
function deleteThis(){

 	$(".deleteItem").die().live("click", function () {
		var checkedNum = $("input[name='mid']:checked").length; 
		if(checkedNum == 0) { 
			alert("请选择至少一项！"); 
			return; 
		} 
         if (!confirm("确认要删除么？")) {
              return false;
         }else{
			  document.forms[0].action = "manage/bom_del";
			  document.forms[0].submit();
		}
    });
}
//删除Bom中心
function deleteThisBC(){

 	$(".deleteItemBC").die().live("click", function () {
		var checkedNum = $("input[name='mid']:checked").length; 
		if(checkedNum == 0) { 
			alert("请选择至少一项！"); 
			return; 
		} 
         if (!confirm("确认要删除么？")) {
              return false;
         }else{
			  document.forms[0].action = "manage/bom_deleteThisBomCenter";
			  document.forms[0].submit();
		}
    });
}

//搜索
function searchThis(){

 	$(".searchItem").live("click", function () {
		var checkedNum = $("#keyword").attr("value");; 
		if(checkedNum == "") { 
			alert("请输入你要查询的关键词！"); 
			return; 
		}else{
			var params={
				"keyword":checkedNum
			}
			 document.forms[0].action = "manage/bom_search?page=1";
    		document.forms[0].submit();
		} 
		
    });
	}
//搜索Bom中心
function searchThisBC(){

 	$(".searchItemBC").live("click", function () {
		var checkedNum = $("#keyword").attr("value");; 
		if(checkedNum == "") { 
			alert("请输入你要查询的关键词！"); 
			return; 
		}else{
			var params={
				"keyword":checkedNum
			}
			 document.forms[0].action = "manage/bom_searchBC?page=1";
    		document.forms[0].submit();
		} 
		
    });
	}

//复制Bom
function copyBom(){
	$(".copyItem").die().live("click", function () {
		var checkedNum = $("input[name='mid']:checked").length; 
		if(checkedNum == 0) { 
			alert("请选择至少一项！"); 
			return; 
		} 
         if (!confirm("确认要复制BOM吗？")) {
              return false;
         }else{
			  document.forms[0].action = "manage/bom_copyBom";
			  document.forms[0].submit();
		}
    });
}
//复制Bom中心
function copyBomBC(){
	$(".copyItemBC").die().live("click", function () {
		var checkedNum = $("input[name='mid']:checked").length; 
		if(checkedNum == 0) { 
			alert("请选择至少一项！"); 
			return; 
		} 
         if (!confirm("确认要复制BOM吗？")) {
              return false;
         }else{
			  document.forms[0].action = "manage/bom_copyBomBC";
			  document.forms[0].submit();
		}
    });
}
//加入bom中心
function addToBomCenter(){
		var checkedNum = $("input[name='mid']:checked").length; 
		if(checkedNum == 0) { 
			alert("请选择至少一项！"); 
			return; 
		} 
         if (!confirm("确认要加入BOM中心吗？")) {
              return false;
         }else{
			  document.forms[0].action = "manage/bom_addToBomCenter";
			  document.forms[0].submit();
		}
}
//Bom列表
function goBomList(UId){
	window.location.href="mange/bom_goBomList?userDTO.UId="+UId+"&UUser="+UUser;
}
//Bom详细
function goBomDetail(bomId) {
	var UId = $("#uid").val();
	window.location.href="manage/bom_goBomDetail?bomDTO.bomId="+bomId+"&bomDTO.UId="+UId;
}
//Bom中心详细
function goBomBCDetail(bomId) {
	window.location.href="manage/bom_goBomBCDetail?bomDTO.bomId="+bomId;
}