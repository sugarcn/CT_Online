<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'goods_tieA.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="js/ueditor/lang/zh-cn/zh-cn.js"></script>
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<script type="text/javascript">
function setContent() {
		var detl = document.getElementById("GDetail").value;
        UE.getEditor('editor').setContent(detl);
    }
function getContent() {
        var value = UE.getEditor('editor').getContent();
        $("#GDetail").val(value);
        document.forms[0].action = "Edit_goods";
    	document.forms[0].submit();
        }
</script>
  </head>
  
  <body onload="setContent()">
    <form action="tieA_goods" name="submitform" method="post">
   		<p>为下列商品编辑描述：</p><br/>
    	
    		<p>${goods.GId }&nbsp;${goods.GName }</p><br/>
    		<input type="hidden" name="GId" value="${goods.GId }" />
    		<input type="hidden" id="GDetail" name="GDetail" value="${goodsDetail.GDetail}" />
    		<input type="hidden" name="detlId" value="${goodsDetail.detlId}" />
		<div>
		<script type="text/javascript">var ue = UE.getEditor('editor');</script>
			<script id="editor" type="text/plain" style="width:1024px;height:500px;"></script>
		</div>
		<input type="button" value="保存" onclick="getContent()" id="tiebtn">
		<input type="button" onclick="location.href='javascript:history.go(-1);'" value="返回" />
    </form>
  </body>
</html>
