<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
		<script type="text/javascript" src="js/goTab.js"></script>
<!-- 设定替代 -->
<script type="text/javascript">
$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
</script>
<script type="text/javascript">
function toedit(){
	document.forms[0].action = "toTieA_goods";
  document.forms[0].submit();
}
</script>
<script type="text/javascript" src="js/goodsReplace.js"></script>


</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">商品管理</a></li>
    <li><a href="#">商品管理</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
    <li><a href="javascript:goTab('${GId }');" >编辑详情</a></li> 
    <li><a href="javascript:goTab2('${GId }');">绑定资料</a></li> 
    <li><a href="javascript:goTab3('${GId }');">编辑属性</a></li>
    <li><a href="javascript:goTab4('${GId }');">设定关联</a></li>
    <li><a href="#tab5"class="selected">设定替代</a></li>
    <li><a href="javascript:goTab6('${GId }');">管理库存</a></li>
  	</ul>
    </div> 
   
	
	<!-- 设置替代 -->
  	<div id="tab5" class="tabson">
          <s:form method="post"  name="form1" theme="simple">   
    <div class="rightinfo">
    
    <!--<div class="tools">
    	<ul class="toolbar">
        <li><a id="go" rel="leanModal" name="signup" href="javascript:history.go(-1);" ><span><img src="images/t01.png" width="24" height="24" /></span>返回</a></li>
		<li class="click"><span><img src="images/t04.png" /></span>添加替代</li>
        </ul>

    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>商品ID</th>
        <th>商品名称</th>
        <th>商品货号</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        	<td width="228" align="center" class="modify">${goods.GId }</td>
        	<td width="212" align="center" class="modify">${goods.GName }</td>
        	<td width="212" align="center" class="modify">${goods.GSn }</td>
        </tr> 
        </tbody>
    </table>
    已绑定的替代：
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>商品ID</th>
        <th>商品名称</th>
        <th>商品货号</th>
        <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="repList" var="list">
        <tr>
        	<td width="228" align="center" class="modify">${list.replaceGId }</td>
        	<td width="212" align="center" class="modify">${list.GName }</td>
        	<td width="212" align="center" class="modify">${list.GSn }</td>
        	<td width="212" align="center" class="modify"><a href="<%=request.getContextPath() %>/unReplace_goods?GId=${goods.GId }&linkGId=${list.replaceGId }" >删除替代</a></td>
        </tr> 
        </s:iterator>
        </tbody>
    </table>
    添加新的替代：<br/>
    <input type="checkbox" id="doubleLink" name="doubleLink"  />双向关联<br/> -->
<div class="h_b4">
	<div class="h_b4_a"><input type="hidden" id="dblk" name="dblk" value="0" /></div>
	<div class="h_b4_b"><input type="text" id="keyword" name="keyword" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="请输入你要查询的关键词" />
</div>
<div class="h_b4_d"><input type="checkbox" name="m" value="" /><span>双向替代</span></div>
	<div class="h_b4_c"><input type="button" value="确定"  id="search"/></div>
</div>    

<table class="tablelist">
<thead>
    	<tr align="center">

        <th>商品ID</th>
        <th>商品名称</th>
        <th>商品货号</th>
        <th>操作</th>        
        </tr>
        </thead>
        <tbody>
        <s:iterator value="goodsList" var="list">
        <tr>

        <td width="20%" align="center" class="modify">${list.GId }</td>
        	<td width="30%" align="center" class="modify">${list.GName }</td>
        	<td width="25%" align="center" class="modify">${list.GSn }</td>
          <td width="25%" align="center" class="modify">
        <span class="h_cz"><a href="/">修改</a>|<a href="/">删除</a></span>s
        </td>      	
        	
        	</tr>
        	</s:iterator>
        </tbody>
</table>

     ${pages.pageStr}
     
   </s:form> 
	</div>
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    
    
    
    
    </div>

</div>
</body>

</html>
