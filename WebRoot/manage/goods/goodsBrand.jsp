<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/brand.js" charset="utf-8"></script>
 <script type="text/javascript"
	src="<%=basePath%>manage/js/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css"
	href="<%=basePath%>manage/js/jquery.fancybox.css?v=2.1.5"
	media="screen" />
</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">商品管理</a></li>
    <li><a href="#">品牌管理</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" id="form1" theme="simple">   
 
 <input type="hidden" id="deleteA" value="delete_goodsBrand">
 <input type="hidden" id="findA" value="find_goodsBrand">
 <input type="hidden" id="searchA" value="search_goodsBrand">
 <input type="hidden" id="updateA" value="update_goodsBrand"> 
 
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li rel="leanModal" name="signup" href="#signup" onclick=""><span><img src="images/t01.png" width="24" height="24" /></span>添加</li>

		<li rel="leanModal1" class="modiItem" href="#signup1"><span><img src="images/t02.png" width="24" height="24" /></span>修改</li>

        <li class="deleteItem" ><span><img src="images/t03.png" /></span>删除</li>
        </ul>

	<div class="ssk">
     
		   <div class="ssbk">
		      <div style="float:left;padding-left:30px;padding-top:1px"><input type="text" id="keyword" name="keyword" class="wbdd" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="请输入你要查询的关键词"></div>
		      <div style="float:right"><img src="images/ss.jpg" class="searchItem"></div>
		      <div class="clear"></div>
		   </div>
   
  </div>
    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th><input type='checkbox' id='checkall' name='checkall' /></th>
        <th>品牌logo</th>
        <th>品牌名称</th>
        <th>URL</th>
        </tr>
        </thead>
        <tbody>
  <s:iterator value="brands" var="list">      
        <tr>
			<td width="29" height="38" style="padding-left:10px">
			<input type="checkbox" name="mid" value=${list.BId } />
       	  </td>
             <td width="212" align="center" class="modify">
				<a class="fancybox" href="<%=basePath%>${list.BLogo }" data-fancybox-group="gallery">
					<img src="<%=basePath%><s:property value="#list.BLogo"/>"style="width: 25px;height: 25px" />
				</a>
			</td>  	      
        	<td width="228" align="center" class="modify"><s:property value="#list.BName"/></td>
        	<td width="228" align="center" class="modify"><s:property value="#list.BUrl"/></td>
        </tr> 
  </s:iterator>       
        </tbody>
    </table>
    
   	${pages.pageStr}
    
   </s:form> 
    
	
	<div id="signup">
			<div id="signup-ct">
				<div id="signup-header">
					<h2>新建品牌</h2>
					<a class="modal_close" href="#"></a>
				</div>
				
			<s:form action="file" id="form" method="post" enctype="multipart/form-data">
				  <div class="txt-fld">
				    <p style=" padding-top:8px;"><label for="">品牌名称:</label>
				    <input id="" class="good_input" name="BName" type="text" /></p>
				     <p style=" padding-top:8px;"><label for="">品牌logo:</label>
				    <input id="" class="good_input" style=" border:0; background:#fff;" name="file" type="file" /></p>
				    <p style=" padding-top:8px;"> <label for="">品牌介绍:</label>
<!-- 				    <input id="" class="good_input" name="BDesc" type="text"TextMode="MultiLine" /> -->
					<textarea style="border:1px solid #ccc;"  id="" class="good_input" name="BDesc" rows="5" cols="40"></textarea></p>
				     <p style=" padding-top:8px;"><label for="">URL:</label>
				    <input id="" class="good_input" name="BUrl" type="text" /></p>
				     <p style=" padding-top:8px;"><label for="">排序:</label>
				    <input id="" class="good_input" name="sortOrder" type="text" /></p>
				   
				    <p style=" padding-top:8px;"><label for="">是否展示:</label>				   
				     <span class="span1"style="float:left; width:120PX;padding-top:8px; "><input id=""style="float:left; width:50PX; padding:0; margin:0;" class="good_input1" name="isShow" type="radio" checked="checked" value="1"/><span style="float:left; padding-left:8px;">是</span></span>
				    <span class="span1"style="float:left; width:120PX; padding-top:8px;"><input id=""style="float:left;width:50PX; " class="good_input1" name="isShow" type="radio"  value="0"/><span style="float:left; padding-left:8px;">否</span></span></p>
				    
				    
				  </div>

				  <div class="btn-fld">
                  <div id="tipHtml" style="color:red;float:left">
                    </div>
				  <button type="submit" onclick="return checkThis(this.form,'file',0);">确定</button>
</div>
				 </s:form>
			</div>
		</div>
		
		<div id="signup1" class="brand_xiugai">
			<div id="signup-ct" >
            <div id="signup-header">
					<h2>修改品牌</h2>
					<a class="modal_close" href="#"></a>
				</div>
              <form method="post" enctype="multipart/form-data">
              <div id="signupmodi">
                    </div>
                    
              <div class="btn-fld">
              <div id="tipHtmlModi" style="color:red;float:left">
                    </div>
				  <button type="submit" onclick="return checkThis(this.form,'file1',1);">确定</button>
</div>
				 </form>
                 </div>
		</div>
	
	
	
    
    
    
    
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	
	

</body>

</html>
