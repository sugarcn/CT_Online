<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="<%=basePath %>manage/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=basePath %>manage/js/jquery.js"></script>
<script type="text/javascript" src="<%=basePath %>manage/js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="<%=basePath %>manage/js/style.css" />
		<script type="text/javascript" src="<%=basePath %>manage/js/jquery.min.js"></script>
		<script type="text/javascript" src="<%=basePath %>manage/js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=basePath %>manage/js/styles.css" />
        <link rel="stylesheet" type="text/css" href="<%=basePath %>manage/js/jquery.confirm.css" />
		<link rel="stylesheet" href="<%=basePath %>manage/css/mycss.css" type="text/css"/>
<script type="text/javascript" src="<%=basePath %>manage/js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="<%=basePath %>manage/js/bomDetail.js" charset="utf-8"></script>
<script type="text/javascript">
			</script>

</head>


<body>
 <s:form method="post"  name="form1" id="form1" theme="simple">   

	<div class="place">
		<span>位置：</span>
		<ul class="placeul">
			<li><a href="#">客户管理</a></li>
			<li><a href="#">BOM列表</a></li>
			<li><a href="#">BOM详情</a></li>
 			<li>当前所在BOM：${bom.bomTitle }</li>
		</ul>
	</div>
 <input type="hidden" id="deleteA" value="delete_depot">
 <input type="hidden" id="findA" value="find_depot">
 <input type="hidden" id="updateA" value="update_depot"> 
 
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
    	 <input type="hidden" id="bomId" value="${bom.bomId}"/>
    	 <input type="hidden" id="GId"/>
    	 <div class="shuru_f">
    	          商品名称：<input type="text" id="GName" onkeyup="getGoodsByGname();" class="bomdetail_shp"/>
    	          <ul class="bomdetail_shpmch" style="display:none;"></ul>   	          	          
    	     <li class="bomdetail_tianjia"><span><img src="<%=basePath %>manage/images/t01.png"  width="24" height="24" /></span><a href="javascript:;" onclick="addBC();">添加</a></li>
		     <%-- <li rel="leanModal1" class="modiItem" href="#signup1" onclick="lookThis();"><span><img src="<%=basePath %>manage/images/t02.png" width="24" height="24" /></span>修改</li> --%>
             <li class="bomdetail_tianjia"><span><img src="<%=basePath %>manage/images/t03.png" onclick="deleteThisBC();" class="deleteItemBC"/></span>删除</li>  
    	 </div>
         
        
        </ul>

	<div class="ssk">
     
		   <div class="ssbk">                                           
		      <div style="float:left;padding-left:30px;padding-top:1px"><input type="text" id="keyword" name="bomGoodsDTO.keyword" class="wbdd" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="${keyword}"></div>
		      <div style="float:right"><img src="<%=basePath %>manage/images/ss.jpg" onclick="searchThisBC();" class="searchItemBC"></div>
		      <div class="clear"></div>
		   </div>
       
   
  </div>
    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th><input type='checkbox' id='checkall' name='checkall' /></th>
        <th>商品图片</th>
        <th>商品编号</th>
        <th>商品名称</th>
        <th>商品品牌</th>
        <th>分类名称</th>
        <th>商品价格</th>
        <th>包装类型</th>
        <th>数量</th>
        </tr>
        </thead>
        <tbody>
   <s:iterator value="bomGoodsList" var="list">     
        <tr>
			<td width="29" height="38" style="padding-left:10px">
			<input type="checkbox" name="bomGoodsDTO.mid" value=${list.bomGoodsId } />
       	  </td>
        	<td width="228" align="center" class="modify">img</td>
        		<td width="212" align="center" class="modify"><s:property value="#list.goods.GId"/></td>
        		<td width="212" align="center" class="modify"><s:property value="#list.goods.GName"/></td>
        		<td width="212" align="center" class="modify"><s:property value="#list.goods.goodsBrand.BName"/></td>
        		<td width="212" align="center" class="modify"><s:property value="#list.goods.goodsCategory.CName"/></td>
        		<s:if test="#list.goods.promotePrice == null">
        			<td width="212" align="center" class="modify"><s:property value="#list.goods.shopPrice"/></td>
        		</s:if>
        		<s:else>
        			<td width="212" align="center" class="modify"><s:property value="#list.goods.promotePrice"/></td>
        		</s:else>
        		<s:if test="#list.pack ==null ">
        			<td>
        			<select id="${list.bomGoodsId }" name="pack" onchange="getPack(${list.bomGoodsId });">
        				<option>--请选择--</option>
        				<option value="${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }">${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }</option>
        				<option value="${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }">${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }</option>
        				<option value="${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }">${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }</option>
        			</select>
        			</td>
        		</s:if>
        		<s:else>
        			<td width="212" align="center" class="modify">
						<c:set var="v1" value="${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }"/>
        				<c:set var="v2" value="${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }"/>
        				<c:set var="v3" value="${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }"/>
        				<c:if test="${list.pack == v1 }">
        					<select id="${list.bomGoodsId }" name="pack" onchange="getPack(${list.bomGoodsId });">
		        				<option value="${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }">${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }</option>
        						<option value="${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }">${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }</option>
		        				<option selected="selected">${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }</option>
        					</select>
        				</c:if>
        				<c:if test="${list.pack == v2}">
        					<select id="${list.bomGoodsId }" name="pack" onchange="getPack(${list.bomGoodsId });">
		        				<option value="${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }">${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }</option>
		        				<option selected="selected">${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }</option>
		        				<option value="${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }">${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }</option>
        					</select>
        				</c:if>
        				<c:if test="${list.pack == v3}">
        					<select id="${list.bomGoodsId }" name="pack" onchange="getPack(${list.bomGoodsId });">
		        				<option selected="selected">${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }</option>
		        				<option value="${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }">${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }</option>
        						<option value="${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }">${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }</option>
        					</select>
        				</c:if>
        			</td>
        		</s:else>
        		<s:if test="#list.goodsNum == null">
        			<td width="212" align="center" class="modify">
        				<a href="javascript:subNum(${list.goods.GId })">-</a>
        				<input style="width: 30px" id="${list.goods.GId}" type="text" value="1"  onkeyup="updateNum(${list.goods.GId });"/>
        				<a href="javascript:addNum(${list.goods.GId })">+</a>
        			</td>
        		</s:if>
        		<s:else>
        			<td width="212" align="center" class="modify">
        				<a href="javascript:subNum(${list.goods.GId })">-</a>
        				<input style="width: 30px" id="${list.goods.GId }" type="text" value="${list.goodsNum}" onkeyup="updateNum(${list.goods.GId });"/>
        				<a href="javascript:addNum(${list.goods.GId })">+</a>
        			</td>
        		</s:else>
        	</tr>
        </s:iterator>
    </thead>
 </table>
    
   
  <div class="pagin">
    <s:property value="page.getTotalPage()"/>
    	<div class="message"  style="float:left">共<i class="blue">${pages.totalCount}</i>条记录，当前显示第&nbsp;<i class="blue">${pages.currentPage}&nbsp;</i>页</div>

    	
<s:bean name="org.apache.struts2.util.Counter" id="counter">   
   <s:param name="first" value="1" />   
   <s:param name="last" value="#request.pages.totalPage" />   
   <input type="hidden" id="totalpage" value="${pages.totalPage }" />
   <ul class="paginList">
   <s:if test="#request.pages.currentPage==1 ">
        <li class="paginItem"><a href="javascript:;"><span class="pagepre"></span></a></li>
    </s:if>
    <s:else>
    	<li class="paginItem"><a href="?page=${pages.currentPage-1}&keyword=${keyword}"><span class="pagepre1"></span></a></li>
    </s:else>
   <s:iterator status="count">   
     
     <s:if test="#request.pages.currentPage!=current-1">
      	<li class="paginItem"><a href="?page=<s:property value='#count.index+1'/>&keyword=${keyword}"><s:property value="#count.index+1"/> </a></li>   	
     </s:if>
     <s:else>
	     <li class="paginItem" style="color:red"><a href="?page=<s:property value='#count.index+1'/>&keyword=${keyword}"><s:property value="#count.index+1"/> </a></li> 
     </s:else>
      
   </s:iterator>
   <s:if test="#request.pages.currentPage==#request.pages.totalPage">  
    <li class="paginItem"><a href="javascript:void();"><span class="pagenxt1"></span></a></li>
    </s:if>
    <s:else>
    <li class="paginItem"><a href="?page=${pages.currentPage+1}&keyword=${keyword}"><span class="pagenxt"></span></a></li>
    </s:else>
<li class="paginItem" ><input type="text" id="getpage" class="dfinputgo" /><input type="button" id="pagego" value="GO" class="go"/></li>
        </ul>
		
	 
</s:bean>

    </div>
   </s:form> 
    
	
	
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	
	

</body>

</html>
