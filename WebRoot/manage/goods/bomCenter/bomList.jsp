<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="<%=basePath %>manage/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=basePath %>manage/js/jquery.js"></script>
<script type="text/javascript" src="<%=basePath %>manage//js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<%=basePath %>manage/js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="<%=basePath %>manage/js/style.css" />
		<script type="text/javascript" src="<%=basePath %>manage/js/jquery.min.js"></script>
		<script type="text/javascript" src="<%=basePath %>manage/js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=basePath %>manage/js/styles.css" />
        <link rel="stylesheet" type="text/css" href="<%=basePath %>manage/js/jquery.confirm.css" />
		<link rel="stylesheet" href="<%=basePath %>manage/css/mycss.css" type="text/css"/>
<script type="text/javascript" src="<%=basePath %>manage/js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="<%=basePath %>manage/js/bom.js" charset="utf-8"></script>
<script type="text/javascript">
$(document).ready(function(){
var flag = "${flag}";
if(flag == "1"){
   $(".sbs").show();
}else{
$(".sbs").hide();
}



});

//复制Bom1
		function copyBomTwo(){
			
			$(".copyItem").die().live("click", function () {
				var checkedNum = $("input[name='mid']:checked").length; 
				if(checkedNum == 0) { 
					alert("请选择至少一项！"); 
					return; 
				} 
				if (!confirm("确认要复制BOM吗？")) {
					return false;
				}else{
					document.forms[0].action = "manage/bom_copyBomTwo";
					document.forms[0].submit();
				}
			});
		}
		
		
</script>
</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">BOM中心</a></li>
	<li><a href="#">BOM列表</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" id="form1" theme="simple">   
 
 <input type="hidden" id="deleteA" value="bom_deleteThisBomCenter?bomDTO.bomId="+mid>
 <input type="hidden" id="searchA" value="bom_searchBC">
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li rel="leanModal" name="signup" href="#signup" onclick=""><span><img src="images/t01.png" width="24" height="24" /></span>添加</li>

		<li rel="leanModal1" class="modiItem" href="#signup1"><span><img src="images/t02.png" width="24" height="24" /></span>修改</li>

                                                                                                                    			
         <li  class="sbs" ><span><img src="images/t01.png" width="24" height="24"  />加入BOM中心</span></li>
        <li class="deleteItembom" ><span><img src="images/t03.png" /></span>删除</li>
        </ul>

	<div class="ssk">
     
		   <div class="ssbk">
		      <div style="float:left;padding-left:30px;padding-top:1px"><input type="text" id="keyword" name="keyword" class="wbdd" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="${keyword}"></div>
		      <div style="float:right"><img src="images/ss.jpg" class="searchItem"></div>
		      <div class="clear"></div>
		   </div>
   
  </div>
    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th><input type='checkbox' id='checkall' name='checkall' /></th>
        <th>BOM名称</th>
        <th>创建时间</th>
        <th>BOM介绍</th>
        </tr>
        </thead>
        <tbody>
   <s:iterator value="bomList" var="list">    
       <s:if test="#list.isHot == 1">
    	<tr>
    		<td width="29" height="38" style="padding-left:10px">
				<input type="checkbox" name="mid" value=${list.bomId } />
       	  	</td>
       	  	<td  width="228" align="center" class="modify"><a href="javascript:goBomBCDetail(${list.bomId })"><s:property value="#list.bomTitle"/></a></td>
        	<td width="212" align="center" class="modify"><s:property value="#list.bomTime"/></td>
        	<td width="212" align="center" class="modify"><s:property value="#list.bomDesc"/></td>
    	</tr>
    	</s:if>
    	<s:else>
    		<tr>
    		<td width="29" height="38" style="padding-left:10px">
				<input type="checkbox" name="mid" value=${list.bomId } />
       	  	</td>
       	  	<td  width="228" align="center" class="modify"><a href="javascript:goBomBCDetail(${list.bomId })"><s:property value="#list.bomTitle"/></a></td>
        	<td width="212" align="center" class="modify"><s:property value="#list.bomTime"/></td>
        	<td width="212" align="center" class="modify"><s:property value="#list.bomDesc"/></td>
    	</tr>
    	</s:else>
    </s:iterator>
    </tbody>
    </table>
  <div>
 <input type="button" src="images/buttonbg.png" value="生成订单" onclick=""  style="background:#108fc6; margin-top:15px; width:80px; height:30px; line-height:30px; color:#fff; border-radius:5px;"/>
 <input type="button" value="复制BOM" onclick="copyBomTwo();" class="copyItem"  style="background:#84c610; margin-top:15px; width:80px; height:30px; line-height:30px; color:#fff; border-radius:5px;"/>
 </div>
   
    <div class="pagin">
    <s:property value="page.getTotalPage()"/>
    	<div class="message"  style="float:left">共<i class="blue">${pages.totalCount}</i>条记录，当前显示第&nbsp;<i class="blue">${pages.currentPage}&nbsp;</i>页</div>

    	
<s:bean name="org.apache.struts2.util.Counter" id="counter">   
   <s:param name="first" value="1" />   
   <s:param name="last" value="#request.pages.totalPage" />   
   <input type="hidden" id="totalpage" value="${pages.totalPage }" />
   <ul class="paginList">
   <s:if test="#request.pages.currentPage==1 ">
        <li class="paginItem"><a href="javascript:;"><span class="pagepre"></span></a></li>
    </s:if>
    <s:else>
    	<li class="paginItem"><a href="?page=${pages.currentPage-1}&keyword=${keyword}"><span class="pagepre1"></span></a></li>
    </s:else>
   <s:iterator status="count">   
     
     <s:if test="#request.pages.currentPage!=current-1">
      	<li class="paginItem"><a href="?page=<s:property value='#count.index+1'/>&keyword=${keyword}"><s:property value="#count.index+1"/> </a></li>   	
     </s:if>
     <s:else>
	     <li class="paginItem" style="color:red"><a href="?page=<s:property value='#count.index+1'/>&keyword=${keyword}"><s:property value="#count.index+1"/> </a></li> 
     </s:else>
      
   </s:iterator>
   <s:if test="#request.pages.currentPage==#request.pages.totalPage">  
    <li class="paginItem"><a href="javascript:void();"><span class="pagenxt1"></span></a></li>
    </s:if>
    <s:else>
    <li class="paginItem"><a href="?page=${pages.currentPage+1}&keyword=${keyword}"><span class="pagenxt"></span></a></li>
    </s:else>
<li class="paginItem" ><input type="text" id="getpage" class="dfinputgo" /><input type="button" id="pagego" value="GO" class="go"/></li>
        </ul>
		
	 
</s:bean>

    </div>
   </s:form> 
    
	
	<div id="signup">
			<div id="signup-ct">
				<div id="signup-header">
					<h2>新建BOM</h2>
					<a class="modal_close" href="#"></a>
				</div>
				
			<s:form >
				  <div class="txt-fld">
				    <label for="">BOM名称:</label>
				    <input id="" class="good_input" name="bomTitle" type="text" style=" margin-bottom:13px;" />
				     <label for="">BOM介绍:</label>
				    <input id="" class="good_input" name="bomDesc" type="text" />
				     
				  </div>

				  <div class="btn-fld">
                  <div id="tipHtml" style="color:red;float:left">
                    </div>
				  <button type="submit" onclick="return checkThis(this.form,'bom_saveAddBomCenter',0);">确定</button>
</div>
				 </s:form>
			</div>
		</div>
		
		<div id="signup1">
			<div id="signup-ct">
            <div id="signup-header">
					<h2>修改BOM</h2>
					<a class="modal_close" href="#"></a>
				</div>
              <form>
              <div id="signupmodi">
                    </div>
                    
              <div class="btn-fld">
              <div id="tipHtmlModi" style="color:red;float:left">
                    </div>
				  <button type="submit" onclick="return checkThis(this.form,'update_bom',1);">确定</button>
</div>
				 </form>
                 </div>
		</div>
	
	
	
    
    
    
    
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	
	

</body>

</html>
