<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>
<script type="text/javascript" src="js/goTab.js"></script>
<!-- 编辑属性 -->
<script type="text/javascript">
$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
</script>
<script type="text/javascript">
function toedit(){
	document.forms[0].action = "toTieA_goods";
  document.forms[0].submit();
}
</script>
<script type="text/javascript" src="js/goodsAttribute.js"></script>

</head>


<body onload="setContent()">

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">商品管理</a></li>
    <li><a href="#">商品管理</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
    <li><a href="javascript:goTab('${GId }');" >编辑详情</a></li> 
    <li><a href="javascript:goTab2('${GId }');">绑定资料</a></li> 
    <li><a href="#tab3" class="selected">编辑属性</a></li>
    <li><a href="javascript:goTab4('${GId }');">设定关联</a></li>
    <li><a href="javascript:goTab5('${GId }');">设定替代</a></li>
    <li><a href="javascript:goTab6('${GId }');">管理库存</a></li>
  	</ul>
    </div> 
 
    <!-- 编辑属性 -->
  	<div id="tab3" class="tabson">
           <s:form method="post"  name="form1" theme="simple">   
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li><a id="go" rel="leanModal" name="signup" href="javascript:history.go(-1);" ><span><img src="images/t01.png" width="24" height="24" /></span>返回</a></li>
		<li onclick="toedit()"><span><img src="images/t04.png" /></span>重新编辑</li>
        </ul>

    
    </div>
    
    
    已绑定的属性：
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>属性名称</th>
        <th>属性内容</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="attList" var="list">
        <tr>
        	<td width="228" align="center" class="modify">${list.attrName }</td>
        	<td width="212" align="center" class="modify">${list.attrValue }</td>
        </tr> 
        </s:iterator>
        </tbody>
    </table>
   
    <div class="pagin">
    <input type="hidden" name="GId" value="${goods.GId }" />
    	
    </div>
    </div>
   </s:form> 
	</div>
	
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    
    
    
    
    </div>

</div>
</body>

</html>
