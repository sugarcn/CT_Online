<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="<%=basePath %>manage/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=basePath %>manage/js/jquery.js"></script>
<script type="text/javascript" src="<%=basePath %>manage/js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="<%=basePath %>manage/js/style.css" />
		<script type="text/javascript" src="<%=basePath %>manage/js/jquery.min.js"></script>
		<script type="text/javascript" src="<%=basePath %>manage/js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=basePath %>manage/js/styles.css" />
        <link rel="stylesheet" type="text/css" href="<%=basePath %>manage/js/jquery.confirm.css" />
		<link rel="stylesheet" href="<%=basePath %>manage/css/mycss.css" type="text/css"/>
<script type="text/javascript" src="<%=basePath %>manage/js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="<%=basePath %>manage/js/bomDetail.js" charset="utf-8"></script>
  </head>
<body>
<div class="place">
		<span>位置：</span>
		<ul class="placeul">
			<li><a href="#">客户管理</a></li>
			<li><a href="#">BOM列表</a></li>
			<li><a href="#">BOM详情</a></li>
		</ul>
	</div>
<s:form>
<input type="hidden" value="${bomDTO.UId }" name="bomDTO.UId"/>
<div class="rightinfo">
 <div >当前所在BOM：${bomGoodsList[0].ctBom.bomTitle }</div>
 <div >当前所在用户：${user.UUserid }</div>
<div class="souleft_f">

 <input type="hidden" id="bomId" value="${bomGoodsList[0].bomId}"/>
 <div class="shanchu_f"><input type="button" value="删除" onclick="deleteThis();" class="deleteItem"/></div>
 <input type="hidden" id="GId"/>
 <div class="shuru_f"><input type="text" id="GName" onkeyup="getGoodsByGname();"/></div>
 <ul class="tan_sou" style="display:none;"></ul>
 <div class="shanchu_f"><input type="button" value="增加" onclick="add();"/></div>
 <div class="shuru_f"><input type="text" id="keyword" name="bomGoodsDTO.keyword" value="${bomGoodsDTO.keyword}"/></div>
 <div class="shanchu_f"><input type="button" value="检索" onclick="searchThis();" class="searchItem"/></div>

 </div>
 
 <table class="tablelist">
 	<thead>
    	<tr align="center">
        <th width="29" height="38" style="padding-left:10px"><input type='checkbox' id='checkall' name='checkall' /></th>
        <th>商品图片</th>
        <th>商品编号</th>
        <th>商品名称</th>
        <th>商品品牌</th>
        <th>分类名称</th>
        <th>商品价格</th>
        <th>包装类型</th>
        <th>数量</th>
        </tr>
        <s:iterator value="bomGoodsList" var="list">
        	<tr>
        		<td width="29" height="38" style="padding-left:10px">
					<input type="checkbox" name="bomGoodsDTO.mid" value=${list.bomGoodsId } />
       	  		</td>
       	  		<td width="228" align="center" class="modify">img</td>
        		<td width="212" align="center" class="modify"><s:property value="#list.goods.GId"/></td>
        		<td width="212" align="center" class="modify"><s:property value="#list.goods.GName"/></td>
        		<td width="212" align="center" class="modify"><s:property value="#list.goods.goodsBrand.BName"/></td>
        		<td width="212" align="center" class="modify"><s:property value="#list.goods.goodsCategory.CName"/></td>
        		<s:if test="#list.goods.promotePrice == null">
        			<td width="212" align="center" class="modify"><s:property value="#list.goods.shopPrice"/></td>
        		</s:if>
        		<s:else>
        			<td width="212" align="center" class="modify"><s:property value="#list.goods.promotePrice"/></td>
        		</s:else>
        		<s:if test="#list.pack ==null ">
        			<td>
        			<select id="${list.bomGoodsId }" name="pack" onchange="getPack(${list.bomGoodsId });">
        				<option>--请选择--</option>
        				<option value="${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }">${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }</option>
        				<option value="${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }">${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }</option>
        				<option value="${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }">${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }</option>
        			</select>
        			</td>
        		</s:if>
        		<s:else>
        			<td width="212" align="center" class="modify">
						<c:set var="v1" value="${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }"/>
        				<c:set var="v2" value="${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }"/>
        				<c:set var="v3" value="${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }"/>
        				<c:if test="${list.pack == v1 }">
        					<select id="${list.bomGoodsId }" name="pack" onchange="getPack(${list.bomGoodsId });">
		        				<option value="${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }">${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }</option>
        						<option value="${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }">${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }</option>
		        				<option selected="selected">${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }</option>
        					</select>
        				</c:if>
        				<c:if test="${list.pack == v2}">
        					<select id="${list.bomGoodsId }" name="pack" onchange="getPack(${list.bomGoodsId });">
		        				<option value="${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }">${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }</option>
		        				<option selected="selected">${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }</option>
		        				<option value="${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }">${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }</option>
        					</select>
        				</c:if>
        				<c:if test="${list.pack == v3}">
        					<select id="${list.bomGoodsId }" name="pack" onchange="getPack(${list.bomGoodsId });">
		        				<option selected="selected">${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }</option>
		        				<option value="${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }">${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }</option>
        						<option value="${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }">${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }</option>
        					</select>
        				</c:if>
        			</td>
        		</s:else>
        		<s:if test="#list.goodsNum == null">
        			<td width="212" align="center" class="modify">
        				<a href="javascript:subNum(${list.goods.GId })">-</a>
        				<input style="width: 30px" id="${list.goods.GId}" type="text" value="1"  onkeyup="updateNum(${list.goods.GId });"/>
        				<a href="javascript:addNum(${list.goods.GId })">+</a>
        			</td>
        		</s:if>
        		<s:else>
        			<td width="212" align="center" class="modify">
        				<a href="javascript:subNum(${list.goods.GId })">-</a>
        				<input style="width: 30px" id="${list.goods.GId }" type="text" value="${list.goodsNum}" onkeyup="updateNum(${list.goods.GId });"/>
        				<a href="javascript:addNum(${list.goods.GId })">+</a>
        			</td>
        		</s:else>
        	</tr>
        </s:iterator>
    </thead>
 </table>
 
     ${pages.pageStr}
     
</div>
</s:form>    
</body>
</html>
