<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src=" js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>


<script type="text/javascript">
$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
</script>
<script type="text/javascript">
function toedit(){
	document.forms[0].action = "toTieA_goods";
  document.forms[0].submit();
}

</script>
<script type="text/javascript" src="js/goodsLink.js"></script>

</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">商品管理</a></li>
    <li><a href="#">商品管理</a></li>
    <li><a href="#">设置关联</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" theme="simple">   
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li><a id="go" rel="leanModal" name="signup" href="javascript:history.go(-1);" ><span><img src="manage/images/t01.png" width="24" height="24" /></span>返回</a></li>
		<li class="click"><span><img src="manage/images/t04.png" /></span>添加关联</li>
        </ul>

    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>商品ID</th>
        <th>商品名称</th>
        <th>商品货号</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        	<td width="228" align="center" class="modify">${goods.GId }</td>
        	<td width="212" align="center" class="modify">${goods.GName }</td>
        	<td width="212" align="center" class="modify">${goods.GSn }</td>
        </tr> 
        </tbody>
    </table>
    已绑定的关联：
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>商品ID</th>
        <th>商品名称</th>
        <th>商品货号</th>
        <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="linkList" var="list">
        <tr>
        	<td width="228" align="center" class="modify">${list.linkGId }</td>
        	<td width="212" align="center" class="modify">${list.GName }</td>
        	<td width="212" align="center" class="modify">${list.GSn }</td>
        	<td width="212" align="center" class="modify"><a href="<%=request.getContextPath() %>/unLink_goods?GId=${goods.GId }&linkGId=${list.linkGId }" >删除关联</a></td>
        </tr> 
        </s:iterator>
        </tbody>
    </table>
    添加新的关联：<br/>
    <input type="checkbox" id="doubleLink" name="doubleLink"  />双向关联<br/>
    <input type="hidden" id="dblk" name="dblk" value="0" />
   <input type="text" id="keyword" name="keyword" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="请输入你要查询的关键词" />
<input type="button" value="搜索"  id="search"/>
<table class="tablelist">
<thead>
    	<tr align="center">
    	<th></th>
        <th>商品ID</th>
        <th>商品名称</th>
        <th>商品货号</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="goodsList" var="list">
        <tr>
        <td width="45" align="center" class="modify">
        <input type="checkbox" name="mid" value=${list.GId } />
        </td>
        <td width="228" align="center" class="modify">${list.GId }</td>
        	<td width="212" align="center" class="modify">${list.GName }</td>
        	<td width="212" align="center" class="modify">${list.GSn }</td>
        	</tr>
        	</s:iterator>
        </tbody>
</table>
    
    ${pages.pageStr}
    
   </s:form> 
    
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>

</body>

</html>
