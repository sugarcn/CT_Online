<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/goodsgroup.js" charset="utf-8"></script>
<script type="text/javascript" src="<%=basePath %>manage/goods/js/groupPrice.js"></script>
</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">商品管理</a></li>
    <li><a href="#">商品管理</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" id="form1" theme="simple">   
 
 <input type="hidden" id="deleteA" value="delete_goodsGroup">
 <input type="hidden" id="findA" value="find_goodsGroup">
 <input type="hidden" id="searchA" value="search_goodsGroup">
 <input type="hidden" id="updateA" value="update_goodsGroup"> 
 
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li rel="leanModal" name="signup" href="#signup" onclick="getMaxRName()"><span><img src="images/t01.png" width="24" height="24" /></span>添加</li>

		<li rel="leanModal1" class="modiItem" href="#signup1"><span><img src="images/t02.png" width="24" height="24" /></span>修改</li>

        <li class="deleteItem" ><span><img src="images/t03.png" /></span>删除</li>
        </ul>

	<div class="ssk">
     
		   <div class="ssbk">
		      <div style="float:left;padding-left:30px;padding-top:1px"><input type="text" id="keyword" name="keyword" class="wbdd" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="${keyword}"></div>
		      <div style="float:right"><img src="images/ss.jpg" class="searchItem"></div>
		      <div class="clear"></div>
		   </div>
   
  </div>
    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th><input type='checkbox' id='checkall' name='checkall' /></th>
        <th>商品组</th>
        </tr>
        </thead>
        <tbody>
  <s:iterator value="goodsGroups" var="list">      
        <tr>
			<td width="29" height="38" style="padding-left:10px">
			<input type="checkbox" name="mid" value=${list.GGId } />
       	  </td>
        	<td width="228" align="center" class="modify">
        	<a href="<%=basePath%>manage/group_goods?GGId=<s:property value="#list.GGId"/>"><s:property
									value="#list.GGName" /></a></td>
        </tr> 
</s:iterator>       
        </tbody>
    </table>
    
   ${pages.pageStr}
    
   </s:form> 

	<div id="signup">
			<div id="signup-ct">
				<div id="signup-header">
					<h2>新建商品组</h2>
					<a class="modal_close" href="#"></a>
				</div>
				
				<form id="addgoods">
				  <div class="txt-fld">
				  <input type="hidden" name="groupPrice.addStr" id="rangePrice" />
				  <input type="hidden" name="groupPrice.addStrForPar" id="rangePriceForPar" />
				    <div class="ct_heng"><label for="">商品组名称</label>
				    <input id="" class="good_input" name="GGName" type="text" /></div>
				     <div class="ct_heng"><label for="">商品组说明</label>
				    <input id="" class="good_input" name="GGDesc" type="text" /></div>
				     <div class="ct_heng"><label for="">是否是样品</label>
				    <span class="ct_a_h"><input id="" value="1" name="isSample" type="radio" class="ct_zna"/>是</span>
				    <span class="ct_a_h"><input id="" value="0" checked="checked" name="isSample" type="radio" class="ct_zna"/>否</span>
				    </div>
				     <div class="ct_heng"><label for="">是否是偏料</label>
				    <span class="ct_a_h"><input id="" value="1" name="isPartial" type="radio" class="ct_zna"  />是</span>
				    <span class="ct_a_h"><input id="" value="0" checked="checked" name="isPartial"type="radio" class="ct_zna"/>否</span>
				    </div>
				    <div id="spanIdNotNull">
					    <div class="ct_heng">
					    <span onclick="javascript:addGui();" class="ct_ann" >添加</span>
					     <label for="">样品 &nbsp;区间</label>
					    </div>
					    <div class="ct_tdd">
					    <input  onblur="isCunGuiGe(this);" style="" onfocus="clearTemp(this);" class="goods_input" name="startSim" size="5" type="text" value="起始区间" />
					    <input  onblur="isCunGuiGe(this);"  style="" onfocus="clearTemp(this);" class="goods_input" name="endSim" size="5" type="text" value="结束区间"  />
					    <input class="goods_input" onfocus="clearTemp(this);" style="" onblur="getTemp(this);"  name="simPrice" size="5" type="text"  value="样品价格" />
					    <input class="goods_input" onfocus="clearTemp(this);" style="" onblur="getTemp(this);"  name="simIncr" size="5" type="text"  value="加量幅度" />
					    </div>
					    <br name="testAadd"/>
				    </div>
				    <div id="spanIdNotNullForPar">
					    <div class="ct_heng">
					    <span onclick="javascript:addPar();" class="ct_ann"  >添加</span>
					     <label for="">批量 &nbsp;区间</label>
					    </div>
					    <div class="ct_tdd">
					    <input  onblur="isCunGuiGeForPar(this);" style="" onfocus="clearTemp(this);" class="goods_input" name="startPar" size="5" type="text" value="起始区间"  />
					    <input  onblur="isCunGuiGeForPar(this);" style="" onfocus="clearTemp(this);" class="goods_input" name="endPar" size="5" type="text" value="结束区间" />
					    <input class="goods_input" onfocus="clearTemp(this);" style="" onblur="getTempForPar(this);"  name="parPrice" size="5" type="text"  value="批量价格"/>
					    <input class="goods_input" onfocus="clearTemp(this);" style="" onblur="getTempForPar(this);"  name="parIncr" size="5" type="text"  value="加量幅度" />
					    </div>
					    <br name="testAaddForPar"/>
				    </div>
				  </div>
				  <div class="btn-fld">
                  <div id="tipHtml" style="color:red;float:left">
                    </div>
				  <button type="submit" id="btnSub" onclick="return checkThis(this.form,'add_goodsGroup',0);">确定</button>
</div>
				 </form>
			</div>
		</div>
		
		<div id="signup1">
			<div id="signup-ct">
            <div id="signup-header">
					<h2>修改商品组</h2>
					<a class="modal_close" href="#"></a>
				</div>
              <form>
              <div id="signupmodi">
                    </div>
                    
              <div class="btn-fld">
              <div id="tipHtmlModi" style="color:red;float:left">
                    </div>
				  <button type="submit" onclick="return checkThis(this.form,'update_goodsGroup',1);">确定</button>
</div>
				 </form>
                 </div>
		</div>
	
	
	
    
    
    
    
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	
	

</body>

</html>
