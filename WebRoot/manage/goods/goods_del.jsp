<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <base href="<%=basePath%>">
    <script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
    <title>Goods.jsp</title>

	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript">
	function toback(){
 	$("#delbtn").live("click", function () {
		var checkedNum = $("input[name='mid']:checked").length; 
		if(checkedNum == 0) { 
			alert("请选择至少一项！"); 
			return; 
		} 
		
		
         if (!confirm("确认要还原么？")) {
              return false;
         }else{
			  document.forms[0].action = "hide_goods";
    		document.forms[0].submit();
		}
    });
	}
	</script>
	<script type="text/javascript">
	function delgoods(){
 	$("#del").live("click", function () {
		var checkedNum = $("input[name='mid']:checked").length; 
		if(checkedNum == 0) { 
			alert("请选择至少一项！"); 
			return; 
		} 
		
		
         if (!confirm("确认要删除吗？")) {
              return false;
         }else if(!confirm("删除操作无法撤销！确认继续吗？")){
         	return false;
         }else{
			  document.forms[0].action = "del_goods";
    		document.forms[0].submit();
		}
    });
	}
	</script>
	<script type="text/javascript">
		function emptygoods(){
		if(!confirm("确认要清空回收站吗？")){
			return false;
		}else if(!confirm("清空操作无法撤销！确认继续吗？")){
			return false;
		}else{
			window.location.href="<%=request.getContextPath()%>/manage/empty_goods.action";
		}
	}
	</script>
  </head>
  
  <body>

 <form name="submitform"> 
 <input type="button" value="清空回收站" onclick="emptygoods()" />
<table border="1" width="960" cellspacing="0" cellpadding="0" bordercolor="#D2CFCF" style="border-collapse: collapse">
	<tr style="font-weight:700;color:#fff;font-size:14px">
		<td width="40" align="center" bgcolor="#909090"></td>
		<td width="40" align="center" bgcolor="#909090">商品ID</td>
		<td width="100" align="center" bgcolor="#909090">商品名称</td>
		<td width="100" align="center" bgcolor="#909090">分类ID</td>
		<td width="100" align="center" bgcolor="#909090">商品货号</td>
		<td width="100" align="center" bgcolor="#909090">品牌ID</td>
		<td width="100" align="center" bgcolor="#909090">市场价</td>
		<td width="100" align="center" bgcolor="#909090">本店售价</td>
		<td width="100" align="center" bgcolor="#909090">促销价</td>
		<td width="100" align="center" bgcolor="#909090">上架</td>
		<td width="100" align="center" bgcolor="#909090">商品关键字</td>
		<td width="100" align="center" bgcolor="#909090">可抵用积分数</td>
		<td width="100" align="center" bgcolor="#909090">赠送积分数</td>
		<td width="100" align="center" bgcolor="#909090">单位</td>
		<td width="100" align="center" bgcolor="#909090">包装1名称</td>
		<td width="100" align="center" bgcolor="#909090">包装1数量</td>
		<td width="100" align="center" bgcolor="#909090">包装2名称</td>
		<td width="100" align="center" bgcolor="#909090">包装2数量</td>
		<td width="100" align="center" bgcolor="#909090">包装3名称</td>
		<td width="100" align="center" bgcolor="#909090">包装3数量</td>
		<td width="100" align="center" bgcolor="#909090">包装4名称</td>
		<td width="100" align="center" bgcolor="#909090">包装4数量</td>
	</tr>
   <s:iterator value="querylist" var="list"> 
	<tr >
		<td width="36" height="38" style="padding-left:10px">
        	<input type="checkbox" name="mid" value=${list.GId } />
        </td>
		<td width="40" align="center" class="modify">${list.GId }</td>
		<td width="100" align="center" class="modify">${list.GName }</td>
  		<td width="100" align="center" class="modify">${list.CId }</td>
  		<td width="100" align="center" class="modify">${list.GSn }</td>
  		<td width="100" align="center" class="modify">${list.BId }</td>
  		<td width="100" align="center" class="modify">${list.marketPrice }</td>
  		<td width="100" align="center" class="modify">${list.shopPrice }</td>
  		<td width="100" align="center" class="modify">${list.promotePrice }</td>
  		<td width="100" align="center" class="modify">${list.isOnSale }</td>
  		<td width="100" align="center" class="modify">${list.GKeywords }</td>
  		<td width="100" align="center" class="modify">${list.tokenCredit }</td>
  		<td width="100" align="center" class="modify">${list.giveCredit }</td>
  		<td width="100" align="center" class="modify">${list.GUnit }</td>
  		<td width="100" align="center" class="modify">${list.pack1 }</td>
  		<td width="100" align="center" class="modify">${list.pack1Num }</td>
  		<td width="100" align="center" class="modify">${list.pack2 }</td>
  		<td width="100" align="center" class="modify">${list.pack2Num }</td>
  		<td width="100" align="center" class="modify">${list.pack3 }</td>
  		<td width="100" align="center" class="modify">${list.pack3Num }</td>
  		<td width="100" align="center" class="modify">${list.pack4 }</td>
  		<td width="100" align="center" class="modify">${list.pack4Num }</td>
	</tr>
	</s:iterator>
	</table>
	<input type="button" value="还原商品" onclick="toback()" id="delbtn">
	<input type="button" value="删除商品" onclick="delgoods()" id="del">
</form>
<input type="button" onclick="location.href='javascript:history.go(-1);'" value="返回" />
  </body>
</html>
