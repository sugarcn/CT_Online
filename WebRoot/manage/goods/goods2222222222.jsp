<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/scss" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>


<script type="text/javascript">
$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
</script>
<script type="text/javascript" src="js/goods.js"></script>
<script type="text/javascript" >
function  goTab(GId){
window.location.href="list_Tab?GId="+GId;

}

	$(".searchItem").live("click", function () {
		var checkedNum = $("#keyword").val(); 
		if(checkedNum == "请输入你要查询的关键词") { 
			  alert("请输入你要查询的关键词");
		}else{
			location.href="search_goods1?keyword="+checkedNum;
		}
    });
</script>


</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">商品管理</a></li>
    <li><a href="#">商品管理</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" theme="simple">   
    <div class="rightinfo">
    <input type="hidden" id="searchA" value="search_goods1">
   <div class="tools">
    	<ul class="toolbar">
        <li><a id="go" rel="leanModal" name="signup" href="toadd_goods" ><span><img src="images/t01.png" width="24" height="24" /></span>添加</a></li>
		
<!--         <li><a id="go" name="signup1" href="javascript:void(0)" onclick="updateGoods()" ><span><img src="images/t02.png" /></span>修改</a></li> -->
        <li class="deleteItem" href="hide_goods" ><span><img src="images/t03.png" /></span>删除</li>
        <li><a href="todel_goods">回收站</a></li>
        </ul>

	<div class="ssk">
     
		   <div class="ssbk">
		      <div style="float:left;padding-left:30px;padding-top:1px"><input type="text" id="keyword" name="keyword" class="wbdd" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="请输入你要查询的关键词"></div>
		      <div style="float:left; background:#fff; padding:0;"><img src="<%=basePath%>manage/images/ss.jpg" class="searchItem"></div>
		      <div class="clear"></div>
		   </div>
   
  </div>
    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th><input type='checkbox' id='checkall' name='checkall' /></th>
        <th>商品名称</th>
        <th>商品货号</th>
        <th>市场售价</th>
        <th>本店售价</th>
        <th>促销售价</th>
        <th>是否上架</th>
        </tr>
        </thead>
        <tbody>
  <s:iterator value="goodsList" var="list">      
        <tr align="center">
			<td width="29" height="38" style="padding-left:0px">
			<input type="checkbox" name="mid" value=${list.GId } />
       	  </td>
        	<td width="228" align="center" class="modify"><a href="javascript:goTab('${list.GId }');"  ><s:property value="#list.GName"/></a></td>
        	<td width="212" align="center" class="modify"><s:property value="#list.GSn"/></td>
        	<td width="212" align="center" class="modify"><s:property value="#list.marketPrice"/></td>
        	<td width="212" align="center" class="modify"><s:property value="#list.shopPrice"/></td>
        	<td width="212" align="center" class="modify"><s:property value="#list.promotePrice"/></td>
        	<td width="212" align="center" class="modify"><s:property value="#list.isOnSale"/></td>
        </tr> 
</s:iterator>       
        </tbody>
    </table>
    
   ${pages.pageStr}
   
   </s:form> 
    
	<div id="signup">
			<div id="signup-ct">
				<div id="signup-header">
					<h2>新建等级</h2>
					<a class="modal_close" href="#"></a>
				</div>
				
				<form>
				  <div class="txt-fld">
				    <label for="">商品名称:</label>
				    <input id="" class="good_input" name="GName" type="text" />
				  </div>
				  <div class="txt-fld">
				    <label for="">商品货号:</label>
				    <input id="" class="good_input" name="GSn" type="text" />
				  </div>
				  <div class="txt-fld">
				    <label for="">市场售价:</label>
				    <input id="" class="good_input" name="marketPrice" type="text" />
				  </div>
				  <div class="txt-fld">
				    <label for="">本店售价:</label>
				    <input id="" class="good_input" name="shopPrice" type="text" />
				  </div>
				  <div class="txt-fld">
				    <label for="">促销售价:</label>
				    <input id="" class="good_input" name="promotePrice" type="text" />
				  </div>
				   <div class="txt-fld">
				    <label for="">是否上架:</label>
				    <input id="" class="good_input" name="isOnSale" type="text" />
				  </div>
				  
				  <div class="btn-fld">
                  <div id="tipHtml" style="color:red;float:left">
                    </div>
				  <button type="submit" onclick="return checkThis(this.form,'add_goods',0);">确定</button>
</div>
				 </form>
			</div>
		</div>
		
		<div id="signup1">
			<div id="signup-ct">
            <div id="signup-header">
					<h2>修改等级</h2>
					<a class="modal_close" href="#"></a>
				</div>
              <form>
              <div id="signupmodi">
                    </div>
                    
              <div class="btn-fld">
              <div id="tipHtmlModi" style="color:red;float:left">
                    </div>
				  <button type="submit" onclick="return checkThis(this.form,'update_goods',1);">确定</button>
</div>
				 </form>
                 </div>
		</div>
	
	
	
    
    
    
    
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>

</body>

</html>
