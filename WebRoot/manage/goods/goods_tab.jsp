<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="../js/select-ui.min.js"></script>
<!-- 编辑详情 -->
	<script type="text/javascript" src="../js/jquery.leanModal.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="../../js/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../js/ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../../js/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript">
function setContent() {
		var detl = document.getElementById("GDetail").value;
        UE.getEditor('editor').setContent(detl);
    }
function getContent() {
        var value = UE.getEditor('editor').getContent();
        $("#GDetail").val(value);
        document.forms[0].action = "Edit_goods";
    	document.forms[0].submit();
        }
</script>

<!-- 绑定资料 -->

<script type="text/javascript">
$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
function submitform(){
	document.forms[0].action = "tieRnT_goods";
  document.forms[0].submit();
}
</script>
<script type="text/javascript" src="../js/goodsResource.js"></script>

<!-- 编辑属性 -->
<script type="text/javascript">
$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
</script>
<script type="text/javascript">
function toedit(){
	document.forms[0].action = "toTieA_goods";
  document.forms[0].submit();
}
</script>
<script type="text/javascript" src="../js/goodsAttribute.js"></script>

<!-- 设定关联 -->
<link rel="stylesheet" type="text/css" href="../js/jquery.confirm.css" />
		<link rel="stylesheet" href="../css/mycss.css" type="text/css"/>
<script type="text/javascript">
$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
</script>
<script type="text/javascript">
function toedit(){
	document.forms[0].action = "toTieA_goods";
  document.forms[0].submit();
}

</script>
<script type="text/javascript" src="../js/goodsLink.js"></script>

<!-- 设定替代 -->
<script type="text/javascript">
$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
</script>
<script type="text/javascript">
function toedit(){
	document.forms[0].action = "toTieA_goods";
  document.forms[0].submit();
}
</script>
<script type="text/javascript" src="../js/goodsReplace.js"></script>

<!-- 管理库存 -->
<script type="text/javascript">
	function add(){
	alert(33);
	$("#addbtn").live("click", function () {
		document.forms[0].action = "addepot_goods";
    	document.forms[0].submit();
	});
	}
	</script>


</head>

<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">商品管理</a></li>
    <li><a href="#">商品管理</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
    <li><a href="#tab1" class="selected">编辑详情</a></li> 
    <li><a href="#tab2">绑定资料</a></li> 
    <li><a href="#tab3">编辑属性</a></li>
    <li><a href="#tab4">设定关联</a></li>
    <li><a href="#tab5">设定替代</a></li>
    <li><a href="#tab6">管理库存</a></li>
  	</ul>
    </div> 
    <!-- 编辑详情 -->
  	<div id="tab1" class="tabson">
     <form action="tieA_goods" name="submitform" method="post">
   		<p>为下列商品编辑描述：</p><br/>
    	<s:iterator      >
    		<p>${goods.GId }&nbsp;${goods.GName }</p><br/>
    		<input type="hidden" name="GId" value="${goods.GId }" />
    		<input type="hidden" id="GDetail" name="GDetail" value="${goodsDetail.GDetail}" />
    		<input type="hidden" name="detlId" value="${goodsDetail.detlId}" />
		<div>
		<script type="text/javascript">var ue = UE.getEditor('editor');</script>
			<script id="editor" type="text/plain" style="width:1024px;height:500px;"></script>
		</div>
		</s:iterator>
		<input type="button" value="保存" onclick="getContent()" id="tiebtn"/>
		<input type="button" onclick="location.href='javascript:history.go(-1);'" value="返回" />
    </form>
    
    </div> 
    
    <!-- 绑定资料 -->
  	<div id="tab2" class="tabson">
    <s:form method="post"  name="form1" theme="simple">   
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li><a id="go" rel="leanModal" name="signup" href="javascript:history.go(-1);" ><span><img src="images/t01.png" width="24" height="24" /></span>返回</a></li>
		<li onclick="submitform()"><span><img src="images/t04.png" /></span>绑定</li>
        </ul>

    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>商品ID</th>
        <th>商品名称</th>
        <th>商品货号</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        	<td width="228" align="center" class="modify">${goods.GId }</td>
        	<td width="212" align="center" class="modify">${goods.GName }</td>
        	<td width="212" align="center" class="modify">${goods.GSn }</td>
        </tr> 
        </tbody>
    </table>
    已绑定的资料：
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>资料名称</th>
        <th>文件路径</th>
        <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="viewresList" var="list">
        <tr>
        	<td width="228" align="center" class="modify">${list.RName }</td>
        	<td width="212" align="center" class="modify">${list.RUrl }</td>
        	<td width="212" align="center" class="modify"><a href="<%=request.getContextPath()%>/unTieRnT_goods?RId=${list.RId }&GId=${list.GId }" >删除</a></td>
        </tr> 
        </s:iterator>
        </tbody>
    </table>
   
    <div class="pagin">
    <input type="hidden" name="GId" value="${goods.GId }" />
    	商品资料：<select id="RId" name="RId">
							<s:iterator value="resList" var="list">
								<option value="${list.RId }">${list.RName }</option>
							</s:iterator>
					</select><br/>
    </div>
    </div>
   </s:form>
        
    </div> 
    
    <!-- 编辑属性 -->
  	<div id="tab3" class="tabson">
    <s:form method="post"  name="form1" theme="simple">   
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li><a id="go" rel="leanModal" name="signup" href="javascript:history.go(-1);" ><span><img src="manage/images/t01.png" width="24" height="24" /></span>返回</a></li>
		<li onclick="toedit()"><span><img src="manage/images/t04.png" /></span>重新编辑</li>
        </ul>

    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>商品ID</th>
        <th>商品名称</th>
        <th>商品货号</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        	<td width="228" align="center" class="modify">${goods.GId }</td>
        	<td width="212" align="center" class="modify">${goods.GName }</td>
        	<td width="212" align="center" class="modify">${goods.GSn }</td>
        </tr> 
        </tbody>
    </table>
    已绑定的属性：
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>属性名称</th>
        <th>属性内容</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="attList" var="list">
        <tr>
        	<td width="228" align="center" class="modify">${list.attrName }</td>
        	<td width="212" align="center" class="modify">${list.attrValue }</td>
        </tr> 
        </s:iterator>
        </tbody>
    </table>
   
    <div class="pagin">
    <input type="hidden" name="GId" value="${goods.GId }" />
    	
    </div>
    </div>
   </s:form> 
        
    </div>
    
    <!-- 设定关联 -->
  	<div id="tab4" class="tabson">
    <s:form method="post"  name="form1" theme="simple">   
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li><a id="go" rel="leanModal" name="signup" href="javascript:history.go(-1);" ><span><img src="manage/images/t01.png" width="24" height="24" /></span>返回</a></li>
		<li class="click"><span><img src="manage/images/t04.png" /></span>添加关联</li>
        </ul>

    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>商品ID</th>
        <th>商品名称</th>
        <th>商品货号</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        	<td width="228" align="center" class="modify">${goods.GId }</td>
        	<td width="212" align="center" class="modify">${goods.GName }</td>
        	<td width="212" align="center" class="modify">${goods.GSn }</td>
        </tr> 
        </tbody>
    </table>
    已绑定的关联：
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>商品ID</th>
        <th>商品名称</th>
        <th>商品货号</th>
        <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="linkList" var="list">
        <tr>
        	<td width="228" align="center" class="modify">${list.linkGId }</td>
        	<td width="212" align="center" class="modify">${list.GName }</td>
        	<td width="212" align="center" class="modify">${list.GSn }</td>
        	<td width="212" align="center" class="modify"><a href="<%=request.getContextPath() %>/unLink_goods?GId=${goods.GId }&linkGId=${list.linkGId }" >删除关联</a></td>
        </tr> 
        </s:iterator>
        </tbody>
    </table>
    添加新的关联：<br/>
    <input type="checkbox" id="doubleLink" name="doubleLink"  />双向关联<br/>
    <input type="hidden" id="dblk" name="dblk" value="0" />
   <input type="text" id="keyword" name="keyword" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="请输入你要查询的关键词" />
<input type="button" value="搜索"  id="search"/>
<table class="tablelist">
<thead>
    	<tr align="center">
    	<th></th>
        <th>商品ID</th>
        <th>商品名称</th>
        <th>商品货号</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="goodsList" var="list">
        <tr>
        <td width="45" align="center" class="modify">
        <input type="checkbox" name="mid" value=${list.GId } />
        </td>
        <td width="228" align="center" class="modify">${list.GId }</td>
        	<td width="212" align="center" class="modify">${list.GName }</td>
        	<td width="212" align="center" class="modify">${list.GSn }</td>
        	</tr>
        	</s:iterator>
        </tbody>
</table>
    </div>
    
   </s:form>
        
    </div>
    
    <!-- 设定替代 -->
  	<div id="tab5" class="tabson">
    <s:form method="post"  name="form1" theme="simple">   
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li><a id="go" rel="leanModal" name="signup" href="javascript:history.go(-1);" ><span><img src="images/t01.png" width="24" height="24" /></span>返回</a></li>
		<li class="click"><span><img src="images/t04.png" /></span>添加替代</li>
        </ul>

    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>商品ID</th>
        <th>商品名称</th>
        <th>商品货号</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        	<td width="228" align="center" class="modify">${goods.GId }</td>
        	<td width="212" align="center" class="modify">${goods.GName }</td>
        	<td width="212" align="center" class="modify">${goods.GSn }</td>
        </tr> 
        </tbody>
    </table>
    已绑定的替代：
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>商品ID</th>
        <th>商品名称</th>
        <th>商品货号</th>
        <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="repList" var="list">
        <tr>
        	<td width="228" align="center" class="modify">${list.replaceGId }</td>
        	<td width="212" align="center" class="modify">${list.GName }</td>
        	<td width="212" align="center" class="modify">${list.GSn }</td>
        	<td width="212" align="center" class="modify"><a href="<%=request.getContextPath() %>/unReplace_goods?GId=${goods.GId }&linkGId=${list.replaceGId }" >删除替代</a></td>
        </tr> 
        </s:iterator>
        </tbody>
    </table>
    添加新的替代：<br/>
    <input type="checkbox" id="doubleLink" name="doubleLink"  />双向关联<br/>
    <input type="hidden" id="dblk" name="dblk" value="0" />
   <input type="text" id="keyword" name="keyword" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="请输入你要查询的关键词" />
<input type="button" value="搜索"  id="search"/>
<table class="tablelist">
<thead>
    	<tr align="center">
    	<th></th>
        <th>商品ID</th>
        <th>商品名称</th>
        <th>商品货号</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="goodsList" var="list">
        <tr>
        <td width="45" align="center" class="modify">
        <input type="checkbox" name="mid" value=${list.GId } />
        </td>
        <td width="228" align="center" class="modify">${list.GId }</td>
        	<td width="212" align="center" class="modify">${list.GName }</td>
        	<td width="212" align="center" class="modify">${list.GSn }</td>
        	</tr>
        	</s:iterator>
        </tbody>
</table>
</div>
   </s:form> 
        
    </div>
    
    <!-- 管理库存 -->
  	<div id="tab6" class="tabson">
     <form action="updateNum_goods" name="submitform" method="post">
   		<p>为下列商品管理库存：</p><br/>
    	
    		<p>${goods.GId }&nbsp;${goods.GName }</p><br/>
    		<input type="hidden" name="GId" value="${goods.GId }" />
    		添加仓库：<select id="depotId" name="depotId">
							<s:iterator value="depotList" var="list">
								<option value="${list.depotId }">${list.depotName }</option>
							</s:iterator>
					</select>
					<input type="button" value="添加" onclick="add()" id="addbtn" /><br/>
    	库存：<br/>
							<s:iterator value="querylist" status="list">
							
								<a><s:property value="querylist[#list.index][0]"/></a>：<input type="text"  name="mval" value="<s:property value="querylist[#list.index][1]"/>"/>
								<input type="hidden"  name="deid" value="<s:property value="querylist[#list.index][2]"/>" />
								<input type="hidden"  name="mid" value="<s:property value="querylist[#list.index][3]"/>" /><br/>
							</s:iterator>
							<br/>
		<input type="submit" value="保存" />
		<input type="button" onclick="location.href='javascript:history.go(-1);'" value="返回" />
    </form>
        
    </div> 
       
	</div> 
 
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    
    
    
    
    </div>


</body>

</html>
