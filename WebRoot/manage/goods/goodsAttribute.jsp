<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>


<script type="text/javascript">
$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
</script>
<script type="text/javascript">
function toedit(){
	document.forms[0].action = "toTieA_goods";
  document.forms[0].submit();
}
</script>
<script type="text/javascript" src="js/goodsAttribute.js"></script>

</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">商品管理</a></li>
    <li><a href="#">商品管理</a></li>
    <li><a href="#">编辑属性</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" theme="simple">   
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li><a id="go" rel="leanModal" name="signup" href="javascript:history.go(-1);" ><span><img src="manage/images/t01.png" width="24" height="24" /></span>返回</a></li>
		<li onclick="toedit()"><span><img src="manage/images/t04.png" /></span>重新编辑</li>
        </ul>

    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>商品ID</th>
        <th>商品名称</th>
        <th>商品货号</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        	<td width="228" align="center" class="modify">${goods.GId }</td>
        	<td width="212" align="center" class="modify">${goods.GName }</td>
        	<td width="212" align="center" class="modify">${goods.GSn }</td>
        </tr> 
        </tbody>
    </table>
    已绑定的属性：
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>属性名称</th>
        <th>属性内容</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="attList" var="list">
        <tr>
        	<td width="228" align="center" class="modify">${list.attrName }</td>
        	<td width="212" align="center" class="modify">${list.attrValue }</td>
        </tr> 
        </s:iterator>
        </tbody>
    </table>
   
    <div class="pagin">
    <input type="hidden" name="GId" value="${goods.GId }" />
    	
    </div>
   </s:form> 
    
	
	<div id="signup">
			<div id="signup-ct">
				<div id="signup-header">
					<h2>新建等级</h2>
					<a class="modal_close" href="#"></a>
				</div>
				
				<form>
				  <div class="txt-fld">
				    <label for="">等级名称:</label>
				    V<input id="" class="good_input" name="RName" type="text" />
				  </div>
				  <div class="txt-fld">
				    <label for="">最小积分:</label>
				    <input id="" class="good_input" name="minPoints" type="text" />
				  </div>
				  <div class="txt-fld">
				    <label for="">最大积分:</label>
				    <input id="" class="good_input" name="maxPoints" type="text" />
				  </div>

				  <div class="btn-fld">
                  <div id="tipHtml" style="color:red;float:left">
                    </div>
				  <button type="submit" onclick="return checkThis(this.form,'add_userRank',0);">确定</button>
</div>
				 </form>
			</div>
		</div>
		
		<div id="signup1">
			<div id="signup-ct">
            <div id="signup-header">
					<h2>修改等级</h2>
					<a class="modal_close" href="#"></a>
				</div>
              <form>
              <div id="signupmodi">
                    </div>
                    
              <div class="btn-fld">
              <div id="tipHtmlModi" style="color:red;float:left">
                    </div>
				  <button type="submit" onclick="return checkThis(this.form,'update_userRank',1);">确定</button>
</div>
				 </form>
                 </div>
		</div>
	
	
    
    
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>

</body>

</html>
