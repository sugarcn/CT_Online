<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="frm" uri="http://java.sun.com/jsp/jstl/fmt" %>


<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="<%=basePath %>manage/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=basePath %>manage/js/jquery.js"></script>
<script type="text/javascript" src="<%=basePath %>manage/js/jquery-latest.pack.js" ></script>
		<script type="text/javascript" src="<%=basePath %>manage/js/jquery.min.js"></script>
		<script type="text/javascript" src="<%=basePath %>manage/js/jquery.leanModal.min.js"></script>
<script type="text/javascript" src="<%=basePath %>manage/js/ct.js" charset="utf-8"></script>


<script type="text/javascript">
$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
</script>
<script type="text/javascript">
function getGoodsByGname(){
	var myselect=document.getElementById("GType");
	var index=myselect.selectedIndex ; 
    var GType = myselect.options[index].value;;
	var GName = $("#GName").val();
	$(".sbsss").html("");
	
	$.post("goods_getGoodsByGname1",{"goodsDTO.GName":GName,"goodsDTO.GType":GType},function(data){
		//data = eval(data);
		var html = "";
		$(".sbsss").css("display","block");
		for(var i=0;i<data.length;i++){
			html = html + "<li onclick=\"javascript:setGid("+data[i][0]+",\'"+data[i][1]+"\')\"><span >"+data[i][1]+"</span></li>";
			$(".sbsss").html(html);
		}
	});
}
//设置下拉客户
function getUserByGname(){

    var UType =$("#UType").val();
	var UUsername = $("#UUsername").val();
	$(".sbs").html("");
	
	$.post("goods_getGoodsByGname2",{"UUsername":UUsername,"UType":UType},function(data){
		//data = eval(data);
		var html = "";
		$(".sbs").css("display","block");
		for(var i=0;i<data.length;i++){
			html = html + "<li onclick=\"javascript:setUid("+data[i][0]+",\'"+data[i][1]+"\')\"><span >"+data[i][1]+"</span></li>";
			$(".sbs").html(html);
		}
	});
}
//设置下拉菜单的商品Uid
function setUid(Uid,UUsername){
	$("#UId").val(UId);
	$("#UUsername").val(UUsername);
	
	$(".sbs").css("display","none");
}
//设置下拉菜单的商品Gid
function setGid(Gid,Gname){
	$("#GId").val(Gid);
	$("#GName").val(Gname);
	
	$(".sbsss").css("display","none");
}
$(".searchItem").live("click", function () {
	document.forms[0].action = "list_price";
  document.forms[0].submit();
});
$(".sb").live("click", function () {
	var price = document.getElementById("price").value;
	var rule1 =new RegExp("^/+[0-9]+(.[0-9]{0,2})?$");
	var rule2 =new RegExp("^/-[0-9]+(.[0-9]{0,2})?$");
	var rule3 =new RegExp("[0-9]+(.[0-9]{0,2})?$");
	var rule4 =new RegExp("[0-9]{,2}%$");
	if(rule1.test(price)||rule2.test(price)||rule3.test(price)||rule4.test(price)){
		document.forms[0].action = "insert_price";
		  document.forms[0].submit();
	}else{
		alert("格式不对！");
	}
});

</script>
<script type="text/javascript" src="js/goodsLink.js"></script>

</head>


<body>
        <input type="hidden" id="GId" name="GId">
        <input type="hidden" id="UId" name="UId">
  
	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">商品管理</a></li>
    <li><a href="#">商品管理</a></li>
    <li><a href="#">设置关联</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" theme="simple">   
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li><a id="go" rel="leanModal" name="signup" href="javascript:history.go(-1);" ><span><img src="<%=request.getContextPath() %>/manage/images/t01.png" width="24" height="24" /></span>返回</a></li>
		<li class="click"><span><img src="<%=request.getContextPath() %>/manage/images/t04.png" /></span>添加关联</li>
        </ul>

     <div class="toolsdd">
        <span style="height:35px; line-height:35px;">商品：</span><input type="text"  id="GName" name="GName" onkeyup="getGoodsByGname();" class="price_input"/>
     
    
      <ul class="sbsss" style="display:none;" ></ul>
    				<select id="GType" name="GType" class="price_select">
							<option value="0">商品</option>
							<option value="1">商品组</option>
					</select>
					
    <span style="height:35px; line-height:35px;">用户：</span><input type="text"  id="UUsername" name="UUsername" onkeyup="getUserByGname();" class="price_input"/>
      
    <ul class="sbs" style="display:none;" ></ul>
    				<select id="UType" name="UType" class="price_select">
							<option value="0">客户</option>
							<option value="1">客户组</option>
					</select>
    <span style="height:35px; line-height:35px;">价格：</span><input type="text" id="price" name="price" class="price_input"/>
    	<ul class="toolbar_ss">
    	<li class="ss_d"><img src="images/ss.jpg" class="searchItem"></li>
        <li class="sb"><img src="images/t01.png" width="24" height="24" />添加</li>
    </ul>
    </div>   
    </div>
    


    <%--<span class="yisheding">已设定的价格：</span>--%>
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th class="gp_1">商品/商品组名称</th>
        <th class="gp_3">用户名称</th>
        <th class="gp_4">用户账号</th>
        <th class="gp_6">商品价格</th>
        <th class="gp_5">修改时间</th>
        <th class="gp_7">操作</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="viewPriceList" var="list">
        <tr>
        	<td align="center" class="modify gp_1">${list.goodsname }</td>
        	<td align="center" class="modify gp_3">${list.username }</td>
        	<td align="center" class="modify gp_4">${list.useraccount }</td>
        	<td align="center" class="modify gp_6">${list.goodsprice }</td>
        	<td align="center" class="modify gp_5">${list.insTime }</td>
        	<td align="center" class="modify gp_7"><a href="delprice_goods?pid=${list.pid }" >删除价格</a></td>
        </tr> 
        </s:iterator>
        </tbody>
    </table>

   ${pages.pageStr}
     
   </s:form> 
	
		
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>

</body>

</html>
