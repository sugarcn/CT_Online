<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>
<script type="text/javascript" src="js/goTab.js"></script>
      <script type="text/javascript" src="goods/js/randePrice.js"></script>
<!-- 编辑详情 -->
	<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../js/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript">

  function setContent() {
		var detl = document.getElementById("GDetail").value;
        UE.getEditor('editor').setContent(detl);
    }
function getContent() {
        var value = UE.getEditor('editor').getContent();
        getTempForPar();
        getTemp();
        $("#GDetail").val(value);
        $("#submitform").attr("action","Edit_goods");
        $("#submitform").submit();
        }
</script>
</head>


<body onload="setContent()">

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">商品管理</a></li>
    <li><a href="#">商品管理</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
    <li><a href="#tab1" class="selected">编辑详情</a></li> 
    <li><a href="javascript:goTab2('${GId }');">绑定资料</a></li> 
    <li><a href="javascript:goTab3('${GId }');">编辑属性</a></li>
    <li><a href="javascript:goTab4('${GId }');">设定关联</a></li>
    <li><a href="javascript:goTab5('${GId }');">设定替代</a></li>
    <li><a href="javascript:goTab6('${GId }');">管理库存</a></li>
  	</ul>
    </div> 
    <!-- 编辑详情 -->
  	<div id="tab1" class="tabson">
    <form name="submitform" id="submitform" method="post">

    		<input type="hidden" name="GId" value="${goods.GId }" />
    		<input type="hidden" id="GDetail" name="GDetail" value="${goodsDetail.GDetail}" />
    		<input type="hidden" name="detlId" value="${goodsDetail.detlId}" />
    		<input type="hidden" name="rangePrice.addStr" id="rangePrice" />
			<input type="hidden" name="rangePrice.addStrForPar" id="rangePriceForPar" />
    		<ul class="ct_h_sf">
    		<li><span class="h_ct_sf_l">是否是样品：</span>
    		<span><input type="radio" name="isSample"
    			<c:if test="${goods.isSample == 1 }">
    				 checked="checked"
    			</c:if>
    		 />是</span>
    		 <span><input type="radio" name="isSample" 
    		 	<c:if test="${goods.isSample == 0 || goods.isSample == null }">
    				 checked="checked"
    			</c:if>
    		  />否</span>
    		  </li>
    		<li><span class="h_ct_sf_l">是否是偏料：</span>
    		<span><input type="radio" name="isPartial" 
    			<c:if test="${goods.isPartial == 1 }">
    				 checked="checked"
    			</c:if>
    		 />是</span>
    		 <span><input type="radio" name="isPartial" 
    		 	<c:if test="${goods.isPartial == 0 || goods.isPartial == null }">
    				 checked="checked"
    			</c:if>
    		  />否</span>
    		  </li>
    		  </ul>
    		  <div class="h_tianj">
    		   
    		  <div class="par_bao"><a href="javascript:test();">添加</a><span class="par_aa">范围小</span><span class="par_bb">范围大</span><span class="par_cc">价格(元)</span><span class="par_dd">加量幅度 </span></div>
    		   
    		  <c:forEach items="${rangePricesList }" var="list">
    		  <div id="spanIdNotNull"><span class='pc_m'>样品规格(pcs)：</span><input value="${list.simSNum }" onblur="isCunGuiGe(this);" type="text" style="text-align: center;" size="5" id="startGui1" name="startGui" /><input style="text-align: center;" value="${list.simENum }" onblur="isCunGuiGe(this);" size="5"  type="text" id="endGui" name="endGui" /><input id="reanPrice"  value="${list.simRPrice }" onblur="getTemp();" name="reanPrice" style="text-align: center;" type="text" size="4" /><input type="text" value="${list.simIncrease }" onblur="getTemp();" size="6" style="text-align: center;" id="increase" name="increase" /><br name="testAadd"/></div>
    		  </c:forEach>
    		  </div>
    		  <div class="h_tianj">
    		   
    		  <div class="par_bao"><a href="javascript:testForPar();">添加</a><span class="par_aa">范围小</span><span class="par_bb">范围大</span><span class="par_cc">价格(元)</span><span class="par_dd">加量幅度 </span></div>
    		   <c:forEach items="${rangePricesList }" var="list">
    		<div id="spanIdNotNullForPar"><span class='pc_m'>批量规格( k )：</span><input type="text"  value="${list.parSNum }"  onblur="isCunGuiGeForPar(this);" id="startPar" name="startPar"  style="text-align: center;" size="5" /><input onblur="isCunGuiGeForPar(this);"  value="${list.parENum }" style="text-align: center;" id="endPar" name="endPar" size="5"  type="text"/><input id="reanPriceForPar"  value="${list.parRPrice }"  name="reanPriceForPar" style="text-align: center;" onblur="getTempForPar();" type="text" size="4" /><input type="text"  value="${list.parIncrease }" size="6" style="text-align: center;" onblur="getTempForPar();" id="increaseForPar" name="increaseForPar" /><br name="testAaddForPar"/></div></c:forEach>
			</div>
		<div class="h_ds">
		<script type="text/javascript">var ue = UE.getEditor('editor');</script>
		<script id="editor" type="text/plain" style="width:100%;height:500px;"></script>
		
		<input type="button" value="保存" onclick="getContent()" id="tiebtn" class="ct_baoc">
		<input type="button" onclick="location.href='javascript:history.go(-1);'" value="返回" class="ct_fanhuis">
		
		</div>
    </form>
    </div> 
    
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    
    
    
    
    </div>

</div>
</body>

</html>
