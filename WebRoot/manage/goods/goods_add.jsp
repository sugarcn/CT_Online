
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<%=basePath %>js/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="<%=basePath %>manage/goods/js/randePrice.js"></script>
  <script type="text/javascript" src="<%=basePath %>manage/goods/js/json2.js"></script>
</head>

<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">首页</a></li>
    <li><a href="#">表单</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    <div class="formtitle"><span>基本信息</span></div>
     <form action="manage/add_goods" id="addgoods" name="addgoods" method="post">
   <input type="hidden" id="GId" name="GId" value="" />
   <input type="hidden" name="cids" id="CIds" />
   <input type="hidden" value='${categoryListJson }' id="categoryList" />
   		<input type="hidden" name="rangePrice.addStr" id="rangePrice" />
		<input type="hidden" name="rangePrice.addStrForPar" id="rangePriceForPar" />
   
    <ul class="forminfo">
	<li><label>商品名称</label><input type="text" id="GName" name="GName" class="dfinput" /><i>标题不能超过30个字符</i></li>
	<li><label>商品分类</label><select name="cidTemp" onChange="isChongfu(this);">
							<s:iterator value="categoryList" var="list">
								<option value="${list.CId }">${list.CName }</option>
							</s:iterator>
					</select><a href="javascript:addType();">添加分类</a><i></i></li>
	<li><label>商品货号</label><input type="text" id="GSn" name="GSn" class="dfinput" /><i>标题不能超过30个字符</i></li>
	<li><label>品牌</label><select id="BId" name="BId">
					<s:iterator value="brandList" var="list">
						<option value="${list.BId }">${list.BName }</option>
					</s:iterator>
				</select><i>标题不能超过30个字符</i></li>
	<li><label>市场价</label><input type="text" id="marketPrice" name="marketPrice" class="dfinput" /><i>标题不能超过30个字符</i></li>
	<li><label>本店售价</label><input type="text" id="shopPrice" name="shopPrice" class="dfinput" /><i>标题不能超过30个字符</i></li>
	<li><label>促销价</label><input type="text" id="promotePrice" name="promotePrice" class="dfinput" /><i>标题不能超过30个字符</i></li>
	<li><label>是否上架</label><select id="isOnSale" name="isOnSale">
							<option value="1">是</option>
							<option value="0">否</option>
						</select><i>标题不能超过30个字符</i></li>
	<li><label>是否是样品</label> <input id="" value="1" name="isSample" type="radio" class="ct_zna"/>是
				   <input id="" value="0" checked="checked" name="isSample" type="radio" class="ct_zna"/>否<i>标题不能超过30个字符</i></li>
	<li><label>是否是偏料</label><input id="" value="1" name="isPartial" type="radio" class="ct_zna"  />是
			<input id="" value="0" checked="checked" name="isPartial"type="radio" class="ct_zna"/>否<i>标题不能超过30个字符</i></li>
	<li><label>商品关键字</label><input type="text" id="GKeywords" name="GKeywords" class="dfinput" /><i>标题不能超过30个字符</i></li>
	<li><label>可抵用积分数</label><input type="text" id="tokenCredit" name="tokenCredit" class="dfinput" /><i>标题不能超过30个字符</i></li>
	<li><label>赠送积分数</label><input type="text" id="giveCredit" name="giveCredit" class="dfinput" /><i>标题不能超过30个字符</i></li>
		<li><label></label><span style="padding-left:90px;color:red;">范围小 &nbsp;&nbsp;&nbsp;范围大&nbsp; &nbsp;&nbsp;价格(元) &nbsp;&nbsp;&nbsp;&nbsp;加量幅度&nbsp;&nbsp;&nbsp;&nbsp;如：100~1000  0.5元  按100加量</span></li>
	<li>
	
	<span id="spanIdNotNull">
			样品规格(pcs)&nbsp;&nbsp;&nbsp;<input  onblur="isCunGuiGe(this);" type="text" size="5" id="startGui1" name="startGui" class="sinput"  />~<input  onBlur="isCunGuiGe(this);" size="5"  type="text" id="endGui" name="endGui"  class="sinput" /> | 
			<input id="reanPrice" onBlur="getTemp();" name="reanPrice"  class="sinput" type="text" size="4"  />    &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" onBlur="getTemp();" size="6"  id="increase" name="increase"  class="sinput" />&nbsp;&nbsp;&nbsp;<a href="javascript:test();">增加新区间</a><br name="testAadd"/></span>
	</li>
	
	
	
	<li>
	
	<span id="spanIdNotNullForPar">
			批量规格( k )&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text"  onblur="isCunGuiGeForPar(this);" id="startPar" name="startPar" class="sinput" size="5" />~<input onBlur="isCunGuiGeForPar(this);" class="sinput" id="endPar" name="endPar" size="5"  type="text"/> | 
			<input id="reanPriceForPar" name="reanPriceForPar" class="sinput" onBlur="getTempForPar();" type="text" size="4" />    &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" size="6" class="sinput" onBlur="getTempForPar();" id="increaseForPar" name="increaseForPar" />&nbsp;&nbsp;&nbsp; <a href="javascript:testForPar();">增加新区间</a><br name="testAaddForPar"/>
		</span>
</li>
	<li><label>单位</label><input type="text" id="GUnit" name="GUnit" class="dfinput" /><i>标题不能超过30个字符</i></li>
	<li><label>包装1名称</label><input type="text" id="pack1" name="pack1" class="dfinput" /><i>标题不能超过30个字符</i></li>
	<li><label>包装1数量</label><input type="text" id="pack1Num" name="pack1Num" class="dfinput" /><i>标题不能超过30个字符</i></li>
	<li><label>包装2名称</label><input type="text" id="pack2" name="pack2" class="dfinput" /><i>标题不能超过30个字符</i></li>
	<li><label>包装2数量</label><input type="text" id="pack2Num" name="pack2Num" class="dfinput" /><i>标题不能超过30个字符</i></li>
	<li><label>包装3名称</label><input type="text" id="pack3" name="pack3" class="dfinput" /><i>标题不能超过30个字符</i></li>
	<li><label>包装3数量</label><input type="text" id="pack3Num" name="pack3Num" class="dfinput" /><i>标题不能超过30个字符</i></li>
	<li><label>包装4名称</label><input type="text" id="pack4" name="pack4"  class="dfinput" /><i>标题不能超过30个字符</i></li>
	<li><label>包装4数量</label><input type="text" id="pack4Num" name="pack4Num" class="dfinput" /><i>标题不能超过30个字符</i></li>
	<li><input type="button" id="subtn" name="subtn" value="保存" class="btn" /></li>
	    <div style="clear:both;"></div>
	
	
	
	
	
	
    </ul>
    
     </form>
    </div>


</body>

</html>
