<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>


<script type="text/javascript">
$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
function submitform(){
     var GTId = $("#GTId").val();
      var GId = $("#GId").val();
	location.href = "toTieA_goods?GTId="+GTId+"&GId="+GId;
}
function submitform1(){
	document.forms[0].action = "tieA_goods";
  document.forms[0].submit();
}
</script>
<script type="text/javascript" src="js/goodsAttribute.js"></script>

</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">商品管理</a></li>
    <li><a href="#">商品管理</a></li>
    <li><a href="#">编辑属性</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" theme="simple">   
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li><a id="go" rel="leanModal" name="signup" href="javascript:history.go(-1);" ><span><img src="manage/images/t01.png" width="24" height="24" /></span>返回</a></li>
		<li><input type="hidden" name="GId" id="GId" value="${goods.GId }" />
    	商品类别：<select id="GTId" name="GTId">
							<s:iterator value="typeList" var="list">
								<option value="${list.GTId }">${list.typeName }</option>
							</s:iterator>
					</select></li>
		<li onclick="submitform()"><span><img src="manage/images/t04.png" /></span>选择类型</li>
		<li onclick="submitform1()"><span><img src="manage/images/t04.png" /></span>保存</li>
        </ul>

    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>商品ID</th>
        <th>商品名称</th>
        <th>商品货号</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        	<td width="228" align="center" class="modify">${goods.GId }</td>
        	<td width="212" align="center" class="modify">${goods.GName }</td>
        	<td width="212" align="center" class="modify">${goods.GSn }</td>
        </tr> 
        </tbody>
    </table>
    编辑属性：
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>属性名称</th>
        <th>属性内容</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="attributeList" var="list">
        <tr>
        	<td width="228" align="center" class="modify">${list.attrName }</td>
        	<td width="212" align="center" class="modify"><input type="text"  name="mval" /><input type="hidden"  name="mid" value="${list.attrId }" /></td>
        </tr> 
        </s:iterator>
        </tbody>
    </table>
   
    <div class="pagin">
    
    </div>
   </s:form> 
    
	
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>

</body>

</html>
