<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>


	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>
<script type="text/javascript" src="js/goTab.js"></script>
<!-- 绑定资料 -->

<script type="text/javascript">
$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
function submitform(){
	document.forms[0].action = "tieRnT_goods";
  document.forms[0].submit();
}
</script>
<script type="text/javascript" src="js/goodsResource.js"></script>
<script type="text/javascript" >
function  goTab(GId){
window.location.href="list_Tab?GId="+GId;

}
</script>

</head>


<body onload="setContent()">

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">商品管理</a></li>
    <li><a href="#">商品管理</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
    <li><a href="javascript:goTab('${GId }');">编辑详情</a></li> 
    <li><a href="#tab2" class="selected">绑定资料</a></li> 
    <li><a href="javascript:goTab3('${GId }');">编辑属性</a></li>
    <li><a href="javascript:goTab4('${GId }');">设定关联</a></li>
    <li><a href="javascript:goTab5('${GId }');">设定替代</a></li>
    <li><a href="javascript:goTab6('${GId }');">管理库存</a></li>
  	</ul>
    </div> 
    <!-- 绑定资料 -->
  	<div id="tab2" class="tabson">
           <s:form method="post"  name="form1" theme="simple">   
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li><a id="go" rel="leanModal" name="signup" href="javascript:history.go(-1);" ><span><img src="images/t01.png" width="24" height="24" /></span>返回</a></li>
		<li onclick="submitform()"><span><img src="images/t04.png" /></span>绑定</li>
        </ul>

    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>商品ID</th>
        <th>商品名称</th>
        <th>商品货号</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        	<td width="228" align="center" class="modify">${goods.GId }</td>
        	<td width="212" align="center" class="modify">${goods.GName }</td>
        	<td width="212" align="center" class="modify">${goods.GSn }</td>
        </tr> 
        </tbody>
    </table>
    已绑定的资料：
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>资料名称</th>
        <th>文件路径</th>
        <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="viewresList" var="list">
        <tr>
        	<td width="228" align="center" class="modify">${list.RName }</td>
        	<td width="212" align="center" class="modify">${list.RUrl }</td>
        	<td width="212" align="center" class="modify"><a href="<%=request.getContextPath()%>/manage/unTieRnT_goods?RId=${list.RId }&GId=${list.GId }" >删除</a></td>
        </tr> 
        </s:iterator>
        </tbody>
    </table>
   
    <div class="pagin">
    <input type="hidden" name="GId" value="${goods.GId }" />
    	商品资料：<select id="RId" name="RId">
							<s:iterator value="resList" var="list">
								<option value="${list.RId }">${list.RName }</option>
							</s:iterator>
					</select><br/>
    </div>
    </div>
   </s:form>
	</div> 
 
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    
    
    
    
    </div>

</div>
</body>

</html>