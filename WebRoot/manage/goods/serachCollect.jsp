<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>公告管理</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="../js/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.all.js"></script>
<script type="text/javascript" charset="utf-8" src="../js/ueditor/lang/zh-cn/zh-cn.js"></script>
<link type="text/css" src="../js/ueditor/themes/default/css/ueditor.css"/>
<script type="text/javascript" src="../js/ajaxfileupload.js" charset="utf-8"></script>
</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">系统管理</a></li>
    <li><a href="list_notice">关键字为空</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" id="form1" theme="simple">   
 
 <input type="hidden" id="deleteA" value="delete_search_collect">
 
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
	        <li class="deleteItem" ><span><img src="images/t03.png" /></span>删除</li>
        </ul>
    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th><input type='checkbox' id='checkall' name='checkall' /></th>
        <th>搜索关键字</th>
        <th>搜索时间</th>
        <th>搜索次数</th>
        <th>当前状态</th>
        <th>操作</th>
        </tr>
        </thead>
        <tbody>
<s:iterator value="searchCollectsList" var="list">      
   <tr align="center">
	<td width="29" height="38" style="padding-left:0px">
	<input type="checkbox" name="mid" value=${list.coId } />
  	  </td>
   	<td width="400" align="center" class="modify"><s:property value="#list.searchKey"/></td>
   	<td width="400" align="center" class="modify"><s:property value="#list.searchTime"/></td>
   	<td width="400" align="center" class="modify"><s:property value="#list.SeachSum"/></td>
   	<td width="400" align="center" class="modify">
   		<c:if test="${list.isOptimise == '0' }">
   			未优化
   		</c:if>
   		<c:if test="${list.isOptimise != '0' }">
   			已优化
   		</c:if>
   	</td>
   	<td width="400" align="center" class="modify">
   		<c:if test="${list.isOptimise == '0' }">
	   		<a style="color: red;" href="javascript:;" onclick="okCollect(${list.coId })">确认优化</a>
   		</c:if>
   	</td>
   </tr> 
</s:iterator>       
        </tbody>
    </table>
   
     ${pages.pageStr}
     
   </s:form> 
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	
	
	

</body>
	<script type="text/javascript">
		function okCollect(coId){
			if(confirm("确认此条数据已经优化")){
				$.post(
					"okSearchCollect",
					{"coId":coId},
					function(data, varstart){
						if(data == "success"){
							location.reload();
						} else {
							alert("后台错误");
						}
					}
				);
			}
		}
	</script>
</html>
