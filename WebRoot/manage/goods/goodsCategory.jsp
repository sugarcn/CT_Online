<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>

<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/category.js" charset="utf-8"></script>


</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">系统管理</a></li>
    <li><a href="#">商品管理</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" id="form1" theme="simple">   
 
 <input type="hidden" id="deleteA" value="delete_goodsCategory">
 <input type="hidden" id="findA" value="find_goodsCategory">
 <input type="hidden" id="searchA" value="search_goodsCategory">
 <input type="hidden" id="updateA" value="update_goodsCategory"> 
 
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li rel="leanModal" name="signup" href="#signup"><span><img src="images/t01.png" width="24" height="24" /></span>添加</li>

		<li rel="leanModal1" class="modiItem" href="#signup1"><span><img src="images/t02.png" width="24" height="24" /></span>修改</li>

        <li class="deleteItem" ><span><img src="images/t03.png" /></span>删除</li>
		
		 <li rel="leanModalupload" name="signup" href="#signupupload"><span><img src="images/t01.png" width="24" height="24" /></span>导入</li>
		 
        </ul>

	<div class="ssk">
     
		   <div class="ssbk">
		      <div style="float:left;padding-left:30px;padding-top:1px"><input type="text" id="keyword" name="keyword" class="wbdd" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="${keyword}"></div>
		      <div style="float:right"><img src="images/ss.jpg" class="searchItem"></div>
		      <div class="clear"></div>
		   </div>
   
  </div>
    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th><input type='checkbox' id='checkall' name='checkall' /></th>
        <th>分类名称</th>
        <th>分类品牌</th>
        </tr>
        </thead>
        <tbody>
  <s:iterator value="categories" var="list">      
        <tr>
			<td width="5" height="38" style="padding-left:30px">
			<input type="checkbox" name="mid" value=${list.CId } />
       	  </td>
        	<td width="228" align="center" class="modify"><a href="list_goodsCategory?CId=<s:property value="#list.CId"/>"><s:property value="#list.CName"/></a></td>
        	<td width="228" align="center" class="modify"><s:property value="#list.BId"/></td>
        </tr>
</s:iterator>       
        </tbody>
    </table>
   ${pages.pageStr}
   </s:form> 
    
	
	<div id="signup" style=" clear:both;">
			<div id="signup-ct">
				<div id="signup-header">
					<h2>新建分类</h2>
					<a class="modal_close" href="#"></a>
				</div>
				
				<form method="post">
				  <div class="txt-fld">
				    <label for="">分类名称:</label>
				    <input id="" class="good_input" name="CName" type="text" /><br/>
				    <label for="">分类类型:</label>
				    <select id="type">
				    	<option value="0">一级分类</option>
				    	<option value="1">二级分类</option>
				    	<option value="2">三级分类</option>
				    </select>
				    <br/>
				    <div style="display: none;">
					    <br/>
					    <button type="button" id="addCateType" >添加品牌</button><br/>
					    <label for="">分类品牌:</label>
	<div class="ct_top_la"><input id="cateType" class="good_input" title="输入检索 &quot;名称&quot;" type="text" style="z-index: 80000;" /></div>
	<br/><br/> 
					   <label for="">品牌编号:</label>
					   <input id="textAddType" name="cateId" readonly="readonly" type="text" />
				    </div>
				  </div> 
				  <div class="btn-fld">
                  <div id="tipHtml" style="color:red;float:left">
                    </div>
				  <button type="submit" onclick="return checkThis(this.form,'add_goodsCategory',0);">确定</button>
</div>
					<input type="hidden" name="parentId" id="parentId" value="0" />
				 </form>
			</div>
							   
		</div>

		<div id="signup1" style="height: 350px;">
			<div id="signup-ct">
            <div id="signup-header">
					<h2>修改分类</h2>
					<a class="modal_close" href="#"></a>
				</div>
              <form method="post">
              <div id="signupmodi">
                    </div>
                    
              <div class="btn-fld">
              <div id="tipHtmlModi" style="color:red;float:left">
                    </div>
				  <button type="submit" onclick="return checkThis(this.form,'update_goodsCategory',1);">确定</button>
</div>
<input type="hidden" name="parentId" id="parentIdUpdate" value="0" />
				 </form>
                 </div>
		</div>
	
	
	
    <div id="signupupload">
			<div id="signup-ct">
				<div id="signup-header">
					<h2>导入</h2>
					<a class="modal_close" href="#"></a>
				</div>
				
				<form action="import_goodsCategory" method="post" enctype="multipart/form-data">
     
				  <div class="txt-fld">
				    <label for="">选择Excel文件</label>
                    <input type="file" name="excelFile" id="excelFile"  />
                  </div>

				  <div class="btn-fld">
				  <button type="submit">确定</button>
</div>
				 </form>
			</div>
		</div>  
    
    
    
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	$(function(){
		$("#typeUp").change(function(){
			
		});	
	});
	
	</script>
	
	

</body>

</html>
