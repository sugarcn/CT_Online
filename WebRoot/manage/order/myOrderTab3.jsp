<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>
<script type="text/javascript" src="js/goTab.js"></script>
<!-- 编辑详情 -->
	<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../js/ueditor/lang/zh-cn/zh-cn.js"></script>

</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/manage/list_order">订单列表</a></li>
    <li><a href="javascript:goOrderTab3('${ctOrderInfo.orderId }');">填写发货信息</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
  	<script type="text/javascript" src="js/tabTiao.js"></script>
    <li><a href="javascript:goOrderTab('${ctOrderInfo.orderId }');">基本信息</a></li> 
    <li><a href="javascript:goOrderTab2('${ctOrderInfo.orderId }');">支付审核</a></li> 
    <li><a href="javascript:goOrderTab3('${ctOrderInfo.orderId }');" class="selected">填写发货信息</a></li>
    <li><a href="javascript:goOrderTab4('${ctOrderInfo.orderId }');">商品信息</a></li>
    <li><a href="javascript:goOrderTab5('${ctOrderInfo.orderId }');">评价详情</a></li>
        <li><a href="javascript:goOrderTab9('${ctOrderInfo.orderId }');">商品退款</a></li>
    	<c:if test="${manager.ctRole.roleName=='客户经理' }">
	    <li><a href="javascript:goOrderTab7('${ctOrderInfo.orderId }');">退货明细-客户经理</a></li>
   	</c:if>
   	<c:if test="${manager.ctRole.roleName=='库房' }">
	    <li><a href="javascript:goOrderTab6('${ctOrderInfo.orderId }');">退货明细-库房审核</a></li>
   	</c:if>
   	<c:if test="${manager.ctRole.roleName=='财务' }">
	    <li><a href="javascript:goOrderTab8('${ctOrderInfo.orderId }');">退货明细-财务审核</a></li>
   	</c:if>
    
  	</ul>
    </div> 
    <!-- 编辑详情 -->
  	<div id="tab1" class="tabson">
    <form action="tieA_goods" name="submitform" method="post">
		<table  class="gridtable">
			<tbody>
				<tr>
				<input type="hidden" id="orderId" value="${ctOrderInfo.orderId }" />
					<th>收货人：</th>
					<td>${ctOrderInfo.consignee }</td>
					<td colspan="2">打印快递单</td>
				</tr>
				<tr>
					<th>地址：</th>
					<td colspan="3">${sheng}-${shi}-${qu}-${ctOrderInfo.address }</td>
				</tr>
				<tr>
					<th>电话：</th>
					<td>${ctOrderInfo.tel }</td>
					<th>邮编：</th>
					<td>${ctOrderInfo.zipcode }</td>
				</tr>
				
				<tr>
					<td colspan="4">
					</td>
				</tr>
				<tr>
					<th>配送物流：</th>
					<td colspan="3">
						<select name="ctOrderInfo.exId" id="exId">
							<c:forEach items="${ctExpressesList }" var="list">
								<c:if test="${list.exId == ctOrderInfo.exId }">
									<option selected="selected" value="${list.exId }">${list.exName }</option>
								</c:if>
								<c:if test="${list.exId != ctOrderInfo.exId }">
									<option value="${list.exId }">${list.exName }</option>
								</c:if>
							</c:forEach>
						</select>
						<!--<s:select list="ctExpressesList" listKey="exId" id="exId" listValue="exName" name="ctOrderInfo.exId"></s:select>-->
					</td>
				</tr>
				<tr>
					<th>发货单号：</th>
					<td colspan="3"><input value="${ctOrderInfo.exNo }" 
					<c:if test="${ctOrderInfo.orderStatus != 3 && ctOrderInfo.orderStatus != 1 && ctOrderInfo.orderStatus != 8 }">
						disabled="disabled"
					</c:if>
					 maxlength="15" type="text" id="exNo" style="width:100px;"/></td>
					
				</tr>
				<tr>
					 <th>客户备注：</th>
					 <td colspan="3">${ctOrderInfo.dsc }</td>
					 
				</tr>
				<tr>
					<th>客服备注：</th>
					 <td colspan="3">${ctOrderInfo.kfDsc }</td>
				</tr>
			</tbody>
		</table>
		
		<input type="hidden" id="orderId" value="${ctOrderInfo.orderId }" />
		<div  style="padding-top:20px; padding-left:200px;">
		
		<input type="button" class="go"
			<c:if test="${ctOrderInfo.orderStatus != 3 && ctOrderInfo.orderStatus != 1 && ctOrderInfo.orderStatus != 8 }">
				disabled="disabled"
			</c:if>
		 value="发货" onclick="updateExNo('${ctOrderInfo.shippingType}');" id="tiebtn">
		 </div>
    </form>
    </div> 
    
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>	
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    
    
    
    
    </div>

</div>
</body>

</html>
