<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>
<script type="text/javascript" src="js/goTab.js"></script>
<!-- 编辑详情 -->
	<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../js/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript">

  function setContent() {
		var detl = document.getElementById("GDetail").value;
        UE.getEditor('editor').setContent(detl);
    }
function getContent() {
        var value = UE.getEditor('editor').getContent();
        $("#GDetail").val(value);
        document.forms[0].action = "Edit_goods";
    	document.forms[0].submit();
        }
</script>
</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/manage/list_order">订单列表</a></li>
    <li><a href="javascript:goOrderTab5('${ctOrderInfo.orderId }');">评价详情</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
  	<script type="text/javascript" src="js/tabTiao.js"></script>
    <li><a href="javascript:goOrderTab('${ctOrderInfo.orderId }');">基本信息</a></li>
    <li><a href="javascript:goOrderTab2('${ctOrderInfo.orderId }');">支付审核</a></li> 
    <li><a href="javascript:goOrderTab3('${ctOrderInfo.orderId }');">填写发货信息</a></li>
    <li><a href="javascript:goOrderTab4('${ctOrderInfo.orderId }');">商品信息</a></li>
    <li><a href="javascript:goOrderTab5('${ctOrderInfo.orderId }');" class="selected">评价详情</a></li>
        <li><a href="javascript:goOrderTab9('${ctOrderInfo.orderId }');">商品退款</a></li>
    	<c:if test="${manager.ctRole.roleName=='客户经理' }">
	    <li><a href="javascript:goOrderTab7('${ctOrderInfo.orderId }');">退货明细-客户经理</a></li>
   	</c:if>
   	<c:if test="${manager.ctRole.roleName=='库房' }">
	    <li><a href="javascript:goOrderTab6('${ctOrderInfo.orderId }');">退货明细-库房审核</a></li>
   	</c:if>
   	<c:if test="${manager.ctRole.roleName=='财务' }">
	    <li><a href="javascript:goOrderTab8('${ctOrderInfo.orderId }');">退货明细-财务审核</a></li>
   	</c:if>
    
  	</ul>
    </div> 
    <!-- 编辑详情 -->
  	<div id="tab1" class="tabson">
    <form action="tieA_goods" name="submitform" method="post">
		<table width="800px;">
			<tbody>
				<c:if test="${evaluation.evaTime != null }">
					<tr>
						<td>订单编号：</td>
						<td>${ctOrderInfo.orderId }</td>
					</tr>
					<tr>
						<td>评价内容：</td>
						<td>${evaluation.evaDesc }</td>
					</tr>
					<tr>
						<td>评价时间：</td>
						<td>${evaluation.evaTime }</td>
					</tr>
					<tr>
						<td>评价用户：</td>
						<td>${evaluation.user.UUsername }</td>
					</tr>
				</c:if>
				<c:if test="${evaluation.evaTime == null }">
					<tr>
						<td>暂无评价</td>
					</tr>
				</c:if>
			</tbody>
		</table>
    </form>
    </div> 
    
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    
    
    
    
    </div>

</div>
</body>

</html>
