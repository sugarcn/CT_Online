<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>
<script type="text/javascript" src="js/goTab.js"></script>
<!-- 编辑详情 -->
	<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../js/ueditor/lang/zh-cn/zh-cn.js"></script>

</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/manage/list_order">订单列表</a></li>
    <li><a href="javascript:goOrderTab2('${ctOrderInfo.orderId }');">支付审核</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
  	<script type="text/javascript" src="js/tabTiao.js"></script>
    <li><a href="javascript:goOrderTab('${ctOrderInfo.orderId }');">基本信息</a></li> 
    <li><a href="javascript:goOrderTab2('${ctOrderInfo.orderId }');" class="selected">支付审核</a></li> 
    <li><a href="javascript:goOrderTab3('${ctOrderInfo.orderId }');">填写发货信息</a></li>
    <li><a href="javascript:goOrderTab4('${ctOrderInfo.orderId }');">商品信息</a></li>
    <li><a href="javascript:goOrderTab5('${ctOrderInfo.orderId }');">评价详情</a></li>
        <li><a href="javascript:goOrderTab9('${ctOrderInfo.orderId }');">商品退款</a></li>
   	<c:if test="${manager.ctRole.roleName=='客户经理' }">
	    <li><a href="javascript:goOrderTab7('${ctOrderInfo.orderId }');">退货明细-客户经理</a></li>
   	</c:if>
   	<c:if test="${manager.ctRole.roleName=='库房' }">
	    <li><a href="javascript:goOrderTab6('${ctOrderInfo.orderId }');">退货明细-库房审核</a></li>
   	</c:if>
   	<c:if test="${manager.ctRole.roleName=='财务' }">
	    <li><a href="javascript:goOrderTab8('${ctOrderInfo.orderId }');">退货明细-财务审核</a></li>
   	</c:if>
    
  	</ul>
    </div> 
    <!-- 编辑详情 -->
  	<div id="tab1" class="tabson">
    <form action="tieA_goods" name="submitform" method="post">
		<table class="gridtable">
			<tbody>
				<tr>
					<th>我司账号：</th>
					<td>${bank.accountNumber }</td>
					<th>开户行：</th>
					<td>${bank.deposit }</td>
				</tr>
				<tr>
					<th>开户名：</th>
					<td>${bank.accountName }</td>
					<th>账号：</th>
					<td>${bank.accountNumber }</td>
				</tr>
				<tr>
					<th>姓名：</th>
					<td>${ctPay.CName }</td>
					<th>电话：</th>
					<td>${ctPay.CTel }</td>
				</tr>
				<tr>
					<th>开户名：</th>
					<td colspan="3">${ctPay.CAName }</td>
				</tr>
				<tr>
					<th>转入金额：</th>
					<td>${ctPay.payAmount }</td>
					<th>转入日期：</th>
					<td>${ctPay.payDate }</td>
				</tr>
				<tr>
					<th>文件：</th>
					<td align="center" colspan="3">
						<c:if test="${ctPay.payFile == null }">
							<a href="javascript:;">未找到文件</a>
						</c:if>
						<c:if test="${ctPay.payFile != null }">
						 	<a target="_blank" style="color: red;" href="${ctPay.payFile }">点击查看</a>
						</c:if>
					 </td>
				</tr>
				<tr>
					<th>备注：</th>
					<td colspan="3">${ctPay.payDesc }</td>
				</tr>
			</tbody>
		</table>
		<div  style="padding-top:20px; padding-left:200px;">
		<input type="hidden" id="orderId" value="${ctOrderInfo.orderId }" />
		<input type="button"  class="go"
			<c:if test="${ctOrderInfo.orderStatus != 2 }">
				disabled="disabled"
			</c:if>
		 value="通过" onclick="updateType()" id="tiebtn">
		<input type="button"  class="go"
			<c:if test="${ctOrderInfo.orderStatus != 2 }">
				disabled="disabled"
			</c:if>
		 onclick="updateTypeForClence()" value="无效" />
		 </div>
    </form>
	
    </div> 
    
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    </div>

</div>
</body>

</html>
