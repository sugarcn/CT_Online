<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>
<script type="text/javascript" src="js/goTab.js"></script>
<!-- 编辑详情 -->
	<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../js/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript">

  function setContent() {
		var detl = document.getElementById("GDetail").value;
        UE.getEditor('editor').setContent(detl);
    }
function getContent() {
        var value = UE.getEditor('editor').getContent();
        $("#GDetail").val(value);
        document.forms[0].action = "Edit_goods";
    	document.forms[0].submit();
        }
</script>
</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/manage/list_order">订单列表</a></li>
    <li><a href="javascript:goOrderTab6('${ctOrderInfo.orderId }');">退货详情</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
  	<script type="text/javascript" src="js/tabTiao.js"></script>
    <li><a href="javascript:goOrderTab('${ctOrderInfo.orderId }');">基本信息</a></li> 
    <li><a href="javascript:goOrderTab2('${ctOrderInfo.orderId }');">支付审核</a></li> 
    <li><a href="javascript:goOrderTab3('${ctOrderInfo.orderId }');">填写发货信息</a></li>
    <li><a href="javascript:goOrderTab4('${ctOrderInfo.orderId }');">商品信息</a></li>
    <li><a href="javascript:goOrderTab5('${ctOrderInfo.orderId }');">评价详情</a></li>
    <li><a href="javascript:goOrderTab9('${ctOrderInfo.orderId }');">商品退款</a></li>
   	<c:if test="${manager.ctRole.roleName=='客户经理' }">
	    <li><a href="javascript:goOrderTab7('${ctOrderInfo.orderId }');" class="selected">退货明细-客户经理</a></li>
   	</c:if>
   	<c:if test="${manager.ctRole.roleName=='库房' }">
	    <li><a href="javascript:goOrderTab6('${ctOrderInfo.orderId }');" class="selected">退货明细-库房审核</a></li>
   	</c:if>
   	<c:if test="${manager.ctRole.roleName=='财务' }">
	    <li><a href="javascript:goOrderTab8('${ctOrderInfo.orderId }');" class="selected">退货明细-财务审核</a></li>
   	</c:if>
    
  	</ul>
    </div> 
    <!-- 编辑详情 -->
  	<div id="tab1" class="tabson">
    <form action="tieA_goods" name="submitform" method="post">
		<table width="400px;">
			<tbody>
			<c:if test="${ctOrderInfo.retTime != null }">
				<tr>
					<td>订单号：${ctOrderInfo.orderSn }</td>
				</tr>
				<tr>
					<td>退款金额：${ctOrderInfo.retNumber }</td>
				</tr>
				<tr>
					<td>退款人：${ctOrderInfo.ctUser.UUsername }</td>
				</tr>
				<tr>
					<td>联系方式：${ctOrderInfo.tel }</td>
				</tr>
				<tr>
					<td>退款时间：${ctOrderInfo.retTime }</td>
				</tr>
				<tr>
					<td>确认商品回库人员：${ctOrderInfo.ware }</td>
				</tr>
				<tr>
					<td>退款时间：${ctOrderInfo.wareTime }</td>
				</tr>
				<tr>
					<td>客户经理：${ctOrderInfo.manage }</td>
				</tr>
				<tr>
					<td>同意时间：${ctOrderInfo.manageTime }</td>
				</tr>
				<tr>
					<td>${ctOrderInfo.retStatus }</td>
				</tr>
				<tr>
					<td id="luIsOk">
					<input type="hidden" id="orderIdForTk" value="${ctOrderInfo.orderId }" />
					
					<c:if test="${ctOrderInfo.retStatus == '财务审核' }">
						<input id="tkIsKu" type="button" value="已经退款" /></td>
					</c:if>
				</tr>
			</c:if>
			<c:if test="${ctOrderInfo.retTime == null }">
				<tr>
					<td>此订单暂无退货记录</td>
				</tr>
			</c:if>
			</tbody>
		</table>
		<script type="text/javascript">
			$(function(){
				$("#tkIsKu").click(function(){
					var orderIdForTk = $("#orderIdForTk").val();
					$.post(
							"${pageContext.request.contextPath}/manage/updateReturnFinanceByOrderId",
							{"orderDTO.orderid":orderIdForTk},
							function(data,varStart){
								$("#tkIsKu").remove();
								$("#luIsOk").html("退款成功");
						}
					);
				});
			});
		</script>
		
		<input type="hidden" id="orderId" value="${ctOrderInfo.orderId }" />
    </form>
    </div> 
    
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    
    
    
    
    </div>

</div>
</body>

</html>
