﻿<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>
<script type="text/javascript" src="js/goTab.js"></script>
<!-- 编辑详情 -->
	<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../js/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript">

  function setContent() {
		var detl = document.getElementById("GDetail").value;
        UE.getEditor('editor').setContent(detl);
    }
function getContent() {
        var value = UE.getEditor('editor').getContent();
        $("#GDetail").val(value);
        document.forms[0].action = "Edit_goods";
    	document.forms[0].submit();
        }
</script>
</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/manage/list_order">订单列表</a></li>
    <li><a href="javascript:goOrderTab9('${ctOrderInfo.orderId }');">商品退款</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
  	<script type="text/javascript" src="js/tabTiao.js"></script>
    <li><a href="javascript:goOrderTab('${ctOrderInfo.orderId }');">基本信息</a></li>
    <li><a href="javascript:goOrderTab2('${ctOrderInfo.orderId }');">支付审核</a></li> 
    <li><a href="javascript:goOrderTab3('${ctOrderInfo.orderId }');">填写发货信息</a></li>
    <li><a href="javascript:goOrderTab4('${ctOrderInfo.orderId }');">商品信息</a></li>
    <li><a href="javascript:goOrderTab5('${ctOrderInfo.orderId }');">评价详情</a></li>
    <li><a href="javascript:goOrderTab9('${ctOrderInfo.orderId }');" class="selected">商品退款</a></li>
    	<c:if test="${manager.ctRole.roleName=='客户经理' }">
	    <li><a href="javascript:goOrderTab7('${ctOrderInfo.orderId }');">退货明细-客户经理</a></li>
   	</c:if>
   	<c:if test="${manager.ctRole.roleName=='库房' }">
	    <li><a href="javascript:goOrderTab6('${ctOrderInfo.orderId }');">退货明细-库房审核</a></li>
   	</c:if>
   	<c:if test="${manager.ctRole.roleName=='财务' }">
	    <li><a href="javascript:goOrderTab8('${ctOrderInfo.orderId }');">退货明细-财务审核</a></li>
   	</c:if>
    
  	</ul>
    </div> 
    <!-- 编辑详情 -->
    <c:if test="${zanwurefund == 'you' }">
	  	<div id="tab1" class="tabson">
	    <form action="tieA_goods" name="submitform" method="post">
	      <div id="myPrintArea">  
	      <input type="hidden" value="${ctOrderInfo.pay }" id="payType" />
		        <div align="center" style="font-size:22px; font-weight:bold;padding-bottom:50px;">长亭易购（www.ctego.com） 购物清单 </div>  
		  <div style="float:left;padding-left:10px;">订单编号：${ctOrderInfo.orderSn}</div>
		   <div  style="float:left;padding-left:250px" >退款总金额：${allPrice}</div>
		    <div style="margin-left:800px">已经退款：${okPrice}</div>
		    <div style="margin-left:800px">剩余退款：${shengPrice}</div>
			<div class="rightinfo">
			<div style="padding-bottom:20px;">
			<div style="text-align: center;">
				<input type="checkbox"
					<c:if test="${isRefStock == 1 }">
						 disabled="disabled"   name="shifouyunfei"
					</c:if>
					
					<c:if test="${isRefStock != 1 }">
						 name="shifouyunfei" id="shifouyunfei"
					</c:if>
				  />是否退运费
			</div>
			    <table class="tablelist">
			    	<thead>
			    	<tr align="center">
				        <th>请选择</th>
				        <th>商品编号</th>
				        <th>商品名称</th>
				        <th>数量</th>
				        <th>样品或批量</th>
				        <th>单价</th>
				        <th>退款金额</th>
				        <th>当前状态</th>
				        <th>更新退款金额</th>
			        </tr>
			        </thead>
			        <tbody>
			        <c:forEach items="${orderGoodsListRefund }" var="list">
				        <tr>
				        <!-- disabled="disabled" -->
				        	<td width="105" height="40" align="center" class="modify">
								<input type="checkbox"
								<c:if test="${list.isRefund==2 || list.isRefund==3 }">
								 disabled="disabled"
								</c:if>
								value="${list.GId }__${ctOrderInfo.orderId}__${list.isParOrSim}" name="refundCheck" />
							</td>
								<!-- disabled="disabled"  -->
				        	<td width="135" height="40" align="center" class="modify">${list.GId }</td>
				        	<td width="346" align="center" class="modify">${list.ctGoods.GName }</td>
				        	<c:if test="${list.isParOrSim == 1 }">
					        	<td width="123" align="center" class="modify">${list.GNumber }</td>
					        	<td width="114" align="center" class="modify">样品</td>
					        	<td width="173" align="center" class="modify">${list.GPrice }</td>
				        	</c:if>
				        	<c:if test="${list.isParOrSim == 0 }">
					        	<td width="123" align="center" class="modify">${list.GParNumber }</td>
					        	<td width="114" align="center" class="modify">小批量</td>
					        	<td width="173" align="center" class="modify">${list.GParPrice }</td>
				        	</c:if>
				        	<td width="181" align="center" class="modify">${list.GSubtotal }</td>
				        	<td width="181" align="center" class="modify">
				        		<c:if test="${list.isRefund==2 }">
				        			已退款 退款金额 ${list.refPrice }
				        		</c:if>
				        		<c:if test="${list.isRefund==1 }">
				        			等待退款
				        		</c:if>
				        		<c:if test="${list.isRefund==3 }">
				        			退款驳回
				        		</c:if>
				        		
				        	</td>
				        	<td width="181" align="center" class="modify">
				        		<c:if test="${list.isRefund!=2 && list.isRefund!=3 }">
								 	<input style="width: 95%; boder:1px solid #999;" id="${list.GId }__${ctOrderInfo.orderId}__${list.isParOrSim}upRefundPrice" type="text" onblur="checkPriceIsOk(this,${list.GSubtotal },'${list.GId }__${ctOrderInfo.orderId}__${list.isParOrSim}')"  />
									<input type="hidden" id="${list.GId }__${ctOrderInfo.orderId}__${list.isParOrSim}hidPr" />
								</c:if>
				        	</td>
				        </tr>
			        </c:forEach>
			        </tbody>
			    </table>
			    
				<p></p>
			</div>	
		
	    </div>
	    	
		<div style="width: 95%;padding-top:100px;">
	    	<label style="float: right; font-weight:bold;font-size:18px;" >
	    		<input type="button" value="同意" onclick="gorefund();" class="go" />
	    		<input type="button" value="驳回" 
	    		<c:if test="${shengPrice == '0.0'}">
	    			 disabled="disabled"
	    		</c:if>
	    		<c:if test="${shengPrice != '0.0'}">
	    			 onclick="norefund('${ctOrderInfo.orderSn}');"
	    		</c:if>
	    		 class="go" />
	    	</label>
    	</div>
		    </div> 
			
	    </form>
	    </div> 
    </c:if>
    <c:if test="${zanwurefund == 'zanwurefund' }">
    	暂无退款信息
    </c:if>
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
    function checkPriceIsOk(yuan, total,ss){
    	if(yuan.value == ''){
    		//alert("未输入值");
    	} else {
    		if(Number(yuan.value) <= total){
	    		//alert();
	    		
	    		$("#"+ss+"hidPr").val(Number(yuan.value)-total);
    		} else {
    			//alert("超出该商品的最大价格");
    			//yuan.value="";
    		}
    	}
    }
    
	$('.tablelist tbody tr:odd').addClass('odd');
	
	
	function gorefund(){
		var str = "";
		var ch = $("input[name=refundCheck]");
		var upprice = "";
		var shifoushoudong = false;
		for(var i=0; i < ch.length; i++){
			var aa = ch[i].checked;
			if(aa){
				str += ch[i].value + "_____";
				var isPanDuan = $("#"+ch[i].value+"hidPr").val();
				if(isPanDuan != '' && isPanDuan != null && isPanDuan != '0'){
					upprice += "|"+ isPanDuan + "++" + ch[i].value;
					shifoushoudong = true;
				}
			}
			//alert($("#"+ch[i].value+"hidPr").val());
		}
		//alert(upprice);
		var yunfei = $("#shifouyunfei").attr("checked");
		var yun = 0;
		if(yunfei == "checked"){
			yun = 10;
		} else {
		}
		if(str == ""){
			alert("请至少选择一项");
			return;
		}
		$.post(
				"manage/goRefund",
				{"data":str,"total":upprice,"Discount":yun},
				function(data, varStrart){
					var typePay = $("#payType").val();
					if(typePay == "3"){
						if(data == '退款成功'){
							location.reload();
						} else {
							alert(data);
							location.reload();
						}
					} else if(typePay == "2"){
						if(data != '退款成功' && data != '退款失败'){
							$("body").html(data);
						}
					}
				}	
		);
	}
	function norefund(orderSn){
		var ch = $("input[name=refundCheck]");
		var isflag = false;
		var pid = "";
		for(var i=0; i < ch.length; i++){
			var aa = ch[i].checked;
			if(aa){
				isflag = true;
				pid = pid + ch[i].value + "++";
			}
		}
		$.post(
				"manage/noRefund",
				{"orderSn":orderSn,"data":pid},
				function(data, varStrart){
					alert(data);
					location.href="list_order";
				}
		);
	}
	</script>
    
    
    
    
    
    </div>

</div>
</body>

</html>
