<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/userRank.js" charset="utf-8"></script>
<script type="text/javascript">
function  goTabForOrder(orderid){
	window.location.href="order_Tab?orderId="+orderid;

	}
</script>
</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/manage/list_order">订单管理</a></li>
    <li><a href="#">投诉建议</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" id="form1" theme="simple">   
 
    <div class="rightinfo">
    
    
    <table class="tablelist">
    	<thead>
    	<tr align="center">
        <th>投诉编号</th>
        <th>投诉类型</th>
        <th>投诉单号</th>
        <th>投诉用户</th>
        <th>电话</th>
        <th>投诉内容</th>
        <th>投诉状态</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${complainList }" var="list">
	        <tr>
	        	<td width="228" height="40" align="center" class="modify">
	        		<a href="complain_getOne?complain.comId=${list.comId }">${list.comId }</a>
	        	</td>
	        	<td width="228" align="center" class="modify">${list.comType }</td>
	        	<td width="228" align="center" class="modify">${list.orderInfo.orderSn }</td>
	        	<td width="228" align="center" class="modify">${list.user.UUsername }</td>
	        	<td width="228" align="center" class="modify">${list.UTel }</td>
	        	<td width="228" align="center" class="modify">
	        		${list.comContent }
	        	</td>
	        	<td width="228" align="center" class="modify">
	        		<c:if test="${list.comStatus == '1' }">
	        			待处理
	        		</c:if>
	        		<c:if test="${list.comStatus == '2' }">
	        			处理完成
	        		</c:if>
	        	</td>
	        </tr> 
        </c:forEach>
        </tbody>
    </table>
    </div>
   </s:form> 
    	 ${pages.pageStr}
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
</body>

</html>
