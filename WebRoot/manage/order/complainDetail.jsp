<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js"></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
<link rel="stylesheet" type="text/css" href="js/styles.css" />
<link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
<link rel="stylesheet" href="css/mycss.css" type="text/css" />
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/userRank.js" charset="utf-8"></script>
<script type="text/javascript">
	function goTabForOrder(orderid) {
		window.location.href = "order_Tab?orderId=" + orderid;

	}
</script>
</head>


<body>

	<div class="place">
		<span>位置：</span>
		<ul class="placeul">
			<li><a href="${pageContext.request.contextPath}/manage/list_order">订单管理</a>
			</li>
			<li><a href="#">投诉建议</a>
			</li>
			<li><a href="#">投诉详情</a>
			</li>
		</ul>
	</div>

	<div class="formbody">

		<div class="formtitle">
			<span>投诉详情</span>
		</div>
		<script type="text/javascript">
			function complainHuiFu(){
				var $ansContent = $("#ansContent").val();
				var $ansTips = $("#ansTips").val();
				if($ansContent.length == 0){
					alert("您还没有输入回复内容呢");
					return;
				}
				if($ansTips.length == 0){
					alert("您还没有输入温馨提示呢");
					return;
				}
				$("#updateCom").submit();
			}
		</script>
		<form action="manage/complain_updateOk" id="updateCom" name="updateCom"
			method="post">
			<input type="hidden" id="CouponId" name="couponId" value="" />
			<ul class="forminfo">
				<li class="infoer"><label>投诉编号：</label><p>${complain.comId }<input type="hidden" id="comId" name="complain.comId" value="${complain.comId }" /></p></li>
				<li class="infoer"><label>投诉类型：</label><p>${complain.comType }</p>
				</li>
				<li class="infoer"><label>投诉的订单号：</label><p>${complain.orderInfo.orderSn }</p>
				</li>
				<li class="infoer"><label>投诉的用户：</label><p>${complain.user.UUsername }</p>
				</li>
				<li><label>电 话：</label><p>${complain.UTel }</p>
				</li>
				<li><label>投诉内容：</label><p style="width:455px; ">${complain.comContent }</p>
				</li>
				<li><label>图片：</label><p><img width="180px;" height="180px;" src="http://127.0.0.1:8080//CT_Web//images//${complain.comFile}"></p>
				</li>
				<li><label>备注：</label><p style="width:455px;">${complain.comDesc }</p>
				</li>
				<li><label>回复内容：</label><p>
				<textarea rows="10" id="ansContent" name="complain.ansContent" >${complain.ansContent }</textarea></p>
				</li>
				<li><label>温馨提示：</label><p><input id="ansTips" name="complain.ansTips"
						type="text" value="${complain.ansTips }" class="dfinput" /></p>
				</li>
				<li><label>&nbsp;</label><p>
				<c:if test="${complain.comStatus == '1' }">
					<input id="btnHui" onclick="complainHuiFu();" type="button" class="btn" value="回复" /></p>
				</c:if>
				<c:if test="${complain.comStatus == '2' }">
					<input onclick="history.go(-1);" type="button" class="btn" value="后退" />
				</c:if>
				</li>
				<div style="clear:both"></div>
			</ul>

		</form>
	</div>

	<script type="text/javascript">
		$('.tablelist tbody tr:odd').addClass('odd');
	</script>
</body>

</html>
