<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="frm" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>
<script type="text/javascript" src="js/goTab.js"></script>
<script type="text/javascript" src="../js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="../js/jquery.PrintArea.js"></script>
<!-- 编辑详情 -->
	<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../js/ueditor/lang/zh-cn/zh-cn.js"></script>
   
    <script type="text/javascript">    
        $(function(){     
            //打印     
            $("#btnPrint").bind("click",function(event){     
                $("#myPrintArea").printArea();     
            });     
        });     
    </script>  

    <style type="text/css">
<!--
.STYLE2 {font-size: 24px}
-->
    </style>
</head>


<body>



	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/manage/list_order">订单列表</a></li>
    <li><a href="javascript:goOrderTab4('${ctOrderInfo.orderId }');">商品信息</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
  	<script type="text/javascript" src="js/tabTiao.js"></script>
    <li><a href="javascript:goOrderTab('${ctOrderInfo.orderId }');">基本信息</a></li>
    <li><a href="javascript:goOrderTab2('${ctOrderInfo.orderId }');">支付审核</a></li> 
    <li><a href="javascript:goOrderTab3('${ctOrderInfo.orderId }');">填写发货信息</a></li>
    <li><a href="javascript:goOrderTab4('${ctOrderInfo.orderId }');" class="selected">商品信息</a></li>
  	<li><a href="javascript:goOrderTab5('${ctOrderInfo.orderId }');">评价详情</a></li>
  	    <li><a href="javascript:goOrderTab9('${ctOrderInfo.orderId }');">商品退款</a></li>
  		<c:if test="${manager.ctRole.roleName=='客户经理' }">
	    <li><a href="javascript:goOrderTab7('${ctOrderInfo.orderId }');">退货明细-客户经理</a></li>
   	</c:if>
   	<c:if test="${manager.ctRole.roleName=='库房' }">
	    <li><a href="javascript:goOrderTab6('${ctOrderInfo.orderId }');">退货明细-库房审核</a></li>
   	</c:if>
   	<c:if test="${manager.ctRole.roleName=='财务' }">
	    <li><a href="javascript:goOrderTab8('${ctOrderInfo.orderId }');">退货明细-财务审核</a></li>
   	</c:if>
    
  	</ul>
    </div> 
    <!-- 编辑详情 -->
  	<div id="tab1" class="tabson">
    <form action="tieA_goods" name="submitform" method="post">
	
	        <input id="btnPrint" type="button" value="打印" class="go"/>  

      <div id="myPrintArea">  
	  
	  
	        <div align="center" style="font-size:22px; font-weight:bold;padding-bottom:50px;">长亭易购（www.ctego.com） 购物清单 </div>  
	  
	  <div style="float:left;padding-left:10px;">订单编号：${orderSn}</div> <div  style="float:left;padding-left:250px" >订购时间：${orderTime}</div> <div  style="margin-left:800px">客户姓名：${consignee}</div>

		<div class="rightinfo">
		<div style="font-size:18px; font-weight:bold;padding-top:30px;" >样品商品</div>
		<div style="padding-bottom:20px;">
		    <table class="tablelist">
		    	<thead>
		    	<tr align="center">
			        <th>序号</th>
			        <th>商品编号</th>
			        <th>商品名称</th>
			        <th>数量</th>
			        <th>单位</th>
			        <th>价格</th>
			        <th>小计（元）</th>
		        </tr>
		        </thead>
		        <tbody>
		        <c:forEach items="${orderGoodsSamList }" var="list" varStatus="starts">
			        <tr>
			        	<td width="135" height="40" align="center" class="modify">${starts.index+1 }</td>
			        	<td width="135" height="40" align="center" class="modify">${list.ctGoods.GSn }</td>
			        	<td width="546" align="center" class="modify">${list.ctGoods.GName }</td>
			        	<td width="123" align="center" class="modify">${list.GNumber }</td>
			        	<td width="114" align="center" class="modify">${list.ctGoods.GUnit }</td>
			        	<td width="173" align="center" class="modify">${list.GPrice }</td>
			        	<td width="181" align="center" class="modify">${list.GSubtotal }</td>
			        </tr>
		        </c:forEach>
		        </tbody>
		    </table>
			<p></p>
		</div>	
			
			<div  style="font-size:18px; font-weight:bold; margin-top:85px;">批量商品</div>
			<div>
		    <table class="tablelist">
		    	<thead>
		    	<tr align="center">
			        <th>序号</th>
			        <th>商品编号</th>
			        <th>商品名称</th>
			        <th>数量</th>
			        <th>单位</th>
			        <th>价格</th>
			        <th>小计（元）</th>
		        </tr>
		        </thead>
		        <tbody>
		        <c:forEach items="${orderGoodsList }" var="list" varStatus="starts">
			        <tr>
			        	<td width="135" height="40" align="center" class="modify">${starts.index+1 }</td>
			        	<td width="135" height="40" align="center" class="modify">${list.ctGoods.GSn }</td>
			        	<td width="546" align="center" class="modify">${list.ctGoods.GName }</td>
			        	<td width="123" align="center" name="parnum" class="modify">${list.GParNumber }</td>
			        	<td width="114" align="center" class="modify">K</td>
			        	<td width="173" align="center" class="modify">${list.GParPrice }</td>
			        	<td width="181" align="center" class="modify">${list.GSubtotal }</td>
			        </tr>
		        </c:forEach>
		        </tbody>
		    </table>
			</div>
    </div>
    	<div style="width: 95%;padding-top:100px;">
	    	<label style="float: right; font-weight:bold;font-size:18px;" >总计:<frm:formatNumber pattern="0.00">${totalY }</frm:formatNumber>元&nbsp;&nbsp;实付：<frm:formatNumber pattern="0.00">${total }</frm:formatNumber>元</label>
    	</div>
	    </div> 
		<c:if test="${ctOrderInfo.orderStatus == 1 }">
		<input type="button" class="btn"
			onclick="beihuoOk('${orderSn}');"
			value="完成备货"
		   id="tiebtn">
		</c:if>
		
    </form>
    </div> 
    
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	function beihuoOk(orderSn){
		//alert(orderSn);
		$.post(
			"manage/beihuoOk",
			{"orderSn":orderSn},
			function(data,varstart){
				alert("备货完成");
				window.location.reload();
			}
		);
	}
	function piliangnum(){
		var num = $("td[name=parnum]");
		for(var i=0; i<num.length; i++){
			var num1 = Number(num[i].textContent) / 1000;
			num1 = Number(num1).toFixed(2);
			var chai = num1.split("\.");
			if(chai[1] == "00"){
				num[i].textContent = chai[0];
			} else {
				num[i].textContent = num1;
			}
		}
	}
	piliangnum();
	</script>
    
    
    
    
    
    </div>

</div>
</body>

</html>
