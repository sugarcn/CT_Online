<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="frm" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>
<script type="text/javascript" src="js/goTab.js"></script>
<!-- 编辑详情 -->
	<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../js/ueditor/lang/zh-cn/zh-cn.js"></script>

</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/manage/list_order">订单列表</a></li>
    <li><a href="javascript:goOrderTab('${ctOrderInfo.orderId }');">基本信息</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
  	<script type="text/javascript" src="js/tabTiao.js"></script>
    <li><a href="javascript:goOrderTab('${ctOrderInfo.orderId }');" class="selected">基本信息</a></li> 
    <li><a href="javascript:goOrderTab2('${ctOrderInfo.orderId }');">支付审核</a></li> 
    <li><a href="javascript:goOrderTab3('${ctOrderInfo.orderId }');">填写发货信息</a></li>
    <li><a href="javascript:goOrderTab4('${ctOrderInfo.orderId }');">商品信息</a></li>
    <li><a href="javascript:goOrderTab5('${ctOrderInfo.orderId }');">评价详情</a></li>
        <li><a href="javascript:goOrderTab9('${ctOrderInfo.orderId }');">商品退款</a></li>
   	<c:if test="${manager.ctRole.roleName=='客户经理' }">
	    <li><a href="javascript:goOrderTab7('${ctOrderInfo.orderId }');">退货明细-客户经理</a></li>
   	</c:if>
   	<c:if test="${manager.ctRole.roleName=='库房' }">
	    <li><a href="javascript:goOrderTab6('${ctOrderInfo.orderId }');">退货明细-库房审核</a></li>
   	</c:if>
   	<c:if test="${manager.ctRole.roleName=='财务' }">
	    <li><a href="javascript:goOrderTab8('${ctOrderInfo.orderId }');">退货明细-财务审核</a></li>
   	</c:if>
    
  	</ul>
    </div> 
    <!-- 编辑详情 -->
  	<div id="tab1" class="tabson">

    <form action="tieA_goods" name="submitform" method="post">
    	<table class="gridtable">
    		<tbody>
    			<tr>
    				<th>订单号：</th>
    				<td>${ctOrderInfo.orderSn }</td>
    				<th>订单状态：</th>
    				<td>
    					<c:if test="${ctOrderInfo.orderStatus == 0 }">待付款</c:if>
		        		<c:if test="${ctOrderInfo.orderStatus == 1 }">已付款</c:if>
		        		<c:if test="${ctOrderInfo.orderStatus == 2 }">待审核</c:if>
		        		<c:if test="${ctOrderInfo.orderStatus == 3 }">配货中</c:if>
		        		<c:if test="${ctOrderInfo.orderStatus == 4 }">已发货</c:if>
		        		<c:if test="${ctOrderInfo.orderStatus == 5 }">已完成</c:if>
		        		<c:if test="${ctOrderInfo.orderStatus == 6 }">取消</c:if>
		        		<c:if test="${ctOrderInfo.orderStatus == 7 }">无效</c:if>
		        		<c:if test="${ctOrderInfo.orderStatus == 8 }">已配货</c:if>
    				</td>
    			</tr>
    			<tr>
    				<th>购货人：</th>
    				<td>${ctUser.UUsername }</td>
    				<th>下单时间：</th>
    				<td>${ctOrderInfo.orderTime }</td>
    			</tr>
    			<tr>
    				<th>支付方式：</th>
    				<td>
    					<c:if test="${ctOrderInfo.total != '0.0' && ctOrderInfo.orderStatus != 0 && ctOrderInfo.orderStatus != 7 && ctOrderInfo.orderStatus != 6}">
	    					<c:if test="${ctOrderInfo.pay==2 }">
	    						支付宝支付
	    					</c:if>
	    					<c:if test="${ctOrderInfo.pay==3 }">
	    						微信支付
	    					</c:if>
	    					<c:if test="${ctOrderInfo.pay==4 }">
	    						在线银联支付
	    					</c:if>
	    					<c:if test="${ctOrderInfo.pay==1 }">
	    						线下支付
	    					</c:if>
    					</c:if>
    				</td>
    				<th>付款时间：</th>
    				<td>
    					<c:if test="${ctOrderInfo.orderStatus != 0 }">
	    					<c:if test="${ctOrderInfo.pay==1 }">
	    						${ctPay.payDate }
	    					</c:if>
	    					<c:if test="${ctOrderInfo.pay!=1 }">
	    						${payInterface.payDate }
	    					</c:if>
    					</c:if>
    				</td>
    			</tr>
    			<tr>
    				<th>配送方式：</th>
    				<td>
    					<c:if test="${ctOrderInfo.shippingType==1 }">
    						长亭配送
    					</c:if>
    					<c:if test="${ctOrderInfo.shippingType==2 }">
    						快递配送
    						<c:if test="${ ctOrderInfo.shoppingTypeCollect == true}"> 运费到付</c:if>
    					</c:if>
    					<c:if test="${ctOrderInfo.shippingType==3 }">
    						物流配送
    						<c:if test="${ ctOrderInfo.shoppingTypeCollect == true}"> 运费到付</c:if>
    					</c:if>
    					<c:if test="${ctOrderInfo.shippingType==4 }">
    						自提配送
    					</c:if>
    				</td>
    				<th>自提地址：</th>
    				<td>
    					<c:if test="${ctOrderInfo.shippingType == 4 }">
    						${ctOrderInfo.shopIdName }
    					</c:if>
    					<c:if test="${ctOrderInfo.shippingType != 4 }">
    						无
    					</c:if>
    				</td>
    			</tr>
    			<tr>
    				<th>总金额：</th>
    				<td><font color="red">
    				<input type="hidden" id="orderId" value="${ctOrderInfo.orderId }" />
    				<input id="totalProce"
    					<c:if test="${ctOrderInfo.orderStatus != 0 }">
    						  readonly="readonly"
    					</c:if>
    				 type="text" size="5" id="payAmount"  value="${ctOrderInfo.total }" />
    				 <c:if test="${ctOrderInfo.orderStatus == 0 }">
    						 <input type="button" onclick="javascript:btnUpdateTotal()" value="修改" />
   					</c:if>
    				 	</font>
   				  </td>
    				<th>付款金额：</th>
    				<td>${ctPay.payAmount }</td>
    			</tr>
    			<tr>
    			<tr>
    				<td colspan="4" height="10"> </td>

    			</tr>
		  <th>发票类型：</th>
    			<td>
    				<c:if test="${ctOrderInfo.invoiceType == 1 }">
    					不开发票
    				</c:if>
    				<c:if test="${ctOrderInfo.invoiceType == 2 }">
    					普通发票
    				</c:if>
    				<c:if test="${ctOrderInfo.invoiceType == 3 }">
    					增值税发票
    				</c:if>
    			</td>
    			<th>发票抬头：</th>
    			<td>${ctUser.UUsername }</td>
    		</tr>
    		<tr>
    			<th>发票内容：</th>
    			<td>${ctOrderInfo.invoice }</td>
    			<th>备注：</th>
    			<td>${ctOrderInfo.dsc }</td>
    		</tr>
    		<tr>
    			<th>客服备注内容：</th>
    			<td colspan="3"><input style="width: 395px;" type="text" id="kfdes" value="${ctOrderInfo.kfDsc }"/>
    			<input type="button" 
    				onclick="upkfdesInfo($('#kfdes').val(),${ctOrderInfo.orderId })"
    			 value="确定" />
    			</td>
    		</tr>
    		<tr>
    			 <c:if test="${ctOrderInfo.urgentOrder != null  }">
    				<th colspan="4" style="text-align: center; color: red;">
    					<c:if test="${ctOrderInfo.urgentOrder !='-1' }">
    						 该订单已经处于加急状态，请尽快处理 加急内容  “${ctOrderInfo.urgentOrder }”
    					</c:if>
    					<c:if test="${ctOrderInfo.urgentOrder =='-1' }">
    						已经处理加急完成
    					</c:if>
    				</th>
    			</c:if>
    			<c:if test="${ctOrderInfo.urgentOrder == null && ctOrderInfo.orderStatus==1 }">
	    			<th>加急订单：</th>
	    			<td colspan="2" style="text-align: center; color: red;">需要给该订单加急处理</td>
	    			<td style="text-align: center;">
	    			<input type="button" onclick="upJiajiInfo(${ctOrderInfo.orderId })" value="加急" />
	    			</td>
    			</c:if>
    		</tr>
    		</tbody>
    	</table>
    	<br />
    	<br />
    	<h1>费用明细</h1>
    	<table  class="gridtable">
    		<tr>
    			<th>商品总金额：</th>
    			<td><frm:formatNumber pattern="0.00">${total }</frm:formatNumber></td>
    		</tr>
    		<tr>
    			<th>运费：</th>
    			<td>
    				<c:if test="${ ctOrderInfo.shoppingTypeCollect == true}"> 到付</c:if>
    				<c:if test="${ ctOrderInfo.shoppingTypeCollect != true}">${ctOrderInfo.freight } </c:if>
    			</td>
    		</tr>
    		<tr>
    			<th>优惠券优惠价格：</th>
    			<td>
    				<c:if test="${couponDetail != null }">
    					<span id="geshiyangshi">${ctOrderInfo.discount }</span>
    				</c:if>
    				<c:if test="${couponDetail == null }">
    					0
    				</c:if>
    			</td>
    		</tr>
    		<tr>
    			<th>订单总金额：</th>
    			<td><frm:formatNumber pattern="0.00">${ctOrderInfo.total }</frm:formatNumber></td>
    		</tr>
    	</table>
		<p><br />
	    </p>
		<div align="center">
		  <input type="button" class="go" onclick="location.href='javascript:history.go(-1);'" value="返回" />
	        </div>
    </form>
    </div> 
    
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    
    <script type="text/javascript">
    function temp(){
    	var parice = $("#geshiyangshi").text();
	    $("#geshiyangshi").text(Number(parice).toFixed(2));
    }
    temp();
    	function upkfdesInfo(str,orderId){
    		if(typeof(str) != "undefined"  && str != "" && str != " " && str != null){
    			$.post(
    					"manage/upkfdesInfo",
    					{"kfDsc":str,"orderid":orderId},
    					function(data,varstart){
    						alert(data);
    						location.reload();
    					}
    				);
    		} else {
    			alert("请输入备注内容");
    		}
    	}
    	function upJiajiInfo(orderId){
    		var jiajidesc = prompt("请输入加急原因");
    		if(jiajidesc == null || jiajidesc == ""){
    			
    		} else {
	    		$.post(
	    			"manage/upJiaJiChuli",
	    			{"orderid":orderId,"urgentOrder":jiajidesc},
	    			function(data, varstarts){
			    		window.location.reload();
	    			}
	    		);
    		}
    	}
    </script>
    
    
    </div>

</div>
</body>

</html>
