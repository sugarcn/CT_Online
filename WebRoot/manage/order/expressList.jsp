<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
		<link rel="stylesheet" href="css/mycss.css" type="text/css"/>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/express.js" charset="utf-8"></script>
</head>


<body>

<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">物流管理</a></li>
    <li><a href="#">物流管理</a></li>
    </ul>
    </div>
 <s:form method="post"  name="form1" id="form1" theme="simple">   
 
 <input type="hidden" id="deleteA" value="delete_express">
 <input type="hidden" id="findA" value="find_express">
 <input type="hidden" id="searchA" value="search_express">
 <input type="hidden" id="updateA" value="update_express"> 
 
    <div class="rightinfo">
    
    <div class="tools">
    	<ul class="toolbar">
        <li rel="leanModal" name="signup" href="#signup" "><span><img src="images/t01.png" width="24" height="24" /></span>添加</li>

		<li rel="leanModal1" class="modiItem" href="#signup1"><span><img src="images/t02.png" width="24" height="24" /></span>修改</li>

        <li class="deleteItem" ><span><img src="images/t03.png" /></span>删除</li>
        </ul>

	<div class="ssk">
     
		   <div class="ssbk">
		      <div style="float:left;padding-left:30px;padding-top:1px"><input type="text" id="keyword" name="keyword" class="wbdd" onfocus="if(this.value=='请输入你要查询的关键词'){this.value=''}" onblur="if(this.value==''){this.value='请输入你要查询的关键词'}"  value="${keyword}"></div>
		      <div style="float:right"><img src="images/ss.jpg" class="searchItem"></div>
		      <div class="clear"></div>
		   </div>
   
  </div>
    
    </div>
    
    
     <table class="tablelist">
    	<thead>
    	<tr align="center">
    	<th><input type='checkbox' id='checkall' name='checkall' /></th>
        <th>快递公司名称</th>
        <th>快递查询网址</th>
        <th>备注</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${express}" var="list">
	        <tr>
	        	<td width="29" height="38" style="padding-left:10px">
					<input type="checkbox" name="mid" value=${list.exId } />
       	 		</td>
	        	<td width="228" align="center" class="modify">${list.exName }</td>
	        	<td width="228" align="center" class="modify">${list.exUrl }</td>
	        	<td width="228" align="center" class="modify">${list.exDesc }</td>	    
	        </tr> 
        </c:forEach>
        </tbody>
    </table>

     ${pages.pageStr}
     
   </s:form> 
    
	
	<div id="signup"  style="width:520px;">
			<div id="signup-ct">
				<div id="signup-header">
					<h2>新建物流</h2>
					<a class="modal_close" href="#"></a>
				</div>
				
				<form method="post">
				  <div class="txt-fld">
				    <label for="" style="width:100px;">快递公司名称:</label>
				    <input id="" class="good_input" name="exName" type="text"/>
				    <label for="" style="width:100px;">快递查询网址:</label>
				    <input id="" class="good_input" name="exUrl" type="text" />
				    <label for="" style="width:100px;">备注:</label>
				    <input id="" class="good_input" name="exDesc" type="text" />
				  </div>

				  <div class="btn-fld">
                  <div id="tipHtml" style="color:red;float:left">
                    </div>
				  <button type="submit" onclick="return checkThis(this.form,'add_express',0);">确定</button>
</div>
				 </form>
			</div>
		</div>
		
		<div id="signup1">
			<div id="signup-ct">
            <div id="signup-header">
					<h2>修改物流</h2>
					<a class="modal_close" href="#"></a>
				</div>
              <form method="post">
              <div id="signupmodi">
                    </div>
                    
              <div class="btn-fld">
              <div id="tipHtmlModi" style="color:red;float:left">
                    </div>
				  <button type="submit" onclick="return checkThis(this.form,'update_express',1);">确定</button>
</div>
				 </form>
                 </div>
		</div>
	
	
	
    
    
    
    
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	
	

</body>

</html>
