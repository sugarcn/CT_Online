<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>登录</title>

<script type="text/javascript" src="js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="js/style.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/styles.css" />
        <link rel="stylesheet" type="text/css" href="js/jquery.confirm.css" />
        
        <link rel="stylesheet" href="css/mycss.css" type="text/css"/>
        <script type="text/javascript" src="js/check.js"></script>
<script type="text/javascript"> 

$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
				$('a[rel*=leanModal1]').leanModal({ top : 200, closeButton: ".modal_close" });
						
			});
function login(){
	
	var username=$("#username").val();
	var passwd=$("#passwd").val();
	if(username == ""){
		$("#usernametip").text("请输入用户名");
	}else {
		$("#usernametip").text("");
		if(passwd.length<6){
			$("#passwdtip").text("密码长度至少6位");
			$("#passwd").val("");
		}else {
			$("#passwdtip").text("");
			if(checkSpe(username,"用户名") && checkSpe(passwd,"密码")){
			if($("#checkbox").attr("checked") == "checked"){
				$("#isAutoLogin").val("autoLogin");
				$("#form").submit();
			}else{
					$("#isAutoLogin").val("notAutoLogin");
					$("#form").submit();
				}
			}
		}
	}
	
	
	
}			
 
</script>
</head>
  
  <body>
    <div style="margin-left: 550px;margin-top: 300px">
    	<s:form action="login_login" id="form" method="post">
    		<input type="hidden" id="isAutoLogin" name="isAutoLogin" value=""/>
    		<span style="color: red">${message }</span>
    		<table>
    			<tr>
    				<td>用户名/已验证手机</td>
    			</tr>
    			<tr>
    				<td><input type="text" id="username" name="ctUser.UUserid"/></td>
    				<td colspan="2"><span style="color: red;" id="usernametip"></span></td>
    			</tr>
    			<tr>
    				<td>密&nbsp&nbsp码</td>
    			</tr>
    			<tr>
    				<td><input type="password" id="passwd" name="ctUser.UPassword"/></td>
    				<td colspan="2"><span style="color: red;" id="passwdtip"></span></td>
    			</tr>
    			<tr>
    				<td><input type="checkbox" id="checkbox" value="自动登录" name="checkbox"  onclick="getCheck();"/>自动登录</td>
    				<td><a href="<%=basePath%>user/findPasswd1.jsp">忘记密码？</a></td>
    			</tr>
    			<tr>
    				<td align="center"><input type="button" style="width: 200px" value="登录" onclick="login();"/></td>
    			</tr>
    			<tr>
    				<td><div style="background-color: gray;">使用合作网站登录CT-GO</div></td>
    				<td><a href="<%=basePath%>user/register.jsp"><input type="button" value="免费注册"/></a></td>
    			</tr>
    		</table>
    	</s:form>
    </div>
  </body>
</html>
