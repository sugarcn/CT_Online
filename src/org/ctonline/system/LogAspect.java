package org.ctonline.system;

import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.ctonline.manager.basic.CtLoginOperationManager;
import org.ctonline.po.basic.CtLoginOperation;
import org.ctonline.po.manager.CtManager;
import org.springframework.aop.AfterReturningAdvice;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

/**
 * 记录操作 新增 修改  删除 异常 日志
 * @author lxk
 *
 */
@Aspect
@Component
public class LogAspect
{
  private CtLoginOperation log;
  private CtLoginOperationManager loginOperation;
  private static Logger logger = Logger.getLogger("CtLog");
  private static int in = 1;
  
  
  /**
   * 在方法出现异常时会执行的代码
   * 可以访问到异常对象，可以指定在出现特定异常时在执行通知代码
   */
     @AfterThrowing(pointcut="execution(* org.ctonline.manager.*.impl.*.*(..))", throwing="ex")
     public void afterThrowing(JoinPoint joinPoint, Exception ex){
    	 try {
				CtManager cm = (CtManager)ServletActionContext.getRequest().getSession().getAttribute("admininfosession");  
				  Long adminUserId = cm.getMId().longValue();  
				 
				 if(adminUserId == null){//没有管理员登录  
					 return ;  
				 } 
				 //判断参数  
				 if(joinPoint.getArgs() == null){//没有参数  
					 return ;  
				 } 
				 String methodName = joinPoint.getSignature().getName();
				 
				 
				 //获取操作内容  
				 String opContent = adminOptionContent(joinPoint.getArgs(), methodName);  
				 
//				 
//				 Boolean isRan = loginOperation.findDescEs(opContent);
//				 if(isRan){
//					 //创建日志对象  
//					 log = new CtLoginOperation();  
//					 log.setUId(adminUserId.intValue());//设置管理员id  
//					 log.setLogTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));//操作时间  
//					 log.setLogModule(methodName);//操作方法 
//					 log.setLogDesc(ex.getMessage());
//					 log.setLogClass(joinPoint.getThis().getClass().getSimpleName().substring(0, joinPoint.getThis().getClass().getSimpleName().indexOf("$$")));
//					 log.setLogIp(getIpAddress(ServletActionContext.getRequest()));
//					 log.setLogState("失败");
//					 loginOperation.addLog(log);//添加日志  
//				 }
				 
				 logger.info("异常："+ex.getMessage() +",操作人: "+cm.getMManagerId()+", 操作数据:" + opContent + ", 操作失败");
				 System.out.println("The method " + methodName + " occurs exception: " + ex);
		} catch (Exception e) {
			e.printStackTrace();
		}
     }
  
//声明这是一个前置通知
  @After("execution(* org.ctonline.manager.*.impl.*.save*(..))")
  public void afterMethodSave(JoinPoint joinpoint){
	  
	  
	  try {
		CtManager cm = (CtManager)ServletActionContext.getRequest().getSession().getAttribute("admininfosession");  
		  Long adminUserId = cm.getMId().longValue();  
		  
		  if(adminUserId == null){//没有管理员登录  
		    cm = new CtManager();
		    cm.setMManagerId("系统");
		  }  
		  
		  //判断参数  
		  if(joinpoint.getArgs() == null){//没有参数  
		    return ;  
		  }  
		  
		  //获取方法名  
		  String methodName = joinpoint.getSignature().getName();  
		  
		  //获取日志操作详细
		  String targetName = joinpoint.getTarget().getClass().getName();  
		  Object[] arguments = joinpoint.getArgs();  
		  Class targetClass = Class.forName(targetName);  
		  Method[] methods = targetClass.getMethods();
		  String operationType = "";
		  String operationName = "";
		   try {
			for (Method method : methods) {  
			       if (method.getName().equals(methodName)) {  
			          Class[] clazzs = method.getParameterTypes();  
			           if (clazzs.length == arguments.length) {  
			               operationType = method.getAnnotation(Logg.class).operationType();
			               operationName = method.getAnnotation(Logg.class).operationName();
			               break;  
			          }  
			      }  
			  }
		} catch (Exception e) {
			e.printStackTrace();
			operationType = "";
			operationName = "";
		}
		  
		if(operationType.equals("") || operationName.equals("")){
		    return;
		}
		JSONArray json = JSONArray.fromObject(joinpoint.getArgs());
        List<Map<String,Object>> mapListJson = (List)json; 
        Map<String,Object> m = new HashMap<String, Object>();
        for (int i = 0; i < mapListJson.size(); i++) {  
        	try {
				Map<String,Object> obj=mapListJson.get(i);  
				  
				for(Entry<String,Object> entry : obj.entrySet()){  
				    String strkey1 = entry.getKey();  
				    Object strval1 = entry.getValue();  
//                System.out.println("KEY:"+strkey1+"  -->  Value:"+strval1+"\n");
				    if(strval1 != null && !strval1.toString().equals("") && !strval1.toString().equals("null") && !strval1.toString().equals("0")){
				    	m.put(strkey1, strval1);
				    }
				}
				json = JSONArray.fromObject(m);
			} catch (Exception e) {
				//e.printStackTrace();
			}   
        }  
        json = JSONArray.fromObject(m);
		String opContent = json.toString();
		  //获取操作内容  
//	  String opContent = adminOptionContent(joinpoint.getArgs(), methodName);  
		  //创建日志对象  
		  log = new CtLoginOperation();  
		  log.setUId(adminUserId.intValue());//设置管理员id  
		  log.setLogTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));//操作时间  
		  log.setLogModule(methodName+(operationName.equals("") ? "" : "_" + operationName));//操作方法 
		  log.setLogDesc(opContent);
		  log.setLogClass(joinpoint.getThis().getClass().getSimpleName().substring(0, joinpoint.getThis().getClass().getSimpleName().indexOf("$$")));
		  log.setLogIp(getIpAddress(ServletActionContext.getRequest()));
		  log.setLogType("0");
		  if(log.getLogClass().equals("CtLoginLogImpl")){
			logger.info(" ");
		    logger.info(cm.getMManagerId() + "于" + log.getLogIp() + "的" + log.getLogTime() + "登录了系统");
		    log.setLogState("登录系统");
		  } else {
		    logger.info(fomatLogStr(cm.getMManagerId(), log.getLogIp(), log.getLogTime(), operationType, operationName));
		    logger.info("新增数据："+opContent);
		    log.setLogState("新增");
		  }
		  loginOperation.addLog(log);//添加日志  
	} catch (ClassNotFoundException e) {
		e.printStackTrace();
	} catch (SecurityException e) {
		e.printStackTrace();
	}
  }
  
  public String fomatLogStr(String userName, String ip, String time, String type, String typeName){
	  String str = userName + " 于" + ip + "的" + time + "进行了" + type + typeName + "的操作。";
	  return str;
  }
  
  
  @After("execution(* org.ctonline.manager.*.impl.*.update*(..))")
  public void afterMethodUpdate(JoinPoint joinpoint) throws Exception{
	  
	  
	  if(loginOperation != null){
		  CtManager cm = (CtManager)ServletActionContext.getRequest().getSession().getAttribute("admininfosession");  
		  Long adminUserId = cm.getMId().longValue();  
		  
		  if(adminUserId == null){//没有管理员登录  
			  return ;  
		  }  
		  
		  //判断参数  
		  if(joinpoint.getArgs() == null){//没有参数  
			  return ;  
		  }  
		  
		  for (int i = 0; i < joinpoint.getArgs().length; i++) {
			  System.out.println(joinpoint.getArgs()[i]);
		  }
		  System.out.println(joinpoint.getSignature().getName());
		  //获取方法名  
		  String methodName = joinpoint.getSignature().getName();  
		  
		  
		//获取日志操作详细
		  String targetName = joinpoint.getTarget().getClass().getName();  
		  Object[] arguments = joinpoint.getArgs();  
		  Class targetClass = Class.forName(targetName);  
		  Method[] methods = targetClass.getMethods();
		  String operationType = "";
		  String operationName = "";
		   try {
			for (Method method : methods) {  
			       if (method.getName().equals(methodName)) {  
			          Class[] clazzs = method.getParameterTypes();  
			           if (clazzs.length == arguments.length) {  
			               operationType = method.getAnnotation(Logg.class).operationType();
			               operationName = method.getAnnotation(Logg.class).operationName();
			               break;  
			          }  
			      }  
			  }
		} catch (Exception e) {
			e.printStackTrace();
			operationType = "";
			operationName = "";
		}
		  
		if(operationType.equals("") || operationName.equals("")){
		    return;
		}
		 
		JSONArray json = JSONArray.fromObject(joinpoint.getArgs());
        List<Map<String,Object>> mapListJson = (List)json; 
        Map<String,Object> m = new HashMap<String, Object>();
        for (int i = 0; i < mapListJson.size(); i++) {  
            try {
				Map<String,Object> obj=mapListJson.get(i);  
				  
				for(Entry<String,Object> entry : obj.entrySet()){  
				    String strkey1 = entry.getKey();  
				    Object strval1 = entry.getValue();  
//                System.out.println("KEY:"+strkey1+"  -->  Value:"+strval1+"\n");
				    if(strval1 != null && !strval1.toString().equals("") && !strval1.toString().equals("null") && !strval1.toString().equals("0")){
				    	m.put(strkey1, strval1);
				    }
				}
				json = JSONArray.fromObject(m);
			} catch (Exception e) {
				//e.printStackTrace();
			}  
        }  
		String opContent = json.toString();
		
		  //获取操作内容  
		  //创建日志对象  
		  log = new CtLoginOperation();  
		  log.setUId(adminUserId.intValue());//设置管理员id  
		  log.setLogTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));//操作时间  
		  log.setLogModule(methodName+(operationName.equals("") ? "" : "_" + operationName));//操作方法 
		  log.setLogDesc(opContent);
		  log.setLogClass(joinpoint.getThis().getClass().getSimpleName().substring(0, joinpoint.getThis().getClass().getSimpleName().indexOf("$$")));
		  log.setLogIp(getIpAddress(ServletActionContext.getRequest()));
		  log.setLogType("0");
		  log.setLogState("修改");
		  logger.info(fomatLogStr(cm.getMManagerId(), log.getLogIp(), log.getLogTime(), operationType, operationName));
		  logger.info("修改数据：" + opContent);
		  loginOperation.addLog(log);//添加日志  
	  }
  }
  @After("execution(* org.ctonline.manager.*.impl.*.delete*(..))")
  public void afterMethodDelete(JoinPoint joinpoint) throws Exception{
	  
	  
	  if(loginOperation != null){
		  CtManager cm = (CtManager)ServletActionContext.getRequest().getSession().getAttribute("admininfosession");  
		  Long adminUserId = cm.getMId().longValue();  
		  
		  if(adminUserId == null){//没有管理员登录  
			  return ;  
		  }  
		  
		  //判断参数  
		  if(joinpoint.getArgs() == null){//没有参数  
			  return ;  
		  }  
		  
		  System.out.println(joinpoint.getSignature().getName());
		  //获取方法名  
		  String methodName = joinpoint.getSignature().getName(); 
		  
		//获取日志操作详细
		  String targetName = joinpoint.getTarget().getClass().getName();  
		  Object[] arguments = joinpoint.getArgs();  
		  Class targetClass = Class.forName(targetName);  
		  Method[] methods = targetClass.getMethods();
		  String operationType = "";
		  String operationName = "";
		   try {
			for (Method method : methods) {  
			       if (method.getName().equals(methodName)) {  
			          Class[] clazzs = method.getParameterTypes();  
			           if (clazzs.length == arguments.length) {  
			               operationType = method.getAnnotation(Logg.class).operationType();
			               operationName = method.getAnnotation(Logg.class).operationName();
			               break;  
			          }  
			      }  
			  }
		} catch (Exception e) {
			e.printStackTrace();
			operationType = "";
			operationName = "";
		}
		  
		if(operationType.equals("") || operationName.equals("")){
		    return;
		}
		  
		  
		  String str = methodName+" [删除编号：";
		  for (int i = 0; i < joinpoint.getArgs().length; i++) {
			  System.out.println(joinpoint.getArgs()[i]);
			  str += joinpoint.getArgs()[i] + ", ";
		  }
		  
		  str += ", time: "+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		  str += "]";
		  
		  //获取操作内容  
		  String opContent = str;  
		  //创建日志对象  
		  log = new CtLoginOperation();  
		  log.setUId(adminUserId.intValue());//设置管理员id  
		  log.setLogTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));//操作时间  
		  log.setLogModule(methodName+(operationName.equals("") ? "" : "_" + operationName));//操作方法 
		  log.setLogDesc(opContent);
		  log.setLogClass(joinpoint.getThis().getClass().getSimpleName().substring(0, joinpoint.getThis().getClass().getSimpleName().indexOf("$$")));
		  log.setLogIp(getIpAddress(ServletActionContext.getRequest()));
		  log.setLogType("0");
		  log.setLogState("删除");
		  loginOperation.addLog(log);//添加日志  
		  logger.info(fomatLogStr(cm.getMManagerId(), log.getLogIp(), log.getLogTime(), operationType, operationName));
		  logger.info("删除数据详细：" + opContent);
	  }
  }
  
  
  public String adminOptionContent(Object[] args, String mname)
    throws Exception
  {
    if (args == null) {
      return null;
    }
    StringBuffer rs = new StringBuffer();
    rs.append(mname);
    String className = null;
    int index = 1;
    Object[] arrayOfObject;
    int j = (arrayOfObject = args).length;
    for (int i = 0; i < j; i++)
    {
      Object info = arrayOfObject[i];
      
      className = info.getClass().getName();
      className = className.substring(className.lastIndexOf(".") + 1);
      rs.append("[参数:" + index + ",类型: " + className + ",值: ");
      
      Method[] methods = info.getClass().getDeclaredMethods();
      Method[] arrayOfMethod1;
      int m = (arrayOfMethod1 = methods).length;
      String methodName;
      Object rsValue;
      for (int k = 0; k < m; k++)
      {
        Method method = arrayOfMethod1[k];
        methodName = method.getName();
        if (methodName.indexOf("get") != -1)
        {
          rsValue = null;
          try
          {
            rsValue = method.invoke(info, new Object[0]);
            if (rsValue != null) {
              rs.append("(" + methodName + " : " + rsValue + ")");
            }
          }
          catch (Exception e) {System.out.println(e.getMessage());}
        }
      }
      rs.append("]");
      index++;
    }
    
    return rs.toString();
  }
  
  public CtLoginOperationManager getLoginOperation()
  {
    return this.loginOperation;
  }
  
  public void setLoginOperation(CtLoginOperationManager loginOperation)
  {
    this.loginOperation = loginOperation;
  }
  /** 
   * 获取请求主机IP地址,如果通过代理进来，则透过防火墙获取真实IP地址; 
   *  
   * @param request 
   * @return 
   * @throws IOException 
   */  
  public static String getIpAddress(HttpServletRequest request) {  
      // 获取请求主机IP地址,如果通过代理进来，则透过防火墙获取真实IP地址  

      String ip = request.getHeader("X-Forwarded-For");  

      if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
          if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
              ip = request.getHeader("Proxy-Client-IP");  
          }  
          if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
              ip = request.getHeader("WL-Proxy-Client-IP");  
          }  
          if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
              ip = request.getHeader("HTTP_CLIENT_IP");  
          }  
          if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
              ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
          }  
          if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
              ip = request.getRemoteAddr();  
          }  
      } else if (ip.length() > 15) {  
          String[] ips = ip.split(",");  
          for (int index = 0; index < ips.length; index++) {  
              String strIp = (String) ips[index];  
              if (!("unknown".equalsIgnoreCase(strIp))) {  
                  ip = strIp;  
                  break;  
              }  
          }  
      }  
      return ip;  
  }


}
