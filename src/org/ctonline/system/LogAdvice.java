package org.ctonline.system;

import java.lang.reflect.Method;
import org.apache.log4j.Logger;
import org.springframework.aop.MethodBeforeAdvice;

public class LogAdvice
  implements MethodBeforeAdvice
{
  private static Logger logger = Logger.getLogger("CtLog");
  
  public void before(Method method, Object[] args, Object target)
    throws Throwable
  {
    StringBuffer loginInfoText = new StringBuffer();
    
    String targetClassName = target.getClass().getName();
    
    String targetMethodName = method.getName();
    
    loginInfoText.append("通知" + targetClassName + "类的" + targetMethodName + "方法开始执行！");
    //暂时不记录通知日志
    //logger.info(loginInfoText);
  }
}
