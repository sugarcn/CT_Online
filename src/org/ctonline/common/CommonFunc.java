package org.ctonline.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CommonFunc {

	// 一页显示多少条
	public static final int pagesize = 10;
	// 当前页数 默认值 1
	private static final int currentPage = 1;
	// 总行数
	private int totalCount;
	// 总页数
	private int totalPage;

	public static List getpagelist(List list, int currentPage) {
		return list.subList(currentPage * 10, (currentPage + 1) * 10);
	}

	public static List getPage(List list, PageBean pagebean) {
		List list1 = new ArrayList();
		// 总条数
		pagebean.setTotalSum(list.size());
		// 总页数
		pagebean.setTotal(new BigDecimal(list.size()).divide(
				new BigDecimal(pagebean.getPerpage()), BigDecimal.ROUND_UP)
				.intValue());
		// 如果总条数小于（当前页*每页显示的条数）
		if (list.size() < (pagebean.getCurrentPage() * pagebean.getPerpage())) {
			// list1=截取(当前页减去1乘以每页条数----总条数)
			list1 = list.subList(
					(pagebean.getCurrentPage() - 1) * pagebean.getPerpage(),
					list.size());
		} else {
			// list1=截取(当前页减去1乘以每页条数----当前页乘以每页的条数)----就是查出下一页的数据
			list1 = list.subList(
					(pagebean.getCurrentPage() - 1) * pagebean.getPerpage(),
					pagebean.getCurrentPage() * pagebean.getPerpage());
		}
		return list1;
	}
}
