package org.ctonline.manager.user;

import java.util.List;

import org.ctonline.po.user.CtUserGroupRelation;

public interface CtUserGroupRelationManager {

	
	public Long save(CtUserGroupRelation groupRelation);
	
	public List<CtUserGroupRelation>  selectAll(Long UGId);
	
	public Long totalCount(Long id);
	
	public int delete(Long id);
}
