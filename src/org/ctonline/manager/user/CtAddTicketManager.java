package org.ctonline.manager.user;

import java.util.List;

import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.basic.CtUserRank;
import org.ctonline.po.user.CtAddTicket;
import org.ctonline.util.Page;

public interface CtAddTicketManager {
	public Long save(CtAddTicket addTicket);//保存
	public CtAddTicket findById(Long id);//根据id查找
	public List<CtAddTicket> loadAll(Page page);//分页找出所有
	public int delete(Long id);//删除一条记录
////	public Long totalCount(String str);//统计条数
	public void update(CtAddTicket addTicket);//更新
//	public List<CtUserRank> findAll(String keyword,Page page);//根据条件检索
//	public String getMaxPoint();
	public List<CtAddTicket> loadOne(Long id);
	public CtAddTicket getTicketByUId(Long UId);
	public List<CtAddTicket>  allPass(String isPass,Page page);
	public List<CtAddTicket>  notAllPass(String isPass,Page page);
}
