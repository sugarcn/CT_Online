package org.ctonline.manager.user;

import java.util.List;

import org.ctonline.po.user.CtUserGroup;
import org.ctonline.po.user.CtUserManageRelation;
import org.ctonline.util.Page;

public interface CtUserGroupManager {
	
	public Long save(CtUserGroup userGroup);//保存
	public CtUserGroup findById(Long id);//根据id查找
	public List<CtUserGroup> loadAll(Page page);//分页找出所有
	public int delete(Long id);//删除一条记录
	public Long totalCount(String str);//统计条数
	public void update(CtUserGroup userGroup);//更新
	public List<CtUserGroup> findAll(String keyword,Page page);//根据条件检索
	//保存保存分配客户组所属客户经理
	public Long saveBelongToManager(CtUserManageRelation cmr);
	//根据UGID和UTpye查询用户组或者用户所属的客户经理
	public List<?> getCManageRelation(Long UGID,String UTpye,Integer roleId);
	//根据UGID和UTpye验证是否存在所属的客户经理
	public CtUserManageRelation getUserManageRelation(Long UGID,String UTpye);
	//修改保存分配客户组所属客户经理
	public void updateUserManageRelation(Long UGID,String UTpye,Integer MId);

}
