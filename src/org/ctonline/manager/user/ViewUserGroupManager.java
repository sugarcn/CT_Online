package org.ctonline.manager.user;

import java.util.List;

import org.ctonline.po.views.ViewUserGroup;


public interface ViewUserGroupManager {
	
	public List<ViewUserGroup>  query(String username,String useraccount);

}
