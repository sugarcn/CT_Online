package org.ctonline.manager.user.impl;

import java.util.List;

import org.ctonline.dao.user.CtUserGroupRelationDAO;
import org.ctonline.manager.user.CtUserGroupRelationManager;
import org.ctonline.po.user.CtUserGroupRelation;

public class CtUserGroupRelationManagerImpl implements CtUserGroupRelationManager{

	private CtUserGroupRelationDAO groupRelationDao;

	public CtUserGroupRelationDAO getGroupRelationDao() {
		return groupRelationDao;
	}

	public void setGroupRelationDao(CtUserGroupRelationDAO groupRelationDao) {
		this.groupRelationDao = groupRelationDao;
	}
	
	
	@Override
	public Long save(CtUserGroupRelation groupRelation){
		
		return this.groupRelationDao.save(groupRelation);
	}
	
	@Override
	public List<CtUserGroupRelation>  selectAll(Long UGId){
		
		return this.groupRelationDao.selectAll(UGId);
	}
	
	@Override
	public Long totalCount(Long id){
		
		return this.groupRelationDao.totalCount(id);
	}
	
	@Override
	public int delete(Long id){
		
		return this.groupRelationDao.delete(id);
	}
}
