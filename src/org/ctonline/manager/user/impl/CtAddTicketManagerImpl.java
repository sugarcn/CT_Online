package org.ctonline.manager.user.impl;

import java.util.List;

import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.basic.CtUserRank;
import org.ctonline.po.user.CtAddTicket;
import org.ctonline.system.Logg;
import org.ctonline.util.Page;
import org.ctonline.dao.basic.CtRegionDAO;
import org.ctonline.dao.basic.CtUserRankDAO;
import org.ctonline.dao.user.CtAddTicketDAO;

public class CtAddTicketManagerImpl implements org.ctonline.manager.user.CtAddTicketManager {

	private CtAddTicketDAO addTicketDao;

	public CtAddTicketDAO getAddTicketDao() {
		return addTicketDao;
	}

	public void setAddTicketDao(CtAddTicketDAO addTicketDao) {
		this.addTicketDao = addTicketDao;
	}

	@Override
	public List<CtAddTicket> loadAll(Page page) {
		// TODO Auto-generated method stub
		return this.addTicketDao.loadAll(page);
	}

	@Override
	public CtAddTicket findById(Long id) {
		// TODO Auto-generated method stub
		return this.addTicketDao.findById(id);
	}

	@Override
	public List<CtAddTicket> loadOne(Long id) {
		// TODO Auto-generated method stub
		return this.addTicketDao.loadOne(id);
	}

	@Logg(operationType="update", operationName="审核增票信息")
	@Override
	public void update(CtAddTicket addTicket) {
		// TODO Auto-generated method stub
		this.addTicketDao.update(addTicket);
	}

	@Override
	public Long save(CtAddTicket addTicket) {
		// TODO Auto-generated method stub
		return this.addTicketDao.save(addTicket);
	}

	@Override
	public CtAddTicket getTicketByUId(Long UId) {
		// TODO Auto-generated method stub
		return this.addTicketDao.getTicketByUId(UId);
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		return this.addTicketDao.delete(id);
	}
	
	@Override
	public List<CtAddTicket>  allPass(String isPass,Page page){
		
		return this.addTicketDao.allPass(isPass, page);
	}
	@Override
	public List<CtAddTicket>  notAllPass(String isPass,Page page){
		
		return this.addTicketDao.notAllPass(isPass, page);
	}



}
