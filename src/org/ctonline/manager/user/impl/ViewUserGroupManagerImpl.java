package org.ctonline.manager.user.impl;

import java.util.List;

import org.ctonline.dao.user.ViewUserGroupDAO;
import org.ctonline.manager.user.ViewUserGroupManager;
import org.ctonline.po.views.ViewUserGroup;

public class ViewUserGroupManagerImpl implements ViewUserGroupManager{
	
	private ViewUserGroupDAO viewUserDao;

	public ViewUserGroupDAO getViewUserDao() {
		return viewUserDao;
	}

	public void setViewUserDao(ViewUserGroupDAO viewUserDao) {
		this.viewUserDao = viewUserDao;
	}
     
	
	@Override
	public List<ViewUserGroup>  query(String username,String useraccount){
		
		
		return this.viewUserDao.query(username, useraccount);
	}
}
