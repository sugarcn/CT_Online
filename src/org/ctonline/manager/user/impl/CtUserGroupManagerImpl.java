package org.ctonline.manager.user.impl;

import java.util.List;

import org.ctonline.dao.user.CtUserGroupDAO;
import org.ctonline.manager.user.CtUserGroupManager;
import org.ctonline.po.user.CtUserGroup;
import org.ctonline.po.user.CtUserManageRelation;
import org.ctonline.util.Page;

public class CtUserGroupManagerImpl implements CtUserGroupManager{
	
	private CtUserGroupDAO userGroupDao;

	public CtUserGroupDAO getUserGroupDao() {
		return userGroupDao;
	}

	public void setUserGroupDao(CtUserGroupDAO userGroupDao) {
		this.userGroupDao = userGroupDao;
	}
	
	@Override
	public Long save(CtUserGroup userGroup) {
		// TODO Auto-generated method stub
		return userGroupDao.save(userGroup);
	}
	
	@Override
	public CtUserGroup findById(Long id) {
		// TODO Auto-generated method stub
		return userGroupDao.findById(id);
	}

	@Override
	public List<CtUserGroup> loadAll(Page page) {
		// TODO Auto-generated method stub
		return userGroupDao.loadAll(page);
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		return userGroupDao.delete(id);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return this.userGroupDao.totalCount(str);
	}

	@Override
	public void update(CtUserGroup userGroup) {
		// TODO Auto-generated method stub
		this.userGroupDao.update(userGroup);
	}

	@Override
	public List<CtUserGroup> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return userGroupDao.findAll(keyword,page);
	}

	@Override
	public Long saveBelongToManager(CtUserManageRelation cmr) {
		// TODO Auto-generated method stub
		return userGroupDao.saveBelongToManager(cmr);
	}

	@Override
	public List<?> getCManageRelation(Long UGID, String UTpye,Integer roleId) {
		// TODO Auto-generated method stub
		return userGroupDao.getCManageRelation(UGID, UTpye,roleId);
	}

	@Override
	public CtUserManageRelation getUserManageRelation(Long UGID, String UTpye) {
		// TODO Auto-generated method stub
		return userGroupDao.getUserManageRelation(UGID, UTpye);
	}

	@Override
	public void updateUserManageRelation(Long UGID, String UTpye, Integer MId) {
		// TODO Auto-generated method stub
		userGroupDao.updateUserManageRelation(UGID, UTpye, MId);
	}


}
