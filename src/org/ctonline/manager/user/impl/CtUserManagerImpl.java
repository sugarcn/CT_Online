package org.ctonline.manager.user.impl;

import java.util.List;

import org.ctonline.dao.user.CtUserDao;
import org.ctonline.manager.user.CtUserManager;
import org.ctonline.po.basic.CtLoginLog;
import org.ctonline.po.user.CtContact;
import org.ctonline.po.user.CtDraw;
import org.ctonline.po.user.CtSms;
import org.ctonline.po.user.CtUesrClient;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.system.Logg;
import org.ctonline.util.Page;

public class CtUserManagerImpl implements CtUserManager {
	private CtUserDao userDao;

	public CtUserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(CtUserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public CtUser getCtUserByUUserid(String UUserid) {
		// TODO Auto-generated method stub
		return this.userDao.getCtUserByUUserid(UUserid);
	}

	@Override
	public Long saveCtSms(CtSms ctSms) {
		// TODO Auto-generated method stub
		return this.userDao.saveCtSms(ctSms);
	}

	@Override
	public CtUser getCtUserByUMb(String UMb) {
		// TODO Auto-generated method stub
		return this.userDao.getCtUserByUMb(UMb);
	}

	@Override
	public CtSms getCtSmsByUMb(String UMb) {
		// TODO Auto-generated method stub
		return this.userDao.getCtSmsByUMb(UMb);
	}

	@Override
	public Long save(CtUser ctUser) {
		// TODO Auto-generated method stub
		return this.userDao.save(ctUser);
	}

	@Override
	public CtUser getCtUserByUUnameOrUMb(String arg0) {
		// TODO Auto-generated method stub
		return this.userDao.getCtUserByUUnameOrUMb(arg0);
	}

	@Override
	public void update(CtUser ctUser) {
		// TODO Auto-generated method stub
		this.userDao.update(ctUser);
	}

	@Override
	public CtUser affirm(String uname, String password) throws Exception {
		// TODO Auto-generated method stub
		return this.userDao.affirm(uname, password);
	}
	
	@Override
		public  List<CtUser> findAll(String keyword){
		
		return this.userDao.findAll(keyword);
				
	}
	
	@Override
	public List<?>  selectAll(Long UGId){
		
		
		return this.userDao.selectAll(UGId);
	}

	@Override
	public void editCtuser(CtUser ctUser) {
		// TODO Auto-generated method stub
		this.userDao.editCtuser(ctUser);
	}

	@Override
	public List<CtUser> loadAll(Page page) {
		// TODO Auto-generated method stub
		return this.userDao.loadAll(page);
	}

	@Override
	public CtUser getCtUserByUEmail(String UEmail) {
		// TODO Auto-generated method stub
		return this.userDao.getCtUserByUEmail(UEmail);
	}

	@Override
	public CtUser getCtuserByUUseridAndUCode(String UUserid, String UCode) {
		// TODO Auto-generated method stub
		return this.userDao.getCtuserByUUseridAndUCode(UUserid, UCode);
	}

	@Override
	public CtUser affimByEmailAndPwd(String email, String password)
			throws Exception {
		// TODO Auto-generated method stub
		return this.userDao.affimByEmailAndPwd(email, password);
	}

	@Override
	public CtUser getCtUserByUId(Long UId) {
		// TODO Auto-generated method stub
		return this.userDao.getCtUserByUId(UId);
	}

	@Override
	public List<CtUser> getCtUserByPage(Page page) {
		return userDao.getCtUserByPage(page);
	}

	@Override
	public List<CtContact> findContactByPage(Page page) {
		return userDao.findContactByPage(page);
	}

	@Override
	public List<CtDraw> findDrawByPage(Page page) {
		return userDao.findDrawByPage(page);
	}

	@Override
	public CtDraw findDrawByDrId(Integer drId) {
		return userDao.findDrawByDrId(drId);
	}

	@Override
	public void updateDraw(CtDraw draw) {
		userDao.updateDraw(draw);
	}

	@Override
	public List<CtUser> findAll(String keyword, Page page) {
		return userDao.findAll(keyword, page);
	}

	@Override
	public void saveLoginCredit(CtLoginLog loginLog) {
		userDao.saveLoginCredit(loginLog);
	}
	
	@Logg(operationType="update", operationName="升级用户为客服账户，并将原地址添加到默认客户中")
	@Override
	public void updateUserCus(Long uId) {
		userDao.updateUserCus(uId);
	}

	@Override
	public List<CtUserAddress> findAllAddressByUid(Long uId, Page page, Integer cid) {
		return userDao.findAllAddressByUid(uId, page, cid);
	}

	@Logg(operationType="update", operationName="设置当前客户为下单客户")
	@Override
	public void updateCusAddressByAid(Long cid, Long uid) {
		// TODO Auto-generated method stub
		userDao.updateCusAddressByAid(cid, uid);
	}

	@Override
	public List<CtUesrClient> findCusMent(Long uId, Page page) {
		return userDao.findCusMent(uId, page);
	}

	@Override
	public Boolean findClientByCName(String userName, Long uid) {
		CtUesrClient client = userDao.findClientByCName(userName, uid);
		if(client == null){
			return true;
		} 
		return false;
	}

	@Logg(operationType="update", operationName="客服账号添加客户")
	@Override
	public void saveClent(CtUesrClient userClient) {
		userDao.saveClent(userClient);
	}

	@Logg(operationType="update", operationName="添加客服账户客户地址")
	@Override
	public void updateClientName(Long m, Integer cid, Long uid, String ACustomer) {
		userDao.updateClientName(m, cid, uid, ACustomer);
	}

	@Override
	public String findCusAddressIsDefultByUid(Long uid) {
		CtUserAddress address = userDao.findCusAddressIsDefultByUid(uid);
		if(address != null){
			return "2";
		}
		return "1";
	}

	@Logg(operationType="delete", operationName="删除客服客户，并且删除关联")
	@Override
	public void deleteClientByCid(Integer cid) {
		userDao.deleteClientByCid(cid);
	}

}
