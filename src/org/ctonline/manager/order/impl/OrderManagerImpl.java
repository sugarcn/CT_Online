package org.ctonline.manager.order.impl;

import com.opensymphony.xwork2.ActionContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.ctonline.dao.order.OrderDAO;
import org.ctonline.manager.order.OrderManager;
import org.ctonline.po.goods.CtBomGoods;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.manager.CtManager;
import org.ctonline.po.order.CtComplain;
import org.ctonline.po.order.CtEvaluation;
import org.ctonline.po.order.CtExpress;
import org.ctonline.po.order.CtOrderGoods;
import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.po.order.CtOrderRelation;
import org.ctonline.po.pay.CtPayInterface;
import org.ctonline.po.user.CtRole;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.po.views.ViewOrderCheck;
import org.ctonline.system.Logg;
import org.ctonline.util.Page;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class OrderManagerImpl
  implements OrderManager
{
private OrderDAO orderDao;
  
  public OrderDAO getOrderDao()
  {
    return this.orderDao;
  }
  
  public void setOrderDao(OrderDAO orderDao)
  {
    this.orderDao = orderDao;
  }
  
  public SessionFactory getSessionFactory()
  {
    return this.orderDao.getSessionFactory();
  }
  
  public List<CtOrderInfo> getIsDis()
  {
    return this.orderDao.getIsDis();
  }
  
  public int updateStartByOrderId(String orderId)
  {
    return this.orderDao.updateStartByOrderId(orderId);
  }
  
  public void setSessionFactory(SessionFactory sessionFactory)
  {
    this.orderDao.setSessionFactory(sessionFactory);
  }
  
  public Session begin()
  {
    return this.orderDao.begin();
  }
  
  public Session getSession()
  {
    return this.orderDao.getSession();
  }
  
  public Session getCurrentSession()
  {
    return this.orderDao.getCurrentSession();
  }
  
  public void clossSession()
  {
    this.orderDao.clossSession();
  }
  
  public void commit()
  {
    this.orderDao.commit();
  }
  
  public List<CtGoods> getGoodsByGId(Long gid)
  {
    return this.orderDao.getGoodsByGId(gid);
  }
  
  public CtUserAddress getAddress(Long addid)
  {
    return this.orderDao.getAddress(addid);
  }
  
  public List<ViewOrderCheck> getOrderCheck(Long uid, Long gid)
  {
    return this.orderDao.getOrderCheck(uid, gid);
  }
  
  public Long saveOrderInfo(CtOrderInfo odinfo)
  {
    return this.orderDao.saveOrderInfo(odinfo);
  }
  
  public void saveOrderGoods(CtOrderGoods odgoods)
  {
    this.orderDao.saveOrderGoods(odgoods);
  }
  
  public String delCartGoods(Long uid, Long gid)
  {
    return this.orderDao.delCartGoods(uid, gid);
  }
  
  public String delCart(Long uid)
  {
    return this.orderDao.delCart(uid);
  }
  
  public List<CtOrderInfo> getOrderListByUId(Long uid, Page page)
  {
    return this.orderDao.getOrderListByUId(uid, page);
  }
  
  public List<CtOrderInfo> getOrderListByTime(Long uid, Page page, String time)
  {
    return this.orderDao.getOrderListByTime(uid, page, time);
  }
  
  public List<CtOrderInfo> getOrderListByStatus(Long uid, Page page, String status)
  {
    return this.orderDao.getOrderListByStatus(uid, page, status);
  }
  
  public void changeStatus(Long orderId, String status)
  {
    this.orderDao.changeStatus(orderId, status);
  }
  
  public List<?> getOrderGoods(Long uid)
  {
    return this.orderDao.getOrderGoods(uid);
  }
  
  public List<CtBomGoods> getGoodsByBom(Integer bomid)
  {
    return this.orderDao.getGoodsByBom(bomid);
  }
  
  public CtOrderInfo getOrderByOrderSn(String orderSn)
  {
    return this.orderDao.getOrderByOrderSn(orderSn);
  }
  
  public List<CtOrderInfo> getAllOrder(Page page,String OrderSn,String stime, String etime, Long uid, String stauts, int searchType)
  {
    try
    {
      Map<String, Object> session = ActionContext.getContext().getSession();
//      CtManager ct = (CtManager)session.get("admininfosession");
      CtManager ct = new  CtManager();
      ct.setCtRole(new CtRole("超级管理员", "超级管理员"));
      String type = "admin";
      if (ct.getCtRole().getRoleName().equals("库房")) {
        type = "库房";
      }
      if (ct.getCtRole().getRoleName().equals("客户经理")) {
        type = "客户经理";
      }
      if (ct.getCtRole().getRoleName().equals("财务")) {
        type = "财务";
      }
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
      
      List<CtOrderInfo> list = this.orderDao.getAllOrder(page,OrderSn, stime, etime, uid, stauts, searchType);
      List<CtOrderInfo> listNew = new ArrayList();
      List<CtOrderInfo> listNewTow = new ArrayList();
      Boolean isOk = Boolean.valueOf(false);
      for (CtOrderInfo ctOrderInfo : list)
      {
        if ((type.equals("财务")) && (
          (ctOrderInfo.getOrderStatus().equals("2")) || ((ctOrderInfo.getRetStatus() != null) && (ctOrderInfo.getRetStatus().equals("财务审核")))))
        {
          listNew.add(ctOrderInfo);
          if (ctOrderInfo.getRetTime() != null)
          {
            Long time = Long.valueOf(sdf.parse(ctOrderInfo.getWareTime()).getTime());
            Long nowTime = Long.valueOf(new Date().getTime());
            this.lei.put(ctOrderInfo, Long.valueOf(nowTime.longValue() - time.longValue()));
          }
          isOk = Boolean.valueOf(true);
        }
        if ((type.equals("库房")) && (
          (ctOrderInfo.getOrderStatus().equals("3")) || ((ctOrderInfo.getRetStatus() != null) && (ctOrderInfo.getRetStatus().equals("提交申请")))))
        {
          listNew.add(ctOrderInfo);
          if (ctOrderInfo.getRetTime() != null)
          {
            Long time = Long.valueOf(sdf.parse(ctOrderInfo.getWareTime()).getTime());
            Long nowTime = Long.valueOf(new Date().getTime());
            this.lei.put(ctOrderInfo, Long.valueOf(nowTime.longValue() - time.longValue()));
          }
          isOk = Boolean.valueOf(true);
        }
        if ((type.equals("客户经理")) && 
          (ctOrderInfo.getRetStatus() != null) && (ctOrderInfo.getRetStatus().equals("库房确认回库")))
        {
          listNew.add(ctOrderInfo);
          if (ctOrderInfo.getRetTime() != null)
          {
            Long time = Long.valueOf(sdf.parse(ctOrderInfo.getWareTime()).getTime());
            Long nowTime = Long.valueOf(new Date().getTime());
            this.lei.put(ctOrderInfo, Long.valueOf(nowTime.longValue() - time.longValue()));
          }
          isOk = Boolean.valueOf(true);
        }
        if (!isOk.booleanValue()) {
          listNewTow.add(ctOrderInfo);
        }
        isOk = Boolean.valueOf(false);
      }
      if (listNew.size() == 0) {
        listNewTow = list;
      }
      addList(listNewTow, listNew);
      return listNewTow;
    }
    catch (ParseException e)
    {
      e.printStackTrace();
    }
    return null;
  }
  
  Map<CtOrderInfo, Long> lei = new HashMap();
  
  private void addList(List<CtOrderInfo> list, List<CtOrderInfo> listNew)
    throws ParseException
  {
	  /*
    this.lei = sortMao(this.lei);
    Iterator<Map.Entry<CtOrderInfo, Long>> it = this.lei.entrySet().iterator();
    int ls = list.size();
    for(int i=0;i<ls;i++)
    {
      Map.Entry<CtOrderInfo, Long> entry = (Map.Entry)it.next();
      listNew.add((CtOrderInfo)entry.getKey());
      i = 0; continue;
      if (((CtOrderInfo)list.get(i)).equals(entry.getKey())) {
        list.remove(i);
      }
      i++;
    }
    for (CtOrderInfo ctOrderInfo2 : list) {
      listNew.add(ctOrderInfo2);
    }
    */
  }
  
  private Map<CtOrderInfo, Long> sortMao(Map<CtOrderInfo, Long> lei)
  {
	  /*
    ArrayList<Map.Entry<CtOrderInfo, Long>> list = new ArrayList(lei.entrySet());
    Collections.sort(list, new Comparator()
    {
      public int compare(Map.Entry<CtOrderInfo, Long> maps, Map.Entry<CtOrderInfo, Long> maps1)
      {
        return (int)(((Long)maps.getValue()).longValue() - ((Long)maps1.getValue()).longValue());
      }
    });
    
    for (int i = 0; i < list.size(); i++) {
      newMap.put((CtOrderInfo)((Map.Entry)list.get(i)).getKey(), (Long)((Map.Entry)list.get(i)).getValue());
    }
    */
	  Map<CtOrderInfo, Long> newMap = new LinkedHashMap();
    return newMap;
    
  }
  
  public CtOrderInfo getOrderByOrderId(Long orderId)
  {
    return this.orderDao.getOrderByOrderId(orderId);
  }
  
  public List<CtOrderGoods> getOrderGoodsByOrderId(Long orderId, int i)
  {
    return this.orderDao.getOrderGoodsByOrderId(orderId, i);
  }
  
	@Logg(operationType="update", operationName="更新订单价格")
  public void updateTotalPriceByOrderId(CtOrderInfo ctOrderInfo)
  {
    this.orderDao.updateTotalPriceByOrderId(ctOrderInfo);
  }
	@Logg(operationType="update", operationName="更新订单状态")
  public void updateStartByOrderId(Long orderId, Long orderStatus)
  {
    this.orderDao.updateStartByOrderId(orderId, orderStatus);
  }
  
  public List<CtExpress> getAllExpress(Page page)
  {
    return this.orderDao.getAllExpress(page);
  }
  
  @Logg(operationType="update", operationName="更新订单已发货")
  public void updateExNoByOrderId(CtOrderInfo ctOrderInfo)
  {
    this.orderDao.updateExNoByOrderId(ctOrderInfo);
  }
  
  @Logg(operationType="save", operationName="新增快递信息")
  public int save(CtExpress express)
  {
    return this.orderDao.save(express);
  }
  @Logg(operationType="delete", operationName="删除快递信息")
  public int delete(Integer m)
  {
    return this.orderDao.delete(m);
  }
  
  @Logg(operationType="update", operationName="更新快递信息")
  public void update(CtExpress express)
  {
    this.orderDao.update(express);
  }
  
  public CtExpress findById(Integer id)
  {
    return this.orderDao.findById(id);
  }
  
  public CtEvaluation getEvaByOrderId(Long evaId)
  {
    return this.orderDao.getEvaByOrderId(evaId);
  }
  
  public void updateReturnStartByOrderId(Long orderId, String manName)
  {
    this.orderDao.updateReturnStartByOrderId(orderId, manName);
  }
  
  public void updateReturnManageByOrderId(Long orderId, String manName)
  {
    this.orderDao.updateReturnManageByOrderId(orderId, manName);
  }
  
  public void updateReturnFinanceByOrderId(Long orderId, String manName)
  {
    this.orderDao.updateReturnFinanceByOrderId(orderId, manName);
  }
  
  public List<CtExpress> findAll(String keyword, Page page)
  {
    return this.orderDao.findAll(keyword, page);
  }
  
  public List<CtComplain> getAllComplain(Page page)
  {
    return this.orderDao.getAllComplain(page);
  }
  
  public CtComplain getComplainByComId(Integer comId)
  {
    return this.orderDao.getComplainByComId(comId);
  }
  
  public void updateComplainByComId(CtComplain complain)
  {
    this.orderDao.updateComplainByComId(complain);
  }
  
  @Logg(operationType="update", operationName="信用支付线下支付打款审核通过恢复用户额度")
  public void updateuser(CtUser m)
  {
    this.orderDao.updateuser(m);
  }
  
  public String getOrderByshopId(Short shopId)
  {
    return this.orderDao.getOrderByshopId(shopId);
  }
  
  public CtPayInterface getPayInterByOrderSn(String orderSn)
  {
    return this.orderDao.getPayInterByOrderSn(orderSn);
  }
  
  public String getShengById(Integer province)
  {
    return this.orderDao.getShengById(province);
  }
  
  public String getShiById(Integer city)
  {
    return this.orderDao.getShiById(city);
  }
  
  public String getQuById(Integer district)
  {
    return this.orderDao.getQuById(district);
  }

	@Logg(operationType="update", operationName="更新订单信息")
@Override
public void updateOrderInfoByInfo(CtOrderInfo info) {
	orderDao.updateOrderInfoByInfo(info);
}

@Override
public List<CtOrderInfo> getOrderListByOrderSn(String orderSn) {
	return orderDao.getOrderListByOrderSn(orderSn);
}

@Override
public List<CtOrderGoods> getordergoodsByOrderId(Long orderId) {
	return orderDao.getordergoodsByOrderId(orderId);
}

@Override
public void updateOrderGoodsByOrderId(Long orderId) {
	orderDao.updateOrderGoodsByOrderId(orderId);
}

@Override
public String getFirstDateOrder() {
	return orderDao.getFirstDateOrder();
}

@Override
public List<CtOrderInfo> getorderByUidAndPayType(Long uid, String string, Page page) {
	return orderDao.getorderByUidAndPayType(uid, string, page);
}

@Override
public List<CtOrderRelation> findRelationsByUid(Long uid, Page page) {
	return orderDao.findRelationsByUid(uid, page);
}

@Override
public CtOrderRelation findRelatinsByOrid(Long orid) {
	return orderDao.findRelatinsByOrid(orid);
}

@Logg(operationType="update", operationName="更新订单退款状态退款失败")
@Override
public void updateOrderGoodsByOidAndGidAndType(Long refGoodsid,
		Long refOrderid, String refGoodsType) {
	orderDao.updateOrderGoodsByOidAndGidAndType(refGoodsid,refOrderid,refGoodsType);
}
}
