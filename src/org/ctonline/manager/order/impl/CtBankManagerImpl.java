package org.ctonline.manager.order.impl;

import java.util.List;
import org.ctonline.dao.order.ICtBankDAO;
import org.ctonline.manager.order.ICtBankManager;
import org.ctonline.po.order.CtBank;
import org.ctonline.system.Logg;
import org.ctonline.util.Page;

public class CtBankManagerImpl
  implements ICtBankManager
{
  private ICtBankDAO dao;
  
  public ICtBankDAO getDao()
  {
    return this.dao;
  }
  
  public void setDao(ICtBankDAO dao)
  {
    this.dao = dao;
  }
  
  public List findAll()
  {
    return this.dao.findAll();
  }
  
  public CtBank findById(Byte bid)
  {
    return this.dao.findById(bid);
  }
  
  public CtBank getBankById(Byte bankId)
  {
    return this.dao.getBankById(bankId);
  }
  
  public List<CtBank> loadAll(Page page)
  {
    return this.dao.loadAll(page);
  }
  
  @Logg(operationType="save", operationName="新增银行账户信息")
  public void save(CtBank bank)
  {
    this.dao.save(bank);
  }
  
  @Logg(operationType="delete", operationName="删除银行账户信息")
  public int delete(Integer id)
  {
    return this.dao.delete(id);
  }
  
  @Logg(operationType="update", operationName="更新银行账户信息")
  public void update(CtBank bank)
  {
    this.dao.update(bank);
  }
  
  public List<CtBank> findAll(String keyword, Page page)
  {
    return this.dao.findAll(keyword, page);
  }
  
  public Long totalCount(String str)
  {
    return this.dao.totalCount(str);
  }
}
