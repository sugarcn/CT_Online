package org.ctonline.manager.order.impl;

import java.util.List;
import org.ctonline.dao.order.ICtPayDAO;
import org.ctonline.manager.order.ICtPayManager;
import org.ctonline.po.order.CtOrderGoods;
import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.po.order.CtPay;
import org.ctonline.po.order.CtRefund;
import org.ctonline.po.pay.CtPayInterface;
import org.ctonline.system.Logg;
import org.ctonline.util.Page;

public class CtPayManagerImpl
  implements ICtPayManager
{
  private ICtPayDAO dao;
  
  public ICtPayDAO getDao()
  {
    return this.dao;
  }
  
  public List findAll(Page page)
  {
    return this.dao.findAll(page);
  }
  
  public void setDao(ICtPayDAO dao)
  {
    this.dao = dao;
  }
  
  public void saveOrderPay(CtPay ctPay)
  {
    this.dao.saveOrderPay(ctPay);
  }
  
  public CtPay getPayByOrderId(Long orderId)
  {
    return this.dao.getPayByOrderId(orderId);
  }

@Override
public List<CtRefund> getRefundByOrderId(Long orderId) {
	return dao.getRefundByOrderId(orderId);
}

@Override
public CtOrderGoods getOrderGoodsByRefund(Long refOrderid, Long gid, int type) {
	return dao.getOrderGoodsByRefund(refOrderid,gid , type);
}

@Override
public CtRefund getRefundByOrderIdAndGidAndType(String string, String string2,
		String string3) {
	return dao.getRefundByOrderIdAndGidAndType(string,string2,string3);
}

@Logg(operationType="update", operationName="更新退款状态已退款")
@Override
public void updateRRefundOk(CtRefund ctRefund, String refund_idBack,
		String out_refund_noBack) {
	dao.updateRRefundOk(ctRefund,refund_idBack,out_refund_noBack);
}

@Logg(operationType="update", operationName="更新订单信息-付款信息")
@Override
public void updateOrderInfo(CtOrderInfo ctOrderInfo) {
	dao.updateOrderInfo(ctOrderInfo);
}

@Logg(operationType="update", operationName="更新订单商品退款状态已退款")
@Override
public void updateOrderGoods(CtOrderGoods ctOrderGoods) {
	dao.updateOrderGoods(ctOrderGoods);
}

@Override
public CtPay findAllByOrderId(Long orderId) {
	return dao.findAllByOrderId(orderId);
}

@Override
public CtPayInterface getPayInterByOrderSn(String orderSn) {
	return dao.getPayInterByOrderSn(orderSn);
}

@Override
public CtRefund findOPatIntById(String string) {
	return dao.findOPatIntById(string);
}
@Logg(operationType="update", operationName="更新退款信息")
@Override
public void updateRef(CtRefund refunds) {
	dao.updateRef(refunds);
}

@Override
public CtPay findPayByPId(String str) {
	return dao.findPayByPId(str);
}
}
