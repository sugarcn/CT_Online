package org.ctonline.manager.order;

import java.util.List;

import org.ctonline.po.order.CtOrderGoods;
import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.po.order.CtPay;
import org.ctonline.po.order.CtRefund;
import org.ctonline.po.pay.CtPayInterface;
import org.ctonline.util.Page;

public abstract interface ICtPayManager
{
  public abstract List findAll(Page page);
  
  public abstract void saveOrderPay(CtPay paramCtPay);
  
  public abstract CtPay getPayByOrderId(Long paramLong);

public abstract List<CtRefund> getRefundByOrderId(Long orderId);

public abstract CtOrderGoods getOrderGoodsByRefund(Long refOrderid, Long gid, int type);

public abstract CtRefund getRefundByOrderIdAndGidAndType(String string,
		String string2, String string3);

public abstract void updateRRefundOk(CtRefund ctRefund, String refund_idBack,
		String out_refund_noBack);

public abstract void updateOrderInfo(CtOrderInfo ctOrderInfo);

public abstract void updateOrderGoods(CtOrderGoods ctOrderGoods);

public abstract CtPay findAllByOrderId(Long orderId);

public abstract CtPayInterface getPayInterByOrderSn(String orderSn);

public abstract CtRefund findOPatIntById(String string);

public abstract void updateRef(CtRefund refunds);

public abstract CtPay findPayByPId(String string);
}
