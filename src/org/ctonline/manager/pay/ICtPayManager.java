package org.ctonline.manager.pay;

import java.util.List;

import org.ctonline.dao.pay.ICtPayDao;
import org.ctonline.po.basic.CtPaymentWx;
import org.ctonline.util.Page;
import org.hibernate.SessionFactory;

public interface ICtPayManager {

	public abstract ICtPayDao getPayDao();

	public abstract void setPayDao(ICtPayDao payDao);

	public abstract SessionFactory getSessionFactory();

	public abstract void setSessionFactory(SessionFactory sessionFactory);

	public abstract List<CtPaymentWx> findAllByPage(Page page);

}