package org.ctonline.manager.pay.impl;

import java.util.List;

import org.ctonline.dao.pay.ICtPayDao;
import org.ctonline.manager.pay.ICtPayManager;
import org.ctonline.po.basic.CtPaymentWx;
import org.ctonline.util.Page;
import org.hibernate.SessionFactory;

public class CtPayManagerImpl implements ICtPayManager {

	private ICtPayDao payDao;

	@Override
	public ICtPayDao getPayDao() {
		return payDao;
	}

	@Override
	public void setPayDao(ICtPayDao payDao) {
		this.payDao = payDao;
	}

	@Override
	public SessionFactory getSessionFactory() {
		return payDao.getSessionFactory();
	}

	@Override
	public void setSessionFactory(SessionFactory sessionFactory) {
		payDao.setSessionFactory(sessionFactory);
	}

	@Override
	public List<CtPaymentWx> findAllByPage(Page page) {
		return payDao.findAllByPage(page);
	}
	
}
