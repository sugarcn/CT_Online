package org.ctonline.manager.front_goods.impl;

import java.util.List;

import org.ctonline.dao.front_goods.F_GoodsDAO;
import org.ctonline.manager.front_goods.F_GoodsManager;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.views.ViewCateNum;
import org.ctonline.po.views.ViewSubcateNum;

public class F_GoodsManagerImpl implements F_GoodsManager{

	private F_GoodsDAO fgoodsDao;

	public F_GoodsDAO getFgoodsDao() {
		return fgoodsDao;
	}

	public void setFgoodsDao(F_GoodsDAO fgoodsDao) {
		this.fgoodsDao = fgoodsDao;
	}

	@Override
	public List<CtGoodsCategory> queryAllCate() {
		// TODO Auto-generated method stub
		return fgoodsDao.queryAllCate();
	}

	@Override
	public List<ViewCateNum> cateCount() {
		// TODO Auto-generated method stub
		return fgoodsDao.cateCount();
	}

	@Override
	public List<ViewSubcateNum> subcateCount() {
		// TODO Auto-generated method stub
		return fgoodsDao.subcateCount();
	}
	
}
