package org.ctonline.manager.front_goods;

import java.util.List;

import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.views.ViewCateNum;
import org.ctonline.po.views.ViewSubcateNum;

public interface F_GoodsManager {
	public List<CtGoodsCategory> queryAllCate();
	public List<ViewCateNum> cateCount();
	public List<ViewSubcateNum> subcateCount();
}
