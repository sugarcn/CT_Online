package org.ctonline.manager.help.impl;

import java.util.List;
import org.ctonline.dao.help.CtHelpDAO;
import org.ctonline.manager.help.CtHelpManager;
import org.ctonline.po.help.CtHelp;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.system.Logg;
import org.ctonline.util.Page;

public class CtHelpManagerImpl
  implements CtHelpManager
{
  private CtHelpDAO helpDAO;

  public List<CtHelp> loadOne(int id)
  {
    return this.helpDAO.loadOne(id);
  }

  public CtHelpDAO getHelpDAO()
  {
    return this.helpDAO;
  }

  public void setHelpDAO(CtHelpDAO helpDAO) {
    this.helpDAO = helpDAO;
  }

  public CtHelp findById(int id)
  {
    return this.helpDAO.findById(id);
  }

  @Logg(operationType="save", operationName="新增帮助")
  public int save(CtHelp help)
  {
    return this.helpDAO.save(help);
  }

  public Long totalCount(String str)
  {
    return this.helpDAO.totalCount(str);
  }

  @Logg(operationType="delete", operationName="删除帮助")
  public int delete(int id)
  {
    return this.helpDAO.delete(id);
  }

  public CtHelp getHelpByHId(int Uid)
  {
    return this.helpDAO.getHelpByHId(Uid);
  }

  public List<CtHelp> loadAll(Page page)
  {
    return this.helpDAO.loadAll(page);
  }

  @Logg(operationType="update", operationName="更新了帮助")
  public void update(CtHelp help)
  {
    this.helpDAO.update(help);
  }

  public List<CtHelp> findAll(String keyword, Page page)
  {
    return this.helpDAO.findAll(keyword, page);
  }

  public List<CtHelpType> queryType()
  {
    return this.helpDAO.queryType();
  }
}