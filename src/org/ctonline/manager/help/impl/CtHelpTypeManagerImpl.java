package org.ctonline.manager.help.impl;

import java.util.List;
import org.ctonline.dao.help.CtHelpTypeDAO;
import org.ctonline.manager.help.CtHelpTypeManager;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.system.Logg;
import org.ctonline.util.Page;

public class CtHelpTypeManagerImpl
  implements CtHelpTypeManager
{
  private CtHelpTypeDAO helpTypeDao;

  public CtHelpTypeDAO getHelpTypeDao()
  {
    return this.helpTypeDao;
  }

  public void setHelpTypeDao(CtHelpTypeDAO helpTypeDao) {
    this.helpTypeDao = helpTypeDao;
  }

  @Logg(operationType="save", operationName="新增帮助类别")
  public Integer save(CtHelpType helpType)
  {
    return this.helpTypeDao.save(helpType);
  }

  public CtHelpType findById(int id)
  {
    return this.helpTypeDao.findById(id);
  }

  public List<CtHelpType> loadAll(Page page)
  {
    return this.helpTypeDao.loadAll(page);
  }

  @Logg(operationType="delete", operationName="删除了帮助类别")
  public int delete(int id)
  {
    return this.helpTypeDao.delete(id);
  }

  public Long totalCount(String str)
  {
    return this.helpTypeDao.totalCount(str);
  }

  @Logg(operationType="update", operationName="更新了帮助类别")
  public void update(CtHelpType helpType)
  {
    this.helpTypeDao.update(helpType);
  }

  public List<CtHelpType> findAll(String keyword, Page page)
  {
    return this.helpTypeDao.findAll(keyword, page);
  }

  public List<CtHelpType> queryType()
  {
    return this.helpTypeDao.queryType();
  }
}