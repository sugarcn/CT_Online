package org.ctonline.manager.help;

import java.util.List;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.util.Page;

public abstract interface CtHelpTypeManager
{
  public abstract Integer save(CtHelpType paramCtHelpType);

  public abstract CtHelpType findById(int paramInt);

  public abstract List<CtHelpType> loadAll(Page paramPage);

  public abstract int delete(int paramInt);

  public abstract Long totalCount(String paramString);

  public abstract void update(CtHelpType paramCtHelpType);

  public abstract List<CtHelpType> findAll(String paramString, Page paramPage);

  public abstract List<CtHelpType> queryType();
}