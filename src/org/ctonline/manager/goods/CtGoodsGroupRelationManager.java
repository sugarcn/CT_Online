package org.ctonline.manager.goods;

import java.util.List;

import org.ctonline.po.goods.CtGoodsAttributeRelation;
import org.ctonline.po.goods.CtGoodsGroupRelation;

public interface CtGoodsGroupRelationManager {

	
	public Long save(CtGoodsGroupRelation groupRelation);
	
	public List<CtGoodsGroupRelation>  selectAll(Long UGId);
	
	public Long totalCount(Long id);
	
	public int delete(Long id);

	public CtGoodsAttributeRelation findRelByGidAndAttrId(Long gtid, Long long1);
}
