package org.ctonline.manager.goods;

import java.util.List;

import org.ctonline.po.goods.CtGoodsLink;
import org.ctonline.po.views.ViewGoodsLink;

public interface CtGoodsLinkManager {
	public void tieLink(CtGoodsLink link);
	public List<ViewGoodsLink> queryTied(Long gid);
	public void unLink(Long gid,Long lid);
}
