package org.ctonline.manager.goods;

import java.util.List;

import org.ctonline.po.basic.CtCashCoupon;
import org.ctonline.po.basic.CtFreeSampleReg;
import org.ctonline.po.basic.CtReplace;
import org.ctonline.po.goods.CtCateGoods;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGoodsResource;
import org.ctonline.po.goods.CtGroupPrice;
import org.ctonline.po.goods.CtRangePrice;
import org.ctonline.po.goods.CtSearchCollect;
import org.ctonline.po.views.GoodsDetailView;
import org.ctonline.util.Page;

public abstract interface CtGoodsManager
{
  public abstract Long save(CtGoods paramCtGoods);
  
  public abstract CtGoods findById(Long paramLong);
  
  public abstract List<CtGoods> loadAll(Page paramPage);
  
  public abstract int delete(Long paramLong);
  
  public abstract Long totalCount(String paramString);
  
  public abstract void update(CtGoods paramCtGoods);
  
  public abstract List<CtGoods> findAll(String paramString, Page paramPage);
  
  public abstract Long getID();
  
  public abstract List<CtGoods> queryAll(String paramString);
  
  public abstract List<CtGoodsCategory> queryCategory(Long paramLong);
  
  public abstract List<CtGoodsBrand> queryBrand();
  
  public abstract void hide(Long paramLong);
  
  public abstract List<GoodsDetailView> queryDetail(Long paramLong);
  
  public abstract void empty();
  
  public abstract void tieRes(CtGoodsResource paramCtGoodsResource);
  
  public abstract List<?> selectAll(Long paramLong);
  
  public abstract void unTieRes(Long paramLong1, Long paramLong2);
  
  public abstract List<CtGoods> getGoodsByGname(String paramString);
  
  public abstract Object getOneGoodsByGname(String paramString);
  
  public abstract void saveRangePrice(CtRangePrice paramCtRangePrice);
  
  public abstract void savegroupPrice(CtGroupPrice paramCtGroupPrice);
  
  public abstract void saveCateGoodsInfo(CtCateGoods paramCtCateGoods);
  
  public abstract List<CtRangePrice> findRanParByGid(Long paramLong);
  
  public abstract void updateForName(String paramString1, String paramString2, String paramString3);

public abstract CtGoods getGoodsByGnameNoLike(String stringCellValue);

public abstract void deleteRenPriceByGid(Long gId);

public abstract void deleteRelationByGid(Long gtid);

public abstract CtGoods getGoodsByGSnNoLike(String trim);

public abstract List<CtFreeSampleReg> getFreeSamRegByPage(Page page);

public abstract CtFreeSampleReg findRegById(Integer samId);

public abstract void updateSamBySamId(Integer samId);

public abstract List<CtCashCoupon> getCashCoupon(Page page);

public abstract List<CtReplace> findReplaceByPage(Page page);

public abstract void saveReplace(CtReplace replace);

public abstract CtReplace findSearchKeyById(Integer id);

public abstract void deleteReplace(CtReplace replace);

public abstract List<CtReplace> findReplaceByKeyAndPage(String key, Page page);

public abstract List<CtSearchCollect> findSearchByPage(Page page);

public abstract CtSearchCollect findCollectById(Integer coId);

public abstract void updateCollect(CtSearchCollect searchCollect);

public abstract void deleteCollect(Long m);

public abstract List<CtGoods> findGoodsByKey(Page page, String keySerach);
}
