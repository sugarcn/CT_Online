package org.ctonline.manager.goods;

import java.util.List;

import org.ctonline.po.goods.CtBom;
import org.ctonline.po.goods.CtBomGoods;
import org.ctonline.util.Page;

public interface CtBomManager {

	//加载所有Bom
	public List<CtBom> loadAll(Page page,Long UId);
	//保存Bom
	public Integer save(CtBom ctBom);
	//根据BomId查询Bom
	public CtBom getCtBomByBomId(Integer BomId);
	//修改Bom
	public void update(CtBom ctBom);
	//批量删除Bom
	public void delete(Integer bomMid);
	//搜索
	public List<CtBom> findAll(String keyword, Page page,Long UId);
	//加载所有Bom详情
	public List<CtBomGoods> loadBomDetailAll(Page page,Integer bomId);
	//删除Bom详情中的某个商品
	public void delGoodsOfBomDetail(Long bomGoodsId);
	//搜索Bom详情中的商品
	public List<CtBomGoods> findBomDetailAll(String keyword, Page page,Integer bomId);
	//根据bomGoodsId查询CtBomGoods
	public CtBomGoods getCtBomGoodsBybomGoodsId(Long bomGoodsId);
	//保存CtBomGoods
	public Long saveCtBomGoods(CtBomGoods cbg);
	//修改CtBomGoods
	public void updateCtBomGoods(CtBomGoods cbg);
	//根据GId查询CtBomGoods
	public CtBomGoods getCtBomGoodsByGId(Long GId);
	//BOM中心列表
	public List<CtBom> loadAllBomCenter(Page page);
	//搜索
	public List<CtBom> findAllBomCenter(String keyword, Page page);
}
