package org.ctonline.manager.goods;

import java.util.List;

import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.util.Page;

public interface CtGoodsBrandManager {
	
	public Long save(CtGoodsBrand brand);//淇濆瓨
	public CtGoodsBrand findById(Long id);//鏍规嵁id鏌ユ壘
	public List<CtGoodsBrand> loadAll(Page page);//鍒嗛〉鎵惧嚭鎵�湁
	public int delete(Long id);//鍒犻櫎涓�潯璁板綍
	public Long totalCount(String str);//缁熻鏉℃暟
	public void update(CtGoodsBrand brand);//鏇存柊
	public List<CtGoodsBrand> findAll(String keyword,Page page);//鏍规嵁鏉′欢妫�储
	public List<CtGoodsBrand> queryAll();
}
