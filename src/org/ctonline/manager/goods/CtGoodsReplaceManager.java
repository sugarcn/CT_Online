package org.ctonline.manager.goods;

import java.util.List;

import org.ctonline.po.goods.CtGoodsReplace;
import org.ctonline.po.views.ViewGoodsReplace;

public interface CtGoodsReplaceManager {
	public void tieReplace(CtGoodsReplace replace);
	public void unReplace(Long gid,Long rid);
	public List<ViewGoodsReplace> queryRepList(Long gid);
}
