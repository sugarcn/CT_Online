package org.ctonline.manager.goods;

import java.util.List;

import org.ctonline.po.goods.CtPrice;
import org.ctonline.po.views.ViewGoodsPrice;
import org.ctonline.util.Page;

	public interface CtPriceManager {
		public List<ViewGoodsPrice> loadAll(Page page);
		public Long totalCount(String str);
		public Long insertPrice(CtPrice price);
		public List<CtPrice> checkPrice(Long gid,String gtype,Long uid,String utype);
		public void delPrice(Long id);
		public void updatePrice(CtPrice price);
}
