package org.ctonline.manager.goods;

import java.util.List;

import org.ctonline.po.views.ViewGoodsGroup;

public interface ViewGoodsGroupManager {
	public List<ViewGoodsGroup> query(String goodsname);
}
