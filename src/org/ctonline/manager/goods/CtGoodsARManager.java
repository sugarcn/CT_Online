package org.ctonline.manager.goods;

import org.ctonline.po.goods.CtGoodsAttributeRelation;
import java.util.List;
import org.ctonline.po.views.ViewGoodsAttribute;

public interface CtGoodsARManager {
//	public void tieAR(CtGoodsAttributeRelation relation);
	  public abstract Long save(CtGoodsAttributeRelation paramCtGoodsAttributeRelation);
	  
	  public abstract List<ViewGoodsAttribute> findById(Long paramLong);
}
