package org.ctonline.manager.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsAttributeDAO;
import org.ctonline.manager.goods.CtGoodsAttributeManager;
import org.ctonline.po.goods.CtGoodsAttribute;
import org.ctonline.po.views.ViewGoodsAttribute;
import org.ctonline.util.Page;

public class CtGoodsAttributeManagerImpl implements CtGoodsAttributeManager{
	
	private CtGoodsAttributeDAO attributeDao;

	public CtGoodsAttributeDAO getAttributeDao() {
		return attributeDao;
	}

	public void setAttributeDao(CtGoodsAttributeDAO attributeDao) {
		this.attributeDao = attributeDao;
	}
	
	@Override
	public Long save(CtGoodsAttribute attribute) {
		// TODO Auto-generated method stub
		return attributeDao.save(attribute);
	}

	@Override
	public CtGoodsAttribute findById(Long id) {
		// TODO Auto-generated method stub
		return attributeDao.findById(id);
	}

	@Override
	public List<CtGoodsAttribute> loadAll(Page page) {
		// TODO Auto-generated method stub
		return attributeDao.loadAll(page);
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		return attributeDao.delete(id);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return attributeDao.totalCount(str);
	}

	@Override
	public void update(CtGoodsAttribute attribute) {
		// TODO Auto-generated method stub
		attributeDao.update(attribute);
	}

	@Override
	public List<CtGoodsAttribute> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return attributeDao.findAll(keyword, page);
	}

	@Override
	public List<CtGoodsAttribute> searchAll(Long id,Page page){
		
		return attributeDao.searchAll(id, page);
	}

	@Override
	public List<CtGoodsAttribute> queryByTID(Long tid) {
		// TODO Auto-generated method stub
		return attributeDao.queryByTID(tid);
	}

	@Override
	public List<ViewGoodsAttribute> queryByGId(Long gid) {
		// TODO Auto-generated method stub
		return attributeDao.queryByGId(gid);
	}

	@Override
	public void delAttRelation(Long gid) {
		// TODO Auto-generated method stub
		attributeDao.delAttRelation(gid);
	}
}
