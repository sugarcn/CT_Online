package org.ctonline.manager.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsLinkDAO;
import org.ctonline.manager.goods.CtGoodsLinkManager;
import org.ctonline.po.goods.CtGoodsLink;
import org.ctonline.po.views.ViewGoodsLink;

public class CtGoodsLinkManagerImpl implements CtGoodsLinkManager{

	private CtGoodsLinkDAO goodslinkDao;
	
	public CtGoodsLinkDAO getGoodslinkDao() {
		return goodslinkDao;
	}

	public void setGoodslinkDao(CtGoodsLinkDAO goodslinkDao) {
		this.goodslinkDao = goodslinkDao;
	}

	@Override
	public void tieLink(CtGoodsLink link) {
		// TODO Auto-generated method stub
		goodslinkDao.tieLink(link);
	}

	@Override
	public List<ViewGoodsLink> queryTied(Long gid) {
		// TODO Auto-generated method stub
		return goodslinkDao.queryTied(gid);
	}

	@Override
	public void unLink(Long gid, Long lid) {
		// TODO Auto-generated method stub
		goodslinkDao.unLink(gid, lid);
	}

}
