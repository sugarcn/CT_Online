package org.ctonline.manager.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsDetailDAO;
import org.ctonline.manager.goods.CtGoodsDetailManager;
import org.ctonline.po.goods.CtGoodsDetail;

public class CtGoodsDetailManagerImpl implements CtGoodsDetailManager{

	private CtGoodsDetailDAO detailDao;

	public CtGoodsDetailDAO getDetailDao() {
		return detailDao;
	}

	public void setDetailDao(CtGoodsDetailDAO detailDao) {
		this.detailDao = detailDao;
	}

	@Override
	public void addDetl(CtGoodsDetail detail) {
		// TODO Auto-generated method stub
		detailDao.addDetl(detail);
	}

	@Override
	public List<CtGoodsDetail> queryByGid(Long gid) {
		// TODO Auto-generated method stub
		return detailDao.queryByGid(gid);
	}

	@Override
	public void updateDetl(CtGoodsDetail detail) {
		// TODO Auto-generated method stub
		detailDao.updateDetl(detail);
	}
	

}
