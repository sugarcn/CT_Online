package org.ctonline.manager.goods.impl;

import java.util.List;
import org.ctonline.dao.goods.CtGoodsDAO;
import org.ctonline.manager.goods.CtGoodsManager;
import org.ctonline.po.basic.CtCashCoupon;
import org.ctonline.po.basic.CtFreeSampleReg;
import org.ctonline.po.basic.CtReplace;
import org.ctonline.po.goods.CtCateGoods;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGoodsResource;
import org.ctonline.po.goods.CtGroupPrice;
import org.ctonline.po.goods.CtRangePrice;
import org.ctonline.po.goods.CtSearchCollect;
import org.ctonline.po.views.GoodsDetailView;
import org.ctonline.system.Logg;
import org.ctonline.util.Page;

public class CtGoodsManagerImpl
  implements CtGoodsManager
{
  private CtGoodsDAO goodsDao;
  
  public CtGoodsDAO getgoodsDao()
  {
    return this.goodsDao;
  }
  
  public void setGoodsDao(CtGoodsDAO goodsDao)
  {
    this.goodsDao = goodsDao;
  }
  
  public Long save(CtGoods goods)
  {
    return this.goodsDao.save(goods);
  }
  
  public CtGoods findById(Long id)
  {
    return this.goodsDao.findById(id);
  }
  
  public List<CtGoods> loadAll(Page page)
  {
    return this.goodsDao.loadAll(page);
  }
  
  @Logg(operationType="delete", operationName="删除商品")
  public int delete(Long id)
  {
    return this.goodsDao.delete(id);
  }
  
  public Long totalCount(String str)
  {
    return this.goodsDao.totalCount(str);
  }
  
  public void update(CtGoods goods)
  {
    this.goodsDao.update(goods);
  }
  
  public List<CtGoods> findAll(String keyword, Page page)
  {
    return this.goodsDao.findAll(keyword, page);
  }
  
  public Long getID()
  {
    return this.goodsDao.getID();
  }
  
  public List<CtGoods> queryAll(String status)
  {
    return this.goodsDao.queryAll(status);
  }
  
  public List<CtGoodsCategory> queryCategory(Long id)
  {
    return this.goodsDao.queryCategory(id);
  }
  
  public List<CtGoodsBrand> queryBrand()
  {
    return this.goodsDao.queryBrand();
  }
  
  public void hide(Long id)
  {
    this.goodsDao.hide(id);
  }
  
  public List<GoodsDetailView> queryDetail(Long id)
  {
    return this.goodsDao.queryDetail(id);
  }
  
  public void empty()
  {
    this.goodsDao.empty();
  }
  
  public void tieRes(CtGoodsResource res)
  {
    this.goodsDao.tieRes(res);
  }
  
  public List<?> selectAll(Long GGId)
  {
    return this.goodsDao.selectAll(GGId);
  }
  
  public void unTieRes(Long gid, Long rid)
  {
    this.goodsDao.unTieRes(gid, rid);
  }
  
  public List<CtGoods> getGoodsByGname(String Gname)
  {
    return this.goodsDao.getGoodsByGname(Gname);
  }
  
  public Object getOneGoodsByGname(String Gname)
  {
    return this.goodsDao.getOneGoodsByGname(Gname);
  }
  
  public void saveRangePrice(CtRangePrice rangePrice)
  {
    this.goodsDao.saveRangePrice(rangePrice);
  }
  
  public void savegroupPrice(CtGroupPrice groupPrice)
  {
    this.goodsDao.savegroupPrice(groupPrice);
  }
  
  public void saveCateGoodsInfo(CtCateGoods cateGoods)
  {
    this.goodsDao.saveCateGoodsInfo(cateGoods);
  }
  
  public List<CtRangePrice> findRanParByGid(Long gId)
  {
    return this.goodsDao.findRanParByGid(gId);
  }
  
  public void updateForName(String tname, String sn, String hits)
  {
    this.goodsDao.updateForName(tname, sn, hits);
  }

@Override
public CtGoods getGoodsByGnameNoLike(String stringCellValue) {
	return goodsDao.getGoodsByGnameNoLike(stringCellValue);
}

@Override
public void deleteRenPriceByGid(Long gId) {
	goodsDao.deleteRenPriceByGid(gId);
}

@Override
public void deleteRelationByGid(Long gid) {
	// TODO Auto-generated method stub
	goodsDao.deleteRelationByGid(gid);
}

@Override
public CtGoods getGoodsByGSnNoLike(String trim) {
	// TODO Auto-generated method stub
	return goodsDao.getGoodsByGSnNoLike(trim);
}

@Override
public List<CtFreeSampleReg> getFreeSamRegByPage(Page page) {
	return goodsDao.getFreeSamRegByPage(page);
}

@Override
public CtFreeSampleReg findRegById(Integer samId) {
	return goodsDao.findRegById(samId);
}

@Override
public void updateSamBySamId(Integer samId) {
	goodsDao.updateSamBySamId(samId);
}

@Override
public List<CtCashCoupon> getCashCoupon(Page page) {
	return goodsDao.getCashCoupon(page);
}

@Override
public List<CtReplace> findReplaceByPage(Page page) {
	return goodsDao.findReplaceByPage(page);
}

@Override
public void saveReplace(CtReplace replace) {
	goodsDao.saveReplace(replace);
}

@Override
public CtReplace findSearchKeyById(Integer id) {
	return goodsDao.findSearchKeyById(id);
}

@Override
public void deleteReplace(CtReplace replace) {
	goodsDao.deleteReplace(replace);
}

@Override
public List<CtReplace> findReplaceByKeyAndPage(String key, Page page) {
	return goodsDao.findReplaceByKeyAndPage(key, page);
}

@Override
public List<CtSearchCollect> findSearchByPage(Page page) {
	return goodsDao.findSearchByPage(page);
}

@Override
public CtSearchCollect findCollectById(Integer coId) {
	return goodsDao.findCollectById(coId);
}

@Override
public void updateCollect(CtSearchCollect searchCollect) {
	goodsDao.updateCollect(searchCollect);
}

@Override
public void deleteCollect(Long m) {
	goodsDao.deleteCollect(m);
}

@Override
public List<CtGoods> findGoodsByKey(Page page, String keySerach) {
	return goodsDao.findGoodsByKey(page, keySerach);
}
}
