package org.ctonline.manager.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsTypeDAO;
import org.ctonline.manager.goods.CtGoodsTypeManager;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsType;
import org.ctonline.util.Page;

public class CtGoodsTypeManagerImpl implements CtGoodsTypeManager{
	
private CtGoodsTypeDAO typeDao;
	
	
	public CtGoodsTypeDAO getTypeDao() {
	return typeDao;
}

public void setTypeDao(CtGoodsTypeDAO typeDao) {
	this.typeDao = typeDao;
}

	@Override
	public Long save(CtGoodsType goodsType) {
		// TODO Auto-generated method stub
		return typeDao.save(goodsType);
	}

	@Override
	public CtGoodsType findById(Long id) {
		// TODO Auto-generated method stub
		return typeDao.findById(id);
	}

	@Override
	public List<CtGoodsType> loadAll(Page page) {
		// TODO Auto-generated method stub
		return typeDao.loadAll(page);
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		return typeDao.delete(id);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return typeDao.totalCount(str);
	}

	@Override
	public void update(CtGoodsType goodsType) {
		// TODO Auto-generated method stub
		typeDao.update(goodsType);
	}

	@Override
	public List<CtGoodsType> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return typeDao.findAll(keyword, page);
	}

	@Override
	public Long getID() {
		// TODO Auto-generated method stub
		return typeDao.getID();
	}

	@Override
	public List<CtGoodsType> queryAll() {
		// TODO Auto-generated method stub
		return typeDao.queryAll();
	}

}
