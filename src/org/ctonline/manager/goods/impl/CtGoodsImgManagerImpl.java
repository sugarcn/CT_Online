package org.ctonline.manager.goods.impl;

import java.util.List;
import org.ctonline.dao.goods.CtGoodsImgDAO;
import org.ctonline.manager.goods.CtGoodsImgManager;
import org.ctonline.po.goods.CtGoodsImg;

public class CtGoodsImgManagerImpl
  implements CtGoodsImgManager
{
  private CtGoodsImgDAO imgDao;
  
  public CtGoodsImgDAO getimgDao()
  {
    return this.imgDao;
  }
  
  public void setimgDao(CtGoodsImgDAO imgDao)
  {
    this.imgDao = imgDao;
  }
  
  public void addImg(CtGoodsImg img)
  {
    this.imgDao.addImg(img);
  }
  
  public List<CtGoodsImg> queryByGid(Long gid)
  {
    return this.imgDao.queryByGid(gid);
  }
  
  public void updateImg(CtGoodsImg img)
  {
    this.imgDao.updateImg(img);
  }
}
