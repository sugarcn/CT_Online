package org.ctonline.manager.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtPriceDAO;
import org.ctonline.manager.goods.CtPriceManager;
import org.ctonline.po.goods.CtPrice;
import org.ctonline.po.views.ViewGoodsPrice;
import org.ctonline.util.Page;

public class CtPriceManagerImpl implements CtPriceManager{
	private CtPriceDAO priceDao;

	public CtPriceDAO getPriceDao() {
		return priceDao;
	}

	public void setPriceDao(CtPriceDAO priceDao) {
		this.priceDao = priceDao;
	}

	@Override
	public Long insertPrice(CtPrice price) {
		// TODO Auto-generated method stub
		return priceDao.insertPrice(price);
	}

	@Override
	public List<CtPrice> checkPrice(Long gid,String gtype,Long uid,String utype) {
		// TODO Auto-generated method stub
		return priceDao.checkPrice(gid,gtype,uid,utype);
	}

	@Override
	public void delPrice(Long id) {
		// TODO Auto-generated method stub
		priceDao.delPrice(id);
	}

	@Override
	public void updatePrice(CtPrice price) {
		// TODO Auto-generated method stub
		priceDao.updatePrice(price);
	}

	@Override
	public List<ViewGoodsPrice> loadAll(Page page) {
		// TODO Auto-generated method stub
		return priceDao.loadAll(page);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return priceDao.totalCount(str);
	}
	
}
