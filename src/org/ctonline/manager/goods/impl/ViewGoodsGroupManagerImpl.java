package org.ctonline.manager.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.ViewGoodsGroupDAO;
import org.ctonline.manager.goods.ViewGoodsGroupManager;
import org.ctonline.po.views.ViewGoodsGroup;

public class ViewGoodsGroupManagerImpl implements ViewGoodsGroupManager{

	private ViewGoodsGroupDAO viewgroupDao;
	
	
	public ViewGoodsGroupDAO getViewgroupDao() {
		return viewgroupDao;
	}


	public void setViewgroupDao(ViewGoodsGroupDAO viewgroupDao) {
		this.viewgroupDao = viewgroupDao;
	}


	@Override
	public List<ViewGoodsGroup> query(String goodsname) {
		// TODO Auto-generated method stub
		return viewgroupDao.query(goodsname);
	}

}
