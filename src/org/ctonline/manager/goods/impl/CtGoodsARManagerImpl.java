package org.ctonline.manager.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsARDAO;
import org.ctonline.manager.goods.CtGoodsARManager;
import org.ctonline.po.goods.CtGoodsAttributeRelation;
import org.ctonline.po.views.ViewGoodsAttribute;

public class CtGoodsARManagerImpl implements CtGoodsARManager{

	private CtGoodsARDAO goodsarDao;
	

	public CtGoodsARDAO getGoodsarDao() {
		return goodsarDao;
	}


	public void setGoodsarDao(CtGoodsARDAO goodsarDao) {
		this.goodsarDao = goodsarDao;
	}


	public Long save(CtGoodsAttributeRelation relation)
	{
	  return this.goodsarDao.save(relation);
	}
	  
	public List<ViewGoodsAttribute> findById(Long gid)
	{
	  return this.goodsarDao.findById(gid);
	}

}
