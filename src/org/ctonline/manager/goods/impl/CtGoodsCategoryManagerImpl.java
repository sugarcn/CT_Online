package org.ctonline.manager.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsCategoryDAO;
import org.ctonline.manager.goods.CtGoodsCategoryManager;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;

public class CtGoodsCategoryManagerImpl implements CtGoodsCategoryManager{
	
	private CtGoodsCategoryDAO categoryDao;

	public CtGoodsCategoryDAO getCategoryDao() {
		return categoryDao;
	}

	public void setCategoryDao(CtGoodsCategoryDAO categoryDao) {
		this.categoryDao = categoryDao;
	}
	
	@Override
	public Long save(CtGoodsCategory category) {
		// TODO Auto-generated method stub
		return categoryDao.save(category);
	}
	
	@Override
	public CtGoodsCategory findById(Long id) {
		// TODO Auto-generated method stub
		return categoryDao.findById(id);
	}

	@Override
	public List<CtGoodsCategory> loadAll(Page page) {
		// TODO Auto-generated method stub
		return categoryDao.loadAll(page);
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		return categoryDao.delete(id);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return this.categoryDao.totalCount(str);
	}

	@Override
	public void update(CtGoodsCategory category) {
		// TODO Auto-generated method stub
		this.categoryDao.update(category);
	}

	@Override
	public List<CtGoodsCategory> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return categoryDao.findAll(keyword,page);
	}

	@Override
	public List<CtGoodsCategory> levelquery(Long id) {
		// TODO Auto-generated method stub
		return categoryDao.levelquery(id);
	}
	
	@Override
	public List<CtGoodsCategory> searchAll(Long id,Page page){
		
		return categoryDao.searchAll(id, page);
	}

}
