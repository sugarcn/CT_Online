package org.ctonline.manager.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsGroupDAO;
import org.ctonline.manager.goods.CtGoodsGroupManager;
import org.ctonline.po.goods.CtGoodsGroup;
import org.ctonline.util.Page;

public class CtGoodsGroupManagerImpl implements CtGoodsGroupManager{
	private CtGoodsGroupDAO goodsGroupDao;

	public CtGoodsGroupDAO getGoodsGroupDao() {
		return goodsGroupDao;
	}

	public void setGoodsGroupDao(CtGoodsGroupDAO goodsGroupDao) {
		this.goodsGroupDao = goodsGroupDao;
	}
	
	@Override
	public Long save(CtGoodsGroup goodsGroup) {
		// TODO Auto-generated method stub
		return goodsGroupDao.save(goodsGroup);
	}
	
	@Override
	public CtGoodsGroup findById(Long id) {
		// TODO Auto-generated method stub
		return goodsGroupDao.findById(id);
	}

	@Override
	public List<CtGoodsGroup> loadAll(Page page) {
		// TODO Auto-generated method stub
		return goodsGroupDao.loadAll(page);
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		return goodsGroupDao.delete(id);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return this.goodsGroupDao.totalCount(str);
	}

	@Override
	public void update(CtGoodsGroup goodsGroup) {
		// TODO Auto-generated method stub
		this.goodsGroupDao.update(goodsGroup);
	}

	@Override
	public List<CtGoodsGroup> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return goodsGroupDao.findAll(keyword,page);
	}

}
