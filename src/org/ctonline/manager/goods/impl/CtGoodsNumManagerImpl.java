package org.ctonline.manager.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsNumDAO;
import org.ctonline.manager.goods.CtGoodsNumManager;
import org.ctonline.po.goods.CtGoodsNum;

public class CtGoodsNumManagerImpl implements CtGoodsNumManager{

	private CtGoodsNumDAO goodsnumDao;
	
	public CtGoodsNumDAO getGoodsnumDao() {
		return goodsnumDao;
	}

	public void setGoodsnumDao(CtGoodsNumDAO goodsnumDao) {
		this.goodsnumDao = goodsnumDao;
	}

	@Override
	public List<CtGoodsNum> queryByGId(Long gid) {
		// TODO Auto-generated method stub
		return goodsnumDao.queryByGId(gid);
	}

	@Override
	public List<?> testQuery(Long gid) {
		// TODO Auto-generated method stub
		return goodsnumDao.testQuery(gid);
	}

	@Override
	public void tieDepot(CtGoodsNum num) {
		// TODO Auto-generated method stub
		goodsnumDao.tieDepot(num);
	}

	@Override
	public void updateNum(CtGoodsNum num) {
		// TODO Auto-generated method stub
		goodsnumDao.updateNum(num);
	}


}
