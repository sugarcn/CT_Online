package org.ctonline.manager.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsBrandDAO;
import org.ctonline.manager.goods.CtGoodsBrandManager;
import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.util.Page;

public class CtGoodsBrandManagerImpl implements CtGoodsBrandManager {
	
	private CtGoodsBrandDAO brandDao;

	public CtGoodsBrandDAO getBrandDao() {
		return brandDao;
	}

	public void setBrandDao(CtGoodsBrandDAO brandDao) {
		this.brandDao = brandDao;
	}
	
	@Override
	public Long save(CtGoodsBrand brand) {
		// TODO Auto-generated method stub
		return brandDao.save(brand);
	}
	
	@Override
	public CtGoodsBrand findById(Long id) {
		// TODO Auto-generated method stub
		return brandDao.findById(id);
	}

	@Override
	public List<CtGoodsBrand> loadAll(Page page) {
		// TODO Auto-generated method stub
		return brandDao.loadAll(page);
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		return brandDao.delete(id);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return this.brandDao.totalCount(str);
	}

	
	 
	@Override
	public void update(CtGoodsBrand brand) {
		// TODO Auto-generated method stub
		this.brandDao.update(brand);
	}

	@Override
	public List<CtGoodsBrand> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return brandDao.findAll(keyword,page);
	}

	@Override
	public List<CtGoodsBrand> queryAll() {
		// TODO Auto-generated method stub
		return brandDao.queryAll();
	}

}
