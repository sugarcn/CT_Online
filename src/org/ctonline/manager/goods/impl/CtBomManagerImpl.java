package org.ctonline.manager.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtBomDao;
import org.ctonline.manager.goods.CtBomManager;
import org.ctonline.po.goods.CtBom;
import org.ctonline.po.goods.CtBomGoods;
import org.ctonline.util.Page;

public class CtBomManagerImpl implements CtBomManager {
	private CtBomDao bomDao;

	public CtBomDao getBomDao() {
		return bomDao;
	}

	public void setBomDao(CtBomDao bomDao) {
		this.bomDao = bomDao;
	}

	@Override
	public List<CtBom> loadAll(Page page,Long UId) {
		// TODO Auto-generated method stub
		return this.bomDao.loadAll(page,UId);
	}

	@Override
	public Integer save(CtBom ctBom) {
		// TODO Auto-generated method stub
		return this.bomDao.save(ctBom);
	}

	@Override
	public CtBom getCtBomByBomId(Integer BomId) {
		// TODO Auto-generated method stub
		return this.bomDao.getCtBomByBomId(BomId);
	}

	@Override
	public void update(CtBom ctBom) {
		// TODO Auto-generated method stub
		this.bomDao.update(ctBom);
	}

	@Override
	public void delete(Integer bomMid) {
		// TODO Auto-generated method stub
		this.bomDao.delete(bomMid);
	}

	@Override
	public List<CtBom> findAll(String keyword, Page page,Long UId) {
		// TODO Auto-generated method stub
		return this.bomDao.findAll(keyword, page,UId);
	}

	@Override
	public List<CtBomGoods> loadBomDetailAll(Page page,Integer bomId) {
		// TODO Auto-generated method stub
		return this.bomDao.loadBomDetailAll(page,bomId);
	}

	@Override
	public void delGoodsOfBomDetail(Long bomGoodsId) {
		// TODO Auto-generated method stub
		this.bomDao.delGoodsOfBomDetail(bomGoodsId);
	}

	@Override
	public List<CtBomGoods> findBomDetailAll(String keyword, Page page,Integer bomId) {
		// TODO Auto-generated method stub
		return this.bomDao.findBomDetailAll(keyword, page,bomId);
	}

	@Override
	public CtBomGoods getCtBomGoodsBybomGoodsId(Long bomGoodsId) {
		// TODO Auto-generated method stub
		return this.bomDao.getCtBomGoodsBybomGoodsId(bomGoodsId);
	}

	@Override
	public Long saveCtBomGoods(CtBomGoods cbg) {
		// TODO Auto-generated method stub
		return this.bomDao.saveCtBomGoods(cbg);
	}

	@Override
	public void updateCtBomGoods(CtBomGoods cbg) {
		// TODO Auto-generated method stub
		this.bomDao.updateCtBomGoods(cbg);
	}

	@Override
	public CtBomGoods getCtBomGoodsByGId(Long GId) {
		// TODO Auto-generated method stub
		return  this.bomDao.getCtBomGoodsByGId(GId);
	}

	@Override
	public List<CtBom> loadAllBomCenter(Page page) {
		// TODO Auto-generated method stub
		return this.bomDao.loadAllBomCenter(page);
	}

	@Override
	public List<CtBom> findAllBomCenter(String keyword, Page page) {
		// TODO Auto-generated method stub
		return this.bomDao.findAllBomCenter(keyword, page);
	}

}
