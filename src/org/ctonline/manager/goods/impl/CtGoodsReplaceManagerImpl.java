package org.ctonline.manager.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsReplaceDAO;
import org.ctonline.manager.goods.CtGoodsReplaceManager;
import org.ctonline.po.goods.CtGoodsReplace;
import org.ctonline.po.views.ViewGoodsReplace;

public class CtGoodsReplaceManagerImpl implements CtGoodsReplaceManager{

	private CtGoodsReplaceDAO goodsreplaceDao;
	
	public CtGoodsReplaceDAO getGoodsreplaceDao() {
		return goodsreplaceDao;
	}

	public void setGoodsreplaceDao(CtGoodsReplaceDAO goodsreplaceDao) {
		this.goodsreplaceDao = goodsreplaceDao;
	}

	@Override
	public void tieReplace(CtGoodsReplace replace) {
		// TODO Auto-generated method stub
		goodsreplaceDao.tieReplace(replace);
	}

	@Override
	public void unReplace(Long gid, Long rid) {
		// TODO Auto-generated method stub
		goodsreplaceDao.unReplace(gid, rid);
	}

	@Override
	public List<ViewGoodsReplace> queryRepList(Long gid) {
		// TODO Auto-generated method stub
		return goodsreplaceDao.queryRepList(gid);
	}

}
