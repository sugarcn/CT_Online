package org.ctonline.manager.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsGroupRelationDAO;
import org.ctonline.dao.user.CtUserGroupRelationDAO;
import org.ctonline.manager.goods.CtGoodsGroupRelationManager;
import org.ctonline.manager.user.CtUserGroupRelationManager;
import org.ctonline.po.goods.CtGoodsAttributeRelation;
import org.ctonline.po.goods.CtGoodsGroupRelation;
import org.ctonline.po.user.CtUserGroupRelation;

public class CtGoodsGroupRelationManagerImpl implements CtGoodsGroupRelationManager{

	private CtGoodsGroupRelationDAO groupRelationDao;

	public CtGoodsGroupRelationDAO getGroupRelationDao() {
		return groupRelationDao;
	}

	public void setGroupRelationDao(CtGoodsGroupRelationDAO groupRelationDao) {
		this.groupRelationDao = groupRelationDao;
	}

	@Override
	public Long save(CtGoodsGroupRelation groupRelation){
		
		return this.groupRelationDao.save(groupRelation);
	}
	
	@Override
	public List<CtGoodsGroupRelation>  selectAll(Long UGId){
		
		return this.groupRelationDao.selectAll(UGId);
	}
	
	@Override
	public Long totalCount(Long id){
		
		return this.groupRelationDao.totalCount(id);
	}
	
	@Override
	public int delete(Long id){
		
		return this.groupRelationDao.delete(id);
	}

	@Override
	public CtGoodsAttributeRelation findRelByGidAndAttrId(Long gtid, Long long1) {
		return groupRelationDao.findRelByGidAndAttrId(gtid, long1);
	}
}
