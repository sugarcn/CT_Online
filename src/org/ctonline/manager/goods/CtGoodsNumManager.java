package org.ctonline.manager.goods;

import java.util.List;

import org.ctonline.po.goods.CtGoodsNum;

public interface CtGoodsNumManager {
	public List<CtGoodsNum> queryByGId(Long gid);
	public List<?> testQuery(Long gid);
	public void tieDepot(CtGoodsNum num);
	public void updateNum(CtGoodsNum num);
}
