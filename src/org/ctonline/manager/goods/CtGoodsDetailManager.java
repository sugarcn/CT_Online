package org.ctonline.manager.goods;

import java.util.List;

import org.ctonline.po.goods.CtGoodsDetail;

public interface CtGoodsDetailManager {
	public void addDetl(CtGoodsDetail detail);//添加详细描述
	public List<CtGoodsDetail> queryByGid(Long gid);
	public void updateDetl(CtGoodsDetail detail);
}
