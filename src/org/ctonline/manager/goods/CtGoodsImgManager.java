package org.ctonline.manager.goods;

import java.util.List;
import org.ctonline.po.goods.CtGoodsImg;

public abstract interface CtGoodsImgManager
{
  public abstract void addImg(CtGoodsImg paramCtGoodsImg);
  
  public abstract List<CtGoodsImg> queryByGid(Long paramLong);
  
  public abstract void updateImg(CtGoodsImg paramCtGoodsImg);
}
