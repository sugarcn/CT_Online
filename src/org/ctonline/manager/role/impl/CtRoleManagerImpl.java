package org.ctonline.manager.role.impl;

import java.util.List;

import org.ctonline.dao.role.CtRoleDao;
import org.ctonline.manager.role.CtRoleManager;
import org.ctonline.po.basic.CtModule;
import org.ctonline.po.user.CtRole;
import org.ctonline.po.user.CtRoleModule;
import org.ctonline.util.Page;

public class CtRoleManagerImpl implements CtRoleManager {

	private CtRoleDao roleDao;
	@Override
	public List<CtRole> loadAll(Page page) {
		// TODO Auto-generated method stub
		return this.roleDao.loadAll(page);
	}
	public CtRoleDao getRoleDao() {
		return roleDao;
	}
	public void setRoleDao(CtRoleDao roleDao) {
		this.roleDao = roleDao;
	}
	@Override
	public Integer save(CtRole role) {
		// TODO Auto-generated method stub
		return this.roleDao.save(role);
	}
	@Override
	public List<?> getModuleList(Integer id) {
		// TODO Auto-generated method stub
		return this.roleDao.getModuleList(id);
	}
	@Override
	public CtRole getRoleByRoleId(Integer roleId) {
		// TODO Auto-generated method stub
		return this.roleDao.getRoleByRoleId(roleId);
	}
	@Override
	public void updateRole(CtRole cr) {
		// TODO Auto-generated method stub
		this.roleDao.updateRole(cr);
	}
	@Override
	public Integer delRole(Integer id) {
		// TODO Auto-generated method stub
		int d = this.roleDao.delRole(id);
		return d;
	}
	@Override
	public List<CtModule> getModuleList2() {
		// TODO Auto-generated method stub
		return this.roleDao.getModuleList2();
	}
	@Override
	public void save(CtRoleModule crm) {
		// TODO Auto-generated method stub
		this.roleDao.save(crm);
	}
	@Override
	public CtRoleModule queryCtRoleModuleByRoleIdAndMoudle(Long moduleId,
			Long roleId) {
		// TODO Auto-generated method stub
		return roleDao.queryCtRoleModuleByRoleIdAndMoudle(moduleId, roleId);
	}
	@Override
	public void update(CtRoleModule crm) {
		// TODO Auto-generated method stub
		roleDao.update(crm);
	}
	@Override
	public List<CtRole> loadAllCtrole() {
		// TODO Auto-generated method stub
		return this.roleDao.loadAllCtrole();
	}
	@Override
	public CtRole getRoleByRoleName() {
		// TODO Auto-generated method stub
		return this.roleDao.getRoleByRoleName();
	}
	
	@Override
	public CtRole findById(Integer id){
		
		return this.roleDao.findById(id);
	}
	
	@Override
	public void update1(CtRole cr){
		
	    this.roleDao.update1(cr);
	}
	

}
