package org.ctonline.manager.role;

import java.util.List;

import org.ctonline.po.basic.CtModule;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.user.CtRole;
import org.ctonline.po.user.CtRoleModule;
import org.ctonline.util.Page;

public interface CtRoleManager {
	public List<CtRole> loadAll(Page page);//��ҳ�ҳ�����
	//��ѯ���н�ɫ
	public List<CtRole> loadAllCtrole();
	public Integer save(CtRole role);//����
	//��ѯ����ģ��
	public List<?> getModuleList(Integer id);
	//���roleID��ѯ��ɫ
	public CtRole getRoleByRoleId(Integer roleId);
	//���½�ɫ
	public void updateRole(CtRole cr);
	//���idɾ���ɫ
	public Integer delRole(Integer id);
	//��ѯ����ģ��2
	public List<CtModule> getModuleList2();
	//��������޸Ľ�ɫȨ��
	public void save(CtRoleModule crm);
	//���roleId��moduleId��ѯCtRoleModule
	public CtRoleModule queryCtRoleModuleByRoleIdAndMoudle(Long moduleId,Long roleId );
	//�޸Ľ�ɫȨ��
	public void update(CtRoleModule crm);
	//根据角色名查询角色
	public CtRole getRoleByRoleName();
	
	public CtRole findById(Integer id);
	
	public void update1(CtRole cr);
}
