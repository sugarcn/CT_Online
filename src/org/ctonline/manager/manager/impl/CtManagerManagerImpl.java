package org.ctonline.manager.manager.impl;

import java.util.List;

import org.ctonline.dao.manager.CtManagerDao;
import org.ctonline.manager.manager.CtManagerManager;
import org.ctonline.po.manager.CtManager;
import org.ctonline.system.Logg;
import org.ctonline.util.Page;

public class CtManagerManagerImpl implements CtManagerManager {

	private CtManagerDao managerDao;
	@Override
	public List<CtManager> loadAll(Page page) {
		// TODO Auto-generated method stub
		return this.managerDao.loadAll(page);
	}
	public CtManagerDao getManagerDao() {
		return managerDao;
	}
	public void setManagerDao(CtManagerDao managerDao) {
		this.managerDao = managerDao;
	}
	@Override
	public CtManager getManagerByMManagerId(String id) {
		// TODO Auto-generated method stub
		return this.managerDao.getManagerByMManagerId(id);
	}
	@Logg(operationType="save", operationName="新增管理员")
	@Override
	public Integer save(CtManager cm) {
		// TODO Auto-generated method stub
		return this.managerDao.save(cm);
	}
	@Override
	public CtManager getManagerByMid(Integer Mid) {
		// TODO Auto-generated method stub
		return this.managerDao.getManagerByMid(Mid);
	}
	@Logg(operationType="update", operationName="修改管理员信息")
	@Override
	public void updateManager(CtManager cm) {
		// TODO Auto-generated method stub
		 this.managerDao.updateManager(cm);
	}
	@Logg(operationType="delete", operationName="删除管理员")
	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		 this.managerDao.delete(id);
	}
	@Override
	public List<CtManager> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return this.managerDao.findAll(keyword, page);
	}
	@Override
	public CtManager affirm(String uname, String password) throws Exception {
		// TODO Auto-generated method stub
		return this.managerDao.affirm(uname, password);
	}
	@Override
	public List<CtManager> getCtManagerByRoleId(Integer roleId) {
		// TODO Auto-generated method stub
		return this.managerDao.getCtManagerByRoleId(roleId);
	}
	
	@Override
	public boolean resetPwd(String id,String pwd){
		
		this.managerDao.resetPwd(id, pwd);
		return true;
	}

}
