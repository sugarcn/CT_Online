package org.ctonline.manager.manager;

import java.util.List;

import org.ctonline.po.manager.CtManager;
import org.ctonline.util.Page;

public interface CtManagerManager {
	public List<CtManager> loadAll(Page page);
	//��ݹ���Աid��ѯ����Ա
	public CtManager getManagerByMManagerId(String id);
	//�������Ա
	public Integer save(CtManager cm);
	//���Id��ѯ����Ա
	public CtManager getManagerByMid(Integer Mid);
	//�޸Ĺ���Ա
	public void updateManager(CtManager cm);
	//����ɾ��
	public void delete(Integer id);
	public List<CtManager> findAll(String keyword, Page page);
	//��½��֤
	public CtManager affirm(String uname,String password) throws Exception;
	//根据roleId查询管理员
	public List<CtManager> getCtManagerByRoleId(Integer roleId);
	
	public boolean resetPwd(String id,String pwd);
}
