package org.ctonline.manager.basic;

import java.util.List;

import org.ctonline.po.basic.CtCashCoupon;
import org.ctonline.po.basic.CtCoupon;
import org.ctonline.util.Page;

public interface CtCouponManager {
	public Long save(CtCoupon coupon);//����
	public CtCoupon findById(Long id);//���id����
	public List<CtCoupon> loadAll(Page page);//��ҳ�ҳ�����
	public int delete(int id);//ɾ��һ����¼
	public Long totalCount(String str);//ͳ������
	public void update(CtCoupon resource);//����
	public List<CtCoupon> findAll(String keyword,Page page);//�����������
	public Long getID();
	public List<CtCoupon> queryAll();
	public List<CtCashCoupon> getCashCoupon(Page page);
	public void updateCashCoupon(CtCashCoupon cashCoupon);
}
