package org.ctonline.manager.basic;
import java.util.List;
import org.ctonline.po.basic.CtCouponDetail;
import org.ctonline.util.Page;
public interface CtCouponDetailManager {
	public Long save(CtCouponDetail coupondetail);	
	public CtCouponDetail findById(Long id);//���id����
	public List<CtCouponDetail> loadAll(Page page);//��ҳ�ҳ�����
	public int delete(int id);//ɾ��һ����¼
	public Long totalCount(String str);//ͳ������
	public void update(CtCouponDetail resource);//����
	public List<CtCouponDetail> findAll(String keyword,Page page);//�����������
	public List<CtCouponDetail> findByCouponId(int id, Page page);
	public Long getID();
	public List<CtCouponDetail> queryAll();
	public String findByCouponIdAndAllIisOk(Integer couponId);
	public Long findUidCount();
	public void updateByCoupIdFaBuAll(long uid, int id);
}
