package org.ctonline.manager.basic;

import java.util.List;

import org.ctonline.po.basic.CtDepot;
import org.ctonline.util.Page;

public interface CtDepotManager {
	public Integer save(CtDepot depot);//保存
	public CtDepot findById(Integer id);//根据id查找
	public List<CtDepot> loadAll(Page page);//分页找出所有
	public int delete(Integer id);//删除一条记录
	public Long totalCount(String str);//统计条数
	public void update(CtDepot depot);//更新
	public List<CtDepot> findAll(String keyword,Page page);//根据条件检索
	public List<CtDepot> searchAll(Integer id,Page page);
	public List<CtDepot> queryAll();
}
