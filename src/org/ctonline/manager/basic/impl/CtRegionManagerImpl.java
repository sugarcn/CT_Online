package org.ctonline.manager.basic.impl;

import java.util.List;

import org.ctonline.po.basic.CtRegion;
import org.ctonline.util.Page;
import org.ctonline.dao.basic.CtRegionDAO;

public class CtRegionManagerImpl implements org.ctonline.manager.basic.CtRegionManager {
	private CtRegionDAO regionDao;
	@Override
	public Long save(CtRegion region) {
		// TODO Auto-generated method stub
		return regionDao.save(region);
	}
	public CtRegionDAO getRegionDao() {
		return regionDao;
	}

	public void setRegionDao(CtRegionDAO regionDao) {
		this.regionDao = regionDao;
	}

	@Override
	public CtRegion findById(Long id) {
		// TODO Auto-generated method stub
		return regionDao.findById(id);
	}

	@Override
	public List<CtRegion> loadAll(Page page) {
		// TODO Auto-generated method stub
		return regionDao.loadAll(page);
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		return regionDao.delete(id);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return this.regionDao.totalCount(str);
	}

	@Override
	public void update(CtRegion region) {
		// TODO Auto-generated method stub
		this.regionDao.update(region);
	}

	@Override
	public List<CtRegion> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return regionDao.findAll(keyword,page);
	}
	
	@Override
	public List<CtRegion> searchAll(Long id,Page page){
		
		return regionDao.searchAll(id, page);
	}
	
	@Override
	public List<CtRegion> queryById(Long id){
		
		return regionDao.queryById(id);
	}

}
