package org.ctonline.manager.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.CtResourceDAO;
import org.ctonline.dao.basic.CtResourceTypeDAO;
import org.ctonline.manager.basic.CtResourceTypeManager;
import org.ctonline.po.basic.CtResourceType;
import org.ctonline.util.Page;

public class CtResourceTypeManagerImpl implements CtResourceTypeManager{
	
	private CtResourceTypeDAO resourceTypeDao;
	
	public CtResourceTypeDAO getResourceTypeDao() {
		return resourceTypeDao;
	}

	public void setResourceTypeDao(CtResourceTypeDAO resourceTypeDao) {
		this.resourceTypeDao = resourceTypeDao;
	}

	@Override
	public Long save(CtResourceType resourceType) {
		// TODO Auto-generated method stub
		return resourceTypeDao.save(resourceType);
	}

	@Override
	public CtResourceType findById(Long id) {
		// TODO Auto-generated method stub
		return resourceTypeDao.findById(id);
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		return resourceTypeDao.delete(id);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return resourceTypeDao.totalCount(str);
	}

	@Override
	public void update(CtResourceType resourceType) {
		// TODO Auto-generated method stub
		resourceTypeDao.update(resourceType);
	}

	@Override
	public List<CtResourceType> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return resourceTypeDao.findAll(keyword, page);
	}

	@Override
	public Long getID() {
		// TODO Auto-generated method stub
		return resourceTypeDao.getID();
	}

	@Override
	public List<CtResourceType> queryAll(Page page) {
		// TODO Auto-generated method stub
		return resourceTypeDao.queryAll(page);
	}

}
