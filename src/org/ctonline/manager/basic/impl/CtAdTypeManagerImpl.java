package org.ctonline.manager.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.CtAdTypeDAO;
import org.ctonline.manager.basic.CtAdTypeManager;
import org.ctonline.po.basic.CtAdType;
import org.ctonline.system.Logg;
import org.ctonline.util.Page;

public class CtAdTypeManagerImpl implements CtAdTypeManager {
	private CtAdTypeDAO adTypeDao;

	public CtAdTypeDAO getAdTypeDao() {
		return adTypeDao;
	}

	public void setAdTypeDao(CtAdTypeDAO adTypeDao) {
		this.adTypeDao = adTypeDao;
	}

	@Logg(operationType="save", operationName="新增广告类别")
	@Override
	public Integer save(CtAdType adType) {
		// TODO Auto-generated method stub
		return adTypeDao.save(adType);
	}

	@Override
	public CtAdType findById(int id) {
		// TODO Auto-generated method stub
		return adTypeDao.findById(id);
	}

	@Override
	public List<CtAdType> loadAll(Page page) {
		// TODO Auto-generated method stub
		return adTypeDao.loadAll(page);
	}

	@Logg(operationType="delete", operationName="删除广告类别")
	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return adTypeDao.delete(id);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return adTypeDao.totalCount(str);
	}

	@Logg(operationType="update", operationName="更新广告类别")
	@Override
	public void update(CtAdType adType) {
		// TODO Auto-generated method stub
		this.adTypeDao.update(adType);
	}

	@Override
	public List<CtAdType> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return adTypeDao.findAll(keyword, page);
	}

	@Override
	public List<CtAdType> findAllType() {
		return adTypeDao.findAllType();
	}

}
