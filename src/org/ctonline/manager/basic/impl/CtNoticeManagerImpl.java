package org.ctonline.manager.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.CtNoticeDAO;
import org.ctonline.manager.basic.CtNoticeManager;
import org.ctonline.po.basic.CtFilter;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.basic.CtNoticeType;
import org.ctonline.system.Logg;
import org.ctonline.util.Page;

public class CtNoticeManagerImpl implements CtNoticeManager{
	private CtNoticeDAO noticeDAO;

	public CtNoticeDAO getNoticeDAO() {
		return noticeDAO;
	}

	public void setNoticeDAO(CtNoticeDAO noticeDAO) {
		this.noticeDAO = noticeDAO;
	}

	@Override
	public List<CtNotice> loadAll(Page page) {
		// TODO Auto-generated method stub
		return noticeDAO.loadAll(page);
	}

	@Override
	public Integer save(CtNotice notice) {
		// TODO Auto-generated method stub
		return noticeDAO.save(notice);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return noticeDAO.totalCount(str);
	}

	@Override
	public CtNotice findById(int id) {
		// TODO Auto-generated method stub
		return noticeDAO.findById(id);
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return noticeDAO.delete(id);
	}

	@Override
	public void update(CtNotice notice) {
		// TODO Auto-generated method stub
		this.noticeDAO.update(notice);
	}

	@Override
	public List<CtNotice> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return noticeDAO.findAll(keyword, page);
	}

	@Override
	public List<CtNoticeType> findTypeByPage(Page page) {
		return noticeDAO.findTypeByPage(page);
	}

	@Override
	public CtNoticeType findTypeByTypeId(int id) {
		return noticeDAO.findTypeByTypeId(id);
	}

	@Logg(operationType="save", operationName="新增公告类别")
	@Override
	public void saveNoticeType(CtNoticeType noticeType) {
		noticeDAO.saveNoticeType(noticeType);
	}

	@Logg(operationType="update", operationName="更新公告类别")
	@Override
	public void updateNoticeType(CtNoticeType noticeType) {
		noticeDAO.updateNoticeType(noticeType);
	}

	@Logg(operationType="delete", operationName="删除公告类别")
	@Override
	public void deleteNoticeTypeById(int m) {
		noticeDAO.deleteNoticeTypeById(m);
	}

	@Override
	public List<CtFilter> findFilterListByPage(Page page) {
		return noticeDAO.findFilterListByPage(page);
	}

	@Override
	public void saveFirlt(CtFilter filter) {
		noticeDAO.saveFirlt(filter);
	}

	@Override
	public CtFilter findFirltById(int id) {
		return noticeDAO.findFirltById(id);
	}

	@Override
	public void deleteFilterByFilter(CtFilter filter) {
		noticeDAO.deleteFilterByFilter(filter);
	}

}
