package org.ctonline.manager.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.CtResourceDAO;
import org.ctonline.manager.basic.CtResourceManager;
import org.ctonline.po.basic.CtResource;
import org.ctonline.po.basic.CtResourceType;
import org.ctonline.po.views.ViewGoodsResource;
import org.ctonline.util.Page;

public class CtResourceManagerImpl implements CtResourceManager {

	private CtResourceDAO resourceDao;
	
	public CtResourceDAO getResourceDao() {
		return resourceDao;
	}

	public void setResourceDao(CtResourceDAO resourceDao) {
		this.resourceDao = resourceDao;
	}

	@Override
	public Long save(CtResource resource) {
		// TODO Auto-generated method stub
		return resourceDao.save(resource);
	}

	@Override
	public CtResource findById(Long id) {
		// TODO Auto-generated method stub
		return resourceDao.findById(id);
	}

	@Override
	public List<CtResource> loadAll(Page page) {
		// TODO Auto-generated method stub
		return resourceDao.loadAll(page);
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		return resourceDao.delete(id);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return resourceDao.totalCount(str);
	}

	@Override
	public void update(CtResource resource) {
		// TODO Auto-generated method stub
		resourceDao.update(resource);
	}

	@Override
	public List<CtResource> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return resourceDao.findAll(keyword, page);
	}

	@Override
	public Long getID() {
		// TODO Auto-generated method stub
		return resourceDao.getID();
	}

	@Override
	public List<CtResource> queryAll(Page page) {
		// TODO Auto-generated method stub
		return resourceDao.queryAll(page);
	}

	@Override
	public List<CtResourceType> queryType() {
		// TODO Auto-generated method stub
		return resourceDao.queryType();
	}

	@Override
	public List<ViewGoodsResource> queryByGoodsID(Long gid) {
		// TODO Auto-generated method stub
		return resourceDao.queryByGoodsID(gid);
	}

	@Override
	public Integer findSortByRTId(Long rTId) {
		return resourceDao.findSortByRTId(rTId);
	}

}
