package org.ctonline.manager.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.CtLoginLogDAO;
import org.ctonline.manager.basic.CtLoginLogManager;
import org.ctonline.po.basic.CtLoginLog;
import org.ctonline.system.Logg;
import org.ctonline.util.Page;

public class CtLoginLogImpl
  implements CtLoginLogManager
{
  private CtLoginLogDAO loginLogDao;
  
  @Logg(operationType="save", operationName="保存登录日志")
  public void save(CtLoginLog log)
  {
    this.loginLogDao.save(log);
  }
  
  public CtLoginLogDAO getLoginLogDao()
  {
    return this.loginLogDao;
  }
  
  public void setLoginLogDao(CtLoginLogDAO loginLogDao)
  {
    this.loginLogDao = loginLogDao;
  }

@Override
public List<CtLoginLog> getLogByPage(Page page) {
	return loginLogDao.getLogByPage(page);
}
}
