package org.ctonline.manager.basic.impl;
import java.util.List;

import org.ctonline.dao.basic.CtCouponDetailDAO;
import org.ctonline.manager.basic.CtCouponDetailManager;
import org.ctonline.po.basic.CtCouponDetail;
import org.ctonline.system.Logg;
import org.ctonline.util.Page;

public class CtCouponDetailMangerImpl implements CtCouponDetailManager{
	CtCouponDetailDAO coupondetailDao;
	
	

	public CtCouponDetailDAO getCoupondetailDao() {
		return coupondetailDao;
	}

	public void setCoupondetailDao(CtCouponDetailDAO coupondetailDao) {
		this.coupondetailDao = coupondetailDao;
	}

	public CtCouponDetailDAO getCouponDetailDao() {
		return coupondetailDao;
	}

	public void setCouponDetailDao(CtCouponDetailDAO coupondetailDao) {
		this.coupondetailDao = coupondetailDao;
	}

	public Long save(CtCouponDetail coupondetail)
	{
		coupondetailDao.save(coupondetail);
		return 1l;
	}
	
	public CtCouponDetail findById(Long id)
	{
		return coupondetailDao.findById(id);
	}
	//public List<CtCoupon> loadAll(Page page);//��ҳ�ҳ�����
	public int delete(Long id)
	{
		return coupondetailDao.delete(id);
	}

	@Logg(operationType="update", operationName="更新优惠券明细")
	//public Long totalCount(String str);//ͳ������
	public void update(CtCouponDetail coupondetail)
	{
		coupondetailDao.update(coupondetail);
	}
	public List<CtCouponDetail> findAll(String keyword,Page page)
	{
		return coupondetailDao.findAll(keyword, page);
	}
	public List<CtCouponDetail> findByCouponId(int id, Page page)
	{
		return coupondetailDao.findByCouponId(id, page);
	}
	//public Long getID();
	public List<CtCouponDetail> queryAll()
	{
		return coupondetailDao.queryAll();
	}

	@Override
	public List<CtCouponDetail> loadAll(Page page) {
		// TODO Auto-generated method stub
		return coupondetailDao.loadAll(page);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long getID() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String findByCouponIdAndAllIisOk(Integer couponId) {
		return coupondetailDao.findByCouponIdAndAllIisOk(couponId);
	}

	@Override
	public Long findUidCount() {
		return coupondetailDao.findUidCount();
	}

	@Logg(operationType="delete", operationName="更新优惠券明细")
	@Override
	public void updateByCoupIdFaBuAll(long uid,int id) {
		coupondetailDao.updateByCoupIdFaBuAll(uid, id);
	}

}
