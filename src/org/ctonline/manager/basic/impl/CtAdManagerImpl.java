package org.ctonline.manager.basic.impl;


import java.util.List;

import org.ctonline.dao.basic.CtAdDAO;
import org.ctonline.manager.basic.CtAdManager;
import org.ctonline.po.basic.CtAd;
import org.ctonline.util.Page;

public class CtAdManagerImpl implements CtAdManager{

	@Override
	public List<CtAd> loadOne(int id) {
		// TODO Auto-generated method stub
		return this.adDAO.loadOne(id);
	}

	private CtAdDAO adDAO;

	public CtAdDAO getAdDAO() {
		return adDAO;
	}

	public void setAdDAO(CtAdDAO adDAO) {
		this.adDAO = adDAO;
	}

	@Override
	public CtAd findById(int id) {
		// TODO Auto-generated method stub
		return this.adDAO.findById(id);
	}

	@Override
	public int save(CtAd ad) {
		// TODO Auto-generated method stub
		return this.adDAO.save(ad);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return this.adDAO.totalCount(str);
	}


	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return this.adDAO.delete(id);
	}

	@Override
	public CtAd getAdByAdId(int Uid) {
		// TODO Auto-generated method stub
		return this.adDAO.getAdByAdId(Uid);
	}

	@Override
	public List<CtAd> loadAll(Page page) {
		// TODO Auto-generated method stub
		return this.adDAO.loadAll(page);
	}

	@Override
	public void update(CtAd ad) {
		// TODO Auto-generated method stub
		this.adDAO.update(ad);
	}

	@Override
	public List<CtAd> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return this.adDAO.findAll(keyword, page);
	}	
}
