package org.ctonline.manager.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.CtLoginOperationDAO;
import org.ctonline.manager.basic.CtLoginOperationManager;
import org.ctonline.po.basic.CtLoginOperation;
import org.ctonline.util.Page;

public class CtLoginOperationImpl
  implements CtLoginOperationManager
{
  private CtLoginOperationDAO operation;
  
  public void addLog(CtLoginOperation loginOperation)
  {
    this.operation.addLog(loginOperation);
  }
  
  public CtLoginOperationDAO getOperation()
  {
    return this.operation;
  }
  
  public void setOperation(CtLoginOperationDAO operation)
  {
    this.operation = operation;
  }

@Override
public Long getLoginUserId() {
	return operation.operation();
}

@Override
public Boolean findDescEs(String opContent) {
	CtLoginOperation oper = operation.findDescEs(opContent);
	if(oper != null){
		return false;
	}
	return true;
}

@Override
public List<CtLoginOperation> findAllLoginByPageAndKey(Page page, String key,
		String stime, String etime, String type, String userName) {
	return operation.findAllLoginByPageAndKey(page, key, stime, etime, type, userName);
}

}
