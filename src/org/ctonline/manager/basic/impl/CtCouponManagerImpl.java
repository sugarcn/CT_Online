package org.ctonline.manager.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.CtCouponDAO;
import org.ctonline.dao.basic.CtCouponDetailDAO;
import org.ctonline.manager.basic.CtCouponManager;
import org.ctonline.po.basic.CtCashCoupon;
import org.ctonline.po.basic.CtCoupon;
import org.ctonline.po.basic.CtCouponDetail;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;

public class CtCouponManagerImpl implements CtCouponManager{
	CtCouponDAO couponDao;

	CtCouponDetailDAO coupondetailDao;
	

	public CtCouponDetailDAO getCoupondetailDao() {
		return coupondetailDao;
	}

	public void setCoupondetailDao(CtCouponDetailDAO coupondetailDao) {
		this.coupondetailDao = coupondetailDao;
	}

	public CtCouponDAO getCouponDao() {
		return couponDao;
	}

	public void setCouponDao(CtCouponDAO couponDao) {
		this.couponDao = couponDao;
	}

	public Long save(CtCoupon coupon)
	{
		CtCouponDetail coupondetail=new CtCouponDetail();
		//CtCouponDetailDAO coupondetailDao;
		couponDao.save(coupon);
		for(int i=0;i<coupon.getReleaseNum();i++)
		{
			coupondetail=new CtCouponDetail();
			coupondetail.setCtCoupon(coupon);
			coupondetailDao.save(coupondetail);
		}
		//coupondetail.setCtCoupon(coupon);//.setCouponNumber(String.valueOf(coupon.getCouponId()));
		
		//int count=coupon.getAmount().intValue();
		
		
		return 1l;
	}
	
	public CtCoupon findById(Long id)
	{
		return couponDao.findById(id);
	}
	//public List<CtCoupon> loadAll(Page page);//��ҳ�ҳ�����
	/*public int delete(Long id)
	{
		return couponDao.delete(id);
	}*/ 

	//public Long totalCount(String str);//ͳ������
	public void update(CtCoupon coupon)
	{
		couponDao.update(coupon);
	}
	public List<CtCoupon> findAll(String keyword,Page page)
	{
		return couponDao.findAll(keyword, page);
	}
	//public Long getID();
	public List<CtCoupon> queryAll()
	{
		return couponDao.queryAll();
	}

	@Override
	public List<CtCoupon> loadAll(Page page) {
		// TODO Auto-generated method stub
		return this.couponDao.loadAll(page);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long getID() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int delete(int id) {
		return couponDao.delete(id);
		// TODO Auto-generated method stub
		//return 0;
	}

	@Override
	public List<CtCashCoupon> getCashCoupon(Page page) {
		return couponDao.getCashCoupon(page);
	}

	@Override
	public void updateCashCoupon(CtCashCoupon cashCoupon) {
		couponDao.updateCashCoupon(cashCoupon);
	}

	//@Override
	//public int delete(long id) {
	//	// TODO Auto-generated method stub
	//	return couponDao.delete(id);
	//}

}
