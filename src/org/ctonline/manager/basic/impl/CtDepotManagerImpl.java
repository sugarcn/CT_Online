package org.ctonline.manager.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.CtDepotDAO;
import org.ctonline.manager.basic.CtDepotManager;
import org.ctonline.po.basic.CtDepot;
import org.ctonline.util.Page;

public class CtDepotManagerImpl implements CtDepotManager{
	
	private CtDepotDAO depotDao;

	public CtDepotDAO getDepotDao() {
		return depotDao;
	}

	public void setDepotDao(CtDepotDAO depotDao) {
		this.depotDao = depotDao;
	}
	
	@Override
	public Integer save(CtDepot depot) {
		// TODO Auto-generated method stub
		return depotDao.save(depot);
	}

	@Override
	public CtDepot findById(Integer id) {
		// TODO Auto-generated method stub
		return depotDao.findById(id);
	}

	@Override
	public List<CtDepot> loadAll(Page page) {
		// TODO Auto-generated method stub
		return depotDao.loadAll(page);
	}

	@Override
	public int delete(Integer id) {
		// TODO Auto-generated method stub
		return depotDao.delete(id);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return depotDao.totalCount(str);
	}

	@Override
	public void update(CtDepot depot) {
		// TODO Auto-generated method stub
		depotDao.update(depot);
	}

	@Override
	public List<CtDepot> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return depotDao.findAll(keyword, page);
	}
	
	@Override
	public List<CtDepot> searchAll(Integer id,Page page){
		
		return depotDao.searchAll(id,page);
	}

	@Override
	public List<CtDepot> queryAll() {
		// TODO Auto-generated method stub
		return depotDao.queryAll();
	}

}
