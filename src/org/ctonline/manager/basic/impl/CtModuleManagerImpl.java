package org.ctonline.manager.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.CtModuleDAO;
import org.ctonline.manager.basic.CtModuleManager;
import org.ctonline.po.basic.CtModule;
import org.ctonline.po.views.ViewModuleLeft;
import org.ctonline.po.views.ViewModuleTop;
import org.ctonline.util.Page;

public class CtModuleManagerImpl implements CtModuleManager{

	private CtModuleDAO moduleDao;

	@Override
	public List<CtModule> searchAll(Integer pid, Page page) {
		// TODO Auto-generated method stub
		return moduleDao.searchAll(pid, page);
	}

	public CtModuleDAO getModuleDao() {
		return moduleDao;
	}

	public void setModuleDao(CtModuleDAO moduleDao) {
		this.moduleDao = moduleDao;
	}

	@Override
	public List<CtModule> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return moduleDao.findAll(keyword,page);
	}

	@Override
	public List<CtModule> loadAll(Page page) {
		// TODO Auto-generated method stub
		return moduleDao.loadAll(page);
	}

	@Override
	public void save(CtModule module) {
		// TODO Auto-generated method stub
		moduleDao.save(module);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return this.moduleDao.totalCount(str);
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return moduleDao.delete(id);
	}

	@Override
	public CtModule findById(Integer id) {
		// TODO Auto-generated method stub
		return moduleDao.findById(id);
	}

	@Override
	public void update(CtModule module) {
		// TODO Auto-generated method stub
		this.moduleDao.update(module);
	}

	@Override
	public List<ViewModuleTop> getTopModule(Integer roleId) {
		// TODO Auto-generated method stub
		return this.moduleDao.getTopModule(roleId);
	}

	@Override
	public List<ViewModuleLeft> getLeftModule(Integer roleId, Integer parentId) {
		// TODO Auto-generated method stub
		return this.moduleDao.getLeftModule(roleId, parentId);
	}

}
