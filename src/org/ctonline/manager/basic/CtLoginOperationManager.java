package org.ctonline.manager.basic;

import java.util.List;

import org.ctonline.po.basic.CtLoginOperation;
import org.ctonline.util.Page;

public abstract interface CtLoginOperationManager
{
  public abstract void addLog(CtLoginOperation paramCtLoginOperation);

public abstract Long getLoginUserId();

public abstract Boolean findDescEs(String opContent);

public abstract List<CtLoginOperation> findAllLoginByPageAndKey(Page page,
		String key, String stime, String etime, String type, String userName);
}
