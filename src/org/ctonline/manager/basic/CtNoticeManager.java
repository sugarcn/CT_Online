package org.ctonline.manager.basic;

import java.util.List;

import org.ctonline.po.basic.CtFilter;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.basic.CtNoticeType;
import org.ctonline.util.Page;

public interface CtNoticeManager {
	public List<CtNotice> loadAll(Page page);
	public Integer save(CtNotice notice);
	public Long totalCount(String str);
	public CtNotice findById(int id);
	public int delete(int id);
	public void update(CtNotice notice);
	public List<CtNotice> findAll(String keyword,Page page);
	public List<CtNoticeType> findTypeByPage(Page page);
	public CtNoticeType findTypeByTypeId(int id);
	public void saveNoticeType(CtNoticeType noticeType);
	public void updateNoticeType(CtNoticeType noticeType);
	public void deleteNoticeTypeById(int m);
	public List<CtFilter> findFilterListByPage(Page page);
	public void saveFirlt(CtFilter filter);
	public CtFilter findFirltById(int id);
	public void deleteFilterByFilter(CtFilter filter);
}
