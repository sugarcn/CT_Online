package org.ctonline.manager.basic;

import java.util.List;

import org.ctonline.po.basic.CtResourceType;
import org.ctonline.util.Page;

public interface CtResourceTypeManager {
	public Long save(CtResourceType resourceType);//保存
	public CtResourceType findById(Long id);//根据id查找
	public int delete(Long id);//删除一条记录
	public Long totalCount(String str);//统计条数
	public void update(CtResourceType resourceType);//更新
	public List<CtResourceType> findAll(String keyword,Page page);//根据条件检索
	public Long getID();
	public List<CtResourceType> queryAll(Page page);//查全表
}
