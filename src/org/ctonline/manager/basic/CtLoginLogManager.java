package org.ctonline.manager.basic;

import java.util.List;

import org.ctonline.po.basic.CtLoginLog;
import org.ctonline.util.Page;

public abstract interface CtLoginLogManager
{
  public abstract void save(CtLoginLog paramCtLoginLog);

public abstract List<CtLoginLog> getLogByPage(Page page);
}
