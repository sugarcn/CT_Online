package org.ctonline.manager.basic;


import java.util.List;

import org.ctonline.po.basic.CtAd;
import org.ctonline.util.Page;

public interface CtAdManager {
	public CtAd findById(int id);
	public List<CtAd> loadAll(Page page);
	public int save(CtAd ad);
	public int delete(int m);
	public Long totalCount(String str);
	public void update(CtAd ad);
	public List<CtAd> loadOne(int id);
	public CtAd getAdByAdId(int Uid);
	public List<CtAd> findAll(String keyword,Page page);
}
