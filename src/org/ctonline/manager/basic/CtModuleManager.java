package org.ctonline.manager.basic;

import java.util.List;

import org.ctonline.po.basic.CtModule;
import org.ctonline.po.views.ViewModuleLeft;
import org.ctonline.po.views.ViewModuleTop;
import org.ctonline.util.Page;

public interface CtModuleManager {

	public List<CtModule> searchAll(Integer pid, Page page);
	public List<CtModule> findAll(String keyword,Page page);
	public List<CtModule> loadAll(Page page);
	public void save(CtModule module);
	public Long totalCount(String str);
	public int delete(int id);
	public CtModule findById(Integer id);
	public void update(CtModule module);
	//取出1层模块
	public List<ViewModuleTop> getTopModule(Integer roleId);
	//生成左侧列表
	public List<ViewModuleLeft> getLeftModule(Integer roleId,Integer parentId);

}
