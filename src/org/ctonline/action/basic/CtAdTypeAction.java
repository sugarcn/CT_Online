package org.ctonline.action.basic;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.dto.basic.CtAdTypeDTO;
import org.ctonline.manager.basic.CtAdTypeManager;
import org.ctonline.po.basic.CtAdType;
import org.ctonline.po.basic.CtUserRank;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class CtAdTypeAction extends ActionSupport implements RequestAware,
		ModelDriven<Object> {
	private List<CtAdType> adTypes;
	private CtAdTypeManager adTypeManager;
	private Map<String, Object> request;
	private Page page;
	private CtAdTypeDTO adTypeDTO = new CtAdTypeDTO();
	private CtAdType adType;
	private Integer parentId;

	public List<CtAdType> getAdTypes() {
		return adTypes;
	}

	public void setAdTypes(List<CtAdType> adTypes) {
		this.adTypes = adTypes;
	}

	public CtAdTypeManager getAdTypeManager() {
		return adTypeManager;
	}

	public void setAdTypeManager(CtAdTypeManager adTypeManager) {
		this.adTypeManager = adTypeManager;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtAdType getAdType() {
		return adType;
	}

	public void setAdType(CtAdType adType) {
		this.adType = adType;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.adTypeDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public String list() {
		try {
			page = new Page();
			if (adTypeDTO.getPage() == 0) {
				adTypeDTO.setPage(1);
			}
			page.setCurrentPage(adTypeDTO.getPage());
			this.adTypes = adTypeManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	public String add() {
		try {
			CtAdType a = new CtAdType();
			a.setAdTypeId(adTypeDTO.getAdTypeId());
			a.setAdTypeName(adTypeDTO.getAdTypeName());
			Integer aa = this.adTypeManager.save(a);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	public String find() {
		try {
			Integer id = adTypeDTO.getId();
			adType = new CtAdType();
			adType.setAdTypeId(id);
			this.adType = adTypeManager.findById(id);
			adTypeDTO.setAdTypeId(adType.getAdTypeId());
			adTypeDTO.setAdTypeName(adType.getAdTypeName());
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	public String update() {
		try {
			CtAdType m = new CtAdType();
			Integer mid = adTypeDTO.getAdTypeId();
			m = this.adTypeManager.findById(adTypeDTO.getId());
			m.setAdTypeName(adTypeDTO.getAdTypeName());
			adTypeManager.update(m);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	public String del() {
		try {
			for (int m : adTypeDTO.getMid()) {
				this.adTypeManager.delete(m);
			}
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	public String search() {
		try {
			Page page = new Page();
			if (adTypeDTO.getPage() == 0) {
				adTypeDTO.setPage(1);
			}
			page.setCurrentPage(adTypeDTO.getPage());
			if (adTypeDTO.getKeyword() != null
					&& !adTypeDTO.getKeyword().equals("请输入关键字")) {
				this.adTypes = adTypeManager.findAll(adTypeDTO.getKeyword(),
						page);
			} else {
				this.adTypes = adTypeManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}
}
