package org.ctonline.action.basic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.action.BaseAction;
import org.ctonline.common.CommonFunc;
import org.ctonline.dto.basic.CtResourceDTO;
import org.ctonline.manager.basic.CtResourceManager;
import org.ctonline.manager.basic.CtResourceTypeManager;
import org.ctonline.po.basic.CtResource;
import org.ctonline.po.basic.CtResourceType;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ModelDriven;

public class CtResourceAction extends BaseAction implements RequestAware,
		ModelDriven<Object> {

	private List<CtResource> resourceList;
	private List<?> queryList;

	public List<?> getQueryList() {
		return queryList;
	}

	public void setQueryList(List<?> queryList) {
		this.queryList = queryList;
	}

	// 资料分类
	private List<CtResourceType> typeList;

	public List<CtResourceType> getTypeList() {
		return typeList;
	}

	public void setTypeList(List<CtResourceType> typeList) {
		this.typeList = typeList;
	}

	private CtResourceManager resourceManager;
	private CtResourceTypeManager resourceTypeManager;
	private Map<String, Object> request;
	private Page page;
	private CtResource resource;
	private CtResourceDTO resourceDTO = new CtResourceDTO();

	public CtResourceDTO getResourceDTO() {
		return resourceDTO;
	}

	public void setResourceDTO(CtResourceDTO resourceDTO) {
		this.resourceDTO = resourceDTO;
	}

	public List<CtResource> getResourceList() {
		return resourceList;
	}

	public void setResourceList(List<CtResource> resourceList) {
		this.resourceList = resourceList;
	}

	public CtResourceManager getResourceManager() {
		return resourceManager;
	}

	public void setResourceManager(CtResourceManager resourceManager) {
		this.resourceManager = resourceManager;
	}

	public CtResourceTypeManager getResourceTypeManager() {
		return resourceTypeManager;
	}

	public void setResourceTypeManager(CtResourceTypeManager resourceTypeManager) {
		this.resourceTypeManager = resourceTypeManager;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtResource getResource() {
		return resource;
	}

	public void setResource(CtResource resource) {
		this.resource = resource;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.resourceDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	// 跳转资料分类
	public String toRestype() {
		try {
			page = new Page();
			if (resourceDTO.getPage() == 0) {
				resourceDTO.setPage(1);
			}
			page.setCurrentPage(resourceDTO.getPage());
			this.typeList = resourceTypeManager.queryAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
	//查询资料分类
	public String findRestype(){
		Long id = resourceDTO.getId();
		CtResourceType restype = new CtResourceType();
		restype = resourceTypeManager.findById(id);
		resourceDTO.setRTName(restype.getRTName());
		return SUCCESS;
	}

	//查询资源
	public String findRes(){
		Long id = resourceDTO.getId();
		CtResource resource = new CtResource();
		resource = resourceManager.findById(id);
		resourceDTO.setRName(resource.getRName());
		resourceDTO.setRUrl(resource.getRUrl());
		resourceDTO.setRTId(resource.getRTId());
		resourceDTO.setResourceTypesList(resourceTypeManager.queryAll(null));
		resourceDTO.setRId(resource.getRId());
		resourceDTO.setRsort(resource.getRSort());
		return SUCCESS;
	}
	
	// 添加资料分类
	public String addrestype() {
		try {
			String rtname = resourceDTO.getRTName();
			CtResourceType restype = new CtResourceType();
			restype.setRTName(rtname);
			resourceTypeManager.save(restype);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	//更新资料分类
	public String updateType(){
		Long id = resourceDTO.getId();
		CtResourceType restype = new CtResourceType();
		restype.setRTId(id);
		restype.setRTName(resourceDTO.getRTName());
		resourceTypeManager.update(restype);
		return SUCCESS;
	}
	
	// 删除资料分类
	public String delrestype() {
		try {
			for (Long m : resourceDTO.getMid()) {
				resourceTypeManager.delete(m);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String list() {
		try {
			page = new Page();
			if (resourceDTO.getPage() == 0) {
				resourceDTO.setPage(1);
			}
			page.setCurrentPage(resourceDTO.getPage());
			this.resourceList = resourceManager.queryAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			this.typeList = resourceTypeManager.queryAll(null);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	//删除资源
	public String delResource(){
		try {
			for (Long m : resourceDTO.getMid()) {
				resourceManager.delete(m);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
	private List<File> file; // 上传文件集合
	private List<String> fileFileName; // 上传文件名集合
	private List<String> fileContentType; // 上传文件内容集合

	public List<File> getFile() {
		return file;
	}

	public void setFile(List<File> file) {
		this.file = file;
	}

	public List<String> getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(List<String> fileFileName) {
		this.fileFileName = fileFileName;
	}

	public List<String> getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(List<String> fileContentType) {
		this.fileContentType = fileContentType;
	}
	
	//更新资源
	public String updateResoutce(){
		Long id = resourceDTO.getRId();
		String name = resourceDTO.getRName();
		Long rtid = resourceDTO.getRTId();
		String url = resourceDTO.getRUrl();
		Integer rsort = resourceDTO.getRsort();
		resource = resourceManager.findById(id);
		resource.setRName(name);
		resource.setRTId(rtid);
		resource.setRUrl(url);
		resource.setRSort(rsort);
		resourceManager.update(resource);
		return SUCCESS;
	}

	public String upload() {
		Long RTId = resourceDTO.getRTId();
		Integer sotrIndex = resourceDTO.getRsort();
		String RName = resourceDTO.getRName();
		String RUrl = resourceDTO.getRUrl();
		resource = new CtResource();
		resource.setRTId(RTId);
		resource.setRName(RName);
		resource.setRUrl(RUrl);
		resource.setRSort(sotrIndex);
		resourceManager.save(resource);
//		for (int i = 0; i < file.size(); i++) {
//			try {
//				uploadFile(i, RTId);
//			} catch (FileNotFoundException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
		return SUCCESS;
	}

	// 执行上传功能
	private void uploadFile(int i, Long RTId) throws FileNotFoundException,
			IOException {
		// 获取年月日作为文件路径
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		int month = now.get(Calendar.MONTH) + 1;
		int day = now.get(Calendar.DAY_OF_MONTH);
		// 获取时间戳
		Date date = new Date();
		long time = date.getTime();
		// 上传文件存放路径
		String UPLOADDIR = "/upload/" + year + "/" + month + "/" + day;
		try {
			InputStream in = new FileInputStream(file.get(i));
			String dir = ServletActionContext.getRequest().getRealPath(
					UPLOADDIR);
			// 获取文件名
			String fileName = this.getFileFileName().get(i);
			// 获取后缀名
			String[] suffixs = fileName.split("\\.");
			String suffix = "." + suffixs[suffixs.length - 1];
			// 时间戳文件名
			String timename = Long.toString(time) + suffix;
			// 存库url
			String url = UPLOADDIR + "/" + timename;
			File fileLocation = new File(dir);
			if (!fileLocation.exists()) {
				boolean isCreated = fileLocation.mkdirs();
				if (!isCreated) {
					System.out.println("上传目录创建失败！");
					// 上传目录创建失败处理
					return;
				}
			}
			File uploadFile = new File(dir, timename);
			OutputStream out = new FileOutputStream(uploadFile);
			byte[] buffer = new byte[1024 * 1024];
			int length;
			while ((length = in.read(buffer)) > 0) {
				out.write(buffer, 0, length);
			}
			in.close();
			out.close();
			// 存库
			CtResource re = new CtResource();
			re.setRName(fileName);
			re.setRUrl(url);
			re.setRTId(RTId);
			Long aa = this.resourceManager.save(re);
		} catch (FileNotFoundException ex) {
			System.out.println("上传失败！");
			ex.printStackTrace();
		} catch (IOException ex) {
			System.out.println("上传失败！");
			ex.printStackTrace();
		}
	}

	public String search() {
		try {
			page = new Page();
			if (resourceDTO.getPage() == 0) {
				resourceDTO.setPage(1);
			}
			page.setCurrentPage(resourceDTO.getPage());
			if (resourceDTO.getKeyword() != null
					&& !resourceDTO.getKeyword().equals("请输入你要查的关键词")) {
				this.resourceList = resourceManager.findAll(
						resourceDTO.getKeyword(), page);
			} else {
				this.resourceList = resourceManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public boolean delFile(String Path) {
		File dfile = new File(Path);
		// 判斷文件是否存在
		if (!dfile.exists()) {
			System.out.println("需要删除的文件不存在");
			return false;
		} else {
			// 判斷是否為文件
			if (dfile.isFile()) {
				dfile.delete();
				System.out.println(Path + "文件已删除");
				return true;
			} else {
				System.out.println("文件检查出错");
				return false;
			}
		}
	}

}
