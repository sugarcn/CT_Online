package org.ctonline.action.basic;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.dto.basic.CtUserRankDTO;
import org.ctonline.manager.basic.CtUserRankManager;
import org.ctonline.po.basic.CtUserRank;
import org.ctonline.util.Page;
import org.omg.CORBA.UserException;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class CtUserRankAction extends ActionSupport implements RequestAware,
		ModelDriven<Object> {

	private List<CtUserRank> userRanks;
	private CtUserRankManager userRankManager;
	private Map<String, Object> request;
	private Page page;
	private CtUserRankDTO userRankDto = new CtUserRankDTO();
	private CtUserRank userRank;
	private String msg;

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.userRankDto;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public List<CtUserRank> getUserRanks() {
		return userRanks;
	}

	public void setUserRanks(List<CtUserRank> userRanks) {
		this.userRanks = userRanks;
	}

	public CtUserRankManager getUserRankManager() {
		return userRankManager;
	}

	public void setUserRankManager(CtUserRankManager userRankManager) {
		this.userRankManager = userRankManager;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtUserRankDTO getUserRankDto() {
		return userRankDto;
	}

	public void setUserRankDto(CtUserRankDTO userRankDto) {
		this.userRankDto = userRankDto;
	}

	public CtUserRank getUserRank() {
		return userRank;
	}

	public void setUserRank(CtUserRank userRank) {
		this.userRank = userRank;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String list() {
		try {
			page = new Page();
			if (userRankDto.getPage() == 0) {
				userRankDto.setPage(1);
			}
			page.setCurrentPage(userRankDto.getPage());
			this.userRanks = userRankManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			userRankDto.setKeyword("请输入你要查询的关键词");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 锟斤拷锟斤拷锟饺硷拷锟斤拷锟�
	public String add() {
		try {
			CtUserRank userRank = new CtUserRank();
			userRank.setRId(userRankDto.getId());
			userRank.setRName(userRankDto.getRName());
			userRank.setMinPoints(userRankDto.getMinPoints());
			userRank.setMaxPoints(userRankDto.getMaxPoints());

			Long aa = this.userRankManager.save(userRank);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 锟斤拷锟斤拷某一锟斤拷锟饺硷拷锟斤拷锟�

	public String find() {
		try {
			Long id = userRankDto.getId();
			userRank = new CtUserRank();
			userRank.setRId(id);
			this.userRank = userRankManager.findById(id);
			userRankDto.setRId(userRank.getRId());
			userRankDto.setRName(userRank.getRName());
			userRankDto.setMinPoints(userRank.getMinPoints());
			userRankDto.setMaxPoints(userRank.getMaxPoints());

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 锟睫改等硷拷锟斤拷锟�
	public String update() {
		try {

			CtUserRank m = new CtUserRank();
			m = this.userRankManager.findById(userRankDto.getId());
			m.setRName(userRankDto.getRName());
			m.setMinPoints(userRankDto.getMinPoints());
			m.setMaxPoints(userRankDto.getMaxPoints());
			this.userRankManager.update(m);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 删锟斤拷某锟斤拷锟饺硷拷锟斤拷锟�
	public String del() {
		try {
			for (Long m : userRankDto.getMid()) {
				this.userRankManager.delete(m);

			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 锟斤拷询
	public String search() {
		try {
			Page page = new Page();
			if (userRankDto.getPage() == 0) {
				userRankDto.setPage(1);
			}
			page.setCurrentPage(userRankDto.getPage());
			if (userRankDto.getKeyword() != null
					&& !userRankDto.getKeyword().equals("请输入你要查询的关键词")) {
				this.userRanks = userRankManager.findAll(
						userRankDto.getKeyword(), page);
			} else {
				this.userRanks = userRankManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String make() {
		try {
			this.userRanks = this.userRankManager.getRank();
			String name = userRanks.get(0).getRName();
			String[] str = name.split("V");
			String RName = "";
			if(str.length == 2){
				try {
					int i = Integer.parseInt(str[1]) + 1;
					RName = "V" + i;
				} catch (Exception e) {
					e.printStackTrace();
					RName = "V0";
				}
			}
			Integer max = userRanks.get(0).getMaxPoints();
			int point = max + 1;
			Integer minPoints = point;
			userRankDto.setRName(RName);
			userRankDto.setMaxPoints(null);
			userRankDto.setMinPoints(minPoints);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}

}
