package org.ctonline.action.basic;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.crypto.Data;

import net.sf.json.JSONArray;

import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.action.BaseAction;
import org.ctonline.common.CommonFunc;
import org.ctonline.dto.basic.CtCouponDTO;
import org.ctonline.manager.basic.CtCouponDetailManager;
import org.ctonline.manager.basic.CtCouponManager;
import org.ctonline.manager.user.CtUserManager;
import org.ctonline.po.basic.CtCashCoupon;
import org.ctonline.po.basic.CtCoupon;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.Page;
import com.opensymphony.xwork2.ModelDriven;

public class CtCouponAction extends BaseAction implements RequestAware,
		ModelDriven<Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<CtCoupon> couponList;
	private List<?> querylist;

	public List<?> getQuerylist() {
		return querylist;
	}

	public void setQuerylist(List<?> querylist) {
		this.querylist = querylist;
	}

	private CtCouponManager couponManager;
	private CtCouponDetailManager coupondetailManager;
	
	private CtUserManager userManager;
	
	public CtUserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(CtUserManager userManager) {
		this.userManager = userManager;
	}

	public CtCouponDetailManager getCoupondetailManager() {
		return coupondetailManager;
	}

	public void setCoupondetailManager(CtCouponDetailManager coupondetailManager) {
		this.coupondetailManager = coupondetailManager;
	}

	private Map<String, Object> request;
	private Page page;
	private CtCoupon coupon;
	private CtCouponDTO couponDTO = new CtCouponDTO();
	private String filePath;
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public CtCouponDTO getCouponDTO() {
		return couponDTO;
	}

	public void setCouponDTO(CtCouponDTO couponDTO) {
		this.couponDTO = couponDTO;
	}

	public List<CtCoupon> getCouponList() {
		return couponList;
	}

	public void setCouponList(List<CtCoupon> couponList) {
		this.couponList = couponList;
	}

	public CtCouponManager getCouponManager() {
		return couponManager;
	}

	public void setCouponManager(CtCouponManager couponManager) {
		this.couponManager = couponManager;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtCoupon getCoupon() {
		return coupon;
	}

	public void setCoupon(CtCoupon coupon) {
		this.coupon = coupon;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.couponDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public String list() {
		try {
			page = new Page();
			if (couponDTO.getPage() == 0) {
				couponDTO.setPage(1);
			}
			page.setCurrentPage(couponDTO.getPage());
			this.couponList = couponManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	public String toadd() {

		return SUCCESS;
	}

	//发行优惠券
	public String couponIssue(){
		System.out.println(id);
		CtCoupon coupon = couponManager.findById((long)id);
		String count = coupondetailManager.findByCouponIdAndAllIisOk(coupon.getCouponId());
		String chai[] = count.split("__");
		this.request.put("allCount", chai[1]);
		this.request.put("weiYongCount", chai[0]);
		Long ucount = coupondetailManager.findUidCount();
		this.request.put("userCount", ucount);
		System.out.println(count);
		this.request.put("couponId", id);
		return SUCCESS;
	}
	//给全部用户发行优惠券
	public String fabuyouhuiquan(){
		resultJson = new JSONArray();
		List<CtUser> list = userManager.findAll("");
		for (int i = 0; i < list.size(); i++) {
			if(list.get(i).getUState() != null && !list.get(i).getUState().equals("0")){
				coupondetailManager.updateByCoupIdFaBuAll(list.get(i).getUId(), id);
			}
		}
		resultJson.add("发布优惠券成功");
		return SUCCESS;
	}
	//给指定用户发行优惠券
	public String fabuyouhui(){
		resultJson = new JSONArray();
		String key = couponDTO.getKeyword();
		String[] chai = key.split("  ");
		String str = "";
		for (int i = 0; i < chai.length; i++) {
			CtUser user = userManager.getCtUserByUUnameOrUMb(chai[i]);
			if(user.getUId() == null){
				str += chai[i]+"  ";
			} else {
				coupondetailManager.updateByCoupIdFaBuAll(user.getUId(), id);
			}
		}
		if(str.equals("")){
			resultJson.add("给指定用户发行优惠券成功");
		} else {
			resultJson.add("给指定用户发行优惠券成功,其中以下用户未找到 " + str);
		}
		return SUCCESS;
	}
	private JSONArray resultJson;
	
	public JSONArray getResultJson() {
		return resultJson;
	}

	public void setResultJson(JSONArray resultJson) {
		this.resultJson = resultJson;
	}

	//查找用户名
	public String findUserNameByPhoneAndName(){
		System.out.println(couponDTO.getKeyword());
		List<CtUser> userList = userManager.findAll(couponDTO.getKeyword());
		List<String> name = new ArrayList<String>();
		for (int i = 0; i < userList.size(); i++) {
			name.add(userList.get(i).getUUsername());
		}
		JSONArray json = new JSONArray();
		json = JSONArray.fromObject(name);
		resultJson = json;
		return SUCCESS;
	}
	public String add() {
		try {
			CtCoupon c = new CtCoupon();
			c = couponDTO.getCoupon();

			this.couponManager.save(c);
			return list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
		// return SUCCESS;
	}

	public String delete() {

		try {
			this.couponManager.delete(id);
			return list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
		// return SUCCESS;
	}
	
	private CtCashCoupon cashCoupon = new CtCashCoupon();
	private List<CtCashCoupon> cashCouponList = new ArrayList<CtCashCoupon>();
	
	public CtCashCoupon getCashCoupon() {
		return cashCoupon;
	}

	public void setCashCoupon(CtCashCoupon cashCoupon) {
		this.cashCoupon = cashCoupon;
	}

	public List<CtCashCoupon> getCashCouponList() {
		return cashCouponList;
	}

	public void setCashCouponList(List<CtCashCoupon> cashCouponList) {
		this.cashCouponList = cashCouponList;
	}

	//现金劵列表
	public String listCashCoupon(){
		this.page = new Page();
		if (this.couponDTO.getPage() == 0) {
		  this.couponDTO.setPage(1);
		}
		this.page.setCurrentPage(this.couponDTO.getPage());
		cashCouponList = couponManager.getCashCoupon(page);
		this.page.setTotalPage(this.page.getTotalPage());
		this.request.put("pages", this.page);
		return SUCCESS;
	}
	//添加现金劵
	public String addCashCoupon(){
		String coupName = couponDTO.getCoupon().getCouponName();
		String coupType = couponDTO.getCoupon().getCouponType();
		String orderSn = couponDTO.getCoupon().getGName();
		String countTime = couponDTO.getCoupon().getStime();
		long time15 = 15*60*1000;//15分钟
		long time20 = 20*60*1000;//20分钟
		long time30 = 30*60*1000;//30分钟
		long time60 = 60*60*1000;//60分钟
		
		//转化选择的时间，自动生成当前时间和有效期时间
		Date sdate = new Date(); // 获取但前时间
		System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(sdate)); //输出当前时间
		String sdatetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(sdate);
		Long stimelo = sdate.getTime();//获取当前时间的毫秒数
		if(countTime.equals("15")){
			stimelo = stimelo + time15;
		} else if (countTime.equals("20")){
			stimelo = stimelo + time20;
		} else if (countTime.equals("30")){
			stimelo = stimelo + time30;
		} else if (countTime.equals("60")){
			stimelo = stimelo + time60;
		}
		Date d = new Date();
		d.setTime(stimelo);
		System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d));  //加时后的时间
		String edatetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
		
		cashCoupon.setCashName(coupName);
		cashCoupon.setCashType(coupType);
		cashCoupon.setStime(sdatetime);
		cashCoupon.setEtime(edatetime);
		cashCoupon.setOrderSn(orderSn);
		
		Double amount = 0D;
		Double dis = 0D;
		//满减
		if(coupType.equals("1")){
			amount = couponDTO.getCoupon().getAmount();
			dis = couponDTO.getCoupon().getDiscount();
			//无限制
		} else if (coupType.equals("2")){
			dis = couponDTO.getCoupon().getDiscount();
			amount = 0D;
		}
		cashCoupon.setAmount(amount);
		cashCoupon.setDiscount(dis);
		couponManager.updateCashCoupon(cashCoupon);
		System.out.println(couponDTO);
		return SUCCESS;
	}

}