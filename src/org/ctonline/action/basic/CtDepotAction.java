package org.ctonline.action.basic;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.dto.basic.CtDepotDTO;
import org.ctonline.manager.basic.CtDepotManager;
import org.ctonline.po.basic.CtDepot;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class CtDepotAction extends ActionSupport implements RequestAware,
		ModelDriven<Object> {

	private List<CtDepot> depots;
	private CtDepotManager depotManager;
	private Map<String, Object> request;
	private Page page;
	private CtDepotDTO depotDTO = new CtDepotDTO();
	private CtDepot depot;

	public List<CtDepot> getDepots() {
		return depots;
	}

	public void setDepots(List<CtDepot> depots) {
		this.depots = depots;
	}

	public CtDepotManager getDepotManager() {
		return depotManager;
	}

	public void setDepotManager(CtDepotManager depotManager) {
		this.depotManager = depotManager;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtDepot getDepot() {
		return depot;
	}

	public void setDepot(CtDepot depot) {
		this.depot = depot;
	}

	@Override
	public Object getModel() {

		return this.depotDTO;
	}

	public String list() {
		try {
			page = new Page();
			if (depotDTO.getPage() == 0) {
				depotDTO.setPage(1);
			}
			page.setCurrentPage(depotDTO.getPage());
			this.depots = depotManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 按照关键字查询商品品牌
	public String search() {
		try {
			Page page = new Page();
			if (depotDTO.getPage() == 0) {
				depotDTO.setPage(1);
			}
			page.setCurrentPage(depotDTO.getPage());
			if (depotDTO.getKeyword() != null
					&& !depotDTO.getKeyword().equals("请输入你要查的关键词")) {
				this.depots = depotManager.findAll(depotDTO.getKeyword(), page);
			} else {
				this.depots = depotManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 增加商品品牌
	public String add() {
		try {
			CtDepot m = new CtDepot();
			m.setDepotAdd(depotDTO.getDepotAdd());
			m.setDepotName(depotDTO.getDepotName());
			m.setDisArr(depotDTO.getDisArr());
			this.depotManager.save(m);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 删除商品品牌
	public String del() {

		try {
			for (Integer m : depotDTO.getMid()) {
				this.depotManager.delete(m);

			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 修改商品品牌
	public String update() {
		try {
			CtDepot m = new CtDepot();
			m = this.depotManager.findById(depotDTO.getId());
			m.setDepotAdd(depotDTO.getDepotAdd());
			m.setDepotName(depotDTO.getDepotName());
			m.setDisArr(depotDTO.getDisArr());
			depotManager.update(m);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String find() {
		try {
			Integer id = depotDTO.getId();
			depot = new CtDepot();
			depot.setDepotId(id);
			this.depot = this.depotManager.findById(id);
			depotDTO.setDepotAdd(depot.getDepotAdd());
			depotDTO.setDepotName(depot.getDepotName());
			depotDTO.setDisArr(depot.getDisArr());

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}

}
