package org.ctonline.action.basic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.dto.basic.CtAdDTO;
import org.ctonline.manager.basic.CtAdManager;
import org.ctonline.manager.basic.CtAdTypeManager;
import org.ctonline.po.basic.CtAd;
import org.ctonline.po.basic.CtAdType;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.util.ImageUtil;
import org.ctonline.util.OSSObjectSample;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class CtAdAction extends ActionSupport implements RequestAware,
		ModelDriven<Object> {
	private List<CtAd> ads;
	private CtAdManager adManager;
	private CtAdTypeManager adTypeManager;
	private Page page;
	private CtAd ad;
	private CtAdDTO adDTO = new CtAdDTO();
	private Map<String, Object> request;
	private CtAdType adType;

	private File file;
	private String fileFileName;
	private String fileContentType;

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public String getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(String fileContentType) {
		this.fileContentType = fileContentType;
	}

	private String level;
	private String type;

	public CtAdType getAdType() {
		return adType;
	}

	public void setAdType(CtAdType adType) {
		this.adType = adType;
	}

	public List<CtAd> getAds() {
		return ads;
	}

	public void setAds(List<CtAd> ads) {
		this.ads = ads;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public CtAdManager getAdManager() {
		return adManager;
	}

	public void setAdManager(CtAdManager adManager) {
		this.adManager = adManager;
	}

	public CtAdTypeManager getAdTypeManager() {
		return adTypeManager;
	}

	public void setAdTypeManager(CtAdTypeManager adTypeManager) {
		this.adTypeManager = adTypeManager;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtAd getAd() {
		return ad;
	}

	public void setAd(CtAd ad) {
		this.ad = ad;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.adDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String list() {
		try {
			page = new Page();
			if (adDTO.getPage() == 0) {
				adDTO.setPage(1);
			}
			page.setCurrentPage(adDTO.getPage());
			this.ads = adManager.loadAll(page);
			List<CtAdType> adTypesList = adTypeManager.findAllType();
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			this.request.put("adTypeList", adTypesList);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	public String getOneAd() {
		int adId = 2;
		this.ad = adManager.getAdByAdId(adId);
		return "showad";
	}

	public String add() {
		try {
			CtAd a = new CtAd();
			a.setAdId(adDTO.getAdId());
			a.setAdTitle(adDTO.getAdTitle());
			a.setAdUrl(adDTO.getAdUrl());
			a.setAdDesc(adDTO.getAdDesc());
			a.setAdImg(adDTO.getAdImg());
			a.setAdWidth(adDTO.getAdWidth());
			a.setAdHeight(adDTO.getAdHeight());
			Integer aa = this.adManager.save(a);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	public String update() {
		try {
			CtAd m = new CtAd();
			Integer mid = adDTO.getAdId();
			m = this.adManager.findById(adDTO.getAdId());
			m.setAdTitle(adDTO.getAdTitle());
			adManager.update(m);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	public String del() {
		try {
			for (int m : adDTO.getMid()) {
				this.adManager.delete(m);
			}
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	public String search() {
		try {
			Page page = new Page();
			if (adDTO.getPage() == 0) {
				adDTO.setPage(1);
			}
			page.setCurrentPage(adDTO.getPage());
			if (adDTO.getKeyword() != null
					&& !adDTO.getKeyword().equals("请输入关键字")) {
				this.ads = adManager.findAll(adDTO.getKeyword(), page);
			} else {
				this.ads = adManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	public String saveAd() {
		try {
			// 上传目录
			String dir = "";
			// 带时间戳的文件名
			String timename = "";
			// 缩略图存储路径
			String imgSmalldir = "";
			// 上传图片
			if (file == null) {
				// return INPUT;
			}
			CtAd ad = new CtAd();
			try {
				InputStream in = new FileInputStream(file);
				String UPLOADDIR = "/upload2";
				dir = ServletActionContext.getRequest().getRealPath(UPLOADDIR);
				String time = new Date().getTime() + "";
				timename = time + getExtention(this.getFileFileName());
				String url = UPLOADDIR + "/" + timename;
				ad.setAdImg(url);
				File fileLocation = new File(dir);
				if (!fileLocation.exists()) {
					boolean isCreated = fileLocation.mkdirs();
					if (!isCreated) {
						System.out.println("上传目录创建失败！");
						// 上传目录创建失败处理
						return null;
					}
				}
				File uploadFile = new File(dir, timename);
				OutputStream out = new FileOutputStream(uploadFile);
				byte[] buffer = new byte[1024 * 1024];
				int length;
				while ((length = in.read(buffer)) > 0) {
					out.write(buffer, 0, length);
				}
				in.close();
				out.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("上传失败！");
			} catch (IOException e) {
				// TODO: handle exception
				e.printStackTrace();
				System.out.println("上传失败！");
			}

			ad.setAdId(adDTO.getAdId());
			ad.setAdTitle(adDTO.getAdTitle());

			ad.setAdUrl(adDTO.getAdUrl());
			ad.setAdDesc(adDTO.getAdDesc());
			ad.setAdWidth(adDTO.getAdWidth());
			ad.setAdHeight(adDTO.getAdHeight());
			int id = adManager.save(ad);
			return "saveAdd";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	private String getExtention(String fileName) {
		// TODO Auto-generated method stub
		int pos = fileName.lastIndexOf(".");
		return fileName.substring(pos);
	}
	
	public String addAdInfo(){
		CtAd a = new CtAd();
		a.setAdId(adDTO.getAdId());
		a.setAdTitle(adDTO.getAdTitle());
		a.setAdUrl(adDTO.getAdUrl());
		a.setAdDesc(adDTO.getAdDesc());
		a.setAdImg(adDTO.getAdImg());
		
		a.setAdType(adDTO.getAdType());
		a.setAdSort(adDTO.getAdSort());
		a.setAdIsUp(adDTO.getAdIsUp());
		
		Integer aa = this.adManager.save(a);
		
		return SUCCESS;
	}
	private File fileIndexImg;
	private File fileUpdate;
	
	
	public CtAdDTO getAdDTO() {
		return adDTO;
	}

	public void setAdDTO(CtAdDTO adDTO) {
		this.adDTO = adDTO;
	}

	public File getFileIndexImg() {
		return fileIndexImg;
	}

	public void setFileIndexImg(File fileIndexImg) {
		this.fileIndexImg = fileIndexImg;
	}

	public File getFileUpdate() {
		return fileUpdate;
	}

	public void setFileUpdate(File fileUpdate) {
		this.fileUpdate = fileUpdate;
	}

	private JSONObject res;
	
	public JSONObject getRes() {
		return res;
	}

	public void setRes(JSONObject res) {
		this.res = res;
	}

	//主图上传至OSS
	public String uploadIndexImgFile(){
		 Map<String,Object> result = new HashMap<String, Object>();
		 	File f = null;
	        if(fileIndexImg == null){
	        	f = fileUpdate;
	        } else {
	        	f = fileIndexImg;
	        }
	        try {
	        	
	            String bucketName = "ctegoimg";
	            String fileName = new SimpleDateFormat("MMddHHmmssSSS").format(new Date())+".jpg";
	            String key = "banner/"+fileName;
	            OSSObjectSample.uploadFile(bucketName, key, f.getPath());	
	            result.put("url", key);
	        }
	        catch (Exception e) {
	        	e.printStackTrace();
	            System.out.println(e.getMessage());
	            result.put("state", "文件上传失败!");
	            result.put("url","");
	            result.put("title", "");
	            result.put("original", "");
	            System.out.println("文件 "+f.getName()+" 上传失败!");
	        }
	        JSONObject json = new JSONObject();
	        res = json.fromObject(result);
	        return SUCCESS;
	}
	
	public String find(){
		try {
			int id = Integer.valueOf(adDTO.getAdId());
			CtAd ad = new CtAd();
			ad = adManager.findById(id);
			
			adDTO.setAdId(ad.getAdId());
			adDTO.setAdDesc(ad.getAdDesc());
			adDTO.setAdImg(ad.getAdImg());
			adDTO.setAdIsUp(ad.getAdIsUp());
			adDTO.setAdSort(ad.getAdSort());
			adDTO.setAdTitle(ad.getAdTitle());
			adDTO.setAdType(ad.getAdType());
			adDTO.setAdUrl(ad.getAdUrl());
			
			List<CtAdType> adTypesList = adTypeManager.findAllType();
			adDTO.setAdTypes(adTypesList);
			
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
	
	//更新广告信息
	public String updateAd(){
		CtAd m = new CtAd();
		Integer mid = adDTO.getAdId();
		m = this.adManager.findById(adDTO.getAdId());
		m.setAdTitle(adDTO.getAdTitle());
		m.setAdDesc(adDTO.getAdDesc());
		m.setAdImg(adDTO.getAdImg());
		m.setAdIsUp(adDTO.getAdIsUp());
		m.setAdSort(adDTO.getAdSort());
		m.setAdType(adDTO.getAdType());
		m.setAdUrl(adDTO.getAdUrl());
		
		adManager.update(m);
		return SUCCESS;
	}
	
}
