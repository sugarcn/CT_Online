package org.ctonline.action.basic;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import oracle.sql.BLOB;

import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.dto.basic.CtNoticeDTO;
import org.ctonline.manager.basic.CtNoticeManager;
import org.ctonline.po.basic.CtFilter;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.basic.CtNoticeType;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.OSSObjectSample;
import org.ctonline.util.Page;
import org.hibernate.Hibernate;
import org.hibernate.LobHelper;
import org.hibernate.internal.SessionImpl;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class CtNoticeAction extends ActionSupport implements RequestAware,
		ModelDriven<Object> {
	private List<CtNotice> notices;
	private CtNoticeManager noticeManager;
	private Page page;
	private CtNoticeDTO noticeDTO = new CtNoticeDTO();
	private Map<String, Object> request;
	private CtNotice notice;

	public CtNoticeManager getNoticeManager() {
		return noticeManager;
	}

	public void setNoticeManager(CtNoticeManager noticeManager) {
		this.noticeManager = noticeManager;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtNoticeDTO getNoticeDTO() {
		return noticeDTO;
	}

	public void setNoticeDTO(CtNoticeDTO noticeDTO) {
		this.noticeDTO = noticeDTO;
	}

	public List<CtNotice> getNotices() {
		return notices;
	}

	public void setNotices(List<CtNotice> notices) {
		this.notices = notices;
	}

	public CtNotice getNotice() {
		return notice;
	}

	public void setNotice(CtNotice notice) {
		this.notice = notice;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.noticeDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public String list() {
		try {
			page = new Page();
			if (noticeDTO.getPage() == 0) {
				noticeDTO.setPage(1);
			}
			page.setCurrentPage(noticeDTO.getPage());
			this.notices = noticeManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			noticeTypesList = noticeManager.findTypeByPage(null);
			
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String find() {
		try {
			int id = Integer.valueOf(noticeDTO.getId());
			notice = new CtNotice();
			notice.setNoId(id);
			this.notice = noticeManager.findById(id);
			noticeDTO.setNoId(notice.getNoId());
			noticeDTO.setNoTitle(notice.getNoTitle());
			noticeDTO.setNoContent(notice.getNoContent());
			noticeDTO.setNoUrl(notice.getNoUrl());
			noticeDTO.setTypeId(notice.getNoTypeId());
			noticeDTO.setImgUrl(notice.getNoImgUrl());
			noticeDTO.setNoticeTypeList(noticeManager.findTypeByPage(null));
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
	public static String ClobToString(Clob clob) throws SQLException, IOException { 

		String reString = ""; 
		Reader is = clob.getCharacterStream();// 得到流 
		BufferedReader br = new BufferedReader(is); 
		String s = br.readLine(); 
		StringBuffer sb = new StringBuffer(); 
		while (s != null) {// 执行循环将字符串全部取出付值给 StringBuffer由StringBuffer转成STRING 
		sb.append(s); 
		s = br.readLine(); 
		} 
		reString = sb.toString(); 
		return reString; 
		}
	public String findNoticeType() {
		try {
			int id = Integer.valueOf(noticeDTO.getId());
			noticeType = noticeManager.findTypeByTypeId(id);
			
			noticeDTO.setNoId(noticeType.getNoTypeId());
			noticeDTO.setNoTitle(noticeType.getNoTypeName());
			noticeDTO.setNoContent(noticeType.getNoTypeDesc());
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
	public String del() {
		try {
			for (int m : noticeDTO.getMid()) {
				this.noticeManager.delete(m);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
	public String delNoticeType() {
		try {
			for (int m : noticeDTO.getMid()) {
				this.noticeManager.deleteNoticeTypeById(m);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String add() {
		try {
			CtNotice cn = new CtNotice();
			cn.setNoId(noticeDTO.getNoId());
			cn.setNoTitle(noticeDTO.getNoTitle());
			cn.setNoTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			cn.setNoContent(noticeDTO.getNoContent());
			cn.setNoUrl(noticeDTO.getNoUrl());
			cn.setNoTypeId(noticeDTO.getTypeId());
			cn.setNoImgUrl(noticeDTO.getImgUrl());
			this.noticeManager.save(cn);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
	public String addNoticeType() {
		try {
			noticeType = new CtNoticeType();
			noticeType.setNoTypeName(noticeDTO.getNoTitle());
			noticeType.setNoTypeDesc(noticeDTO.getNoContent());
			noticeManager.saveNoticeType(noticeType);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String update() {
		try {
			CtNotice cn = new CtNotice();
			cn = this.noticeManager.findById(Integer.valueOf(noticeDTO.getId()));
			cn.setNoTitle(noticeDTO.getNoTitle());
			cn.setNoContent(noticeDTO.getNoContent());
			cn.setNoUrl(noticeDTO.getNoUrl());
			cn.setNoTypeId(noticeDTO.getTypeId());
			cn.setNoImgUrl(noticeDTO.getImgUrl());
			noticeManager.update(cn);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
	public String updateNoticeType() {
		try {
			noticeType = new CtNoticeType();
			noticeType.setNoTypeId(Integer.valueOf(noticeDTO.getId()));
			noticeType.setNoTypeName(noticeDTO.getNoTitle());
			noticeType.setNoTypeDesc(noticeDTO.getNoContent());
			noticeManager.updateNoticeType(noticeType);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String search() {
		try {
			Page page = new Page();
			if (noticeDTO.getPage() == 0) {
				noticeDTO.setPage(1);
			}
			page.setCurrentPage(noticeDTO.getPage());
			if (noticeDTO.getKeyword() != null
					&& !noticeDTO.getKeyword().equals("请输入关键字")) {
				this.notices = noticeManager.findAll(noticeDTO.getKeyword(),
						page);
			} else {
				this.notices = noticeManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

private String encode;

	public String getEncode() {
	return encode;
}

public void setEncode(String encode) {
	this.encode = encode;
}

private String fileuploadPath;
private String httpPath;

	public String getFileuploadPath() {
	return fileuploadPath;
}

public void setFileuploadPath(String fileuploadPath) {
	this.fileuploadPath = fileuploadPath;
}

public String getHttpPath() {
	return httpPath;
}

public void setHttpPath(String httpPath) {
	this.httpPath = httpPath;
}

private File upfile;
private String upfileFileName;


	public String getUpfileFileName() {
	return upfileFileName;
}

public void setUpfileFileName(String upfileFileName) {
	this.upfileFileName = upfileFileName;
}

	public File getUpfile() {
	return upfile;
}

public void setUpfile(File upfile) {
	this.upfile = upfile;
}

	//公告图片上传
	public String upImgFile() throws Exception{
	
		 Map<String,Object> result = new HashMap<String, Object>();
	        
	        try {
	        	
	            String bucketName = "ctegoimg";
	            String fileName = new SimpleDateFormat("MMddHHmmssSSS").format(new Date());
	            String key = "img/"+fileName;
	    		
	            OSSObjectSample.uploadFile(bucketName, key, upfile.getPath());	
	            
	            result.put("state", "SUCCESS");// UEDITOR的规则:不为SUCCESS则显示state的内容
	            result.put("url",fileName);
	            result.put("title", fileName);
	            result.put("original", fileName);
	        }
	        catch (Exception e) {
	        	e.printStackTrace();
	            System.out.println(e.getMessage());
	            result.put("state", "文件上传失败!");
	            result.put("url","");
	            result.put("title", "");
	            result.put("original", "");
	            System.out.println("文件 "+upfile.getName()+" 上传失败!");
	        }
	        JSONObject json = new JSONObject();
	        res = json.fromObject(result);
	        return SUCCESS;
	}
	private JSONObject res;

	public JSONObject getRes() {
		return res;
	}

	public void setRes(JSONObject res) {
		this.res = res;
	}
	private List<CtNoticeType> noticeTypesList = new ArrayList<CtNoticeType>();
	private CtNoticeType noticeType = new CtNoticeType();
	
	public List<CtNoticeType> getNoticeTypesList() {
		return noticeTypesList;
	}

	public void setNoticeTypesList(List<CtNoticeType> noticeTypesList) {
		this.noticeTypesList = noticeTypesList;
	}

	public CtNoticeType getNoticeType() {
		return noticeType;
	}

	public void setNoticeType(CtNoticeType noticeType) {
		this.noticeType = noticeType;
	}

	public String listNoticeType(){
		try {
			page = new Page();
			if (noticeDTO.getPage() == 0) {
				noticeDTO.setPage(1);
			}
			page.setCurrentPage(noticeDTO.getPage());
			noticeTypesList = noticeManager.findTypeByPage(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		return SUCCESS;
	}
	private File file;              //文件  
	private File fileUpdate;        //文件  
    private String fileFileName;    //文件名   
    private String filePath;        //文件路径
    private String downloadFilePath;  //文件下载路径
    
	public File getFileUpdate() {
		return fileUpdate;
	}

	public void setFileUpdate(File fileUpdate) {
		this.fileUpdate = fileUpdate;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getDownloadFilePath() {
		return downloadFilePath;
	}

	public void setDownloadFilePath(String downloadFilePath) {
		this.downloadFilePath = downloadFilePath;
	}

	public String uploadNoImgFile(){

		 Map<String,Object> result = new HashMap<String, Object>();
		 	File f = null;
	        if(file == null){
	        	f = fileUpdate;
	        } else {
	        	f = file;
	        }
	        try {
	        	
	            String bucketName = "ctegoimg";
	            String fileName = new SimpleDateFormat("MMddHHmmssSSS").format(new Date());
	            String key = "img/"+fileName;
	            OSSObjectSample.uploadFile(bucketName, key, f.getPath());	
	            result.put("url", key);
	        }
	        catch (Exception e) {
	        	e.printStackTrace();
	            System.out.println(e.getMessage());
	            result.put("state", "文件上传失败!");
	            result.put("url","");
	            result.put("title", "");
	            result.put("original", "");
	            System.out.println("文件 "+f.getName()+" 上传失败!");
	        }
	        JSONObject json = new JSONObject();
	        res = json.fromObject(result);
	        return SUCCESS;
	}
	private List<CtFilter> filtersList = new ArrayList<CtFilter>();
	private CtFilter filter = new CtFilter();
	public List<CtFilter> getFiltersList() {
		return filtersList;
	}

	public void setFiltersList(List<CtFilter> filtersList) {
		this.filtersList = filtersList;
	}

	public CtFilter getFilter() {
		return filter;
	}

	public void setFilter(CtFilter filter) {
		this.filter = filter;
	}

	//关键字管理
	public String firltList(){
		page = new Page();
		if (noticeDTO.getPage() == 0) {
			noticeDTO.setPage(1);
		}
		page.setCurrentPage(noticeDTO.getPage());
		filtersList = noticeManager.findFilterListByPage(page);
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);
		return SUCCESS;
	}
	public String addFirlt(){
		String name = noticeDTO.getNoTitle();
		filter.setFname(name);
		noticeManager.saveFirlt(filter);
		return SUCCESS;
	}
	public String findFirlt(){
		int id = Integer.valueOf(noticeDTO.getId());
		filter = noticeManager.findFirltById(id);
		noticeDTO.setNoId(filter.getFid());
		noticeDTO.setNoTitle(filter.getFname());
		return SUCCESS;
	}
	public String updateFirlt(){
		String name = noticeDTO.getNoTitle();
		Integer id = Integer.valueOf(noticeDTO.getId());
		filter.setFid(id);
		filter.setFname(name);
		noticeManager.saveFirlt(filter);
		return SUCCESS;
	}
	public String deleteFirlt(){
		for (int m : noticeDTO.getMid()) {
			filter = noticeManager.findFirltById(m);
			noticeManager.deleteFilterByFilter(filter);
		}
		return SUCCESS;
	}
	

}
