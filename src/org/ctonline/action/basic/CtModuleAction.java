package org.ctonline.action.basic;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;

import org.ctonline.dto.basic.CtModuleDTO;
import org.ctonline.manager.basic.CtModuleManager;
import org.ctonline.po.basic.CtModule;
import org.ctonline.po.manager.CtManager;
import org.ctonline.po.views.ViewModuleLeft;
import org.ctonline.po.views.ViewModuleTop;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class CtModuleAction extends ActionSupport implements RequestAware,
		ModelDriven<Object> {
	private List<CtModule> modules;
	private CtModuleManager moduleManager;
	private Map<String, Object> request;
	private Page page;
	private CtModuleDTO moduleDTO = new CtModuleDTO();
	private CtModule module;
	private Integer parentId;
	private List<ViewModuleTop> lists;
	private List<ViewModuleLeft> leftlists;

	public List<CtModule> getModules() {
		return modules;
	}

	public void setModules(List<CtModule> modules) {
		this.modules = modules;
	}

	public CtModuleManager getModuleManager() {
		return moduleManager;
	}

	public void setModuleManager(CtModuleManager moduleManager) {
		this.moduleManager = moduleManager;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtModule getModule() {
		return module;
	}

	public void setModule(CtModule module) {
		this.module = module;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.moduleDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public String getTopModule() {
		try {
			Map session = ActionContext.getContext().getSession();
			CtManager cm = (CtManager) session.get("admininfosession");
			Integer roleId = cm.getRoleId();

			this.lists = moduleManager.getTopModule(roleId);
			return "topmodule";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String getLeftModule() {
		try {
			Integer moduleId = moduleDTO.getModuleId();
			Map session = ActionContext.getContext().getSession();
			CtManager cm = (CtManager) session.get("admininfosession");
			Integer roleId = cm.getRoleId();
			this.leftlists = moduleManager.getLeftModule(roleId, moduleId);
			return "leftmodule";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String list() {
		try {
			page = new Page();
			if (moduleDTO.getPage() == 0) {
				moduleDTO.setPage(1);
			}
			page.setCurrentPage(moduleDTO.getPage());
			this.modules = moduleManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String search() {
		try {
			Page page = new Page();
			if (moduleDTO.getPage() == 0) {
				moduleDTO.setPage(1);
			}
			page.setCurrentPage(moduleDTO.getPage());
			if (moduleDTO.getKeyword() != null
					&& !moduleDTO.getKeyword().equals("请输入关键字")) {
				this.modules = moduleManager.findAll(moduleDTO.getKeyword(),
						page);
			} else {
				this.modules = moduleManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			moduleDTO.setKeyword(moduleDTO.getKeyword());

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String add() {
		try {
			CtModule m = new CtModule();
			m.setModuleName(moduleDTO.getModuleName());
			m.setParentId(moduleDTO.getParentId());
			m.setModuleDesc(moduleDTO.getModuleDesc());
			m.setImgUrl(moduleDTO.getImgUrl());
			m.setModuleUrl(moduleDTO.getModuleUrl());
			this.moduleManager.save(m);
			Integer moduleId = moduleDTO.getParentId();
			parentId = moduleId;
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String del() {
		try {
			for (int m : moduleDTO.getMid()) {
				this.moduleManager.delete(m);

			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String update() {
		try {
			CtModule m = new CtModule();
			Integer mid = moduleDTO.getModuleId();
			m = this.moduleManager.findById(mid);
			m.setModuleName(moduleDTO.getModuleName());
			m.setModuleDesc(moduleDTO.getModuleDesc());
			m.setImgUrl(moduleDTO.getImgUrl());
			m.setModuleUrl(moduleDTO.getModuleUrl());
			m.setParentId(moduleDTO.getParentId());
			this.moduleManager.update(m);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String find() {
		try {
			Integer id = moduleDTO.getId();
			module = new CtModule();
			module.setModuleId(id);
			this.module = this.moduleManager.findById(id);
			moduleDTO.setParentId(module.getParentId());
			moduleDTO.setModuleName(module.getModuleName());
			moduleDTO.setModuleUrl(module.getModuleUrl());
			moduleDTO.setModuleDesc(module.getModuleDesc());
			moduleDTO.setImgUrl(module.getImgUrl());
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}

	public CtModuleDTO getModuleDTO() {
		return moduleDTO;
	}

	public void setModuleDTO(CtModuleDTO moduleDTO) {
		this.moduleDTO = moduleDTO;
	}

	public List<ViewModuleTop> getLists() {
		return lists;
	}

	public void setLists(List<ViewModuleTop> lists) {
		this.lists = lists;
	}

	public List<ViewModuleLeft> getLeftlists() {
		return leftlists;
	}

	public void setLeftlists(List<ViewModuleLeft> leftlists) {
		this.leftlists = leftlists;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

}
