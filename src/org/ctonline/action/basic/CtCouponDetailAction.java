package org.ctonline.action.basic;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.action.BaseAction;
import org.ctonline.common.CommonFunc;
import org.ctonline.dto.basic.CtCouponDetailDTO;
import org.ctonline.manager.basic.CtCouponDetailManager;
import org.ctonline.po.basic.CtResourceType;
import org.ctonline.po.basic.CtCouponDetail;
import org.ctonline.util.Page;
import com.opensymphony.xwork2.ModelDriven;

public class CtCouponDetailAction extends BaseAction implements RequestAware,
		ModelDriven<Object> {

	private List<CtCouponDetail> coupondetailList;
	private List<?> querylist;
	int id;

	public List<CtCouponDetail> getCoupondetailList() {
		return coupondetailList;
	}

	public void setCoupondetailList(List<CtCouponDetail> coupondetailList) {
		this.coupondetailList = coupondetailList;
	}

	public CtCouponDetailManager getCoupondetailManager() {
		return coupondetailManager;
	}

	public void setCoupondetailManager(CtCouponDetailManager coupondetailManager) {
		this.coupondetailManager = coupondetailManager;
	}

	public CtCouponDetail getCoupondetail() {
		return coupondetail;
	}

	public void setCoupondetail(CtCouponDetail coupondetail) {
		this.coupondetail = coupondetail;
	}

	public CtCouponDetailDTO getCoupondetailDTO() {
		return coupondetailDTO;
	}

	public void setCoupondetailDTO(CtCouponDetailDTO coupondetailDTO) {
		this.coupondetailDTO = coupondetailDTO;
	}

	public List<?> getQuerylist() {
		return querylist;
	}

	public void setQuerylist(List<?> querylist) {
		this.querylist = querylist;
	}

	private List<CtResourceType> typeList;

	public List<CtResourceType> getTypeList() {
		return typeList;
	}

	public void setTypeList(List<CtResourceType> typeList) {
		this.typeList = typeList;
	}

	private CtCouponDetailManager coupondetailManager;
	private Map<String, Object> request;
	private Page page;
	private CtCouponDetail coupondetail;
	private CtCouponDetailDTO coupondetailDTO = new CtCouponDetailDTO();
	private String filePath;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public CtCouponDetailDTO getCouponDetailDTO() {
		return coupondetailDTO;
	}

	public void setCouponDetailDTO(CtCouponDetailDTO coupondetailDTO) {
		this.coupondetailDTO = coupondetailDTO;
	}

	public List<CtCouponDetail> getCouponDetailList() {
		return coupondetailList;
	}

	public void setCouponDetailList(List<CtCouponDetail> coupondetailList) {
		this.coupondetailList = coupondetailList;
	}

	public CtCouponDetailManager getCouponDetailManager() {
		return coupondetailManager;
	}

	public void setCouponDetailManager(CtCouponDetailManager coupondetailManager) {
		this.coupondetailManager = coupondetailManager;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtCouponDetail getCoupon() {
		return coupondetail;
	}

	public void setCouponDetail(CtCouponDetail coupondetail) {
		this.coupondetail = coupondetail;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.coupondetailDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public String list() {
		try {
			if (id == 0)
				return listAll();

			page = new Page();
			if (coupondetailDTO.getPage() == 0) {
				coupondetailDTO.setPage(1);
			}
			page.setCurrentPage(coupondetailDTO.getPage());
			this.coupondetailList = coupondetailManager.findByCouponId(id, page);

			Collections.reverse(coupondetailList);
			querylist = CommonFunc.getPage(coupondetailList, getPagebean());
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}

	}

	public String listOne(){
		page = new Page();
		if (coupondetailDTO.getPage() == 0) {
			coupondetailDTO.setPage(1);
		}
		page.setCurrentPage(coupondetailDTO.getPage());
		coupondetailList = coupondetailManager.findByCouponId(coupondetailDTO.getCouponDetail().getCouponId(), page);
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);
		return SUCCESS;
	}
	
	public String listAll() {

		try {
			page = new Page();
			if (coupondetailDTO.getPage() == 0) {
				coupondetailDTO.setPage(1);
			}
			page.setCurrentPage(coupondetailDTO.getPage());
			// this.coupondetailList= coupondetailManager.findByCouponId(id);
			this.coupondetailList = coupondetailManager.queryAll();
			Collections.reverse(coupondetailList);
			querylist = CommonFunc.getPage(coupondetailList, getPagebean());
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String toadd() {

		return SUCCESS;
	}

	public String add() {
		try {
			CtCouponDetail c = new CtCouponDetail();
			c = coupondetailDTO.getCouponDetail();

			this.coupondetailManager.save(c);
			return list();
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String delete() {

		try {
			this.coupondetailManager.delete(id);
			return list();
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

}
