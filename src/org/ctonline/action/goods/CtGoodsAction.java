package org.ctonline.action.goods;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.action.BaseAction;
import org.ctonline.common.CommonFunc;
import org.ctonline.common.Solr;
import org.ctonline.dto.goods.CtGoodsDTO;
import org.ctonline.manager.basic.CtDepotManager;
import org.ctonline.manager.basic.CtResourceManager;
import org.ctonline.manager.goods.CtGoodsARManager;
import org.ctonline.manager.goods.CtGoodsAttributeManager;
import org.ctonline.manager.goods.CtGoodsDetailManager;
import org.ctonline.manager.goods.CtGoodsGroupRelationManager;
import org.ctonline.manager.goods.CtGoodsImgManager;
import org.ctonline.manager.goods.CtGoodsLinkManager;
import org.ctonline.manager.goods.CtGoodsManager;
import org.ctonline.manager.goods.CtGoodsNumManager;
import org.ctonline.manager.goods.CtGoodsReplaceManager;
import org.ctonline.manager.goods.CtGoodsTypeManager;
import org.ctonline.po.basic.CtCashCoupon;
import org.ctonline.po.basic.CtDepot;
import org.ctonline.po.basic.CtFreeSampleReg;
import org.ctonline.po.basic.CtReplace;
import org.ctonline.po.basic.CtResource;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsAttribute;
import org.ctonline.po.goods.CtGoodsAttributeRelation;
import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGoodsDetail;
import org.ctonline.po.goods.CtGoodsGroupRelation;
import org.ctonline.po.goods.CtGoodsLink;
import org.ctonline.po.goods.CtGoodsNum;
import org.ctonline.po.goods.CtGoodsReplace;
import org.ctonline.po.goods.CtGoodsResource;
import org.ctonline.po.goods.CtGoodsType;
import org.ctonline.po.goods.CtRangePrice;
import org.ctonline.po.goods.CtSearchCollect;
import org.ctonline.po.views.GoodsDetailView;
import org.ctonline.po.views.GoodsNumView;
import org.ctonline.po.views.ViewGoodsAttribute;
import org.ctonline.po.views.ViewGoodsLink;
import org.ctonline.po.views.ViewGoodsReplace;
import org.ctonline.po.views.ViewGoodsResource;
import org.ctonline.util.ExportUtil;
import org.ctonline.util.Page;
import org.json.JSONException;
import org.json.JSONObject;

import com.opensymphony.xwork2.ModelDriven;

import freemarker.template.SimpleNumber;

public class CtGoodsAction extends BaseAction implements RequestAware,
		ModelDriven<Object> {

	private List<CtGoods> goodsList;
	
	private JSONArray resultJson;
	public JSONArray getResultJson() {
		return resultJson;
	}

	public void setResultJson(JSONArray resultJson) {
		this.resultJson = resultJson;
	}

	// 详情List
	private List<GoodsDetailView> goodsDetailList;

	public List<GoodsDetailView> getGoodsDetailList() {
		return goodsDetailList;
	}

	public void setGoodsDetailList(List<GoodsDetailView> goodsDetailList) {
		this.goodsDetailList = goodsDetailList;
	}

	CtGoodsDetail goodsDetail;

	public CtGoodsDetail getGoodsDetail() {
		return goodsDetail;
	}

	public void setGoodsDetail(CtGoodsDetail goodsDetail) {
		this.goodsDetail = goodsDetail;
	}

	// 资料List
	private List<CtResource> resList;

	public List<CtResource> getResList() {
		return resList;
	}

	public void setResList(List<CtResource> resList) {
		this.resList = resList;
	}

	private Long GId;

	public Long getGId() {
		return GId;
	}

	public void setGId(Long gId) {
		GId = gId;
	}

	// 类别List
	private List<CtGoodsType> typeList;

	public List<CtGoodsType> getTypeList() {
		return typeList;
	}

	public void setTypeList(List<CtGoodsType> typeList) {
		this.typeList = typeList;
	}

	// 分页传值List
	private List<?> querylist;

	public List<?> getQuerylist() {
		return querylist;
	}

	public void setQuerylist(List<?> querylist) {
		this.querylist = querylist;
	}

	// 品牌List
	List<CtGoodsBrand> brandList;

	public List<CtGoodsBrand> getBrandList() {
		return brandList;
	}

	public void setBrandList(List<CtGoodsBrand> brandList) {
		this.brandList = brandList;
	}

	// 商品分类List
	List<CtGoodsCategory> categoryList;

	public List<CtGoodsCategory> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<CtGoodsCategory> categoryList) {
		this.categoryList = categoryList;
	}

	// 商品属性List
	List<CtGoodsAttribute> attributeList;

	public List<CtGoodsAttribute> getAttributeList() {
		return attributeList;
	}

	public void setAttributeList(List<CtGoodsAttribute> attributeList) {
		this.attributeList = attributeList;
	}

	// 仓库List
	List<CtDepot> depotList;

	public List<CtDepot> getDepotList() {
		return depotList;
	}

	public void setDepotList(List<CtDepot> depotList) {
		this.depotList = depotList;
	}

	// NumViewList
	List<GoodsNumView> numView;

	public List<GoodsNumView> getNumView() {
		return numView;
	}

	public void setNumView(List<GoodsNumView> numView) {
		this.numView = numView;
	}

	// 详细描述List
	private List<CtGoodsDetail> detailList;

	public List<CtGoodsDetail> getDetailList() {
		return detailList;
	}

	public void setDetailList(List<CtGoodsDetail> detailList) {
		this.detailList = detailList;
	}

	private List<ViewGoodsResource> viewresList;

	public List<ViewGoodsResource> getViewresList() {
		return viewresList;
	}

	public void setViewresList(List<ViewGoodsResource> viewresList) {
		this.viewresList = viewresList;
	}

	private List<ViewGoodsAttribute> attList;

	public List<ViewGoodsAttribute> getAttList() {
		return attList;
	}

	public void setAttList(List<ViewGoodsAttribute> attList) {
		this.attList = attList;
	}

	private List<ViewGoodsLink> linkList;

	public List<ViewGoodsLink> getLinkList() {
		return linkList;
	}

	public void setLinkList(List<ViewGoodsLink> linkList) {
		this.linkList = linkList;
	}

	private List<ViewGoodsReplace> repList;

	public List<ViewGoodsReplace> getRepList() {
		return repList;
	}

	public void setRepList(List<ViewGoodsReplace> repList) {
		this.repList = repList;
	}

	private CtGoodsManager goodsManager;
	private CtGoodsTypeManager typeManager;
	private CtResourceManager resourceManager;
	private CtGoodsAttributeManager attributeManager;
	private CtGoodsLinkManager goodslinkManager;
	private CtGoodsReplaceManager goodsreplaceManager;
	private CtGoodsARManager goodsarManager;
	private CtDepotManager depotManager;
	private CtGoodsNumManager goodsNumManager;
	private CtGoodsDetailManager goodsDetailManager;
	private CtGoodsGroupRelationManager relationManager;
	private CtGoodsImgManager imgManager;
	public CtGoodsImgManager getImgManager() {
		return imgManager;
	}

	public void setImgManager(CtGoodsImgManager imgManager) {
		this.imgManager = imgManager;
	}

	private Map<String, Object> request;
	private Page page;
	private CtGoods goods;
	private CtGoodsDTO goodsDTO = new CtGoodsDTO();
	private String filePath;
	private CtGoodsGroupRelation groupRelation;
	private String result;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public CtGoodsGroupRelation getGroupRelation() {
		return groupRelation;
	}

	public void setGroupRelation(CtGoodsGroupRelation groupRelation) {
		this.groupRelation = groupRelation;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public CtGoodsDTO getGoodsDTO() {
		return goodsDTO;
	}

	public void setGoodsDTO(CtGoodsDTO goodsDTO) {
		this.goodsDTO = goodsDTO;
	}

	public List<CtGoods> getGoodsList() {
		return goodsList;
	}

	public void setGoodsList(List<CtGoods> goodsList) {
		this.goodsList = goodsList;
	}

	public CtGoodsManager getGoodsManager() {
		return goodsManager;
	}

	public void setGoodsManager(CtGoodsManager goodsManager) {
		this.goodsManager = goodsManager;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtGoods getGoods() {
		return goods;
	}

	public void setGoods(CtGoods goods) {
		this.goods = goods;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public CtGoodsTypeManager getTypeManager() {
		return typeManager;
	}

	public void setTypeManager(CtGoodsTypeManager typeManager) {
		this.typeManager = typeManager;
	}

	public CtResourceManager getResourceManager() {
		return resourceManager;
	}

	public void setResourceManager(CtResourceManager resourceManager) {
		this.resourceManager = resourceManager;
	}

	public CtGoodsAttributeManager getAttributeManager() {
		return attributeManager;
	}

	public void setAttributeManager(CtGoodsAttributeManager attributeManager) {
		this.attributeManager = attributeManager;
	}

	public CtGoodsLinkManager getGoodslinkManager() {
		return goodslinkManager;
	}

	public void setGoodslinkManager(CtGoodsLinkManager goodslinkManager) {
		this.goodslinkManager = goodslinkManager;
	}

	public CtGoodsReplaceManager getGoodsreplaceManager() {
		return goodsreplaceManager;
	}

	public void setGoodsreplaceManager(CtGoodsReplaceManager goodsreplaceManager) {
		this.goodsreplaceManager = goodsreplaceManager;
	}

	public CtGoodsARManager getGoodsarManager() {
		return goodsarManager;
	}

	public void setGoodsarManager(CtGoodsARManager goodsarManager) {
		this.goodsarManager = goodsarManager;
	}

	public CtDepotManager getDepotManager() {
		return depotManager;
	}

	public void setDepotManager(CtDepotManager depotManager) {
		this.depotManager = depotManager;
	}

	public CtGoodsNumManager getGoodsNumManager() {
		return goodsNumManager;
	}

	public void setGoodsNumManager(CtGoodsNumManager goodsNumManager) {
		this.goodsNumManager = goodsNumManager;
	}

	public CtGoodsDetailManager getGoodsDetailManager() {
		return goodsDetailManager;
	}

	public void setGoodsDetailManager(CtGoodsDetailManager goodsDetailManager) {
		this.goodsDetailManager = goodsDetailManager;
	}

	public CtGoodsGroupRelationManager getRelationManager() {
		return relationManager;
	}

	public void setRelationManager(CtGoodsGroupRelationManager relationManager) {
		this.relationManager = relationManager;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.goodsDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public String list() {
		try {
			page = new Page();
			if (goodsDTO.getPage() == 0) {
				goodsDTO.setPage(1);
			}
			page.setCurrentPage(goodsDTO.getPage());
			this.goodsList = goodsManager.loadAll(page);
			boolean isflag = false;
			for (int i = 0; i < goodsList.size(); i++) {
				try {
					JSONObject json = readJsonFromUrl("http://stock.ctelec.cn/server/post1.ashx?action=kcquery&fnumber="+goodsList.get(i).getGSn()+"&fstocknumber=1"); 
					System.out.println(json.toString()); 
					String kc = json.get("kc").toString();
					if(kc.equals("-1")){
						goodsList.get(i).setDianZiKc("无库存");
					} else {
						goodsList.get(i).setDianZiKc(new DecimalFormat("0.000").format(Double.valueOf(kc)));
					}
					System.out.println(json.get("kc"));
				} catch (Exception e) {
//					goodsList.get(i).setDianZiKc("获取电子库存失败");
					e.printStackTrace();
					isflag = true;
					break;
				} 
			}
			if(isflag){
				for (int i = 0; i < goodsList.size(); i++) {
					goodsList.get(i).setDianZiKc("获取电子库存失败");
				}
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	private static String readAll(Reader rd) throws IOException { 
	    StringBuilder sb = new StringBuilder(); 
	    int cp; 
	    while ((cp = rd.read()) != -1) { 
	      sb.append((char) cp); 
	    } 
	    return sb.toString(); 
	  } 
	  
	  public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException { 
	    InputStream is = new URL(url).openStream(); 
	    try { 
	      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("GBK"))); 
	      String jsonText = readAll(rd); 
	      JSONObject json = new JSONObject(jsonText); 
	      return json; 
	    } finally { 
	      is.close(); 
	     // System.out.println("同时 从这里也能看出 即便return了，仍然会执行finally的！"); 
	    } 
	  } 
	  

	
	//查询商品数据
	public String search1(){
		try {
			page = new Page();
			if (goodsDTO.getPage() == 0) {
				goodsDTO.setPage(1);
			}
			page.setCurrentPage(goodsDTO.getPage());
			String keySerach = goodsDTO.getKeyword();
			if(keySerach != null && !keySerach.equals("") && !keySerach.equals("请输入你要查询的关键词")){
				this.goodsList = goodsManager.findGoodsByKey(page, keySerach);
			} else {
				this.goodsList = goodsManager.findGoodsByKey(page, null);
			}
			boolean isflag = false;
			for (int i = 0; i < goodsList.size(); i++) {
				try {
					JSONObject json = readJsonFromUrl("http://stock.ctelec.cn/server/post1.ashx?action=kcquery&fnumber="+goodsList.get(i).getGSn()+"&fstocknumber=1"); 
					System.out.println(json.toString()); 
					String kc = json.get("kc").toString();
					if(kc.equals("-1")){
						goodsList.get(i).setDianZiKc("无库存");
					} else {
						goodsList.get(i).setDianZiKc(new DecimalFormat("0.000").format(Double.valueOf(kc)));
					}
					System.out.println(json.get("kc"));
				} catch (Exception e) {
//					goodsList.get(i).setDianZiKc("获取电子库存失败");
					e.printStackTrace();
					isflag = true;
					break;
				} 
			}
			if(isflag){
				for (int i = 0; i < goodsList.size(); i++) {
					goodsList.get(i).setDianZiKc("获取电子库存失败");
				}
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			this.request.put("key", keySerach.toUpperCase());
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
	
	// 查询
	public String search() {
		try {
			page = new Page();
			if (goodsDTO.getPage() == 0) {
				goodsDTO.setPage(1);
			}
			page.setCurrentPage(goodsDTO.getPage());
			this.GId = goodsDTO.getGId();
			Long GGId = goodsDTO.getGGId();
			if (goodsDTO.getKeyword() != null
					&& !goodsDTO.getKeyword().equals("请输入你要查的关键词")) {
				this.goodsList = goodsManager.findAll(goodsDTO.getKeyword(),
						page);
			} else {
				this.goodsList = goodsManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			this.request.put("GGId", GGId);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 跳转详情页
	public String detail() {
		try {
			Long id = goodsDTO.getGId();
			this.goodsDetailList = goodsManager.queryDetail(id);
			// this.goodsDetail = goodsDetailList.get(0);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 跳转添加页面
	public String toadd() {
		try {
			Long parid = 0l;
			this.categoryList = goodsManager.queryCategory(parid);
			this.brandList = goodsManager.queryBrand();
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 根据parentID取分类
	public void getCate(Long parid) {
		this.categoryList = goodsManager.queryCategory(parid);
	}

	// 逻辑删除
	public String hideGoods() {
		try {
			for (Long m : goodsDTO.getMid()) {
				goodsManager.hide(m);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 物理删除
	public String delGoods() {
		try {
			for (Long m : goodsDTO.getMid()) {
				goodsManager.delete(m);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 跳转回收站
	public String toDel() {
		try {
			this.goodsList = goodsManager.queryAll("1");
			Collections.reverse(goodsList);
			querylist = CommonFunc.getPage(goodsList, getPagebean());
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 清空回收站
	public String empty() {
		try {
			goodsManager.empty();
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 跳转资料种类绑定页面
	public String toTieRnT() {
		try {
			Long id = goodsDTO.getGId();
			this.goods = goodsManager.findById(id);
			page = new Page();

			if (page == null)
				page.setCurrentPage(0);

			this.resList = resourceManager.queryAll(page);
			this.viewresList = resourceManager.queryByGoodsID(id);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 绑定操作
	public String tieRnT() {
		try {
			CtGoodsResource res = new CtGoodsResource();
			Long gid = goodsDTO.getGId();
			Long rid = goodsDTO.getRId();
			res.setGId(gid);
			res.setResourceId(rid);
			res.setGoodsType("0");
			goodsManager.tieRes(res);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 删除绑定
	public String unTieRnT() {
		try {
			Long rid = goodsDTO.getRId();
			Long gid = goodsDTO.getGId();
			goodsManager.unTieRes(gid, rid);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 跳转选择类型
	public String toTieT() {
		try {
			Long id = goodsDTO.getGId();
			this.attList = attributeManager.queryByGId(id);
			this.goods = goodsManager.findById(id);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 跳转属性编辑
	public String toTieA() {
		try {
			Long gid = goodsDTO.getGId();
			Long tid = goodsDTO.getGTId();
			attributeManager.delAttRelation(gid);
			this.typeList = typeManager.queryAll();
			this.goods = goodsManager.findById(gid);
			if (tid != null) {
				this.attributeList = attributeManager.queryByTID(tid);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 保存属性
	public String tieA() {
	    try
	    {
	      Long gid = this.goodsDTO.getGId();
	      String val = this.goodsDTO.getMval();
	      String[] list = val.split(",");
	      int index = 0;
	      Long[] arrayOfLong;
	      int j = (arrayOfLong = this.goodsDTO.getMid()).length;
	      for (int i = 0; i < j; i++)
	      {
	        Long m = arrayOfLong[i];
	        CtGoodsAttributeRelation relation = new CtGoodsAttributeRelation();
	        relation.setGId(gid);
	        relation.setAttrId(m);
	        String s = list[index];
	        relation.setAttrValue(s);
	        relation.setTag("0");
	        this.goodsarManager.save(relation);
	        index++;
	      }
	      return "success";
	    }
	    catch (Exception e)
	    {
	      e.printStackTrace();
	    }
	    return "error";
	}

	// 跳转替代
	public String toReplace() {
		try {
			Long gid = goodsDTO.getGId();
			this.goods = goodsManager.findById(gid);
			this.repList = goodsreplaceManager.queryRepList(gid);
			page = new Page();
			if (goodsDTO.getPage() == 0) {
				goodsDTO.setPage(1);
			}
			page.setCurrentPage(goodsDTO.getPage());
			if (goodsDTO.getKeyword() != null
					&& !goodsDTO.getKeyword().equals("请输入你要查的关键词")) {
				this.goodsList = goodsManager.findAll(goodsDTO.getKeyword(),
						page);
			} else {
				this.goodsList = goodsManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 跳转关联
	public String toLink() {
		try {
			Long gid = goodsDTO.getGId();
			this.goods = goodsManager.findById(gid);
			this.linkList = goodslinkManager.queryTied(gid);
			page = new Page();
			if (goodsDTO.getPage() == 0) {
				goodsDTO.setPage(1);
			}
			page.setCurrentPage(goodsDTO.getPage());
			if (goodsDTO.getKeyword() != null
					&& !goodsDTO.getKeyword().equals("请输入你要查的关键词")) {
				this.goodsList = goodsManager.findAll(goodsDTO.getKeyword(),
						page);
			} else {
				this.goodsList = goodsManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String unLink() {
		try {
			Long gid = goodsDTO.getGId();
			Long lid = goodsDTO.getLinkGId();
			goodslinkManager.unLink(gid, lid);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String unReplace() {
		try {
			Long gid = goodsDTO.getGId();
			Long lid = goodsDTO.getLinkGId();
			goodsreplaceManager.unReplace(gid, lid);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 绑定替代
	public String Replace() {
		try {
			Long gid = goodsDTO.getGId();
			String dblk = goodsDTO.getDblk();
			for (Long m : goodsDTO.getMid()) {
				CtGoodsReplace replace = new CtGoodsReplace();
				replace.setGId(gid);
				replace.setReplaceGId(m);
				replace.setIsDouble(dblk);
				goodsreplaceManager.tieReplace(replace);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 绑定关联
	public String Link() {
		try {
			Long gid = goodsDTO.getGId();
			String dblk = goodsDTO.getDblk();
			for (Long m : goodsDTO.getMid()) {
				CtGoodsLink link = new CtGoodsLink();
				link.setGId(gid);
				link.setLinkGId(m);
				link.setIsDouble(dblk);
				goodslinkManager.tieLink(link);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 跳转库存管理
	public String toDepot() {
		try {
			Long gid = goodsDTO.getGId();
			this.goods = goodsManager.findById(gid);
			this.depotList = depotManager.queryAll();
			this.querylist = goodsNumManager.testQuery(gid);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 添加仓库
	public String addDepot() {
		try {
			Long gid = goodsDTO.getGId();
			Integer depid = goodsDTO.getDepotId();
			CtGoodsNum num = new CtGoodsNum();
			num.setGId(gid);
			num.setDepotId(depid);
			num.setGNum(0);
			goodsNumManager.tieDepot(num);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 更新库存
	public String updateNum() {
		try {
			Long gid = goodsDTO.getGId();
			String val = goodsDTO.getMval();
			Integer[] deid = goodsDTO.getDeid();
			String[] list1 = val.split(",");
			int index = 0;
			for (Long m : goodsDTO.getMid()) {
				CtGoodsNum num = new CtGoodsNum();
				num.setGId(gid);
				num.setId(m);
				num.setDepotId(deid[index]);
				String s = list1[index].trim();
				num.setGNum(Integer.parseInt(s));
				goodsNumManager.updateNum(num);
				index = index + 1;
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 跳转编辑详细描述
	public String toEditDetl() {
		try {
			Long gid = goodsDTO.getGId();
			this.goods = goodsManager.findById(gid);
			this.detailList = goodsDetailManager.queryByGid(gid);
			if (detailList == null || detailList.size() <= 0) {
				CtGoodsDetail detail = new CtGoodsDetail();
				detail.setGId(gid);
				goodsDetailManager.addDetl(detail);
				this.goodsDetail = goodsDetailManager.queryByGid(gid).get(0);
			} else {
				this.goodsDetail = detailList.get(0);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 更新详细描述
	public String editDetl() {
		try {
			Long gid = goodsDTO.getGId();
			Long did = goodsDTO.getDetlId();
			String value = goodsDTO.getGDetail();
			CtGoodsDetail detail = new CtGoodsDetail();
			detail.setDetlId(did);
			detail.setGId(gid);
			detail.setGDetail(value);
			goodsDetailManager.updateDetl(detail);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 添加到商品组
	public String addtoGroup() {
		try {
			Long num = null;
			CtGoodsGroupRelation m = new CtGoodsGroupRelation();
			Long GGId = goodsDTO.getGGId();
			this.relationManager.delete(GGId);
			String ss = goodsDTO.getUIds();
			String[] str = ss.split("@");
			for (int i = 0; i < str.length; i++) {
				Long id = Long.valueOf(str[i]);
				m.setGGId(GGId);
				m.setGId(id);
				num = this.relationManager.save(m);
			}
			if (num > 0) {
				result = "success";
			} else {
				result = "fail";
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 添加商品
	public String add() {
		try {
			CtGoods gs = new CtGoods();
			gs.setGName(goodsDTO.getGName());
			gs.setCId(goodsDTO.getCId());
			gs.setGSn(goodsDTO.getGSn());
			gs.setBId(goodsDTO.getBId());
			gs.setMarketPrice(goodsDTO.getMarketPrice());
			gs.setShopPrice(goodsDTO.getShopPrice());
			gs.setPromotePrice(goodsDTO.getPromotePrice());
			gs.setIsOnSale(goodsDTO.getIsOnSale());
			gs.setGKeywords(goodsDTO.getGKeywords());
			gs.setTokenCredit(goodsDTO.getTokenCredit());
			gs.setGiveCredit(goodsDTO.getGiveCredit());
			gs.setGUnit(goodsDTO.getGUnit());
			gs.setPack1(goodsDTO.getPack1());
			gs.setPack1Num(goodsDTO.getPack1Num());
			gs.setPack2(goodsDTO.getPack2());
			gs.setPack2Num(goodsDTO.getPack2Num());
			gs.setPack3(goodsDTO.getPack3());
			gs.setPack3Num(goodsDTO.getPack3Num());
			gs.setPack4(goodsDTO.getPack4());
			gs.setPack4Num(goodsDTO.getPack4Num());
			gs.setIsDel("0");
			this.goodsManager.save(gs);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String exportGoods() {
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/binary;charset=UTF-8");
			try {
				ServletOutputStream outputStream = response.getOutputStream();
				String fileName = new String(("商品表").getBytes("GBK"),
						"ISO-8859-1");
				response.setHeader("Content-disposition",
						"attachment; filename=" + fileName + ".xls");// 组装附件名称和格式
				String[] titles = { "商品ID", "商品名称", "分类ID", "商品货号", "品牌ID",
						"市场价", "本店售价", "促销价", "上架", "商品关键字", "可抵用积分数", "赠送积分数",
						"单位", "包装1名称", "包装1数量", "包装2名称", "包装2数量", "包装3名称",
						"包装3数量", "包装4名称", "包装4数量" };
				exportExcel(titles, outputStream);
				return null;
			} catch (IOException e) {
				e.printStackTrace();
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	  public String importGoods()
	  {
	    try
	    {
	      try
	      {
	        FileInputStream si = new FileInputStream(this.goodsDTO.getExcelFile());
	        XSSFWorkbook wb1 = null;
	        HSSFWorkbook wb2 = null;
	        Sheet sheet = null;
	        try {
	        	wb2 = new HSSFWorkbook(si); 
	        	sheet = wb2.getSheetAt(0);
			} catch (Exception e) {
				e.printStackTrace();
				wb1 = new XSSFWorkbook(si);
				sheet = wb1.getSheetAt(0);
			}
	        
	        int rowNum = sheet.getLastRowNum() + 1;
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
	        
	        Row row1 = sheet.getRow(0);
	        int cellNum = row1.getLastCellNum();
	        
	        Long[] attId = new Long[cellNum - 42];
	        String[] attValue = new String[cellNum - 42];
	        for (int n = 43; n <= cellNum; n++)
	        {
	          Cell cell1 = row1.getCell(n - 1);
	          String a = cell1.getStringCellValue();
	          Long b = Long.valueOf(a);
	          attId[(n - 43)] = b;
	        }
	        for (int i = 1; i < rowNum; i++)
	        {
	          CtRangePrice rp1 = new CtRangePrice();
	          CtRangePrice rp2 = new CtRangePrice();
	          CtRangePrice rp3 = new CtRangePrice();
	          CtGoods goods = new CtGoods();
	          Row row = sheet.getRow(i);
	          
	          goods = new CtGoods();
	          if ((row.getCell(1).getStringCellValue() == null) || 
	            (row.getCell(1).getStringCellValue().equals("")))
	          {
	            System.out.println("第" + i + "行" + "商品标题为空值！此行数据未导入！");
	          }
	          else if ((row.getCell(2).getStringCellValue() == null) || 
	            (row.getCell(2).getStringCellValue().equals("")))
	          {
	            System.out.println("第" + i + "行" + "商品分类为空值！此行数据未导入！");
	          }
	          else if ((row.getCell(3).getStringCellValue() == null) || 
	            (row.getCell(3).getStringCellValue().equals("")))
	          {
	            System.out.println("第" + i + "行" + "商品名称为空值！此行数据未导入！");
	          }
	          else if ((row.getCell(5).getStringCellValue() == null) || 
	            (row.getCell(5).getStringCellValue().equals("")))
	          {
	            System.out.println("第" + i + "行" + "商品品牌为空值！此行数据未导入！");
	          }
	          else
	          {
	            for (int j = 0; j < cellNum; j++)
	            {
	              Cell cell = row.getCell(j);
	              String cellValue = "";
	              if (cell != null)
	              {
	                if (cell.getCellType() == 1) {
	                  cellValue = cell.getStringCellValue();
	                } else if (cell.getCellType() == 0) {
	                  if (HSSFDateUtil.isCellDateFormatted(cell))
	                  {
	                    Date d = cell.getDateCellValue();
	                    DateFormat formater = new SimpleDateFormat(
	                      "yyyy-MM-dd hh:mm");
	                    cellValue = formater.format(d);
	                  }
	                  else
	                  {
	                	  System.out.println(j);
	                    cellValue = cell.getStringCellValue();
	                  }
	                }
	                if (!cellValue.equals(""))
	                {
	                  switch (j)
	                  {
	                  case 0: 
	                    goods.setTname(cellValue);
	                    break;
	                  case 1: 
	                    goods.setCId(Long.valueOf(cellValue));
	                    break;
	                  case 2: 
	                    goods.setuName(cellValue);
	                    goods.setGName(cellValue);
	                    break;
	                  case 3: 
	                    goods.setGSn(cellValue);
	                    break;
	                  case 4: 
	                    goods.setBId(Long.valueOf(cellValue));
	                    break;
	                  case 5: 
	                    goods.setSeries(cellValue);
	                    break;
	                  case 6: 
	                    goods.setAlw(cellValue);
	                    break;
	                  case 7: 
	                    goods.setSection(cellValue);
	                    break;
	                  case 8: 
	                    goods.setMarketPrice(
	                      Double.valueOf(cellValue));
	                    break;
	                  case 9: 
	                    goods.setShopPrice(
	                      Double.valueOf(cellValue));
	                    break;
	                  case 10: 
	                    goods.setPromotePrice(
	                      Double.valueOf(cellValue));
	                    break;
	                  case 11: 
	                    goods.setIsSample(cellValue);
	                    break;
	                  case 12: 
	                    goods.setIsPartial(cellValue);
	                    break;
	                  case 13: 
	                    goods.setIsOnSale(cellValue);
	                    break;
	                  case 14: 
	                    goods.setGKeywords(cellValue);
	                    break;
	                  case 15: 
	                    goods.setGUnit(cellValue);
	                    break;
	                  case 16: 
	                    goods.setPack1(cellValue);
	                    break;
	                  case 17: 
	                    goods.setPack1Num(
	                      Integer.valueOf(cellValue));
	                    break;
	                  case 18: 
	                    goods.setPack2(cellValue);
	                    break;
	                  case 19: 
	                    goods.setPack2Num(
	                      Integer.valueOf(cellValue));
	                    break;
	                  case 20: 
	                    goods.setPack3(cellValue);
	                    break;
	                  case 21: 
	                    goods.setPack3Num(
	                      Integer.valueOf(cellValue));
	                    break;
	                  case 22: 
	                    goods.setPack4(cellValue);
	                    break;
	                  case 23: 
	                    goods.setPack4Num(
	                      Integer.valueOf(cellValue));
	                    break;
	                  case 24: 
	                    rp1.setSimSNum(
	                      Integer.valueOf(cellValue));
	                    break;
	                  case 25: 
	                    rp1.setSimENum(
	                      Integer.valueOf(cellValue));
	                    break;
	                  case 26: 
	                    rp1.setSimRPrice(
	                      Double.valueOf(cellValue));
	                    break;
	                  case 27: 
	                    rp1.setSimIncrease(
	                      Integer.valueOf(cellValue));
	                    break;
	                  case 28: 
	                    rp2.setSimSNum(
	                      Integer.valueOf(cellValue));
	                    break;
	                  case 29: 
	                    rp2.setSimENum(
	                      Integer.valueOf(cellValue));
	                    break;
	                  case 30: 
	                    rp2.setSimRPrice(
	                      Double.valueOf(cellValue));
	                    break;
	                  case 31: 
	                    rp2.setSimIncrease(
	                      Integer.valueOf(cellValue));
	                    break;
	                  case 32: 
	                    rp3.setSimSNum(
	                      Integer.valueOf(cellValue));
	                    break;
	                  case 33: 
	                    rp3.setSimENum(
	                      Integer.valueOf(cellValue));
	                    break;
	                  case 34: 
	                    rp3.setSimRPrice(
	                      Double.valueOf(cellValue));
	                    break;
	                  case 35: 
	                    rp3.setSimIncrease(
	                      Integer.valueOf(cellValue));
	                    break;
	                  case 36: 
	                	  Double snum = cellValue == null ? null : Double.valueOf(cellValue) * 1000;
	                    rp1.setParSNum(
	                      Integer.valueOf(snum != null ?new DecimalFormat("#.##").format(snum):null));
	                    break;
	                  case 37: 
	                	  Double enums = cellValue == null ? null : Double.valueOf(cellValue) * 1000;
	                    rp1.setParENum(
	                      Integer.valueOf(enums != null ?new DecimalFormat("#.##").format(enums):null));
	                    break;
	                  case 38: 
	                    rp1.setParRPrice(Double.valueOf(cellValue));
	                    break;
	                  case 39: 
	                	  Double inc = Double.valueOf(cellValue) * 1000;
	                    rp1.setParIncrease(
	                      Integer.valueOf(new DecimalFormat("#.##").format(inc)));
	                    break;
	                  case 40: 
	                    goods.setGnum(Long.valueOf(cellValue));
	                  }
	                  if (j >= 42) {
	                    attValue[(j - 42)] = cellValue;
	                  }
	                }
	              }
	            }
	            goods.setIsDel("0");
	            goods.setTokenCredit(
	              Long.valueOf(0L));
	            goods.setGiveCredit(
	              Long.valueOf(0L));
	            CtGoods goo = goodsManager.getGoodsByGnameNoLike(goods.getGName());
	            Long gtid = null;
	            boolean isCun = false;
		          if(goo != null){
		        	  //商品已存在的
		        	  goodsManager.deleteRenPriceByGid(goo.getGId());
		        	  gtid = this.goodsManager.save(goo);
		        	  isCun = true;
		        	  System.out.println("已存在商品 UPDATE");
		          } else {
		        	  gtid = this.goodsManager.save(goods);
		        	  isCun = false;
		          }
	            
	            rp1.setGId(gtid);
	            rp2.setGId(gtid);
	            rp3.setGId(gtid);
	            rp1.setInsTime(sdf.format(new Date()));
	            rp2.setInsTime(sdf.format(new Date()));
	            rp3.setInsTime(sdf.format(new Date()));
	            if(rp1.getSimSNum() != null || rp1.getParSNum() != null){
	            	this.goodsManager.saveRangePrice(rp1);
	            }
	            if(rp2.getSimSNum() != null || rp2.getParSNum() != null){
	            	this.goodsManager.saveRangePrice(rp2);
	            }
	            if(rp3.getSimSNum() != null || rp3.getParSNum() != null){
	            	this.goodsManager.saveRangePrice(rp3);
	            }
	            for (int m = 42; m < cellNum; m++) {
	            	if ((attValue[(m - 42)] != null) && (attValue[(m - 42)] != ""))
	            	{
	            		CtGoodsAttributeRelation relation = new CtGoodsAttributeRelation();
	            		relation = relationManager.findRelByGidAndAttrId(gtid, attId[(m - 42)]);
	            		if(relation == null){
	            			relation = new CtGoodsAttributeRelation();
	            		}
	            		relation.setGId(gtid);
	            		relation.setAttrId(attId[(m - 42)]);
	            		relation.setAttrValue(attValue[(m - 42)]);
	            		relation.setTag("0");
	            		this.goodsarManager.save(relation);
	            	}
	            }
	          }
	        }
	      }
	      catch (FileNotFoundException e)
	      {
	        e.printStackTrace();
	      }
	      catch (IOException e)
	      {
	        e.printStackTrace();
	      }
	      return "success";
	    }
	    catch (Exception e)
	    {
	      e.printStackTrace();
	    }
	    return "error";
	  }
	
	//获取一个Goods
		public String getOneGoodsByGname(){
			try {
				String Gname = this.goodsDTO.getGName();
				Object cg = this.goodsManager.getOneGoodsByGname(Gname);
				if(cg == null){
					List<String> l = new ArrayList<String>();
					l.add("error");
					JSONArray json = new JSONArray();
					json = JSONArray.fromObject(l);
					resultJson = json;
				}else {
					JSONArray json = new JSONArray();
					json = JSONArray.fromObject(cg);
					resultJson = json;
				}
				return SUCCESS;
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				return "error";
			}

		}

		
		//获取Goods列表
		public String getGoodsByGname(){
			try{
			String Gname = this.goodsDTO.getGName();
			List<CtGoods> list = this.goodsManager.getGoodsByGname(Gname);
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(list);
			resultJson = json;
			return SUCCESS;
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				return "error";
			}
		}
		
		
	public void exportExcel(String[] titles, ServletOutputStream outputStream) {
		goodsList = goodsManager.queryAll("0");
		// 创建一个workbook 对应一个excel应用文件
		XSSFWorkbook workBook = new XSSFWorkbook();
		// 在workbook中添加一个sheet,对应Excel文件中的sheet
		XSSFSheet sheet = workBook.createSheet("用户表");
		ExportUtil exportUtil = new ExportUtil(workBook, sheet);
		XSSFCellStyle headStyle = exportUtil.getHeadStyle();
		XSSFCellStyle bodyStyle = exportUtil.getBodyStyle();
		// 构建表头
		XSSFRow headRow = sheet.createRow(0);
		XSSFCell cell = null;
		for (int i = 0; i < titles.length; i++) {
			cell = headRow.createCell(i);
			cell.setCellStyle(headStyle);
			cell.setCellValue(titles[i]);
		}
		int j = 0;
		// 构建表体数据
		for (CtGoods gs : goodsList) {

			XSSFRow bodyRow = sheet.createRow(j + 1);
			cell = bodyRow.createCell(0);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getGId() ? "" : gs.getGId().toString());

			cell = bodyRow.createCell(1);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getGName() ? "" : gs.getGName()
					.toString());

			cell = bodyRow.createCell(2);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getCId() ? "" : gs.getCId().toString());

			cell = bodyRow.createCell(3);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getGSn() ? "" : gs.getGSn().toString());

			cell = bodyRow.createCell(4);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getBId() ? "" : gs.getBId().toString());

			cell = bodyRow.createCell(5);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getMarketPrice() ? "" : gs
					.getMarketPrice().toString());

			cell = bodyRow.createCell(6);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getShopPrice() ? "" : gs
					.getShopPrice().toString());

			cell = bodyRow.createCell(7);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getPromotePrice() ? "" : gs
					.getPromotePrice().toString());

			cell = bodyRow.createCell(8);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getIsOnSale() ? "" : gs.getIsOnSale()
					.toString());

			cell = bodyRow.createCell(9);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getGKeywords() ? "" : gs
					.getGKeywords().toString());

			cell = bodyRow.createCell(10);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getTokenCredit() ? "" : gs
					.getTokenCredit().toString());

			cell = bodyRow.createCell(11);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getGiveCredit() ? "" : gs
					.getGiveCredit().toString());

			cell = bodyRow.createCell(12);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getGUnit() ? "" : gs.getGUnit()
					.toString());

			cell = bodyRow.createCell(13);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getPack1() ? "" : gs.getPack1()
					.toString());

			cell = bodyRow.createCell(14);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getPack1Num() ? "" : gs.getPack1Num()
					.toString());

			cell = bodyRow.createCell(15);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getPack2() ? "" : gs.getPack2()
					.toString());

			cell = bodyRow.createCell(16);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getPack2Num() ? "" : gs.getPack2Num()
					.toString());

			cell = bodyRow.createCell(17);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getPack3() ? "" : gs.getPack3()
					.toString());

			cell = bodyRow.createCell(18);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getPack3Num() ? "" : gs.getPack3Num()
					.toString());

			cell = bodyRow.createCell(19);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getPack4() ? "" : gs.getPack4()
					.toString());

			cell = bodyRow.createCell(20);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(null == gs.getPack4Num() ? "" : gs.getPack4Num()
					.toString());

			j++;
		}
		try {
			workBook.write(outputStream);
			outputStream.flush();
			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
	private CtFreeSampleReg freeSampleReg;
	private List<CtFreeSampleReg> freeSampleRegList = new ArrayList<CtFreeSampleReg>();
	
	public CtFreeSampleReg getFreeSampleReg() {
		return freeSampleReg;
	}

	public void setFreeSampleReg(CtFreeSampleReg freeSampleReg) {
		this.freeSampleReg = freeSampleReg;
	}

	public List<CtFreeSampleReg> getFreeSampleRegList() {
		return freeSampleRegList;
	}

	public void setFreeSampleRegList(List<CtFreeSampleReg> freeSampleRegList) {
		this.freeSampleRegList = freeSampleRegList;
	}

	public String samRegList(){
		this.page = new Page();
		if (this.goodsDTO.getPage() == 0) {
		  this.goodsDTO.setPage(1);
		}
		this.page.setCurrentPage(this.goodsDTO.getPage());
		freeSampleRegList = goodsManager.getFreeSamRegByPage(page);
		this.page.setTotalPage(this.page.getTotalPage());
		this.request.put("pages", this.page);
		return SUCCESS;
	}
	public String regListHui(){
		goodsManager.updateSamBySamId(freeSampleReg.getSamId());
		return SUCCESS;
	}
	
	private CtReplace replace = new CtReplace();
	private List<CtReplace> replacesList = new ArrayList<CtReplace>();
	
	public CtReplace getReplace() {
		return replace;
	}

	public void setReplace(CtReplace replace) {
		this.replace = replace;
	}

	public List<CtReplace> getReplacesList() {
		return replacesList;
	}

	public void setReplacesList(List<CtReplace> replacesList) {
		this.replacesList = replacesList;
	}

	//商品替换词管理
	public String replaceInfo(){
		this.page = new Page();
		if (this.goodsDTO.getPage() == 0) {
		  this.goodsDTO.setPage(1);
		}
		this.page.setCurrentPage(this.goodsDTO.getPage());
		replacesList = goodsManager.findReplaceByPage(page);
		this.page.setTotalPage(this.page.getTotalPage());
		this.request.put("pages", this.page);
		return SUCCESS;
	}

	//添加商品替换词
	public String addSearchKeyReplace(){
		System.out.println(goodsDTO);
		if(goodsDTO.getRe1() != null){
			replace.setRe1(goodsDTO.getRe1().toUpperCase());
		}
		if(goodsDTO.getRe2() != null){
			replace.setRe2(goodsDTO.getRe2().toUpperCase());
		}
		if(goodsDTO.getRe3() != null){
			replace.setRe3(goodsDTO.getRe3().toUpperCase());
		}
		if(goodsDTO.getRe4() != null){
			replace.setRe4(goodsDTO.getRe4().toUpperCase());
		}
		if(goodsDTO.getRe5() != null){
			replace.setRe5(goodsDTO.getRe5().toUpperCase());
		}
		goodsManager.saveReplace(replace);
		return SUCCESS;
	}
	//查询指定替换词
	public String findGoodsSearchKey(){
		Integer id = goodsDTO.getId().intValue();
		replace = goodsManager.findSearchKeyById(id);
		goodsDTO.setReplace(replace);
		System.out.println(id);
		return SUCCESS;
	}
	//修改指定替换词
	public String updateSearchKeyReplace(){
		Integer id = goodsDTO.getId().intValue();
		String re1 = goodsDTO.getRe1();
		String re2 = goodsDTO.getRe2();
		String re3 = goodsDTO.getRe3();
		String re4 = goodsDTO.getRe4();
		String re5 = goodsDTO.getRe5();
		replace.setRe1(re1.toUpperCase());
		replace.setRe2(re2.toUpperCase());
		replace.setRe3(re3.toUpperCase());
		replace.setRe4(re4.toUpperCase());
		replace.setRe5(re5.toUpperCase());
		replace.setReId(id);
		goodsManager.saveReplace(replace);
		return SUCCESS;
	}
	//删除替换词
	public String deleteSearchKeyReplace(){
		for (Long m : goodsDTO.getMid()) {
			replace = goodsManager.findSearchKeyById(m.intValue());
			if(replace != null){
				goodsManager.deleteReplace(replace);
			}
		}
		return SUCCESS;
	}
	//查询替换词
	public String searchReplace(){
		String key = goodsDTO.getKeyword();
		this.page = new Page();
		if (this.goodsDTO.getPage() == 0) {
		  this.goodsDTO.setPage(1);
		}
		this.page.setCurrentPage(this.goodsDTO.getPage());
		replacesList = goodsManager.findReplaceByKeyAndPage(key, page);
		this.page.setTotalPage(this.page.getTotalPage());
		this.request.put("pages", this.page);
		return SUCCESS;
	}
	private CtSearchCollect searchCollect = new CtSearchCollect();
	private List<CtSearchCollect> searchCollectsList = new ArrayList<CtSearchCollect>();
	
	public CtSearchCollect getSearchCollect() {
		return searchCollect;
	}

	public void setSearchCollect(CtSearchCollect searchCollect) {
		this.searchCollect = searchCollect;
	}

	public List<CtSearchCollect> getSearchCollectsList() {
		return searchCollectsList;
	}

	public void setSearchCollectsList(List<CtSearchCollect> searchCollectsList) {
		this.searchCollectsList = searchCollectsList;
	}

	//查询词为空列表
	public String listSerachCollect(){
		this.page = new Page();
		if (this.goodsDTO.getPage() == 0) {
		  this.goodsDTO.setPage(1);
		}
		this.page.setCurrentPage(this.goodsDTO.getPage());
		searchCollectsList = goodsManager.findSearchByPage(page);
		this.page.setTotalPage(this.page.getTotalPage());
		this.request.put("pages", this.page);
		return SUCCESS;
	}
	//查询词优化
	public String okSearchCollect(){
		Integer coId = goodsDTO.getCoId();
		searchCollect = goodsManager.findCollectById(coId);
		if(searchCollect != null){
			searchCollect.setIsOptimise("1");
			goodsManager.updateCollect(searchCollect);
		}
		result = "success";
		return SUCCESS;
	}
	//删除查询词
	public String deleteSearchCollect(){
		try {
			for (Long m : goodsDTO.getMid()) {
				this.goodsManager.deleteCollect(m);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
	//更新库存
	public String updateGoodsNum(){
		try {
			Solr solr = new Solr();
			System.out.println(goodsDTO);
			Long id = goodsDTO.getGId();
			Integer num = goodsDTO.getGnum();
			goods = goodsManager.findById(id);
			goods.setGnum(Long.valueOf(num.toString()));
			goodsManager.update(goods);
			solr.updateSolrGnum(id.toString(), num.toString());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SolrServerException e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
}
