package org.ctonline.action.goods;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.action.BaseAction;
import org.ctonline.dto.goods.CtGoodsPriceDTO;
import org.ctonline.manager.goods.CtPriceManager;
import org.ctonline.manager.goods.ViewGoodsGroupManager;
import org.ctonline.manager.user.ViewUserGroupManager;
import org.ctonline.po.goods.CtPrice;
import org.ctonline.po.views.ViewGoodsGroup;
import org.ctonline.po.views.ViewGoodsPrice;
import org.ctonline.po.views.ViewUserGroup;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ModelDriven;

public class CtGoodsPriceAction extends BaseAction implements RequestAware,
		ModelDriven<Object> {
	private ViewGoodsGroupManager viewGoodsManager;
	private ViewUserGroupManager viewUserManager;
	private CtPriceManager priceManager;
	private List<ViewGoodsGroup> viewgoodsList;
	private List<ViewUserGroup> viewuserList;
	private List<CtPrice> priceList;
	private List<ViewGoodsPrice> viewPriceList;
	private CtPrice price;
	private Page page;
	private CtGoodsPriceDTO priceDTO = new CtGoodsPriceDTO();
	private Map<String, Object> request;

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.priceDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public List<CtPrice> getPriceList() {
		return priceList;
	}

	public void setPriceList(List<CtPrice> priceList) {
		this.priceList = priceList;
	}

	public CtPrice getPrice() {
		return price;
	}

	public void setPrice(CtPrice price) {
		this.price = price;
	}

	public ViewGoodsGroupManager getViewGoodsManager() {
		return viewGoodsManager;
	}

	public void setViewGoodsManager(ViewGoodsGroupManager viewGoodsManager) {
		this.viewGoodsManager = viewGoodsManager;
	}

	public ViewUserGroupManager getViewUserManager() {
		return viewUserManager;
	}

	public void setViewUserManager(ViewUserGroupManager viewUserManager) {
		this.viewUserManager = viewUserManager;
	}

	public List<ViewGoodsGroup> getViewgoodsList() {
		return viewgoodsList;
	}

	public void setViewgoodsList(List<ViewGoodsGroup> viewgoodsList) {
		this.viewgoodsList = viewgoodsList;
	}

	public List<ViewUserGroup> getViewuserList() {
		return viewuserList;
	}

	public void setViewuserList(List<ViewUserGroup> viewuserList) {
		this.viewuserList = viewuserList;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtGoodsPriceDTO getPriceDTO() {
		return priceDTO;
	}

	public void setPriceDTO(CtGoodsPriceDTO priceDTO) {
		this.priceDTO = priceDTO;
	}

	public CtPriceManager getPriceManager() {
		return priceManager;
	}

	public void setPriceManager(CtPriceManager priceManager) {
		this.priceManager = priceManager;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public List<ViewGoodsPrice> getViewPriceList() {
		return viewPriceList;
	}

	public void setViewPriceList(List<ViewGoodsPrice> viewPriceList) {
		this.viewPriceList = viewPriceList;
	}

	// 查价格表
	public String list() {
		try {
			page = new Page();
			if (priceDTO.getPage() == 0) {
				priceDTO.setPage(1);
			}
			page.setCurrentPage(priceDTO.getPage());
			this.viewPriceList = priceManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			// 查询
			String goodskey = priceDTO.getGoodsKey();
			String userkey = priceDTO.getUserKey();
			if (goodskey != null && goodskey.length() > 0) {
				this.viewgoodsList = viewGoodsManager.query(goodskey);
			}
			if (userkey != null && userkey.length() > 0) {
				this.viewuserList = viewUserManager.query(userkey, userkey);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 删价格
	public String delPrice() {
		try {
			Long pid = priceDTO.getPid();
			priceManager.delPrice(pid);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 查询商品&组
	public List<ViewGoodsGroup> queryGoods(String key) {

		viewgoodsList = viewGoodsManager.query(key);
		return viewgoodsList;
	}

	// 查询用户&组
	public List<ViewUserGroup> queryUser(String key) {

		viewuserList = viewUserManager.query(key, key);
		return viewuserList;
	}

	// 查询价格
	public String checkPrice() {
		try {
			Long uid = priceDTO.getUId();
			String utype = priceDTO.getUType();
			Long gid = priceDTO.getGId();
			String gtype = priceDTO.getGType();
			this.priceList = priceManager.checkPrice(uid, utype, gid, gtype);
			this.price = priceList.get(0);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 更新价格
	public String updatePrice() {
		try {
			CtPrice price = new CtPrice();
			price.setId(priceDTO.getId());
			price.setGId(priceDTO.getGId());
			price.setGType(priceDTO.getGType());
			price.setUId(priceDTO.getUId());
			price.setUType(priceDTO.getUType());
			price.setPrice(priceDTO.getPrice());
			priceManager.updatePrice(price);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 新增价格
	public String addPrice() {
		try {
			String ginfo = priceDTO.getGInfo();
			String uinfo = priceDTO.getUInfo();
			String[] glist = ginfo.split("@");
			Long gid = Long.parseLong(glist[0]);
			String gtype = glist[1];
			if (gtype.equals("组")) {
				gtype = "1";
			} else {
				gtype = "0";
			}
			String[] ulist = uinfo.split("@");
			Long uid = Long.parseLong(ulist[0]);
			String utype = ulist[1];
			if (utype.equals("组")) {
				utype = "1";
			} else {
				utype = "0";
			}
			CtPrice price = new CtPrice();
			price.setGId(gid);
			price.setGType(gtype);
			price.setUId(uid);
			price.setUType(utype);
			price.setPrice(priceDTO.getPrice());
			priceManager.insertPrice(price);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
}
