package org.ctonline.action.goods;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.dto.goods.CtGoodsGroupDTO;
import org.ctonline.manager.goods.CtGoodsGroupManager;
import org.ctonline.manager.goods.CtGoodsGroupRelationManager;
import org.ctonline.manager.goods.CtGoodsManager;
import org.ctonline.po.goods.CtGoodsGroup;
import org.ctonline.po.goods.CtGoodsGroupRelation;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class CtGoodsGroupAction extends ActionSupport implements RequestAware,
		ModelDriven<Object> {

	private List<CtGoodsGroup> goodsGroups;
	private CtGoodsGroupManager goodsGroupManager;
	private Map<String, Object> request;
	private Page page;
	private CtGoodsGroupDTO goodsGroupDTO = new CtGoodsGroupDTO();
	private CtGoodsGroup goodsGroup;
	private List<?> user;
	private List<CtGoodsGroupRelation> groupRelations;
	private CtGoodsGroupRelationManager relationManager;
	private CtGoodsManager goodsManager;

	@Override
	public Object getModel() {

		return this.goodsGroupDTO;
	}

	public List<CtGoodsGroup> getGoodsGroups() {
		return goodsGroups;
	}

	public void setGoodsGroups(List<CtGoodsGroup> goodsGroups) {
		this.goodsGroups = goodsGroups;
	}

	public CtGoodsGroupManager getGoodsGroupManager() {
		return goodsGroupManager;
	}

	public void setGoodsGroupManager(CtGoodsGroupManager goodsGroupManager) {
		this.goodsGroupManager = goodsGroupManager;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtGoodsGroup getGoodsGroup() {
		return goodsGroup;
	}

	public void setGoodsGroup(CtGoodsGroup goodsGroup) {
		this.goodsGroup = goodsGroup;
	}

	public CtGoodsGroupDTO getGoodsGroupDTO() {
		return goodsGroupDTO;
	}

	public void setGoodsGroupDTO(CtGoodsGroupDTO goodsGroupDTO) {
		this.goodsGroupDTO = goodsGroupDTO;
	}

	public List<?> getUser() {
		return user;
	}

	public void setUser(List<?> user) {
		this.user = user;
	}

	public List<CtGoodsGroupRelation> getGroupRelations() {
		return groupRelations;
	}

	public void setGroupRelations(List<CtGoodsGroupRelation> groupRelations) {
		this.groupRelations = groupRelations;
	}

	public CtGoodsGroupRelationManager getRelationManager() {
		return relationManager;
	}

	public void setRelationManager(CtGoodsGroupRelationManager relationManager) {
		this.relationManager = relationManager;
	}

	public CtGoodsManager getGoodsManager() {
		return goodsManager;
	}

	public void setGoodsManager(CtGoodsManager goodsManager) {
		this.goodsManager = goodsManager;
	}

	// 页面显示商品组
	public String list() {
		try {
			page = new Page();
			if (goodsGroupDTO.getPage() == 0) {
				goodsGroupDTO.setPage(1);
			}
			page.setCurrentPage(goodsGroupDTO.getPage());
			this.goodsGroups = goodsGroupManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 按照关键字查找商品组
	public String search() {
		try {
			Page page = new Page();
			if (goodsGroupDTO.getPage() == 0) {
				goodsGroupDTO.setPage(1);
			}
			page.setCurrentPage(goodsGroupDTO.getPage());
			if (goodsGroupDTO.getKeyword() != null
					&& !goodsGroupDTO.getKeyword().equals("请输入你要查的关键词")) {
				this.goodsGroups = goodsGroupManager.findAll(
						goodsGroupDTO.getKeyword(), page);
			} else {
				this.goodsGroups = goodsGroupManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 添加商品组
	public String add() {
		try {
			CtGoodsGroup m = new CtGoodsGroup();
			m.setGGName(goodsGroupDTO.getGGName());
			m.setGGDesc(goodsGroupDTO.getGGDesc());
			Long aa = this.goodsGroupManager.save(m);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 删除商品组
	public String del() {
		try {
			for (Long m : goodsGroupDTO.getMid()) {
				this.goodsGroupManager.delete(m);

			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 修改商品组
	public String update() {
		try {
			CtGoodsGroup m = new CtGoodsGroup();
			m = this.goodsGroupManager.findById(goodsGroupDTO.getId());

			m.setGGName(goodsGroupDTO.getGGName());
			m.setGGDesc(goodsGroupDTO.getGGDesc());
			goodsGroupManager.update(m);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String find() {
		try {
			Long id = goodsGroupDTO.getId();
			goodsGroup = new CtGoodsGroup();
			goodsGroup.setGGId(id);
			this.goodsGroup = this.goodsGroupManager.findById(id);
			goodsGroupDTO.setGGDesc(goodsGroup.getGGDesc());
			goodsGroupDTO.setGGName(goodsGroup.getGGName());

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}

	public String group() {
		try {
			Long GGId = goodsGroupDTO.getGGId();

			this.user = this.goodsManager.selectAll(GGId);

			this.request.put("GGId", GGId);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
}
