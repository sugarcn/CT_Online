package org.ctonline.action.goods;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.dto.goods.CtGoodsCategoryDTO;
import org.ctonline.manager.goods.CtGoodsCategoryManager;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class CtGoodsCategoryAction extends ActionSupport implements
		RequestAware, ModelDriven<Object> {
	private List<CtGoodsCategory> categories;
	private CtGoodsCategoryManager categoryManager;
	private Map<String, Object> request;
	private Page page;
	private CtGoodsCategoryDTO categoryDTO = new CtGoodsCategoryDTO();
	private CtGoodsCategory category;
	private Long parentId;

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public List<CtGoodsCategory> getCategories() {
		return categories;
	}

	public void setCategories(List<CtGoodsCategory> categories) {
		this.categories = categories;
	}

	public CtGoodsCategoryManager getCategoryManager() {
		return categoryManager;
	}

	public void setCategoryManager(CtGoodsCategoryManager categoryManager) {
		this.categoryManager = categoryManager;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.categoryDTO;
	}

	public CtGoodsCategory getCategory() {
		return category;
	}

	public void setCategory(CtGoodsCategory category) {
		this.category = category;
	}

	// 页面显示商品分类
	public String list() {
		try {
			Long id = categoryDTO.getCId();
			Long pid;
			if (id == null || "".equals(id)) {
				pid = (long) 0;

			} else {
				pid = id;
			}
			Page page = new Page();
			if (categoryDTO.getPage() == 0) {
				categoryDTO.setPage(1);
			}
			page.setCurrentPage(categoryDTO.getPage());
			this.categories = categoryManager.searchAll(pid, page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			CtGoodsCategory category = new CtGoodsCategory();
			category.setParentId(pid);
			this.request.put("pid", category);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 按照关键字查询商品分类
	public String search() {
		try {
			Page page = new Page();
			if (categoryDTO.getPage() == 0) {
				categoryDTO.setPage(1);
			}
			page.setCurrentPage(categoryDTO.getPage());
			if (categoryDTO.getKeyword() != null
					&& !categoryDTO.getKeyword().equals("请输入你要查的关键词")) {
				this.categories = categoryManager.findAll(
						categoryDTO.getKeyword(), page);
			} else {
				this.categories = categoryManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 增加商品类别
	public String add() {
		try {
			CtGoodsCategory m = new CtGoodsCategory();
			m.setCName(categoryDTO.getCName());
			m.setParentId(categoryDTO.getParentId());
			m.setFilterAttr(categoryDTO.getFilterAttr());
			List<CtGoodsCategory> cc = categoryManager.levelquery(categoryDTO.getParentId());
			if(cc != null && cc.size() > 0){
				m.setSortOrder(cc.size() + 1L);
			} else {
				m.setSortOrder(1L);
			}
			this.categoryManager.save(m);
			Long CId = categoryDTO.getParentId();
			parentId = CId;

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 删除商品类别
	public String del() {
		try {
			for (Long m : categoryDTO.getMid()) {
				this.categoryManager.delete(m);

			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 修改商品类别
	public String update() {
		try {
			CtGoodsCategory m = new CtGoodsCategory();
			m = this.categoryManager.findById(categoryDTO.getId());

			m.setCName(categoryDTO.getCName());
			m.setFilterAttr(categoryDTO.getFilterAttr());
			m.setParentId(categoryDTO.getParentId());
			m.setSortOrder(categoryDTO.getSortOrder());
			categoryManager.update(m);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String find() {
		try {
			Long id = categoryDTO.getId();
			category = new CtGoodsCategory();
			category.setCId(id);
			this.category = this.categoryManager.findById(id);
			if(category.getParentId() == 0){
				categoryDTO.setType(0L);
				category.setType(0L);
			} else {
				CtGoodsCategory c = this.categoryManager.findById(category.getParentId());
				if(c != null && c.getParentId() != 0){
					categoryDTO.setType(2L);
					category.setType(2L);
				} else {
					categoryDTO.setType(1L);
					category.setType(1L);
				}
			}
			categoryDTO.setParentId(category.getParentId());
			categoryDTO.setCName(category.getCName());
			categoryDTO.setFilterAttr(category.getFilterAttr());
			categoryDTO.setSortOrder(category.getSortOrder());

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}
	private JSONArray resultJson;
	
	public CtGoodsCategoryDTO getCategoryDTO() {
		return categoryDTO;
	}

	public void setCategoryDTO(CtGoodsCategoryDTO categoryDTO) {
		this.categoryDTO = categoryDTO;
	}

	public JSONArray getResultJson() {
		return resultJson;
	}

	public void setResultJson(JSONArray resultJson) {
		this.resultJson = resultJson;
	}

	//查询二级分类
	public String findTwoParCre(){
		Long type = categoryDTO.getType();
		if(type == 1){
			//一级分类
			categories = categoryManager.levelquery(0L);
		} else if(type == 2){
			//一级分类
			categories = categoryManager.levelquery(0L);
			List<CtGoodsCategory> categoriesTwo = new ArrayList<CtGoodsCategory>();
			List<CtGoodsCategory> categoriesTwoFind = new ArrayList<CtGoodsCategory>();
			//二级分类
			for (int i = 0; i < categories.size(); i++) {
				categoriesTwoFind = categoryManager.levelquery(categories.get(i).getCId());
				if(categoriesTwoFind != null){
					for (int j = 0; j < categoriesTwoFind.size(); j++) {
						categoriesTwo.add(categoriesTwoFind.get(j));
					}
				}
			}
			categories = categoriesTwo;
		} else {
			//一级分类
			categories = categoryManager.levelquery(0L);
		}
		JSONArray json = new JSONArray();
		json = JSONArray.fromObject(categories);
		resultJson = json;
		return SUCCESS;
	}

}
