package org.ctonline.action.goods;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.dto.goods.CtGoodsAttributeDTO;
import org.ctonline.manager.goods.CtGoodsAttributeManager;
import org.ctonline.po.goods.CtGoodsAttribute;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class CtGoodsAttributeAction extends ActionSupport implements
		RequestAware, ModelDriven<Object> {

	private List<CtGoodsAttribute> attributes;
	private CtGoodsAttributeManager attributeManager;
	private Map<String, Object> request;
	private Page page;
	private CtGoodsAttributeDTO attributeDTO = new CtGoodsAttributeDTO();
	private CtGoodsAttribute attribute;
	private Long GTId;

	public Long getGTId() {
		return GTId;
	}

	public void setGTId(Long gTId) {
		GTId = gTId;
	}

	public List<CtGoodsAttribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<CtGoodsAttribute> attributes) {
		this.attributes = attributes;
	}

	public CtGoodsAttributeManager getAttributeManager() {
		return attributeManager;
	}

	public void setAttributeManager(CtGoodsAttributeManager attributeManager) {
		this.attributeManager = attributeManager;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtGoodsAttribute getAttribute() {
		return attribute;
	}

	public void setAttribute(CtGoodsAttribute attribute) {
		this.attribute = attribute;
	}

	@Override
	public Object getModel() {

		return this.attributeDTO;
	}

	// 页面显示商品品牌
	public String list() {
		try {
			Long id = attributeDTO.getGTId();
			Page page = new Page();
			if (attributeDTO.getPage() == 0) {
				attributeDTO.setPage(1);
			}
			page.setCurrentPage(attributeDTO.getPage());
			this.attributes = attributeManager.searchAll(id, page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			this.request.put("GTId", id);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 按照关键字查询商品品牌
	public String search() {
		try {

			Page page = new Page();
			if (attributeDTO.getPage() == 0) {
				attributeDTO.setPage(1);
			}
			page.setCurrentPage(attributeDTO.getPage());
			if (attributeDTO.getKeyword() != null
					&& !attributeDTO.getKeyword().equals("请输入你要查的关键词")) {
				this.attributes = attributeManager.findAll(
						attributeDTO.getKeyword(), page);
			} else {
				this.attributes = attributeManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 增加商品品牌
	public String add() {
		try {

			CtGoodsAttribute m = new CtGoodsAttribute();
			m.setAttrName(attributeDTO.getAttrName());
			m.setSortOrder(attributeDTO.getSortOrder());
			m.setGTId(attributeDTO.getGTId());
			this.attributeManager.save(m);
			GTId = attributeDTO.getGTId();

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 删除商品品牌
	public String del() {

		try {
			for (Long m : attributeDTO.getMid()) {
				this.attributeManager.delete(m);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 修改商品品牌
	public String update() {
		try {
			CtGoodsAttribute m = new CtGoodsAttribute();
			m.setAttrId(attributeDTO.getAttrId());
			m.setGTId(attributeDTO.getGTId());
			m.setAttrName(attributeDTO.getAttrName());
			m.setSortOrder(attributeDTO.getSortOrder());
			attributeManager.update(m);
			GTId = attributeDTO.getGTId();
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String find() {
		try {
			Long id = attributeDTO.getId();

			attribute = new CtGoodsAttribute();
			attribute.setAttrId(id);
			this.attribute = this.attributeManager.findById(id);
			attributeDTO.setAttrName(attribute.getAttrName());
			attributeDTO.setSortOrder(attribute.getSortOrder());
			attributeDTO.setAttrId(attribute.getAttrId());
			attributeDTO.setGTId(attribute.getGTId());

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}

}
