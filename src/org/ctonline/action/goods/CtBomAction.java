package org.ctonline.action.goods;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.action.BaseAction;
import org.ctonline.dao.goods.CtBomDao;
import org.ctonline.dto.goods.CtBomDTO;
import org.ctonline.dto.goods.CtBomGoodsDTO;
import org.ctonline.dto.user.CtUserDTO;
import org.ctonline.manager.goods.CtBomManager;
import org.ctonline.manager.user.CtUserManager;
import org.ctonline.po.goods.CtBom;
import org.ctonline.po.goods.CtBomGoods;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;

public class CtBomAction extends BaseAction implements RequestAware,
		ModelDriven<Object> {

	private Map<String, Object> request;
	private CtBomDTO bomDTO = new CtBomDTO();
	private CtUserDTO userDTO = new CtUserDTO();
	private CtUser user;
	private CtBom bom;
	private CtBomManager bomManager;
	private CtUserManager userManager;
	private Page page;
	private List<CtBom> bomList;
	private List<CtBomGoods> bomGoodsList;
	private CtBomGoodsDTO bomGoodsDTO = new CtBomGoodsDTO();
	private String result;
	private JSONArray resultJson;

	// bom列表
	public String goBomList() {
		try {
			Long UId = userDTO.getUId();
			page = new Page();
			if (bomDTO.getPage() == 0) {
				bomDTO.setPage(1);
			}
			page.setCurrentPage(bomDTO.getPage());
			this.bomList = bomManager.loadAll(page, UId);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "goBomList";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	// 新增bom
	public String add() {
		try {
			Long UId = userDTO.getUId();
			this.userDTO.setUId(UId);
			return "add";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	// 保存bom
	public String saveAdd() {
		try {
			Long UId = userDTO.getUId();
			String bomTitle = this.bomDTO.getBomTitle();
			String bomDesc = this.bomDTO.getBomDesc();
			Date date = new Date();
			SimpleDateFormat siFormat = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			CtBom cb = new CtBom();
			cb.setBomTitle(bomTitle);
			cb.setBomDesc(bomDesc);
			cb.setBomTime(siFormat.format(date));
			cb.setUId(UId);
			cb.setIsHot("0");
			Integer id = this.bomManager.save(cb);
			if (id != null) {
				return "saveAdd";
			} else {
				return "error";
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	// 修改Bom
	public String editBom() {
		try {
			Long UId = userDTO.getUId();
			this.userDTO.setUId(UId);
			Integer[] bomId = this.bomDTO.getMid();
			this.bom = this.bomManager.getCtBomByBomId(bomId[0]);
			return "editBom";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	// 保存修改Bom
	public String saveUpdate() {
		try {
			Integer bomId = this.bomDTO.getBomId();
			String bomTitle = this.bomDTO.getBomTitle();
			String bomDesc = this.bomDTO.getBomDesc();
			CtBom cb = this.bomManager.getCtBomByBomId(bomId);
			cb.setBomTitle(bomTitle);
			cb.setBomDesc(bomDesc);
			this.bomManager.update(cb);
			return "saveUpdate";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	// 删除Bom
	public String del() {
		try {
			for (Integer b : bomDTO.getMid()) {
				this.bomManager.delete(b);
			}
			return "del";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	// 搜素
	public String search() {
		try {
			Long UId = userDTO.getUId();
			Page page = new Page();
			this.bomDTO.setKeyword(bomDTO.getKeyword());
			if (bomDTO.getPage() == 0) {
				bomDTO.setPage(1);
			}
			page.setCurrentPage(bomDTO.getPage());
			if (!"".equals(bomDTO.getKeyword())) {
				this.bomList = bomManager.findAll(bomDTO.getKeyword(), page,
						UId);
			} else {
				this.bomList = bomManager.loadAll(page, UId);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "search";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 复制Bom
	public String copyBom() {
		try {
			Long UId = userDTO.getUId();
			for (Integer b : bomDTO.getMid()) {
				// this.bomManager.delete(b);
				CtBom cb = this.bomManager.getCtBomByBomId(b);
				CtBom cbcopy = new CtBom();
				String bomTitleCopy = cb.getBomTitle() + "副本";
				Date date = new Date();
				SimpleDateFormat siFormat = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				cbcopy.setBomTitle(bomTitleCopy);
				cbcopy.setBomTime(siFormat.format(date));
				cbcopy.setBomDesc(cb.getBomDesc());
				cbcopy.setUId(UId);
				this.bomManager.save(cbcopy);
			}
			return "copyBom";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	// 加入bom中
	public String addToBomCenter() {
		try {
			for (Integer b : bomDTO.getMid()) {
				CtBom cb = this.bomManager.getCtBomByBomId(b);
				cb.setIsHot("1");
				this.bomManager.update(cb);
			}
			return "addToBomCenter";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	// Bom详细
	public String goBomDetail() {
		try {
			Long UId = this.bomDTO.getUId();
			this.user = this.userManager.getCtUserByUId(UId);
			Integer bomId = this.bomDTO.getBomId();
			this.bomDTO.setUId(UId);
			this.bomDTO.setBomId(bomId);
			this.bom = this.bomManager.getCtBomByBomId(bomId);
			page = new Page();
			if (bomGoodsDTO.getPage() == 0) {
				bomGoodsDTO.setPage(1);
			}
			page.setCurrentPage(bomGoodsDTO.getPage());
			this.bomGoodsList = bomManager.loadBomDetailAll(page, bomId);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "goBomDetail";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	// 删除Bom详情中的某个商品
	public String delGoodsOfBomDetail() {
		try {
			Integer bomId = this.bomDTO.getBomId();
			for (Long b : bomGoodsDTO.getMid()) {
				this.bomManager.delGoodsOfBomDetail(b);
			}
			page = new Page();
			if (bomGoodsDTO.getPage() == 0) {
				bomGoodsDTO.setPage(1);
			}
			page.setCurrentPage(bomGoodsDTO.getPage());
			this.bomGoodsList = bomManager.loadBomDetailAll(page, bomId);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "delGoodsOfBomDetail";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	// 搜索Bom详情中的商品
	public String searchGoodsOfBomDetail() {
		try {
			Integer bomId = this.bomGoodsDTO.getBomId();
			this.bomGoodsDTO.setKeyword(bomGoodsDTO.getKeyword());
			Page page = new Page();
			if (bomGoodsDTO.getPage() == 0) {
				bomGoodsDTO.setPage(1);
			}
			page.setCurrentPage(bomGoodsDTO.getPage());
			if (!"".equals(bomGoodsDTO.getKeyword())) {
				this.bomGoodsList = bomManager.findBomDetailAll(
						bomGoodsDTO.getKeyword(), page, bomId);
			} else {
				this.bomGoodsList = bomManager.loadBomDetailAll(page, bomId);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "searchGoodsOfBomDetail";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	// 保存Pack
	public String savePack() {
		try {
			String pack = this.bomGoodsDTO.getPack();
			Long bomGoodsId = this.bomGoodsDTO.getBomGoodsId();
			CtBomGoods cbg = this.bomManager
					.getCtBomGoodsBybomGoodsId(bomGoodsId);
			cbg.setPack(pack);
			this.bomManager.updateCtBomGoods(cbg);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	// 更新数量
	public String updateGoodsNum() {
		try {
			Integer goodsNum = this.bomGoodsDTO.getGoodsNum();
			Long GId = this.bomGoodsDTO.getGId();
			CtBomGoods cbg = this.bomManager.getCtBomGoodsByGId(GId);
			cbg.setGoodsNum(goodsNum);
			this.bomManager.updateCtBomGoods(cbg);
			this.result = "success";
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			this.result = "error";
			return "error";
		}
	}

	// 新增Bom详情中的商品
	public String addGoodsOfBomDetail() {
		try {
			Integer bomId = this.bomGoodsDTO.getBomId();
			Long GId = this.bomGoodsDTO.getGId();
			CtBomGoods cbg = this.bomManager.getCtBomGoodsByGId(GId);
			if (cbg != null) {
				if (cbg.getGoodsNum() == null) {
					cbg.setGoodsNum(2);
				} else {
					cbg.setGoodsNum(cbg.getGoodsNum() + 1);
				}
				this.bomManager.updateCtBomGoods(cbg);
			} else {
				CtBomGoods cbg2 = new CtBomGoods();
				cbg2.setBomId(bomId);
				cbg2.setGId(GId);
				this.bomManager.saveCtBomGoods(cbg2);
			}
			page = new Page();
			if (bomGoodsDTO.getPage() == 0) {
				bomGoodsDTO.setPage(1);
			}
			page.setCurrentPage(bomGoodsDTO.getPage());
			this.bomGoodsList = bomManager.loadBomDetailAll(page, bomId);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "addGoodsOfBomDetail";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	// BOM中心列表
	public String bomCenter() {
		try {
			page = new Page();
			if (bomDTO.getPage() == 0) {
				bomDTO.setPage(1);
			}
			page.setCurrentPage(bomDTO.getPage());
			this.bomList = bomManager.loadAllBomCenter(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "bomCenter";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "errro";
		}
	}

	// Bom中心新增
	public String addBomCenter() {
		try {
			return "addBomCenter";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "errro";
		}
	}

	// Bom中心保存新增
	public String saveAddBomCenter() {
		try {
			String bomTitle = this.bomDTO.getBomTitle();
			String bomDesc = this.bomDTO.getBomDesc();
			Date date = new Date();
			SimpleDateFormat siFormat = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			CtBom cb = new CtBom();
			cb.setBomTitle(bomTitle);
			cb.setBomDesc(bomDesc);
			cb.setBomTime(siFormat.format(date));
			cb.setIsHot("1");
			Integer id = this.bomManager.save(cb);
			if (id != null) {
				return "saveAddBomCenter";
			} else {
				return "error";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "errro";
		}
	}

	// Bom中心修改
	public String editBomBomCenter() {
		try {
			Integer[] bomId = this.bomDTO.getMid();
			this.bom = this.bomManager.getCtBomByBomId(bomId[0]);
			return "editBomBomCenter";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "errro";
		}
	}

	// Bom中心保存修改
	public String saveUpdateBomCenter() {
		try {
			Integer bomId = this.bomDTO.getBomId();
			String bomTitle = this.bomDTO.getBomTitle();
			String bomDesc = this.bomDTO.getBomDesc();
			CtBom cb = this.bomManager.getCtBomByBomId(bomId);
			cb.setBomTitle(bomTitle);
			cb.setBomDesc(bomDesc);
			this.bomManager.update(cb);
			return "saveUpdateBomCenter";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "errro";
		}
	}

	// Bom中心删除
	public String deleteThisBomCenter() {
		try {
			for (Integer b : bomDTO.getMid()) {
				this.bomManager.delete(b);
			}
			return "deleteThisBomCenter";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "errro";
		}
	}

	// 复制Bom中心
	public String copyBomBC() {
		try {
			Long UId = userDTO.getUId();
			for (Integer b : bomDTO.getMid()) {
				CtBom cb = this.bomManager.getCtBomByBomId(b);
				CtBom cbcopy = new CtBom();
				String bomTitleCopy = cb.getBomTitle() + "副本";
				Date date = new Date();
				SimpleDateFormat siFormat = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				cbcopy.setBomTitle(bomTitleCopy);
				cbcopy.setBomTime(siFormat.format(date));
				cbcopy.setBomDesc(cb.getBomDesc());
				cbcopy.setIsHot("1");
				this.bomManager.save(cbcopy);
			}
			return "copyBomBC";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	// 搜素bom中心
	public String searchBC() {
		Page page = new Page();
		this.bomDTO.setKeyword(bomDTO.getKeyword());
		if (bomDTO.getPage() == 0) {
			bomDTO.setPage(1);
		}
		page.setCurrentPage(bomDTO.getPage());
		if (!"".equals(bomDTO.getKeyword())) {
			this.bomList = bomManager.findAllBomCenter(bomDTO.getKeyword(),
					page);
		} else {
			this.bomList = bomManager.loadAllBomCenter(page);
		}
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);
		return "searchBC";
	}

	// Bom中心详细
	public String goBomBCDetail() {
		try {
			Integer bomId = this.bomDTO.getBomId();
			this.bom = this.bomManager.getCtBomByBomId(bomId);
			page = new Page();
			if (bomGoodsDTO.getPage() == 0) {
				bomGoodsDTO.setPage(1);
			}
			page.setCurrentPage(bomGoodsDTO.getPage());
			this.bomGoodsList = bomManager.loadBomDetailAll(page, bomId);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "goBomBCDetail";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	// 新增Bom中心详情中的商品
	public String addGoodsOfBomDetailBC() {
		try {
			Integer bomId = this.bomGoodsDTO.getBomId();
			Long GId = this.bomGoodsDTO.getGId();
			CtBomGoods cbg = this.bomManager.getCtBomGoodsByGId(GId);
			if (cbg != null) {
				if (cbg.getGoodsNum() == null) {
					cbg.setGoodsNum(2);
				} else {
					cbg.setGoodsNum(cbg.getGoodsNum() + 1);
				}
				this.bomManager.updateCtBomGoods(cbg);
			} else {
				CtBomGoods cbg2 = new CtBomGoods();
				cbg2.setBomId(bomId);
				cbg2.setGId(GId);
				this.bomManager.saveCtBomGoods(cbg2);
			}
			page = new Page();
			if (bomGoodsDTO.getPage() == 0) {
				bomGoodsDTO.setPage(1);
			}
			page.setCurrentPage(bomGoodsDTO.getPage());
			this.bomGoodsList = bomManager.loadBomDetailAll(page, bomId);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "addGoodsOfBomDetailBC";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	// 搜索Bom中心详情中的商品
	public String searchGoodsOfBomDetailBC() {
		try {
			Integer bomId = this.bomGoodsDTO.getBomId();
			this.bomGoodsDTO.setKeyword(bomGoodsDTO.getKeyword());
			Page page = new Page();
			if (bomGoodsDTO.getPage() == 0) {
				bomGoodsDTO.setPage(1);
			}
			page.setCurrentPage(bomGoodsDTO.getPage());
			if (!"".equals(bomGoodsDTO.getKeyword())) {
				this.bomGoodsList = bomManager.findBomDetailAll(
						bomGoodsDTO.getKeyword(), page, bomId);
			} else {
				this.bomGoodsList = bomManager.loadBomDetailAll(page, bomId);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "searchGoodsOfBomDetailBC";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	// 删除Bom中心详情中的某个商品
	public String delGoodsOfBomDetailBC() {
		try {
			Integer bomId = this.bomDTO.getBomId();
			for (Long b : bomGoodsDTO.getMid()) {
				this.bomManager.delGoodsOfBomDetail(b);
			}
			page = new Page();
			if (bomGoodsDTO.getPage() == 0) {
				bomGoodsDTO.setPage(1);
			}
			page.setCurrentPage(bomGoodsDTO.getPage());
			this.bomGoodsList = bomManager.loadBomDetailAll(page, bomId);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "delGoodsOfBomDetailBC";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.bomDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public CtBomDTO getBomDTO() {
		return bomDTO;
	}

	public void setBomDTO(CtBomDTO bomDTO) {
		this.bomDTO = bomDTO;
	}

	public CtBomManager getBomManager() {
		return bomManager;
	}

	public void setBomManager(CtBomManager bomManager) {
		this.bomManager = bomManager;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public List<CtBom> getBomList() {
		return bomList;
	}

	public void setBomList(List<CtBom> bomList) {
		this.bomList = bomList;
	}

	public CtBom getBom() {
		return bom;
	}

	public void setBom(CtBom bom) {
		this.bom = bom;
	}

	public List<CtBomGoods> getBomGoodsList() {
		return bomGoodsList;
	}

	public void setBomGoodsList(List<CtBomGoods> bomGoodsList) {
		this.bomGoodsList = bomGoodsList;
	}

	public CtBomGoodsDTO getBomGoodsDTO() {
		return bomGoodsDTO;
	}

	public void setBomGoodsDTO(CtBomGoodsDTO bomGoodsDTO) {
		this.bomGoodsDTO = bomGoodsDTO;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public JSONArray getResultJson() {
		return resultJson;
	}

	public void setResultJson(JSONArray resultJson) {
		this.resultJson = resultJson;
	}

	public CtUserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(CtUserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public CtUserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(CtUserManager userManager) {
		this.userManager = userManager;
	}

	public CtUser getUser() {
		return user;
	}

	public void setUser(CtUser user) {
		this.user = user;
	}

}
