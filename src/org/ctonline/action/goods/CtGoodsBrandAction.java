package org.ctonline.action.goods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.dto.goods.CtGoodsBrandDTO;
import org.ctonline.manager.goods.CtGoodsBrandManager;
import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.util.Page;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class CtGoodsBrandAction extends ActionSupport implements RequestAware,
		ModelDriven<Object> {
	private List<CtGoodsBrand> brands;
	private CtGoodsBrandManager brandManager;
	private Map<String, Object> request;
	private Page page;
	private CtGoodsBrandDTO brandDTO = new CtGoodsBrandDTO();
	private CtGoodsBrand brand;
	private File file;
	private String fileFileName;
	private String fileContentType;

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public String getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(String fileContentType) {
		this.fileContentType = fileContentType;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.brandDTO;
	}

	public List<CtGoodsBrand> getBrands() {
		return brands;
	}

	public void setBrands(List<CtGoodsBrand> brands) {
		this.brands = brands;
	}

	public CtGoodsBrandManager getBrandManager() {
		return brandManager;
	}

	public void setBrandManager(CtGoodsBrandManager brandManager) {
		this.brandManager = brandManager;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtGoodsBrand getBrand() {
		return brand;
	}

	public void setBrand(CtGoodsBrand brand) {
		this.brand = brand;
	}

	// 页面显示商品品牌
	public String list() {
		try {
			page = new Page();
			if (brandDTO.getPage() == 0) {
				brandDTO.setPage(1);
			}
			page.setCurrentPage(brandDTO.getPage());
			this.brands = brandManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 按照关键字查询商品品牌
	public String search() {
		try {
			Page page = new Page();
			if (brandDTO.getPage() == 0) {
				brandDTO.setPage(1);
			}
			page.setCurrentPage(brandDTO.getPage());
			if (brandDTO.getKeyword() != null
					&& !brandDTO.getKeyword().equals("请输入你要查的关键词")) {
				this.brands = brandManager.findAll(brandDTO.getKeyword(), page);
			} else {
				this.brands = brandManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 增加商品品牌

	public String saveBrand() {
		// 上传目录
		String dir = "";
		// 带时间戳的文件名
		String timename = "";
		// 缩略图存储路径
		String imgSmalldir = "";
		// 上传图片
		if (file == null) {
			// return INPUT;
		}
		CtGoodsBrand m = new CtGoodsBrand();
		try {
			InputStream in = new FileInputStream(file);
			String UPLOADDIR = "/upload3";
			dir = ServletActionContext.getRequest().getRealPath(UPLOADDIR);
			String time = new Date().getTime() + "";
			timename = time + getExtention(this.getFileFileName());
			String url = UPLOADDIR + "/" + timename;
			m.setBLogo(url);
			File fileLocation = new File(dir);
			if (!fileLocation.exists()) {
				boolean isCreated = fileLocation.mkdirs();
				if (!isCreated) {
					System.out.println("上传目录创建失败！");
					// 上传目录创建失败处理
					return null;
				}
			}
			File uploadFile = new File(dir, timename);
			OutputStream out = new FileOutputStream(uploadFile);
			byte[] buffer = new byte[1024 * 1024];
			int length;
			while ((length = in.read(buffer)) > 0) {
				out.write(buffer, 0, length);
			}
			in.close();
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("上传失败！");
		} catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println("上传失败！");
		}

		m.setBName(brandDTO.getBName());
		m.setBDesc(brandDTO.getBDesc());
		m.setBUrl(brandDTO.getBUrl());
		m.setSortOrder(brandDTO.getSortOrder());
		m.setIsShow(brandDTO.getIsShow());
		this.brandManager.save(m);
		return "saveBrand";
	}

	private String getExtention(String fileName) {
		int pos = fileName.lastIndexOf(".");
		return fileName.substring(pos);
	}

	// 删除商品品牌
	public String del() {
		try {
			for (Long m : brandDTO.getMid()) {
				this.brandManager.delete(m);

			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 修改商品品牌
	public String update() {
		try {
			CtGoodsBrand m = new CtGoodsBrand();
			m = this.brandManager.findById(brandDTO.getId());
			m.setBName(brandDTO.getBName());
			m.setBDesc(brandDTO.getBDesc());
			m.setBLogo(brandDTO.getBLogo());
			m.setBUrl(brandDTO.getBUrl());
			m.setSortOrder(brandDTO.getSortOrder());
			m.setIsShow(brandDTO.getIsShow());
			brandManager.update(m);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String find() {
		try {
			Long id = brandDTO.getId();
			brand = new CtGoodsBrand();
			brand.setBId(id);
			this.brand = this.brandManager.findById(id);
			brandDTO.setBDesc(brand.getBDesc());
			brandDTO.setBLogo(brand.getBLogo());
			brandDTO.setBName(brand.getBName());
			brandDTO.setBUrl(brand.getBUrl());
			brandDTO.setIsShow(brand.getIsShow());
			brandDTO.setSortOrder(brand.getSortOrder());

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}

}
