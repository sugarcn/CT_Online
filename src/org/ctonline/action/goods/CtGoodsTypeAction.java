package org.ctonline.action.goods;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.dto.goods.CtGoodsTypeDTO;
import org.ctonline.manager.goods.CtGoodsTypeManager;
import org.ctonline.po.goods.CtGoodsType;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class CtGoodsTypeAction extends ActionSupport implements RequestAware,
		ModelDriven<Object> {

	private List<CtGoodsType> types;
	private CtGoodsTypeManager typeManager;
	private Map<String, Object> request;
	private Page page;
	private CtGoodsTypeDTO typeDTO = new CtGoodsTypeDTO();
	private CtGoodsType goodsType;

	public List<CtGoodsType> getTypes() {
		return types;
	}

	public void setTypes(List<CtGoodsType> types) {
		this.types = types;
	}

	public CtGoodsTypeManager getTypeManager() {
		return typeManager;
	}

	public void setTypeManager(CtGoodsTypeManager typeManager) {
		this.typeManager = typeManager;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtGoodsType getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(CtGoodsType goodsType) {
		this.goodsType = goodsType;
	}

	@Override
	public Object getModel() {

		return this.typeDTO;
	}

	// 页面显示商品类型
	public String list() {
		try {
			page = new Page();
			if (typeDTO.getPage() == 0) {
				typeDTO.setPage(1);
			}
			page.setCurrentPage(typeDTO.getPage());
			this.types = typeManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 按照关键字查询商品类型
	public String search() {
		try {
			Page page = new Page();
			if (typeDTO.getPage() == 0) {
				typeDTO.setPage(1);
			}
			page.setCurrentPage(typeDTO.getPage());
			if (typeDTO.getKeyword() != null
					&& !typeDTO.getKeyword().equals("请输入你要查的关键词")) {
				this.types = typeManager.findAll(typeDTO.getKeyword(), page);
			} else {
				this.types = typeManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 增加商品类型
	public String add() {
		try {
			CtGoodsType m = new CtGoodsType();
			m.setTypeName(typeDTO.getTypeName());
			m.setEnabled(typeDTO.getEnabled());
			this.typeManager.save(m);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 删除商品类型
	public String del() {
		try {
			for (Long m : typeDTO.getMid()) {
				this.typeManager.delete(m);

			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 修改商品类型
	public String update() {
		try {
			CtGoodsType m = new CtGoodsType();
			m = this.typeManager.findById(typeDTO.getId());
			m.setTypeName(typeDTO.getTypeName());
			m.setEnabled(typeDTO.getEnabled());
			typeManager.update(m);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String find() {
		try {
			Long id = typeDTO.getId();
			goodsType = new CtGoodsType();
			goodsType.setGTId(id);
			this.goodsType = this.typeManager.findById(id);
			typeDTO.setTypeName(goodsType.getTypeName());
			typeDTO.setEnabled(goodsType.getEnabled());

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}

}
