package org.ctonline.action.order;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.action.BaseAction;
import org.ctonline.config.AlipayConfig;
import org.ctonline.dto.order.OrderDTO;
import org.ctonline.dto.user.CtUserDTO;
import org.ctonline.manager.basic.CtCouponDetailManager;
import org.ctonline.manager.basic.CtCouponManager;
import org.ctonline.manager.goods.CtGoodsManager;
import org.ctonline.manager.order.ICtBankManager;
import org.ctonline.manager.order.ICtPayManager;
import org.ctonline.manager.order.OrderManager;
import org.ctonline.manager.user.CtUserManager;
import org.ctonline.po.basic.CtCouponDetail;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.manager.CtManager;
import org.ctonline.po.order.CtBank;
import org.ctonline.po.order.CtComplain;
import org.ctonline.po.order.CtEvaluation;
import org.ctonline.po.order.CtExpress;
import org.ctonline.po.order.CtOrderGoods;
import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.po.order.CtOrderRelation;
import org.ctonline.po.order.CtPay;
import org.ctonline.po.order.CtRefund;
import org.ctonline.po.pay.CtPayInterface;
import org.ctonline.po.user.CtUser;
import org.ctonline.service.EJLInfoServiceImplDelegate;
import org.ctonline.service.EJLInfoServiceImplService;
import org.ctonline.service.JdpCtegoTrade;
import org.ctonline.util.AlipayNotify;
import org.ctonline.util.AlipaySubmit;
import org.ctonline.util.GetWxOrderno;
import org.ctonline.util.Page;
import org.ctonline.util.RequestHandler;
import org.ctonline.util.TenpayUtil;
import org.json.JSONException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;

public class OrderAction
  extends BaseAction
  implements RequestAware, ModelDriven<Object>, SessionAware, ServletResponseAware
{
  private ICtBankManager bankManager;
  private ICtPayManager payManager;
  private OrderManager orderManager;
  private CtOrderInfo ctOrderInfo;
  private List<CtBank> banks;
  private CtPay ctPay;
  private CtOrderGoods ctOrderGoods;
  private List<CtOrderGoods> orderGoodsList = new ArrayList();
  
  public List<CtOrderGoods> getOrderGoodsSamList()
  {
    return this.orderGoodsSamList;
  }
  
  public void setOrderGoodsSamList(List<CtOrderGoods> orderGoodsSamList)
  {
    this.orderGoodsSamList = orderGoodsSamList;
  }
  
  private List<CtOrderGoods> orderGoodsSamList = new ArrayList();
  private List<CtExpress> express;
  private CtExpress expres;
  private CtCouponDetail couponDetail;
  private CtCouponDetailManager coupondetailManager;
  private CtCouponManager couponManager;
  private CtUserManager userManager;
  private CtGoodsManager goodsManager;
  private Page page;
  private CtManager manager;
  private CtComplain complain;
  private List<CtComplain> complainList = new ArrayList();
  private CtBank bank;
  private CtUser ctUser;
  private HttpServletResponse response;
  private HttpServletRequest requ;
  private Map<String, Object> session;
  private Map<String, Object> request;
  
  public List<CtBank> getBanks()
  {
    return this.banks;
  }
  
  public void setBanks(List<CtBank> banks)
  {
    this.banks = banks;
  }
  
  public CtComplain getComplain()
  {
    return this.complain;
  }
  
  public void setComplain(CtComplain complain)
  {
    this.complain = complain;
  }
  
  public List<CtComplain> getComplainList()
  {
    return this.complainList;
  }
  
  public void setComplainList(List<CtComplain> complainList)
  {
    this.complainList = complainList;
  }
  
  public CtManager getManager()
  {
    return this.manager;
  }
  
  public void setManager(CtManager manager)
  {
    this.manager = manager;
  }
  
  public CtExpress getExpres()
  {
    return this.expres;
  }
  
  public void setExpres(CtExpress expres)
  {
    this.expres = expres;
  }
  
  public List<CtExpress> getExpress()
  {
    return this.express;
  }
  
  public void setExpress(List<CtExpress> express)
  {
    this.express = express;
  }
  
  public Page getPage()
  {
    return this.page;
  }
  
  public void setPage(Page page)
  {
    this.page = page;
  }
  
  public CtGoodsManager getGoodsManager()
  {
    return this.goodsManager;
  }
  
  public void setGoodsManager(CtGoodsManager goodsManager)
  {
    this.goodsManager = goodsManager;
  }
  
  public CtBank getBank()
  {
    return this.bank;
  }
  
  public void setBank(CtBank bank)
  {
    this.bank = bank;
  }
  
  public CtUser getCtUser()
  {
    return this.ctUser;
  }
  
  public void setCtUser(CtUser ctUser)
  {
    this.ctUser = ctUser;
  }
  
  public CtUserManager getUserManager()
  {
    return this.userManager;
  }
  
  public void setUserManager(CtUserManager userManager)
  {
    this.userManager = userManager;
  }
  
  public CtCouponDetail getCouponDetail()
  {
    return this.couponDetail;
  }
  
  public void setCouponDetail(CtCouponDetail couponDetail)
  {
    this.couponDetail = couponDetail;
  }
  
  public CtCouponDetailManager getCoupondetailManager()
  {
    return this.coupondetailManager;
  }
  
  public void setCoupondetailManager(CtCouponDetailManager coupondetailManager)
  {
    this.coupondetailManager = coupondetailManager;
  }
  
  public CtCouponManager getCouponManager()
  {
    return this.couponManager;
  }
  
  public void setCouponManager(CtCouponManager couponManager)
  {
    this.couponManager = couponManager;
  }
  
  public List<CtOrderGoods> getOrderGoodsList()
  {
    return this.orderGoodsList;
  }
  
  public void setOrderGoodsList(List<CtOrderGoods> orderGoodsList)
  {
    this.orderGoodsList = orderGoodsList;
  }
  
  public CtOrderGoods getCtOrderGoods()
  {
    return this.ctOrderGoods;
  }
  
  public void setCtOrderGoods(CtOrderGoods ctOrderGoods)
  {
    this.ctOrderGoods = ctOrderGoods;
  }
  
  public CtPay getCtPay()
  {
    return this.ctPay;
  }
  
  public void setCtPay(CtPay ctPay)
  {
    this.ctPay = ctPay;
  }
  
  public CtOrderInfo getCtOrderInfo()
  {
    return this.ctOrderInfo;
  }
  
  public void setCtOrderInfo(CtOrderInfo ctOrderInfo)
  {
    this.ctOrderInfo = ctOrderInfo;
  }
  
  
  public void setOrderManager(OrderManager orderManager)
  {
    this.orderManager = orderManager;
  }
  
  private List<CtOrderInfo> checks = new ArrayList();
  
  public List<CtOrderInfo> getChecks()
  {
    return this.checks;
  }
  
  public void setChecks(List<CtOrderInfo> checks)
  {
    this.checks = checks;
  }
  
  public OrderDTO getOrderDTO()
  {
    return this.orderDTO;
  }
  
  public void setOrderDTO(OrderDTO orderDTO)
  {
    this.orderDTO = orderDTO;
  }
  
  public void setServletResponse(HttpServletResponse response)
  {
    this.response = response;
    this.response.setCharacterEncoding("UTF-8");
    this.response.setContentType("text/html");
  }
  
  public ICtBankManager getBankManager()
  {
    return this.bankManager;
  }
  
  public void setBankManager(ICtBankManager bankManager)
  {
    this.bankManager = bankManager;
  }
  
  public ICtPayManager getPayManager()
  {
    return this.payManager;
  }
  
  public void setPayManager(ICtPayManager payManager)
  {
    this.payManager = payManager;
  }
  
  public HttpServletResponse getResponse()
  {
	  response = ServletActionContext.getResponse();
    return this.response;
  }
  
  public HttpServletRequest getRequ() {
	return requ;
}

public void setRequ(HttpServletRequest requ) {
	requ = ServletActionContext.getRequest();
	this.requ = requ;
}

public void setResponse(HttpServletResponse response)
  {
    this.response = response;
  }
  
  public Map<String, Object> getSession()
  {
    return this.session;
  }
  
  public void setSession(Map<String, Object> session)
  {
    this.session = session;
  }
  
  public Map<String, Object> getRequest()
  {
    return this.request;
  }
  
  public void setRequest(Map<String, Object> request)
  {
    this.request = request;
  }
  
  private OrderDTO orderDTO = new OrderDTO();
  
  public Object getModel()
  {
    return this.orderDTO;
  }
	//用户订单列表
	public String getUserOrderListInfo(){
		  try {
			  Long uid = orderDTO.getUid();
			  //System.out.println(new String(orderDTO.getOrderSn().getBytes("UTF-8"),"GBK"));
			  String firstDate = orderManager.getFirstDateOrder();
			  request.put("firstDate", firstDate);
			String sn = "";
			  if(orderDTO.getOrderSn()!= null && !new String(orderDTO.getOrderSn().getBytes("GBK"),"UTF-8").equals("订单编号")){
				  sn = orderDTO.getOrderSn().toUpperCase();
				  session.put("findOrderSnKey", sn);
			  } else {
				  sn = "noFind";
				  session.remove("findOrderSnKey");
			  }
			this.page = new Page();
			if (this.orderDTO.getPage() == 0) {
			  this.orderDTO.setPage(1);
			}
			this.page.setCurrentPage(this.orderDTO.getPage());
			this.checks = this.orderManager.getAllOrder(this.page,sn,orderDTO.getStime(), orderDTO.getEtime(), uid, null, 0);
			this.page.setTotalPage(this.page.getTotalPage());
			this.manager = ((CtManager)this.session.get("admininfosession"));
			this.request.put("pages", this.page);
	   // List<CtPay> ctPays = this.payManager.findAll(page);
			for (int i = 0; i < checks.size(); i++) {
				CtPay pay = this.payManager.findAllByOrderId(checks.get(i).getOrderId());
				if(pay != null){
					this.checks.get(i).setPayPrice(pay.getPayAmount().toString());
				}
			}
//	    for (int i = 0; i < ctPays.size(); i++) {
//	      for (int j = 0; j < this.checks.size(); j++) {
//	        if (((CtOrderInfo)this.checks.get(j)).getOrderId().equals(((CtPay)ctPays.get(i)).getOrderId()))
//	        {
//	          ((CtOrderInfo)this.checks.get(j)).setPayPrice(((CtPay)ctPays.get(i)).getPayAmount().toString());
//	          break;
//	        }
//	      }
//	    }
			request.put("stime", orderDTO.getStime());
			request.put("etime", orderDTO.getEtime());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	    return "success";
	}
	
	private List<String> orderstart = new ArrayList<String>();
	
  public List<String> getOrderstart() {
		return orderstart;
	}

	public void setOrderstart(List<String> orderstart) {
		this.orderstart = orderstart;
	}

public String findOrder()
  {
	  try {
		  //System.out.println(new String(orderDTO.getOrderSn().getBytes("UTF-8"),"GBK"));
		  Long uid = orderDTO.getUid();
		  String firstDate = orderManager.getFirstDateOrder();
		  request.put("firstDate", firstDate);
		String sn = "";
		  if(orderDTO.getOrderSn()!= null && !new String(orderDTO.getOrderSn().getBytes("GBK"),"UTF-8").equals("订单编号") && !new String(orderDTO.getOrderSn().getBytes("GBK"),"UTF-8").equals("商品编号")){
			  sn = orderDTO.getOrderSn().toUpperCase();
			  session.put("findOrderSnKey", sn.replaceAll("%20", " "));
		  } else {
			  sn = "noFind";
			  session.remove("findOrderSnKey");
		  }
		this.page = new Page();
		if (this.orderDTO.getPage() == 0) {
		  this.orderDTO.setPage(1);
		}
		this.page.setCurrentPage(this.orderDTO.getPage());
		if(uid != null){
			int searchType = orderDTO.getSearchType();
			String stauts = orderDTO.getStatus();
			this.checks = this.orderManager.getAllOrder(this.page,sn,orderDTO.getStime(), orderDTO.getEtime(), uid, stauts, searchType);
			String uname = orderDTO.getUname();
			request.put("orderUserName", uname);
			request.put("searchTypeSelect", searchType);
			orderstart.add("全部");
			orderstart.add("待付款");
			orderstart.add("已付款");
			orderstart.add("待审核");
			orderstart.add("配货中");
			orderstart.add("已发货");
			orderstart.add("已完成");
			orderstart.add("取消");
			orderstart.add("无效");
			orderstart.add("已备货");
			if(stauts != null){
				request.put("orStauts", stauts);
			}
		} else {
			this.checks = this.orderManager.getAllOrder(this.page,sn,orderDTO.getStime(), orderDTO.getEtime(), null, null, 0);
		}
		this.page.setTotalPage(this.page.getTotalPage());
		this.manager = ((CtManager)this.session.get("admininfosession"));
		this.request.put("pages", this.page);
   // List<CtPay> ctPays = this.payManager.findAll(page);
		for (int i = 0; i < checks.size(); i++) {
			CtPay pay = this.payManager.findAllByOrderId(checks.get(i).getOrderId());
			if(pay != null){
				this.checks.get(i).setPayPrice(pay.getPayAmount().toString());
			}
			refundsList = payManager.getRefundByOrderId(checks.get(i).getOrderId());
			if(refundsList != null){
				Double num = 0D;
				if(this.checks.get(i).getIsRefinfo() != null && this.checks.get(i).getIsRefinfo().equals("0")){
					for (int j = 0; j < refundsList.size(); j++) {
						num += refundsList.get(j).getRefPrice() == null ? 0D : Double.valueOf(refundsList.get(j).getRefPrice());
//						num += Double.valueOf(refundsList.get(j).getRefPrice());
					}
					if(this.checks.get(i).getIsRefStock() != null && this.checks.get(i).getIsRefStock().equals("1")){
						num = num + 10;
					}
					this.checks.get(i).setAllRefCount(num.toString());
				}
			}
		}
//    for (int i = 0; i < ctPays.size(); i++) {
//      for (int j = 0; j < this.checks.size(); j++) {
//        if (((CtOrderInfo)this.checks.get(j)).getOrderId().equals(((CtPay)ctPays.get(i)).getOrderId()))
//        {
//          ((CtOrderInfo)this.checks.get(j)).setPayPrice(((CtPay)ctPays.get(i)).getPayAmount().toString());
//          break;
//        }
//      }
//    }
		request.put("uidOrder", uid);
		
		request.put("stime", orderDTO.getStime());
		request.put("etime", orderDTO.getEtime());
	} catch (UnsupportedEncodingException e) {
		e.printStackTrace();
	}
    return "success";
  }
  private String rs;
  

public String getRs() {
	return rs;
}

public void setRs(String rs) {
	this.rs = rs;
}

//给订单添加加急原因
  public String upJiaJiChuli(){
	  CtOrderInfo orderInfo = orderManager.getOrderByOrderId(orderDTO.getOrderid());
	  orderInfo.setUrgentOrder(orderDTO.getUrgentOrder());
	  orderManager.updateOrderInfoByInfo(orderInfo);
	  return SUCCESS;
  }
  
  private double total = 0.0D;
  
  public double getTotal()
  {
    return this.total;
  }
  
  public void setTotal(double total)
  {
    this.total = total;
  }
  
  private CtPayInterface payInterface = new CtPayInterface();
  private String fileName;
  private String result;
  
  public CtPayInterface getPayInterface()
  {
    return this.payInterface;
  }
  
  public void setPayInterface(CtPayInterface payInterface)
  {
    this.payInterface = payInterface;
  }
  
  public String TabOrder()
  {
    try
    {
      Long orderId = this.orderDTO.getOrderid();
      this.ctOrderInfo = this.orderManager.getOrderByOrderId(orderId);
      Short shopId = this.ctOrderInfo.getSId();
      if ((shopId != null) && (shopId.shortValue() != 0))
      {
        String shopName = this.orderManager.getOrderByshopId(shopId);
        this.ctOrderInfo.setShopIdName(shopName);
      }
      if ((!this.ctOrderInfo.getOrderStatus().equals("0")) && (!this.ctOrderInfo.getPay().equals("1"))) {
        this.payInterface = this.orderManager.getPayInterByOrderSn(this.ctOrderInfo.getOrderSn());
      }
      this.manager = ((CtManager)this.session.get("admininfosession"));
      if (this.ctOrderInfo != null)
      {
        this.ctPay = this.payManager.getPayByOrderId(orderId);
        this.orderGoodsList = this.orderManager.getOrderGoodsByOrderId(orderId, 1);
        for (int i = 0; i < this.orderGoodsList.size(); i++)
        {
          String aa = ((CtOrderGoods)this.orderGoodsList.get(i)).getGSubtotal();
          this.total += Double.valueOf(aa).doubleValue();
        }
        this.orderGoodsList = this.orderManager.getOrderGoodsByOrderId(orderId, 0);
        for (int i = 0; i < this.orderGoodsList.size(); i++)
        {
          Double aa = Double.valueOf(Double.valueOf(((CtOrderGoods)this.orderGoodsList.get(i)).getGParPrice()).doubleValue() * Double.valueOf(((CtOrderGoods)this.orderGoodsList.get(i)).getGParNumber().longValue()).doubleValue());
          this.total += Double.valueOf(aa.doubleValue()).doubleValue();
        }
        this.session.put("total", Double.valueOf(this.total));
        if ((this.ctOrderInfo.getCouponId() != null) || ((this.orderGoodsList.size() > 0) && (((CtOrderGoods)this.orderGoodsList.get(0)).getCouponId() != null))) {
          this.couponDetail = new CtCouponDetail();
        }
      }
      else
      {
        return "error";
      }
      this.ctUser = this.userManager.getCtUserByUId(Long.valueOf(this.ctOrderInfo.getUId()));
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return "success";
  }
  
  public InputStream getDownloadFile()
    throws FileNotFoundException, UnsupportedEncodingException
  {
    System.out.println(getFileName());
    ServletActionContext.getResponse().setHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode(this.fileName, "UTF-8"));
    
    InputStream inputStream = ServletActionContext.getServletContext().getResourceAsStream("/update/" + getFileName());
    System.out.println(inputStream);
    return inputStream;
  }
  
  public String downloadFile()
    throws Exception
  {
    return "success";
  }
  
  public String getFileName()
    throws UnsupportedEncodingException
  {
    return this.fileName;
  }
  
  public void setFileName(String fileName)
    throws UnsupportedEncodingException
  {
    this.fileName = fileName;
  }
  
  public String getResult()
  {
    return this.result;
  }
  
  public void setResult(String result)
  {
    this.result = result;
  }
  
  public String updateTotalPrice()
  {
    String total = this.ctOrderInfo.getTotal();
    
    this.orderManager.updateTotalPriceByOrderId(this.ctOrderInfo);
    this.result = "修改成功";
    return "success";
  }
  
  public String TabOrder2()
  {
    Long orderId = this.orderDTO.getOrderid();
    this.ctOrderInfo = this.orderManager.getOrderByOrderId(orderId);
    this.manager = ((CtManager)this.session.get("admininfosession"));
    this.ctPay = this.payManager.getPayByOrderId(orderId);
    if (this.ctPay != null) {
      this.bank = this.bankManager.getBankById(this.ctPay.getBankId());
    }
    return "success";
  }
  
  public String updateType()
  {
    Long orderId = this.ctOrderInfo.getOrderId();
    this.orderManager.updateStartByOrderId(orderId, Long.valueOf(1L));
    this.result = "配货";
    return "success";
  }
  
  private List<CtExpress> ctExpressesList = new ArrayList();
  private CtEvaluation evaluation;
  
  public List<CtExpress> getCtExpressesList()
  {
    return this.ctExpressesList;
  }
  
  public void setCtExpressesList(List<CtExpress> ctExpressesList)
  {
    this.ctExpressesList = ctExpressesList;
  }
  
  public String updateTypeForClence()
  {
    Long orderId = this.ctOrderInfo.getOrderId();
    this.orderManager.updateStartByOrderId(orderId, Long.valueOf(7L));
    this.result = "无效，余额不足";
    return "success";
  }
  
  public String TabOrder3()
  {
    Long orderId = this.orderDTO.getOrderid();
    this.ctOrderInfo = this.orderManager.getOrderByOrderId(orderId);
    
    String sheng = this.orderManager.getShengById(this.ctOrderInfo.getProvince());
    String shi = this.orderManager.getShiById(this.ctOrderInfo.getCity());
    String qu = this.orderManager.getQuById(this.ctOrderInfo.getDistrict());
    this.session.put("sheng", sheng);
    this.session.put("shi", shi);
    this.session.put("qu", qu);
    
    String start = this.ctOrderInfo.getOrderStatus();
    this.page = new Page();
    if (this.orderDTO.getPage() == 0) {
      this.orderDTO.setPage(1);
    }
    this.page.setCurrentPage(this.orderDTO.getPage());
    this.manager = ((CtManager)this.session.get("admininfosession"));
    if ((!this.ctOrderInfo.getOrderStatus().equals("3")) && (!this.ctOrderInfo.getOrderStatus().equals("1")) && (!this.ctOrderInfo.getOrderStatus().equals("8")))
    {
      this.ctOrderInfo.setOrderStatus("0");
      this.ctOrderInfo.setOrderId(orderId);
      this.ctExpressesList = this.orderManager.getAllExpress(this.page);
    }
    else
    {
      this.ctExpressesList = this.orderManager.getAllExpress(this.page);
    }
    this.orderManager.updateStartByOrderId(this.ctOrderInfo.getOrderId(), Long.valueOf(start));
    return "success";
  }
  
  public String updateExNo()
  {
    this.orderManager.updateExNoByOrderId(this.ctOrderInfo);
    this.result = "发货成功";
    return "success";
  }
  public String beihuoOk(){
	  System.out.println(orderDTO.getOrderSn());
	  ctOrderInfo = orderManager.getOrderByOrderSn(orderDTO.getOrderSn());
	  ctOrderInfo.setOrderStatus("8");
	  ctOrderInfo.setUrgentOrder("-1");
	  ctOrderInfo.setStockingTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
	  orderManager.updateOrderInfoByInfo(ctOrderInfo);
	  return SUCCESS;
  }
  public String TabOrder4()
  {
    Long orderId = this.orderDTO.getOrderid();
    this.ctOrderInfo = this.orderManager.getOrderByOrderId(orderId);
    this.manager = ((CtManager)this.session.get("admininfosession"));
    String sa = "";
    double sta = 0.0D;
    double pta = 0.0D;
    if (this.ctOrderInfo != null)
    {
      this.orderGoodsSamList = this.orderManager.getOrderGoodsByOrderId(orderId, 1);
      this.orderGoodsList = this.orderManager.getOrderGoodsByOrderId(orderId, 2);
      for (int i = 0; i < this.orderGoodsSamList.size(); i++)
      {
        //CtGoods ctGoods = this.goodsManager.findById(((CtOrderGoods)this.orderGoodsSamList.get(i)).getGId());
        
        sa = ((CtOrderGoods)this.orderGoodsSamList.get(i)).getGSubtotal();
        sta += Double.valueOf(sa).doubleValue();
      }
      for (int i = 0; i < this.orderGoodsList.size(); i++)
      {
        //CtGoods ctGoods = this.goodsManager.findById(((CtOrderGoods)this.orderGoodsList.get(i)).getGId());
        
        sa = ((CtOrderGoods)this.orderGoodsList.get(i)).getGSubtotal();
        pta += Double.valueOf(sa).doubleValue();
      }
      this.total = (sta + pta);
      this.session.put("totalY", Double.valueOf(this.total));
    }
    this.orderDTO.setOrderSn(this.ctOrderInfo.getOrderSn());
    this.orderDTO.setOrderTime(this.ctOrderInfo.getOrderTime());
    this.orderDTO.setConsignee(this.ctOrderInfo.getConsignee());
    this.orderDTO.setTotal(this.ctOrderInfo.getTotal());
    return "success";
  }
  
  public CtEvaluation getEvaluation()
  {
    return this.evaluation;
  }
  
  public void setEvaluation(CtEvaluation evaluation)
  {
    this.evaluation = evaluation;
  }
  
  public String TabOrder5()
  {
    Long orderId = this.orderDTO.getOrderid();
    this.ctOrderInfo = this.orderManager.getOrderByOrderId(orderId);
    this.manager = ((CtManager)this.session.get("admininfosession"));
    if (this.ctOrderInfo.getEvaId() != null) {
      this.evaluation = this.orderManager.getEvaByOrderId(this.ctOrderInfo.getOrderId());
    }
    return "success";
  }
  private net.sf.json.JSONArray resultJson;
  
  public net.sf.json.JSONArray getResultJson() {
	return resultJson;
}

public void setResultJson(net.sf.json.JSONArray resultJson) {
	this.resultJson = resultJson;
}

//查询订单编号
  public String findOrderSnUi(){
//	  String orderSn = orderDTO.getOrderSn();
//	  List<CtOrderInfo> orderInfosList = orderManager.getOrderListByOrderSn(orderSn.toUpperCase());
//	  List<String> orderSnFind = new ArrayList<String>();
//	  if(orderInfosList != null){
//		  for (int i = 0; i < orderInfosList.size(); i++) {
//			  orderSnFind.add(orderInfosList.get(i).getOrderSn());
//		}
//	  }
//	  net.sf.json.JSONArray json = new net.sf.json.JSONArray();
//	  json = net.sf.json.JSONArray.fromObject(orderSnFind);
//	  resultJson = json;
	  return SUCCESS;
  }
  public String TabOrder6()
  {
    Long orderId = this.orderDTO.getOrderid();
    this.ctOrderInfo = this.orderManager.getOrderByOrderId(orderId);
    this.manager = ((CtManager)this.session.get("admininfosession"));
    return "success";
  }
  
  public String updateReturnByOrderId()
  {
    Long orderId = this.orderDTO.getOrderid();
    this.manager = ((CtManager)this.session.get("admininfosession"));
    String manName = this.manager.getMManagerId();
    this.orderManager.updateReturnStartByOrderId(orderId, manName);
    this.result = "库房确认商品已经回库";
    return "success";
  }
  
  public String TabOrder7()
  {
    Long orderId = this.orderDTO.getOrderid();
    this.ctOrderInfo = this.orderManager.getOrderByOrderId(orderId);
    this.manager = ((CtManager)this.session.get("admininfosession"));
    return "success";
  }
  
  public String updateReturnManageByOrderId()
  {
    Long orderId = this.orderDTO.getOrderid();
    this.manager = ((CtManager)this.session.get("admininfosession"));
    String manName = this.manager.getMManagerId();
    this.orderManager.updateReturnManageByOrderId(orderId, manName);
    this.result = "通过";
    return "success";
  }
  
  public String TabOrder8()
  {
    Long orderId = this.orderDTO.getOrderid();
    this.ctOrderInfo = this.orderManager.getOrderByOrderId(orderId);
    this.manager = ((CtManager)this.session.get("admininfosession"));
    return "success";
  }
  private List<CtRefund> refundsList = new ArrayList<CtRefund>();
  private CtRefund refund = new CtRefund();
  
  public List<CtRefund> getRefundsList() {
	return refundsList;
}

public void setRefundsList(List<CtRefund> refundsList) {
	this.refundsList = refundsList;
}

public CtRefund getRefund() {
	return refund;
}

public void setRefund(CtRefund refund) {
	this.refund = refund;
}
List<CtOrderGoods> orderGoodsListRefund = new ArrayList<CtOrderGoods>();

public List<CtOrderGoods> getOrderGoodsListRefund() {
	return orderGoodsListRefund;
}

public void setOrderGoodsListRefund(List<CtOrderGoods> orderGoodsListRefund) {
	this.orderGoodsListRefund = orderGoodsListRefund;
}

public String TabOrder9()
  {
	  Long orderId = this.orderDTO.getOrderid();
	  this.ctOrderInfo = this.orderManager.getOrderByOrderId(orderId);
	  refundsList = payManager.getRefundByOrderId(orderId);
	  double allPrice = 0;
	  double okPrice = 0;
	  double shengPrice = 0;
	  int type = 0;
	  boolean isTrue = false;
	  if(refundsList != null ){
		  for (int i = 0; i < refundsList.size(); i++) {
			  CtOrderGoods goods = new CtOrderGoods();
			  if(refundsList.get(i).getRefGoodsType().equals("0")){
				  type = 1;
			  } else {
				  type = 0;
			  }
			  goods = payManager.getOrderGoodsByRefund(refundsList.get(i).getRefOrderid(),refundsList.get(i).getRefGoodsid(),type);
			  if(goods != null ){
				  try {
					  goods.setRefPrice(refundsList.get(i).getRefPrice());
					orderGoodsListRefund.add(goods);
					  double num = Double.valueOf(goods.getGSubtotal());

					  allPrice = add(allPrice+"",num+"");
					  if(goods.getIsRefund().equals("1")){
						  shengPrice = add(shengPrice+"",num+"");
					  }
					  if(goods.getIsRefund().equals("2")){
						 // okPrice = add(okPrice,num);
					  }
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			  }
			  if(refundsList.get(i).getRefStatus().equals("2")){
				  okPrice = add(okPrice+"", refundsList.get(i).getRefPrice() == null ? "0" : refundsList.get(i).getRefPrice());
				  isTrue = true;
			  }
		  }
		  if(!isTrue){
			  okPrice = 0;
		  }
		  session.put("allPrice", allPrice);
		  session.put("okPrice", okPrice);
		  session.put("shengPrice", shengPrice);
		  session.put("zanwurefund", "you");
	  } else {
		  session.put("zanwurefund", "zanwurefund");
	  }
	  return "success";
  }
public static double add(String v1, String v2) {  
    BigDecimal b1 = new BigDecimal(v1);  
    BigDecimal b2 = new BigDecimal(v2);  
    return b1.add(b2).doubleValue();  
} 
/**
 * 获取随机字符串
 * 
 * @return
 */
public static String getNonceStr() {
	// 随机数
	String currTime = TenpayUtil.getCurrTime();
	// 8位日期
	String strTime = currTime.substring(8, currTime.length());
	// 四位随机数
	String strRandom = TenpayUtil.buildRandom(4) + "";
	// 10位序列号,可以自行调整。
	return strTime + strRandom;
}
/**
 * 元转换成分
 * 
 * @param money
 * @return
 */
public static String getMoney(String amount) {
	if (amount == null) {
		return "";
	}
	// 金额转化为分为单位
	String currency = amount.replaceAll("\\$|\\￥|\\,", ""); // 处理包含, ￥
															// 或者$的金额
	int index = currency.indexOf(".");
	int length = currency.length();
	Long amLong = 0l;
	if (index == -1) {
		amLong = Long.valueOf(currency + "00");
	} else if (length - index >= 3) {
		amLong = Long.valueOf((currency.substring(0, index + 3)).replace(
				".", ""));
	} else if (length - index == 2) {
		amLong = Long.valueOf((currency.substring(0, index + 2)).replace(
				".", "") + 0);
	} else {
		amLong = Long.valueOf((currency.substring(0, index + 1)).replace(
				".", "") + "00");
	}
	return amLong.toString();
}

	//支付宝退款返回
	public String alipayRefundNotify(){
		try {
			refundsList = new ArrayList<CtRefund>();
			requ = ServletActionContext.getRequest();
			//获取支付宝POST过来反馈信息
 			Map<String,String> params = new HashMap<String,String>();
			Map requestParams = requ.getParameterMap();
			String[] refId = null;
			for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
				String name = (String) iter.next();
				String[] values = (String[]) requestParams.get(name);
				String valueStr = "";
				for (int i = 0; i < values.length; i++) {
					valueStr = (i == values.length - 1) ? valueStr + values[i]
							: valueStr + values[i] + ",";
				}
				//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
				//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
				if(name.equals("refundCtId")){
					refId = valueStr.split("_");
				}
				params.put(name, valueStr);
			}
			
			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)//
			//批次号

			String batch_no = new String(requ.getParameter("batch_no").getBytes("ISO-8859-1"),"UTF-8");
			
			String notify_id = new String(requ.getParameter("notify_id").getBytes("ISO-8859-1"),"UTF-8");

			//批量退款数据中转账成功的笔数

			String success_num = new String(requ.getParameter("success_num").getBytes("ISO-8859-1"),"UTF-8");

			//批量退款数据中的详细信息
			String result_details = new String(requ.getParameter("result_details").getBytes("ISO-8859-1"),"UTF-8");

			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以上仅供参考)//

			if(AlipayNotify.verify(params)){//验证成功
				//////////////////////////////////////////////////////////////////////////////////////////
				//请在这里加上商户的业务逻辑程序代码

				//——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
				
				//判断是否在商户网站中已经做过了这次通知返回的处理
					//如果没有做过处理，那么执行商户的业务程序
					//如果有做过处理，那么不执行商户的业务程序
				//String[] getAliOrder = result_details.split("\\^");
				String[] chai = batch_no.split("I");
				
				ActionContext context = ActionContext.getContext();
				Map<String,Object> application = context.getApplication();
				
				
				String chais = (String) application.get("dindanId"+batch_no.substring(batch_no.indexOf("CT"),batch_no.indexOf("I")));
				chai = chais.split("I");
				for (int i = 0; i < chai.length; i++) { 
					try {
						CtRefund cr = payManager.findOPatIntById(chai[i]);
						if(cr != null){
							refundsList.add(cr);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				ctOrderInfo = orderManager.getOrderByOrderSn(batch_no.substring(batch_no.indexOf("CT"),batch_no.indexOf("I")));
				ctOrderInfo.setIsRefinfo("0");
				orderManager.updateOrderInfoByInfo(ctOrderInfo);
				for (int i = 0; i < refundsList.size(); i++) {
					payManager.updateRRefundOk(refundsList.get(i),notify_id,batch_no.substring(batch_no.indexOf("CT")));
					int type = 0;
					 if(refundsList.get(i).getRefGoodsType().equals("0")){
						  type = 1;
					  } else {
						  type = 0;
					  }
					ctOrderGoods = payManager.getOrderGoodsByRefund(refundsList.get(i).getRefOrderid(), refundsList.get(i).getRefGoodsid(), type);
					ctOrderGoods.setIsRefund("2");
					payManager.updateOrderGoods(ctOrderGoods);
				}
				response.getWriter().print("success");	//请不要修改或删除

				//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
				application.remove("dindanId"+batch_no.substring(batch_no.indexOf("CT"),batch_no.indexOf("I")));
				//////////////////////////////////////////////////////////////////////////////////////////
			}else{//验证失败
				response.getWriter().print("fail");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	//支付宝退款返回alipayRefundReturn
	public String alipayRefundReturn(){
		try {
			refundsList = new ArrayList<CtRefund>();
			requ = ServletActionContext.getRequest();
			//获取支付宝POST过来反馈信息
			Map<String,String> params = new HashMap<String,String>();
			Map requestParams = requ.getParameterMap();
			String[] refId = null;
			for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
				String name = (String) iter.next();
				String[] values = (String[]) requestParams.get(name);
				String valueStr = "";
				for (int i = 0; i < values.length; i++) {
					valueStr = (i == values.length - 1) ? valueStr + values[i]
							: valueStr + values[i] + ",";
				}
				//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
				//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
				if(name.equals("refundCtId")){
					refId = valueStr.split("_");
				}
				params.put(name, valueStr);
			}
			
			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)//
			//批次号
			
			String batch_no = new String(requ.getParameter("batch_no").getBytes("ISO-8859-1"),"UTF-8");
			
			String notify_id = new String(requ.getParameter("notify_id").getBytes("ISO-8859-1"),"UTF-8");
			
			//批量退款数据中转账成功的笔数
			
			String success_num = new String(requ.getParameter("success_num").getBytes("ISO-8859-1"),"UTF-8");
			
			//批量退款数据中的详细信息
			String result_details = new String(requ.getParameter("result_details").getBytes("ISO-8859-1"),"UTF-8");
			
			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以上仅供参考)//
			
			if(AlipayNotify.verify(params)){//验证成功
				//////////////////////////////////////////////////////////////////////////////////////////
				//请在这里加上商户的业务逻辑程序代码
				
				//——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
				
				//判断是否在商户网站中已经做过了这次通知返回的处理
				//如果没有做过处理，那么执行商户的业务程序
				//如果有做过处理，那么不执行商户的业务程序
				//String[] getAliOrder = result_details.split("\\^");
//				String[] chai = batch_no.split("I");
////				
//				String chais = (String) session.get("dindanId");
//				chai = chais.split("I");
//				for (int i = 0; i < chai.length; i++) {
//					CtRefund cr = payManager.findOPatIntById(chai[i]);
//					if(cr != null){
//						refundsList.add(cr);
//					}
//				}
				
				String[] chai = batch_no.split("I");
				
				ActionContext context = ActionContext.getContext();
				Map<String,Object> application = context.getApplication();
				
				
				String chais = (String) application.get("dindanId"+batch_no.substring(batch_no.indexOf("CT"),batch_no.indexOf("I")));
				chai = chais.split("I");
				for (int i = 0; i < chai.length; i++) { 
					try {
						CtRefund cr = payManager.findOPatIntById(chai[i]);
						if(cr != null){
							refundsList.add(cr);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				application.remove("dindanId"+batch_no.substring(batch_no.indexOf("CT"),batch_no.indexOf("I")));
				
				
				ctOrderInfo = orderManager.getOrderByOrderSn(batch_no.substring(batch_no.indexOf("CT"),batch_no.indexOf("I")));
				ctOrderInfo.setIsRefinfo("0");
				orderManager.updateOrderInfoByInfo(ctOrderInfo);
				for (int i = 0; i < refundsList.size(); i++) {
					payManager.updateRRefundOk(refundsList.get(i),notify_id,batch_no.substring(batch_no.indexOf("CT")));
					int type = 0;
					if(refundsList.get(i).getRefGoodsType().equals("0")){
						type = 1;
					} else {
						type = 0;
					}
					ctOrderGoods = payManager.getOrderGoodsByRefund(refundsList.get(i).getRefOrderid(), refundsList.get(i).getRefGoodsid(), type);
					ctOrderGoods.setIsRefund("2");
					payManager.updateOrderGoods(ctOrderGoods);
				}
				response.getWriter().print("success");	//请不要修改或删除
				
				//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
				
				//////////////////////////////////////////////////////////////////////////////////////////
			}else{//验证失败
				response.getWriter().print("fail");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String alipayRefundReturn1(){
		try {
			refundsList = (List<CtRefund>) session.get("refundsList");
			requ = ServletActionContext.getRequest();
			//获取支付宝POST过来反馈信息
			Map<String,String> params = new HashMap<String,String>();
			Map requestParams = requ.getParameterMap();
			for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
				String name = (String) iter.next();
				String[] values = (String[]) requestParams.get(name);
				String valueStr = "";
				for (int i = 0; i < values.length; i++) {
					valueStr = (i == values.length - 1) ? valueStr + values[i]
							: valueStr + values[i] + ",";
				}
				//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
				//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
				params.put(name, valueStr);
			}
			
			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)//
			//批次号

			String batch_no = new String(requ.getParameter("batch_no").getBytes("ISO-8859-1"),"UTF-8");

			//批量退款数据中转账成功的笔数

			String success_num = new String(requ.getParameter("success_num").getBytes("ISO-8859-1"),"UTF-8");

			//批量退款数据中的详细信息
			String result_details = new String(requ.getParameter("result_details").getBytes("ISO-8859-1"),"UTF-8");

			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以上仅供参考)//

			if(AlipayNotify.verify(params)){//验证成功
				//////////////////////////////////////////////////////////////////////////////////////////
				//请在这里加上商户的业务逻辑程序代码

				//——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
				
				//判断是否在商户网站中已经做过了这次通知返回的处理
					//如果没有做过处理，那么执行商户的业务程序
					//如果有做过处理，那么不执行商户的业务程序
//				String[] getAliOrder = result_details.split("\\^");
				String[] chai = batch_no.split("I");
//				
				String chais = (String) session.get("dindanId");
				chai = chais.split("I");
				for (int i = 0; i < chai.length; i++) {
					CtRefund cr = payManager.findOPatIntById(chai[i]);
					if(cr != null){
						refundsList.add(cr);
					}
				}
				ctOrderInfo = orderManager.getOrderByOrderSn(batch_no.substring(batch_no.indexOf("CT"),batch_no.indexOf("I")));
				ctOrderInfo.setIsRefinfo("0");
				orderManager.updateOrderInfoByInfo(ctOrderInfo);
				for (int i = 0; i < refundsList.size(); i++) {
//					payManager.updateRRefundOk(refundsList.get(i),notify_id,batch_no.substring(batch_no.indexOf("CT")));
					int type = 0;
					 if(refundsList.get(i).getRefGoodsType().equals("0")){
						  type = 0;
					  } else {
						  type = 1;
					  }
					ctOrderGoods = payManager.getOrderGoodsByRefund(refundsList.get(i).getRefOrderid(), refundsList.get(i).getRefGoodsid(), type);
					ctOrderGoods.setIsRefund("2");
					payManager.updateOrderGoods(ctOrderGoods);
				}
				response.getWriter().print("success");	//请不要修改或删除

				//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

				//////////////////////////////////////////////////////////////////////////////////////////
			}else{//验证失败
				response.getWriter().print("fail");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	
	//不同意退款信息
	public String noRefund(){
		int norefNum = 0;
		String orderSn = orderDTO.getOrderSn();
		String data = orderDTO.getData();
		ctOrderInfo = orderManager.getOrderByOrderSn(orderSn);
		List<CtRefund>  refunds = payManager.getRefundByOrderId(ctOrderInfo.getOrderId());
		
		for (int i = 0; i < refunds.size(); i++) {
			String[] findData = data.split("\\++");
			for (int j = 0; j < findData.length; j++) {
				String[] dedata = findData[j].split("__");
				if(dedata != null && dedata.length == 3){
					if(dedata[2].equals("0")){
						dedata[2] = "1";
					} else {
						dedata[2] = "0";
					}
					if(dedata[0].equals(refunds.get(i).getRefGoodsid().toString()) &&
							dedata[1].equals(refunds.get(i).getRefOrderid().toString()) && 
							dedata[2].equals(refunds.get(i).getRefGoodsType())){
						refunds.get(i).setRefStatus("3");
						norefNum++;
						payManager.updateRef(refunds.get(i));
						//更新订单商品信息为退款失败
						orderManager.updateOrderGoodsByOidAndGidAndType(refunds.get(i).getRefGoodsid(),refunds.get(i).getRefOrderid(),refunds.get(i).getRefGoodsType().equals("0")? "1" : "0");
					}
				}
			}
		}
//		
		List<CtRefund>  refundsNew = payManager.getRefundByOrderId(ctOrderInfo.getOrderId());
		for (int i = 0; i < refundsNew.size(); i++) {
			if(refundsNew.get(i).getRefStatus().equals("3")){
				norefNum++;
			}
		}                                         
		if(norefNum == refunds.size()){
			ctOrderInfo.setIsRefinfo("3");
			orderManager.updateOrderInfoByInfo(ctOrderInfo);
		}
		
//		orderManager.updateOrderGoodsByOrderId(ctOrderInfo.getOrderId());
//		orderManager.update
		result = "驳回成功";
		return SUCCESS;
	}
	//退款
	public String goRefund() throws Exception {
		
		String backXml="";
		String refund_idBack="";
		String out_refund_noBack="";
		boolean isRefStock = false;
//		try {
			boolean refundok = false;
			String tuik = orderDTO.getTotal();
			String data = orderDTO.getData();
			String yun = orderDTO.getDiscount();
			String[] fen = data.split("_____");
			String payType = "";
			for (int i = 0; i < fen.length; i++) {
				String[] chai = fen[i].split("__");
				if(chai[2].equals("0")){
					chai[2]="1";
				  } else {
					chai[2]="0";
				  }
				refund = payManager.getRefundByOrderIdAndGidAndType(chai[0], chai[1], chai[2]);
				if(refund != null){
					payType = refund.getRefPaytype();
					refundsList.add(refund);
				}
				ctOrderInfo = orderManager.getOrderByOrderId(Long.valueOf(chai[1]));
			}
			Double allPrice = 0D;
			for (int i = 0; i < refundsList.size(); i++) {
				 CtOrderGoods goods = new CtOrderGoods();
				 int type = 0;
				 if(refundsList.get(i).getRefGoodsType().equals("0")){
					  type = 1;
				  } else {
					  type = 0;
				  }
				 goods = payManager.getOrderGoodsByRefund(refundsList.get(i).getRefOrderid(),refundsList.get(i).getRefGoodsid(),type);
				  if(goods != null ){
					  orderGoodsListRefund.add(goods);
					  //allPrice += Double.valueOf(goods.getGSubtotal());
					  allPrice = add(allPrice.toString(),goods.getGSubtotal());
				  }
				  refundsList.get(i).setRefPrice(goods.getGSubtotal());
				  payManager.updateRef(refundsList.get(i));
			}
			if(!tuik.equals("") && tuik != null){
				String[] p = tuik.split("\\|");
				Double d = 0D;
				for (int i = 1; i < p.length; i++) {
					String[] pp = p[i].split("\\+\\+");
					d = add(d.toString(),pp[0]);
					allPrice = add(allPrice.toString(),pp[0]);
					String[] c = pp[1].split("__");
					if(c[2].equals("0")){
						c[2]="1";
					  } else {
						c[2]="0";
					  }
					refund = payManager.getRefundByOrderIdAndGidAndType(c[0], c[1], c[2]);
					Double newPrice = add(refund.getRefPrice(),pp[0]);
					refund.setRefPrice(newPrice.toString());
					payManager.updateRef(refund);
				}
				//allPrice = d;
			}
			if(!yun.equals("0") && !yun.equals("") && yun != null){
				allPrice = add(allPrice.toString(),10+"");
				isRefStock = true;
			}
			if(!ctOrderInfo.getPay().equals("1")){
				//支付宝
				if(payType.equals("2")){
					
					payInterface = payManager.getPayInterByOrderSn(ctOrderInfo.getOrderSn());
					String dindanid = "I";
					for (int i = 0; i < refundsList.size(); i++) {
						dindanid += refundsList.get(i).getRefId()+"I";
					}
					ActionContext context = ActionContext.getContext();
					Map<String,Object> application = context.getApplication();
					application.put("dindanId"+ctOrderInfo.getOrderSn(), dindanid);
//					session.put("dindanId", dindanid);
					String batch_no = new SimpleDateFormat("yyyyMMdd").format(new Date())+ctOrderInfo.getOrderSn()+"I1";
					
					//退款笔数，必填，参数detail_data的值中，“#”字符出现的数量加1，最大支持1000笔（即“#”字符出现的数量999个）
					
					String batch_num = "1";
					
					//退款详细数据，必填，格式（支付宝交易号^退款金额^备注），多笔请用#隔开
					String detail_data = payInterface.getTradeNo()+"^"+allPrice+"^"+"客户退款";
					
					Map<String, String> sParaTemp = new HashMap<String, String>();
					sParaTemp.put("service", AlipayConfig.service);
					sParaTemp.put("partner", AlipayConfig.partner);
					sParaTemp.put("_input_charset", AlipayConfig.input_charset);
					sParaTemp.put("notify_url", AlipayConfig.notify_url);
					sParaTemp.put("seller_user_id", AlipayConfig.seller_user_id);
					sParaTemp.put("refund_date", AlipayConfig.refund_date);
					sParaTemp.put("batch_no", batch_no);
					sParaTemp.put("batch_num", batch_num);
					sParaTemp.put("detail_data", detail_data);
					
					//建立请求
					String sHtmlText = AlipaySubmit.buildRequest(sParaTemp,"get","确认",refundsList);
					//out.println(sHtmlText);
					result=sHtmlText;
					session.put("refundsList", refundsList);
					return SUCCESS;
				}
				//微信
				if(payType.equals("3")){
					String appid = AlipayConfig.appid;
					String mch_id = AlipayConfig.partner_Weixin;
					String nonce_str = getNonceStr();
					String out_trade_no = ctOrderInfo.getOrderSn();
					String out_refund_no = ctOrderInfo.getOrderSn() + "_____" + new String(new Date().getTime() + "").substring(2, 8);
					String total_fee = getMoney(payManager.getPayInterByOrderSn(ctOrderInfo.getOrderSn()).getTotalFee());
					String refund_fee = getMoney(allPrice.toString());
					String op_user_id = AlipayConfig.partner_Weixin;
					SortedMap<String, String> packageParams = new TreeMap<String, String>();
					packageParams.put("appid", appid);
					packageParams.put("mch_id", mch_id);
					packageParams.put("nonce_str", nonce_str);
					packageParams.put("out_trade_no", out_trade_no);
					packageParams.put("out_refund_no", out_refund_no);
					packageParams.put("total_fee", total_fee);
					packageParams.put("refund_fee", refund_fee);
					packageParams.put("op_user_id", op_user_id);
					
					RequestHandler requestHandler = new RequestHandler(null, null);
					requestHandler.init(AlipayConfig.appid, AlipayConfig.appsecret, AlipayConfig.partnerkey);
					String sign = requestHandler.createSign(packageParams);
					
					StringBuffer xmlsb = new StringBuffer();
					xmlsb.append("<xml>");
					xmlsb.append("   <appid>"+appid+"</appid>");
					xmlsb.append("   <mch_id>"+mch_id+"</mch_id>");
					xmlsb.append("   <nonce_str><![CDATA["+nonce_str+"]]></nonce_str>");
					xmlsb.append("   <op_user_id><![CDATA["+op_user_id+"]]></op_user_id>");
					xmlsb.append("   <out_refund_no><![CDATA["+out_refund_no+"]]></out_refund_no>");
					xmlsb.append("   <out_trade_no><![CDATA["+out_trade_no+"]]></out_trade_no>");
					xmlsb.append("   <transaction_id></transaction_id>");
					xmlsb.append("   <refund_fee>"+refund_fee+"</refund_fee>");
					xmlsb.append("   <total_fee>"+total_fee+"</total_fee>");
					xmlsb.append("   <sign>"+sign+"</sign>");
					xmlsb.append("</xml>");
					String createOrderURL = "https://api.mch.weixin.qq.com/secapi/pay/refund";
					backXml = new GetWxOrderno().doSendMoney(createOrderURL, xmlsb.toString());
					if(!backXml.split("\\|")[0].equals("error")){
						Map map = new GetWxOrderno().doXMLParse(backXml);
						refund_idBack  = (String) map.get("refund_id");
						out_refund_noBack = (String) map.get("out_refund_no");
						out_refund_noBack = out_refund_noBack.split("_____")[0];
						ctOrderInfo = orderManager.getOrderByOrderSn(out_refund_noBack);
						ctOrderInfo.setIsRefinfo("0");
						orderManager.updateOrderInfoByInfo(ctOrderInfo);
						for (int i = 0; i < refundsList.size(); i++) {
							payManager.updateRRefundOk(refundsList.get(i),refund_idBack,out_refund_noBack);
							int type = 0;
							if(refundsList.get(i).getRefGoodsType().equals("0")){
								type = 1;
							} else {	
								type = 0;
							}
							ctOrderGoods = payManager.getOrderGoodsByRefund(refundsList.get(i).getRefOrderid(), refundsList.get(i).getRefGoodsid(), type);
							ctOrderGoods.setIsRefund("2");
							payManager.updateOrderGoods(ctOrderGoods);
						}
						refundok = true;
					} else {
						result = backXml.split("\\|")[1];
						refundok = false;
					}
				}
				//银联
				if(payType.equals("4")){
					
				}
				//财付通
				if(payType.equals("5")){
					
				}
			} else {
//				ctOrderInfo = orderManager.getOrderByOrderSn(out_refund_noBack);
				ctOrderInfo.setIsRefinfo("0");
				orderManager.updateOrderInfoByInfo(ctOrderInfo);
				for (int i = 0; i < refundsList.size(); i++) {
					payManager.updateRRefundOk(refundsList.get(i),ctOrderInfo.getOrderSn(),ctOrderInfo.getOrderSn()+(new Date().getTime()+""));
					int type = 0;
					if(refundsList.get(i).getRefGoodsType().equals("0")){
						type = 1;
					} else {	
						type = 0;
					}
					ctOrderGoods = payManager.getOrderGoodsByRefund(refundsList.get(i).getRefOrderid(), refundsList.get(i).getRefGoodsid(), type);
					ctOrderGoods.setIsRefund("2");
					payManager.updateOrderGoods(ctOrderGoods);
				}
				refundok = true;
			}
			if(refundok){
				refundsList = payManager.getRefundByOrderId(ctOrderInfo.getOrderId());
				int typeeq1 = 0;
				int type = 0;
				if(refundsList != null ){
					for (int i = 0; i < refundsList.size(); i++) {
						CtOrderGoods goods = new CtOrderGoods();
						if(refundsList.get(i).getRefGoodsType().equals("0")){
							type = 0;
						} else {
							type = 1;
						}
						goods = payManager.getOrderGoodsByRefund(refundsList.get(i).getRefOrderid(),refundsList.get(i).getRefGoodsid(),type);
						if(goods != null ){
							orderGoodsListRefund.add(goods);
							allPrice += Double.valueOf(goods.getGSubtotal());
							if(goods.getIsRefund().equals("1")){
								typeeq1++;
							}
						}
					}
					
				}
				if(typeeq1 == 0){
					ctOrderInfo.setIsRefinfo("0");
					if(isRefStock){
						ctOrderInfo.setIsRefStock("1");
					} else {
						ctOrderInfo.setIsRefStock("0");
					}
					payManager.updateOrderInfo(ctOrderInfo);
				}
				result= "退款成功";
			} else {
				result += ",退款失败";
			}
				return SUCCESS;
//		} catch (NumberFormatException e) {
//			e.printStackTrace();
//			result=backXml+","+refund_idBack+","+out_refund_noBack+","+refundsList.size()+"退款失败1,"+e.getMessage();
//			return SUCCESS;
//		} catch (Exception e) {
//			e.printStackTrace();
//			result=backXml+","+refund_idBack+","+out_refund_noBack+","+refundsList.size()+"退款失败2,"+e.getMessage();
//			return SUCCESS;
//		}
	}

  public String updateReturnFinanceByOrderId()
  {
    Long orderId = this.orderDTO.getOrderid();
    this.manager = ((CtManager)this.session.get("admininfosession"));
    String manName = this.manager.getMManagerId();
    this.orderManager.updateReturnFinanceByOrderId(orderId, manName);
    this.result = "已经退款";
    return "success";
  }
  
  public String complainList()
  {
	  
    try {
		this.page = new Page();
		if (this.orderDTO.getPage() == 0) {
		  this.orderDTO.setPage(1);
		}
		this.page.setCurrentPage(this.orderDTO.getPage());
		this.complainList = this.orderManager.getAllComplain(this.page);
		this.page.setTotalPage(this.page.getTotalPage());
		this.request.put("pages", this.page);
	} catch (Exception e) {
		e.printStackTrace();
	}
    return "success";
  }
  
  public String complainGetOne()
  {
    this.complain = this.orderManager.getComplainByComId(this.complain.getComId());
    return "success";
  }
  
  public String complainUpdateOk()
  {
    this.orderManager.updateComplainByComId(this.complain);
    return "success";
  }
  
  public List<CtOrderInfo> test()
  {
    ApplicationContext c = new ClassPathXmlApplicationContext(new String[] { "applicationContext.xml", 
      "spring/applicationContext-order.xml", "spring/applicationContext-basic.xml", "spring/applicationContext-goods.xml", 
      "spring/applicationContext-manager.xml", "spring/applicationContext-pay.xml", "spring/applicationContext-role.xml", 
      "spring/applicationContext-service.xml", "spring/applicationContext-user.xml" });
    OrderManager manager = (OrderManager)c.getBean("orderManager", OrderManager.class);
    List<CtOrderInfo> list = manager.getIsDis();
    return list;
  }
  
  public int updateChaoQue(String orderId)
  {
    ApplicationContext c = new ClassPathXmlApplicationContext(new String[] { "applicationContext.xml", 
      "spring/applicationContext-order.xml", "spring/applicationContext-basic.xml", "spring/applicationContext-goods.xml", 
      "spring/applicationContext-manager.xml", "spring/applicationContext-pay.xml", "spring/applicationContext-role.xml", 
      "spring/applicationContext-service.xml", "spring/applicationContext-user.xml" });
    OrderManager manager = (OrderManager)c.getBean("orderManager", OrderManager.class);
    int count = manager.updateStartByOrderId(orderId);
    return count;
  }
  
  public String exlist()
  {
    try
    {
      Page page = new Page();
      if (this.orderDTO.getPage() == 0) {
        this.orderDTO.setPage(1);
      }
      page.setCurrentPage(this.orderDTO.getPage());
      this.express = this.orderManager.getAllExpress(page);
      page.setTotalPage(page.getTotalPage());
      this.request.put("pages", page);
      return "success";
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return "error";
  }
  
  public String saveExpress()
  {
    try
    {
      CtExpress express = new CtExpress();
      express.setExName(this.orderDTO.getExName());
      express.setExUrl(this.orderDTO.getExUrl());
      express.setExDesc(this.orderDTO.getExDesc());
      Integer aa = Integer.valueOf(this.orderManager.save(express));
      return "success";
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return "error";
  }
  
  public String find()
  {
    try
    {
      Page page = new Page();
      if (this.orderDTO.getPage() == 0) {
        this.orderDTO.setPage(1);
      }
      Integer id = this.orderDTO.getId();
      this.expres = new CtExpress();
      this.expres.setExId(id);
      System.out.println(id);
      this.expres = this.orderManager.findById(id);
      this.orderDTO.setExName(this.expres.getExName());
      this.orderDTO.setExUrl(this.expres.getExUrl());
      this.orderDTO.setExDesc(this.expres.getExDesc());
      return "success";
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return "error";
  }
  
  public String update()
  {
    try
    {
      CtExpress m = new CtExpress();
      Integer mid = this.orderDTO.getExId();
      m = this.orderManager.findById(this.orderDTO.getId());
      
      m.setExUrl(this.orderDTO.getExUrl());
      m.setExDesc(this.orderDTO.getExDesc());
      m.setExName(this.orderDTO.getExName());
      this.orderManager.update(m);
      return "success";
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return "error";
  }
  
  public String del()
  {
    try
    {
      Integer[] arrayOfInteger;
      int j = (arrayOfInteger = this.orderDTO.getMid()).length;
      for (int i = 0; i < j; i++)
      {
        int m = arrayOfInteger[i].intValue();
        this.orderManager.delete(Integer.valueOf(m));
      }
      return "success";
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return "error";
  }
  
  public String searchExpress()
  {
    try
    {
      Page page = new Page();
      if (this.orderDTO.getPage() == 0) {
        this.orderDTO.setPage(1);
      }
      page.setCurrentPage(this.orderDTO.getPage());
      if ((this.orderDTO.getKeyword() != null) && 
        (!this.orderDTO.getKeyword().equals("请输入关键字"))) {
        this.express = this.orderManager.findAll(this.orderDTO.getKeyword(), page);
      } else {
        this.express = this.orderManager.getAllExpress(page);
      }
      page.setTotalPage(page.getTotalPage());
      this.request.put("pages", page);
      return "success";
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return "error";
  }
  
  public String banklist()
  {
    try
    {
      Page page = new Page();
      if (this.orderDTO.getPage() == 0) {
        this.orderDTO.setPage(1);
      }
      page.setCurrentPage(this.orderDTO.getPage());
      this.banks = this.bankManager.loadAll(page);
      page.setTotalPage(page.getTotalPage());
      this.request.put("pages", page);
      return "success";
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return "error";
  }
  
  public String saveBank()
  {
    try
    {
      CtBank bank = new CtBank();
      bank.setDeposit(this.orderDTO.getDeposit());
      bank.setAccountName(this.orderDTO.getAccountName());
      bank.setAccountNumber(this.orderDTO.getAccountNumber());
      this.bankManager.save(bank);
      return "success";
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return "error";
  }
  
  public String findBank()
  {
    try
    {
      Page page = new Page();
      if (this.orderDTO.getPage() == 0) {
        this.orderDTO.setPage(1);
      }
      Integer id = this.orderDTO.getId();
      this.bank = new CtBank();
      this.bank.setBankId(Byte.valueOf(id.byteValue()));
      this.bank = this.bankManager.findById(Byte.valueOf(id.byteValue()));
      this.orderDTO.setDeposit(this.bank.getDeposit());
      this.orderDTO.setAccountName(this.bank.getAccountName());
      this.orderDTO.setAccountNumber(this.bank.getAccountNumber());
      return "success";
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return "error";
  }
  
  public String updateBank()
  {
    try
    {
      CtBank m = new CtBank();
      Integer mid = Integer.valueOf(this.orderDTO.getBankId().byteValue());
      m = this.bankManager.findById(Byte.valueOf(this.orderDTO.getId().byteValue()));
      m.setDeposit(this.orderDTO.getDeposit());
      m.setAccountName(this.orderDTO.getAccountName());
      m.setAccountNumber(this.orderDTO.getAccountNumber());
      this.bankManager.update(m);
      return "success";
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return "error";
  }
  
  public String delBank()
  {
    try
    {
      Integer[] arrayOfInteger;
      int j = (arrayOfInteger = this.orderDTO.getMid()).length;
      for (int i = 0; i < j; i++)
      {
        int m = arrayOfInteger[i].intValue();
        this.bankManager.delete(Integer.valueOf(m));
      }
      return "success";
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return "error";
  }
  
  public String searchBank()
  {
    try
    {
      Page page = new Page();
      if (this.orderDTO.getPage() == 0) {
        this.orderDTO.setPage(1);
      }
      page.setCurrentPage(this.orderDTO.getPage());
      if ((this.orderDTO.getKeyword() != null) && 
        (!this.orderDTO.getKeyword().equals("请输入关键字"))) {
        this.banks = this.bankManager.findAll(this.orderDTO.getKeyword(), page);
      } else {
        this.banks = this.bankManager.loadAll(page);
      }
      page.setTotalPage(page.getTotalPage());
      this.request.put("pages", page);
      return "success";
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return "error";
  }
  
  private CtUserDTO userDTO = new CtUserDTO();
  
  public CtUserDTO getUserDTO()
  {
    return this.userDTO;
  }
  
  public void setUserDTO(CtUserDTO userDTO)
  {
    this.userDTO = userDTO;
  }
  
  public String updateuser()
  {
    try
    {
      CtUser m = new CtUser();
      m = this.userManager.getCtUserByUId(this.userDTO.getId());
      this.session.put("m", m);
      String dis = "";
      String distribut = this.userDTO.getUDistribution();
      if (distribut.trim().equals("是")) {
        dis = "1";
      } else {
        dis = "0";
      }
      m.setUDistribution(dis);
      String maps = "分销用户:(" + dis + ")";
      this.session.remove("updates");
      this.session.put("updates", maps);
      
      this.orderManager.updateuser(m);
      
      return "success";
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return "error";
  }
  
  //客服备注
  public String upkfdesInfo(){
	  try {
		System.out.println(orderDTO.getKfDsc());
		  if(orderDTO.getKfDsc().equals("") || orderDTO.getKfDsc() == null){
			  result = "备注内容为空";
		  } else {
			  CtOrderInfo info = orderManager.getOrderByOrderId(orderDTO.getOrderid());
			  info.setKfDsc(orderDTO.getKfDsc());
			  orderManager.updateOrderInfoByInfo(info);
			  if(info.getOrderStatus().equals("1") || info.getOrderStatus().equals("4") || info.getOrderStatus().equals("8") || info.getOrderStatus().equals("5")){
				  EJLInfoServiceImplDelegate delegate = new EJLInfoServiceImplService().getEJLInfoServiceImplPort();
				  JdpCtegoTrade jdpTrade = delegate.getAllTradeByOrderId(orderDTO.getOrderid());
				  jdpTrade.setJdpModified(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				  jdpTrade.setModified(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				  StringBuffer json = new StringBuffer(jdpTrade.getJdpResponse());
				  int desStart = json.indexOf("kfDsc") + 8;
				  if(desStart-8 == -1){
					  int endshuang = json.lastIndexOf("\"");
					  String newjson = json.substring(0, endshuang) + "\",\"kfDsc\":\"" + orderDTO.getKfDsc() + "\"}}}";
					  jdpTrade.setJdpResponse(newjson);
				  } else {
					  int desEnd = json.indexOf(",",desStart) - 1;
					  String des = json.substring(desStart, desEnd);
					  des = orderDTO.getKfDsc();
					  String jsonNew = json.substring(0, desStart) + des + json.substring(desEnd, json.length());
					  jdpTrade.setJdpResponse(jsonNew);
				  }
				  delegate.jdpUpdateInfo(jdpTrade);
			  }
			  result="备注成功";
		  }
	} catch (JSONException e) {
		e.printStackTrace();
		result=e.getMessage();
	}
	  
	  return SUCCESS;
  }
  //查询订单编号
  public String getOrderByOrderSn(){
	  ctOrderInfo = orderManager.getOrderByOrderSn(orderDTO.getOrderSn());
	  if(ctOrderInfo != null){
		  result = ctOrderInfo.getOrderSn();
	  } else {
		  result = "请确认输入的订单";
	  }
	  return SUCCESS;
  }
  //根据用户查询信用支付订单列表
  public String findXinYongOrderInfo(){
	  Long uid = orderDTO.getUid();
	  if(uid != null){
		  Page page = new Page();
		  if (this.orderDTO.getPage() == 0) {
			  this.orderDTO.setPage(1);
		  }
		  page.setCurrentPage(this.orderDTO.getPage());
		  String type = orderDTO.getKeyword();
		  String t = "";
		  if(type == null){
			  t = "5,6,7,8";
		  } else {
			  if(type.equals("1")){
				  t= "6,8";
			  } else {
				  t= "5,7";
			  }
		  }
		  checks = orderManager.getorderByUidAndPayType(uid, t, page);
		  if(checks != null){
			  for (int i = 0; i < checks.size(); i++) {
				  payInterface = payManager.getPayInterByOrderSn(checks.get(i).getOrderSn());
				  checks.get(i).setPayInterface(payInterface);
			  }
			  page.setTotalPage(page.getTotalPage());
			  this.request.put("pages", page);
			  this.session.put("orderInfoUid", uid);
		  }
		  CtUser cu = userManager.getCtUserByUId(uid);
		  this.request.put("shenCountPrice", cu.getUCreditLimit()-cu.getURemainingAmount());
		  
	  }
	  return SUCCESS;
  }
  private CtOrderRelation relation = new CtOrderRelation();
  private List<CtOrderRelation> relationsList = new ArrayList<CtOrderRelation>();
  
  public CtOrderRelation getRelation() {
	return relation;
}

public void setRelation(CtOrderRelation relation) {
	this.relation = relation;
}

public List<CtOrderRelation> getRelationsList() {
	return relationsList;
}

public void setRelationsList(List<CtOrderRelation> relationsList) {
	this.relationsList = relationsList;
}
private List<CtPay> paysList = new ArrayList<CtPay>();

public List<CtPay> getPaysList() {
	return paysList;
}

public void setPaysList(List<CtPay> paysList) {
	this.paysList = paysList;
}

//查询用户信用还款线下支付列表
  public String findUserXianCre(){
	  Long uid = orderDTO.getUid();
	  Page page = new Page();
      if (this.orderDTO.getPage() == 0) {
        this.orderDTO.setPage(1);
      }
      page.setCurrentPage(this.orderDTO.getPage());
      
	  relationsList = orderManager.findRelationsByUid(uid,page);
	  if(relationsList!= null && relationsList.size() > 0){
		  for (int i = 0; i < relationsList.size(); i++) {
			  String pids[] = relationsList.get(i).getPayId().split(",");
			  ctPay = payManager.findPayByPId(pids[0]);
			  page.setTotalPage(page.getTotalPage());
			  this.request.put("pages", page);
			  ctPay.setOrid(relationsList.get(i).getOrid());
			  if(ctPay.getOrderInfo().getPay().equals("7")){
				  paysList.add(ctPay);
			  }
		  }
	  }
	  return SUCCESS;
  }
  private JSONArray res = new JSONArray();
  
  public JSONArray getRes() {
	return res;
}

public void setRes(JSONArray res) {
	this.res = res;
}

//查询单笔线下支付的订单信息
  public String findOrderInfoByOrId(){
	  Long orid = orderDTO.getOrid();
	  relation = orderManager.findRelatinsByOrid(orid);
	  String payIds[] = relation.getPayId().split(",");
	  String data = "";
	  for (int i = 0; i < payIds.length; i++) {
		  ctPay = payManager.findPayByPId(payIds[i]);
		  checks.add(ctPay.getOrderInfo());
		  if(ctPay.getOrderInfo().getPay().equals("7")){
			  data += ctPay.getOrderInfo().getOrderSn()+"|" + ctPay.getOrderInfo().getOrderTime() + "|" +
					  ctPay.getOrderInfo().getTotal() + "|" + ctPay.getOrderInfo().getConsignee() + "|" +
					  ctPay.getOrderInfo().getTel() + "+";
		  }
	}
	  if(data != null && !data.equals("")){
		  data = data.substring(0, data.length()-1);
	  }
	  result = data;
	  return SUCCESS;
  }
  //审核通过
  public String okCreLinePay(){
	  Long orid = orderDTO.getOrid();
	  relation = orderManager.findRelatinsByOrid(orid);
	  String payIds[] = relation.getPayId().split(",");
	  Double d = 0D;
	  ctUser = new CtUser();
	  for (int i = 0; i < payIds.length; i++) {
		  ctPay = payManager.findPayByPId(payIds[i]);
		  ctOrderInfo = ctPay.getOrderInfo();
		  ctOrderInfo.setPay("8");
		  orderManager.updateOrderInfoByInfo(ctOrderInfo);
		  d = add(d.toString(), ctOrderInfo.getTotal());
		  ctUser = ctOrderInfo.getCtUser();
	  }
	  ctUser.setURemainingAmount(add(ctUser.getURemainingAmount().toString(), d.toString()));
	  orderManager.updateuser(ctUser);
	  return SUCCESS;
  }
}
