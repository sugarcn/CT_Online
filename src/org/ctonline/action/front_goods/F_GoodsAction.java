package org.ctonline.action.front_goods;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.action.BaseAction;
import org.ctonline.dto.front_goods.F_GoodsDTO;
import org.ctonline.manager.front_goods.F_GoodsManager;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.views.ViewCateNum;
import org.ctonline.po.views.ViewSubcateNum;

import com.opensymphony.xwork2.ModelDriven;

public class F_GoodsAction extends BaseAction implements RequestAware, ModelDriven<Object>{

	private F_GoodsDTO fgoodsDTO = new F_GoodsDTO();
	private Map<String, Object> request;
	private F_GoodsManager fgoodsManager;
	private List<CtGoodsCategory> cateList;
	private List<ViewCateNum> catenumList;
	private List<ViewSubcateNum> subcatenumList;
	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.fgoodsDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request; 
	}

	public F_GoodsDTO getFgoodsDTO() {
		return fgoodsDTO;
	}

	public void setFgoodsDTO(F_GoodsDTO fgoodsDTO) {
		this.fgoodsDTO = fgoodsDTO;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	
	public F_GoodsManager getFgoodsManager() {
		return fgoodsManager;
	}

	public void setFgoodsManager(F_GoodsManager fgoodsManager) {
		this.fgoodsManager = fgoodsManager;
	}

	
	public List<CtGoodsCategory> getCateList() {
		return cateList;
	}

	public void setCateList(List<CtGoodsCategory> cateList) {
		this.cateList = cateList;
	}

	public List<ViewCateNum> getCatenumList() {
		return catenumList;
	}

	public void setCatenumList(List<ViewCateNum> catenumList) {
		this.catenumList = catenumList;
	}

	public List<ViewSubcateNum> getSubcatenumList() {
		return subcatenumList;
	}

	public void setSubcatenumList(List<ViewSubcateNum> subcatenumList) {
		this.subcatenumList = subcatenumList;
	}

	public String list(){
		this.cateList = fgoodsManager.queryAllCate();
		this.catenumList = fgoodsManager.cateCount();
		this.subcatenumList = fgoodsManager.subcateCount();
		return SUCCESS;
	}
}
