package org.ctonline.action.user;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.dto.user.CtUserAddressDTO;
import org.ctonline.manager.basic.CtRegionManager;
import org.ctonline.manager.user.CtUserAddressManager;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class CtUserAddressAction extends ActionSupport implements RequestAware,
		ModelDriven<Object> {

	private List<CtUserAddress> addresses;

	public CtRegionManager getRegionManager() {
		return regionManager;
	}

	public void setRegionManager(CtRegionManager regionManager) {
		this.regionManager = regionManager;
	}

	private CtUserAddressManager addressManager;
	private List<CtRegion> regions;

	public List<CtRegion> getRegions() {
		return regions;
	}

	public void setRegions(List<CtRegion> regions) {
		this.regions = regions;
	}

	private CtRegionManager regionManager;

	private Map<String, Object> request;
	private Page page;
	private CtUserAddressDTO addressDTO = new CtUserAddressDTO();
	private CtUserAddress address;

	public List<CtUserAddress> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<CtUserAddress> addresses) {
		this.addresses = addresses;
	}

	public CtUserAddressManager getAddressManager() {
		return addressManager;
	}

	public void setAddressManager(CtUserAddressManager addressManager) {
		this.addressManager = addressManager;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtUserAddress getAddress() {
		return address;
	}

	public void setAddress(CtUserAddress address) {
		this.address = address;
	}

	@Override
	public Object getModel() {

		return this.addressDTO;
	}

	// 页面显示用户组
	public String list() {
		try {
			page = new Page();
			if (addressDTO.getPage() == 0) {
				addressDTO.setPage(1);
			}
			page.setCurrentPage(addressDTO.getPage());
			this.addresses = addressManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 按照关键字查询用户组
	public String search() {
		try {

			Page page = new Page();
			if (addressDTO.getPage() == 0) {
				addressDTO.setPage(1);
			}
			page.setCurrentPage(addressDTO.getPage());
			if (addressDTO.getKeyword() != null
					&& !addressDTO.getKeyword().equals("请输入你要查的关键词")) {
				this.addresses = addressManager.findAll(
						addressDTO.getKeyword(), page);
			} else {
				this.addresses = addressManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 添加用户组
	public String add() {
		try {
			CtUserAddress m = new CtUserAddress();
			m.setUId(addressDTO.getUId());
			m.setAConsignee(addressDTO.getAConsignee());
			m.setAddress(addressDTO.getAddress());
			m.setCity(addressDTO.getCity());
			m.setCountry(addressDTO.getCountry());
			m.setProvince(addressDTO.getProvince());
			m.setDistrict(addressDTO.getDistrict());
			m.setMb(addressDTO.getMb());
			m.setTel(addressDTO.getTel());
			m.setZipcode(addressDTO.getZipcode());

			this.addressManager.save(m);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 删除用户组
	public String del() {
		try {
			for (Long m : addressDTO.getMid()) {
				this.addressManager.delete(m);

			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 修改用户组
	public String update() {
		try {
			CtUserAddress m = new CtUserAddress();
			m.setAId(addressDTO.getAId());
			m.setUId(addressDTO.getUId());
			m.setAConsignee(addressDTO.getAConsignee());
			m.setAddress(addressDTO.getAddress());
			m.setCity(addressDTO.getCity());
			m.setCountry(addressDTO.getCountry());
			m.setProvince(addressDTO.getProvince());
			m.setDistrict(addressDTO.getDistrict());
			m.setMb(addressDTO.getMb());
			m.setTel(addressDTO.getTel());
			m.setZipcode(addressDTO.getZipcode());
			addressManager.update(m);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String find() {
		try {
			Long pid = (long) 0;
			this.regions = regionManager.queryById(pid);

			Long id = addressDTO.getId();
			address = new CtUserAddress();
			address.setAId(id);
			this.address = this.addressManager.findById(id);
			addressDTO.setAId(address.getAId());
			addressDTO.setUId(address.getUId());
			addressDTO.setAConsignee(address.getAConsignee());
			addressDTO.setAddress(address.getAddress());
			addressDTO.setCity(address.getCity());
			addressDTO.setCountry(address.getCountry());
			addressDTO.setDistrict(address.getDistrict());
			addressDTO.setProvince(address.getProvince());
			addressDTO.setTel(address.getTel());
			addressDTO.setMb(address.getMb());
			addressDTO.setZipcode(address.getZipcode());

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}

}
