package org.ctonline.action.user;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletRequestAware;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.dto.user.CtUserDTO;
import org.ctonline.manager.basic.CtRegionManager;
import org.ctonline.manager.user.CtUserAddressManager;
import org.ctonline.manager.user.CtUserManager;
import org.ctonline.po.basic.CtLoginLog;
import org.ctonline.po.basic.CtModule;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.manager.CtManager;
import org.ctonline.po.user.CtContact;
import org.ctonline.po.user.CtDraw;
import org.ctonline.po.user.CtUesrClient;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.util.Page;
import org.ctonline.util.UtilDate;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class CtUserAction extends ActionSupport implements RequestAware,
		ModelDriven<Object>, SessionAware {

	private List<CtUser> users;
	private CtUser ctUser;
	private CtUserDTO userDTO = new CtUserDTO();
	private CtUserManager userManager;
	private Map<String, Object> request;
	private Map<String, Object> session;
	private Page page;
	private CtUserAddress userAddress;
	private List<CtUserAddress> userAddressesList = new ArrayList<CtUserAddress>();
	private List<CtUserAddress> customerMentList = new ArrayList<CtUserAddress>();
	
	private List<CtUesrClient> uesrClientsList = new ArrayList<CtUesrClient>();
	
	private CtUesrClient uesrClient = new CtUesrClient();
	
	public CtUesrClient getUesrClient() {
		return uesrClient;
	}

	public void setUesrClient(CtUesrClient uesrClient) {
		this.uesrClient = uesrClient;
	}

	public List<CtUesrClient> getUesrClientsList() {
		return uesrClientsList;
	}

	public void setUesrClientsList(List<CtUesrClient> uesrClientsList) {
		this.uesrClientsList = uesrClientsList;
	}

	private HttpServletResponse response;
	private CtRegionManager regionManager;
	private CtRegion region;
	private List<CtRegion> regionsList = new ArrayList<CtRegion>();
	private CtUserAddressManager addressManager;
	
	public CtUserAddressManager getAddressManager() {
		return addressManager;
	}

	public List<CtUserAddress> getCustomerMentList() {
		return customerMentList;
	}

	public void setCustomerMentList(List<CtUserAddress> customerMentList) {
		this.customerMentList = customerMentList;
	}

	public void setAddressManager(CtUserAddressManager addressManager) {
		this.addressManager = addressManager;
	}

	public CtRegionManager getRegionManager() {
		return regionManager;
	}

	public void setRegionManager(CtRegionManager regionManager) {
		this.regionManager = regionManager;
	}

	public CtRegion getRegion() {
		return region;
	}

	public void setRegion(CtRegion region) {
		this.region = region;
	}

	public List<CtRegion> getRegionsList() {
		return regionsList;
	}

	public void setRegionsList(List<CtRegion> regionsList) {
		this.regionsList = regionsList;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public List<CtUserAddress> getUserAddressesList() {
		return userAddressesList;
	}

	public void setUserAddressesList(List<CtUserAddress> userAddressesList) {
		this.userAddressesList = userAddressesList;
	}

	public CtUserAddress getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(CtUserAddress userAddress) {
		this.userAddress = userAddress;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.getUserDTO();
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	// 用户列表
	public String list() {
		try {
			page = new Page();
			if (userDTO.getPage() == 0) {
				userDTO.setPage(1);
			}
			page.setCurrentPage(userDTO.getPage());
			users = this.userManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			this.request.put("level", 2);
			session.remove("uuid");
			session.remove("cusUid");
			session.remove("cusCid");
			return "list";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// �޸��û���ת
	public String editUser() {
		try {

			ctUser = (CtUser) this.session.get("userinfosession");
			return "editUser";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// �����޸��û�
	public String save() {
		try {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			cu.setUUsername(ctUser.getUUsername());
			cu.setUEmail(ctUser.getUEmail());
			cu.setUQq(ctUser.getUQq());
			cu.setUWeibo(ctUser.getUWeibo());
			cu.setUTb(ctUser.getUTb());
			cu.setUSex(ctUser.getUSex());
			cu.setUBirthday(ctUser.getUBirthday());
			userManager.editCtuser(cu);
			return "save";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public List<CtUser> getUsers() {
		return users;
	}

	public void setUsers(List<CtUser> users) {
		this.users = users;
	}

	public CtUserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(CtUserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public CtUser getCtUser() {
		return ctUser;
	}

	public void setCtUser(CtUser ctUser) {
		this.ctUser = ctUser;
	}

	public void setUser(List<?> user) {
		this.user = user;
	}

	private List<?> user;

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtUserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(CtUserManager userManager) {
		this.userManager = userManager;
	}

	public List<?> getUser() {
		return user;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public String search() {
		try {
			Long UGId = userDTO.getUGId();
			this.user = this.userManager.selectAll(UGId);
			if (userDTO.getKeyword() != null
					&& !"".equals(userDTO.getKeyword())) {
				this.users = userManager.findAll(userDTO.getKeyword());
			} else {
				this.users = null;
			}
			this.request.put("UGId", UGId);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
	public String search1() {
		try {
			Long UGId = userDTO.getUGId();
			this.user = this.userManager.selectAll(UGId);
			page = new Page();
			if (userDTO.getPage() == 0) {
				userDTO.setPage(1);
			}
			page.setCurrentPage(userDTO.getPage());
			this.users = userManager.findAll(userDTO.getKeyword(), page);
			this.request.put("UGId", UGId);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
	//登录日志管理
	public String logLogin(){
		page = new Page();
		if (userDTO.getPage() == 0) {
			userDTO.setPage(1);
		}
		page.setCurrentPage(userDTO.getPage());
		users = this.userManager.getCtUserByPage(page);
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);
		return SUCCESS;
	}
	private CtContact contact = new CtContact();
	private List<CtContact> contactsList = new ArrayList<CtContact>();
	
	public CtContact getContact() {
		return contact;
	}

	public void setContact(CtContact contact) {
		this.contact = contact;
	}

	public List<CtContact> getContactsList() {
		return contactsList;
	}

	public void setContactsList(List<CtContact> contactsList) {
		this.contactsList = contactsList;
	}

	//用户留言列表
	public String userMessage(){
		page = new Page();
		if (userDTO.getPage() == 0) {
			userDTO.setPage(1);
		}
		page.setCurrentPage(userDTO.getPage());
		contactsList = userManager.findContactByPage(page);
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);
		return SUCCESS;
	}
	private CtDraw draw = new CtDraw();
	private List<CtDraw> drawsList = new ArrayList<CtDraw>();
	
	public CtDraw getDraw() {
		return draw;
	}

	public void setDraw(CtDraw draw) {
		this.draw = draw;
	}

	public List<CtDraw> getDrawsList() {
		return drawsList;
	}

	public void setDrawsList(List<CtDraw> drawsList) {
		this.drawsList = drawsList;
	}

	//大转盘活动管理
	public String listDraw(){
		page = new Page();
		if (userDTO.getPage() == 0) {
			userDTO.setPage(1);
		}
		page.setCurrentPage(userDTO.getPage());
		drawsList = userManager.findDrawByPage(page);
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);
		return SUCCESS;
	}
	//添加一次抽奖机会
	public String addOneDrawNum(){
		Integer drId = userDTO.getDrId();
		draw = userManager.findDrawByDrId(drId);
		if(draw != null){
			draw.setDrNum(draw.getDrNum()+1);
			userManager.updateDraw(draw);
		}
		return SUCCESS;
	}
	//清空抽奖机会
	public String clearDrawNum(){
		Integer drId = userDTO.getDrId();
		draw = userManager.findDrawByDrId(drId);
		if(draw != null){
			draw.setDrNum(0);
			userManager.updateDraw(draw);
		}
		return SUCCESS;
	}
	private String result;
	
	//信用支付抽奖
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	private CtLoginLog loginLog = new CtLoginLog();
	
	public CtLoginLog getLoginLog() {
		return loginLog;
	}

	public void setLoginLog(CtLoginLog loginLog) {
		this.loginLog = loginLog;
	}

	public String updateCreditLimit(){
		Long uid = userDTO.getUId();
		Double creditLimit = userDTO.getUCreditLimit();
		ctUser = userManager.getCtUserByUId(uid);
		//判断输入额度是否比原额度小
		if(creditLimit < ctUser.getUCreditLimit()){
			//判断是否是使用过额度
			if(!ctUser.getURemainingAmount().equals(ctUser.getUCreditLimit())){
				result = "-1"+"__"+ctUser.getUCreditLimit();
			} else {
				result = "1";
			}
		} else {
			result = "1";
		}
		if(result == "1"){
			//日志记录
			CtManager cu = (CtManager) this.session.get("admininfosession");
			loginLog.setLogClass("CtUserAction");
			loginLog.setLogDesc(""+cu.getMManagerId()+"将"+ctUser.getUUsername()+"的信用额度从"+ctUser.getUCreditLimit()+"调整为"+creditLimit+"");
			
			HttpServletRequest req = ServletActionContext.getRequest();
			String ip =UtilDate.getIpAddr(req);
			
			loginLog.setLogIp(ip);
			loginLog.setLogModule("信用额度修改");
			loginLog.setLogTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			loginLog.setUId(cu.getMId());
			userManager.saveLoginCredit(loginLog);
			
			
			Double newCredit = creditLimit;
			Double newRemaining = ctUser.getURemainingAmount() + (creditLimit - ctUser.getUCreditLimit());
			ctUser.setUCreditLimit(newCredit);
			ctUser.setURemainingAmount(newRemaining);
			userManager.update(ctUser);
			result+= "__" + newRemaining;
		}
		System.out.println(uid + "=================" + creditLimit);
		return SUCCESS;
	}
	
	
	//升级用户为客服账户
	public String setUpUserCus(){
		try {
			if(userDTO.getUUserid() != null){
				String cusName = new String(userDTO.getUUserid().getBytes("ISO-8859-1"),"UTF-8");
				session.put("uuid", cusName);
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		System.out.println(userDTO.getUId());
		userManager.updateUserCus(userDTO.getUId());
//		userAddressesList = userManager.findAllAddressByUid(userDTO.getUId());
		session.put("cusUid", userDTO.getUId());
		return SUCCESS;
	}
	//客户收货地址管理
	public String findCusAddress(){
		page = new Page();
		if (userDTO.getPage() == 0) {
			userDTO.setPage(1);
		}
		try {
			if(userDTO.getCusName() != null){
				String cusName = new String(userDTO.getCusName().getBytes("ISO-8859-1"),"UTF-8");
				session.put("cnamec", cusName);
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		page.setCurrentPage(userDTO.getPage());
		Integer cid = userDTO.getClientId();
		Long uid = userDTO.getUId();
		if(uid != null){
			userAddressesList = userManager.findAllAddressByUid(uid, page, cid == null ? (Integer)session.get("cusCid") : cid);
			uesrClientsList = userManager.findCusMent(uid, page);
			session.put("cusUid", uid);
			session.put("cusCid", userDTO.getClientId());
		} else {
			uid = (Long) session.get("cusUid");
			userAddressesList = userManager.findAllAddressByUid(uid, page,  cid == null ? (Integer)session.get("cusCid") : cid);
			uesrClientsList = userManager.findCusMent(uid, page);
//			session.put("cusUid", userDTO.getUId());
		}
		if(userAddressesList != null && uesrClientsList != null){
			for (int i = 0; i < userAddressesList.size(); i++) {
				for (int j = 0; j < uesrClientsList.size(); j++) {
					if(userAddressesList.get(i).getCid() != null && userAddressesList.get(i).getCid().toString().equals(uesrClientsList.get(j).getCid().toString())){
						userAddressesList.get(i).setCname(uesrClientsList.get(j).getCname());
					}
				}
			}
		}
		
		page.setTotalPage(page.getTotalPage());
		
		
		regionsList = regionManager.queryById(0L);
		
		this.request.put("pages", page);
		return SUCCESS;
	}
	
	//客服账户客户地址列表
	public String userCusList(){
		page = new Page();
		if (userDTO.getPage() == 0) {
			userDTO.setPage(1);
		}
		try {
			if(userDTO.getUUserid() != null){
				String cusName = new String(userDTO.getUUserid().getBytes("ISO-8859-1"),"UTF-8");
				session.put("uuid", cusName);
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		page.setCurrentPage(userDTO.getPage());
		Long uid = userDTO.getUId();
		if(uid != null){
			userAddressesList = userManager.findAllAddressByUid(uid, page, -1);
			uesrClientsList = userManager.findCusMent(uid, page);
//			session.put("uuid", userDTO.getUUserid());
			session.put("cusUid", uid);
		} else {
			uid = (Long) session.get("cusUid");
			userAddressesList = userManager.findAllAddressByUid(uid, page, -1);
			uesrClientsList = userManager.findCusMent(uid, page);
			session.put("cusUid", uid);
//			session.put("uuid", userDTO.getUUserid());
		}
		if(userAddressesList != null && uesrClientsList != null){
			for (int i = 0; i < userAddressesList.size(); i++) {
				for (int j = 0; j < uesrClientsList.size(); j++) {
					if(userAddressesList.get(i).getCid() != null && userAddressesList.get(i).getCid().toString().equals(uesrClientsList.get(j).getCid().toString())){
						userAddressesList.get(i).setCname(uesrClientsList.get(j).getCname());
					}
				}
			}
		}
		
		page.setTotalPage(page.getTotalPage());
		
		
		regionsList = regionManager.queryById(0L);
		
		
		
		this.request.put("pages", page);
		return SUCCESS;
	}
	
	//异步查询客户姓名是否重复
	public String findClientName(){
		Long uid = (Long) session.get("cusUid");
		String userName = userDTO.getCusName();
		Boolean isOk = userManager.findClientByCName(userName, uid);
		if(isOk){
			result="success";
		} else {
			result="error";
		}
		return SUCCESS;
	}
	
	//添加地址到客户
	public String updateCusClientAddress(){
		Long uid = (Long) session.get("cusUid");
		String ACustomer = userManager.findCusAddressIsDefultByUid(uid);
		Long[] mid = userDTO.getMid();
		for (Long m : mid) {
			userManager.updateClientName(m, userDTO.getClientId(), uid, ACustomer);
		}
		return SUCCESS;
	}
	
	//添加客户姓名
	public String saveCusName(){
		uesrClient = new CtUesrClient();
		String userName = userDTO.getCusName();
		Long uid = (Long) session.get("cusUid");
		uesrClient.setCname(userName);
		uesrClient.setUid(uid);
		uesrClient.setCdefult("0");
		userManager.saveClent(uesrClient);
		return SUCCESS;
	}
	
	//设置客服账户下单地址
	public String setupUserCusAddess(){
		Long cid = userDTO.getAid();
		Long uid = (Long) session.get("cusUid");
		
		userManager.updateCusAddressByAid(cid, uid);
		
		return SUCCESS;
	}
	
	//客服新增客户地址
	public String saveCusAddress(){
		userAddress = new CtUserAddress();
		userAddress.setAConsignee(userDTO.getAddessUserName());
		userAddress.setCountry(userDTO.getCountry());
		userAddress.setProvince(userDTO.getProvince());
		userAddress.setCity(userDTO.getCity());
		userAddress.setDistrict(userDTO.getDistrict());
		userAddress.setAddress(userDTO.getAddress());
		userAddress.setZipcode("000000");
		userAddress.setTel(userDTO.getPhone());
		userAddress.setMb(userDTO.getUsermb() != null ? userDTO.getUsermb() : " ");
		userAddress.setACustomer("1");
		userAddress.setUId((Long) session.get("cusUid"));
		userAddress.setAdesc(userDTO.getEtcDesc() != null ? userDTO.getEtcDesc() : " ");
		userAddress.setCid(userDTO.getClientId());
		addressManager.save(userAddress);
		session.put("cusCid", userDTO.getClientId());
		return SUCCESS;
	}
	
	public String deleteCusAddress(){
		try {
			for (Long m : userDTO.getMid()) {
				this.addressManager.delete(m);
				session.put("cusCid", userDTO.getClientId());
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
	
	//修改客服客户地址
	public String updateCusAddress(){
		userAddress = new CtUserAddress();
		userAddress.setAConsignee(userDTO.getAddessUserName());
		userAddress.setCountry(userDTO.getCountry());
		userAddress.setProvince(userDTO.getProvince());
		userAddress.setCity(userDTO.getCity());
		userAddress.setDistrict(userDTO.getDistrict());
		userAddress.setAddress(userDTO.getAddress());
		userAddress.setZipcode("000000");
		userAddress.setTel(userDTO.getPhone());
		userAddress.setMb(userDTO.getUsermb() != null ? userDTO.getUsermb() : " ");
		userAddress.setACustomer("1");
		userAddress.setUId((Long) session.get("cusUid"));
		userAddress.setAdesc(userDTO.getEtcDesc() != null ? userDTO.getEtcDesc() : " ");
		userAddress.setAId(userDTO.getId());
		userAddress.setCid(userDTO.getClientId());
		addressManager.update(userAddress);
		session.put("cusCid", userDTO.getClientId());
		return SUCCESS;
	}
	
	//查询单个地址
	public String findCusAddressById(){
		Long id = userDTO.getId();
		userAddress = addressManager.findById(id);
		
		userDTO.setAddessUserName(userAddress.getAConsignee());
		userDTO.setCountry(userAddress.getCountry());
		userDTO.setProvince(userAddress.getProvince());
		userDTO.setCity(userAddress.getCity());
		userDTO.setDistrict(userAddress.getDistrict());
		userDTO.setAddress(userAddress.getAddress());
		userDTO.setZipCode(userAddress.getZipcode());
		userDTO.setPhone(userAddress.getTel());
		userDTO.setUsermb(userAddress.getMb());
		userDTO.setACustomer(userAddress.getACustomer());
		userDTO.setEtcDesc(userAddress.getAdesc());
		userDTO.setUId(userAddress.getUId());
		
		
		return SUCCESS;
	}
	
	// 根据收货地址id获取一个收货地址
		public String getAddressByAid() {
			try {
				Long id = userDTO.getId();;
				CtUserAddress address = this.addressManager.findById(id);
				List<String> l = new ArrayList<String>();
				l.add(address.getRegionCountry().getRegionName());
				l.add(address.getRegionCountry().getRegionId().toString());
				l.add(address.getRegionProvince().getRegionName());
				l.add(address.getRegionProvince().getRegionId().toString());
				l.add(address.getRegionCity().getRegionName());
				l.add(address.getRegionCity().getRegionId().toString());
				l.add(address.getRegionDistrict().getRegionName());
				l.add(address.getRegionDistrict().getRegionId().toString());
				l.add(address.getAddress());
				if (address.getAdesc() == null) {
					l.add("");
				} else {
					l.add(address.getAdesc().trim());
				}
				if (address.getZipcode() == null) {
					l.add("");
				} else {
					l.add(address.getZipcode().trim());
				}
				l.add(address.getAConsignee());
				if (address.getTel() == null) {
					l.add("");
				} else {
					l.add(address.getTel().trim());
				}
				if (address.getMb() == null) {
					l.add("");
				} else {
					l.add(address.getMb().trim());
				}
				l.add(address.getAId().toString());
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			} catch (Exception e) {
				// TODO: handle exception
				//e.printStackTrace();
				List<String> l = new ArrayList<String>();
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			}
		}
		private JSONArray resultJson = new JSONArray();

		public JSONArray getResultJson() {
			return resultJson;
		}

		public void setResultJson(JSONArray resultJson) {
			this.resultJson = resultJson;
		}
		//删除选中的客户
		public String deleteCusByUser(){
			Long[] mid = userDTO.getMid();
			for (Long m : mid) {
				userManager.deleteClientByCid(m.intValue());
			}
//			Integer cid = userDTO.getClientId();
//			userManager.deleteClientByCid(cid);
			return SUCCESS;
		}
}
