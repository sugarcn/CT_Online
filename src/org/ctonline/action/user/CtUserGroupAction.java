package org.ctonline.action.user;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.dto.manager.CtManagerDTO;
import org.ctonline.dto.user.CtUserDTO;
import org.ctonline.dto.user.CtUserGroupDTO;
import org.ctonline.dto.user.CtUserManageRelationDTO;
import org.ctonline.manager.manager.CtManagerManager;
import org.ctonline.manager.role.CtRoleManager;
import org.ctonline.manager.user.CtUserGroupManager;
import org.ctonline.manager.user.CtUserGroupRelationManager;
import org.ctonline.manager.user.CtUserManager;
import org.ctonline.po.manager.CtManager;
import org.ctonline.po.user.CtRole;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserGroup;
import org.ctonline.po.user.CtUserGroupRelation;
import org.ctonline.po.user.CtUserManageRelation;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class CtUserGroupAction extends ActionSupport implements RequestAware,
		ModelDriven<Object> {
	private List<CtUserGroup> userGroups;
	private CtUserGroup userGroup;
	private Page page;
	private Map<String, Object> request;
	private CtUserGroupDTO userGroupDTO = new CtUserGroupDTO();
	private CtUserManageRelationDTO userManageRelationDTO = new CtUserManageRelationDTO();
	private CtUserManageRelation userManageRelation;
	private CtUserGroupManager userGroupManager;
	private CtUserGroupRelationManager userGroupRelationManager;
	private CtRoleManager roleManager;
	private CtUserManager userManager;
	private List<?> user;
	private List<CtUserGroupRelation> groupRelations;
	private CtManagerManager managerManager;
	private CtManagerDTO managerDTO = new CtManagerDTO();
	private List<CtManager> managers;
	private List<?> cmAndCumr;
	private CtUserDTO userDTO = new CtUserDTO();
	// 客户、客户组
	private String utpye;
	private String result;

	public List<?> getUser() {
		return user;
	}

	public void setUser(List<?> user) {
		this.user = user;
	}

	public List<CtUserGroupRelation> getGroupRelations() {
		return groupRelations;
	}

	public void setGroupRelations(List<CtUserGroupRelation> groupRelations) {
		this.groupRelations = groupRelations;
	}

	public CtUserGroupRelationManager getGroupRelationManager() {
		return groupRelationManager;
	}

	public void setGroupRelationManager(
			CtUserGroupRelationManager groupRelationManager) {
		this.groupRelationManager = groupRelationManager;
	}

	private CtUserGroupRelationManager groupRelationManager;

	public CtUserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(CtUserManager userManager) {
		this.userManager = userManager;
	}

	@Override
	public Object getModel() {
		return this.userGroupDTO;
	}

	public List<CtUserGroup> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(List<CtUserGroup> userGroups) {
		this.userGroups = userGroups;
	}

	public CtUserGroup getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(CtUserGroup userGroup) {
		this.userGroup = userGroup;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}

	public CtUserGroupManager getUserGroupManager() {
		return userGroupManager;
	}

	public void setUserGroupManager(CtUserGroupManager userGroupManager) {
		this.userGroupManager = userGroupManager;
	}

	public CtUserGroupDTO getUserGroupDTO() {
		return userGroupDTO;
	}

	public void setUserGroupDTO(CtUserGroupDTO userGroupDTO) {
		this.userGroupDTO = userGroupDTO;
	}

	public String getUtpye() {
		return utpye;
	}

	public void setUtpye(String utpye) {
		this.utpye = utpye;
	}

	public CtUserManageRelation getUserManageRelation() {
		return userManageRelation;
	}

	public void setUserManageRelation(CtUserManageRelation userManageRelation) {
		this.userManageRelation = userManageRelation;
	}

	// ҳ����ʾ�û���
	public String list() {
		try {
			page = new Page();
			if (userGroupDTO.getPage() == 0) {
				userGroupDTO.setPage(1);
			}
			page.setCurrentPage(userGroupDTO.getPage());
			this.userGroups = userGroupManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// ���չؼ��ֲ�ѯ�û���
	public String search() {
		try {
			Page page = new Page();
			if (userGroupDTO.getPage() == 0) {
				userGroupDTO.setPage(1);
			}
			page.setCurrentPage(userGroupDTO.getPage());
			if (userGroupDTO.getKeyword() != null
					&& !userGroupDTO.getKeyword().equals("��������Ҫ��Ĺؼ��")) {
				this.userGroups = userGroupManager.findAll(
						userGroupDTO.getKeyword(), page);
			} else {
				this.userGroups = userGroupManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// ����û���
	public String add() {
		try {
			CtUserGroup m = new CtUserGroup();
			m.setUGName(userGroupDTO.getUGName());
			m.setUGDesc(userGroupDTO.getUGDesc());
			Long aa = this.userGroupManager.save(m);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// ɾ���û���
	public String del() {
		try {
			for (Long m : userGroupDTO.getMid()) {
				this.userGroupManager.delete(m);

			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// �޸��û���
	public String update() {
		try {
			CtUserGroup m = new CtUserGroup();
			m = this.userGroupManager.findById(userGroupDTO.getId());

			m.setUGName(userGroupDTO.getUGName());
			m.setUGDesc(userGroupDTO.getUGDesc());
			userGroupManager.update(m);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String find() {
		try {
			Long id = userGroupDTO.getId();
			userGroup = new CtUserGroup();
			userGroup.setUGId(id);
			this.userGroup = this.userGroupManager.findById(id);
			userGroupDTO.setUGDesc(userGroup.getUGDesc());
			userGroupDTO.setUGName(userGroup.getUGName());

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}

	public String group() {
		try {
			Long UGId = userGroupDTO.getUGId();
			this.user = this.userManager.selectAll(UGId);

			this.request.put("UGId", UGId);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 分配客户组所属客户经理
	public String belongToManager() {
		String Utype = userManageRelationDTO.getUType();
		try {
			if ("1".equals(Utype)) {
				Long UGId = userGroupDTO.getUGId();
				this.utpye = Utype;
				CtRole role = roleManager.getRoleByRoleName();
				Integer roleId = role.getRoleId();
				this.managers = managerManager.getCtManagerByRoleId(roleId);
				this.cmAndCumr = userGroupManager.getCManageRelation(UGId,
						Utype, roleId);
				return "usergroupBTM";
			} else if ("0".equals(Utype)) {
				System.out.println("qqqqqqqqq");
				Long UId = userDTO.getUId();
				this.utpye = Utype;
				CtRole role = roleManager.getRoleByRoleName();
				Integer roleId = role.getRoleId();
				this.managers = managerManager.getCtManagerByRoleId(roleId);
				this.cmAndCumr = userGroupManager.getCManageRelation(UId,
						Utype, roleId);
				return "userBTM";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
		return null;
	}

	// 保存分配客户组所属客户经理
	public String saveBelongToManager() {
		try {
			String Utype = userManageRelationDTO.getUType();
			if ("1".equals(Utype)) {
				Integer MId = managerDTO.getMId();
				Long UGId = userGroupDTO.getUGId();
				CtUserManageRelation userManageRelation = new CtUserManageRelation();
				userManageRelation.setMId(Long.parseLong(MId.toString()));
				userManageRelation.setUId(UGId);
				userManageRelation.setUType(Utype);
				CtUserManageRelation cumr = userGroupManager
						.getUserManageRelation(UGId, Utype);
				if (cumr != null) {
					this.userGroupManager.updateUserManageRelation(UGId, Utype,
							MId);
				} else {
					this.userGroupManager
							.saveBelongToManager(userManageRelation);
				}
				this.result = "success";
			} else if ("0".equals(Utype)) {
				Integer MId = managerDTO.getMId();
				Long UId = userDTO.getUId();
				CtUserManageRelation userManageRelation = new CtUserManageRelation();
				userManageRelation.setMId(Long.parseLong(MId.toString()));
				userManageRelation.setUId(UId);
				userManageRelation.setUType(Utype);
				CtUserManageRelation cumr = userGroupManager
						.getUserManageRelation(UId, Utype);
				if (cumr != null) {
					this.userGroupManager.updateUserManageRelation(UId, Utype,
							MId);
				} else {
					this.userGroupManager
							.saveBelongToManager(userManageRelation);
				}
				this.result = "success";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			this.result = "error";
			return "error";
		}
		return SUCCESS;
	}

	public CtUserManageRelationDTO getUserManageRelationDTO() {
		return userManageRelationDTO;
	}

	public void setUserManageRelationDTO(
			CtUserManageRelationDTO userManageRelationDTO) {
		this.userManageRelationDTO = userManageRelationDTO;
	}

	public CtUserGroupRelationManager getUserGroupRelationManager() {
		return userGroupRelationManager;
	}

	public void setUserGroupRelationManager(
			CtUserGroupRelationManager userGroupRelationManager) {
		this.userGroupRelationManager = userGroupRelationManager;
	}

	public CtRoleManager getRoleManager() {
		return roleManager;
	}

	public void setRoleManager(CtRoleManager roleManager) {
		this.roleManager = roleManager;
	}

	public List<CtManager> getManagers() {
		return managers;
	}

	public void setManagers(List<CtManager> managers) {
		this.managers = managers;
	}

	public CtManagerManager getManagerManager() {
		return managerManager;
	}

	public void setManagerManager(CtManagerManager managerManager) {
		this.managerManager = managerManager;
	}

	public CtManagerDTO getManagerDTO() {
		return managerDTO;
	}

	public void setManagerDTO(CtManagerDTO managerDTO) {
		this.managerDTO = managerDTO;
	}

	public List<?> getCmAndCumr() {
		return cmAndCumr;
	}

	public void setCmAndCumr(List<?> cmAndCumr) {
		this.cmAndCumr = cmAndCumr;
	}

	public CtUserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(CtUserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}
