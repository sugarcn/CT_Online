package org.ctonline.action.user;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.json.annotations.JSON;
import org.apache.xmlbeans.impl.regex.REUtil;
import org.ctonline.dto.user.CtSmsDTO;
import org.ctonline.dto.user.CtUserDTO;
import org.ctonline.manager.user.CtUserManager;
import org.ctonline.po.user.CtSms;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.MD5Util;
import org.ctonline.util.MialUtil;
import org.springframework.http.HttpRequest;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class LoginAction extends ActionSupport implements RequestAware,SessionAware,ServletResponseAware,ServletRequestAware
,ModelDriven<Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CtUserManager userManager;
	private CtUser ctUser;
	private CtUserDTO userDTO = new CtUserDTO();
	private CtSmsDTO smsDTO = new CtSmsDTO();
	private CtSms ctSms;
	private Long UId;
	private Map<String, Object> request;
	private String result;
	private Map<String, Object> session;
	private HttpServletResponse response;
	private HttpServletRequest _reRequest;
//	private String phoneYzm;
	private String yzm;
	private String isAutoLogin;
	private String message;
	private String uid;
	private String ucode;
	 

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.userDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}
	//��֤�û����Ƿ����
	public String checkName(){
		String UUserid = ctUser.getUUserid();
		CtUser ctUser = userManager.getCtUserByUUserid(UUserid);
		if(ctUser == null){
			result = "success";
			this.result = "success";
		}else{
			this.result = "error";
		}
		return SUCCESS;
	}
	//检测邮箱是否被注册
	public String checkEmail(){
		String UEmail = ctUser.getUEmail();
		CtUser ctUser = userManager.getCtUserByUEmail(UEmail);
		if(ctUser == null){
			result = "success";
			this.result = "success";
		}else{
			this.result = "error";
		}
		return SUCCESS;
	}
	//��֤�ֻ���Ƿ�ע��
	public String checkPhone(){
		String UMb = ctUser.getUMb();
		CtUser ctUser = userManager.getCtUserByUMb(UMb);
		
		if(ctUser == null){
			result = "success";
			this.result = "success";
		}else{
			this.result = "error";
		}
		return SUCCESS;
	}
	
	//��֤�ֻ�ʱ������֤��Ϣ
	public String savePhoneYzm(){
		ActionContext ac = ActionContext.getContext().getActionInvocation().getInvocationContext();
		HttpServletRequest httpServletRequest = (HttpServletRequest) ac.get(ServletActionContext.HTTP_REQUEST);
		CtSms cs = new CtSms();
		String ip = httpServletRequest.getRemoteAddr();
		UId = userDTO.getUId();
		Date date = new Date();
		SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		String base="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		int length=base.length();
		String sRand = "";
		for (int i = 0; i < 4; i++) {
			Random random = new Random();  
			int start = random.nextInt(length);
			String rand = base.substring(start, start+1);
			sRand += rand;
		}
		
		String UMb = ctUser.getUMb();
		String yzm = sRand;
		cs.setSms(yzm);
		cs.setSmsIp(ip);
		cs.setSmsTime(siFormat.format(date));
		cs.setUMb(UMb);
		Long id = this.userManager.saveCtSms(cs);
		if(id != null){
			this.result = "success";
		}else {
			this.result = "error";
		}
		return SUCCESS;
	}
	//��֤�������֤���Ƿ���ȷ
	public String checkYzmIsEqual(){
		String UMb = ctUser.getUMb();
		String yzm = ctSms.getSms();
		CtSms cs = this.userManager.getCtSmsByUMb(UMb);
		if(cs != null){
			String yzm2 = cs.getSms();
			if(yzm.equals(yzm2)){
				this.result = "success";
			}else {
				this.result = "error";
			}
		}else {
			this.result = "error";
		}
		
		return SUCCESS;
	}
	
	//ע���û�
	public String register() throws Exception{
		CtUser ct = new CtUser();
		ct.setUUserid(ctUser.getUUserid());
		ct.setUUsername(ctUser.getUUserid());
		String passwd = ctUser.getUPassword();
		String passwdorg = "CT"+ctUser.getUUserid()+passwd;
		String encryptPwd = MD5Util.getEncryptPasswd(passwdorg);
		ct.setUPassword(encryptPwd);
		ct.setUMb(ctUser.getUMb());
		ct.setUCode("1");
		Long id = this.userManager.save(ct);
		this.session.put("userinfosession", ct);
		return "register";
	}
	//邮箱注册
	public String registerEmail() throws Exception{
		try {
			CtUser ct = new CtUser();
			ct.setUUserid(ctUser.getUUserid());
			ct.setUUsername(ctUser.getUUserid());
			ct.setUEmail(ctUser.getUEmail());
			ct.setUState("0");
			ct.setUCode(UUID.randomUUID().toString());
			
			System.out.println(ct.getUCode());
			String passwd = ctUser.getUPassword();
			String passwdorg = "CT"+ctUser.getUUserid()+passwd;
			String encryptPwd = MD5Util.getEncryptPasswd(passwdorg);
			ct.setUPassword(encryptPwd);
			Long id = this.userManager.save(ct);
			this.session.put("userinfosession", ct);
			if(id != null){
				new Thread(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						try {
							MialUtil.sendMail();
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
						
					}
				}).start();
			}else {
				return "error";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
		return "registeremail";
	}
	
	//邮箱注册后激活
	public String activate(){
		try {
			String UUserid = this.getUid();
			String UCode = this.getUcode();
			CtUser cu = userManager.getCtuserByUUseridAndUCode(UUserid,UCode);
			if(cu != null){
				CtUser cu2 = userManager.getCtUserByUUserid(UUserid);
				cu2.setUState("1");
				userManager.update(cu2);
			}else {
				return "unActivate";
			}
			return "activate";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "unActivate";
		}
		
	}
	
	//����û�������ֻ���Ƿ����
	public String checkUserName(){
		String UUserid = ctUser.getUUserid();
		CtUser ctUser = userManager.getCtUserByUUserid(UUserid);
		if(ctUser == null){
			CtUser ctUser2 = userManager.getCtUserByUMb(UUserid);
			if(ctUser2 == null){
				this.result = "error";
			}else {
				this.result = "success";
			}
		}else{
			this.result = "success";
		}

		return SUCCESS;
	}
	
	//��֤�һ�����ʱ�������֤���Ƿ���ȷ
	public String checkYzm(){
		String yzm = this.getYzm();
		StringBuffer sb = new StringBuffer();
		for(int i=0;i<yzm.length();i++){
			char c = yzm.charAt(i);
			if(Character.isLowerCase(c)){
				sb.append(Character.toUpperCase(c));
			}else {
				sb.append(c);
			}
		}
		String realyzm = (String) this.getSession().get("rand");
		if(sb.toString().equals(realyzm)){
			this.result = "success";
		}else {
			this.result = "error";
		}
		return SUCCESS;
	}
	//�һ�����1
	public String findpwd1(){
		String UUserid = ctUser.getUUserid();
		this.ctUser = userManager.getCtUserByUUnameOrUMb(UUserid);
		return "findpwd1";
	}
	
	//�һ�����1
	public String findpasswd2(){
		String UMb = ctUser.getUMb();
		this.ctUser = userManager.getCtUserByUMb(UMb);
		return "findpwd2";
	}
	
	//�޸�����
	public String updatePasswd() throws Exception{
		
		String UUserid = ctUser.getUUserid();
		CtUser cu = userManager.getCtUserByUUserid(UUserid);
		String passwd = ctUser.getUPassword();
		String passwdorg = "CT"+ctUser.getUUserid()+passwd;
		String encryptPwd = MD5Util.getEncryptPasswd(passwdorg);
		cu.setUPassword(encryptPwd);
		userManager.update(cu);
		return "findpwd3";
	}
	
	//��ͨ�û���¼
	public String login() throws Exception {
			String isAutoLogin = this.getIsAutoLogin();
			if(ctUser != null){
				String uname2 = this.ctUser.getUUserid();
				String password = this.ctUser.getUPassword();
				if("autoLogin".equals(isAutoLogin)){
					CtUser cu = userManager.affirm(uname2, password);
					if(cu != null){
						this.session.put("userinfosession", cu);
						Cookie cookie = new Cookie("usercookie", uname2+"=="+password);
						cookie.setMaxAge(24*60*60);
						response.addCookie(cookie);
						return "login_user";
					}else {
						this.setMessage("�û���������벻��");
						return "login";
					}
				}else {
					CtUser cu = userManager.affirm(uname2, password);
					if(cu != null){
						this.session.put("userinfosession", cu);
						return "login_user";
					}else {
						this.setMessage("�û���������벻��");
						return "login";
					}
				}
			}else {
				return "login";
			}
			
//		}
	}
	

	public CtUserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(CtUserManager userManager) {
		this.userManager = userManager;
	}

	public CtUser getCtUser() {
		return ctUser;
	}

	public void setCtUser(CtUser ctUser) {
		this.ctUser = ctUser;
	}

	public CtUserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(CtUserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public CtSmsDTO getSmsDTO() {
		return smsDTO;
	}

	public void setSmsDTO(CtSmsDTO smsDTO) {
		this.smsDTO = smsDTO;
	}

	public CtSms getCtSms() {
		return ctSms;
	}

	public void setCtSms(CtSms ctSms) {
		this.ctSms = ctSms;
	}

	public Long getUId() {
		return UId;
	}

	public void setUId(Long uId) {
		UId = uId;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public String getYzm() {
		return yzm;
	}

	public void setYzm(String yzm) {
		this.yzm = yzm;
	}

	public String getIsAutoLogin() {
		return isAutoLogin;
	}

	public void setIsAutoLogin(String isAutoLogin) {
		this.isAutoLogin = isAutoLogin;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

//	@Override
//	public void setServletResponse(HttpServletResponse arg0) {
//		// TODO Auto-generated method stub
//		
//	}

	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
		this.response.setCharacterEncoding("UTF-8");
		this.response.setContentType("text/html");
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	@Override
	public void setServletRequest(HttpServletRequest arg0) {
		// TODO Auto-generated method stub
		this._reRequest = arg0;
	}

	public HttpServletRequest get_reRequest() {
		return _reRequest;
	}

	public void set_reRequest(HttpServletRequest _reRequest) {
		this._reRequest = _reRequest;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getUcode() {
		return ucode;
	}

	public void setUcode(String ucode) {
		this.ucode = ucode;
	}

//	public String getPhoneYzm() {
//		return phoneYzm;
//	}
//
//	public void setPhoneYzm(String phoneYzm) {
//		this.phoneYzm = phoneYzm;
//	}

}
