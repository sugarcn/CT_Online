package org.ctonline.action.user;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.dto.user.CtAddTicketDTO;
import org.ctonline.dto.user.CtUserDTO;
import org.ctonline.manager.user.CtAddTicketManager;
import org.ctonline.po.user.CtAddTicket;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.ImageUtil;
import org.ctonline.util.Page;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class CtAddTicketAction extends ActionSupport implements RequestAware,
		ModelDriven<Object> {

	private List<CtAddTicket> addTickets;
	private CtAddTicketManager addTicketManager;
	private Map<String, Object> request;
	private Page page;
	private CtAddTicketDTO addTicketDTO = new CtAddTicketDTO();
	private CtAddTicket addTicket;
	private CtUser ctUser;
	private CtUserDTO userDTO = new CtUserDTO();

	private List<File> file; // 上传文件集合
	private List<String> fileFileName; // 上传文件名集合
	private List<String> fileContentType; // 上传文件内容集合
	private String level;
	private String type;

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.addTicketDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public String list() {
		try {
			page = new Page();
			if (addTicketDTO.getPage() == 0) {
				addTicketDTO.setPage(1);
			}
			page.setCurrentPage(addTicketDTO.getPage());
			this.addTickets = addTicketManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			this.request.put("level", 2);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 根据UId查询增票
	public String getOneTicket() {
		try {
			Long uId = 2l;
			this.addTicket = addTicketManager.getTicketByUId(uId);
			return "showticket";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 新增票跳转
	public String add() {
		return "add";
	}

	// 根据UId删除增票
	public String del() {
		try {
			Long uId = 2l;
			int d = addTicketManager.delete(uId);
			return "del";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 审核
	/*public String shenhe() {
		try {
			Long ATId = addTicket.getATId();
			CtAddTicket ctAddTicket = this.addTicketManager.findById(ATId);
			if ("pass".equals(type)) {
				ctAddTicket.setIsPass("1");
			} else {
				ctAddTicket.setIsPass("0");
			}
			this.addTicketManager.update(ctAddTicket);
			return "shenhe";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}*/
	
	//审核
	public String pass(){
		for(long m :addTicketDTO.getMid() ){
			CtAddTicket ctAddTicket = this.addTicketManager.findById(m);
			ctAddTicket.setIsPass("1");
			this.addTicketManager.update(ctAddTicket);
		}
		
		return "pass";
	}
	
	//未审核
	public String notPass(){
		for(long m :addTicketDTO.getMid() ){
			CtAddTicket ctAddTicket = this.addTicketManager.findById(m);
			ctAddTicket.setIsPass("0");
			this.addTicketManager.update(ctAddTicket);
		}
		
		return "notPass";
	}
	
	

	// 上传图片和保存其他信息
	public String saveTicket() {
		// 上传目录
		String dir = "";
		// 时间戳
		// String date = new Date().getTime()+"";
		// 带时间戳的文件名
		String timename = "";
		// 缩略图存储路径
		String imgSmalldir = "";
		// 上传图片
		if (file == null) {
			// return INPUT;
		}
		CtAddTicket ticket = new CtAddTicket();
		for (int i = 0; i < file.size(); i++) {
			try {
				InputStream in = new FileInputStream(file.get(i));
				String UPLOADDIR = "/uploadmessage";
				dir = ServletActionContext.getRequest().getRealPath(UPLOADDIR);
				String time = new Date().getTime() + "";
				timename = time + getExtention(this.getFileFileName().get(i));
				String url = UPLOADDIR + "/" + timename;
				String date = new Date().getTime() + "";
				imgSmalldir = dir + "/" + date + "_Small";
				dir = dir.replaceAll("\\\\", "/");
				imgSmalldir = imgSmalldir.replaceAll("\\\\", "/");
				String urlSmall = UPLOADDIR + "/" + date + "_Small.jpg";
				dir = ServletActionContext.getRequest().getRealPath(UPLOADDIR);
				System.out.println("dir=" + dir);
				if (i == 0) {
					ticket.setLisence(url);
					ticket.setLisenceSmall(urlSmall);
				} else if (i == 1) {
					ticket.setTaxCertifi(url);
					ticket.setTaxCertifiSmall(urlSmall);
				} else if (i == 2) {
					ticket.setGeneralCertifi(url);
					ticket.setGeneralCertifiSmall(urlSmall);
				} else if (i == 3) {
					ticket.setBankLisence(url);
					ticket.setBankLisenceSmall(urlSmall);
				}
				File fileLocation = new File(dir);
				if (!fileLocation.exists()) {
					boolean isCreated = fileLocation.mkdirs();
					if (!isCreated) {
						System.out.println("上传目录创建失败！");
						// 上传目录创建失败处理
						return null;
					}
				}
				File uploadFile = new File(dir, timename);
				OutputStream out = new FileOutputStream(uploadFile);
				byte[] buffer = new byte[1024 * 1024];
				int length;
				while ((length = in.read(buffer)) > 0) {
					out.write(buffer, 0, length);
				}
				in.close();
				out.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("上传失败！");
			} catch (IOException e) {
				// TODO: handle exception
				e.printStackTrace();
				System.out.println("上传失败！");
			}
			try {
				ImageUtil.saveImageAsJpg(dir + "\\\\" + timename, imgSmalldir
						+ ".jpg", 50, 50);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		ticket.setATId(addTicketDTO.getATId());
		ticket.setUId(1l);
		ticket.setCompanyName(addTicket.getCompanyName());
		ticket.setCompanySn(addTicket.getCompanySn());
		ticket.setRegisteAddress(addTicket.getRegisteAddress());
		ticket.setRegisteTel(addTicket.getRegisteTel());
		ticket.setDepositBank(addTicket.getDepositBank());
		ticket.setBankAccount(addTicket.getBankAccount());
		ticket.setIsPass("0");
		Long id = addTicketManager.save(ticket);
		return "saveAdd";
	}

	private static String getExtention(String fileName) {
		int pos = fileName.lastIndexOf(".");
		return fileName.substring(pos);
	}

	public List<CtAddTicket> getAddTickets() {
		return addTickets;
	}

	public void setAddTickets(List<CtAddTicket> addTickets) {
		this.addTickets = addTickets;
	}

	public CtAddTicketManager getAddTicketManager() {
		return addTicketManager;
	}

	public void setAddTicketManager(CtAddTicketManager addTicketManager) {
		this.addTicketManager = addTicketManager;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtAddTicketDTO getAddTicketDTO() {
		return addTicketDTO;
	}

	public void setAddTicketDTO(CtAddTicketDTO addTicketDTO) {
		this.addTicketDTO = addTicketDTO;
	}

	public CtAddTicket getAddTicket() {
		return addTicket;
	}

	public void setAddTicket(CtAddTicket addTicket) {
		this.addTicket = addTicket;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public CtUser getCtUser() {
		return ctUser;
	}

	public void setCtUser(CtUser ctUser) {
		this.ctUser = ctUser;
	}

	public CtUserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(CtUserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public List<File> getFile() {
		return file;
	}

	public void setFile(List<File> file) {
		this.file = file;
	}

	public List<String> getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(List<String> fileFileName) {
		this.fileFileName = fileFileName;
	}

	public List<String> getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(List<String> fileContentType) {
		this.fileContentType = fileContentType;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String allPass() {
		try {
			String isPass = "1";
			page = new Page();
			if (addTicketDTO.getPage() == 0) {
				addTicketDTO.setPage(1);
			}
			page.setCurrentPage(addTicketDTO.getPage());
			this.addTickets = addTicketManager.allPass(isPass, page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String notAllPass() {
		try {
			String isPass = "0";
			page = new Page();
			if (addTicketDTO.getPage() == 0) {
				addTicketDTO.setPage(1);
			}
			page.setCurrentPage(addTicketDTO.getPage());
			this.addTickets = addTicketManager.allPass(isPass, page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			this.request.put("level", 2);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

}
