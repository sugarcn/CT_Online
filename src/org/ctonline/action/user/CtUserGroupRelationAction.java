package org.ctonline.action.user;

import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.dto.user.CtUserGroupRelationDTO;
import org.ctonline.manager.user.CtUserGroupRelationManager;
import org.ctonline.po.user.CtUserGroupRelation;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class CtUserGroupRelationAction extends ActionSupport implements
		RequestAware, ModelDriven<Object> {
	private CtUserGroupRelation groupRelation;
	private CtUserGroupRelationManager groupRelationManager;
	private Map<String, Object> request;
	private String result;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}

	private CtUserGroupRelationDTO relationDTO = new CtUserGroupRelationDTO();

	public CtUserGroupRelation getGroupRelation() {
		return groupRelation;
	}

	public void setGroupRelation(CtUserGroupRelation groupRelation) {
		this.groupRelation = groupRelation;
	}

	public CtUserGroupRelationManager getGroupRelationManager() {
		return groupRelationManager;
	}

	public void setGroupRelationManager(
			CtUserGroupRelationManager groupRelationManager) {
		this.groupRelationManager = groupRelationManager;
	}

	// 添加用户组关联
	@Override
	public Object getModel() {

		return this.relationDTO;
	}

	public String add() {
		try {
			Long num = null;
			CtUserGroupRelation m = new CtUserGroupRelation();
			Long UGId = relationDTO.getUGId();
			this.groupRelationManager.delete(UGId);
			String ss = relationDTO.getUIds();
			String[] str = ss.split("@");
			for (int i = 0; i < str.length; i++) {
				Long id = Long.valueOf(str[i]);
				m.setUGId(UGId);
				m.setUId(id);
				num = this.groupRelationManager.save(m);

			}
			if (num > 0) {
				result = "success";
			} else {
				result = "fail";
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

}
