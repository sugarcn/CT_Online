package org.ctonline.action.manager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.dto.manager.CtManagerDTO;
import org.ctonline.dto.role.CtRoleDTO;
import org.ctonline.manager.basic.CtLoginOperationManager;
import org.ctonline.manager.manager.CtManagerManager;
import org.ctonline.manager.role.CtRoleManager;
import org.ctonline.manager.user.CtUserManager;
import org.ctonline.po.basic.CtLoginLog;
import org.ctonline.po.basic.CtLoginOperation;
import org.ctonline.po.manager.CtManager;
import org.ctonline.po.user.CtRole;
import org.ctonline.util.MD5Util;
import org.ctonline.util.Page;
import org.ctonline.util.UtilDate;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class ManagerAction extends ActionSupport implements RequestAware,
		ModelDriven<Object>, SessionAware {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map<String, Object> session;
	private List<CtManager> managers;
	private CtManagerDTO managerDTO = new CtManagerDTO();
	private CtManager manager;
	private CtRoleDTO roleDTO = new CtRoleDTO();
	private CtRole ctRole;
	private CtManagerManager managerManager;
	private CtUserManager userManager;
	private Map<String, Object> request;
	private Page page;
	private String result;
	private String message;
	private String info;
	public Boolean flag;
	private CtLoginOperationManager loginOperationManager;
	private CtLoginOperation loginOperation;
	private List<CtLoginOperation> loginOperationsList = new ArrayList<CtLoginOperation>();
	
	

	
	public CtUserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(CtUserManager userManager) {
		this.userManager = userManager;
	}

	public List<CtLoginOperation> getLoginOperationsList() {
		return loginOperationsList;
	}

	public void setLoginOperationsList(List<CtLoginOperation> loginOperationsList) {
		this.loginOperationsList = loginOperationsList;
	}

	public CtLoginOperationManager getLoginOperationManager() {
		return loginOperationManager;
	}

	public void setLoginOperationManager(
			CtLoginOperationManager loginOperationManager) {
		this.loginOperationManager = loginOperationManager;
	}

	public CtLoginOperation getLoginOperation() {
		return loginOperation;
	}

	public void setLoginOperation(CtLoginOperation loginOperation) {
		this.loginOperation = loginOperation;
	}

	public Boolean getFlag() {
		return flag;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	private List<CtRole> roles;
	private CtRoleManager roleManager;

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.getManagerDTO();
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	// 管理员列表
	public String list() {
		try {
			page = new Page();
			if (managerDTO.getPage() == 0) {
				managerDTO.setPage(1);
			}
			page.setCurrentPage(managerDTO.getPage());
			this.managers = managerManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "list";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 新增管理员跳转
	public String add() {
		try {
			this.roles = roleManager.loadAllCtrole();
			return "add";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 登录
	public String login() {
		return "login";
	}

	// 登录
	public String main() throws Exception {
		String MManagerId = manager.getMManagerId();
		String MPassword = manager.getMPassword();
		this.manager = managerManager.affirm(MManagerId, MPassword);
		if (manager == null) {
			this.setMessage("用户名或密码不正确");
			return "login";
		} else {
			this.session.remove("admininfosession");
			this.session.put("admininfosession", manager);
			return "main";
		}
	}

	// 验证管理员用户是否存在
	public String checkName() {
		try {
			String MManagerId = this.managerDTO.getMManagerId();
			CtManager cm = managerManager.getManagerByMManagerId(MManagerId);
			if (cm == null) {
				this.result = "success";
			} else {
				this.result = "error";
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 保存新增
	public String save() throws Exception {
		ActionContext ac = ActionContext.getContext().getActionInvocation()
				.getInvocationContext();
		HttpServletRequest httpServletRequest = (HttpServletRequest) ac
				.get(ServletActionContext.HTTP_REQUEST);
		String ip = httpServletRequest.getRemoteAddr();
		Date date = new Date();
		SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		CtManager cm = new CtManager();
		cm.setMManagerId(manager.getMManagerId());
		cm.setRoleId(roleDTO.getRoleId());
		String pwd = manager.getMPassword();
		String passwdorg = "CT" + manager.getMManagerId() + pwd;
		String encryptPwd = MD5Util.getEncryptPasswd(passwdorg);
		cm.setMLastIp(ip);
		cm.setMLastTime(siFormat.format(date));
		cm.setMPassword(encryptPwd);
		managerManager.save(cm);
		return "save";
	}

	// 修改管理员信息
	public String editManager() {
		try {
			Integer Mid = managerDTO.getMId();
			this.manager = managerManager.getManagerByMid(Mid);
			this.roles = roleManager.loadAllCtrole();
			this.request.put("manager", manager);
			return "editManager";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 保存修改
	public String saveEdit() {
		try {
			Integer Mid = manager.getMId();
			String MManagername = this.manager.getMManagername();
			CtManager cm = managerManager.getManagerByMid(Mid);
			cm.setMManagername(MManagername);
			cm.setRoleId(roleDTO.getRoleId());
			managerManager.updateManager(cm);
			return "saveEdit";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 批量删除
	public String del() {
		try {
			for (Integer m : managerDTO.getMid()) {
				this.managerManager.delete(m);
			}
			return "delManager";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 搜素
	public String search() {
		try {
			Page page = new Page();
			if (managerDTO.getPage() == 0) {
				managerDTO.setPage(1);
			}
			page.setCurrentPage(managerDTO.getPage());
			if (managerDTO.getKeyword() != null
					&& !"请输入你要查询的关键词".equals(managerDTO.getKeyword())) {
				this.managers = managerManager.findAll(managerDTO.getKeyword(),
						page);
			} else {
				this.managers = managerManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "search";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 修改密码
	public String editPwd() {
		return "editPwd";
	}

	// 保存修改的密码
	public String saveEditPwd() throws Exception {
		Integer Mid = 1155;
		CtManager cm = managerManager.getManagerByMid(Mid);
		String pwd = manager.getMPassword();
		String passwdorg = "CT" + manager.getMManagerId() + pwd;
		String encryptPwd = MD5Util.getEncryptPasswd(passwdorg);
		cm.setMPassword(encryptPwd);
		managerManager.updateManager(cm);
		return "saveEditPwd";
	}

	// 退出
	public String outLogin() {

		return SUCCESS;
	}

	// 重置密码
	public String resetPwd() throws Exception {
		String MManagerId = managerDTO.getMManagerId();
		String pwd = MD5Util.getEncryptPasswd("CT" + MManagerId+"123456");

		flag = this.managerManager.resetPwd(MManagerId, pwd);
		return SUCCESS;
	}
	
	//验证密码
	public String checkPassword(){
		String MManagerPassword = managerDTO.getMPassword();
		CtManager c = (CtManager) session.get("admininfosession");
		try {
			this.manager = managerManager.affirm(c.getMManagerId(), MManagerPassword);
			if(manager == null){
				flag = false;
			} else {
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	//修改管理员密码
	public String editManagerPassword() throws Exception{
		String MManagerPassword = managerDTO.getMPassword();
		String MManagerPasswordAgain = managerDTO.getMPasswordAgain();
		if(MManagerPassword != null && !MManagerPassword.equals("")){
			if(MManagerPassword.equals(MManagerPasswordAgain)){
				CtManager c = (CtManager) session.get("admininfosession");
				String pwd = MD5Util.getEncryptPasswd("CT" + c.getMManagerId()+MManagerPassword);
				flag = this.managerManager.resetPwd(c.getMManagerId(), pwd);
				session.remove("admininfosession");
			}
		}
		return SUCCESS;
	}
	public List<CtManager> getManagers() {
		return managers;
	}

	public void setManagers(List<CtManager> managers) {
		this.managers = managers;
	}

	public CtManagerDTO getManagerDTO() {
		return managerDTO;
	}

	public void setManagerDTO(CtManagerDTO managerDTO) {
		this.managerDTO = managerDTO;
	}

	public CtManager getManager() {
		return manager;
	}

	public void setManager(CtManager manager) {
		this.manager = manager;
	}

	public CtManagerManager getManagerManager() {
		return managerManager;
	}

	public void setManagerManager(CtManagerManager managerManager) {
		this.managerManager = managerManager;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtRole getCtRole() {
		return ctRole;
	}

	public void setCtRole(CtRole ctRole) {
		this.ctRole = ctRole;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public CtRoleManager getRoleManager() {
		return roleManager;
	}

	public void setRoleManager(CtRoleManager roleManager) {
		this.roleManager = roleManager;
	}

	public List<CtRole> getRoles() {
		return roles;
	}

	public void setRoles(List<CtRole> roles) {
		this.roles = roles;
	}

	public CtRoleDTO getRoleDTO() {
		return roleDTO;
	}

	public void setRoleDTO(CtRoleDTO roleDTO) {
		this.roleDTO = roleDTO;
	}
	
	//业务日志列表
	public String businessLog(){
		try {
			Page page = new Page();
			if (managerDTO.getPage() == 0) {
				managerDTO.setPage(1);
			}
			String key = "";
			page.setCurrentPage(managerDTO.getPage());
			if (managerDTO.getKeyword() != null
					&& !"请输入你要查询的关键词".equals(managerDTO.getKeyword())) {
				key = managerDTO.getKeyword();
			} else {
				key = "";
			}
			String type = "2";
			if(managerDTO.getLogType() != null && managerDTO.getLogType().equals("1")){
				type = "1";
			} else if(managerDTO.getLogType() != null && managerDTO.getLogType().equals("0")) {
				type = "0";
			} else {
				type = "2";
			}
			session.put("logType", type);
			loginOperationsList = loginOperationManager.findAllLoginByPageAndKey(page, key, managerDTO.getStime(), managerDTO.getEtime(), type, managerDTO.getUserName());
			if(loginOperationsList != null){
				for (int i = 0; i < loginOperationsList.size(); i++) {
					if(loginOperationsList.get(i).getLogType().equals("1")){
						loginOperationsList.get(i).setUserName(userManager.getCtUserByUId(loginOperationsList.get(i).getUId().longValue()).getUUsername());
						loginOperationsList.get(i).setAdminName("");
					} else if (loginOperationsList.get(i).getLogType().equals("0")){
						loginOperationsList.get(i).setAdminName(managerManager.getManagerByMid(loginOperationsList.get(i).getUId().intValue()).getMManagerId());
						loginOperationsList.get(i).setUserName("");
					} else {
					}
				}
				
				page.setTotalPage(page.getTotalPage());
				this.request.put("pages", page);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

}
