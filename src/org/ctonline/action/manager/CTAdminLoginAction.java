package org.ctonline.action.manager;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.dto.manager.CtManagerDTO;
import org.ctonline.dto.role.CtRoleDTO;
import org.ctonline.manager.basic.CtLoginLogManager;
import org.ctonline.manager.manager.CtManagerManager;
import org.ctonline.manager.role.CtRoleManager;
import org.ctonline.po.basic.CtLoginLog;
import org.ctonline.po.manager.CtManager;
import org.ctonline.po.user.CtRole;
import org.ctonline.util.MD5Util;
import org.ctonline.util.Page;
import org.ctonline.util.UtilDate;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class CTAdminLoginAction extends ActionSupport implements RequestAware,ModelDriven<Object>,SessionAware{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map<String, Object> session;
	private List<CtManager> managers;
	private CtManagerDTO managerDTO = new CtManagerDTO();
	private CtManager manager;
	private CtManagerManager managerManager;
	private Map<String, Object> request;
	private String message;
	private CtRoleManager roleManager;
	private List<?> modules;
	public CtRoleManager getRoleManager() {
		return roleManager;
	}

	public void setRoleManager(CtRoleManager roleManager) {
		this.roleManager = roleManager;
	}

	public CtLoginLogManager getLogManager() {
		return logManager;
	}

	public void setLogManager(CtLoginLogManager logManager) {
		this.logManager = logManager;
	}

	private CtLoginLog log = new CtLoginLog();
	private CtLoginLogManager logManager;
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.getManagerDTO();
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}
	//璺宠浆鍒扮櫥闄嗙晫闈�
	public String login(){
		return "login";
	}
	
	//鐧诲綍
	public String main() throws Exception{
		if(manager != null){
			String MManagerId = manager.getMManagerId();
			String MPassword = manager.getMPassword();
			CtManager cm = managerManager.affirm(MManagerId, MPassword);
			if(cm == null){
				this.setMessage("用户名或密码不正确");
				return "login";
			}else {
				CtLoginLog log = new CtLoginLog();
				log.setLogClass("CTAdminLoginAcion");
				log.setLogDesc(cm.getCtRole().getRoleName()+":"+cm.getMManagerId()+"登陆了后台系统");
				log.setLogModule("登录模块");
				log.setLogTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				HttpServletRequest req = ServletActionContext.getRequest();
				String Ip =UtilDate.getIpAddr(req);
				log.setLogIp(Ip);
				log.setUId(cm.getMId());
				this.session.remove("admininfosession");
				this.session.put("admininfosession", cm);
				logManager.save(log);
				return "main";
			}
		}else {
			return "login";
		}
	}
	private Page page;
	
	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}
	List<CtLoginLog> loginLogsList = new ArrayList<CtLoginLog>();
	
	public List<CtLoginLog> getLoginLogsList() {
		return loginLogsList;
	}

	public void setLoginLogsList(List<CtLoginLog> loginLogsList) {
		this.loginLogsList = loginLogsList;
	}

	//登录日志列表
	public String adminLoginLog(){
		page = new Page();
		if (managerDTO.getPage() == 0) {
			managerDTO.setPage(1);
		}
		page.setCurrentPage(managerDTO.getPage());
		loginLogsList = logManager.getLogByPage(page);
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);
		return SUCCESS;
	}
	
	public List<CtManager> getManagers() {
		return managers;
	}

	public void setManagers(List<CtManager> managers) {
		this.managers = managers;
	}

	public CtManagerDTO getManagerDTO() {
		return managerDTO;
	}

	public void setManagerDTO(CtManagerDTO managerDTO) {
		this.managerDTO = managerDTO;
	}

	public CtManager getManager() {
		return manager;
	}

	public void setManager(CtManager manager) {
		this.manager = manager;
	}

	public CtManagerManager getManagerManager() {
		return managerManager;
	}

	public void setManagerManager(CtManagerManager managerManager) {
		this.managerManager = managerManager;
	}
	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
