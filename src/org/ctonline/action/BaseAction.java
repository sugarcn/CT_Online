package org.ctonline.action;

import org.ctonline.common.PageBean;

import com.opensymphony.xwork2.ActionSupport;

public class BaseAction extends ActionSupport {

	private PageBean pagebean = new PageBean();

	public PageBean getPagebean() {
		return pagebean;
	}

	public void setPagebean(PageBean pagebean) {
		this.pagebean = pagebean;
	}

}
