package org.ctonline.action.role;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.dto.module.CtModuleDTO;
import org.ctonline.dto.role.CtRoleDTO;
import org.ctonline.dto.rolemodule.CtRoleModuleDTO;
import org.ctonline.manager.role.CtRoleManager;
import org.ctonline.manager.user.CtUserManager;
import org.ctonline.po.basic.CtModule;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.user.CtRole;
import org.ctonline.po.user.CtRoleModule;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class RoleAction extends ActionSupport implements RequestAware,
		ModelDriven<Object> {

	private CtRoleManager roleManager;
	private CtRole role;
	private CtRoleDTO roleDTO = new CtRoleDTO();
	private List<CtRole> roles;
	private List<?> modules;
	private CtModuleDTO moduleDTO = new CtModuleDTO();
	private Page page;
	private Map<String, Object> request;
	private String id;
	private CtRoleModule roleModule;
	private CtRoleModuleDTO roleModuleDTO = new CtRoleModuleDTO();
	private Integer roleId;
	private String right;
	private String moduleid;

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.roleDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	// 查询所有的等级
	public String list() {
		try {
			page = new Page();
			if (roleDTO.getPage() == 0) {
				roleDTO.setPage(1);
			}
			page.setCurrentPage(roleDTO.getPage());
			this.roles = roleManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "list";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 新增角色跳转
	public String add() {
		return "add";
	}

	// 保存新增角色
	public String save() {
		try {
			CtRole cr = new CtRole();
			cr.setRoleName(roleDTO.getRoleName());
			cr.setRoleDesc(roleDTO.getRoleDesc());
			roleManager.save(cr);
			return "save";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 角色的权限
	public String role() {
		try {
			Integer roleid = roleDTO.getRoleId();
			this.roleId = roleid;
			modules = roleManager.getModuleList(roleid);
			return "role";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 修改跳转
	public String editRole() {
		try {
			Integer roleid = roleDTO.getRoleId();
			this.role = roleManager.getRoleByRoleId(roleid);
			return "editRole";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 保存修改
	public String saveEdit() {
		try {
			CtRole cr = this.getRole();
			this.roleManager.updateRole(cr);
			return "saveEdit";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 新增
	public String add1() {
		try {
			CtRole cr = new CtRole();
			cr.setRoleDesc(roleDTO.getRoleDesc());
			cr.setRoleName(roleDTO.getRoleName());
			this.roleManager.save(cr);
			return "save";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 删除角色
	public String delRole() {

		try {
			for (Long m : roleDTO.getMid()) {
				// Integer roleid =new Integer(m.intValue());
				Integer roleid = Integer.parseInt(m.toString());
				this.roleManager.delRole(roleid);

			}
			return "delRole";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	// 保存修改角色权限
	public String saveOrUpdate() {
		try {
			String rightarray = roleModuleDTO.getRight();
			String modid = roleModuleDTO.getModuleId();
			String[] m1 = modid.split(",", 2);
			String roleid = m1[0];
			String modId = m1[1];
			String[] a = rightarray.split(",");// 权限数组
			String[] m2 = modId.split(",");// 模块id数组
			for (int i = 0; i < a.length; i++) {

				CtRoleModule crm = new CtRoleModule();
				crm.setRoleId(roleId);
				crm.setModuleId(Long.parseLong(m2[i]));
				crm.setRight(a[i]);

				CtRoleModule c1 = roleManager.queryCtRoleModuleByRoleIdAndMoudle(
						Long.parseLong(m2[i]), Long.parseLong(roleid));
				if (c1 == null) {
					roleManager.save(crm);
				} else {
					if (c1.getRoleModuleId() != null) {
						crm.setRoleModuleId(c1.getRoleModuleId());
						roleManager.update(crm);
					}
				}
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return "saveOrUpdate";
	}

	public String find1() {
		try {
			Long roleid = roleDTO.getId();
			Integer id = new Integer(roleid.intValue());
			role = new CtRole();
			role.setRoleId(id);
			this.role = this.roleManager.findById(id);
			roleDTO.setRoleName(role.getRoleName());
			roleDTO.setRoleDesc(role.getRoleDesc());

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}

	public String update1() {
		try {
			Long roleid = roleDTO.getId();
			Integer id = new Integer(roleid.intValue());
			CtRole cr = new CtRole();
			cr = this.roleManager.findById(id);

			cr.setRoleDesc(roleDTO.getRoleDesc());
			cr.setRoleName(roleDTO.getRoleName());
			roleManager.update1(cr);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public CtRoleManager getRoleManager() {
		return roleManager;
	}

	public void setRoleManager(CtRoleManager roleManager) {
		this.roleManager = roleManager;
	}

	public CtRole getRole() {
		return role;
	}

	public void setRole(CtRole role) {
		this.role = role;
	}

	public CtRoleDTO getRoleDTO() {
		return roleDTO;
	}

	public void setRoleDTO(CtRoleDTO roleDTO) {
		this.roleDTO = roleDTO;
	}

	public List<CtRole> getRoles() {
		return roles;
	}

	public void setRoles(List<CtRole> roles) {
		this.roles = roles;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<?> getModules() {
		return modules;
	}

	public void setModules(List<?> modules) {
		this.modules = modules;
	}

	public CtModuleDTO getModuleDTO() {
		return moduleDTO;
	}

	public void setModuleDTO(CtModuleDTO moduleDTO) {
		this.moduleDTO = moduleDTO;
	}

	public CtRoleModule getRoleModule() {
		return roleModule;
	}

	public void setRoleModule(CtRoleModule roleModule) {
		this.roleModule = roleModule;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getRight() {
		return right;
	}

	public void setRight(String right) {
		this.right = right;
	}

	public String getModuleid() {
		return moduleid;
	}

	public void setModuleid(String moduleid) {
		this.moduleid = moduleid;
	}

	public CtRoleModuleDTO getRoleModuleDTO() {
		return roleModuleDTO;
	}

	public void setRoleModuleDTO(CtRoleModuleDTO roleModuleDTO) {
		this.roleModuleDTO = roleModuleDTO;
	}

}
