package org.ctonline.action.help;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.dto.help.CtHelpDTO;
import org.ctonline.dto.help.CtHelpTypeDTO;
import org.ctonline.manager.help.CtHelpManager;
import org.ctonline.manager.help.CtHelpTypeManager;
import org.ctonline.po.help.CtHelp;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.util.Page;

public class CtHelpAction extends ActionSupport
  implements RequestAware, ModelDriven<Object>, SessionAware
{
  private List<CtHelp> helps;
  private CtHelpManager helpManager;
  private CtHelpTypeManager helpTypeManager;
  private Page page;
  private CtHelp help;
  private CtHelpDTO helpDTO = new CtHelpDTO();
  private Map<String, Object> request;
  private CtHelpType helpType;
  private CtHelpTypeDTO helptypeDTO = new CtHelpTypeDTO();
  private List<CtHelpType> typeList;
  private Map<String, Object> session;

  public List<CtHelp> getHelps()
  {
    return this.helps;
  }

  public void setHelps(List<CtHelp> helps) {
    this.helps = helps;
  }

  public CtHelpManager getHelpManager() {
    return this.helpManager;
  }

  public void setHelpManager(CtHelpManager helpManager) {
    this.helpManager = helpManager;
  }

  public CtHelpTypeManager getHelpTypeManager() {
    return this.helpTypeManager;
  }

  public void setHelpTypeManager(CtHelpTypeManager helpTypeManager) {
    this.helpTypeManager = helpTypeManager;
  }

  public Page getPage() {
    return this.page;
  }

  public void setPage(Page page) {
    this.page = page;
  }

  public CtHelp getHelp() {
    return this.help;
  }

  public void setHelp(CtHelp help) {
    this.help = help;
  }

  public CtHelpDTO getHelpDTO() {
    return this.helpDTO;
  }

  public void setHelpDTO(CtHelpDTO helpDTO) {
    this.helpDTO = helpDTO;
  }

  public CtHelpType getHelpType() {
    return this.helpType;
  }

  public void setHelpType(CtHelpType helpType) {
    this.helpType = helpType;
  }

  public CtHelpTypeDTO getHelptypeDTO() {
    return this.helptypeDTO;
  }

  public void setHelptypeDTO(CtHelpTypeDTO helptypeDTO) {
    this.helptypeDTO = helptypeDTO;
  }

  public List<CtHelpType> getTypeList() {
    return this.typeList;
  }

  public void setTypeList(List<CtHelpType> typeList) {
    this.typeList = typeList;
  }

  public Map<String, Object> getRequest() {
    return this.request;
  }

  public Object getModel()
  {
    return getHelpDTO();
  }

  public void setRequest(Map<String, Object> request)
  {
    this.request = request;
  }
  public String tohelptype() {
    try {
      this.page = new Page();
      if (this.helptypeDTO.getPage() == 0) {
        this.helptypeDTO.setPage(1);
      }
      this.page.setCurrentPage(this.helptypeDTO.getPage());
      this.typeList = this.helpTypeManager.loadAll(this.page);
      this.page.setTotalPage(this.page.getTotalPage());
      this.request.put("pages", this.page);
      return "success";
    }
    catch (Exception e) {
      e.printStackTrace();
    }return "error";
  }

  public String list()
  {
    try {
      this.page = new Page();
      if (this.helpDTO.getPage() == 0) {
        this.helpDTO.setPage(1);
      }
      this.page.setCurrentPage(this.helpDTO.getPage());
      this.typeList = this.helpManager.queryType();
      this.helps = this.helpManager.loadAll(this.page);
      this.page.setTotalPage(this.page.getTotalPage());
      this.request.put("pages", this.page);
      return "success";
    }
    catch (Exception e) {
      e.printStackTrace();
    }return "error";
  }

  public String getOneHelp()
  {
    int HId = 2;
    this.help = this.helpManager.getHelpByHId(HId);
    return "showad";
  }

  public String add() {
    try {
      CtHelp a = new CtHelp();
      a.setHId(this.helpDTO.getHId());
      a.setHTitle(this.helpDTO.getHTitle());
      a.setHDesc(this.helpDTO.getHDesc());
      a.setHTypeId(this.helpDTO.getHTypeId());

      String maps = "新增了名称为:(" + a.getHTitle() + ")的功能";
      this.session.remove("inserts");
      this.session.put("inserts", maps);

      Integer aa = Integer.valueOf(this.helpManager.save(a));
      return "success";
    }
    catch (Exception e) {
      e.printStackTrace();
    }return "error";
  }

  public String find()
  {
    try
    {
      Page page = new Page();
      if (this.helpDTO.getPage() == 0) {
        this.helpDTO.setPage(1);
      }
      int id = this.helpDTO.getId();
      CtHelp m = new CtHelp();
      m.setHId(Integer.valueOf(id));
      this.typeList = this.helpManager.queryType();
      this.help = this.helpManager.findById(id);
      Map session = ActionContext.getContext().getSession();
      session.put("help", this.help);
      this.helpDTO.setHDesc(this.help.getHDesc());
      this.helpDTO.setHTitle(this.help.getHTitle());
      this.helpDTO.setHTypeId(this.help.getHTypeId());
      return "success";
    }
    catch (Exception e) {
      e.printStackTrace();
    }return "error";
  }

  public String del()
  {
    try {
      for (int m : this.helpDTO.getMid()) {
        CtHelp a = new CtHelp();
        a = this.helpManager.findById(m);

        String maps = "删除了名称为:(" + a.getHTitle() + ")的功能";
        this.session.remove("deletes");
        this.session.put("deletes", maps);

        this.helpManager.delete(m);
      }
      return "success";
    }
    catch (Exception e) {
      e.printStackTrace();
    }return "error";
  }

  public String saveHelp() {
    try {
      CtHelp help = new CtHelp();
      help.setHTitle(this.helpDTO.getHTitle());
      help.setHDesc(this.helpDTO.getHDesc());
      help.setHTypeId(this.helpDTO.getHTypeId());

      String maps = "新增了名称为:(" + help.getHTitle() + ")的功能";
      this.session.remove("inserts");
      this.session.put("inserts", maps);

      Integer aa = Integer.valueOf(this.helpManager.save(help));
      return "success";
    }
    catch (Exception e) {
      e.printStackTrace();
    }return "error";
  }

  public String update()
  {
    try
    {
      CtHelp m = new CtHelp();
      Integer mid = this.helpDTO.getHId();
      m = this.helpManager.findById(this.helpDTO.getId());
      m.setHTitle(this.helpDTO.getHTitle());
      m.setHDesc(this.helpDTO.getHDesc());
      m.setHTypeId(this.helpDTO.getHTypeId());

      String maps = "新增了名称为:(" + m.getHTitle() + ")的功能";
      this.session.remove("updates");
      this.session.put("updates", maps);

      this.helpManager.update(m);
      return "success";
    }
    catch (Exception e) {
      e.printStackTrace();
    }return "error";
  }

  public String search()
  {
    try {
      Page page = new Page();
      if (this.helpDTO.getPage() == 0) {
        this.helpDTO.setPage(1);
      }
      page.setCurrentPage(this.helpDTO.getPage());
      if ((this.helpDTO.getKeyword() != null) && 
        (!this.helpDTO.getKeyword().equals("请输入关键字")))
        this.helps = this.helpManager.findAll(this.helpDTO.getKeyword(), page);
      else {
        this.helps = this.helpManager.loadAll(page);
      }
      page.setTotalPage(page.getTotalPage());
      this.request.put("pages", page);
      return "success";
    }
    catch (Exception e) {
      e.printStackTrace();
    }return "error";
  }

  public Map<String, Object> getSession()
  {
    return this.session;
  }

  public void setSession(Map<String, Object> session) {
    this.session = session;
  }
}