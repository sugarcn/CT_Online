package org.ctonline.action.help;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.dto.help.CtHelpTypeDTO;
import org.ctonline.manager.help.CtHelpTypeManager;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.util.Page;

public class CtHelpTypeAction extends ActionSupport
  implements RequestAware, ModelDriven<Object>, SessionAware
{
  private List<CtHelpType> helpTypes;
  private CtHelpTypeManager helpTypeManager;
  private Map<String, Object> request;
  private Page page;
  private CtHelpTypeDTO helpTypeDTO = new CtHelpTypeDTO();
  private CtHelpType helpType;
  private Integer parentId;
  private Map<String, Object> session;

  public List<CtHelpType> getHelpTypes()
  {
    return this.helpTypes;
  }

  public void setHelpTypes(List<CtHelpType> helpTypes) {
    this.helpTypes = helpTypes;
  }

  public CtHelpTypeManager getHelpTypeManager() {
    return this.helpTypeManager;
  }

  public void setHelpTypeManager(CtHelpTypeManager helpTypeManager) {
    this.helpTypeManager = helpTypeManager;
  }

  public CtHelpTypeDTO getHelpTypeDTO() {
    return this.helpTypeDTO;
  }

  public void setHelpTypeDTO(CtHelpTypeDTO helpTypeDTO) {
    this.helpTypeDTO = helpTypeDTO;
  }

  public CtHelpType getHelpType() {
    return this.helpType;
  }

  public void setHelpType(CtHelpType helpType) {
    this.helpType = helpType;
  }

  public Map<String, Object> getRequest() {
    return this.request;
  }

  public Page getPage() {
    return this.page;
  }

  public void setPage(Page page) {
    this.page = page;
  }

  public Integer getParentId() {
    return this.parentId;
  }

  public void setParentId(Integer parentId) {
    this.parentId = parentId;
  }

  public Object getModel()
  {
    return this.helpTypeDTO;
  }

  public void setRequest(Map<String, Object> request)
  {
    this.request = request;
  }

  public String list() {
    try {
      this.page = new Page();
      if (this.helpTypeDTO.getPage() == 0) {
        this.helpTypeDTO.setPage(1);
      }
      this.page.setCurrentPage(this.helpTypeDTO.getPage());
      this.helpTypes = this.helpTypeManager.loadAll(this.page);
      this.page.setTotalPage(this.page.getTotalPage());
      this.request.put("pages", this.page);
      return "success";
    }
    catch (Exception e) {
      e.printStackTrace();
    }return "error";
  }

  public String add()
  {
    try {
      CtHelpType a = new CtHelpType();
      a.setHTypeId(this.helpTypeDTO.getHTypeId());
      a.setHTypeName(this.helpTypeDTO.getHTypeName());

      String maps = "新增了名称为:(" + a.getHTypeName() + ")的功能";
      this.session.remove("inserts");
      this.session.put("inserts", maps);

      Integer aa = this.helpTypeManager.save(a);
      return "success";
    }
    catch (Exception e) {
      e.printStackTrace();
    }return "error";
  }

  public String find()
  {
    try {
      Integer id = this.helpTypeDTO.getId();
      this.helpType = new CtHelpType();
      this.helpType.setHTypeId(id);
      this.helpType = this.helpTypeManager.findById(id.intValue());
      this.helpTypeDTO.setHTypeId(this.helpType.getHTypeId());
      this.helpTypeDTO.setHTypeName(this.helpType.getHTypeName());
      return "success";
    }
    catch (Exception e) {
      e.printStackTrace();
    }return "error";
  }

  public String update()
  {
    try {
      CtHelpType m = new CtHelpType();
      Integer mid = this.helpTypeDTO.getHTypeId();
      m = this.helpTypeManager.findById(this.helpTypeDTO.getId().intValue());
      m.setHTypeName(this.helpTypeDTO.getHTypeName());

      String maps = "修改了名称为:(" + m.getHTypeName() + ")的功能";
      this.session.remove("updates");
      this.session.put("updates", maps);

      this.helpTypeManager.update(m);
      return "success";
    }
    catch (Exception e) {
      e.printStackTrace();
    }return "error";
  }

  public String del()
  {
    try {
      for (int m : this.helpTypeDTO.getMid()) {
        CtHelpType e = new CtHelpType();
        e = this.helpTypeManager.findById(m);

        String maps = "删除了名称为:(" + e.getHTypeName() + ")的功能";
        this.session.remove("deletes");
        this.session.put("deletes", maps);

        this.helpTypeManager.delete(m);
      }
      return "success";
    }
    catch (Exception e) {
      e.printStackTrace();
    }return "error";
  }

  public String search()
  {
    try {
      Page page = new Page();
      if (this.helpTypeDTO.getPage() == 0) {
        this.helpTypeDTO.setPage(1);
      }
      page.setCurrentPage(this.helpTypeDTO.getPage());
      if ((this.helpTypeDTO.getKeyword() != null) && 
        (!this.helpTypeDTO.getKeyword().equals("请输入关键字")))
        this.helpTypes = this.helpTypeManager.findAll(this.helpTypeDTO.getKeyword(), page);
      else {
        this.helpTypes = this.helpTypeManager.loadAll(page);
      }
      page.setTotalPage(page.getTotalPage());
      this.request.put("pages", page);
      return "success";
    }
    catch (Exception e) {
      e.printStackTrace();
    }return "error";
  }

  public Map<String, Object> getSession()
  {
    return this.session;
  }

  public void setSession(Map<String, Object> session) {
    this.session = session;
  }
}