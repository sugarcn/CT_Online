package org.ctonline.action.pay;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.ctonline.dto.goods.CtBomDTO;
import org.ctonline.manager.pay.ICtPayManager;
import org.ctonline.po.basic.CtPaymentWx;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.config.entities.ActionConfig;
import com.sun.corba.se.spi.servicecontext.ServiceContext;

public class PayAction extends ActionSupport {
	
	private ICtPayManager payManager;
	
	public ICtPayManager getPayManager() {
		return payManager;
	}

	public void setPayManager(ICtPayManager payManager) {
		this.payManager = payManager;
	}
	private List<CtPaymentWx> wxs = new ArrayList<CtPaymentWx>();
	
	public List<CtPaymentWx> getWxs() {
		return wxs;
	}

	public void setWxs(List<CtPaymentWx> wxs) {
		this.wxs = wxs;
	}
	private Map<String, Object> session;
	
	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
	private CtBomDTO bomDTO = new CtBomDTO();
	
	public CtBomDTO getBomDTO() {
		return bomDTO;
	}

	public void setBomDTO(CtBomDTO bomDTO) {
		this.bomDTO = bomDTO;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}
	private Page page;
	public String wxpayList(){
		session = ActionContext.getContext().getSession();
		page = new Page();
		if (bomDTO.getPage() == 0) {
			bomDTO.setPage(1);
		}
		page.setCurrentPage(bomDTO.getPage());
		wxs = payManager.findAllByPage(page);
		page.setTotalPage(page.getTotalPage());
		this.session.put("pages", page);
		
		return SUCCESS;
	}
}
