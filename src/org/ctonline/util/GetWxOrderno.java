package org.ctonline.util;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.Args;
import org.apache.http.util.CharArrayBuffer;
import org.apache.http.util.EntityUtils;
import org.ctonline.util.http.HttpClientConnectionManager;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;


public class GetWxOrderno
{
  public static DefaultHttpClient httpclient;

  static
  {
    httpclient = new DefaultHttpClient();
    httpclient = (DefaultHttpClient)HttpClientConnectionManager.getSSLInstance(httpclient);
  }


  /**
   *description:获取预支付id
   *@param url
   *@param xmlParam
   *@return
   * @author ex_yangxiaoyi
   * @see
   */
  public static String getPayNo(String url,String xmlParam){
	  DefaultHttpClient client = new DefaultHttpClient();
	  client.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);
	  HttpPost httpost= HttpClientConnectionManager.getPostMethod(url);
	  String prepay_id = "";
     try {
		 httpost.setEntity(new StringEntity(xmlParam, "UTF-8"));
		 HttpResponse response = httpclient.execute(httpost);
	     String jsonStr = EntityUtils.toString(response.getEntity(), "UTF-8");
	     //System.out.println(jsonStr);
	    if(jsonStr.indexOf("FAIL")!=-1){
	    	return prepay_id;
	    }
	    Map map = doXMLParse(jsonStr);
	    prepay_id  = (String) map.get("prepay_id");
	} catch (Exception e) {
		// TODO Auto-generated catch block
		//e.printStackTrace();
	}
	return prepay_id;
  }
  
  /**
   *description:获取扫码支付连接
   *@param url
   *@param xmlParam
   *@return
   * @author ex_yangxiaoyi
   * @see
   */
  public static String getCodeUrl(String url,String xmlParam){
	  DefaultHttpClient client = new DefaultHttpClient();
	  client.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);
	  HttpPost httpost= HttpClientConnectionManager.getPostMethod(url);
	  String code_url = "";
     try {
		 httpost.setEntity(new StringEntity(xmlParam, "UTF-8"));
		 HttpResponse response = httpclient.execute(httpost);
	     String jsonStr = EntityUtils.toString(response.getEntity(), "UTF-8");
	     //System.out.println(jsonStr);
	    if(jsonStr.indexOf("FAIL")!=-1){
	    	return code_url;
	    }
	    
	    Map map = doXMLParse(jsonStr);
	    code_url  = (String) map.get("code_url");
	} catch (Exception e) {
		// TODO Auto-generated catch block
		//e.printStackTrace();
	}
	return code_url;
  }
  final static String KEYSTORE_FILE = "/xvdb/apiclient_cert.p12";
	final static String KEYSTORE_PASSWORD = "1434536402";
  
  public static String doSendMoney(String url, String data) throws Exception {
		KeyStore keyStore  = KeyStore.getInstance("PKCS12");

FileInputStream instream = new FileInputStream(new File(KEYSTORE_FILE));//P12文件目录
//		InputStream instream = MoneyUtils.class.getResourceAsStream("apiclient_cert.p12");
      try {
          keyStore.load(instream, KEYSTORE_PASSWORD.toCharArray());//这里写密码..默认是你的MCHID
      } finally {
          instream.close();
      }
      // Trust own CA and all self-signed certs
      SSLContext sslcontext = SSLContexts.custom()
              .loadKeyMaterial(keyStore, KEYSTORE_PASSWORD.toCharArray())//这里也是写密码的
              .build();
      // Allow TLSv1 protocol only
      SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, new String[] { "TLSv1" }, null,
      		SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
      CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
      try {
      	HttpPost httpost = new HttpPost(url); // 设置响应头信息
      	httpost.addHeader("Connection", "keep-alive");
      	httpost.addHeader("Accept", "*/*");
      	httpost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
      	httpost.addHeader("Host", "api.mch.weixin.qq.com");
      	httpost.addHeader("X-Requested-With", "XMLHttpRequest");
      	httpost.addHeader("Cache-Control", "max-age=0");
      	httpost.addHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) ");
  		httpost.setEntity(new StringEntity(data, "UTF-8"));
          CloseableHttpResponse response = httpclient.execute(httpost);
          try {
              HttpEntity entity = response.getEntity();
              String jsonStr = toStringInfo(response.getEntity(),"UTF-8");
              if(jsonStr.indexOf("FAIL")!=-1){
            	  Map map = new GetWxOrderno().doXMLParse(jsonStr);
            	  String errcode = (String) map.get("err_code_des");
            	  
    			  return "error|"+errcode;
    		  }
              //微信返回的报文时GBK，直接使用httpcore解析乱码
            //  String jsonStr = EntityUtils.toString(response.getEntity(),"UTF-8");
              EntityUtils.consume(entity);
             return jsonStr;
          } finally {
              response.close();
          }
      } finally {
          httpclient.close();
      }
	}
  private static String toStringInfo(HttpEntity entity, String defaultCharset) throws Exception, IOException{
		final InputStream instream = entity.getContent();
	    if (instream == null) {
	        return null;
	    }
	    try {
	        Args.check(entity.getContentLength() <= Integer.MAX_VALUE,
	                "HTTP entity too large to be buffered in memory");
	        int i = (int)entity.getContentLength();
	        if (i < 0) {
	            i = 4096;
	        }
	        Charset charset = null;
	        
	        if (charset == null) {
	            charset = Charset.forName(defaultCharset);
	        }
	        if (charset == null) {
	            charset = HTTP.DEF_CONTENT_CHARSET;
	        }
	        final Reader reader = new InputStreamReader(instream, charset);
	        final CharArrayBuffer buffer = new CharArrayBuffer(i);
	        final char[] tmp = new char[1024];
	        int l;
	        while((l = reader.read(tmp)) != -1) {
	            buffer.append(tmp, 0, l);
	        }
	        return buffer.toString();
	    } finally {
	        instream.close();
	    }
	}
  
  /**
   *description:获取退款xml
   *@param url
   *@param xmlParam
   *@return
   * @see
   */
  public static String getRefundXml(String url,String xmlParam){
	  DefaultHttpClient client = new DefaultHttpClient();
	  client.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);
	  HttpPost httpost= HttpClientConnectionManager.getPostMethod(url);
	  HttpGet httpGet = HttpClientConnectionManager.getGetMethod(url);
	  String code_url = "";
	  String jsonStr = "";
	  try {
		  httpost.setEntity(new StringEntity(xmlParam, "UTF-8"));
		  //HttpResponse response1 = httpclient.execute(httpGet);
		  HttpResponse response = httpclient.execute(httpost);
		  jsonStr = EntityUtils.toString(response.getEntity(), "UTF-8");
		  //System.out.println(jsonStr);
		  if(jsonStr.indexOf("FAIL")!=-1){
			  return code_url;
		  }
		  
		  ///Map map = doXMLParse(jsonStr);
		  ///code_url  = (String) map.get("code_url");
	  } catch (Exception e) {
		  // TODO Auto-generated catch block
		  e.printStackTrace();
	  }
	  return jsonStr;
  }
  /**
   *description:获取预支付id prepay_id
   *@param url
   *@param xmlParam
   *@return
   * @author ex_yangxiaoyi
   * @see
   */
  public static Map getXml(String url,String xmlParam){
	  DefaultHttpClient client = new DefaultHttpClient();
	  client.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);
	  HttpPost httpost= HttpClientConnectionManager.getPostMethod(url);
	  String prepayId = "";
	  Map map = null;
	  try {
		  httpost.setEntity(new StringEntity(xmlParam, "UTF-8"));
		  HttpResponse response = httpclient.execute(httpost);
		  String jsonStr = EntityUtils.toString(response.getEntity(), "UTF-8");
		  //System.out.println(jsonStr);
		  if(jsonStr.indexOf("FAIL")!=-1){
			  return null;
		  }
		  
		  map = doXMLParse(jsonStr);
		  //prepayId = (String) map.get("prepay_id");
	  } catch (Exception e) {
		  // TODO Auto-generated catch block
		  //e.printStackTrace();
	  }
	  return map;
  }
  
  
  /**
   *description:获取订单状态
   *@param url
   *@param xmlParam
   *@return
   * @author ex_yangxiaoyi
   * @see
   */
  public static String getOrderSta(String url,String xmlParam){
	  DefaultHttpClient client = new DefaultHttpClient();
	  client.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);
	  HttpPost httpost= HttpClientConnectionManager.getPostMethod(url);
	  String trade_state = "";
     try {
		 httpost.setEntity(new StringEntity(xmlParam, "GBK"));
		 HttpResponse response = httpclient.execute(httpost);
	     String jsonStr = EntityUtils.toString(response.getEntity(), "GBK");
	     if(jsonStr.indexOf("FAIL")!=-1){
	    	return trade_state;
	    }
	    Map map = doXMLParse(jsonStr);
	    trade_state  = (String) map.get("trade_state");
	} catch (Exception e) {
		// TODO Auto-generated catch block
		//e.printStackTrace();
	}
	return trade_state;
  }
  /**
   *description:获取订单状态
   *@param url
   *@param xmlParam
   *@return
   * @author ex_yangxiaoyi
   * @see
   */
  public static String getOrderInfo(String url,String xmlParam){
	  DefaultHttpClient client = new DefaultHttpClient();
	  client.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);
	  HttpPost httpost= HttpClientConnectionManager.getPostMethod(url);
	  String trade_state = "";
     try {
		 httpost.setEntity(new StringEntity(xmlParam, "UTF-8"));
		 HttpResponse response = httpclient.execute(httpost);
	     String jsonStr = EntityUtils.toString(response.getEntity(), "UTF-8");
	     if(jsonStr.indexOf("FAIL")!=-1){
	    	return trade_state;
	    }
	    Map map = doXMLParse(jsonStr);
	    trade_state = jsonStr;
	} catch (Exception e) {
		// TODO Auto-generated catch block
		//e.printStackTrace();
	}
	return trade_state;
  }
  
  /**
	 * 解析xml,返回第一级元素键值对。如果第一级元素有子节点，则此节点的值是子节点的xml数据。
	 * @param strxml
	 * @return
	 * @throws JDOMException
	 * @throws IOException
	 */
	public static Map doXMLParse(String strxml) throws Exception {
		if(null == strxml || "".equals(strxml)) {
			return null;
		}
		
		Map m = new HashMap();
		InputStream in = String2Inputstream(strxml);
		SAXBuilder builder = new SAXBuilder();
		Document doc = builder.build(in);
		Element root = doc.getRootElement();
		List list = root.getChildren();
		Iterator it = list.iterator();
		while(it.hasNext()) {
			Element e = (Element) it.next();
			String k = e.getName();
			String v = "";
			List children = e.getChildren();
			if(children.isEmpty()) {
				v = e.getTextNormalize();
			} else {
				v = getChildrenText(children);
			}
			
			m.put(k, v);
		}
		
		//关闭流
		in.close();
		
		return m;
	}
	/**
	 * 获取子结点的xml
	 * @param children
	 * @return String
	 */
	public static String getChildrenText(List children) {
		StringBuffer sb = new StringBuffer();
		if(!children.isEmpty()) {
			Iterator it = children.iterator();
			while(it.hasNext()) {
				Element e = (Element) it.next();
				String name = e.getName();
				String value = e.getTextNormalize();
				List list = e.getChildren();
				sb.append("<" + name + ">");
				if(!list.isEmpty()) {
					sb.append(getChildrenText(list));
				}
				sb.append(value);
				sb.append("</" + name + ">");
			}
		}
		
		return sb.toString();
	}
  public static InputStream String2Inputstream(String str) {
		return new ByteArrayInputStream(str.getBytes());
	}
  
}