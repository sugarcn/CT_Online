package org.ctonline.util;

import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.ctonline.action.manager.CTAdminLoginAction;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

public class ValidateAdminLogin extends MethodFilterInterceptor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected String doIntercept(ActionInvocation invo) throws Exception {
		// TODO Auto-generated method stub
		if(invo.getAction().getClass().equals(CTAdminLoginAction.class)){
			return invo.invoke();
		}
		Map session =  invo.getInvocationContext().getSession();
		if(session.get("admininfosession") != null){
			return invo.invoke();
		}else {
			return Action.LOGIN;
		}
	}
		
}

