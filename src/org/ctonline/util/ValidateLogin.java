package org.ctonline.util;

import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.ctonline.action.manager.CTAdminLoginAction;
import org.ctonline.action.user.LoginAction;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

public class ValidateLogin extends MethodFilterInterceptor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String doIntercept(ActionInvocation invo) throws Exception {
		// TODO Auto-generated method stub
		if(invo.getAction().getClass().equals(LoginAction.class)){
			return invo.invoke();
		}
		Map session =  invo.getInvocationContext().getSession();
		Cookie[] cookies = ServletActionContext.getRequest().getCookies();
		String [] cooks = null;
		String uname ="";
		String passwd = "";
		if(cookies !=null){
			for(Cookie coo : cookies){
				String aa = coo.getValue();
				cooks = aa.split("==");
				if(cooks.length == 2){
					uname = cooks[0];
					passwd = cooks[1];
				}
			}
			if("".equals(uname) || "".equals(passwd)){
				if(session.get("userinfosession") != null){
					System.out.println(session.get("userinfosession"));
					return "login_user";
				}else {
					return invo.invoke();
				}
			}else {
				return "login_user";
			}
		}else{
			if(session.get("userinfosession") != null){
				return "login_user";
			}else {
				return "login";
			}
		}
		
	}
}

