package org.ctonline.po.user;

/**
 * CtDrawPacket entity. @author MyEclipse Persistence Tools
 */

public class CtDrawPacket implements java.io.Serializable {

	// Fields

	private Integer paId;
	private Integer RMon;
	private Integer RNum;
	private Integer RRNum;
	private Integer RJNum;

	// Constructors

	/** default constructor */
	public CtDrawPacket() {
	}

	/** full constructor */
	public CtDrawPacket(Integer RMon, Integer RNum, Integer RRNum, Integer RJNum) {
		this.RMon = RMon;
		this.RNum = RNum;
		this.RRNum = RRNum;
		this.RJNum = RJNum;
	}

	// Property accessors

	public Integer getPaId() {
		return this.paId;
	}

	public void setPaId(Integer paId) {
		this.paId = paId;
	}

	public Integer getRMon() {
		return this.RMon;
	}

	public void setRMon(Integer RMon) {
		this.RMon = RMon;
	}

	public Integer getRNum() {
		return this.RNum;
	}

	public void setRNum(Integer RNum) {
		this.RNum = RNum;
	}

	public Integer getRRNum() {
		return this.RRNum;
	}

	public void setRRNum(Integer RRNum) {
		this.RRNum = RRNum;
	}

	public Integer getRJNum() {
		return this.RJNum;
	}

	public void setRJNum(Integer RJNum) {
		this.RJNum = RJNum;
	}

}