package org.ctonline.po.user;

import org.ctonline.po.basic.CtRegion;

/**
 * CtUserAddress entity. @author MyEclipse Persistence Tools
 */

public class CtUesrClient implements java.io.Serializable {

	// Fields

	private Integer cid;
	private String cname;
	private Long uid;
	private String cdefult;
	
	
	public String getCdefult() {
		return cdefult;
	}
	public void setCdefult(String cdefult) {
		this.cdefult = cdefult;
	}
	public Integer getCid() {
		return cid;
	}
	public void setCid(Integer cid) {
		this.cid = cid;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public Long getUid() {
		return uid;
	}
	public void setUid(Long uid) {
		this.uid = uid;
	}
	
	
	// Constructors

	
}