package org.ctonline.po.user;

/**
 * CtUserGroupRelation entity. @author MyEclipse Persistence Tools
 */

public class CtUserGroupRelation implements java.io.Serializable {

	// Fields

	private Long UId;
	private Long UGId;
	// Constructors
	public Long getUId() {
		return UId;
	}
	public void setUId(Long uId) {
		UId = uId;
	}
	public Long getUGId() {
		return UGId;
	}
	public void setUGId(Long uGId) {
		UGId = uGId;
	}
	public CtUserGroupRelation(Long uId, Long uGId) {
		super();
		UId = uId;
		UGId = uGId;
	}
	public CtUserGroupRelation() {
		// TODO Auto-generated constructor stub
	}

	/** default constructor */

	/** full constructor */


}