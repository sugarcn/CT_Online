package org.ctonline.po.user;


/**
 * CtUser entity. @author MyEclipse Persistence Tools
 */

public class CtUser implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3590738543262798987L;
	// Fields

	private Long UId;
	private String UUserid;
	private String UUsername;
	private String UPassword;
	private String UMb;
	private String UEmail;
	private String UQq;
	private String UWeibo;
	private String UTb;
	private String USex;
	private String UBirthday;
	private String ULastTime;
	private String URegTime;
	private String ULastIp;
	private Long RId;
	private Long UGId;
	private String UState;
	private String UCode;
	private String UDistribution;
	
	private String UIsCustomer;
	
	
	private Double UCreditLimit;
	private Double URemainingAmount;
	
	
	
//	private CtAddTicket ctAddTicket;
	
	// Constructors

	public String getUIsCustomer() {
		return UIsCustomer;
	}

	public void setUIsCustomer(String uIsCustomer) {
		UIsCustomer = uIsCustomer;
	}

	public Double getUCreditLimit() {
		return UCreditLimit;
	}

	public void setUCreditLimit(Double uCreditLimit) {
		UCreditLimit = uCreditLimit;
	}

	public Double getURemainingAmount() {
		return URemainingAmount;
	}

	public void setURemainingAmount(Double uRemainingAmount) {
		URemainingAmount = uRemainingAmount;
	}

	public String getUDistribution() {
		return UDistribution;
	}

	public void setUDistribution(String uDistribution) {
		UDistribution = uDistribution;
	}

	/** default constructor */
	public CtUser() {
	}

	/** minimal constructor */
	public CtUser(String UUserid, String UUsername, String UPassword, String UMb) {
		this.UUserid = UUserid;
		this.UUsername = UUsername;
		this.UPassword = UPassword;
		this.UMb = UMb;
	}

	/** full constructor */
	public CtUser(String UUserid, String UUsername, String UPassword,
			String UMb, String UEmail, String UQq, String UWeibo, String UTb,
			String USex, String UBirthday, String ULastTime, String URegTime,
			String ULastIp, Long RId, Long UGId) {
		this.UUserid = UUserid;
		this.UUsername = UUsername;
		this.UPassword = UPassword;
		this.UMb = UMb;
		this.UEmail = UEmail;
		this.UQq = UQq;
		this.UWeibo = UWeibo;
		this.UTb = UTb;
		this.USex = USex;
		this.UBirthday = UBirthday;
		this.ULastTime = ULastTime;
		this.URegTime = URegTime;
		this.ULastIp = ULastIp;
		this.RId = RId;
		this.UGId = UGId;
	}

	// Property accessors

	public Long getUId() {
		return this.UId;
	}

	public void setUId(Long UId) {
		this.UId = UId;
	}

	public String getUUserid() {
		return this.UUserid;
	}

	public void setUUserid(String UUserid) {
		this.UUserid = UUserid;
	}

	public String getUUsername() {
		return this.UUsername;
	}

	public void setUUsername(String UUsername) {
		this.UUsername = UUsername;
	}

	public String getUPassword() {
		return this.UPassword;
	}

	public void setUPassword(String UPassword) {
		this.UPassword = UPassword;
	}

	public String getUMb() {
		return this.UMb;
	}

	public void setUMb(String UMb) {
		this.UMb = UMb;
	}

	public String getUEmail() {
		return this.UEmail;
	}

	public void setUEmail(String UEmail) {
		this.UEmail = UEmail;
	}

	public String getUQq() {
		return this.UQq;
	}

	public void setUQq(String UQq) {
		this.UQq = UQq;
	}

	public String getUWeibo() {
		return this.UWeibo;
	}

	public void setUWeibo(String UWeibo) {
		this.UWeibo = UWeibo;
	}

	public String getUTb() {
		return this.UTb;
	}

	public void setUTb(String UTb) {
		this.UTb = UTb;
	}

	public String getUSex() {
		return this.USex;
	}

	public void setUSex(String USex) {
		this.USex = USex;
	}

	public String getUBirthday() {
		return this.UBirthday;
	}

	public void setUBirthday(String UBirthday) {
		this.UBirthday = UBirthday;
	}

	public String getULastTime() {
		return this.ULastTime;
	}

	public void setULastTime(String ULastTime) {
		this.ULastTime = ULastTime;
	}

	public String getURegTime() {
		return this.URegTime;
	}

	public void setURegTime(String URegTime) {
		this.URegTime = URegTime;
	}

	public String getULastIp() {
		return this.ULastIp;
	}

	public void setULastIp(String ULastIp) {
		this.ULastIp = ULastIp;
	}

	public Long getRId() {
		return this.RId;
	}

	public void setRId(Long RId) {
		this.RId = RId;
	}

	public Long getUGId() {
		return this.UGId;
	}

	public void setUGId(Long UGId) {
		this.UGId = UGId;
	}

	public String getUState() {
		return UState;
	}

	public void setUState(String uState) {
		UState = uState;
	}

	public String getUCode() {
		return UCode;
	}

	public void setUCode(String uCode) {
		UCode = uCode;
	}

//	public CtAddTicket getCtAddTicket() {
//		return ctAddTicket;
//	}
//
//	public void setCtAddTicket(CtAddTicket ctAddTicket) {
//		this.ctAddTicket = ctAddTicket;
//	}


}