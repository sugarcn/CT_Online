package org.ctonline.po.user;

/**
 * CtDrawDetail entity. @author MyEclipse Persistence Tools
 */

public class CtDrawDetail implements java.io.Serializable {

	// Fields

	private Integer drDeId;
	private Integer UId;
	private Integer DNum;
	private Integer DSum;
	private String DSn;
	private String DTime;
	private String DGTime;
	private String DOpenid;
	private String DUsername;

	private String mdName;
	
	
	// Constructors

	public String getMdName() {
		return mdName;
	}

	public void setMdName(String mdName) {
		this.mdName = mdName;
	}

	/** default constructor */
	public CtDrawDetail() {
	}

	/** full constructor */
	public CtDrawDetail(Integer UId, Integer DNum, Integer DSum, String DSn,
			String DTime, String DGTime, String DOpenid, String DUsername) {
		this.UId = UId;
		this.DNum = DNum;
		this.DSum = DSum;
		this.DSn = DSn;
		this.DTime = DTime;
		this.DGTime = DGTime;
		this.DOpenid = DOpenid;
		this.DUsername = DUsername;
	}

	// Property accessors

	public Integer getDrDeId() {
		return this.drDeId;
	}

	public void setDrDeId(Integer drDeId) {
		this.drDeId = drDeId;
	}

	public Integer getUId() {
		return this.UId;
	}

	public void setUId(Integer UId) {
		this.UId = UId;
	}

	public Integer getDNum() {
		return this.DNum;
	}

	public void setDNum(Integer DNum) {
		this.DNum = DNum;
	}

	public Integer getDSum() {
		return this.DSum;
	}

	public void setDSum(Integer DSum) {
		this.DSum = DSum;
	}

	public String getDSn() {
		return this.DSn;
	}

	public void setDSn(String DSn) {
		this.DSn = DSn;
	}

	public String getDTime() {
		return this.DTime;
	}

	public void setDTime(String DTime) {
		this.DTime = DTime;
	}

	public String getDGTime() {
		return this.DGTime;
	}

	public void setDGTime(String DGTime) {
		this.DGTime = DGTime;
	}

	public String getDOpenid() {
		return this.DOpenid;
	}

	public void setDOpenid(String DOpenid) {
		this.DOpenid = DOpenid;
	}

	public String getDUsername() {
		return this.DUsername;
	}

	public void setDUsername(String DUsername) {
		this.DUsername = DUsername;
	}

}