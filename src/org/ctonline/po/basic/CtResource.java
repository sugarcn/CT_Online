package org.ctonline.po.basic;

/**
 * CtResource entity. @author MyEclipse Persistence Tools
 */

public class CtResource implements java.io.Serializable {

	// Fields

	private Long RId;
	private String RName;
	private String RUrl;
	private Long RTId;
	private CtResourceType resourceType;
	private Integer RSort;
	

	// Constructors

	public Integer getRSort() {
		return RSort;
	}

	public void setRSort(Integer rSort) {
		RSort = rSort;
	}

	/** default constructor */
	public CtResource() {
	}

	public CtResourceType getResourceType() {
		return resourceType;
	}

	public void setResourceType(CtResourceType resourceType) {
		this.resourceType = resourceType;
	}

	/** minimal constructor */
	public CtResource(String RName) {
		this.RName = RName;
	}

	/** full constructor */
	public CtResource(String RName, String RUrl, Long RTId) {
		this.RName = RName;
		this.RUrl = RUrl;
		this.RTId = RTId;
	}

	// Property accessors

	public Long getRId() {
		return this.RId;
	}

	public void setRId(Long RId) {
		this.RId = RId;
	}

	public String getRName() {
		return this.RName;
	}

	public void setRName(String RName) {
		this.RName = RName;
	}

	public String getRUrl() {
		return this.RUrl;
	}

	public void setRUrl(String RUrl) {
		this.RUrl = RUrl;
	}

	public Long getRTId() {
		return this.RTId;
	}

	public void setRTId(Long RTId) {
		this.RTId = RTId;
	}

}