package org.ctonline.po.basic;

public class CtReplace {

	private Integer reId;
	private String re1;
	private String re2;
	private String re3;
	private String re4;
	private String re5;
	public Integer getReId() {
		return reId;
	}
	public void setReId(Integer reId) {
		this.reId = reId;
	}
	public String getRe1() {
		return re1;
	}
	public void setRe1(String re1) {
		this.re1 = re1;
	}
	public String getRe2() {
		return re2;
	}
	public void setRe2(String re2) {
		this.re2 = re2;
	}
	public String getRe3() {
		return re3;
	}
	public void setRe3(String re3) {
		this.re3 = re3;
	}
	public String getRe4() {
		return re4;
	}
	public void setRe4(String re4) {
		this.re4 = re4;
	}
	public String getRe5() {
		return re5;
	}
	public void setRe5(String re5) {
		this.re5 = re5;
	}
	
	
}
