package org.ctonline.po.basic;

/**
 * CtRegion entity. @author MyEclipse Persistence Tools
 */

public class CtRegion implements java.io.Serializable {

	// Fields

	private Long regionId;
	private String regionName;
	private Long parentId;
	private Integer regionType;

	// Constructors

	/** default constructor */
	public CtRegion() {
	}

	/** minimal constructor */
	public CtRegion(String regionName, Long parentId) {
		this.regionName = regionName;
		this.parentId = parentId;
	}

	/** full constructor */
	public CtRegion(String regionName, Long parentId, Integer regionType) {
		this.regionName = regionName;
		this.parentId = parentId;
		this.regionType = regionType;
	}

	// Property accessors

	public Long getRegionId() {
		return this.regionId;
	}

	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}

	public String getRegionName() {
		return this.regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public Long getParentId() {
		return this.parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getRegionType() {
		return this.regionType;
	}

	public void setRegionType(Integer regionType) {
		this.regionType = regionType;
	}

}