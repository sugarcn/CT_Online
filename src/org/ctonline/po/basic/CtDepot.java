package org.ctonline.po.basic;

/**
 * CtDepot entity. @author MyEclipse Persistence Tools
 */

public class CtDepot implements java.io.Serializable {

	// Fields

	private Integer depotId;
	private String depotName;
	private String depotAdd;
	private String disArr;

	// Constructors

	/** default constructor */
	public CtDepot() {
	}

	/** minimal constructor */
	public CtDepot(String depotName) {
		this.depotName = depotName;
	}

	/** full constructor */
	public CtDepot(String depotName, String depotAdd, String disArr) {
		this.depotName = depotName;
		this.depotAdd = depotAdd;
		this.disArr = disArr;
	}

	// Property accessors

	public Integer getDepotId() {
		return this.depotId;
	}

	public void setDepotId(Integer depotId) {
		this.depotId = depotId;
	}

	public String getDepotName() {
		return this.depotName;
	}

	public void setDepotName(String depotName) {
		this.depotName = depotName;
	}

	public String getDepotAdd() {
		return this.depotAdd;
	}

	public void setDepotAdd(String depotAdd) {
		this.depotAdd = depotAdd;
	}

	public String getDisArr() {
		return this.disArr;
	}

	public void setDisArr(String disArr) {
		this.disArr = disArr;
	}

}