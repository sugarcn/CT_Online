package org.ctonline.po.basic;

public class CtNoticeType implements java.io.Serializable  {

	private Integer noTypeId;
	private String noTypeName;
	private String noTypeDesc;
	
	
	public String getNoTypeDesc() {
		return noTypeDesc;
	}
	public void setNoTypeDesc(String noTypeDesc) {
		this.noTypeDesc = noTypeDesc;
	}
	public Integer getNoTypeId() {
		return noTypeId;
	}
	public void setNoTypeId(Integer noTypeId) {
		this.noTypeId = noTypeId;
	}
	public String getNoTypeName() {
		return noTypeName;
	}
	public void setNoTypeName(String noTypeName) {
		this.noTypeName = noTypeName;
	}
	public CtNoticeType(Integer noTypeId, String noTypeName) {
		super();
		this.noTypeId = noTypeId;
		this.noTypeName = noTypeName;
	}
	public CtNoticeType() {
		super();
	}
	
}
