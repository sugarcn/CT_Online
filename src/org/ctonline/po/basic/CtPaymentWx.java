package org.ctonline.po.basic;

/**
 * CtPaymentWx entity. @author MyEclipse Persistence Tools
 */

public class CtPaymentWx implements java.io.Serializable {

	// Fields

	private Integer payId;
	private String payOrdersn;
	private String payPrepayid;
	private String payPayname;
	private String payPaytotal;
	private String payDatatime;
	private String payOpenId;
	
	
	// Constructors

	public String getPayOpenId() {
		return payOpenId;
	}

	public void setPayOpenId(String payOpenId) {
		this.payOpenId = payOpenId;
	}

	/** default constructor */
	public CtPaymentWx() {
	}

	/** full constructor */
	public CtPaymentWx(String payOrdersn, String payPrepayid,
			String payPayname, String payPaytotal, String payDatatime) {
		this.payOrdersn = payOrdersn;
		this.payPrepayid = payPrepayid;
		this.payPayname = payPayname;
		this.payPaytotal = payPaytotal;
		this.payDatatime = payDatatime;
	}

	// Property accessors

	public Integer getPayId() {
		return this.payId;
	}

	public void setPayId(Integer payId) {
		this.payId = payId;
	}

	public String getPayOrdersn() {
		return this.payOrdersn;
	}

	public void setPayOrdersn(String payOrdersn) {
		this.payOrdersn = payOrdersn;
	}

	public String getPayPrepayid() {
		return this.payPrepayid;
	}

	public void setPayPrepayid(String payPrepayid) {
		this.payPrepayid = payPrepayid;
	}

	public String getPayPayname() {
		return this.payPayname;
	}

	public void setPayPayname(String payPayname) {
		this.payPayname = payPayname;
	}

	public String getPayPaytotal() {
		return this.payPaytotal;
	}

	public void setPayPaytotal(String payPaytotal) {
		this.payPaytotal = payPaytotal;
	}

	public String getPayDatatime() {
		return this.payDatatime;
	}

	public void setPayDatatime(String payDatatime) {
		this.payDatatime = payDatatime;
	}

}