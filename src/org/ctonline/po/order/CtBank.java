package org.ctonline.po.order;

import java.io.Serializable;

public class CtBank
  implements Serializable
{
  private Byte bankId;
  private String deposit;
  private String accountName;
  private String accountNumber;
  
  public CtBank() {}
  
  public CtBank(String deposit, String accountName, String accountNumber)
  {
    this.deposit = deposit;
    this.accountName = accountName;
    this.accountNumber = accountNumber;
  }
  
  public Byte getBankId()
  {
    return this.bankId;
  }
  
  public void setBankId(Byte bankId)
  {
    this.bankId = bankId;
  }
  
  public String getDeposit()
  {
    return this.deposit;
  }
  
  public void setDeposit(String deposit)
  {
    this.deposit = deposit;
  }
  
  public String getAccountName()
  {
    return this.accountName;
  }
  
  public void setAccountName(String accountName)
  {
    this.accountName = accountName;
  }
  
  public String getAccountNumber()
  {
    return this.accountNumber;
  }
  
  public void setAccountNumber(String accountNumber)
  {
    this.accountNumber = accountNumber;
  }
}
