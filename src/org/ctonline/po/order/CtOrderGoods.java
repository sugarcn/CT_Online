package org.ctonline.po.order;

import java.io.Serializable;
import org.ctonline.po.goods.CtGoods;

public class CtOrderGoods
  implements Serializable
{
  private Long orderGoodsId;
  private Long orderId;
  private Long GId;
  private String GPrice;
  private String GNumber;
  private String pack;
  private Long couponId;
  private String GSubtotal;
  private String GParPrice;
  private Long GParNumber;
  private String isParOrSim;
  private String GName;
  private CtGoods ctGoods;
  private String isRefund;
  private String refPrice;
  
  public String getRefPrice() {
	return refPrice;
}

public void setRefPrice(String refPrice) {
	this.refPrice = refPrice;
}

public String getIsRefund() {
	return isRefund;
}

public void setIsRefund(String isRefund) {
	this.isRefund = isRefund;
}

public CtGoods getCtGoods()
  {
    return this.ctGoods;
  }
  
  public String getGParPrice()
  {
    return this.GParPrice;
  }
  
  public void setGParPrice(String gParPrice)
  {
    this.GParPrice = gParPrice;
  }
  
  public Long getGParNumber()
  {
    return this.GParNumber;
  }
  
  public void setGParNumber(Long gParNumber)
  {
    this.GParNumber = gParNumber;
  }
  
  public String getIsParOrSim()
  {
    return this.isParOrSim;
  }
  
  public void setIsParOrSim(String isParOrSim)
  {
    this.isParOrSim = isParOrSim;
  }
  
  public void setCtGoods(CtGoods ctGoods)
  {
    this.ctGoods = ctGoods;
  }
  
  public String getGName()
  {
    return this.GName;
  }
  
  public void setGName(String gName)
  {
    this.GName = gName;
  }
  
  public CtOrderGoods() {}
  
  public CtOrderGoods(Long orderId, Long GId, String GPrice, String GNumber, String pack, String GSubtotal)
  {
    this.orderId = orderId;
    this.GId = GId;
    this.GPrice = GPrice;
    this.GNumber = GNumber;
    this.pack = pack;
    this.GSubtotal = GSubtotal;
  }
  
  public CtOrderGoods(Long orderId, Long GId, String GPrice, String GNumber, String pack, Long couponId, String GSubtotal)
  {
    this.orderId = orderId;
    this.GId = GId;
    this.GPrice = GPrice;
    this.GNumber = GNumber;
    this.pack = pack;
    this.couponId = couponId;
    this.GSubtotal = GSubtotal;
  }
  
  public Long getOrderGoodsId()
  {
    return this.orderGoodsId;
  }
  
  public void setOrderGoodsId(Long orderGoodsId)
  {
    this.orderGoodsId = orderGoodsId;
  }
  
  public Long getOrderId()
  {
    return this.orderId;
  }
  
  public void setOrderId(Long orderId)
  {
    this.orderId = orderId;
  }
  
  public Long getGId()
  {
    return this.GId;
  }
  
  public void setGId(Long GId)
  {
    this.GId = GId;
  }
  
  public String getGPrice()
  {
    return this.GPrice;
  }
  
  public void setGPrice(String GPrice)
  {
    this.GPrice = GPrice;
  }
  
  public String getGNumber()
  {
    return this.GNumber;
  }
  
  public void setGNumber(String GNumber)
  {
    this.GNumber = GNumber;
  }
  
  public String getPack()
  {
    return this.pack;
  }
  
  public void setPack(String pack)
  {
    this.pack = pack;
  }
  
  public Long getCouponId()
  {
    return this.couponId;
  }
  
  public void setCouponId(Long couponId)
  {
    this.couponId = couponId;
  }
  
  public String getGSubtotal()
  {
    return this.GSubtotal;
  }
  
  public void setGSubtotal(String GSubtotal)
  {
    this.GSubtotal = GSubtotal;
  }
}
