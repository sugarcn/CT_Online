package org.ctonline.po.order;

/**
 * CtOrderRelation entity. @author MyEclipse Persistence Tools
 */

public class CtOrderRelation implements java.io.Serializable {

	// Fields

	private Long orid;
	private String payId;
	private String payType;
	private Long UId;

	// Constructors

	/** default constructor */
	public CtOrderRelation() {
	}

	/** full constructor */
	public CtOrderRelation(String payId, String payType, Long UId) {
		this.payId = payId;
		this.payType = payType;
		this.UId = UId;
	}

	// Property accessors

	public Long getOrid() {
		return this.orid;
	}

	public void setOrid(Long orid) {
		this.orid = orid;
	}

	public String getPayId() {
		return this.payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getPayType() {
		return this.payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public Long getUId() {
		return this.UId;
	}

	public void setUId(Long UId) {
		this.UId = UId;
	}

}