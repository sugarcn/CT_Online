package org.ctonline.po.order;

import java.io.Serializable;

import org.ctonline.po.pay.CtPayInterface;
import org.ctonline.po.user.CtUser;

public class CtOrderInfo
  implements Serializable
{
  private Long orderId;
  private String orderSn;
  private String UId;
  private String orderStatus;
  private String consignee;
  private Integer country;
  private Integer province;
  private Integer city;
  private Integer district;
  private String address;
  private String zipcode;
  private String tel;
  private String shippingType;
  private String invoiceType;
  private String invoice;
  private String pay;
  private String couponId;
  private String freight;
  private String total;
  private String discount;
  private String dsc;
  private String orderTime;
  private String isOkTime;
  private Byte exId;
  private String exNo;
  private String deliverTime;
  private String evaDesc;
  private String evaTime;
  private Long evaId;
  private CtUser ctUser;
  private Double retNumber;
  private String retTime;
  private String retStatus;
  private String ware;
  private String wareTime;
  private String manage;
  private String manageTime;
  private String finance;
  private String financeTime;
  private Short SId;
  private String orderIp;
  private Boolean shoppingTypeCollect;
  private String shopIdName;
  private String payPrice;
  private String isRefinfo;
  private String stockingTime;
  
  private String urgentOrder;
  
  private String xianPayDate;
  private String xiaPayDate;
  
  private String isRefStock;
  
  private String allRefCount;
  
  private CtPayInterface payInterface;
  
  
  public CtPayInterface getPayInterface() {
	return payInterface;
}

public void setPayInterface(CtPayInterface payInterface) {
	this.payInterface = payInterface;
}

public String getAllRefCount() {
	return allRefCount;
}

public void setAllRefCount(String allRefCount) {
	this.allRefCount = allRefCount;
}

public String getIsRefStock() {
	return isRefStock;
}

public void setIsRefStock(String isRefStock) {
	this.isRefStock = isRefStock;
}

public String getXianPayDate() {
	return xianPayDate;
}

public void setXianPayDate(String xianPayDate) {
	this.xianPayDate = xianPayDate;
}

public String getXiaPayDate() {
	return xiaPayDate;
}

public void setXiaPayDate(String xiaPayDate) {
	this.xiaPayDate = xiaPayDate;
}

public String getUrgentOrder() {
	return urgentOrder;
}

public void setUrgentOrder(String urgentOrder) {
	this.urgentOrder = urgentOrder;
}

public String getStockingTime() {
	return stockingTime;
}

public void setStockingTime(String stockingTime) {
	this.stockingTime = stockingTime;
}

private String kfDsc;
  
  
  public String getKfDsc() {
	return kfDsc;
}

public void setKfDsc(String kfDsc) {
	this.kfDsc = kfDsc;
}

public String getIsRefinfo() {
	return isRefinfo;
}

public void setIsRefinfo(String isRefinfo) {
	this.isRefinfo = isRefinfo;
}

public Boolean getShoppingTypeCollect()
  {
    return this.shoppingTypeCollect;
  }
  
  public void setShoppingTypeCollect(Boolean shoppingTypeCollect)
  {
    this.shoppingTypeCollect = shoppingTypeCollect;
  }
  
  public String getShopIdName()
  {
    return this.shopIdName;
  }
  
  public void setShopIdName(String shopIdName)
  {
    this.shopIdName = shopIdName;
  }
  
  public Short getSId()
  {
    return this.SId;
  }
  
  public void setSId(Short sId)
  {
    this.SId = sId;
  }
  
  public String getOrderIp()
  {
    return this.orderIp;
  }
  
  public void setOrderIp(String orderIp)
  {
    this.orderIp = orderIp;
  }
  
  public String getIsOkTime()
  {
    return this.isOkTime;
  }
  
  public void setIsOkTime(String isOkTime)
  {
    this.isOkTime = isOkTime;
  }
  
  public String getWare()
  {
    return this.ware;
  }
  
  public void setWare(String ware)
  {
    this.ware = ware;
  }
  
  public String getWareTime()
  {
    return this.wareTime;
  }
  
  public void setWareTime(String wareTime)
  {
    this.wareTime = wareTime;
  }
  
  public String getManage()
  {
    return this.manage;
  }
  
  public void setManage(String manage)
  {
    this.manage = manage;
  }
  
  public String getManageTime()
  {
    return this.manageTime;
  }
  
  public void setManageTime(String manageTime)
  {
    this.manageTime = manageTime;
  }
  
  public String getFinance()
  {
    return this.finance;
  }
  
  public void setFinance(String finance)
  {
    this.finance = finance;
  }
  
  public String getFinanceTime()
  {
    return this.financeTime;
  }
  
  public void setFinanceTime(String financeTime)
  {
    this.financeTime = financeTime;
  }
  
  public CtUser getCtUser()
  {
    return this.ctUser;
  }
  
  public void setCtUser(CtUser ctUser)
  {
    this.ctUser = ctUser;
  }
  
  public Double getRetNumber()
  {
    return this.retNumber;
  }
  
  public void setRetNumber(Double retNumber)
  {
    this.retNumber = retNumber;
  }
  
  public String getRetTime()
  {
    return this.retTime;
  }
  
  public void setRetTime(String retTime)
  {
    this.retTime = retTime;
  }
  
  public String getRetStatus()
  {
    return this.retStatus;
  }
  
  public void setRetStatus(String retStatus)
  {
    this.retStatus = retStatus;
  }
  
  public Long getEvaId()
  {
    return this.evaId;
  }
  
  public void setEvaId(Long evaId)
  {
    this.evaId = evaId;
  }
  
  public String getEvaDesc()
  {
    return this.evaDesc;
  }
  
  public void setEvaDesc(String evaDesc)
  {
    this.evaDesc = evaDesc;
  }
  
  public String getEvaTime()
  {
    return this.evaTime;
  }
  
  public void setEvaTime(String evaTime)
  {
    this.evaTime = evaTime;
  }
  
  public String getDeliverTime()
  {
    return this.deliverTime;
  }
  
  public void setDeliverTime(String deliverTime)
  {
    this.deliverTime = deliverTime;
  }
  
  public String getPayPrice()
  {
    return this.payPrice;
  }
  
  public void setPayPrice(String payPrice)
  {
    this.payPrice = payPrice;
  }
  
  public Byte getExId()
  {
    return this.exId;
  }
  
  public void setExId(Byte exId)
  {
    this.exId = exId;
  }
  
  public String getExNo()
  {
    return this.exNo;
  }
  
  public void setExNo(String exNo)
  {
    this.exNo = exNo;
  }
  
  public CtOrderInfo() {}
  
  public CtOrderInfo(String orderSn, String UId, String orderStatus, String consignee)
  {
    this.orderSn = orderSn;
    this.UId = UId;
    this.orderStatus = orderStatus;
    this.consignee = consignee;
  }
  
  public CtOrderInfo(String orderSn, String UId, String orderStatus, String consignee, Integer country, Integer province, Integer city, Integer district, String address, String zipcode, String tel, String shippingType, String invoiceType, String invoice, String pay, String couponId, String freight, String total, String discount, String dsc, String orderTime)
  {
    this.orderSn = orderSn;
    this.UId = UId;
    this.orderStatus = orderStatus;
    this.consignee = consignee;
    this.country = country;
    this.province = province;
    this.city = city;
    this.district = district;
    this.address = address;
    this.zipcode = zipcode;
    this.tel = tel;
    this.shippingType = shippingType;
    this.invoiceType = invoiceType;
    this.invoice = invoice;
    this.pay = pay;
    this.couponId = couponId;
    this.freight = freight;
    this.total = total;
    this.discount = discount;
    this.dsc = dsc;
    this.orderTime = orderTime;
  }
  
  public Long getOrderId()
  {
    return this.orderId;
  }
  
  public void setOrderId(Long orderId)
  {
    this.orderId = orderId;
  }
  
  public String getOrderSn()
  {
    return this.orderSn;
  }
  
  public void setOrderSn(String orderSn)
  {
    this.orderSn = orderSn;
  }
  
  public String getUId()
  {
    return this.UId;
  }
  
  public void setUId(String UId)
  {
    this.UId = UId;
  }
  
  public String getOrderStatus()
  {
    return this.orderStatus;
  }
  
  public void setOrderStatus(String orderStatus)
  {
    this.orderStatus = orderStatus;
  }
  
  public String getConsignee()
  {
    return this.consignee;
  }
  
  public void setConsignee(String consignee)
  {
    this.consignee = consignee;
  }
  
  public Integer getCountry()
  {
    return this.country;
  }
  
  public void setCountry(Integer country)
  {
    this.country = country;
  }
  
  public Integer getProvince()
  {
    return this.province;
  }
  
  public void setProvince(Integer province)
  {
    this.province = province;
  }
  
  public Integer getCity()
  {
    return this.city;
  }
  
  public void setCity(Integer city)
  {
    this.city = city;
  }
  
  public Integer getDistrict()
  {
    return this.district;
  }
  
  public void setDistrict(Integer district)
  {
    this.district = district;
  }
  
  public String getAddress()
  {
    return this.address;
  }
  
  public void setAddress(String address)
  {
    this.address = address;
  }
  
  public String getZipcode()
  {
    return this.zipcode;
  }
  
  public void setZipcode(String zipcode)
  {
    this.zipcode = zipcode;
  }
  
  public String getTel()
  {
    return this.tel;
  }
  
  public void setTel(String tel)
  {
    this.tel = tel;
  }
  
  public String getShippingType()
  {
    return this.shippingType;
  }
  
  public void setShippingType(String shippingType)
  {
    this.shippingType = shippingType;
  }
  
  public String getInvoiceType()
  {
    return this.invoiceType;
  }
  
  public void setInvoiceType(String invoiceType)
  {
    this.invoiceType = invoiceType;
  }
  
  public String getInvoice()
  {
    return this.invoice;
  }
  
  public void setInvoice(String invoice)
  {
    this.invoice = invoice;
  }
  
  public String getPay()
  {
    return this.pay;
  }
  
  public void setPay(String pay)
  {
    this.pay = pay;
  }
  
  public String getCouponId()
  {
    return this.couponId;
  }
  
  public void setCouponId(String couponId)
  {
    this.couponId = couponId;
  }
  
  public String getFreight()
  {
    return this.freight;
  }
  
  public void setFreight(String freight)
  {
    this.freight = freight;
  }
  
  public String getTotal()
  {
    return this.total;
  }
  
  public void setTotal(String total)
  {
    this.total = total;
  }
  
  public String getDiscount()
  {
    return this.discount;
  }
  
  public void setDiscount(String discount)
  {
    this.discount = discount;
  }
  
  public String getDsc()
  {
    return this.dsc;
  }
  
  public void setDsc(String dsc)
  {
    this.dsc = dsc;
  }
  
  public String getOrderTime()
  {
    return this.orderTime;
  }
  
  public void setOrderTime(String orderTime)
  {
    this.orderTime = orderTime;
  }
}
