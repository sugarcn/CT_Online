package org.ctonline.po.order;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CtPay
  implements Serializable
{
  private Long payId;
  private Long orderId;
  private Byte bankId;
  private String CName;
  private String CTel;
  private String CAName;
  private Double payAmount;
  private String payDate;
  private String payDesc;
  private String payFile;
  
  private Long orid;
  
  private CtOrderInfo orderInfo = new CtOrderInfo();
  
  
  public CtOrderInfo getOrderInfo() {
	return orderInfo;
}

public void setOrderInfo(CtOrderInfo orderInfo) {
	this.orderInfo = orderInfo;
}

public Long getOrid() {
	return orid;
}

public void setOrid(Long orid) {
	this.orid = orid;
}

private CtBank bank = new CtBank();
  
  

public CtBank getBank() {
	return bank;
}

public void setBank(CtBank bank) {
	this.bank = bank;
}

public CtPay() {}
  
  public CtPay(Long orderId, Byte bankId)
  {
    this.orderId = orderId;
    this.bankId = bankId;
  }
  
  public CtPay(Long orderId, Byte bankId, String CName, String CTel, String CAName, Double payAmount, String payDate, String payDesc, String payFile)
  {
    this.orderId = orderId;
    this.bankId = bankId;
    this.CName = CName;
    this.CTel = CTel;
    this.CAName = CAName;
    this.payAmount = payAmount;
    this.payDate = payDate;
    this.payDesc = payDesc;
    this.payFile = payFile;
  }
  
  public Long getPayId()
  {
    return this.payId;
  }
  
  public void setPayId(Long payId)
  {
    this.payId = payId;
  }
  
  public Long getOrderId()
  {
    return this.orderId;
  }
  
  public void setOrderId(Long orderId)
  {
    this.orderId = orderId;
  }
  
  public Byte getBankId()
  {
    return this.bankId;
  }
  
  public void setBankId(Byte bankId)
  {
    this.bankId = bankId;
  }
  
  public String getCName()
  {
    return this.CName;
  }
  
  public void setCName(String CName)
  {
    this.CName = CName;
  }
  
  public String getCTel()
  {
    return this.CTel;
  }
  
  public void setCTel(String CTel)
  {
    this.CTel = CTel;
  }
  
  public String getCAName()
  {
    return this.CAName;
  }
  
  public void setCAName(String CAName)
  {
    this.CAName = CAName;
  }
  
  public Double getPayAmount()
  {
    return this.payAmount;
  }
  
  public void setPayAmount(Double payAmount)
  {
    this.payAmount = payAmount;
  }
  
  public String getPayDate()
  {
    return this.payDate;
  }
  
  public void setPayDate(String payDate)
  {
    this.payDate = payDate;
  }
  
  public String getPayDesc()
  {
    return this.payDesc;
  }
  
  public void setPayDesc(String payDesc)
  {
    this.payDesc = payDesc;
  }
  
  public String getPayFile()
  {
    return this.payFile;
  }
  
  public void setPayFile(String payFile)
  {
    this.payFile = payFile;
  }
}
