package org.ctonline.po.order;

import java.io.Serializable;

public class CtShop
  implements Serializable
{
  private Short SId;
  private String shopName;
  private String shopAdd;
  private String shopTel;
  private String shopDesc;
  
  public CtShop() {}
  
  public CtShop(String shopName, String shopAdd, String shopTel, String shopDesc)
  {
    this.shopName = shopName;
    this.shopAdd = shopAdd;
    this.shopTel = shopTel;
    this.shopDesc = shopDesc;
  }
  
  public Short getSId()
  {
    return this.SId;
  }
  
  public void setSId(Short SId)
  {
    this.SId = SId;
  }
  
  public String getShopName()
  {
    return this.shopName;
  }
  
  public void setShopName(String shopName)
  {
    this.shopName = shopName;
  }
  
  public String getShopAdd()
  {
    return this.shopAdd;
  }
  
  public void setShopAdd(String shopAdd)
  {
    this.shopAdd = shopAdd;
  }
  
  public String getShopTel()
  {
    return this.shopTel;
  }
  
  public void setShopTel(String shopTel)
  {
    this.shopTel = shopTel;
  }
  
  public String getShopDesc()
  {
    return this.shopDesc;
  }
  
  public void setShopDesc(String shopDesc)
  {
    this.shopDesc = shopDesc;
  }
}
