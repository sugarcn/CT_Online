package org.ctonline.po.help;

import java.io.Serializable;

public class CtHelp
  implements Serializable
{
  private Integer HId;
  private Integer HTypeId;
  private String HTitle;
  private String HDesc;
  private CtHelpType helptype;
  
  public CtHelpType getHelptype()
  {
    return this.helptype;
  }
  
  public void setHelptype(CtHelpType helptype)
  {
    this.helptype = helptype;
  }
  
  public CtHelp() {}
  
  public CtHelp(Integer HTypeId, String HTitle, String HDesc)
  {
    this.HTypeId = HTypeId;
    this.HTitle = HTitle;
    this.HDesc = HDesc;
  }
  
  public CtHelp(Integer HTypeId, String HTitle, String HDesc, CtHelpType helptype)
  {
    this.helptype = helptype;
    this.HTypeId = HTypeId;
    this.HTitle = HTitle;
    this.HDesc = HDesc;
  }
  
  public Integer getHId()
  {
    return this.HId;
  }
  
  public void setHId(Integer hId)
  {
    this.HId = hId;
  }
  
  public Integer getHTypeId()
  {
    return this.HTypeId;
  }
  
  public void setHTypeId(Integer hTypeId)
  {
    this.HTypeId = hTypeId;
  }
  
  public String getHTitle()
  {
    return this.HTitle;
  }
  
  public void setHTitle(String hTitle)
  {
    this.HTitle = hTitle;
  }
  
  public String getHDesc()
  {
    return this.HDesc;
  }
  
  public void setHDesc(String hDesc)
  {
    this.HDesc = hDesc;
  }
}
