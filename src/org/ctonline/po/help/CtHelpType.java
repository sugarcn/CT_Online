package org.ctonline.po.help;

import java.io.Serializable;

public class CtHelpType
  implements Serializable
{
  private Integer HTypeId;
  private String HTypeName;
  
  public CtHelpType() {}
  
  public CtHelpType(String HTypeName)
  {
    this.HTypeName = HTypeName;
  }
  
  public Integer getHTypeId()
  {
    return this.HTypeId;
  }
  
  public void setHTypeId(Integer HTypeId)
  {
    this.HTypeId = HTypeId;
  }
  
  public String getHTypeName()
  {
    return this.HTypeName;
  }
  
  public void setHTypeName(String HTypeName)
  {
    this.HTypeName = HTypeName;
  }
}
