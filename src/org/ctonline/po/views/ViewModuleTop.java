package org.ctonline.po.views;

/**
 * ViewModuleTopId entity. @author MyEclipse Persistence Tools
 */

public class ViewModuleTop implements java.io.Serializable {

	// Fields

	private String moduleName;
	private String imgUrl;
	private Integer moduleId;
	private Integer roleId;

	// Constructors

	/** default constructor */
	public ViewModuleTop() {
	}

	/** minimal constructor */
	public ViewModuleTop(String moduleName, Integer moduleId) {
		this.moduleName = moduleName;
		this.moduleId = moduleId;
	}

	/** full constructor */
	public ViewModuleTop(String moduleName, String imgUrl, Integer moduleId,
			Integer roleId) {
		this.moduleName = moduleName;
		this.imgUrl = imgUrl;
		this.moduleId = moduleId;
		this.roleId = roleId;
	}

	// Property accessors

	public String getModuleName() {
		return this.moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getImgUrl() {
		return this.imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public Integer getModuleId() {
		return this.moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

}