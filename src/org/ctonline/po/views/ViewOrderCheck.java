package org.ctonline.po.views;

import java.io.Serializable;
import java.math.BigDecimal;

public class ViewOrderCheck
  implements Serializable
{
  private Long cartId;
  private String UId;
  private String GId;
  private String PUrl;
  private String GName;
  private BigDecimal promotePrice;
  private BigDecimal shopPrice;
  private String GUnit;
  private String pack;
  private String pack1;
  private BigDecimal pack1Num;
  private String pack2;
  private BigDecimal pack2Num;
  private String pack3;
  private BigDecimal pack3Num;
  private String pack4;
  private BigDecimal pack4Num;
  private String GNumber;
  private String GPrice;
  
  public ViewOrderCheck() {}
  
  public ViewOrderCheck(Long cartId, String UId, String GId, String GNumber, String GPrice)
  {
    this.cartId = cartId;
    this.UId = UId;
    this.GId = GId;
    this.GNumber = GNumber;
    this.GPrice = GPrice;
  }
  
  public ViewOrderCheck(Long cartId, String UId, String GId, String PUrl, String GName, BigDecimal promotePrice, BigDecimal shopPrice, String GUnit, String pack, String pack1, BigDecimal pack1Num, String pack2, BigDecimal pack2Num, String pack3, BigDecimal pack3Num, String pack4, BigDecimal pack4Num, String GNumber, String GPrice)
  {
    this.cartId = cartId;
    this.UId = UId;
    this.GId = GId;
    this.PUrl = PUrl;
    this.GName = GName;
    this.promotePrice = promotePrice;
    this.shopPrice = shopPrice;
    this.GUnit = GUnit;
    this.pack = pack;
    this.pack1 = pack1;
    this.pack1Num = pack1Num;
    this.pack2 = pack2;
    this.pack2Num = pack2Num;
    this.pack3 = pack3;
    this.pack3Num = pack3Num;
    this.pack4 = pack4;
    this.pack4Num = pack4Num;
    this.GNumber = GNumber;
    this.GPrice = GPrice;
  }
  
  public Long getCartId()
  {
    return this.cartId;
  }
  
  public void setCartId(Long cartId)
  {
    this.cartId = cartId;
  }
  
  public String getUId()
  {
    return this.UId;
  }
  
  public void setUId(String UId)
  {
    this.UId = UId;
  }
  
  public String getGId()
  {
    return this.GId;
  }
  
  public void setGId(String GId)
  {
    this.GId = GId;
  }
  
  public String getPUrl()
  {
    return this.PUrl;
  }
  
  public void setPUrl(String PUrl)
  {
    this.PUrl = PUrl;
  }
  
  public String getGName()
  {
    return this.GName;
  }
  
  public void setGName(String GName)
  {
    this.GName = GName;
  }
  
  public BigDecimal getPromotePrice()
  {
    return this.promotePrice;
  }
  
  public void setPromotePrice(BigDecimal promotePrice)
  {
    this.promotePrice = promotePrice;
  }
  
  public BigDecimal getShopPrice()
  {
    return this.shopPrice;
  }
  
  public void setShopPrice(BigDecimal shopPrice)
  {
    this.shopPrice = shopPrice;
  }
  
  public String getGUnit()
  {
    return this.GUnit;
  }
  
  public void setGUnit(String GUnit)
  {
    this.GUnit = GUnit;
  }
  
  public String getPack()
  {
    return this.pack;
  }
  
  public void setPack(String pack)
  {
    this.pack = pack;
  }
  
  public String getPack1()
  {
    return this.pack1;
  }
  
  public void setPack1(String pack1)
  {
    this.pack1 = pack1;
  }
  
  public BigDecimal getPack1Num()
  {
    return this.pack1Num;
  }
  
  public void setPack1Num(BigDecimal pack1Num)
  {
    this.pack1Num = pack1Num;
  }
  
  public String getPack2()
  {
    return this.pack2;
  }
  
  public void setPack2(String pack2)
  {
    this.pack2 = pack2;
  }
  
  public BigDecimal getPack2Num()
  {
    return this.pack2Num;
  }
  
  public void setPack2Num(BigDecimal pack2Num)
  {
    this.pack2Num = pack2Num;
  }
  
  public String getPack3()
  {
    return this.pack3;
  }
  
  public void setPack3(String pack3)
  {
    this.pack3 = pack3;
  }
  
  public BigDecimal getPack3Num()
  {
    return this.pack3Num;
  }
  
  public void setPack3Num(BigDecimal pack3Num)
  {
    this.pack3Num = pack3Num;
  }
  
  public String getPack4()
  {
    return this.pack4;
  }
  
  public void setPack4(String pack4)
  {
    this.pack4 = pack4;
  }
  
  public BigDecimal getPack4Num()
  {
    return this.pack4Num;
  }
  
  public void setPack4Num(BigDecimal pack4Num)
  {
    this.pack4Num = pack4Num;
  }
  
  public String getGNumber()
  {
    return this.GNumber;
  }
  
  public void setGNumber(String GNumber)
  {
    this.GNumber = GNumber;
  }
  
  public String getGPrice()
  {
    return this.GPrice;
  }
  
  public void setGPrice(String GPrice)
  {
    this.GPrice = GPrice;
  }
  
  public boolean equals(Object other)
  {
    if (this == other) {
      return true;
    }
    if (other == null) {
      return false;
    }
    if (!(other instanceof ViewOrderCheck)) {
      return false;
    }
    ViewOrderCheck castOther = (ViewOrderCheck)other;
    if (((getCartId() == castOther.getCartId()) || (
      (getCartId() != null) && (castOther.getCartId() != null) && 
      (getCartId().equals(castOther.getCartId())))) && (
      (getUId() == castOther.getUId()) || ((getUId() != null) && 
      (castOther.getUId() != null) && (getUId().equals(
      castOther.getUId()))))) {
      if ((getGId() == castOther.getGId()) || ((getGId() != null) && 
        (castOther.getGId() != null) && (getGId().equals(
        castOther.getGId())))) {
        if (((getPUrl() == castOther.getPUrl()) || ((getPUrl() != null) && 
          (castOther.getPUrl() != null) && 
          (getPUrl().equals(castOther.getPUrl())))) && 
          ((getGName() == castOther.getGName()) || (
          (getGName() != null) && (castOther.getGName() != null) && 
          (getGName().equals(castOther.getGName())))) && 
          ((getPromotePrice() == castOther.getPromotePrice()) || (
          (getPromotePrice() != null) && 
          (castOther.getPromotePrice() != null) && 
          (getPromotePrice().equals(castOther.getPromotePrice())))) && 
          ((getShopPrice() == castOther.getShopPrice()) || (
          (getShopPrice() != null) && 
          (castOther.getShopPrice() != null) && 
          (getShopPrice().equals(castOther.getShopPrice())))) && 
          ((getGUnit() == castOther.getGUnit()) || (
          (getGUnit() != null) && (castOther.getGUnit() != null) && 
          (getGUnit().equals(castOther.getGUnit())))) && 
          ((getPack() == castOther.getPack()) || ((getPack() != null) && 
          (castOther.getPack() != null) && 
          (getPack().equals(castOther.getPack())))) && 
          ((getPack1() == castOther.getPack1()) || (
          (getPack1() != null) && (castOther.getPack1() != null) && 
          (getPack1().equals(castOther.getPack1())))) && 
          ((getPack1Num() == castOther.getPack1Num()) || (
          (getPack1Num() != null) && 
          (castOther.getPack1Num() != null) && 
          (getPack1Num().equals(castOther.getPack1Num())))) && 
          ((getPack2() == castOther.getPack2()) || (
          (getPack2() != null) && (castOther.getPack2() != null) && 
          (getPack2().equals(castOther.getPack2())))) && 
          ((getPack2Num() == castOther.getPack2Num()) || (
          (getPack2Num() != null) && 
          (castOther.getPack2Num() != null) && 
          (getPack2Num().equals(castOther.getPack2Num())))) && 
          ((getPack3() == castOther.getPack3()) || (
          (getPack3() != null) && (castOther.getPack3() != null) && 
          (getPack3().equals(castOther.getPack3())))) && 
          ((getPack3Num() == castOther.getPack3Num()) || (
          (getPack3Num() != null) && 
          (castOther.getPack3Num() != null) && 
          (getPack3Num().equals(castOther.getPack3Num())))) && 
          ((getPack4() == castOther.getPack4()) || (
          (getPack4() != null) && (castOther.getPack4() != null) && 
          (getPack4().equals(castOther.getPack4())))) && 
          ((getPack4Num() == castOther.getPack4Num()) || (
          (getPack4Num() != null) && 
          (castOther.getPack4Num() != null) && 
          (getPack4Num().equals(castOther.getPack4Num())))) && 
          ((getGNumber() == castOther.getGNumber()) || (
          (getGNumber() != null) && (castOther.getGNumber() != null) && 
          (getGNumber().equals(castOther.getGNumber())))) && (
          (getGPrice() == castOther.getGPrice()) || (
          (getGPrice() != null) && (castOther.getGPrice() != null) && 
          (getGPrice().equals(castOther.getGPrice()))))) {
          return true;
        }
      }
    }
    return 
    
      false;
  }
  
  public int hashCode()
  {
    int result = 17;
    
    result = 37 * result + (
      getCartId() == null ? 0 : getCartId().hashCode());
    result = 37 * result + (
      getUId() == null ? 0 : getUId().hashCode());
    result = 37 * result + (
      getGId() == null ? 0 : getGId().hashCode());
    result = 37 * result + (
      getPUrl() == null ? 0 : getPUrl().hashCode());
    result = 37 * result + (
      getGName() == null ? 0 : getGName().hashCode());
    result = 37 * 
      result + (
      getPromotePrice() == null ? 0 : getPromotePrice()
      .hashCode());
    result = 37 * result + (
      getShopPrice() == null ? 0 : getShopPrice().hashCode());
    result = 37 * result + (
      getGUnit() == null ? 0 : getGUnit().hashCode());
    result = 37 * result + (
      getPack() == null ? 0 : getPack().hashCode());
    result = 37 * result + (
      getPack1() == null ? 0 : getPack1().hashCode());
    result = 37 * result + (
      getPack1Num() == null ? 0 : getPack1Num().hashCode());
    result = 37 * result + (
      getPack2() == null ? 0 : getPack2().hashCode());
    result = 37 * result + (
      getPack2Num() == null ? 0 : getPack2Num().hashCode());
    result = 37 * result + (
      getPack3() == null ? 0 : getPack3().hashCode());
    result = 37 * result + (
      getPack3Num() == null ? 0 : getPack3Num().hashCode());
    result = 37 * result + (
      getPack4() == null ? 0 : getPack4().hashCode());
    result = 37 * result + (
      getPack4Num() == null ? 0 : getPack4Num().hashCode());
    result = 37 * result + (
      getGNumber() == null ? 0 : getGNumber().hashCode());
    result = 37 * result + (
      getGPrice() == null ? 0 : getGPrice().hashCode());
    return result;
  }
}
