package org.ctonline.po.views;

public class GoodsDetailView {
	private Long GId;
	private String GName;
	private String GSn;
	private Double marketPrice;
	private Double shopPrice;
	private Double promotePrice;
	private String isOnSale;
	private String GKeywords;
	private Long tokenCredit;
	private Long giveCredit;
	private String GUnit;
	private String pack1;
	private Integer pack1Num;
	private String pack2;
	private Integer pack2Num;
	private String pack3;
	private Integer pack3Num;
	private String pack4;
	private Integer pack4Num;
	private String bname;
	private String cname;
	public Long getGId() {
		return GId;
	}
	public void setGId(Long gId) {
		GId = gId;
	}
	public String getGName() {
		return GName;
	}
	public void setGName(String gName) {
		GName = gName;
	}
	public String getGSn() {
		return GSn;
	}
	public void setGSn(String gSn) {
		GSn = gSn;
	}
	public Double getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(Double marketPrice) {
		this.marketPrice = marketPrice;
	}
	public Double getShopPrice() {
		return shopPrice;
	}
	public void setShopPrice(Double shopPrice) {
		this.shopPrice = shopPrice;
	}
	public Double getPromotePrice() {
		return promotePrice;
	}
	public void setPromotePrice(Double promotePrice) {
		this.promotePrice = promotePrice;
	}
	public String getIsOnSale() {
		return isOnSale;
	}
	public void setIsOnSale(String isOnSale) {
		this.isOnSale = isOnSale;
	}
	public String getGKeywords() {
		return GKeywords;
	}
	public void setGKeywords(String gKeywords) {
		GKeywords = gKeywords;
	}
	public Long getTokenCredit() {
		return tokenCredit;
	}
	public void setTokenCredit(Long tokenCredit) {
		this.tokenCredit = tokenCredit;
	}
	public Long getGiveCredit() {
		return giveCredit;
	}
	public void setGiveCredit(Long giveCredit) {
		this.giveCredit = giveCredit;
	}
	public String getGUnit() {
		return GUnit;
	}
	public void setGUnit(String gUnit) {
		GUnit = gUnit;
	}
	public String getPack1() {
		return pack1;
	}
	public void setPack1(String pack1) {
		this.pack1 = pack1;
	}
	public Integer getPack1Num() {
		return pack1Num;
	}
	public void setPack1Num(Integer pack1Num) {
		this.pack1Num = pack1Num;
	}
	public String getPack2() {
		return pack2;
	}
	public void setPack2(String pack2) {
		this.pack2 = pack2;
	}
	public Integer getPack2Num() {
		return pack2Num;
	}
	public void setPack2Num(Integer pack2Num) {
		this.pack2Num = pack2Num;
	}
	public String getPack3() {
		return pack3;
	}
	public void setPack3(String pack3) {
		this.pack3 = pack3;
	}
	public Integer getPack3Num() {
		return pack3Num;
	}
	public void setPack3Num(Integer pack3Num) {
		this.pack3Num = pack3Num;
	}
	public String getPack4() {
		return pack4;
	}
	public void setPack4(String pack4) {
		this.pack4 = pack4;
	}
	public Integer getPack4Num() {
		return pack4Num;
	}
	public void setPack4Num(Integer pack4Num) {
		this.pack4Num = pack4Num;
	}
	public String getBname() {
		return bname;
	}
	public void setBname(String bname) {
		this.bname = bname;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	
}
