package org.ctonline.po.views;

public class GoodsNumView {
	private Long depotid;
	private Long goodsnum;
	private String depotname;
	public Long getDepotid() {
		return depotid;
	}
	public void setDepotid(Long depotid) {
		this.depotid = depotid;
	}
	public Long getGoodsnum() {
		return goodsnum;
	}
	public void setGoodsnum(Long goodsnum) {
		this.goodsnum = goodsnum;
	}
	public String getDepotname() {
		return depotname;
	}
	public void setDepotname(String depotname) {
		this.depotname = depotname;
	}
	
	
}
