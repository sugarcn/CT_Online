package org.ctonline.po.views;

import java.math.BigDecimal;

/**
 * ViewGoodsResourceId entity. @author MyEclipse Persistence Tools
 */

public class ViewGoodsResource implements java.io.Serializable {

	// Fields

	private Long GId;
	private BigDecimal RId;
	private String RName;
	private String RUrl;
	private BigDecimal RTId;

	// Constructors

	/** default constructor */
	public ViewGoodsResource() {
	}

	/** minimal constructor */
	public ViewGoodsResource(Long GId, BigDecimal RId, String RName) {
		this.GId = GId;
		this.RId = RId;
		this.RName = RName;
	}

	/** full constructor */
	public ViewGoodsResource(Long GId, BigDecimal RId, String RName,
			String RUrl, BigDecimal RTId) {
		this.GId = GId;
		this.RId = RId;
		this.RName = RName;
		this.RUrl = RUrl;
		this.RTId = RTId;
	}

	// Property accessors

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public BigDecimal getRId() {
		return this.RId;
	}

	public void setRId(BigDecimal RId) {
		this.RId = RId;
	}

	public String getRName() {
		return this.RName;
	}

	public void setRName(String RName) {
		this.RName = RName;
	}

	public String getRUrl() {
		return this.RUrl;
	}

	public void setRUrl(String RUrl) {
		this.RUrl = RUrl;
	}

	public BigDecimal getRTId() {
		return this.RTId;
	}

	public void setRTId(BigDecimal RTId) {
		this.RTId = RTId;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ViewGoodsResource))
			return false;
		ViewGoodsResource castOther = (ViewGoodsResource) other;

		return ((this.getGId() == castOther.getGId()) || (this.getGId() != null
				&& castOther.getGId() != null && this.getGId().equals(
				castOther.getGId())))
				&& ((this.getRId() == castOther.getRId()) || (this.getRId() != null
						&& castOther.getRId() != null && this.getRId().equals(
						castOther.getRId())))
				&& ((this.getRName() == castOther.getRName()) || (this
						.getRName() != null && castOther.getRName() != null && this
						.getRName().equals(castOther.getRName())))
				&& ((this.getRUrl() == castOther.getRUrl()) || (this.getRUrl() != null
						&& castOther.getRUrl() != null && this.getRUrl()
						.equals(castOther.getRUrl())))
				&& ((this.getRTId() == castOther.getRTId()) || (this.getRTId() != null
						&& castOther.getRTId() != null && this.getRTId()
						.equals(castOther.getRTId())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getGId() == null ? 0 : this.getGId().hashCode());
		result = 37 * result
				+ (getRId() == null ? 0 : this.getRId().hashCode());
		result = 37 * result
				+ (getRName() == null ? 0 : this.getRName().hashCode());
		result = 37 * result
				+ (getRUrl() == null ? 0 : this.getRUrl().hashCode());
		result = 37 * result
				+ (getRTId() == null ? 0 : this.getRTId().hashCode());
		return result;
	}

}