package org.ctonline.po.goods;

import java.util.HashSet;
import java.util.Set;

/**
 * CtBom entity. @author MyEclipse Persistence Tools
 */

public class CtBom implements java.io.Serializable {

	// Fields

	private Integer bomId;
	private String bomTitle;
	private String bomDesc;
	private String bomTime;
	private Set ctBomGoodses = new HashSet(0);
	private Long UId;
	private String isHot;

	// Constructors

	/** default constructor */
	public CtBom() {
	}

	/** minimal constructor */
	public CtBom(String bomTitle) {
		this.bomTitle = bomTitle;
	}

	/** full constructor */
	public CtBom(String bomTitle, String bomDesc, String bomTime,
			Set ctBomGoodses) {
		this.bomTitle = bomTitle;
		this.bomDesc = bomDesc;
		this.bomTime = bomTime;
		this.ctBomGoodses = ctBomGoodses;
	}

	// Property accessors

	public Integer getBomId() {
		return this.bomId;
	}

	public void setBomId(Integer bomId) {
		this.bomId = bomId;
	}

	public String getBomTitle() {
		return this.bomTitle;
	}

	public void setBomTitle(String bomTitle) {
		this.bomTitle = bomTitle;
	}

	public String getBomDesc() {
		return this.bomDesc;
	}

	public void setBomDesc(String bomDesc) {
		this.bomDesc = bomDesc;
	}

	public String getBomTime() {
		return this.bomTime;
	}

	public void setBomTime(String bomTime) {
		this.bomTime = bomTime;
	}

	public Set getCtBomGoodses() {
		return this.ctBomGoodses;
	}

	public void setCtBomGoodses(Set ctBomGoodses) {
		this.ctBomGoodses = ctBomGoodses;
	}

	public Long getUId() {
		return UId;
	}

	public void setUId(Long uId) {
		UId = uId;
	}

	public String getIsHot() {
		return isHot;
	}

	public void setIsHot(String isHot) {
		this.isHot = isHot;
	}

}