package org.ctonline.po.goods;

/**
 * CtGoodsNum entity. @author MyEclipse Persistence Tools
 */

public class CtGoodsNum implements java.io.Serializable {

	// Fields

	private Long id;
	private Long GId;
	private Integer depotId;
	private Integer GNum;

	// Constructors

	/** default constructor */
	public CtGoodsNum() {
	}

	/** full constructor */
	public CtGoodsNum(Long GId, Integer depotId, Integer GNum) {
		this.GId = GId;
		this.depotId = depotId;
		this.GNum = GNum;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public Integer getDepotId() {
		return this.depotId;
	}

	public void setDepotId(Integer depotId) {
		this.depotId = depotId;
	}

	public Integer getGNum() {
		return this.GNum;
	}

	public void setGNum(Integer GNum) {
		this.GNum = GNum;
	}

}