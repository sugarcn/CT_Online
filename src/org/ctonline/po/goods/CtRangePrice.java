package org.ctonline.po.goods;

import java.io.Serializable;

public class CtRangePrice
  implements Serializable
{
  private Long RPid;
  private Long GId;
  private Integer simSNum;
  private Integer simENum;
  private Double simRPrice;
  private Integer simIncrease;
  private Integer parSNum;
  private Integer parENum;
  private Double parRPrice;
  private Integer parIncrease;
  private String insTime;
  private String addStr;
  private String addStrForPar;
  
  public String getInsTime()
  {
    return this.insTime;
  }
  
  public void setInsTime(String insTime)
  {
    this.insTime = insTime;
  }
  
  public String getAddStrForPar()
  {
    return this.addStrForPar;
  }
  
  public void setAddStrForPar(String addStrForPar)
  {
    this.addStrForPar = addStrForPar;
  }
  
  public String getAddStr()
  {
    return this.addStr;
  }
  
  public void setAddStr(String addStr)
  {
    this.addStr = addStr;
  }
  
  public CtRangePrice() {}
  
  public CtRangePrice(Long GId, Integer simSNum, Integer simENum, Double simRPrice, Integer simIncrease, Integer parSNum, Integer parENum, Double parRPrice, Integer parIncrease)
  {
    this.GId = GId;
    this.simSNum = simSNum;
    this.simENum = simENum;
    this.simRPrice = simRPrice;
    this.simIncrease = simIncrease;
    this.parSNum = parSNum;
    this.parENum = parENum;
    this.parRPrice = parRPrice;
    this.parIncrease = parIncrease;
  }
  
  public Long getRPid()
  {
    return this.RPid;
  }
  
  public void setRPid(Long RPid)
  {
    this.RPid = RPid;
  }
  
  public Long getGId()
  {
    return this.GId;
  }
  
  public void setGId(Long GId)
  {
    this.GId = GId;
  }
  
  public Integer getSimSNum()
  {
    return this.simSNum;
  }
  
  public void setSimSNum(Integer simSNum)
  {
    this.simSNum = simSNum;
  }
  
  public Integer getSimENum()
  {
    return this.simENum;
  }
  
  public void setSimENum(Integer simENum)
  {
    this.simENum = simENum;
  }
  
  public Double getSimRPrice()
  {
    return this.simRPrice;
  }
  
  public void setSimRPrice(Double simRPrice)
  {
    this.simRPrice = simRPrice;
  }
  
  public Integer getSimIncrease()
  {
    return this.simIncrease;
  }
  
  public void setSimIncrease(Integer simIncrease)
  {
    this.simIncrease = simIncrease;
  }
  
  public Integer getParSNum()
  {
    return this.parSNum;
  }
  
  public void setParSNum(Integer parSNum)
  {
    this.parSNum = parSNum;
  }
  
  public Integer getParENum()
  {
    return this.parENum;
  }
  
  public void setParENum(Integer parENum)
  {
    this.parENum = parENum;
  }
  
  public Double getParRPrice()
  {
    return this.parRPrice;
  }
  
  public void setParRPrice(Double parRPrice)
  {
    this.parRPrice = parRPrice;
  }
  
  public Integer getParIncrease()
  {
    return this.parIncrease;
  }
  
  public void setParIncrease(Integer parIncrease)
  {
    this.parIncrease = parIncrease;
  }
}
