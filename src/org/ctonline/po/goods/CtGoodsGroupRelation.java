package org.ctonline.po.goods;

/**
 * CtGoodsGroupRelation entity. @author MyEclipse Persistence Tools
 */

public class CtGoodsGroupRelation implements java.io.Serializable {

	// Fields

	private Long GId;
	private Long GGId;

	// Constructors

	/** default constructor */
	public CtGoodsGroupRelation() {
	}

	/** full constructor */
	public CtGoodsGroupRelation(Long GGId) {
		this.GGId = GGId;
	}

	// Property accessors

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public Long getGGId() {
		return this.GGId;
	}

	public void setGGId(Long GGId) {
		this.GGId = GGId;
	}

}