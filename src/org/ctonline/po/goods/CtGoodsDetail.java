package org.ctonline.po.goods;

/**
 * CtGoodsDetail entity. @author MyEclipse Persistence Tools
 */

public class CtGoodsDetail implements java.io.Serializable {

	// Fields

	private Long detlId;
	private Long GId;
	private String GDetail;

	// Constructors

	/** default constructor */
	public CtGoodsDetail() {
	}

	/** minimal constructor */
	public CtGoodsDetail(Long GId) {
		this.GId = GId;
	}

	/** full constructor */
	public CtGoodsDetail(Long GId, String GDetail) {
		this.GId = GId;
		this.GDetail = GDetail;
	}

	// Property accessors

	public Long getDetlId() {
		return this.detlId;
	}

	public void setDetlId(Long detlId) {
		this.detlId = detlId;
	}

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public String getGDetail() {
		return this.GDetail;
	}

	public void setGDetail(String GDetail) {
		this.GDetail = GDetail;
	}

}