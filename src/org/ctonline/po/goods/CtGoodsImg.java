package org.ctonline.po.goods;

import java.io.Serializable;

public class CtGoodsImg
  implements Serializable
{
  private Long PId;
  private String PUrl;
  private String PName;
  private Long GId;
  private Integer sortOrder;
  private String coverImg;
  
  public CtGoodsImg() {}
  
  public CtGoodsImg(String pUrl)
  {
    this.PUrl = pUrl;
  }
  
  public CtGoodsImg(String PUrl, String PName, Long GId, Integer sortOrder, String coverImg)
  {
    this.PUrl = PUrl;
    this.PName = PName;
    this.GId = GId;
    this.sortOrder = sortOrder;
    this.coverImg = coverImg;
  }
  
  public Long getPId()
  {
    return this.PId;
  }
  
  public void setPId(Long PId)
  {
    this.PId = PId;
  }
  
  public String getPUrl()
  {
    return this.PUrl;
  }
  
  public void setPUrl(String PUrl)
  {
    this.PUrl = PUrl;
  }
  
  public String getPName()
  {
    return this.PName;
  }
  
  public void setPName(String PName)
  {
    this.PName = PName;
  }
  
  public Long getGId()
  {
    return this.GId;
  }
  
  public void setGId(Long GId)
  {
    this.GId = GId;
  }
  
  public Integer getSortOrder()
  {
    return this.sortOrder;
  }
  
  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  
  public String getCoverImg()
  {
    return this.coverImg;
  }
  
  public void setCoverImg(String coverImg)
  {
    this.coverImg = coverImg;
  }
}
