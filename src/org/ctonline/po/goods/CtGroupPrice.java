package org.ctonline.po.goods;

import java.io.Serializable;

public class CtGroupPrice
  implements Serializable
{
  private Long GPid;
  private Long GGId;
  private Integer simSNum;
  private Integer simENum;
  private Double simRPrice;
  private Integer simIncrease;
  private Integer parSNum;
  private Integer parENum;
  private Double parRPrice;
  private Integer parIncrease;
  private String insTime;
  private String addStr;
  private String addStrForPar;
  
  public String getInsTime()
  {
    return this.insTime;
  }
  
  public void setInsTime(String insTime)
  {
    this.insTime = insTime;
  }
  
  public String getAddStr()
  {
    return this.addStr;
  }
  
  public void setAddStr(String addStr)
  {
    this.addStr = addStr;
  }
  
  public String getAddStrForPar()
  {
    return this.addStrForPar;
  }
  
  public void setAddStrForPar(String addStrForPar)
  {
    this.addStrForPar = addStrForPar;
  }
  
  public CtGroupPrice() {}
  
  public CtGroupPrice(Long GGId, Integer simSNum, Integer simENum, Double simRPrice, Integer simIncrease, Integer parSNum, Integer parENum, Double parRPrice, Integer parIncrease)
  {
    this.GGId = GGId;
    this.simSNum = simSNum;
    this.simENum = simENum;
    this.simRPrice = simRPrice;
    this.simIncrease = simIncrease;
    this.parSNum = parSNum;
    this.parENum = parENum;
    this.parRPrice = parRPrice;
    this.parIncrease = parIncrease;
  }
  
  public Long getGPid()
  {
    return this.GPid;
  }
  
  public void setGPid(Long GPid)
  {
    this.GPid = GPid;
  }
  
  public Long getGGId()
  {
    return this.GGId;
  }
  
  public void setGGId(Long GGId)
  {
    this.GGId = GGId;
  }
  
  public Integer getSimSNum()
  {
    return this.simSNum;
  }
  
  public void setSimSNum(Integer simSNum)
  {
    this.simSNum = simSNum;
  }
  
  public Integer getSimENum()
  {
    return this.simENum;
  }
  
  public void setSimENum(Integer simENum)
  {
    this.simENum = simENum;
  }
  
  public Double getSimRPrice()
  {
    return this.simRPrice;
  }
  
  public void setSimRPrice(Double simRPrice)
  {
    this.simRPrice = simRPrice;
  }
  
  public Integer getSimIncrease()
  {
    return this.simIncrease;
  }
  
  public void setSimIncrease(Integer simIncrease)
  {
    this.simIncrease = simIncrease;
  }
  
  public Integer getParSNum()
  {
    return this.parSNum;
  }
  
  public void setParSNum(Integer parSNum)
  {
    this.parSNum = parSNum;
  }
  
  public Integer getParENum()
  {
    return this.parENum;
  }
  
  public void setParENum(Integer parENum)
  {
    this.parENum = parENum;
  }
  
  public Double getParRPrice()
  {
    return this.parRPrice;
  }
  
  public void setParRPrice(Double parRPrice)
  {
    this.parRPrice = parRPrice;
  }
  
  public Integer getParIncrease()
  {
    return this.parIncrease;
  }
  
  public void setParIncrease(Integer parIncrease)
  {
    this.parIncrease = parIncrease;
  }
}
