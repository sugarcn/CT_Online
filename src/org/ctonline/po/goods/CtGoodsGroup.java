package org.ctonline.po.goods;

/**
 * CtGoodsGroup entity. @author MyEclipse Persistence Tools
 */

public class CtGoodsGroup implements java.io.Serializable {

	// Fields

	private Long GGId;
	private String GGName;
	private String GGDesc;
	private String isSample;
	public String getIsSample() {
		return isSample;
	}

	public void setIsSample(String isSample) {
		this.isSample = isSample;
	}

	public String getIsPartial() {
		return isPartial;
	}

	public void setIsPartial(String isPartial) {
		this.isPartial = isPartial;
	}

	private String isPartial;
	// Constructors

	/** default constructor */
	public CtGoodsGroup() {
	}

	/** minimal constructor */
	public CtGoodsGroup(String GGName) {
		this.GGName = GGName;
	}

	/** full constructor */
	public CtGoodsGroup(String GGName, String GGDesc) {
		this.GGName = GGName;
		this.GGDesc = GGDesc;
	}

	// Property accessors

	public Long getGGId() {
		return this.GGId;
	}

	public void setGGId(Long GGId) {
		this.GGId = GGId;
	}

	public String getGGName() {
		return this.GGName;
	}

	public void setGGName(String GGName) {
		this.GGName = GGName;
	}

	public String getGGDesc() {
		return this.GGDesc;
	}

	public void setGGDesc(String GGDesc) {
		this.GGDesc = GGDesc;
	}

}