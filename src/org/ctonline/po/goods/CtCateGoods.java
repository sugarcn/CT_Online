package org.ctonline.po.goods;

import java.io.Serializable;

public class CtCateGoods
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private Long GId;
  private Long CId;
  
  public Long getGId()
  {
    return this.GId;
  }
  
  public void setGId(Long gId)
  {
    this.GId = gId;
  }
  
  public Long getCId()
  {
    return this.CId;
  }
  
  public void setCId(Long cId)
  {
    this.CId = cId;
  }
  
  public CtCateGoods(Long gId, Long cId)
  {
    this.GId = gId;
    this.CId = cId;
  }
  
  public CtCateGoods() {}
}
