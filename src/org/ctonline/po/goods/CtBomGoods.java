package org.ctonline.po.goods;

/**
 * CtBomGoods entity. @author MyEclipse Persistence Tools
 */

public class CtBomGoods implements java.io.Serializable {

	// Fields

	private Long bomGoodsId;
	private CtBom ctBom;
	private Long GId;
	private String pack;
	private Integer goodsNum;
	private CtGoods goods;
	private Integer bomId;

	// Constructors

	/** default constructor */
	public CtBomGoods() {
	}

	/** full constructor */
	public CtBomGoods(CtBom ctBom, Long GId, String pack, Integer goodsNum) {
		this.ctBom = ctBom;
		this.GId = GId;
		this.pack = pack;
		this.goodsNum = goodsNum;
	}

	// Property accessors

	public Long getBomGoodsId() {
		return this.bomGoodsId;
	}

	public void setBomGoodsId(Long bomGoodsId) {
		this.bomGoodsId = bomGoodsId;
	}

	public CtBom getCtBom() {
		return this.ctBom;
	}

	public void setCtBom(CtBom ctBom) {
		this.ctBom = ctBom;
	}

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public String getPack() {
		return this.pack;
	}

	public void setPack(String pack) {
		this.pack = pack;
	}

	public Integer getGoodsNum() {
		return this.goodsNum;
	}

	public void setGoodsNum(Integer goodsNum) {
		this.goodsNum = goodsNum;
	}

	public CtGoods getGoods() {
		return goods;
	}

	public void setGoods(CtGoods goods) {
		this.goods = goods;
	}

	public Integer getBomId() {
		return bomId;
	}

	public void setBomId(Integer bomId) {
		this.bomId = bomId;
	}

}