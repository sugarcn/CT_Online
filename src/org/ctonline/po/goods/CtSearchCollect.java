package org.ctonline.po.goods;

/**
 * CtSearchCollect entity. @author MyEclipse Persistence Tools
 */

public class CtSearchCollect implements java.io.Serializable {

	// Fields

	private Integer coId;
	private String searchKey;
	private String isOptimise;
	private String searchTime;
	private Integer SeachSum;
	
	
	// Constructors

	public Integer getSeachSum() {
		return SeachSum;
	}

	public void setSeachSum(Integer seachSum) {
		SeachSum = seachSum;
	}

	/** default constructor */
	public CtSearchCollect() {
	}

	/** full constructor */
	public CtSearchCollect(String searchKey, String isOptimise,
			String searchTime) {
		this.searchKey = searchKey;
		this.isOptimise = isOptimise;
		this.searchTime = searchTime;
	}

	// Property accessors

	public Integer getCoId() {
		return this.coId;
	}

	public void setCoId(Integer coId) {
		this.coId = coId;
	}

	public String getSearchKey() {
		return this.searchKey;
	}

	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}

	public String getIsOptimise() {
		return this.isOptimise;
	}

	public void setIsOptimise(String isOptimise) {
		this.isOptimise = isOptimise;
	}

	public String getSearchTime() {
		return this.searchTime;
	}

	public void setSearchTime(String searchTime) {
		this.searchTime = searchTime;
	}

}