package org.ctonline.po.goods;

/**
 * CtGoodsLink entity. @author MyEclipse Persistence Tools
 */

public class CtGoodsLink implements java.io.Serializable {

	// Fields

	private Long GId;
	private Long linkGId;
	private String isDouble;

	// Constructors

	/** default constructor */
	public CtGoodsLink() {
	}

	/** minimal constructor */
	public CtGoodsLink(Long linkGId) {
		this.linkGId = linkGId;
	}

	/** full constructor */
	public CtGoodsLink(Long linkGId, String isDouble) {
		this.linkGId = linkGId;
		this.isDouble = isDouble;
	}

	// Property accessors

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public Long getLinkGId() {
		return this.linkGId;
	}

	public void setLinkGId(Long linkGId) {
		this.linkGId = linkGId;
	}

	public String getIsDouble() {
		return this.isDouble;
	}

	public void setIsDouble(String isDouble) {
		this.isDouble = isDouble;
	}

}