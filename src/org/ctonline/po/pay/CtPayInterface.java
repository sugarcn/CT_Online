package org.ctonline.po.pay;

import java.io.Serializable;

public class CtPayInterface
  implements Serializable
{
  private Long PInId;
  private String totalFee;
  private String tradeType;
  private String tradeNo;
  private String orderSn;
  private String tradeStatus;
  private String payDate;
  
  public Long getPInId()
  {
    return this.PInId;
  }
  
  public void setPInId(Long pInId)
  {
    this.PInId = pInId;
  }
  
  public String getTradeType()
  {
    return this.tradeType;
  }
  
  public void setTradeType(String tradeType)
  {
    this.tradeType = tradeType;
  }
  
  public String getTradeNo()
  {
    return this.tradeNo;
  }
  
  public String getTotalFee()
  {
    return this.totalFee;
  }
  
  public void setTotalFee(String totalFee)
  {
    this.totalFee = totalFee;
  }
  
  public void setTradeNo(String tradeNo)
  {
    this.tradeNo = tradeNo;
  }
  
  public String getOrderSn()
  {
    return this.orderSn;
  }
  
  public void setOrderSn(String orderSn)
  {
    this.orderSn = orderSn;
  }
  
  public String getTradeStatus()
  {
    return this.tradeStatus;
  }
  
  public void setTradeStatus(String tradeStatus)
  {
    this.tradeStatus = tradeStatus;
  }
  
  public String getPayDate()
  {
    return this.payDate;
  }
  
  public void setPayDate(String payDate)
  {
    this.payDate = payDate;
  }
  
  public CtPayInterface() {}
  
  public CtPayInterface(Long PInId, String tradeNo, String orderSn, String tradeStatus, String tradeType)
  {
    this.PInId = PInId;
    this.tradeNo = tradeNo;
    this.orderSn = orderSn;
    this.tradeStatus = tradeStatus;
    this.tradeType = tradeType;
  }
}
