package org.ctonline.po.manager;

import org.ctonline.po.user.CtRole;

/**
 * CtManager entity. @author MyEclipse Persistence Tools
 */

public class CtManager implements java.io.Serializable {

	// Fields

	private Integer MId;
	private CtRole ctRole;
	private String MManagerId;
	private String MManagername;
	private String MPassword;
	private String MLastTime;
	private String MLastIp;
	private Integer MGId;
	private Integer roleId;

	// Constructors

	/** default constructor */
	public CtManager() {
	}

	/** minimal constructor */
	public CtManager(String MManagerId, String MManagername, String MPassword) {
		this.MManagerId = MManagerId;
		this.MManagername = MManagername;
		this.MPassword = MPassword;
	}

	/** full constructor */
	public CtManager(CtRole ctRole, String MManagerId, String MManagername,
			String MPassword, String MLastTime, String MLastIp, Integer MGId) {
		this.ctRole = ctRole;
		this.MManagerId = MManagerId;
		this.MManagername = MManagername;
		this.MPassword = MPassword;
		this.MLastTime = MLastTime;
		this.MLastIp = MLastIp;
		this.MGId = MGId;
	}

	// Property accessors

	public Integer getMId() {
		return this.MId;
	}

	public void setMId(Integer MId) {
		this.MId = MId;
	}

	public CtRole getCtRole() {
		return this.ctRole;
	}

	public void setCtRole(CtRole ctRole) {
		this.ctRole = ctRole;
	}

	public String getMManagerId() {
		return this.MManagerId;
	}

	public void setMManagerId(String MManagerId) {
		this.MManagerId = MManagerId;
	}

	public String getMManagername() {
		return this.MManagername;
	}

	public void setMManagername(String MManagername) {
		this.MManagername = MManagername;
	}

	public String getMPassword() {
		return this.MPassword;
	}

	public void setMPassword(String MPassword) {
		this.MPassword = MPassword;
	}

	public String getMLastTime() {
		return this.MLastTime;
	}

	public void setMLastTime(String MLastTime) {
		this.MLastTime = MLastTime;
	}

	public String getMLastIp() {
		return this.MLastIp;
	}

	public void setMLastIp(String MLastIp) {
		this.MLastIp = MLastIp;
	}

	public Integer getMGId() {
		return this.MGId;
	}

	public void setMGId(Integer MGId) {
		this.MGId = MGId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

}