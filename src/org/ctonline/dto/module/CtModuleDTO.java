package org.ctonline.dto.module;

/**
 * CtModule entity. @author MyEclipse Persistence Tools
 */

public class CtModuleDTO implements java.io.Serializable {

	// Fields

	private Integer moduleId;
	private String moduleName;
	private String moduleUrl;
	private String moduleDesc;
	private Integer parentId;

	// Constructors

	/** default constructor */
	public CtModuleDTO() {
	}

	/** full constructor */
	public CtModuleDTO(String moduleName, String moduleUrl, String moduleDesc,
			Integer parentId) {
		this.moduleName = moduleName;
		this.moduleUrl = moduleUrl;
		this.moduleDesc = moduleDesc;
		this.parentId = parentId;
	}

	// Property accessors

	public Integer getModuleId() {
		return this.moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return this.moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModuleUrl() {
		return this.moduleUrl;
	}

	public void setModuleUrl(String moduleUrl) {
		this.moduleUrl = moduleUrl;
	}

	public String getModuleDesc() {
		return this.moduleDesc;
	}

	public void setModuleDesc(String moduleDesc) {
		this.moduleDesc = moduleDesc;
	}

	public Integer getParentId() {
		return this.parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

}