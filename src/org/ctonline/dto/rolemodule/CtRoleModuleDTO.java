package org.ctonline.dto.rolemodule;

import org.ctonline.po.basic.CtModule;
import org.ctonline.po.user.CtRole;

/**
 * CtRoleModule entity. @author MyEclipse Persistence Tools
 */

public class CtRoleModuleDTO implements java.io.Serializable {

	// Fields

	private Long roleModuleId;
	private CtRole ctRole;
	private CtModule ctModule;
	private String right;
	private String moduleId;
	private Integer roleId;

	// Constructors

	/** default constructor */
	public CtRoleModuleDTO() {
	}

	/** full constructor */
	public CtRoleModuleDTO(CtRole ctRole, CtModule ctModule, String right) {
		this.ctRole = ctRole;
		this.ctModule = ctModule;
		this.right = right;
	}

	// Property accessors

	public Long getRoleModuleId() {
		return this.roleModuleId;
	}

	public void setRoleModuleId(Long roleModuleId) {
		this.roleModuleId = roleModuleId;
	}

	public CtRole getCtRole() {
		return this.ctRole;
	}

	public void setCtRole(CtRole ctRole) {
		this.ctRole = ctRole;
	}

	public CtModule getCtModule() {
		return this.ctModule;
	}

	public void setCtModule(CtModule ctModule) {
		this.ctModule = ctModule;
	}

	public String getRight() {
		return this.right;
	}

	public void setRight(String right) {
		this.right = right;
	}


	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}



}