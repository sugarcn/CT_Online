package org.ctonline.dto.manager;

import org.ctonline.po.user.CtRole;

/**
 * CtManager entity. @author MyEclipse Persistence Tools
 */

public class CtManagerDTO implements java.io.Serializable {

	// Fields

	private Integer MId;
	private CtRole ctRole;
	private String MManagerId;
	private String MManagername;
	private String MPassword;
	private String MPasswordAgain;
	private String MLastTime;
	private String MLastIp;
	private Integer MGId;
	private int page;
	private String keyword;
	private Integer[] mid;
	private Long id;

	private String stime;
	private String etime;
	private String logType;
	private String userName;
	
	// Constructors

	public String getStime() {
		return stime;
	}

	public void setStime(String stime) {
		this.stime = stime;
	}

	public String getEtime() {
		return etime;
	}

	public void setEtime(String etime) {
		this.etime = etime;
	}

	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/** default constructor */
	public CtManagerDTO() {
	}

	public String getMPasswordAgain() {
		return MPasswordAgain;
	}

	public void setMPasswordAgain(String mPasswordAgain) {
		MPasswordAgain = mPasswordAgain;
	}

	/** minimal constructor */
	public CtManagerDTO(String MManagerId, String MManagername, String MPassword) {
		this.MManagerId = MManagerId;
		this.MManagername = MManagername;
		this.MPassword = MPassword;
	}

	/** full constructor */
	public CtManagerDTO(CtRole ctRole, String MManagerId, String MManagername,
			String MPassword, String MLastTime, String MLastIp, Integer MGId) {
		this.ctRole = ctRole;
		this.MManagerId = MManagerId;
		this.MManagername = MManagername;
		this.MPassword = MPassword;
		this.MLastTime = MLastTime;
		this.MLastIp = MLastIp;
		this.MGId = MGId;
	}

	// Property accessors

	public Integer getMId() {
		return this.MId;
	}

	public void setMId(Integer MId) {
		this.MId = MId;
	}

	public CtRole getCtRole() {
		return this.ctRole;
	}

	public void setCtRole(CtRole ctRole) {
		this.ctRole = ctRole;
	}

	public String getMManagerId() {
		return this.MManagerId;
	}

	public void setMManagerId(String MManagerId) {
		this.MManagerId = MManagerId;
	}

	public String getMManagername() {
		return this.MManagername;
	}

	public void setMManagername(String MManagername) {
		this.MManagername = MManagername;
	}

	public String getMPassword() {
		return this.MPassword;
	}

	public void setMPassword(String MPassword) {
		this.MPassword = MPassword;
	}

	public String getMLastTime() {
		return this.MLastTime;
	}

	public void setMLastTime(String MLastTime) {
		this.MLastTime = MLastTime;
	}

	public String getMLastIp() {
		return this.MLastIp;
	}

	public void setMLastIp(String MLastIp) {
		this.MLastIp = MLastIp;
	}

	public Integer getMGId() {
		return this.MGId;
	}

	public void setMGId(Integer MGId) {
		this.MGId = MGId;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Integer[] getMid() {
		return mid;
	}

	public void setMid(Integer[] mid) {
		this.mid = mid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}