package org.ctonline.dto.goods;

public class CtGoodsBrandDTO {
	private Long BId;
	private String BName;
	private String BLogo;
	private String BDesc;
	private String BUrl;
	private Integer sortOrder;
	private Boolean isShow;
	private int page;
	private String keyword;
	private Long[] mid;
	private Long id;
	public Long getBId() {
		return BId;
	}
	public void setBId(Long bId) {
		BId = bId;
	}
	public String getBName() {
		return BName;
	}
	public void setBName(String bName) {
		BName = bName;
	}
	public String getBLogo() {
		return BLogo;
	}
	public void setBLogo(String bLogo) {
		BLogo = bLogo;
	}
	public String getBDesc() {
		return BDesc;
	}
	public void setBDesc(String bDesc) {
		BDesc = bDesc;
	}
	public String getBUrl() {
		return BUrl;
	}
	public void setBUrl(String bUrl) {
		BUrl = bUrl;
	}
	public Integer getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
	public Boolean getIsShow() {
		return isShow;
	}
	public void setIsShow(Boolean isShow) {
		this.isShow = isShow;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public Long[] getMid() {
		return mid;
	}
	public void setMid(Long[] mid) {
		this.mid = mid;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

}
