package org.ctonline.dto.goods;

public class CtGoodsTypeDTO {
	
	private Long GTId;
	private String typeName;
	private String enabled;
	private int page;
	private String keyword;
	private Long[] mid;
	private Long id;
	public Long getGTId() {
		return GTId;
	}
	public void setGTId(Long gTId) {
		GTId = gTId;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getEnabled() {
		return enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public Long[] getMid() {
		return mid;
	}
	public void setMid(Long[] mid) {
		this.mid = mid;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

}
