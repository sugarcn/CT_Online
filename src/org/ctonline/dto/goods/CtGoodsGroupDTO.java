package org.ctonline.dto.goods;

public class CtGoodsGroupDTO {
	private Long GGId;
	private String GGName;
	private String GGDesc;
	private int page;
	private String keyword;
	private Long[] mid;
	private Long id;
	public Long getGGId() {
		return GGId;
	}
	public void setGGId(Long gGId) {
		GGId = gGId;
	}
	public String getGGName() {
		return GGName;
	}
	public void setGGName(String gGName) {
		GGName = gGName;
	}
	public String getGGDesc() {
		return GGDesc;
	}
	public void setGGDesc(String gGDesc) {
		GGDesc = gGDesc;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public Long[] getMid() {
		return mid;
	}
	public void setMid(Long[] mid) {
		this.mid = mid;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

}
