package org.ctonline.dto.goods;

public class CtGoodsAttributeDTO {
	
	private Long attrId;
	private Long GTId;
	private String attrName;
	private Short sortOrder;
	private int page;
	private String keyword;
	private Long[] mid;
	private Long id;
	public Long getAttrId() {
		return attrId;
	}
	public void setAttrId(Long attrId) {
		this.attrId = attrId;
	}
	public Long getGTId() {
		return GTId;
	}
	public void setGTId(Long gTId) {
		GTId = gTId;
	}
	public String getAttrName() {
		return attrName;
	}
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}
	public Short getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(Short sortOrder) {
		this.sortOrder = sortOrder;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public Long[] getMid() {
		return mid;
	}
	public void setMid(Long[] mid) {
		this.mid = mid;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

}
