package org.ctonline.dto.goods;

public class CtGoodsCategoryDTO {
	
	private Long CId;
	private String CName;
	private Long parentId;
	private Long sortOrder;
	private String filterAttr;
	private int page;
	private String keyword;
	private Long[] mid;
	private Long id;
	private Long type;
	
	public Long getType() {
		return type;
	}
	public void setType(Long type) {
		this.type = type;
	}
	public Long getCId() {
		return CId;
	}
	public void setCId(Long cId) {
		CId = cId;
	}
	public String getCName() {
		return CName;
	}
	public void setCName(String cName) {
		CName = cName;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public Long getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(Long sortOrder) {
		this.sortOrder = sortOrder;
	}
	public String getFilterAttr() {
		return filterAttr;
	}
	public void setFilterAttr(String filterAttr) {
		this.filterAttr = filterAttr;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public Long[] getMid() {
		return mid;
	}
	public void setMid(Long[] mid) {
		this.mid = mid;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

}
