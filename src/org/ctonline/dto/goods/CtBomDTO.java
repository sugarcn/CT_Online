package org.ctonline.dto.goods;

import java.util.HashSet;
import java.util.Set;

/**
 * CtBom entity. @author MyEclipse Persistence Tools
 */

public class CtBomDTO implements java.io.Serializable {

	// Fields

	private Integer bomId;
	private String bomTitle;
	private String bomDesc;
	private String bomTime;
	private Set ctBomGoodses = new HashSet(0);
	private int page;
	private String keyword;
	private Integer[] mid;
	private Long id;
	private Long UId;

	// Constructors

	public Long getUId() {
		return UId;
	}

	public void setUId(Long uId) {
		UId = uId;
	}

	/** default constructor */
	public CtBomDTO() {
	}

	/** minimal constructor */
	public CtBomDTO(String bomTitle) {
		this.bomTitle = bomTitle;
	}

	/** full constructor */
	public CtBomDTO(String bomTitle, String bomDesc, String bomTime,
			Set ctBomGoodses) {
		this.bomTitle = bomTitle;
		this.bomDesc = bomDesc;
		this.bomTime = bomTime;
		this.ctBomGoodses = ctBomGoodses;
	}

	// Property accessors

	public Integer getBomId() {
		return this.bomId;
	}

	public void setBomId(Integer bomId) {
		this.bomId = bomId;
	}

	public String getBomTitle() {
		return this.bomTitle;
	}

	public void setBomTitle(String bomTitle) {
		this.bomTitle = bomTitle;
	}

	public String getBomDesc() {
		return this.bomDesc;
	}

	public void setBomDesc(String bomDesc) {
		this.bomDesc = bomDesc;
	}

	public String getBomTime() {
		return this.bomTime;
	}

	public void setBomTime(String bomTime) {
		this.bomTime = bomTime;
	}

	public Set getCtBomGoodses() {
		return this.ctBomGoodses;
	}

	public void setCtBomGoodses(Set ctBomGoodses) {
		this.ctBomGoodses = ctBomGoodses;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Integer[] getMid() {
		return mid;
	}

	public void setMid(Integer[] mid) {
		this.mid = mid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}