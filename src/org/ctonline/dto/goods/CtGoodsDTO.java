package org.ctonline.dto.goods;

import java.io.File;

import org.ctonline.po.basic.CtReplace;

public class CtGoodsDTO {
	
	private Long GId;
	private String GName;
	private Long CId;
	private String GSn;
	private Long BId;
	private Double marketPrice;
	private Double shopPrice;
	private Double promotePrice;
	private String isOnSale;
	private String GKeywords;
	private Long tokenCredit;
	private Long giveCredit;
	private String GUnit;
	private String pack1;
	private Integer pack1Num;
	private String pack2;
	private Integer pack2Num;
	private String pack3;
	private Integer pack3Num;
	private String pack4;
	private Integer pack4Num;
	private String isDel;
	
	 private String GType;
	  private String UType;
	  private String UUsername;
	  private String cids;
	  private String isSample;
	  private String isPartial;
	  private File excelFile;

	private CtReplace replace;
	private String re1;
	private String re2;
	private String re3;
	private String re4;
	private String re5;
	private String reId;
	
	private Integer coId;
	
	private Integer gnum;
	
	
	public Integer getGnum() {
		return gnum;
	}
	public void setGnum(Integer gnum) {
		this.gnum = gnum;
	}
	public Integer getCoId() {
		return coId;
	}
	public void setCoId(Integer coId) {
		this.coId = coId;
	}
	public String getRe1() {
		return re1;
	}
	public void setRe1(String re1) {
		this.re1 = re1;
	}
	public String getRe2() {
		return re2;
	}
	public void setRe2(String re2) {
		this.re2 = re2;
	}
	public String getRe3() {
		return re3;
	}
	public void setRe3(String re3) {
		this.re3 = re3;
	}
	public String getRe4() {
		return re4;
	}
	public void setRe4(String re4) {
		this.re4 = re4;
	}
	public String getRe5() {
		return re5;
	}
	public void setRe5(String re5) {
		this.re5 = re5;
	}
	public String getReId() {
		return reId;
	}
	public void setReId(String reId) {
		this.reId = reId;
	}
	public CtReplace getReplace() {
		return replace;
	}
	public void setReplace(CtReplace replace) {
		this.replace = replace;
	}
	public String getGType() {
		return GType;
	}
	public void setGType(String gType) {
		GType = gType;
	}
	public String getUType() {
		return UType;
	}
	public void setUType(String uType) {
		UType = uType;
	}
	public String getUUsername() {
		return UUsername;
	}
	public void setUUsername(String uUsername) {
		UUsername = uUsername;
	}
	public String getCids() {
		return cids;
	}
	public void setCids(String cids) {
		this.cids = cids;
	}
	public String getIsSample() {
		return isSample;
	}
	public void setIsSample(String isSample) {
		this.isSample = isSample;
	}
	public String getIsPartial() {
		return isPartial;
	}
	public void setIsPartial(String isPartial) {
		this.isPartial = isPartial;
	}
	public File getExcelFile() {
		return excelFile;
	}
	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}
	public String getIsDel() {
		return isDel;
	}
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	public String getGUnit() {
		return GUnit;
	}
	public void setGUnit(String gUnit) {
		GUnit = gUnit;
	}
	public String getPack1() {
		return pack1;
	}
	public void setPack1(String pack1) {
		this.pack1 = pack1;
	}
	public Integer getPack1Num() {
		return pack1Num;
	}
	public void setPack1Num(Integer pack1Num) {
		this.pack1Num = pack1Num;
	}
	public String getPack2() {
		return pack2;
	}
	public void setPack2(String pack2) {
		this.pack2 = pack2;
	}
	public Integer getPack2Num() {
		return pack2Num;
	}
	public void setPack2Num(Integer pack2Num) {
		this.pack2Num = pack2Num;
	}
	public String getPack3() {
		return pack3;
	}
	public void setPack3(String pack3) {
		this.pack3 = pack3;
	}
	public Integer getPack3Num() {
		return pack3Num;
	}
	public void setPack3Num(Integer pack3Num) {
		this.pack3Num = pack3Num;
	}
	public String getPack4() {
		return pack4;
	}
	public void setPack4(String pack4) {
		this.pack4 = pack4;
	}
	public Integer getPack4Num() {
		return pack4Num;
	}
	public void setPack4Num(Integer pack4Num) {
		this.pack4Num = pack4Num;
	}
	private int page;
	private String keyword;
	private Long[] mid;
	private Long id;
	public Long getGId() {
		return GId;
	}
	public void setGId(Long gId) {
		GId = gId;
	}
	public String getGName() {
		return GName;
	}
	public void setGName(String gName) {
		GName = gName;
	}
	public Long getCId() {
		return CId;
	}
	public void setCId(Long cId) {
		CId = cId;
	}
	public String getGSn() {
		return GSn;
	}
	public void setGSn(String gSn) {
		GSn = gSn;
	}
	public Long getBId() {
		return BId;
	}
	public void setBId(Long bId) {
		BId = bId;
	}
	public Double getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(Double marketPrice) {
		this.marketPrice = marketPrice;
	}
	public Double getShopPrice() {
		return shopPrice;
	}
	public void setShopPrice(Double shopPrice) {
		this.shopPrice = shopPrice;
	}
	public Double getPromotePrice() {
		return promotePrice;
	}
	public void setPromotePrice(Double promotePrice) {
		this.promotePrice = promotePrice;
	}
	public String getIsOnSale() {
		return isOnSale;
	}
	public void setIsOnSale(String isOnSale) {
		this.isOnSale = isOnSale;
	}
	public String getGKeywords() {
		return GKeywords;
	}
	public void setGKeywords(String gKeywords) {
		GKeywords = gKeywords;
	}
	public Long getTokenCredit() {
		return tokenCredit;
	}
	public void setTokenCredit(Long tokenCredit) {
		this.tokenCredit = tokenCredit;
	}
	public Long getGiveCredit() {
		return giveCredit;
	}
	public void setGiveCredit(Long giveCredit) {
		this.giveCredit = giveCredit;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public Long[] getMid() {
		return mid;
	}
	public void setMid(Long[] mid) {
		this.mid = mid;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	private Long RId;
	public Long getRId() {
		return RId;
	}
	public void setRId(Long rId) {
		RId = rId;
	}
	private Long GTId;
	public Long getGTId() {
		return GTId;
	}
	public void setGTId(Long gTId) {
		GTId = gTId;
	}
	private String dblk;
	public String getDblk() {
		return dblk;
	}
	public void setDblk(String dblk) {
		this.dblk = dblk;
	}
	private String mval;
	public String getMval() {
		return mval;
	}
	public void setMval(String mval) {
		this.mval = mval;
	}
	private Integer depotId;
	public Integer getDepotId() {
		return depotId;
	}
	public void setDepotId(Integer depotId) {
		this.depotId = depotId;
	}
	private Integer[] deid;
	public Integer[] getDeid() {
		return deid;
	}
	public void setDeid(Integer[] deid) {
		this.deid = deid;
	}
	private Long detlId;
	public Long getDetlId() {
		return detlId;
	}
	public void setDetlId(Long detlId) {
		this.detlId = detlId;
	}
	private String GDetail;
	public String getGDetail() {
		return GDetail;
	}
	public void setGDetail(String gDetail) {
		GDetail = gDetail;
	}
	
	private Long GGId;
	private String UIds;
	public String getUIds() {
		return UIds;
	}
	public void setUIds(String uIds) {
		UIds = uIds;
	}
	public Long getGGId() {
		return GGId;
	}
	public void setGGId(Long gGId) {
		GGId = gGId;
	}
	private Long linkGId;
	public Long getLinkGId() {
		return linkGId;
	}
	public void setLinkGId(Long linkGId) {
		this.linkGId = linkGId;
	}
	
}
