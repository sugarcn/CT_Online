package org.ctonline.dto.goods;


public class CtGoodsPriceDTO {
	private String goodsname;
	private String goodstype;
	private String userid;
	private String username;
	private String usertype;
	private String goodsKey;
	private String userKey;
	private int page;
	private Long id;
	private Long UId;
	private String UType;
	private Long GId;
	private String GType;
	private String price;
	private Long pid;
	
	private String GInfo;
	private String UInfo;
	
	
	public String getGInfo() {
		return GInfo;
	}
	public void setGInfo(String gInfo) {
		GInfo = gInfo;
	}
	public String getUInfo() {
		return UInfo;
	}
	public void setUInfo(String uInfo) {
		UInfo = uInfo;
	}
	public Long getPid() {
		return pid;
	}
	public void setPid(Long pid) {
		this.pid = pid;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getGoodsname() {
		return goodsname;
	}
	public void setGoodsname(String goodsname) {
		this.goodsname = goodsname;
	}
	public String getGoodstype() {
		return goodstype;
	}
	public void setGoodstype(String goodstype) {
		this.goodstype = goodstype;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public String getGoodsKey() {
		return goodsKey;
	}
	public void setGoodsKey(String goodsKey) {
		this.goodsKey = goodsKey;
	}
	public String getUserKey() {
		return userKey;
	}
	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUType() {
		return UType;
	}
	public void setUType(String uType) {
		UType = uType;
	}
	public String getGType() {
		return GType;
	}
	public void setGType(String gType) {
		GType = gType;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public Long getUId() {
		return UId;
	}
	public void setUId(Long uId) {
		UId = uId;
	}
	public Long getGId() {
		return GId;
	}
	public void setGId(Long gId) {
		GId = gId;
	}

	
	
	
}
