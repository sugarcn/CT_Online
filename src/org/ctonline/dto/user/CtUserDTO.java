package org.ctonline.dto.user;

import java.util.List;
import java.util.Set;

/**
 * CtUser entity. @author MyEclipse Persistence Tools
 */

public class CtUserDTO{

	// Fields

	private Long UId;
	private String UUserid;
	private String UUsername;
	private String UPassword;
	private String UMb;
	private String UEmail;
	private String UQq;
	private String UWeibo;
	private String UTb;
	private String USex;
	private String UBirthday;
	private String ULastTime;
	private String URegTime;
	private String ULastIp;
	private Long RId;
	private Long UGId;
	private String UState;
	private String UCode;
	
	private int page;
	private String keyword;
	private Long[] mid;
	private Long id;
	
	private String flag;
	private String UDistribution;
	private String UType;

	private Integer drId;
	
	private Double UCreditLimit;
	private Double URemainingAmount;

	private Integer clientId;
	
	
	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	private String addessUserName;
	private Long country;
	private Long province;
	private Long city;
	private Long district;
	private String phone;
	private String usermb;
	private String etcDesc;
	private String address;
	private String zipCode;
	private String ACustomer;
	
	private String cusName;
	
	
	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getACustomer() {
		return ACustomer;
	}

	public void setACustomer(String aCustomer) {
		ACustomer = aCustomer;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddessUserName() {
		return addessUserName;
	}

	public void setAddessUserName(String addessUserName) {
		this.addessUserName = addessUserName;
	}


	public Long getCountry() {
		return country;
	}

	public void setCountry(Long country) {
		this.country = country;
	}

	public Long getProvince() {
		return province;
	}

	public void setProvince(Long province) {
		this.province = province;
	}

	public Long getCity() {
		return city;
	}

	public void setCity(Long city) {
		this.city = city;
	}

	public Long getDistrict() {
		return district;
	}

	public void setDistrict(Long district) {
		this.district = district;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUsermb() {
		return usermb;
	}

	public void setUsermb(String usermb) {
		this.usermb = usermb;
	}

	public String getEtcDesc() {
		return etcDesc;
	}

	public void setEtcDesc(String etcDesc) {
		this.etcDesc = etcDesc;
	}

	private Long aid;
	
	
	public Long getAid() {
		return aid;
	}

	public void setAid(Long aid) {
		this.aid = aid;
	}

	public Double getUCreditLimit() {
		return UCreditLimit;
	}

	public void setUCreditLimit(Double uCreditLimit) {
		UCreditLimit = uCreditLimit;
	}

	public Double getURemainingAmount() {
		return URemainingAmount;
	}

	public void setURemainingAmount(Double uRemainingAmount) {
		URemainingAmount = uRemainingAmount;
	}

	public Integer getDrId() {
		return drId;
	}

	public void setDrId(Integer drId) {
		this.drId = drId;
	}

	public Long getUId() {
		return this.UId;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getUDistribution() {
		return UDistribution;
	}

	public void setUDistribution(String uDistribution) {
		UDistribution = uDistribution;
	}

	public String getUType() {
		return UType;
	}

	public void setUType(String uType) {
		UType = uType;
	}

	public void setUId(Long UId) {
		this.UId = UId;
	}

	public String getUUserid() {
		return this.UUserid;
	}

	public void setUUserid(String UUserid) {
		this.UUserid = UUserid;
	}

	public String getUUsername() {
		return this.UUsername;
	}

	public void setUUsername(String UUsername) {
		this.UUsername = UUsername;
	}

	public String getUPassword() {
		return this.UPassword;
	}

	public void setUPassword(String UPassword) {
		this.UPassword = UPassword;
	}

	public String getUMb() {
		return this.UMb;
	}

	public void setUMb(String UMb) {
		this.UMb = UMb;
	}

	public String getUEmail() {
		return this.UEmail;
	}

	public void setUEmail(String UEmail) {
		this.UEmail = UEmail;
	}

	public String getUQq() {
		return this.UQq;
	}

	public void setUQq(String UQq) {
		this.UQq = UQq;
	}

	public String getUWeibo() {
		return this.UWeibo;
	}

	public void setUWeibo(String UWeibo) {
		this.UWeibo = UWeibo;
	}

	public String getUTb() {
		return this.UTb;
	}

	public void setUTb(String UTb) {
		this.UTb = UTb;
	}

	public String getUSex() {
		return this.USex;
	}

	public void setUSex(String USex) {
		this.USex = USex;
	}

	public String getUBirthday() {
		return this.UBirthday;
	}

	public void setUBirthday(String UBirthday) {
		this.UBirthday = UBirthday;
	}

	public String getULastTime() {
		return this.ULastTime;
	}

	public void setULastTime(String ULastTime) {
		this.ULastTime = ULastTime;
	}

	public String getURegTime() {
		return this.URegTime;
	}

	public void setURegTime(String URegTime) {
		this.URegTime = URegTime;
	}

	public String getULastIp() {
		return this.ULastIp;
	}

	public void setULastIp(String ULastIp) {
		this.ULastIp = ULastIp;
	}

	public Long getRId() {
		return this.RId;
	}

	public void setRId(Long RId) {
		this.RId = RId;
	}

	public Long getUGId() {
		return this.UGId;
	}

	public void setUGId(Long UGId) {
		this.UGId = UGId;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Long[] getMid() {
		return mid;
	}

	public void setMid(Long[] mid) {
		this.mid = mid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

//	public CtAddTicket getCtAddTicket() {
//		return ctAddTicket;
//	}
//
//	public void setCtAddTicket(CtAddTicket ctAddTicket) {
//		this.ctAddTicket = ctAddTicket;
//	}
	public String getUState() {
		return UState;
	}

	public void setUState(String uState) {
		UState = uState;
	}

	public String getUCode() {
		return UCode;
	}

	public void setUCode(String uCode) {
		UCode = uCode;
	}

}