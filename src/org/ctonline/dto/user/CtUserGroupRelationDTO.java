package org.ctonline.dto.user;

public class CtUserGroupRelationDTO {
	private Long UId;
	private Long UGId;
	private String UIds;
	
	
	
	
	public String getUIds() {
		return UIds;
	}
	public void setUIds(String uIds) {
		UIds = uIds;
	}
	public Long getUId() {
		return UId;
	}
	public void setUId(Long uId) {
		UId = uId;
	}
	public Long getUGId() {
		return UGId;
	}
	public void setUGId(Long uGId) {
		UGId = uGId;
	}
	

}
