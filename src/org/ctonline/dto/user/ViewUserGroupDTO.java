package org.ctonline.dto.user;

import java.util.List;

import org.ctonline.po.views.ViewUserGroup;


public class ViewUserGroupDTO {
	private Long id;
	private String userid;
	private String username;
	private String usertype;
	private String user;
	private String goods;
	private List<ViewUserGroup> list;
	public List<ViewUserGroup> getList() {
		return list;
	}
	public void setList(List<ViewUserGroup> list) {
		this.list = list;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getGoods() {
		return goods;
	}
	public void setGoods(String goods) {
		this.goods = goods;
	}

}
