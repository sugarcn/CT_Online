package org.ctonline.dto.user;

public class CtUserGroupDTO {
	private Long UGId;
	private String UGName;
	private String UGDesc;
	private int page;
	private String keyword;
	private Long[] mid;
	private Long id;
	public Long getUGId() {
		return UGId;
	}
	public void setUGId(Long uGId) {
		UGId = uGId;
	}
	public String getUGName() {
		return UGName;
	}
	public void setUGName(String uGName) {
		UGName = uGName;
	}
	public String getUGDesc() {
		return UGDesc;
	}
	public void setUGDesc(String uGDesc) {
		UGDesc = uGDesc;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public Long[] getMid() {
		return mid;
	}
	public void setMid(Long[] mid) {
		this.mid = mid;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

}
