package org.ctonline.dto.help;

public class CtHelpDTO
{
  private Integer HId;
  private Integer HTypeId;
  private String HTitle;
  private int[] mid;
  private String HDesc;
  private String HTypeName;
  private int page;
  private int id;
  private String keyword;

  public String getHTypeName()
  {
    return this.HTypeName;
  }
  public void setHTypeName(String hTypeName) {
    this.HTypeName = hTypeName;
  }
  public Integer getHId() {
    return this.HId;
  }
  public void setHId(Integer hId) {
    this.HId = hId;
  }
  public Integer getHTypeId() {
    return this.HTypeId;
  }
  public void setHTypeId(Integer hTypeId) {
    this.HTypeId = hTypeId;
  }
  public String getHTitle() {
    return this.HTitle;
  }
  public void setHTitle(String hTitle) {
    this.HTitle = hTitle;
  }
  public int[] getMid() {
    return this.mid;
  }
  public void setMid(int[] mid) {
    this.mid = mid;
  }
  public String getHDesc() {
    return this.HDesc;
  }
  public void setHDesc(String hDesc) {
    this.HDesc = hDesc;
  }
  public int getPage() {
    return this.page;
  }
  public void setPage(int page) {
    this.page = page;
  }
  public int getId() {
    return this.id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getKeyword() {
    return this.keyword;
  }
  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }
}