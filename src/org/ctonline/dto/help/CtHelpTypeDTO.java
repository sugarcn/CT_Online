package org.ctonline.dto.help;

public class CtHelpTypeDTO
{
  private Integer HTypeId;
  private String HTypeName;
  private int page;
  private String keyword;
  private int[] mid;
  private int id;
  private int AdId;

  public void setId(int id)
  {
    this.id = id;
  }
  public Integer getId() {
    return Integer.valueOf(this.id);
  }
  public void setId(Integer id) {
    this.id = id.intValue();
  }
  public Integer getHTypeId() {
    return this.HTypeId;
  }
  public void setHTypeId(Integer hTypeId) {
    this.HTypeId = hTypeId;
  }
  public String getHTypeName() {
    return this.HTypeName;
  }
  public void setHTypeName(String hTypeName) {
    this.HTypeName = hTypeName;
  }
  public int[] getMid() {
    return this.mid;
  }
  public void setMid(int[] mid) {
    this.mid = mid;
  }
  public int getPage() {
    return this.page;
  }
  public void setPage(int page) {
    this.page = page;
  }
  public String getKeyword() {
    return this.keyword;
  }
  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }
  public int getAdId() {
    return this.AdId;
  }
  public void setAdId(int adId) {
    this.AdId = adId;
  }
}