package org.ctonline.dto.basic;

public class CtAdTypeDTO {
	private Integer adTypeId;
	private String adTypeName;
	private int page;
	private String keyword;
	private int[] mid;
	private int id;
	private int AdId;
	private String AdImg;
	
	public void setId(int id) {
		this.id = id;
	}
	public void setAdImg(String adImg) {
		AdImg = adImg;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAdTypeId() {
		return adTypeId;
	}
	public void setAdTypeId(Integer adTypeId) {
		this.adTypeId = adTypeId;
	}
	public String getAdTypeName() {
		return adTypeName;
	}
	public int[] getMid() {
		return mid;
	}
	public void setMid(int[] mid) {
		this.mid = mid;
	}
	public void setAdTypeName(String adTypeName) {
		this.adTypeName = adTypeName;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public int getAdId() {
		return AdId;
	}
	public void setAdId(int adId) {
		AdId = adId;
	}
	public String getAdImg() {
		return AdImg;
	}
	
	
}
