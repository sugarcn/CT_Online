package org.ctonline.dto.basic;

public class CtDepotDTO {
	
	private Integer depotId;
	private String depotName;
	private String depotAdd;
	private String disArr;
	private int page;
	private String keyword;
	private Integer[] mid;
	private Integer id;
	public Integer getDepotId() {
		return depotId;
	}
	public void setDepotId(Integer depotId) {
		this.depotId = depotId;
	}
	public String getDepotName() {
		return depotName;
	}
	public void setDepotName(String depotName) {
		this.depotName = depotName;
	}
	public String getDepotAdd() {
		return depotAdd;
	}
	public void setDepotAdd(String depotAdd) {
		this.depotAdd = depotAdd;
	}
	public String getDisArr() {
		return disArr;
	}
	public void setDisArr(String disArr) {
		this.disArr = disArr;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public Integer[] getMid() {
		return mid;
	}
	public void setMid(Integer[] mid) {
		this.mid = mid;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

}
