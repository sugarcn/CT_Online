package org.ctonline.dto.basic;

import org.ctonline.po.basic.CtCouponDetail;

public class CtCouponDetailDTO {
private CtCouponDetail coupondetail;
	
	private String coupondeId;
	private int page;
	private String keyword;
	public int getPage() {
		return page;
	}
	public CtCouponDetail getCoupondetail() {
		return coupondetail;
	}
	public void setCoupondetail(CtCouponDetail coupondetail) {
		this.coupondetail = coupondetail;
	}
	public String getCoupondeId() {
		return coupondeId;
	}
	public void setCoupondeId(String coupondeId) {
		this.coupondeId = coupondeId;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public CtCouponDetail getCouponDetail() {
		return coupondetail;
	}
	public void setCouponDetail(CtCouponDetail coupondetail) {
		this.coupondetail = coupondetail;
	}
}
	
	
	
