package org.ctonline.dto.basic;

import org.ctonline.po.basic.CtCoupon;

//import java.util.Set;
//import org.ctonline.po.coupon.CtCouponDetail;

public class CtCouponDTO  {
	
	private CtCoupon coupon;
	
	private int page;
	private String keyword;
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public CtCoupon getCoupon() {
		return coupon;
	}
	public void setCoupon(CtCoupon coupon) {
		this.coupon = coupon;
	}
	
	
	
	
}
