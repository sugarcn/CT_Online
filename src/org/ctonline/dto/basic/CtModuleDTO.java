package org.ctonline.dto.basic;

import java.util.List;

import org.ctonline.po.basic.CtModule;

public class CtModuleDTO {
	private  String ModuleUrl;
	private String imgUrl;
	private Integer moduleId;
	private String moduleName;
	private String moduleDesc;
	private Integer parentId;
	private Integer moduleType;
	private int page;
	private String keyword;
	private int[] mid;
	private Integer id;
	
	public Integer getModuleId() {
		return moduleId;
	}
	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public Integer getModuleType() {
		return moduleType;
	}
	public void setModuleType(Integer moduleType) {
		this.moduleType = moduleType;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int[] getMid() {
		return mid;
	}
	public void setMid(int[] mid) {
		this.mid = mid;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List<CtModule> getPlist() {
		return plist;
	}
	public void setPlist(List<CtModule> plist) {
		this.plist = plist;
	}
	private List<CtModule> plist;
	

	public String getModuleDesc() {
		return moduleDesc;
	}
	public void setModuleDesc(String moduleDesc) {
		this.moduleDesc = moduleDesc;
	}
	public String getModuleUrl() {
		return ModuleUrl;
	}
	public void setModuleUrl(String ModuleUrl){
		this.ModuleUrl = ModuleUrl;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	
}
