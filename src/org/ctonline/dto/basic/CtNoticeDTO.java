package org.ctonline.dto.basic;

import java.util.ArrayList;
import java.util.List;

import org.ctonline.po.basic.CtNoticeType;

public class CtNoticeDTO {
	private int page;
	private Integer noId;
	private String noTitle;
	private String noContent;
	private String noUrl;
	private String keyword;
	private int[] mid;
	private String id;
	private String imgUrl;
	
	
	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	private List<CtNoticeType> noticeTypeList = new ArrayList<CtNoticeType>();
	
	public List<CtNoticeType> getNoticeTypeList() {
		return noticeTypeList;
	}

	public void setNoticeTypeList(List<CtNoticeType> noticeTypeList) {
		this.noticeTypeList = noticeTypeList;
	}

	private Integer typeId;
	 
	
	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public Integer getNoId() {
		return noId;
	}

	public void setNoId(Integer noId) {
		this.noId = noId;
	}

	public String getNoTitle() {
		return noTitle;
	}

	public void setNoTitle(String noTitle) {
		this.noTitle = noTitle;
	}

	public String getNoContent() {
		return noContent;
	}

	public void setNoContent(String noContent) {
		this.noContent = noContent;
	}

	public String getNoUrl() {
		return noUrl;
	}

	public void setNoUrl(String noUrl) {
		this.noUrl = noUrl;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int[] getMid() {
		return mid;
	}

	public void setMid(int[] mid) {
		this.mid = mid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
