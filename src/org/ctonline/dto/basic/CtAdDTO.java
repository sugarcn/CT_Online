package org.ctonline.dto.basic;

import java.util.List;

import org.ctonline.po.basic.CtAdType;

public class CtAdDTO {
	private int AdId;
	private int AdTypeId;
	private String Url;
	private String AdTypeName;
	private int[] mid;
	private String adTitle;
	private String adImg;
	private String adUrl;
	private String adDesc;
	private String adWidth;
	private String adHeight;
	private int page;
	private String keyword;
	
	
	private Integer adSort;
	private String adIsUp;
	
	private Integer adType;
	
	private List<CtAdType> adTypes;
	
	
	public List<CtAdType> getAdTypes() {
		return adTypes;
	}
	public void setAdTypes(List<CtAdType> adTypes) {
		this.adTypes = adTypes;
	}
	public Integer getAdSort() {
		return adSort;
	}
	public void setAdSort(Integer adSort) {
		this.adSort = adSort;
	}
	public String getAdIsUp() {
		return adIsUp;
	}
	public void setAdIsUp(String adIsUp) {
		this.adIsUp = adIsUp;
	}
	public Integer getAdType() {
		return adType;
	}
	public void setAdType(Integer adType) {
		this.adType = adType;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getAdTitle() {
		return adTitle;
	}
	public void setAdTitle(String adTitle) {
		this.adTitle = adTitle;
	}
	public String getAdImg() {
		return adImg;
	}
	public void setAdImg(String adImg) {
		this.adImg = adImg;
	}
	public String getAdUrl() {
		return adUrl;
	}
	public void setAdUrl(String adUrl) {
		this.adUrl = adUrl;
	}
	public String getAdDesc() {
		return adDesc;
	}
	public void setAdDesc(String adDesc) {
		this.adDesc = adDesc;
	}
	public String getAdWidth() {
		return adWidth;
	}
	public void setAdWidth(String adWidth) {
		this.adWidth = adWidth;
	}
	public String getAdHeight() {
		return adHeight;
	}
	public void setAdHeight(String adHeight) {
		this.adHeight = adHeight;
	}
	public String getUrl() {
		return Url;
	}
	public void setUrl(String url) {
		Url = url;
	}
	public int[] getMid() {
		return mid;
	}
	public void setMid(int[] mid) {
		this.mid = mid;
	}
	public int getAdTypeId() {
		return AdTypeId;
	}
	public void setAdTypeId(int adTypeId) {
		AdTypeId = adTypeId;
	}
	public String getAdTypeName() {
		return AdTypeName;
	}
	public void setAdTypeName(String adTypeName) {
		AdTypeName = adTypeName;
	}
	public int getAdId() {
		return AdId;
	}
	public void setAdId(int adId) {
		AdId = adId;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	
}
