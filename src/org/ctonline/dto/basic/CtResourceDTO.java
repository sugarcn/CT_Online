package org.ctonline.dto.basic;

import java.util.ArrayList;
import java.util.List;

import org.ctonline.po.basic.CtResourceType;

public class CtResourceDTO {
	private Long RId;
	private String RName;
	private String RUrl;
	private String RTName;
	private List<CtResourceType> resourceTypesList = new ArrayList<CtResourceType>();
	
	private Integer rsort;
	
	
	public Integer getRsort() {
		return rsort;
	}
	public void setRsort(Integer rsort) {
		this.rsort = rsort;
	}
	public List<CtResourceType> getResourceTypesList() {
		return resourceTypesList;
	}
	public void setResourceTypesList(List<CtResourceType> resourceTypesList) {
		this.resourceTypesList = resourceTypesList;
	}
	public Long getRId() {
		return RId;
	}
	public void setRId(Long rId) {
		RId = rId;
	}
	public String getRName() {
		return RName;
	}
	public void setRName(String rName) {
		RName = rName;
	}
	public String getRUrl() {
		return RUrl;
	}
	public void setRUrl(String rUrl) {
		RUrl = rUrl;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public Long[] getMid() {
		return mid;
	}
	public void setMid(Long[] mid) {
		this.mid = mid;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	private int page;
	private String keyword;
	private Long[] mid;
	private Long id;
	private Long RTId;
	public Long getRTId() {
		return RTId;
	}
	public void setRTId(Long rTId) {
		RTId = rTId;
	}
	public String getRTName() {
		return RTName;
	}
	public void setRTName(String rTName) {
		RTName = rTName;
	}
}
