package org.ctonline.dto.role;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.ctonline.po.user.CtRole;

/**
 * CtRole entity. @author MyEclipse Persistence Tools
 */

public class CtRoleDTO implements java.io.Serializable {

	// Fields

	private Integer roleId;
	private String roleName;
	private String roleDesc;
	private int page;
	private String keyword;
	private Long[] mid;
	private Long id;
	private List<CtRole> rlist;

	// Constructors

	/** default constructor */
	public CtRoleDTO() {
	}

	/** minimal constructor */
	public CtRoleDTO(String roleName, String roleDesc) {
		this.roleName = roleName;
		this.roleDesc = roleDesc;
	}

	// Property accessors

	public Integer getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDesc() {
		return this.roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}




	public Long[] getMid() {
		return mid;
	}

	public void setMid(Long[] mid) {
		this.mid = mid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<CtRole> getRlist() {
		return rlist;
	}

	public void setRlist(List<CtRole> rlist) {
		this.rlist = rlist;
	}


}