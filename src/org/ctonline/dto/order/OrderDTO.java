package org.ctonline.dto.order;

import org.ctonline.po.pay.CtPayInterface;

public class OrderDTO
{
  private int page;
  private Long uid;
  private Long GId;
  private Long gnum;
  private String pack;
  private Long cupon;
  private String Discount;
  private String orderSn;
  private Integer id;
  private String keyword;
  private Integer[] mid;
  private Byte bid;
  private Byte[] baid;
  private Integer exId;
  private String exName;
  private String exUrl;
  private String exDesc;
  private Byte bankId;
  private String deposit;
  private String accountName;
  private String accountNumber;
  private String orderTime;
  private Boolean shoppingTypeCollect;
  private String consignee;
  private String data;
  private Long ctid;
  private String ctids;
  private String gids;
  private String addid;
  private String sptype;
  private String billtype;
  private String bill;
  private String pay;
  private String post;
  private String total;
  private String remark;
  private String time;
  private String status;
  private Long orderid;
  private Integer bomid;
  private String kfDsc;
  private String urgentOrder;
  
  private String stime;
  private String etime;
  
  private String uname;
  
  private int searchType;
  
  private Long orid;
  

public Long getOrid() {
	return orid;
}

public void setOrid(Long orid) {
	this.orid = orid;
}

public int getSearchType() {
	return searchType;
}

public void setSearchType(int searchType) {
	this.searchType = searchType;
}

public String getUname() {
	return uname;
}

public void setUname(String uname) {
	this.uname = uname;
}

public String getStime() {
	return stime;
}

public void setStime(String stime) {
	this.stime = stime;
}

public String getEtime() {
	return etime;
}

public void setEtime(String etime) {
	this.etime = etime;
}

public String getUrgentOrder() {
	return urgentOrder;
}

public void setUrgentOrder(String urgentOrder) {
	this.urgentOrder = urgentOrder;
}

public String getKfDsc() {
	return kfDsc;
}

public void setKfDsc(String kfDsc) {
	this.kfDsc = kfDsc;
}

public Boolean getShoppingTypeCollect()
  {
    return this.shoppingTypeCollect;
  }
  
  public void setShoppingTypeCollect(Boolean shoppingTypeCollect)
  {
    this.shoppingTypeCollect = shoppingTypeCollect;
  }
  
  public String getConsignee()
  {
    return this.consignee;
  }
  
  public void setConsignee(String consignee)
  {
    this.consignee = consignee;
  }
  
  public String getOrderTime()
  {
    return this.orderTime;
  }
  
  public void setOrderTime(String orderTime)
  {
    this.orderTime = orderTime;
  }
  
  public Byte[] getBaid()
  {
    return this.baid;
  }
  
  public void setBaid(Byte[] baid)
  {
    this.baid = baid;
  }
  
  public Byte getBid()
  {
    return this.bid;
  }
  
  public void setBid(Byte bid)
  {
    this.bid = bid;
  }
  
  public Byte getBankId()
  {
    return this.bankId;
  }
  
  public void setBankId(Byte bankId)
  {
    this.bankId = bankId;
  }
  
  public String getDeposit()
  {
    return this.deposit;
  }
  
  public void setDeposit(String deposit)
  {
    this.deposit = deposit;
  }
  
  public String getAccountName()
  {
    return this.accountName;
  }
  
  public void setAccountName(String accountName)
  {
    this.accountName = accountName;
  }
  
  public String getAccountNumber()
  {
    return this.accountNumber;
  }
  
  public void setAccountNumber(String accountNumber)
  {
    this.accountNumber = accountNumber;
  }
  
  public Integer getExId()
  {
    return this.exId;
  }
  
  public void setExId(Integer exId)
  {
    this.exId = exId;
  }
  
  public String getExName()
  {
    return this.exName;
  }
  
  public void setExName(String exName)
  {
    this.exName = exName;
  }
  
  public String getExUrl()
  {
    return this.exUrl;
  }
  
  public void setExUrl(String exUrl)
  {
    this.exUrl = exUrl;
  }
  
  public String getExDesc()
  {
    return this.exDesc;
  }
  
  public void setExDesc(String exDesc)
  {
    this.exDesc = exDesc;
  }
  
  public String getKeyword()
  {
    return this.keyword;
  }
  
  public void setKeyword(String keyword)
  {
    this.keyword = keyword;
  }
  
  public Integer getId()
  {
    return this.id;
  }
  
  public void setId(Integer id)
  {
    this.id = id;
  }
  
  public String getOrderSn()
  {
    return this.orderSn;
  }
  
  public void setOrderSn(String orderSn)
  {
    this.orderSn = orderSn;
  }
  
  public String getDiscount()
  {
    return this.Discount;
  }
  
  public void setDiscount(String discount)
  {
    this.Discount = discount;
  }
  
  public Integer[] getMid()
  {
    return this.mid;
  }
  
  public void setMid(Integer[] mid)
  {
    this.mid = mid;
  }
  
  public int getPage()
  {
    return this.page;
  }
  
  public void setPage(int page)
  {
    this.page = page;
  }
  
  public Long getUid()
  {
    return this.uid;
  }
  
  public void setUid(Long uid)
  {
    this.uid = uid;
  }
  
  public Long getGId()
  {
    return this.GId;
  }
  
  public void setGId(Long gId)
  {
    this.GId = gId;
  }
  
  public Long getGnum()
  {
    return this.gnum;
  }
  
  public void setGnum(Long gnum)
  {
    this.gnum = gnum;
  }
  
  public String getPack()
  {
    return this.pack;
  }
  
  public void setPack(String pack)
  {
    this.pack = pack;
  }
  
  public Long getCupon()
  {
    return this.cupon;
  }
  
  public void setCupon(Long cupon)
  {
    this.cupon = cupon;
  }
  
  public String getCtids()
  {
    return this.ctids;
  }
  
  public void setCtids(String ctids)
  {
    this.ctids = ctids;
  }
  
  public Long getCtid()
  {
    return this.ctid;
  }
  
  public void setCtid(Long ctid)
  {
    this.ctid = ctid;
  }
  
  public String getData()
  {
    return this.data;
  }
  
  public void setData(String data)
  {
    this.data = data;
  }
  
  public String getGids()
  {
    return this.gids;
  }
  
  public void setGids(String gids)
  {
    this.gids = gids;
  }
  
  public String getAddid()
  {
    return this.addid;
  }
  
  public void setAddid(String addid)
  {
    this.addid = addid;
  }
  
  public String getSptype()
  {
    return this.sptype;
  }
  
  public void setSptype(String sptype)
  {
    this.sptype = sptype;
  }
  
  public String getBill()
  {
    return this.bill;
  }
  
  public void setBill(String bill)
  {
    this.bill = bill;
  }
  
  public String getBilltype()
  {
    return this.billtype;
  }
  
  public void setBilltype(String billtype)
  {
    this.billtype = billtype;
  }
  
  public String getPay()
  {
    return this.pay;
  }
  
  public void setPay(String pay)
  {
    this.pay = pay;
  }
  
  public String getPost()
  {
    return this.post;
  }
  
  public void setPost(String post)
  {
    this.post = post;
  }
  
  public String getTotal()
  {
    return this.total;
  }
  
  public void setTotal(String total)
  {
    this.total = total;
  }
  
  public String getRemark()
  {
    return this.remark;
  }
  
  public void setRemark(String remark)
  {
    this.remark = remark;
  }
  
  public String getTime()
  {
    return this.time;
  }
  
  public void setTime(String time)
  {
    this.time = time;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public void setStatus(String status)
  {
    this.status = status;
  }
  
  public Long getOrderid()
  {
    return this.orderid;
  }
  
  public void setOrderid(Long orderid)
  {
    this.orderid = orderid;
  }
  
  public Integer getBomid()
  {
    return this.bomid;
  }
  
  public void setBomid(Integer bomid)
  {
    this.bomid = bomid;
  }
}
