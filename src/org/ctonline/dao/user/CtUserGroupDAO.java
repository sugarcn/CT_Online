package org.ctonline.dao.user;

import java.util.List;

import org.ctonline.po.user.CtUserGroup;
import org.ctonline.po.user.CtUserGroupRelation;
import org.ctonline.po.user.CtUserManageRelation;
import org.ctonline.util.Page;

public interface CtUserGroupDAO {
	public Long save(CtUserGroup userGroup);//淇濆瓨
	public CtUserGroup findById(Long id);//鏍规嵁id鏌ユ壘
	public List<CtUserGroup> loadAll(Page page);//鍒嗛〉鎵惧嚭鎵�湁
	public int delete(Long id);//鍒犻櫎涓�潯璁板綍
	public Long totalCount(String str);//缁熻鏉℃暟
	public void update(CtUserGroup userGroup);//鏇存柊
	public List<CtUserGroup> findAll(String keyword,Page page);//鏍规嵁鏉′欢妫�储
	//保存分配客户组所属客户经理
	public Long saveBelongToManager(CtUserManageRelation cmr);
	//根据UGID和UTpye查询用户组或者用户所属的客户经理
	public List<?> getCManageRelation(Long UGID,String UTpye,Integer roleId);
	//根据UGID和UTpye验证是否存在所属的客户经理
	public CtUserManageRelation getUserManageRelation(Long UGID,String UTpye);
	//修改保存分配客户组所属客户经理
	public void updateUserManageRelation(Long UGID,String UTpye,Integer MId);
     
}
