package org.ctonline.dao.user;

import java.util.List;

import org.ctonline.po.views.ViewUserGroup;


public interface ViewUserGroupDAO {
	
	public List<ViewUserGroup>  query(String username,String useraccount);

}
