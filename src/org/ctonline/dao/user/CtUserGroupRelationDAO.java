package org.ctonline.dao.user;

import java.util.List;

import org.ctonline.po.user.CtUserGroupRelation;


public interface CtUserGroupRelationDAO {
	//保存用户到用户组
	public Long save(CtUserGroupRelation groupRelation);
	//根据用户组id查询用户
	public List<CtUserGroupRelation>  selectAll(Long UGId);
	//查询用户个数
	public Long totalCount(Long id);
	//删除关联表中重复用户
	public int delete(Long id);

}
