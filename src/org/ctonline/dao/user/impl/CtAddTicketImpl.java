package org.ctonline.dao.user.impl;

import java.util.List;

import org.ctonline.dao.basic.CtRegionDAO;
import org.ctonline.dao.basic.CtUserRankDAO;
import org.ctonline.dao.user.CtAddTicketDAO;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.basic.CtUserRank;
import org.ctonline.po.user.CtAddTicket;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtAddTicketImpl implements CtAddTicketDAO {
	private SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<CtAddTicket> loadAll(Page page) {
		// TODO Auto-generated method stub
		List<CtAddTicket> ctAddTickets = null;
		Session session=this.sessionFactory.openSession();
		String hql = "from CtAddTicket";
		Query query = session.createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		ctAddTickets = query.list();
		colseSession(session);
		return ctAddTickets;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
	
	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtUser cu,CtAddTicket ct where cu.UId=ct.UId";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public CtAddTicket findById(Long id) {
		// TODO Auto-generated method stub
		Session session =this.sessionFactory.openSession();
		CtAddTicket addTicket = (CtAddTicket) session.get(CtAddTicket.class, id);
		colseSession(session);
		return addTicket;
	}

	@Override
	public List<CtAddTicket> loadOne(Long id) {
		// TODO Auto-generated method stub
		List<CtAddTicket> ctAddTickets = null;
		Session session=this.sessionFactory.openSession();
		String hql = "from CtAddTicket ct where ct.UId="+id;
		Query query = session.createQuery(hql);
		ctAddTickets =query.list();
		colseSession(session);
		return ctAddTickets;
	}

	@Override
	public void update(CtAddTicket ctAddTicket) {
		// TODO Auto-generated method stub
		Session session = this.getSessionFactory().openSession();
		session.update(ctAddTicket);
		session.beginTransaction().commit();
		colseSession(session);
		
	}

	@Override
	public Long save(CtAddTicket addTicket) {
		// TODO Auto-generated method stub
		Session session = this.getSessionFactory().openSession();
		Long id = (Long) session.save(addTicket);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public CtAddTicket getTicketByUId(Long UId) {
		// TODO Auto-generated method stub
		Session session = this.getSessionFactory().openSession();
		String hql = "from CtAddTicket ct where ct.UId="+UId;
		CtAddTicket ctAddTicket = (CtAddTicket) session.createQuery(hql).uniqueResult();
		colseSession(session);
		return ctAddTicket;
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		Session session = this.getSessionFactory().openSession();
		String hql = "delete from CtAddTicket ct where ct.UId="+id;
		Query query = session.createQuery(hql);
		int d = query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
		return d;
	}

	public List<CtAddTicket>  allPass(String isPass,Page page){
		
		List<CtAddTicket> ctAddTickets = null;
		Session session=this.sessionFactory.openSession();
		String hql = "from CtAddTicket ct where ct.isPass="+isPass;
		Query query = session.createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(" and ct.isPass="+isPass));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		ctAddTickets = query.list();
		colseSession(session);
		return ctAddTickets;
		
		
		
	}
	public List<CtAddTicket>  notAllPass(String isPass,Page page){
		List<CtAddTicket> ctAddTickets = null;
		Session session=this.sessionFactory.openSession();
		String hql = "from CtAddTicket ct where ct.isPass="+isPass;
		Query query = session.createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(" and ct.isPass="+isPass));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		ctAddTickets = query.list();
		colseSession(session);
		return ctAddTickets;
		
		
	}
	

	
}
