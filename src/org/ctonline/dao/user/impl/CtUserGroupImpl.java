package org.ctonline.dao.user.impl;

import java.util.List;

import org.ctonline.dao.user.CtUserGroupDAO;
import org.ctonline.po.manager.CtManager;
import org.ctonline.po.user.CtUserGroup;
import org.ctonline.po.user.CtUserGroupRelation;
import org.ctonline.po.user.CtUserManageRelation;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtUserGroupImpl implements CtUserGroupDAO{
	private SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
	@Override
	public Long save(CtUserGroup userGroup) {
		// TODO Auto-generated method stub
		Long id;
		Session session=this.sessionFactory.openSession();
		id =(Long)session.save(userGroup);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public CtUserGroup findById(Long id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		CtUserGroup userGroup = (CtUserGroup)session.get(CtUserGroup.class, id);
		colseSession(session);
		return userGroup;//get or load
	}
    
	@SuppressWarnings("unchecked")
	@Override
	public List<CtUserGroup> loadAll(Page page) {
		// TODO Auto-generated method stub
		List< CtUserGroup> userGroup=null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from CtUserGroup order by UGName");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		userGroup=query.list();
		colseSession(session);
		return userGroup;
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		int d;
		String hql="delete CtUserGroup as u where u.id=?";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		query.setParameter(0, id);
		d= query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
		return d;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtUserGroup";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public void update(CtUserGroup userGroup) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(userGroup);
		session.beginTransaction().commit();
		colseSession(session);
	}
    
	@SuppressWarnings("unchecked")
	@Override
	public List<CtUserGroup> findAll(String keyword,Page page) {
		// TODO Auto-generated method stub
		List< CtUserGroup> userGroup=null;
		String sql="from CtUserGroup";
		String str="";
		if (!keyword.equals("������ؼ���") && !keyword.equals("")){
			str+=" where UGName like '%"+keyword+"%' ";
		}
		sql+=str;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		userGroup=query.list();
		colseSession(session);
		return userGroup;
	}

	@Override
	public Long saveBelongToManager(CtUserManageRelation cmr) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.save(cmr);
		session.beginTransaction().commit();
		colseSession(session);
		return null;
	}

	@Override
	public List<?> getCManageRelation(Long UGID, String UTpye,Integer roleId) {
		// TODO Auto-generated method stub
		List<?> cmAndCumr;
		Session session=this.sessionFactory.openSession();	
		String sql = "SELECT a.mid1,a.MManagerId ,CT_USER_MANAGE_RELATION.M_ID mid2 FROM(SELECT  CT_MANAGER.M_ID mid1,CT_MANAGER.M_MANAGER_ID MManagerId FROM CT_MANAGER  WHERE CT_MANAGER.ROLE_ID ="+ roleId+") a LEFT JOIN CT_USER_MANAGE_RELATION"+
				" on a.mid1 = CT_USER_MANAGE_RELATION.M_ID AND CT_USER_MANAGE_RELATION.U_ID ="+ UGID+ " and CT_USER_MANAGE_RELATION.U_TYPE = "+UTpye;
		Query query = session.createSQLQuery(sql);
		//query.setParameter(0, UGID);
		//query.setParameter(1, UTpye);
		cmAndCumr = query.list();
		colseSession(session);
		return cmAndCumr;
	}

	@Override
	public CtUserManageRelation getUserManageRelation(Long UGID, String UTpye) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();	
		String hql = "from CtUserManageRelation cumr where cumr.UId="+UGID+" and cumr.UType="+UTpye;
		Query query = session.createQuery(hql);
		//query.setParameter(0, UGID);
		//query.setParameter(1, UTpye);
		CtUserManageRelation cumr = (CtUserManageRelation) query.uniqueResult();
		colseSession(session);
		return cumr;
	}

	@Override
	public void updateUserManageRelation(Long UGID, String UTpye, Integer MId) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();	
		String hql = "update CtUserManageRelation cumr set cumr.MId = ? where cumr.UId=? and cumr.UType=?";
		Query query = session.createQuery(hql);
		query.setParameter(0, Long.parseLong(MId.toString()));
		query.setParameter(1, UGID);
		query.setParameter(2, UTpye);
		query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
	}
}
