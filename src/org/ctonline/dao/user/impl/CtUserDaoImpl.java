package org.ctonline.dao.user.impl;

import java.util.List;

import org.ctonline.dao.user.CtUserDao;
import org.ctonline.po.basic.CtLoginLog;
import org.ctonline.po.user.CtContact;
import org.ctonline.po.user.CtDraw;
import org.ctonline.po.user.CtSms;
import org.ctonline.po.user.CtUesrClient;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.util.MD5Util;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtUserDaoImpl implements CtUserDao {
	private SessionFactory sessionFactory;
	

	@Override
	public CtUser getCtUser(CtUser ctUser) {
		// TODO Auto-generated method stub
		Session session = this.getSessionFactory().openSession();
		return null;
	}


	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}


	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	@Override
	public CtUser getCtUserByUUserid(String UUserid) {
		// TODO Auto-generated method stub
		Session session = this.getSessionFactory().openSession();
//		String hql = "from CtUser cu where cu.UUsername='"+UUsername+"'";
		String hql = "from CtUser cu where cu.UUserid=?";
		Query query= session.createQuery(hql);
		query.setParameter(0, UUserid);
		List<CtUser> ctUsers =query.list();
		CtUser ctUser = new CtUser();
		if(ctUsers.size() <= 0){
			ctUser = null;
		}else {
			ctUser = ctUsers.get(0);
		}
		colseSession(session);
		return ctUser;
	}


	@Override
	public Long saveCtSms(CtSms ctSms) {
		// TODO Auto-generated method stub
		Long id;
		Session session = this.getSessionFactory().openSession();
		id = (Long) session.save(ctSms);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}


	@Override
	public CtUser getCtUserByUMb(String UMb) {
		// TODO Auto-generated method stub
		Session session = this.getSessionFactory().openSession();
		String hql = "from CtUser cu where cu.UMb=?";
		Query query= session.createQuery(hql);
		query.setParameter(0, UMb);
		List<CtUser> ctUsers =query.list();
		CtUser ctUser = new CtUser();
		if(ctUsers.size() <= 0){
			ctUser = null;
		}else {
			ctUser = ctUsers.get(0);
		}
		colseSession(session);
		return ctUser;
	}


	@Override
	public CtSms getCtSmsByUMb(String UMb) {
		// TODO Auto-generated method stub
		Session session = this.getSessionFactory().openSession();
		String hql = "from CtSms cs where cs.UMb=? order by cs.smsTime desc";
		Query query = session.createQuery(hql);
		query.setParameter(0, UMb);
		CtSms cs = new CtSms();
		List<CtSms> ctSms = query.list();
		if(ctSms.size()<=0){
			cs = null;
		}else {
			cs = ctSms.get(0);
		}
		colseSession(session);
		return cs;
	}


	@Override
	public Long save(CtUser ctUser) {
		// TODO Auto-generated method stub
		Session session = this.getSessionFactory().openSession();
		Long id = (Long) session.save(ctUser);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}


	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
	@Override
	public CtUser getCtUserByUUnameOrUMb(String arg0) {
		// TODO Auto-generated method stub
		Session session = this.getSessionFactory().openSession();
		String hql = "from CtUser cu where cu.UUsername =? or cu.UMb=?";
		Query query = session.createQuery(hql);
		query.setParameter(0, arg0);
		query.setParameter(1, arg0);
		CtUser ctUser = new CtUser();
		List<CtUser> ctUsers =query.list();
		if(ctUsers != null && ctUsers.size()>0){
			ctUser = (CtUser) query.list().get(0);
		}
		colseSession(session);
		return ctUser;
	}


	@Override
	public void update(CtUser ctUser) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(ctUser);
		session.beginTransaction().commit();
		colseSession(session);
	}


	@Override
	public CtUser affirm(String uname,String password) throws Exception {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		String passwdorg = "CT"+uname+password;
		String encryptPwd = MD5Util.getEncryptPasswd(passwdorg);
		String hql = "from CtUser cu where cu.UUserid =? and cu.UPassword=?";
		Query query = session.createQuery(hql);
		query.setParameter(0, uname);
		query.setParameter(1, encryptPwd);
		CtUser ctUser = new CtUser();
		List<CtUser> ctUsers =query.list();
		if(ctUsers.size() <= 0){
			ctUser = null;
		}else {
			ctUser = ctUsers.get(0);
		}
		colseSession(session);
		return ctUser;
	}


	@Override
	public void editCtuser(CtUser ctUser) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(ctUser);
		session.beginTransaction().commit();
		colseSession(session);
	}
	@Override
	public List<CtUser> findAll(String keyword) {
		// TODO Auto-generated method stub
		List< CtUser> user=null;
		String sql="from CtUser";
		String str="";
		if (keyword !=null && !keyword.equals("")){
			str+=" where UUsername like '%"+keyword+"%' ";
		}
		//str+=" order by regionID desc";
		sql+=str;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		
		user=query.list();
		colseSession(session);
		return user;
	}
	
	public List<?>  selectAll(Long UGId){
		
		List<?> user=null;
		String sql="select a.UId , a.UUsername  from CtUser a , CtUserGroupRelation b where a.UId = b.UId and b.UGId="+UGId;
		//str+=" order by regionID desc";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		
		user=query.list();
		colseSession(session);
		return user;
		
	}


	@Override
	public List<CtUser> loadAll(Page page) {
		// TODO Auto-generated method stub
		List<CtUser> users = null;
		Session session=this.sessionFactory.openSession();
		String hql = "from CtUser u order by u.UId desc";
		Query query = session.createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		users = query.list();
		colseSession(session);
		return users;
	}


	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtUser";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}


	@Override
	public CtUser getCtUserByUEmail(String UEmail) {
		// TODO Auto-generated method stub
		Session session = this.getSessionFactory().openSession();
		String hql = "from CtUser cu where cu.UEmail=?";
		Query query= session.createQuery(hql);
		query.setParameter(0, UEmail);
		List<CtUser> ctUsers =query.list();
		CtUser ctUser = new CtUser();
		if(ctUsers.size() <= 0){
			ctUser = null;
		}else {
			ctUser = ctUsers.get(0);
		}
		colseSession(session);
		return ctUser;
	}


	@Override
	public CtUser getCtuserByUUseridAndUCode(String UUserid, String UCode) {
		// TODO Auto-generated method stub
		Session session = this.getSessionFactory().openSession();
		String hql = "from CtUser cu where cu.UUserid=? and cu.UCode=?";
		Query query= session.createQuery(hql);
		query.setParameter(0, UUserid);
		query.setParameter(1, UCode);
		List<CtUser> ctUsers =query.list();
		CtUser ctUser = new CtUser();
		if(ctUsers.size() <= 0){
			ctUser = null;
		}else {
			ctUser = ctUsers.get(0);
		}
		colseSession(session);
		return ctUser;
	}


	@Override
	public CtUser affimByEmailAndPwd(String email, String password) throws Exception {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		CtUser cu = getCtUserByUEmail(email);
		String passwdorg = "CT"+cu.getUUserid()+password;
		String encryptPwd = MD5Util.getEncryptPasswd(passwdorg);
		String hql = "from CtUser cu where cu.UEmail =? and cu.UPassword=?";
		Query query = session.createQuery(hql);
		query.setParameter(0, email);
		query.setParameter(1, encryptPwd);
		CtUser ctUser = new CtUser();
		List<CtUser> ctUsers =query.list();
		colseSession(session);
		if(ctUsers.size() <= 0){
			ctUser = null;
		}else {
			ctUser = ctUsers.get(0);
		}
		return ctUser;
	}


	@Override
	public CtUser getCtUserByUId(Long UId) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.openSession();
		String hql = "from CtUser cu where cu.UId=?";
		Query query = session.createQuery(hql);
		query.setParameter(0, UId);
		CtUser ctUser;
		List<CtUser> ctUsers =query.list();
		colseSession(session);
		if(ctUsers.size() <= 0){
			ctUser = null;
		}else {
			ctUser = ctUsers.get(0);
		}
		return ctUser;
	}


	@Override
	public List<CtUser> getCtUserByPage(Page page) {
		List<CtUser> users = null;
		Session session=this.sessionFactory.openSession();
		String hql = " from CtUser u where u.ULastTime is not null order by u.ULastTime desc";
		Query query = session.createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCountChu(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		users = query.list();
		colseSession(session);
		return users;
	}


	private Long totalCountChu(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtUser u where u.ULastTime is not null";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}


	@Override
	public List<CtContact> findContactByPage(Page page) {
		String hql = " from CtContact cc order by cc.conId desc";
		Session session =this.sessionFactory.openSession();
		Query query = session.createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCountContact(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		List<CtContact> list = query.list();
		if(list != null && list.size() > 0){
			return list;
		}
		return null;
	}


	private Long totalCountContact(String str) {
		String hql="select count(*) from CtContact ";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}


	@Override
	public List<CtDraw> findDrawByPage(Page page) {
		String hql = " from CtDraw cc order by cc.drId desc";
		Session session =this.sessionFactory.openSession();
		Query query = session.createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCountDraw(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		List<CtDraw> list = query.list();
		if(list != null && list.size() > 0){
			colseSession(session);
			return list;
		}
		colseSession(session);
		return null;
	}


	private Long totalCountDraw(String str) {
		String hql="select count(*) from CtDraw ";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}


	@Override
	public CtDraw findDrawByDrId(Integer drId) {
		String hql = " from CtDraw cc where cc.drId="+drId;
		Session session =this.sessionFactory.openSession();
		Query query = session.createQuery(hql);
		List<CtDraw> list = query.list();
		if(list != null && list.size() > 0){
			colseSession(session);
			return list.get(0);
		}
		colseSession(session);
		return null;
	}


	@Override
	public void updateDraw(CtDraw draw) {
		Session session =this.sessionFactory.openSession();
		session.merge(draw);
		session.beginTransaction().commit();
		colseSession(session);
	}


	@Override
	public List<CtUser> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List< CtUser> user=null;
		String sql="from CtUser";
		String str="";
		if (keyword !=null && !keyword.equals("")){
			str+=" where UUsername like '%"+keyword+"%' ";
		}
		str+=" order by UId desc";
		sql+=str;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCountUserByKey(str));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		user=query.list();
		colseSession(session);
		return user;
	}


	private Long totalCountUserByKey(String str) {
		Session session=this.sessionFactory.openSession();
		Query query = session.createQuery("select count(*) from CtUser " + str);
		Object count = query.uniqueResult();
		colseSession(session);
		return Long.valueOf(count+"");
	}


	@Override
	public void saveLoginCredit(CtLoginLog loginLog) {
		Session session=this.sessionFactory.openSession();
		session.merge(loginLog);
		session.beginTransaction().commit();
		colseSession(session);
	}


	@Override
	public void updateUserCus(Long uId) {
		Session session=this.sessionFactory.openSession();
		CtUesrClient client = new CtUesrClient();
		client.setCdefult("0");
		client.setCname("原账户地址");
		client.setUid(uId);
		client = (CtUesrClient) session.merge(client);

		session.beginTransaction().commit();
		
		
		String hql = " update CtUser cu set cu.UIsCustomer=1 where cu.UId=" + uId;
		Query query=session.createQuery(hql);
		
		query.executeUpdate();
		session.beginTransaction().commit();
		
		String hqlAdd = " update CtUserAddress cua set cua.ACustomer=1, cua.cid="+client.getCid()+" where cua.UId=" + uId;
		Query query1=session.createQuery(hqlAdd);
		query1.executeUpdate();
		session.beginTransaction().commit();
		
		
		
		
		
		colseSession(session);
	}


	@Override
	public List<CtUserAddress> findAllAddressByUid(Long uId, Page page, Integer cid) {
		Session session=this.sessionFactory.openSession();
		
		String str = "";
		if(cid != null && cid != -1){
			str += " and cua.cid=" + cid;
		}
		
		String hql = " from CtUserAddress cua where cua.UId=" + uId + str + " order by cua.ACustomer desc, cua.AId desc";
		Query query=session.createQuery(hql);
		
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalAddressByUid(uId + str));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		
		List<CtUserAddress> list = query.list();
		if(list != null && list.size() > 0){
			return list;
		}
		return null;
	}


	private Long totalAddressByUid(String uId) {
		Session session=this.sessionFactory.openSession();
		String hql = " select count(*) from CtUserAddress cua where cua.UId=" + uId;
		Query query=session.createQuery(hql);
		Long count = (Long) query.uniqueResult();
		colseSession(session);
		return count;
	}


	@Override
	public void updateCusAddressByAid(Long cid, Long uid) {
		Session session=this.sessionFactory.openSession();
		String hqlAdd = " update CtUserAddress cua set cua.ACustomer=1 where cua.UId=" + uid;
		Query query1=session.createQuery(hqlAdd);
		query1.executeUpdate();
		session.beginTransaction().commit();
		
		String hqlUp = " update CtUserAddress cua set cua.ACustomer=2 where cua.cid=" + cid;
		Query query=session.createQuery(hqlUp);
		query.executeUpdate();
		session.beginTransaction().commit();
		
		hqlUp = " update CtUesrClient cuc set cuc.cdefult=0 where cuc.uid=" + uid;
		query=session.createQuery(hqlUp);
		query.executeUpdate();
		session.beginTransaction().commit();
		
		hqlUp = " update CtUesrClient cuc set cuc.cdefult=1 where cuc.cid=" + cid;
		query=session.createQuery(hqlUp);
		query.executeUpdate();
		session.beginTransaction().commit();
		
		colseSession(session);
	}


	@Override
	public List<CtUesrClient> findCusMent(Long uId, Page page) {
		Session session=this.sessionFactory.openSession();
		String hql = " from CtUesrClient cua where cua.uid=" + uId + " and cua.cname is not null order by cua.cdefult desc";
		Query query=session.createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalClient(uId));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		List<CtUesrClient> list = query.list();
		if(list != null && list.size() > 0){
			colseSession(session);
			return  list;
		}
		return null;
	}


	private Long totalClient(Long uId) {
		Session session=this.sessionFactory.openSession();
		String hql = "select count(*) from CtUesrClient cua where cua.uid=" + uId + " and cua.cname is not null order by cua.cdefult desc";
		Query query=session.createQuery(hql);
		return (Long) query.uniqueResult();
	}


	@Override
	public CtUesrClient findClientByCName(String userName, Long uid) {
		Session session=this.sessionFactory.openSession();
		String hql = " from CtUesrClient cuc where cuc.cname='"+userName+"' and cuc.uid=" + uid;
		Query query = session.createQuery(hql);
		List<CtUesrClient> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}


	@Override
	public void saveClent(CtUesrClient userClient) {
		Session session=this.sessionFactory.openSession();
		session.merge(userClient);
		session.beginTransaction().commit();
		colseSession(session);
	}


	@Override
	public void updateClientName(Long m, Integer cid, Long uid, String ACustomer) {
		Session session=this.sessionFactory.openSession();

		
		if(ACustomer.equals("2")){
			
		}
		
		String hql = " update CtUserAddress cua set cua.cid=" + cid + ", cua.ACustomer='"+1+"' where cua.AId=" + m;
		Query query = session.createQuery(hql);
		query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
	}


	@Override
	public CtUserAddress findCusAddressIsDefultByUid(Long uid) {
		Session session=this.sessionFactory.openSession();
		String hql = " from CtUserAddress cua where cua.UId=" + uid +" and cua.ACustomer=2";
		Query query = session.createQuery(hql);
		List<CtUserAddress> list = query.list();
		if(list != null && list.size() > 0){
			String hqlAdd = " update CtUserAddress cua set cua.ACustomer=1 where cua.UId=" + uid;
			Query query1=session.createQuery(hqlAdd);
			query1.executeUpdate();
			session.beginTransaction().commit();
			colseSession(session);
			return list.get(0);
		}
		return null;
	}


	@Override
	public void deleteClientByCid(Integer cid) {
		Session session=this.sessionFactory.openSession();
		String hql = " update CtUserAddress cua set cua.ACustomer=0, cua.cid='' where cua.cid=" + cid;
		session.createQuery(hql).executeUpdate();
		session.beginTransaction().commit();
		
		hql = " delete from CtUesrClient cua where cua.cid=" + cid;
		session.createQuery(hql).executeUpdate();
		session.beginTransaction().commit();
		
		colseSession(session);
	}
}
