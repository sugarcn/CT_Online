package org.ctonline.dao.user.impl;

import java.util.List;

import org.ctonline.dao.user.CtUserGroupRelationDAO;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserGroupRelation;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtUserGroupRelationImpl implements CtUserGroupRelationDAO{
	
	private SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
	@Override
	public Long save(CtUserGroupRelation groupRelation){
		
		Long id=0l;
		Session session=this.sessionFactory.openSession();
		session.save(groupRelation);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
		
	}
	
	@Override
public List<CtUserGroupRelation>  selectAll(Long UGId){
		
		List< CtUserGroupRelation> groupRelation=null;
		String sql="from CtUserGroupRelation where  UGId="+UGId;
		//str+=" order by regionID desc";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		
		groupRelation=query.list();
		colseSession(session);
		return groupRelation;
		
	}
	
	@Override
		
		public Long totalCount(Long id) {
			// TODO Auto-generated method stub
			String hql="select count(*) from CtUserGroupRelation where UId="+id;
			Session session =this.sessionFactory.openSession();
			Long count= (Long)session.createQuery(hql).uniqueResult();
			colseSession(session);
			return count;
		}
	@Override
	public int delete(Long id){
		int d;
		String hql="delete CtUserGroupRelation as u where u.UGId="+id;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		d= query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
		return d;
		
		
	}
		
	}
	

