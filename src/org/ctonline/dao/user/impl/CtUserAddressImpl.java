package org.ctonline.dao.user.impl;

import java.util.List;

import org.ctonline.dao.user.CtUserAddressDAO;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtUserAddressImpl implements CtUserAddressDAO{
	
private SessionFactory sessionFactory;
	
	public SessionFactory getsessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
	@Override
	public Long save(CtUserAddress address) {
		// TODO Auto-generated method stub
		Long id;
		Session session=this.sessionFactory.openSession();
		id =(Long)session.save(address);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public CtUserAddress findById(Long id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		CtUserAddress address = (CtUserAddress)session.get(CtUserAddress.class, id);
		colseSession(session);
		return address;
	}

	@Override
	public List<CtUserAddress> loadAll(Page page) {
		// TODO Auto-generated method stub
		List< CtUserAddress> types=null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from CtUserAddress");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		types=query.list();
		colseSession(session);
		return types;
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		int d;
		String hql="delete CtUserAddress as u where u.id=?";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		query.setParameter(0, id);
		d= query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
		return d;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtUserAddress";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public void update(CtUserAddress address) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(address);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public List<CtUserAddress> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List< CtUserAddress > types=null;
		String sql="from CtUserAddress";
		String str="";
		if (!keyword.equals("请输入你要查询的关键词") && !keyword.equals("")){
			str+=" where address like '%"+keyword+"%' ";
		}
		//str+=" order by regionID desc";
		sql+=str;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		types=query.list();
		colseSession(session);
		return types;
	}

	@Override
	public List<CtRegion> queryById(Long id){
		List< CtRegion> region=null;
		String sql="from CtRegion  where parentId="+id;
	Session session=this.sessionFactory.openSession();
	Query query= session.createQuery(sql);
	region=query.list();
	colseSession(session);
	return region;
		
	}


}
