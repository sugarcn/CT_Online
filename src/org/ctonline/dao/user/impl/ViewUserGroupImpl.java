package org.ctonline.dao.user.impl;

import java.util.List;

import org.ctonline.dao.user.ViewUserGroupDAO;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.views.ViewUserGroup;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class ViewUserGroupImpl implements ViewUserGroupDAO{
	
	private SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
	@Override
	public List<ViewUserGroup>  query(String username,String useraccount){
		Session session = this.getSessionFactory().openSession();
		String hql = " from ViewUserGroup a where a.userid like'%"+username+"%' or a.username like '%"+useraccount+"%'";
		Query query = session.createQuery(hql);
		List<ViewUserGroup> viewUser =query.list();
		colseSession(session);
		return viewUser;
	}

}
