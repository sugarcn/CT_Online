package org.ctonline.dao.user;

import java.util.List;

import org.ctonline.po.basic.CtLoginLog;
import org.ctonline.po.user.CtContact;
import org.ctonline.po.user.CtDraw;
import org.ctonline.po.user.CtSms;
import org.ctonline.po.user.CtUesrClient;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.po.user.CtUserGroup;
import org.ctonline.util.Page;

public interface CtUserDao {
	
	//��ѯһ���û�
	public CtUser getCtUser(CtUser ctUser); 
	//����û����ѯһ���û�
	public CtUser getCtUserByUUserid(String UUsername);
	//���������֤��
	public Long saveCtSms(CtSms ctSms);
	//����ֻ��ȥ��ѯһ���û�
	public CtUser getCtUserByUMb(String UMb);
	//����ֻ�Ų�ѯһ����֤��
	public CtSms getCtSmsByUMb(String UMb);
	//ע�Ტ�����û�
	public Long save(CtUser ctUser);
	//����û�������ֻ�Ų�ѯһ���û�
	public CtUser getCtUserByUUnameOrUMb(String arg0);
	//�޸�����
	public void update(CtUser ctUser);
	//��֤��¼�û��Ƿ�Ϸ�
	public CtUser affirm(String uname,String password) throws Exception;
	//�޸��û���Ϣ
	public void editCtuser(CtUser ctUser);
	//���չؼ��ֲ�ѯ�û�
	public  List<CtUser> findAll(String keyword);
	//����û���id��ѯ���û���������û�
	public List<?>  selectAll(Long UGId);
	//查询所有用户
	public List<CtUser> loadAll(Page page);
	public Long totalCount(String str);
	
	//根据UEmail获取一个用户
	public CtUser getCtUserByUEmail(String UEmail);
	//通过UUserid和Ucode查询一个用户
	public CtUser getCtuserByUUseridAndUCode(String UUserid,String UCode);
	//通过邮件地址和密码验证登录
	public CtUser affimByEmailAndPwd(String email,String password)throws Exception;
	//通过UId查询CtUser
	public CtUser getCtUserByUId(Long UId);
	public List<CtUser> getCtUserByPage(Page page);
	public List<CtContact> findContactByPage(Page page);
	public List<CtDraw> findDrawByPage(Page page);
	public CtDraw findDrawByDrId(Integer drId);
	public void updateDraw(CtDraw draw);
	public List<CtUser> findAll(String keyword, Page page);
	public void saveLoginCredit(CtLoginLog loginLog);
	public void updateUserCus(Long uId);
	public List<CtUserAddress> findAllAddressByUid(Long uId, Page page, Integer cid);
	public void updateCusAddressByAid(Long aid, Long uid);
	public List<CtUesrClient> findCusMent(Long uId, Page page);
	public CtUesrClient findClientByCName(String userName, Long uid);
	public void saveClent(CtUesrClient userClient);
	public void updateClientName(Long m, Integer cid, Long uid, String ACustomer);
	public CtUserAddress findCusAddressIsDefultByUid(Long uid);
	public void deleteClientByCid(Integer cid);
}
