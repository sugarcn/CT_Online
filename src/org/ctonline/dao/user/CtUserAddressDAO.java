package org.ctonline.dao.user;

import java.util.List;

import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.util.Page;

public interface CtUserAddressDAO {
	
	public Long save(CtUserAddress address);//保存
	public CtUserAddress findById(Long id);//根据id查找
	public List<CtUserAddress> loadAll(Page page);//分页找出所有
	public int delete(Long id);//删除一条记录
	public Long totalCount(String str);//统计条数
	public void update(CtUserAddress address);//更新
	public List<CtUserAddress> findAll(String keyword,Page page);//根据条件检索
	public List<CtRegion> queryById(Long id);

}
