package org.ctonline.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public abstract interface BaseHibernateDAO
{
  public abstract SessionFactory getSessionFactory();
  
  public abstract void setSessionFactory(SessionFactory paramSessionFactory);
  
  public abstract Session begin();
  
  public abstract Session getSession();
  
  public abstract Session getCurrentSession();
  
  public abstract void clossSession();
  
  public abstract void commit();
}
