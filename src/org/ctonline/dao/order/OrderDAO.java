package org.ctonline.dao.order;

import java.util.List;
import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.goods.CtBomGoods;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.order.CtComplain;
import org.ctonline.po.order.CtEvaluation;
import org.ctonline.po.order.CtExpress;
import org.ctonline.po.order.CtOrderGoods;
import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.po.order.CtOrderRelation;
import org.ctonline.po.pay.CtPayInterface;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.po.views.ViewOrderCheck;
import org.ctonline.util.Page;

public abstract interface OrderDAO
  extends BaseHibernateDAO
{
  public abstract List<CtGoods> getGoodsByGId(Long paramLong);
  
  public abstract CtUserAddress getAddress(Long paramLong);
  
  public abstract List<ViewOrderCheck> getOrderCheck(Long paramLong1, Long paramLong2);
  
  public abstract Long saveOrderInfo(CtOrderInfo paramCtOrderInfo);
  
  public abstract void saveOrderGoods(CtOrderGoods paramCtOrderGoods);
  
  public abstract String delCartGoods(Long paramLong1, Long paramLong2);
  
  public abstract String delCart(Long paramLong);
  
  public abstract List<CtOrderInfo> getOrderListByUId(Long paramLong, Page paramPage);
  
  public abstract List<CtOrderInfo> getOrderListByTime(Long paramLong, Page paramPage, String paramString);
  
  public abstract List<CtOrderInfo> getOrderListByStatus(Long paramLong, Page paramPage, String paramString);
  
  public abstract void changeStatus(Long paramLong, String paramString);
  
  public abstract int updateStartByOrderId(String paramString);
  
  public abstract List<?> getOrderGoods(Long paramLong);
  
  public abstract List<CtOrderInfo> getIsDis();
  
  public abstract List<CtBomGoods> getGoodsByBom(Integer paramInteger);
  
  public abstract CtOrderInfo getOrderByOrderSn(String paramString);
  
  public abstract List<CtOrderInfo> getAllOrder(Page paramPage,String OrderSn,String stime, String etime, Long uid, String stauts, int searchType);
  
  public abstract CtOrderInfo getOrderByOrderId(Long paramLong);
  
  public abstract List<CtOrderGoods> getOrderGoodsByOrderId(Long paramLong, int paramInt);
  
  public abstract void updateTotalPriceByOrderId(CtOrderInfo paramCtOrderInfo);
  
  public abstract void updateStartByOrderId(Long paramLong1, Long paramLong2);
  
  public abstract CtExpress findById(Integer paramInteger);
  
  public abstract List<CtExpress> getAllExpress(Page paramPage);
  
  public abstract int save(CtExpress paramCtExpress);
  
  public abstract int delete(Integer paramInteger);
  
  public abstract void update(CtExpress paramCtExpress);
  
  public abstract List<CtExpress> findAll(String paramString, Page paramPage);
  
  public abstract void updateExNoByOrderId(CtOrderInfo paramCtOrderInfo);
  
  public abstract CtEvaluation getEvaByOrderId(Long paramLong);
  
  public abstract void updateReturnStartByOrderId(Long paramLong, String paramString);
  
  public abstract void updateReturnManageByOrderId(Long paramLong, String paramString);
  
  public abstract void updateReturnFinanceByOrderId(Long paramLong, String paramString);
  
  public abstract List<CtComplain> getAllComplain(Page paramPage);
  
  public abstract CtComplain getComplainByComId(Integer paramInteger);
  
  public abstract void updateComplainByComId(CtComplain paramCtComplain);
  
  public abstract void updateuser(CtUser paramCtUser);
  
  public abstract String getOrderByshopId(Short paramShort);
  
  public abstract CtPayInterface getPayInterByOrderSn(String paramString);
  
  public abstract String getShengById(Integer paramInteger);
  
  public abstract String getShiById(Integer paramInteger);
  
  public abstract String getQuById(Integer paramInteger);

public abstract void updateOrderInfoByInfo(CtOrderInfo info);

public abstract List<CtOrderInfo> getOrderListByOrderSn(String orderSn);

public abstract List<CtOrderGoods> getordergoodsByOrderId(Long orderId);

public abstract void updateOrderGoodsByOrderId(Long orderId);

public abstract String getFirstDateOrder();

public abstract List<CtOrderInfo> getorderByUidAndPayType(Long uid,
		String string, Page page);

public abstract List<CtOrderRelation> findRelationsByUid(Long uid, Page page);

public abstract CtOrderRelation findRelatinsByOrid(Long orid);

public abstract void updateOrderGoodsByOidAndGidAndType(Long refGoodsid,
		Long refOrderid, String refGoodsType);
}
