package org.ctonline.dao.order.impl;

import java.util.List;
import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.order.ICtPayDAO;
import org.ctonline.po.order.CtOrderGoods;
import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.po.order.CtPay;
import org.ctonline.po.order.CtRefund;
import org.ctonline.po.pay.CtPayInterface;
import org.ctonline.util.Page;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

public class CtPayDAOImpl
  extends BaseHibernateDAOImpl
  implements ICtPayDAO
{
  public List<CtPay> findAll(Page page)
  {
    String hql = " from CtPay";
    Query query = getSession().createQuery(hql);
    if (page.getCurrentPage() != 0)
    {
      page.setTotalCount(totalCountForPay(null));
      query.setFirstResult(page.getFirstIndex());
      query.setMaxResults(page.getPageSize());
    }
    return query.list();
  }
  
  private Long totalCountForPay(Object object) {
	  String hql = "select count(*) from CtPay";
	  Query query = getSession().createQuery(hql);
	return (Long) query.uniqueResult();
}

public void saveOrderPay(CtPay payDTO)
  {
    getSession().save(payDTO);
  }
  
  public CtPay getPayByOrderId(Long orderId)
  {
    String hql = "from CtPay cp where cp.orderId = " + orderId;
    Query query = getSession().createQuery(hql);
    List<CtPay> list = query.list();
    if ((list != null) && (list.size() > 0)) {
      return (CtPay)list.get(0);
    }
    return null;
  }
  
  public Long totalCount(String str)
  {
    String hql = "select count(*) from CtUserGroup";
    if (str != null) {
      hql = hql + str;
    }
    Long count = (Long)getSession().createQuery(hql).uniqueResult();
    return count;
  }

@Override
public List<CtRefund> getRefundByOrderId(Long orderId) {
	try {
		String hql = " from CtRefund cr where cr.refOrderid = " + orderId;
		Query query = getSession().createQuery(hql);
		List<CtRefund> list = query.list();
		if(list != null && list.size() > 0){
			for (int k = 0; k < list.size(); k++)  //外循环是循环的次数
			{
				for (int j = list.size() - 1 ; j > k; j--)  //内循环是 外循环一次比较的次数
				{
					if (list.get(k).getRefOrderid().toString().equals(list.get(j).getRefOrderid().toString()) &&
							list.get(k).getRefGoodsid().toString().equals(list.get(j).getRefGoodsid().toString()) && 
							list.get(k).getRefGoodsType().toString().equals(list.get(j).getRefGoodsType().toString())
							)
					{
						list.remove(j);
					}
				}
			}
			return list;
		}
	} catch (HibernateException e) {
		e.printStackTrace();
	}
	return null;
}

@Override
public CtOrderGoods getOrderGoodsByRefund(Long refOrderid, Long gid, int type) {
	String hql = " from CtOrderGoods cog where cog.orderId = " + refOrderid + " and cog.GId=" + gid + " and cog.isParOrSim=" + type;
	Query query = getSession().createQuery(hql);
	List<CtOrderGoods> list = query.list();
	if(list != null && list.size() > 0){
		return list.get(0);
	}
	return null;
}

@Override
public CtRefund getRefundByOrderIdAndGidAndType(String gid, String orderId,
		String type) {
	String hql = " from CtRefund crf where crf.refGoodsid=" + gid + " and crf.refOrderid=" + orderId + " and crf.refGoodsType=" + type;
	Query query = getSession().createQuery(hql);
	List<CtRefund> list = query.list();
	if(list != null && list.size() > 0){
		if(list.size() > 1){
			for (int i = 1; i < list.size(); i++) {
				getSession().delete(list.get(i));
			}
		}
		return list.get(0);
	}
	return null;
}

@Override
public void updateRRefundOk(CtRefund ctRefund, String refund_idBack,
		String out_refund_noBack) {
	ctRefund.setRefCtrefid(out_refund_noBack);
	ctRefund.setRefPayId(refund_idBack);
	ctRefund.setRefStatus("2");
	getSession().update(ctRefund);
}

@Override
public void updateOrderInfo(CtOrderInfo ctOrderInfo) {
	getSession().merge(ctOrderInfo);
}

@Override
public void updateOrderGoods(CtOrderGoods ctOrderGoods) {
	getSession().merge(ctOrderGoods);
}

@Override
public CtPay findAllByOrderId(Long orderId) {
	String hql = " from CtPay c where c.orderId = "+orderId;
	Query query = getSession().createQuery(hql);
	List<CtPay> pays = query.list();
	if(pays != null && pays.size() > 0){
		return pays.get(0);
	}
	return null;
}

@Override
public CtPayInterface getPayInterByOrderSn(String orderSn) {
	String hql = " from CtPayInterface cpi where cpi.orderSn='"+ orderSn +"' order by cpi.PInId desc";
	Query query = getSession().createQuery(hql);
	List<CtPayInterface> pays = query.list();
	if(pays != null && pays.size() > 0){
		return pays.get(0);
	}
	return null;
}

@Override
public CtRefund findOPatIntById(String string) {
	String hql = " from CtRefund cr where cr.refId="+string;
	Query query = getSession().createQuery(hql);
	List<CtRefund> pays = query.list();
	if(pays != null && pays.size() > 0){
		return pays.get(0);
	}
	return null;
}

@Override
public void updateRef(CtRefund refunds) {
	getSession().merge(refunds);
}

@Override
public CtPay findPayByPId(String str) {
	String hql = " from CtPay cp where cp.payId="+str;
	Query query = getSession().createQuery(hql);
	List<CtPay> pays = query.list();
	if(pays != null && pays.size() > 0){
		return pays.get(0);
	}
	return null;
}
}
