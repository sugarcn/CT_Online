package org.ctonline.dao.order.impl;

import java.util.List;
import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.order.ICtBankDAO;
import org.ctonline.po.order.CtBank;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;

public class CtBankDAOImpl
  extends BaseHibernateDAOImpl
  implements ICtBankDAO
{
  public List<CtBank> findAll()
  {
    String hql = "from CtBank";
    Query query = getSession().createQuery(hql);
    return query.list();
  }
  
  public CtBank getBankById(Byte bankId)
  {
    String hql = " from CtBank ck where ck.bankId = " + bankId;
    Query query = getSession().createQuery(hql);
    List<CtBank> list = query.list();
    if ((list != null) && (list.size() > 0)) {
      return (CtBank)list.get(0);
    }
    return null;
  }
  
  public List<CtBank> loadAll(Page page)
  {
    List<CtBank> banks = null;
    String hql = " from CtBank ";
    Query query = getSession().createQuery(hql);
    if (page.getCurrentPage() != 0)
    {
      page.setTotalCount(totalCount(null));
      query.setFirstResult(page.getFirstIndex());
      query.setMaxResults(page.getPageSize());
    }
    banks = query.list();
    return banks;
  }
  
  public void save(CtBank bank)
  {
    String hql = "insert into Ct_Bank(BANK_ID,DEPOSIT,ACCOUNT_NAME,ACCOUNT_NUMBER) values(seq_bank.nextval,'" + bank.getDeposit() + "','" + bank.getAccountName() + "','" + bank.getAccountNumber() + "')";
    Query query = getSession().createSQLQuery(hql);
    query.executeUpdate();
  }
  
  public int delete(Integer id)
  {
    String hql = "delete from CtBank ct where ct.bankId=" + id;
    Query query = getSession().createQuery(hql);
    int d = query.executeUpdate();
    return d;
  }
  
  public void update(CtBank bank)
  {
    getSession().update(bank);
  }
  
  public List<CtBank> findAll(String keyword, Page page)
  {
    List<CtBank> bank = null;
    String sql = " from CtBank ct";
    String str = "";
    if ((!keyword.equals("请输入关键字")) && (!keyword.equals(""))) {
      str = str + " where ct.deposit like '%" + keyword + "%' or ct.accountName like '%" + keyword + "%' or ct.accountNumber like '%" + keyword + "%'";
    }
    Query query = getSession().createQuery(sql + str);
    page.setTotalCount(totalCount(str));
    query.setFirstResult(page.getFirstIndex());
    query.setMaxResults(page.getPageSize());
    bank = query.list();
    return bank;
  }
  
  public Long totalCount(String str)
  {
    String hql = "select count(*) from CtBank ct ";
    if (str != null) {
      hql = hql + str;
    }
    Long count = (Long)getSession().createQuery(hql).uniqueResult();
    return count;
  }
  
  public CtBank findById(Byte bid)
  {
    CtBank bank = (CtBank)getSession().get(CtBank.class, bid);
    return bank;
  }
}
