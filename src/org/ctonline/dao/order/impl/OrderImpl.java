package org.ctonline.dao.order.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.order.OrderDAO;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.goods.CtBomGoods;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.order.CtComplain;
import org.ctonline.po.order.CtEvaluation;
import org.ctonline.po.order.CtExpress;
import org.ctonline.po.order.CtOrderGoods;
import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.po.order.CtOrderRelation;
import org.ctonline.po.order.CtShop;
import org.ctonline.po.pay.CtPayInterface;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.po.views.ViewOrderCheck;
import org.ctonline.util.Page;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

public class OrderImpl
  extends BaseHibernateDAOImpl
  implements OrderDAO
{
  public List<CtGoods> getGoodsByGId(Long gid)
  {
    List<CtGoods> goodsList = null;
    String hql = "from CtGoods as cg where cg.GId = " + gid;
    Query query = getSession().createQuery(hql);
    goodsList = query.list();
    
    return goodsList;
  }
  
  public List<ViewOrderCheck> getOrderCheck(Long uid, Long gid)
  {
    List<ViewOrderCheck> list = null;
    String hql = "from ViewOrderCheck as oc where oc.UId = " + uid + 
      " and oc.cartId = " + gid;
    
    Query query = getSession().createQuery(hql);
    list = query.list();
    
    return list;
  }
  
  public CtUserAddress getAddress(Long addid)
  {
    CtUserAddress address = new CtUserAddress();
    String hql = "from CtUserAddress as ua where ua.AId = " + addid;
    
    Query query = getSession().createQuery(hql);
    List<CtUserAddress> list = query.list();
    address = (CtUserAddress)list.get(0);
    
    return address;
  }
  
  public Long saveOrderInfo(CtOrderInfo odinfo)
  {
    getSession().save(odinfo);
    Long infoID = odinfo.getOrderId();
    return infoID;
  }
  
  public int updateStartByOrderId(String orderId)
  {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    String hql = " update from CtOrderInfo coi set coi.orderStatus = 5, coi.isOkTime = '" + sdf.format(new Date()) + "' where coi.orderId = " + orderId;
    Query query = getSession().createQuery(hql);
    return query.executeUpdate();
  }
  
  public List<CtOrderInfo> getIsDis()
  {
    String hql = " from CtOrderInfo coi where coi.orderStatus = 4 order by orderId desc";
    Query query = getSession().createQuery(hql);
    return query.list();
  }
  
  public void saveOrderGoods(CtOrderGoods odgoods)
  {
    getSession().save(odgoods);
  }
  
  public String delCartGoods(Long uid, Long gid)
  {
    String sql = "DELETE FROM CT_CART cc WHERE CC.CART_ID = " + gid + 
      " AND CC.U_ID = " + uid;
    
    Query query = getSession().createSQLQuery(sql);
    query.executeUpdate();
    
    return "success";
  }
  
  public String delCart(Long uid)
  {
    String sql = "DELETE FROM CT_CART cc WHERE CC.U_ID = " + uid;
    
    Query query = getSession().createSQLQuery(sql);
    query.executeUpdate();
    
    return "success";
  }
  
  public List<CtOrderInfo> getOrderListByUId(Long uid, Page page)
  {
    List<CtOrderInfo> orderList = null;
    String hql = "from CtOrderInfo as oi where oi.UId = " + uid + 
      "order by oi.orderId desc";
    Query query = getSession().createQuery(hql);
    if (page.getCurrentPage() != 0)
    {
      page.setTotalCount(orderCount(uid));
      query.setFirstResult(page.getFirstIndex());
      query.setMaxResults(page.getPageSize());
    }
    orderList = query.list();
    return orderList;
  }
  
  public Long orderCount(Long uid)
  {
    String hql = "select count(*) from CtOrderInfo as ci where ci.UId = " + 
      uid;
    Long count = (Long)getSession().createQuery(hql).uniqueResult();
    return count;
  }
  
  public List<?> getOrderGoods(Long uid)
  {
    List<?> goodsList = null;
    StringBuffer sql = new StringBuffer();
    sql.append(" SELECT");
    sql.append(" COG.ORDER_GOODS_ID,COG.ORDER_ID,COG.G_ID,COG.G_PRICE,COG.G_NUMBER,COG.PACK,");
    sql.append(" NVL(COG.COUPON_ID,0) AS COUPON_ID,COG.G_SUBTOTAL,NVL(CGI.P_URL,'nourl') AS P_URL,");
    sql.append(" CG.G_NAME");
    sql.append(" FROM");
    sql.append(" CT_ORDER_GOODS cog");
    sql.append(" LEFT JOIN");
    sql.append(" CT_ORDER_INFO coi");
    sql.append(" ON");
    sql.append(" COG.ORDER_ID = COI.ORDER_ID");
    sql.append(" LEFT JOIN");
    sql.append(" CT_GOODS cg");
    sql.append(" ON");
    sql.append(" CG.G_ID = COG.G_ID");
    sql.append(" LEFT JOIN");
    sql.append(" CT_GOODS_IMG CGI");
    sql.append(" ON");
    sql.append(" COG.G_ID = CGI.G_ID");
    sql.append(" AND");
    sql.append(" CGI.COVER_IMG = 1");
    sql.append(" WHERE");
    sql.append(" COI.U_ID = " + uid);
    sql.append(" ORDER BY");
    sql.append(" COG.ORDER_GOODS_ID");
    SQLQuery query = getSession().createSQLQuery(sql.toString());
    goodsList = query.list();
    return goodsList;
  }
  
  public List<CtOrderInfo> getOrderListByTime(Long uid, Page page, String time)
  {
    List<CtOrderInfo> orderList = null;
    String hql = "from CtOrderInfo as oi where oi.UId = " + 
      uid + 
      " and to_date(oi.orderTime, 'yyyy-mm-dd hh24:mi:ss') BETWEEN ADD_MONTHS(SYSDATE,-" + 
      time + ") AND SYSDATE order by oi.orderId desc";
    Query query = getSession().createQuery(hql);
    if (page.getCurrentPage() != 0)
    {
      page.setTotalCount(orderCount(uid));
      query.setFirstResult(page.getFirstIndex());
      query.setMaxResults(page.getPageSize());
    }
    orderList = query.list();
    return orderList;
  }
  
  public List<CtOrderInfo> getOrderListByStatus(Long uid, Page page, String status)
  {
    List<CtOrderInfo> orderList = null;
    String hql = "from CtOrderInfo as oi where oi.UId = " + uid + 
      " and oi.orderStatus = " + status + 
      " order by oi.orderId desc";
    Query query = getSession().createQuery(hql);
    if (page.getCurrentPage() != 0)
    {
      page.setTotalCount(orderCount(uid));
      query.setFirstResult(page.getFirstIndex());
      query.setMaxResults(page.getPageSize());
    }
    orderList = query.list();
    return orderList;
  }
  
  public void changeStatus(Long orderId, String status)
  {
    String sql = "UPDATE CT_ORDER_INFO oi SET OI.ORDER_STATUS = " + status + 
      " WHERE OI.ORDER_ID = " + orderId;
    Query query = getSession().createSQLQuery(sql);
    query.executeUpdate();
  }
  
  public List<CtBomGoods> getGoodsByBom(Integer bomid)
  {
    List<CtBomGoods> goodsList = null;
    String hql = "from CtBomGoods as cbg where cbg.bomId = " + bomid;
    Query query = getSession().createQuery(hql);
    goodsList = query.list();
    return goodsList;
  }
  
  public CtOrderInfo getOrderByOrderSn(String orderSn)
  {
    String hql = "from CtOrderInfo co where co.orderSn = '" + orderSn + "'";
    Query query = getSession().createQuery(hql);
    List<CtOrderInfo> list = query.list();
    if (list.size() > 0) {
      return (CtOrderInfo)list.get(0);
    }
    return null;
  }
  
  public List<CtOrderInfo> getAllOrder(Page page,String OrderSn,String stime, String etime, Long uid, String stauts, int searchType)
  {//case t.isrefund when '1' then 1 end,t.order_id desc;
	  //select * from ct_order_info a  left join CT_PAY_INTERFACE b on a.order_sn=b.order_sn order by a.order_id desc, b.pay_date desc
	  //select * from ct_order_info a  left join CT_PAY_INTERFACE b on a.order_sn=b.order_sn order by case a.isrefinfo when '1' then 1 end, a.order_id desc, b.pay_date desc
    List<CtOrderInfo> list;
    List<Object[]> listDate = new ArrayList<Object[]>();
    List<Object[]> listDateXia = new ArrayList<Object[]>();
	try {
		String str = "";
		String findGoodsSn = "";
		if(searchType == 0 && OrderSn != null && !OrderSn.equals("noFind")){
			str = " and  a.order_sn like '%"+OrderSn+"%' ";
		} else {
			str="";
			findGoodsSn = "";
		}
		//商品编号搜索
		if(searchType == 1){
			OrderSn = OrderSn.replaceAll("%20", " ");
			findGoodsSn = " and e.g_name like '%"+OrderSn.toUpperCase()+"%'";
		}
		if((stime != null && etime != null) && (!stime.equals("") && !etime.equals("")) ){
			str += " and a.order_time between '"+stime+" 00:00:00' and '"+etime+" 23:59:59' ";
		}
		if(uid != null){
			str += " and a.u_id=" + uid + " ";
		}
		if(stauts != null && !stauts.equals("-1")){
			str += " and a.ORDER_STATUS="+stauts;
		}
		String delsql = "delete from CtPayInterface where  " +
				" orderSn in (select peopleName    from people group by peopleName   " +
				"   having count(peopleName) > 1) and   peopleId not in (select min(peopleId) from people group by peopleName  " +
				"   having count(peopleName)>1) ";
		String hql = " from CtOrderInfo coi, CtPayInterface cpi where coi.orderSn=cpi.orderSn order by case coi.isRefinfo when '1' then 1 end,coi.orderId desc,cpi.payDate desc";
		String sql = "select k.* from " +
				"(select a.* from ct_order_info a left join CT_PAY_INTERFACE b on a.order_sn=b.order_sn" +
				" left join ct_pay c on a.order_id=c.order_id where 1=1 "+str+" " +
						"order by case a.isrefinfo when '1' then 1 end," +
						"case when b.pay_date is null then nvl(a.order_time,0)  " +
						"else nvl(b.pay_date,0)  end desc," +
						"case when c.pay_date is null then nvl(a.order_time,0) else nvl(c.pay_date,0) end desc," +
						" a.order_time desc) k," +
						"(select * from (select g.order_id oids, " +
						"max(g.order_goods_id) max_id from ct_goods e left join" +
						" ct_order_goods g on e.g_id = g.g_id where 1=1 " +findGoodsSn+ " " +
						"group by g.order_id) h, ct_order_goods i where h.oids=i.order_id and h.max_id=i.order_goods_id) j where k.order_id=j.order_id";
		String h = " from CtOrderInfo coi where coi.";
		//Query query = getSession().createQuery(hql);
		
		SQLQuery sqlQuery = getSession().createSQLQuery(sql).addEntity("orderInfo",CtOrderInfo.class);
		if (page.getCurrentPage() != 0)
		{
			if(searchType == 0){
				page.setTotalCount(totalCountForOrder(str));
			}
			if(searchType == 1){
				page.setTotalCount(totalCountForOrder(str, findGoodsSn));
			}
		  sqlQuery.setFirstResult(page.getFirstIndex());
		  sqlQuery.setMaxResults(page.getPageSize());
		}
		String payDateSql = "select k.* from (select b.pay_date,a.order_id from ct_order_info a left join CT_PAY_INTERFACE b on a.order_sn=b.order_sn left join ct_pay c on a.order_id=c.order_id where 1=1 "+str+" order by case a.isrefinfo when '1' then 1 end,case when b.pay_date is null then nvl(a.order_time,0)  else nvl(b.pay_date,0)  end desc,case when c.pay_date is null then nvl(a.order_time,0) else nvl(c.pay_date,0) end desc, a.order_time desc) k ,(select * from (select g.order_id oids, max(g.order_goods_id) max_id from ct_goods e left join ct_order_goods g on e.g_id = g.g_id" +
				" where 1=1 "+findGoodsSn+" group by g.order_id) h, ct_order_goods i where h.oids=i.order_id and h.max_id=i.order_goods_id) j where k.order_id=j.order_id";
		SQLQuery sqlQueryDate = getSession().createSQLQuery(payDateSql);
		if (page.getCurrentPage() != 0)
		{
		  sqlQueryDate.setFirstResult(page.getFirstIndex());
		  sqlQueryDate.setMaxResults(page.getPageSize());
		}
		list = sqlQuery.list();
		listDate = sqlQueryDate.list();
		payDateSql = "select k.* from (select c.pay_date,a.order_id from ct_order_info a left join CT_PAY_INTERFACE b on a.order_sn=b.order_sn left join ct_pay c on a.order_id=c.order_id where 1=1 "+str+" order by case a.isrefinfo when '1' then 1 end,case when b.pay_date is null then nvl(a.order_time,0)  else nvl(b.pay_date,0)  end desc,case when c.pay_date is null then nvl(a.order_time,0) else nvl(c.pay_date,0) end desc, a.order_time desc) k ,(select * from (select g.order_id oids, max(g.order_goods_id) max_id from ct_goods e left join ct_order_goods g on e.g_id = g.g_id" +
				" where 1=1 "+findGoodsSn+" group by g.order_id) h, ct_order_goods i where h.oids=i.order_id and h.max_id=i.order_goods_id) j where k.order_id=j.order_id";
		sqlQueryDate = getSession().createSQLQuery(payDateSql);
		if (page.getCurrentPage() != 0)
		{
		  sqlQueryDate.setFirstResult(page.getFirstIndex());
		  sqlQueryDate.setMaxResults(page.getPageSize());
		}
		listDateXia = sqlQueryDate.list();
		                                  
		for (int i = 0; i < list.size(); i++) {
			System.out.println(listDate.get(i));
			try {
				Object[] obj = listDate.get(i);
				System.out.println(obj[0]);
				if(obj[0] != null){
					list.get(i).setXianPayDate(obj[0] + "");
				}
				obj = listDateXia.get(i);
				System.out.println(obj[0]);
				if(obj[0] != null){
					list.get(i).setXiaPayDate(obj[0] + "");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				if(listDate.get(i) != null){
					list.get(i).setXianPayDate(listDate.get(i) + "");
				}
				if(listDateXia.get(i) != null){
					list.get(i).setXiaPayDate(listDateXia.get(i) + "");
				}
			}
		}
		
		return list;
	} catch (HibernateException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
    return null;
  }
  private Long totalCountForOrder(String str, String findGoodsSn) {
	  String sql = "select count(1) from " +
				"(select a.* from ct_order_info a left join CT_PAY_INTERFACE b on a.order_sn=b.order_sn" +
				" left join ct_pay c on a.order_id=c.order_id where 1=1 "+str+" " +
						"order by case a.isrefinfo when '1' then 1 end," +
						"case when b.pay_date is null then nvl(a.order_time,0)  " +
						"else nvl(b.pay_date,0)  end desc," +
						"case when c.pay_date is null then nvl(a.order_time,0) else nvl(c.pay_date,0) end desc," +
						" a.order_time desc) k," +
						"(select * from (select g.order_id oids, " +
						"max(g.order_goods_id) max_id from ct_goods e left join" +
						" ct_order_goods g on e.g_id = g.g_id where 1=1 " +findGoodsSn+ " " +
						"group by g.order_id) h, ct_order_goods i where h.oids=i.order_id and h.max_id=i.order_goods_id) j where k.order_id=j.order_id";
	  Object count = getSession().createSQLQuery(sql).uniqueResult();
	    return Long.valueOf(count.toString());
}

/*
   * select k.* from (select a.* from ct_order_info a left join
                 CT_PAY_INTERFACE b on a.order_sn=b.order_sn left join
                  ct_pay c on a.order_id=c.order_id where 1=1 and
 a.u_id=8  order by case a.isrefinfo when  '1' then 1 end,case when b.pay_date is null then nvl(a.order_time,0)  else nvl(b.pay_date,0)  end desc,case when c.pay_date is null then nvl(a.order_time,0) else nvl(c.pay_date,0) end desc, a.order_time desc
                    
                    ) k ,(select * from (select g.order_id oids, max(g.order_goods_id) max_id from ct_goods e left join ct_order_goods g on e.g_id = g.g_id where e.g_name='0603 J 100R' group by g.order_id) h, ct_order_goods i where h.oids=i.order_id and h.max_id=i.order_goods_id) j where k.order_id=j.order_id
                    
   */
  public Long totalCount(String str)
  {
    String hql = "select count(*) from CtUserGroup";
    if (str != null) {
      hql = hql + str;
    }
    Long count = (Long)getSession().createQuery(hql).uniqueResult();
    return count;
  }
  
  public Long totalCountByComplain(String str)
  {
    String hql = "select count(*) from CtComplain";
    if (str != null) {
      hql = hql + str;
    }
    Long count = (Long)getSession().createQuery(hql).uniqueResult();
    return count;
  }
  
  public Long totalCountForOrder(String str)
  {
    String hql = "select count(*) from ct_order_info a where 1=1 ";
    if (str != null) {
      hql = hql + str;
    }
    Object count = getSession().createSQLQuery(hql).uniqueResult();
    return Long.valueOf(count.toString());
  }
  
  public Long totalCountFor(String str)
  {
    String hql = "select count(*) from CtExpress ct ";
    if (str != null) {
      hql = hql + str;
    }
    Long count = (Long)getSession().createQuery(hql).uniqueResult();
    return count;
  }
  
  public CtOrderInfo getOrderByOrderId(Long orderId)
  {
    String hql = "from CtOrderInfo coi where coi.orderId = " + orderId;
    Query query = getSession().createQuery(hql);
    
    List<CtOrderInfo> list = query.list();
    if ((list != null) && (list.size() > 0)) {
      return (CtOrderInfo)list.get(0);
    }
    return null;
  }
  
  public List<CtOrderGoods> getOrderGoodsByOrderId(Long orderId, int i)
  {
    String hql = "";
    if (i == 1) {
      hql = " from CtOrderGoods cog where cog.orderId = " + orderId + " and isParOrSim=1 order by GId";
    } else {
      hql = " from CtOrderGoods cog where cog.orderId = " + orderId + " and isParOrSim=0 order by GId";
    }
    Query query = getSession().createQuery(hql);
    return query.list();
  }
  
  public void updateTotalPriceByOrderId(CtOrderInfo ctOrderInfo)
  {
    String hql = "update from CtOrderInfo cog set cog.total = '" + 
      ctOrderInfo.getTotal() + "' where cog.orderId = " + 
      ctOrderInfo.getOrderId();
    Query query = getSession().createQuery(hql);
    query.executeUpdate();
  }
  
  public void updateStartByOrderId(Long orderId, Long orderStatus)
  {
    String hql = "update from CtOrderInfo cog set cog.orderStatus = " + 
      orderStatus + " where cog.orderId = " + orderId;
    Query query = getSession().createQuery(hql);
    query.executeUpdate();
  }
  
  public List<CtExpress> getAllExpress(Page page)
  {
    String hql = " from CtExpress ce";
    Query query = getSession().createQuery(hql);
    if (page.getCurrentPage() != 0)
    {
      page.setTotalCount(totalCountFor(null));
      query.setFirstResult(page.getFirstIndex());
      query.setMaxResults(page.getPageSize());
    }
    return query.list();
  }
  
  public void updateExNoByOrderId(CtOrderInfo ctOrderInfo)
  {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date date = new Date();
    String dateFmt = sdf.format(date);
    String hql = "update from CtOrderInfo coi set coi.exId = " + 
      ctOrderInfo.getExId() + " , coi.exNo = '" + 
      ctOrderInfo.getExNo() + "', coi.orderStatus = 4, coi.deliverTime = '" + dateFmt + "',coi.urgentOrder='-1' where coi.orderId = " + 
      ctOrderInfo.getOrderId();
    Query query = getSession().createQuery(hql);
    query.executeUpdate();
  }
  
  public int save(CtExpress express)
  {
    String hql = "insert into Ct_Express(EX_ID,EX_NAME,EX_URL,EX_DESC) values(SEQ_EXPRESS.nextval,'" + express.getExName() + "','" + express.getExUrl() + "','" + express.getExDesc() + "')";
    Query query = getSession().createSQLQuery(hql);
    int id = query.executeUpdate();
    return id;
  }
  
  public int delete(Integer id)
  {
    String hql = "delete from CtExpress ct where ct.exId=" + id;
    Query query = getSession().createQuery(hql);
    int d = query.executeUpdate();
    return d;
  }
  
  public void update(CtExpress express)
  {
    getSession().update(express);
  }
  
  public CtExpress findById(Integer id)
  {
    CtExpress express = (CtExpress)getSession().get(CtExpress.class, id);
    return express;
  }
  
  public CtEvaluation getEvaByOrderId(Long orderId)
  {
    String hql = " from CtEvaluation eva where eva.orderId = " + orderId;
    Query query = getSession().createQuery(hql);
    List<CtEvaluation> list = query.list();
    if ((list != null) && (list.size() > 0)) {
      return (CtEvaluation)list.get(0);
    }
    return null;
  }
  
  public void updateReturnStartByOrderId(Long orderId, String manName)
  {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    String hql = " update from CtOrderInfo coi set coi.retStatus = '库房确认回库',coi.ware='" + manName + "' ,coi.wareTime = '" + sdf.format(new Date()) + "' where coi.orderId=" + orderId;
    Query query = getSession().createQuery(hql);
    query.executeUpdate();
  }
  
  public void updateReturnManageByOrderId(Long orderId, String manName)
  {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    String hql = " update from CtOrderInfo coi set coi.retStatus = '财务审核',coi.manage='" + manName + "' ,coi.manageTime = '" + sdf.format(new Date()) + "' where coi.orderId=" + orderId;
    Query query = getSession().createQuery(hql);
    query.executeUpdate();
  }
  
  public void updateReturnFinanceByOrderId(Long orderId, String manName)
  {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    String hql = " update from CtOrderInfo coi set coi.retStatus = '退款成功',coi.finance='" + manName + "' ,coi.financeTime = '" + sdf.format(new Date()) + "' where coi.orderId=" + orderId;
    Query query = getSession().createQuery(hql);
    query.executeUpdate();
  }
  
  public List<CtExpress> findAll(String keyword, Page page)
  {
    List<CtExpress> express = null;
    String sql = " from CtExpress ct";
    String str = "";
    if ((!keyword.equals("请输入关键字")) && (!keyword.equals(""))) {
      str = str + " where ct.exName like '%" + keyword + "%' or ct.exUrl like '%" + keyword + "%' or ct.exDesc like '%" + keyword + "%'";
    }
    Query query = getSession().createQuery(sql + str);
    page.setTotalCount(totalCountFor(str));
    query.setFirstResult(page.getFirstIndex());
    query.setMaxResults(page.getPageSize());
    express = query.list();
    return express;
  }
  
  public List<CtComplain> getAllComplain(Page page)
  {
    String hql = " from CtComplain cc order by cc.comStatus";
    Query query = getSession().createQuery(hql);
    if (page.getCurrentPage() != 0)
    {
      page.setTotalCount(totalCountByComplain(null));
      query.setFirstResult(page.getFirstIndex());
      query.setMaxResults(page.getPageSize());
    }
    List<CtComplain> list = query.list();
    return list;
  }
  
  public CtComplain getComplainByComId(Integer comId)
  {
    String hql = " from CtComplain cc where cc.comId = " + comId;
    Query query = getSession().createQuery(hql);
    List<CtComplain> list = query.list();
    if ((list != null) && (list.size() > 0)) {
      return (CtComplain)list.get(0);
    }
    return null;
  }
  
  public void updateComplainByComId(CtComplain complain)
  {
    String hql = " update from CtComplain cc set cc.ansContent = '" + complain.getAnsContent() + "', " + 
      " cc.ansTips = '" + complain.getAnsTips() + "', cc.comStatus = '2' where cc.comId = " + complain.getComId();
    Query query = getSession().createQuery(hql);
    query.executeUpdate();
  }
  
  public void updateuser(CtUser m)
  {
    getSession().merge(m);
  }
  
  public String getOrderByshopId(Short shopId)
  {
    String hql = " from CtShop coi where coi.SId = " + shopId;
    Query query = getSession().createQuery(hql);
    List<CtShop> list = query.list();
    return ((CtShop)list.get(0)).getShopName();
  }
  
  public CtPayInterface getPayInterByOrderSn(String orderSn)
  {
    String hql = " from CtPayInterface cpi where cpi.orderSn = '" + orderSn + "'";
    Query query = getSession().createQuery(hql);
    List<CtPayInterface> list = query.list();
    if ((list != null) && (list.size() > 0)) {
      return (CtPayInterface)list.get(0);
    }
    return null;
  }
  
  public String getShengById(Integer province)
  {
    String hql = " from CtRegion cr where cr.regionId = " + province;
    Query query = getSession().createQuery(hql);
    List<CtRegion> list = query.list();
    if ((list != null) && (list.size() > 0)) {
      return ((CtRegion)list.get(0)).getRegionName();
    }
    return null;
  }
  
  public String getShiById(Integer city)
  {
    String hql = " from CtRegion cr where cr.regionId = " + city;
    Query query = getSession().createQuery(hql);
    List<CtRegion> list = query.list();
    if ((list != null) && (list.size() > 0)) {
      return ((CtRegion)list.get(0)).getRegionName();
    }
    return null;
  }
  
  public String getQuById(Integer district)
  {
    String hql = " from CtRegion cr where cr.regionId = " + district;
    Query query = getSession().createQuery(hql);
    List<CtRegion> list = query.list();
    if ((list != null) && (list.size() > 0)) {
      return ((CtRegion)list.get(0)).getRegionName();
    }
    return null;
  }

@Override
public void updateOrderInfoByInfo(CtOrderInfo info) {
	getSession().merge(info);
}

@Override
public List<CtOrderInfo> getOrderListByOrderSn(String orderSn) {
	String hql = " from CtOrderInfo ct where ct.orderSn like '" + orderSn + "%'";
	Query query = getSession().createQuery(hql);
	query.setFirstResult(0);
    query.setMaxResults(12);
	List<CtOrderInfo> list = query.list();
	if(list != null && list.size() > 0){
		return list;
	}
	return null;
}

@Override
public List<CtOrderGoods> getordergoodsByOrderId(Long orderId) {
	String hql = " from CtOrderGoods ct where ct.orderId="+orderId;
	Query query = getSession().createQuery(hql);
	List<CtOrderGoods> list = query.list();
	if(list != null && list.size() > 0){
		return list;
	}
	return null;
}

@Override
public void updateOrderGoodsByOrderId(Long orderId) {
	String hql = " update CtOrderGoods cog set cog.isRefund=3 where cog.orderId="+orderId;
	Query query = getSession().createQuery(hql);
	query.executeUpdate();
}

@Override
public String getFirstDateOrder() {
	String hql = " from CtOrderInfo co order by co.orderId asc";
	Query query = getSession().createQuery(hql);
	query.setFirstResult(0);
    query.setMaxResults(5);
    List<CtOrderInfo> list = query.list();
    if(list != null && list.size() > 0){
    	return list.get(0).getOrderTime().split(" ")[0];
    }
	return null;
}

@Override
public List<CtOrderInfo> getorderByUidAndPayType(Long uid, String str, Page page) {
	String types[] = str.split(",");
	String s = "";
	for (int i = 0; i < types.length; i++) {
		s += " co.pay="+types[i]+ " or ";
	}
	s = s.substring(0, s.length() - 3);
//	s += " or co.pay=7 or co.pay=8 ";
	String hql = " from CtOrderInfo co where co.UId = " + uid + " and (" + s + ") ";
	Query query = getSession().createQuery(hql);
	if (page.getCurrentPage() != 0)
    {
      page.setTotalCount(totalCountOrderInfoByUidAndType(uid, s));
      query.setFirstResult(page.getFirstIndex());
      query.setMaxResults(page.getPageSize());
    }
	List<CtOrderInfo> list = query.list();
	if(list != null && list.size() > 0){
		return list;
	}
	return null;
}

private Long totalCountOrderInfoByUidAndType(Long uid, String s) {
	String hql = "select count(*) from CtOrderInfo co where co.UId = " + uid + " and (" + s + ") ";
	Long count = (Long)getSession().createQuery(hql).uniqueResult();
	return count;
}

@Override
public List<CtOrderRelation> findRelationsByUid(Long uid, Page page) {
	String hql = " from CtOrderRelation co where co.UId="+uid;
	Query query = getSession().createQuery(hql);
	if (page.getCurrentPage() != 0)
    {
      page.setTotalCount(totalCountRelaByUid(uid));
      query.setFirstResult(page.getFirstIndex());
      query.setMaxResults(page.getPageSize());
    }
	List<CtOrderRelation> list = query.list();
	if(list != null && list.size() > 0){
		return list;
	}
	return null;
}

private Long totalCountRelaByUid(Long uid) {
	String hql = "select count(*) from CtOrderRelation co where co.UId = " + uid + " ";
	Long count = (Long)getSession().createQuery(hql).uniqueResult();
	return count;
}

@Override
public CtOrderRelation findRelatinsByOrid(Long orid) {
	String hql = " from CtOrderRelation co where co.orid="+orid;
	Query query = getSession().createQuery(hql);
	List<CtOrderRelation> list = query.list();
	if(list != null && list.size() > 0){
		return list.get(0);
	}
	return null;
}

@Override
public void updateOrderGoodsByOidAndGidAndType(Long refGoodsid,
		Long refOrderid, String refGoodsType) {
	String hql = " update CtOrderGoods cog set cog.isRefund=3 where cog.ctGoods.GId=" + refGoodsid + " and cog.orderId=" + refOrderid + " and cog.isParOrSim=" + refGoodsType ;
	Query query = getSession().createQuery(hql);
	query.executeUpdate();
}
}
