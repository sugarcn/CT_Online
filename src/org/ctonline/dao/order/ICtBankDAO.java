package org.ctonline.dao.order;

import java.util.List;
import org.ctonline.po.order.CtBank;
import org.ctonline.util.Page;

public abstract interface ICtBankDAO
{
  public abstract List findAll();
  
  public abstract CtBank getBankById(Byte paramByte);
  
  public abstract List<CtBank> loadAll(Page paramPage);
  
  public abstract void save(CtBank paramCtBank);
  
  public abstract int delete(Integer paramInteger);
  
  public abstract void update(CtBank paramCtBank);
  
  public abstract List<CtBank> findAll(String paramString, Page paramPage);
  
  public abstract Long totalCount(String paramString);
  
  public abstract CtBank findById(Byte paramByte);
}
