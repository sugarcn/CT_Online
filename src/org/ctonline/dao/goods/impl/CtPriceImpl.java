package org.ctonline.dao.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtPriceDAO;
import org.ctonline.po.goods.CtPrice;
import org.ctonline.po.views.ViewGoodsPrice;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtPriceImpl implements CtPriceDAO{
	
	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	@Override
	public Long insertPrice(CtPrice price) {
		// TODO Auto-generated method stub
		Long id;
		Session session=this.sessionFactory.openSession();
		id =(Long)session.save(price);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public List<CtPrice> checkPrice(Long gid,String gtype,Long uid,String utype) {
		// TODO Auto-generated method stub
		List<CtPrice> priceList = null;
		String hql = "from CtPrice as cp where cp.GId=? and cp.GType=? and cp.UId=? and cp.UType=?";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		query.setParameter(0, gid);
		query.setParameter(1, gtype);
		query.setParameter(2, uid);
		query.setParameter(3, utype);
		priceList = query.list();
		colseSession(session);
		return priceList;
	}

	@Override
	public void delPrice(Long id) {
		// TODO Auto-generated method stub
		String hql="delete CtPrice as u where u.id=?";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		query.setParameter(0, id);
		query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public void updatePrice(CtPrice price) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(price);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public List<ViewGoodsPrice> loadAll(Page page) {
		// TODO Auto-generated method stub
		List<ViewGoodsPrice> priceList = null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from ViewGoodsPrice");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		priceList=query.list();
		colseSession(session);
		return priceList;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtPrice";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
}
