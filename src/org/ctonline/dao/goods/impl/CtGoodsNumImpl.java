package org.ctonline.dao.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsNumDAO;
import org.ctonline.po.goods.CtGoodsNum;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtGoodsNumImpl implements CtGoodsNumDAO{

	private SessionFactory sessionFactory;
	
	public SessionFactory getsessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<CtGoodsNum> queryByGId(Long gid) {
		// TODO Auto-generated method stub
		List<CtGoodsNum> numList = null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from CtGoodsNum  where GId=?");
		query.setParameter(0, gid);
		numList=query.list();
		colseSession(session);
		return numList;
	}

	@Override
	public List<?> testQuery(Long gid) {
		// TODO Auto-generated method stub
		List<?> querylist = null;
		Session session = this.sessionFactory.openSession();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ");
		sql.append("CD.DEPOT_NAME as depotname,CGN.G_NUM as goodsnum,CGN.DEPOT_ID as depotid,CGN.ID as cgnid ");
		sql.append("FROM ");
		sql.append("CT_GOODS_NUM cgn ");
		sql.append("LEFT JOIN ");
		sql.append("CT_DEPOT cd ");
		sql.append("ON ");
		sql.append("CGN.DEPOT_ID = cd.DEPOT_ID ");
		sql.append("WHERE ");
		sql.append("CGN.G_ID= "+gid);
		SQLQuery query = session.createSQLQuery(sql.toString());
		querylist = query.list();
		colseSession(session);
		return querylist;
	}

	@Override
	public void tieDepot(CtGoodsNum num) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.openSession();
		session.save(num);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public void updateNum(CtGoodsNum num) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(num);
		session.beginTransaction().commit();
		colseSession(session);
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
}
