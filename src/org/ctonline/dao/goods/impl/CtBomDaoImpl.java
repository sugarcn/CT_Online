package org.ctonline.dao.goods.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.ctonline.dao.goods.CtBomDao;
import org.ctonline.po.goods.CtBom;
import org.ctonline.po.goods.CtBomGoods;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.Page;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.opensymphony.xwork2.ActionContext;

public class CtBomDaoImpl implements CtBomDao {

	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
	@Override
	public List<CtBom> loadAll(Page page,Long UId) {
		// TODO Auto-generated method stub
		List<CtBom> boms = null;
//		Map usersession = ActionContext.getContext().getSession();
//		CtUser cu = (CtUser) usersession.get("userinfosession");
//		Long UId = cu.getUId();
		Session session = this.sessionFactory.openSession();
		String hql = "from CtBom cb where cb.UId="+UId+" order by cb.bomTime desc";
		String str = "where cb.UId="+UId+" order by cb.bomTime desc";
		Query query = session.createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(str));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		boms = query.list();
		colseSession(session);
		return boms;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtBom cb ";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public Integer save(CtBom ctBom) {
		// TODO Auto-generated method stub
		Integer id;
		Session session =this.sessionFactory.openSession();
		id =  (Integer) session.save(ctBom);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public CtBom getCtBomByBomId(Integer BomId) {
		// TODO Auto-generated method stub
		CtBom bom;
		Session session =this.sessionFactory.openSession();
		String hql = "from CtBom cb where cb.bomId=?";
		Query query = session.createQuery(hql);
		query.setParameter(0, BomId);
		bom = (CtBom) query.uniqueResult();
		colseSession(session);
		return bom;
	}

	@Override
	public void update(CtBom ctBom) {
		// TODO Auto-generated method stub
		Session session =this.sessionFactory.openSession();
		session.update(ctBom);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public void delete(Integer bomMid) {
		// TODO Auto-generated method stub
		String hql="delete CtBom as cb where cb.bomId=?";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		query.setParameter(0, bomMid);
		query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public List<CtBom> findAll(String keyword, Page page,Long UId) {
		// TODO Auto-generated method stub
		List< CtBom> boms=null;
		String sql="from CtBom cb";
		String str="";
		if (!keyword.equals("")){
			str+=" where ( cb.bomTitle like '%"+keyword+"%' or cb.bomDesc like '%"+keyword+"%' ) and cb.UId="+UId+"  order by cb.bomTime desc";
		}
		//str+=" order by regionID desc";
		sql+=str;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		boms=query.list();
		colseSession(session);
		return boms;
	}

	@Override
	public List<CtBomGoods> loadBomDetailAll(Page page,Integer bomId) {
		// TODO Auto-generated method stub
		List<CtBomGoods> bomGoodsList;
		Session session=this.sessionFactory.openSession();
		String hql = "from CtBomGoods cbg where cbg.bomId=?";
		Query query = session.createQuery(hql);
		query.setParameter(0, bomId);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCountCtBomGoods(bomId));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		bomGoodsList = query.list();
		colseSession(session);
		return bomGoodsList;
	}
	
	public Long totalCountCtBomGoods(Integer bomId) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtBomGoods cbg where cbg.bomId="+bomId;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public void delGoodsOfBomDetail(Long bomGoodsId) {
		// TODO Auto-generated method stub
		String hql="delete CtBomGoods as cbg where cbg.bomGoodsId=?";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		query.setParameter(0, bomGoodsId);
		query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public List<CtBomGoods> findBomDetailAll(String keyword, Page page,Integer bomId) {
		// TODO Auto-generated method stub
		List< CtBomGoods> bomGoodsList=null;
		String hql="from CtBomGoods cbg where cbg.goods.GName like '%"+keyword+"%' or cbg.goods.goodsBrand.BName like '%"+keyword+"%' or cbg.goods.goodsCategory.CName like '%"+keyword+"%' or cbg.goods.shopPrice like '%"+keyword+"%' and cbg.bomId="+bomId;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		page.setTotalCount(this.totalCountOfBomDetailAll(hql));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		bomGoodsList=query.list();
		colseSession(session);
		return bomGoodsList;
	}
	
	public Long totalCountOfBomDetailAll(String hql) {
		// TODO Auto-generated method stub
		String str = "select count (*) ";
		str = str + hql;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(str).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public CtBomGoods getCtBomGoodsBybomGoodsId(Long bomGoodsId) {
		// TODO Auto-generated method stub
		Session session =this.sessionFactory.openSession();
		String hql = "from CtBomGoods cbg where cbg.bomGoodsId=?";
		Query query = session.createQuery(hql);
		query.setParameter(0, bomGoodsId);
		List<CtBomGoods> lists = query.list();
		colseSession(session);
		CtBomGoods bomGoods=null;
		if(lists.size() >0){
			 bomGoods = lists.get(0);
		}else {
			bomGoods = null;
		}
		return bomGoods;
	}

	@Override
	public Long saveCtBomGoods(CtBomGoods cbg) {
		// TODO Auto-generated method stub
		Session session =this.sessionFactory.openSession();
		Long id = (Long) session.save(cbg);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public void updateCtBomGoods(CtBomGoods cbg) {
		// TODO Auto-generated method stub
		Session session =this.sessionFactory.openSession();
		session.update(cbg);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public CtBomGoods getCtBomGoodsByGId(Long GId) {
		// TODO Auto-generated method stub
		Session session =this.sessionFactory.openSession();
		String hql = "from CtBomGoods cbg where cbg.GId=?";
		Query query = session.createQuery(hql);
		query.setParameter(0, GId);
		List<CtBomGoods> lists = query.list();
		colseSession(session);
		CtBomGoods bomGoods=null;
		if(lists.size() >0){
			 bomGoods = lists.get(0);
		}else {
			bomGoods = null;
		}
		return bomGoods;
	}

	@Override
	public List<CtBom> loadAllBomCenter(Page page) {
		// TODO Auto-generated method stub
		List<CtBom> boms = null;
		Session session = this.sessionFactory.openSession();
		String hql = "from CtBom cb where cb.isHot=1 order by cb.bomTime desc";
		String str = "where cb.isHot=1 order by cb.bomTime desc";
		Query query = session.createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(str));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		boms = query.list();
		colseSession(session);
		return boms;
	}

	@Override
	public List<CtBom> findAllBomCenter(String keyword, Page page) {
		// TODO Auto-generated method stub
		List< CtBom> boms=null;
		String sql="from CtBom cb";
		String str="";
		if (!keyword.equals("")){
			str+=" where ( cb.bomTitle like '%"+keyword+"%' or cb.bomDesc like '%"+keyword+"%' ) and cb.isHot=1  order by cb.bomTime desc";
		}
		//str+=" order by regionID desc";
		sql+=str;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		boms=query.list();
		colseSession(session);
		return boms;
	}
}
