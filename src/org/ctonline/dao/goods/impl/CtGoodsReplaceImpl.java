package org.ctonline.dao.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsReplaceDAO;
import org.ctonline.po.goods.CtGoodsReplace;
import org.ctonline.po.views.ViewGoodsReplace;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtGoodsReplaceImpl implements CtGoodsReplaceDAO{

	private SessionFactory sessionFactory;
	
	public SessionFactory getsessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public void tieReplace(CtGoodsReplace replace) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.openSession();
		session.save(replace);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public void unReplace(Long gid, Long rid) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		String hql = ("delete CtGoodsReplace as u where u.GId=? and u.replaceGId=?");
		Query query= session.createQuery(hql);
		query.setParameter(0, gid);
		query.setParameter(1, rid);
		query.executeUpdate();
		colseSession(session);
	}

	@Override
	public List<ViewGoodsReplace> queryRepList(Long gid) {
		// TODO Auto-generated method stub
		List<ViewGoodsReplace> reList = null;
		Session session=this.sessionFactory.openSession();
		String hql = ("from ViewGoodsReplace as vgl where vgl.GId = "+gid +" order by vgl.replaceGId");
		Query query= session.createQuery(hql);
		reList = query.list();
		colseSession(session);
		return reList;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
}
