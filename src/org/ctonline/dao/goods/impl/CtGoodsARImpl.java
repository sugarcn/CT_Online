package org.ctonline.dao.goods.impl;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.goods.CtGoodsARDAO;
import org.ctonline.po.goods.CtGoodsAttributeRelation;
import org.ctonline.po.views.ViewGoodsAttribute;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtGoodsARImpl extends BaseHibernateDAOImpl implements CtGoodsARDAO{

	@Override
	public Long save(CtGoodsAttributeRelation relation)
	  {
		CtGoodsAttributeRelation id = (CtGoodsAttributeRelation)getSession().merge(relation);
		
	    return id.getId();
	  }
	@Override
	public List<ViewGoodsAttribute> findById(Long gid)
	{	
		List<ViewGoodsAttribute> gaList = null;
	    String hql = "from ViewGoodsAttribute as vga where vga.GId = " + gid + "order by vga.sortOrder";
	    Query query = getSession().createQuery(hql);
	    gaList = query.list();
	    return gaList;
	}

}
