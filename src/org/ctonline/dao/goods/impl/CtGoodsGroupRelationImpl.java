package org.ctonline.dao.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsGroupRelationDAO;
import org.ctonline.po.goods.CtGoodsAttributeRelation;
import org.ctonline.po.goods.CtGoodsGroupRelation;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtGoodsGroupRelationImpl implements CtGoodsGroupRelationDAO{

	private SessionFactory sessionFactory;
	
	public SessionFactory getsessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Long save(CtGoodsGroupRelation groupRelation){
		
		Long id=0l;
		Session session=this.sessionFactory.openSession();
		session.save(groupRelation);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
		
	}
	
	@Override
public List<CtGoodsGroupRelation>  selectAll(Long UGId){
		
		List< CtGoodsGroupRelation> groupRelation=null;
		String sql="from CtUserGroupRelation where  UGId="+UGId;
		//str+=" order by regionID desc";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		
		groupRelation=query.list();
		colseSession(session);
		return groupRelation;
		
	}
	
	@Override
		
		public Long totalCount(Long id) {
			// TODO Auto-generated method stub
			String hql="select count(*) from CtGoodsGroupRelation where UId="+id;
			Session session =this.sessionFactory.openSession();
			Long count= (Long)session.createQuery(hql).uniqueResult();
			colseSession(session);
			return count;
		}
	@Override
	public int delete(Long id){
		int d;
		String hql="delete CtGoodsGroupRelation as u where u.GGId="+id;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		d= query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
		return d;
		
		
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}

	@Override
	public CtGoodsAttributeRelation findRelByGidAndAttrId(Long gtid, Long long1) {
		String hql = " from CtGoodsAttributeRelation cgar where cgar.GId=" + gtid + " and cgar.attrId=" + long1;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		List<CtGoodsAttributeRelation> list = query.list();
		if(list != null && list.size() > 0){
			session.beginTransaction().commit();
			colseSession(session);
			return list.get(0);
		}
		session.beginTransaction().commit();
		colseSession(session);
		return null;
	}
	

}
