package org.ctonline.dao.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsAttributeDAO;
import org.ctonline.po.goods.CtGoodsAttribute;
import org.ctonline.po.views.ViewGoodsAttribute;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtGoodsAttributeImpl implements CtGoodsAttributeDAO{
	
	private SessionFactory sessionFactory;
	
	public SessionFactory getsessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Long save(CtGoodsAttribute attribute) {
		// TODO Auto-generated method stub
		Long id;
		Session session=this.sessionFactory.openSession();
		id =(Long)session.save(attribute);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public CtGoodsAttribute findById(Long id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		CtGoodsAttribute attribute = (CtGoodsAttribute)session.get(CtGoodsAttribute.class, id);
		colseSession(session);
		return attribute;
	}

	@Override
	public List<CtGoodsAttribute> loadAll(Page page) {
		// TODO Auto-generated method stub
		List< CtGoodsAttribute> types=null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from CtGoodsAttribute");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		types=query.list();
		colseSession(session);
		return types;
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		int d;
		String hql="delete CtGoodsAttribute as u where u.id=?";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		query.setParameter(0, id);
		d= query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
		return d;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtGoodsAttribute";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public void update(CtGoodsAttribute attribute) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(attribute);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public List<CtGoodsAttribute> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List< CtGoodsAttribute > types=null;
		String sql="from CtGoodsAttribute";
		String str="";
		if (!keyword.equals("请输入你要查询的关键词") && !keyword.equals("")){
			str+=" where attrName like '%"+keyword+"%' ";
		}
		//str+=" order by regionID desc";
		sql+=str;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		types=query.list();
		colseSession(session);
		return types;
	}

		@Override
		public List<CtGoodsAttribute> searchAll(Long id,Page page){
			List< CtGoodsAttribute> attribute=null;
			String sql="from CtGoodsAttribute  where GTId="+id;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(" where GTId="+id ));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		attribute=query.list();
		colseSession(session);
		return attribute;
		}

		@Override
		public List<CtGoodsAttribute> queryByTID(Long tid) {
			// TODO Auto-generated method stub
			List< CtGoodsAttribute> attList=null;
			Session session=this.sessionFactory.openSession();
			Query query= session.createQuery("from CtGoodsAttribute as cga where cga.GTId = ?");
			query.setParameter(0, tid);
			attList=query.list();
			colseSession(session);
			return attList;
		}

		@Override
		public List<ViewGoodsAttribute> queryByGId(Long gid) {
			// TODO Auto-generated method stub
			List<ViewGoodsAttribute> gaList = null;
			Session session=this.sessionFactory.openSession();
			String hql = ("from ViewGoodsAttribute as vga where vga.GId = "+gid +"order by vga.attrId");
			Query query= session.createQuery(hql);
			gaList = query.list();
			colseSession(session);
			return gaList;
		}

		@Override
		public void delAttRelation(Long gid) {
			// TODO Auto-generated method stub
			String hql="delete CtGoodsAttributeRelation as u where u.GId=?";
			Session session=this.sessionFactory.openSession();
			Query query= session.createQuery(hql);
			query.setParameter(0, gid);
			query.executeUpdate();
			session.beginTransaction().commit();
			colseSession(session);
		}
		private void colseSession(Session session) {
			session.clear();
			session.flush();
			session.close();
		}
}
