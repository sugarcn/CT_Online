package org.ctonline.dao.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsCategoryDAO;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtGoodsCategoryImpl implements CtGoodsCategoryDAO {
	private SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Long save(CtGoodsCategory category) {
		// TODO Auto-generated method stub
		Long id;
		Session session=this.sessionFactory.openSession();
		id =(Long)session.save(category);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public CtGoodsCategory findById(Long id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		CtGoodsCategory category = (CtGoodsCategory)session.get(CtGoodsCategory.class, id);
		colseSession(session);
		return category;//get or load
	}

	@Override
	public List<CtGoodsCategory> loadAll(Page page) {
		// TODO Auto-generated method stub
		List< CtGoodsCategory> category=null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from CtGoodsCategory");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		category=query.list();
		colseSession(session);
		return category;
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		int d;
		String hql="delete CtGoodsCategory as u where u.id=?";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		query.setParameter(0, id);
		d= query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
		return d;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtGoodsCategory";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public void update(CtGoodsCategory category) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(category);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public List<CtGoodsCategory> findAll(String keyword,Page page) {
		// TODO Auto-generated method stub
		List< CtGoodsCategory> category=null;
		String sql="from CtGoodsCategory";
		String str="";
		if (!keyword.equals("请输入关键字") && !keyword.equals("")){
			str+=" where CName like '%"+keyword+"%' ";
		}
		sql+=str;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		category=query.list();
		colseSession(session);
		return category;
	}

	@Override
	public List<CtGoodsCategory> levelquery(Long id) {
		// TODO Auto-generated method stub
		List<CtGoodsCategory> list = null;
		String sql="from CtGoodsCategory  where parentId="+id + " order by sortOrder asc";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		list = query.list();
		colseSession(session);
		return list;
	}
	
	@Override
	public List<CtGoodsCategory> searchAll(Long id,Page page){
		List< CtGoodsCategory> category=null;
		String sql="from CtGoodsCategory  where parentId="+id+" order by CName";
	Session session=this.sessionFactory.openSession();
	Query query= session.createQuery(sql);
	if (page.getCurrentPage()!=0){
		page.setTotalCount(this.totalCount(" where parentId="+id));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
	}
	category=query.list();
	colseSession(session);
	return category;
	}
	
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
}
