package org.ctonline.dao.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.ViewGoodsGroupDAO;
import org.ctonline.po.views.ViewGoodsGroup;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class ViewGoodsGroupImpl implements ViewGoodsGroupDAO{
	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<ViewGoodsGroup> query(String goodsname){
		List<ViewGoodsGroup> goodslist = null;
		Session session = this.getSessionFactory().openSession();
		String hql = " from ViewGoodsGroup a where a.goodsname like'%"+goodsname+"%'";
		Query query = session.createQuery(hql);
		goodslist = query.list();
		colseSession(session);
		return goodslist;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
}
