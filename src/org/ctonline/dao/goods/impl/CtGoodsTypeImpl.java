package org.ctonline.dao.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsTypeDAO;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsType;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtGoodsTypeImpl implements CtGoodsTypeDAO{
	
	private SessionFactory sessionFactory;
	
	public SessionFactory getsessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Long save(CtGoodsType goodsType) {
		// TODO Auto-generated method stub
		Long id;
		Session session=this.sessionFactory.openSession();
		id =(Long)session.save(goodsType);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public CtGoodsType findById(Long id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		CtGoodsType goodsType = (CtGoodsType)session.get(CtGoodsType.class, id);
		colseSession(session);
		return goodsType;
	}

	@Override
	public List<CtGoodsType> loadAll(Page page) {
		// TODO Auto-generated method stub
		List< CtGoodsType> types=null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from CtGoodsType");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		types=query.list();
		colseSession(session);
		return types;
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		int d;
		String hql="delete from CtGoodsType u  where u.GTId=?";
		String hql1="delete from CtGoodsAttribute a  where a.GTId=?";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		Query query1= session.createQuery(hql1);
		query.setParameter(0, id);
		query1.setParameter(0, id);
		d= query.executeUpdate();
		query1.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
		return d;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtGoodsType";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public void update(CtGoodsType goodsType) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(goodsType);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public List<CtGoodsType> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List< CtGoodsType > types=null;
		String sql="from CtGoodsType";
		String str="";
		if (!keyword.equals("请输入你要查询的关键词") && !keyword.equals("")){
			str+=" where typeName like '%"+keyword+"%' ";
		}
		//str+=" order by regionID desc";
		sql+=str;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		types=query.list();
		colseSession(session);
		return types;
	}

	@Override
	public Long getID() {
		// TODO Auto-generated method stub
		String sql = "select SEQ_REGION .nextval as id from dual";
		Session session=this.sessionFactory.openSession();
		Long id= (Long)session.createQuery(sql).uniqueResult();
		colseSession(session);
		return id;
	}

	@Override
	public List<CtGoodsType> queryAll() {
		// TODO Auto-generated method stub
		List< CtGoodsType> types=null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from CtGoodsType");
		types=query.list();
		colseSession(session);
		return types;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
}
