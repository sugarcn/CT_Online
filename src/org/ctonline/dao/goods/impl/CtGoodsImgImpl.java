package org.ctonline.dao.goods.impl;

import java.util.List;
import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.goods.CtGoodsImgDAO;
import org.ctonline.po.goods.CtGoodsImg;
import org.hibernate.Query;
import org.hibernate.Session;

public class CtGoodsImgImpl
  extends BaseHibernateDAOImpl
  implements CtGoodsImgDAO
{
  public void addImg(CtGoodsImg img)
  {
    getSession().save(img);
  }
  
  public List<CtGoodsImg> queryByGid(Long gid)
  {
    List<CtGoodsImg> detailList = null;
    Query query = getSession().createQuery("from CtGoodsImg  where GId=?");
    query.setParameter(0, gid);
    detailList = query.list();
    return detailList;
  }
  
  public void updateImg(CtGoodsImg img)
  {
    getSession().update(img);
  }
}
