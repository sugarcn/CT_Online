package org.ctonline.dao.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsGroupDAO;
import org.ctonline.po.goods.CtGoodsGroup;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtGoodsGroupImpl implements CtGoodsGroupDAO {
	
	private SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Long save(CtGoodsGroup goodsGroup) {
		// TODO Auto-generated method stub
		Long id;
		Session session=this.sessionFactory.openSession();
		id =(Long)session.save(goodsGroup);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public CtGoodsGroup findById(Long id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		CtGoodsGroup goodsGroup = (CtGoodsGroup)session.get(CtGoodsGroup.class, id);
		colseSession(session);
		return goodsGroup;//get or load
	}
    
	@SuppressWarnings("unchecked")
	@Override
	public List<CtGoodsGroup> loadAll(Page page) {
		// TODO Auto-generated method stub
		List< CtGoodsGroup> goodsGroup=null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from CtGoodsGroup order by GGName");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		goodsGroup=query.list();
		colseSession(session);
		return goodsGroup;
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		int d;
		String hql="delete CtGoodsGroup as u where u.id=?";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		query.setParameter(0, id);
		d= query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
		return d;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtGoodsGroup";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public void update(CtGoodsGroup goodsGroup) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(goodsGroup);
		session.beginTransaction().commit();
		colseSession(session);
	}
    
	@SuppressWarnings("unchecked")
	@Override
	public List<CtGoodsGroup> findAll(String keyword,Page page) {
		// TODO Auto-generated method stub
		List< CtGoodsGroup> goodsGroup=null;
		String sql="from CtGoodsGroup";
		String str="";
		if (!keyword.equals("请输入关键字") && !keyword.equals("")){
			str+=" where GGName like '%"+keyword+"%' ";
		}
		sql+=str;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		goodsGroup=query.list();
		colseSession(session);
		return goodsGroup;
	}
	
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
}
