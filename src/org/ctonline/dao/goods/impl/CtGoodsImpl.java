package org.ctonline.dao.goods.impl;

import java.util.List;


import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.goods.CtGoodsDAO;
import org.ctonline.po.basic.CtCashCoupon;
import org.ctonline.po.basic.CtFreeSampleReg;
import org.ctonline.po.basic.CtReplace;
import org.ctonline.po.goods.CtCateGoods;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGoodsResource;
import org.ctonline.po.goods.CtGoodsType;
import org.ctonline.po.goods.CtGroupPrice;
import org.ctonline.po.goods.CtRangePrice;
import org.ctonline.po.goods.CtSearchCollect;
import org.ctonline.po.views.GoodsDetailView;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;


public class CtGoodsImpl extends BaseHibernateDAOImpl implements CtGoodsDAO{
	
//	private SessionFactory sessionFactory;
//	
//	public SessionFactory getsessionFactory() {
//		return sessionFactory;
//	}
//
//	public void setSessionFactory(SessionFactory sessionFactory) {
//		this.sessionFactory = sessionFactory;
//	}

	@Override
	public Long save(CtGoods goods) {
		// TODO Auto-generated method stub
		//Long id;
	    goods = (CtGoods)getSession().merge(goods);
	    
	    return goods.getGId();
	}

	@Override
	public CtGoods findById(Long id) {
		// TODO Auto-generated method stub
	    CtGoods goods = new CtGoods();
	    Query query = getSession().createQuery("from CtGoods  where GId=?");
	    query.setParameter(0, id);
	    goods = (CtGoods)query.uniqueResult();
	    return goods;
	}

	@Override
	public List<CtGoods> loadAll(Page page) {
		// TODO Auto-generated method stub
	    List<CtGoods> resource = null;
	    Query query = getSession().createQuery(
	      "from CtGoods as u where u.isDel = 0 ");
	    if (page.getCurrentPage() != 0)
	    {
	      page.setTotalCount(totalCount(null));
	      query.setFirstResult(page.getFirstIndex());
	      query.setMaxResults(page.getPageSize());
	    }
	    resource = query.list();
	    return resource;
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
	    String hql = "delete CtGoods as u where u.id=?";
	    Query query = getSession().createQuery(hql);
	    query.setParameter(0, id);
	    int d = query.executeUpdate();
	    return d;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
	    String hql = "select count(*) from CtGoods";
	    if (str != null) {
	      hql = hql + str;
	    }
	    Long count = (Long)getSession().createQuery(hql).uniqueResult();
	    return count;
	}

	@Override
	public void update(CtGoods goods) {
		// TODO Auto-generated method stub
		getSession().update(goods);
	}
	
	@Override
	public void updateForName(String tname, String sn, String hits)
	{
		String hql = "update CtGoods set tname='" + tname + "',hits=" + hits + " where GSn='" + sn + "'";
	    Query query = getSession().createQuery(hql);
	    query.executeUpdate();
	}

	@Override
	public List<CtGoods> findAll(String keyword,Page page) {
		// TODO Auto-generated method stub
	    List<CtGoods> resource = null;
	    String sql = "from CtGoods";
	    String str = "";
	    if ((!keyword.equals("请输入你要查询的关键词")) && (!keyword.equals(""))) {
	      str = 
	        str + " where GId not in ( select GId from CtGoodsGroupRelation ) and  G_NAME like '%" + keyword + "%' ";
	    }
	    sql = sql + str;
	    Query query = getSession().createQuery(sql);
	    if (page.getCurrentPage() != 0)
	    {
	      page.setTotalCount(totalCount(null));
	      query.setFirstResult(page.getFirstIndex());
	      query.setMaxResults(page.getPageSize());
	    }
	    resource = query.list();
	    return resource;
	}

	@Override
	public Long getID() {
		// TODO Auto-generated method stub
	    String sql = "select SEQ_REGION .nextval as id from dual";
	    Long id = (Long)getSession().createQuery(sql).uniqueResult();
	    return id;
	}

	@Override
	public List<CtGoods> queryAll(String status) {
		// TODO Auto-generated method stub
	    List<CtGoods> resource = null;
	    String hql = "from CtGoods cg where cg.isDel = ?";
	    Query query = getSession().createQuery(hql);
	    query.setParameter(0, status);
	    resource = query.list();
	    return resource;
	}

	@Override
	public List<CtGoodsCategory> queryCategory(Long id) {
		// TODO Auto-generated method stub
	    List<CtGoodsCategory> category = null;
	    Query query = getSession().createQuery(
	      "from CtGoodsCategory  where parentId=?");
	    query.setParameter(0, id);
	    category = query.list();
	    return category;
	}

	@Override
	public List<CtGoodsBrand> queryBrand() {
		// TODO Auto-generated method stub
	    List<CtGoodsBrand> brand = null;
	    Query query = getSession().createQuery("from CtGoodsBrand");
	    brand = query.list();
	    return brand;
	}

	@Override
	public void hide(Long id) {
		// TODO Auto-generated method stub
	    String hql = "update CtGoods cg set cg.isDel = -(cg.isDel-1) where cg.GId = ?";
	    Query query = getSession().createQuery(hql);
	    query.setParameter(0, id);
	    query.executeUpdate();
	}

	@Override
	public List<GoodsDetailView> queryDetail(Long id) {
		// TODO Auto-generated method stub
		//Session session = this.sessionFactory.openSession();
		List<GoodsDetailView> detailList = null;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ");
		sql.append("CG.G_ID as GId,CG.G_NAME as GName,CG.G_SN as GSn,CG.MARKET_PRICE as marketPrice, ");
		sql.append("CG.SHOP_PRICE as shopPrice,CG.PROMOTE_PRICE as promotePrice,CG.IS_ON_SALE as isOnSale, ");
		sql.append("CG.G_KEYWORDS as GKeywords,CG.TOKEN_CREDIT as tokenCredit,CG.GIVE_CREDIT as giveCredit, ");
		sql.append("CG.G_UNIT as GUnit,CG.PACK1 as pack1,CG.PACK1_NUM as pack1Num,CG.PACK2 as pack2,CG.PACK2_NUM as pack2Num ");
		sql.append(",CG.PACK3 as pack3,CG.PACK3_NUM as pack3Num,CG.PACK4 as pack4,CG.PACK4_NUM as pack4Num, ");
		sql.append("GBC.bname,GBC.cname ");
		sql.append("FROM ");
		sql.append("CT_GOODS cg ");
		sql.append("LEFT JOIN ");
		sql.append("(SELECT ");
		sql.append("CGB.B_ID as bid,CGB.B_NAME as bname,CGC.C_ID as cid,CGC.C_NAME as cname ");
		sql.append("FROM ");
		sql.append("CT_GOODS_BRAND cgb,CT_GOODS_CATEGORY cgc ");
		sql.append(") gbc ");
		sql.append("ON ");
		sql.append("cg.B_ID=gbc.bid ");
		sql.append("AND ");
		sql.append("CG.C_ID=GBC.cid ");
		sql.append("WHERE ");
		sql.append("CG.G_ID = " + id);
		SQLQuery query = getSession().createSQLQuery(sql.toString());
	    detailList = query.list();
	    return detailList;
	}

	@Override
	public void empty() {
		// TODO Auto-generated method stub
	    String hql = "delete CtGoods as u where u.isDel=1";
	    Query query = getSession().createQuery(hql);
	    query.executeUpdate();
	}

	@Override
	public List<CtGoodsResource> queryRes() {
		// TODO Auto-generated method stub
	    List<CtGoodsResource> resList = null;
	    Query query = getSession().createQuery("from CtGoodsResource");
	    resList = query.list();
	    return resList;
	}

	@Override
	public List<CtGoodsType> queryType() {
		// TODO Auto-generated method stub
	    List<CtGoodsType> typeList = null;
	    Query query = getSession().createQuery("from CtGoodsType");
	    typeList = query.list();
	    return typeList;
	}

	@Override
	public void tieRes(CtGoodsResource res) {
		// TODO Auto-generated method stub
	    getSession().save(res);
	}

	@Override
	public List<?> selectAll(Long GGId) {
		// TODO Auto-generated method stub
	    List<?> user = null;
	    String sql = "select a.GId , a.GName  from CtGoods a , CtGoodsGroupRelation b where a.GId = b.GId and b.GGId=" + 
	      GGId;
	    
	    Query query = getSession().createQuery(sql);
	    
	    user = query.list();
	    return user;
	}

	@Override
	public void unTieRes(Long gid, Long rid) {
		// TODO Auto-generated method stub
	    String hql = "delete CtGoodsResource as cgr where cgr.GId = " + gid + 
	    	      " and cgr.resourceId = " + rid;
	    	    Query query = getSession().createQuery(hql);
	    	    query.executeUpdate();
	}
	

	public List<CtGoods> getGoodsByGname(String Gname) {
		// TODO Auto-generated method stub
	    String hql = "select G_ID,G_NAME from CT_GOODS cg where G_NAME like '%" + 
	    	      Gname + "%' and rownum <=10";
	    Query query = getSession().createSQLQuery(hql);
	    	    
	    List<CtGoods> list = query.list();
	    return list;
	}

	@Override
	public Object getOneGoodsByGname(String Gname) {
		// TODO Auto-generated method stub
		String hql = "select G_ID,G_NAME from CT_GOODS cg where G_NAME ='"+Gname+"' and rownum <=10";
		Query query = getSession().createSQLQuery(hql);
		List<CtGoods> list = query.list();
		Object obj;
		if(list.size() > 0){
			obj = list.get(0);
		}else {
			obj = null;
		}
		return obj;
	}
	
	@Override
	public void saveRangePrice(CtRangePrice rangePrice){
	    getSession().merge(rangePrice);
	}
	
	@Override
	public void savegroupPrice(CtGroupPrice groupPrice){
		getSession().merge(groupPrice);
	}
	
	@Override
	public void saveCateGoodsInfo(CtCateGoods cateGoods){
		getSession().save(cateGoods);
	}

	@Override
	public List<CtRangePrice> findRanParByGid(Long gId){
		String hql = " from CtRangePrice crp where crp.GId = " + gId + 
			      " order by crp.insTime desc";
		Query query = getSession().createQuery(hql);
		List<CtRangePrice> list = query.list();
		for (int i = 0; i < list.size(); i++) {
			for (int j = list.size() - 1; j > i; j--) {
				if ((((CtRangePrice)list.get(i)).getSimRPrice().equals(((CtRangePrice)list.get(j))
			          .getSimRPrice())) || 
			          (((CtRangePrice)list.get(i)).getParRPrice().equals(((CtRangePrice)list.get(j))
			          .getParRPrice()))) {
					list.remove(j);
				}
			}
		}
		return list;
	}

	@Override
	public CtGoods getGoodsByGnameNoLike(String stringCellValue) {
		// TODO Auto-generated method stub
	    String hql = " from CtGoods cg where cg.GName='" + 
	    		stringCellValue + "' ";
	    Query query = getSession().createQuery(hql);
	    	    
	    List<CtGoods> list = query.list();
	    if(list != null && list.size() > 0){
	    	return list.get(0);
	    }
	    return null;
	}
	
	@Override
	public CtGoods getGoodsByGSnNoLike(String stringCellValue) {
		// TODO Auto-generated method stub
	    String hql = " from CtGoods cg where cg.GSn='" + 
	    		stringCellValue + "' ";
	    Query query = getSession().createQuery(hql);
	    	    
	    List<CtGoods> list = query.list();
	    if(list != null && list.size() > 0){
	    	return list.get(0);
	    }
	    return null;
	}	

	@Override
	public void deleteRenPriceByGid(Long gId) {
		String hql = " delete from CtRangePrice t where t.GId=" + gId;
		Query query = getSession().createQuery(hql);
		query.executeUpdate();
	}

	@Override
	public void deleteRelationByGid(Long gid) {
		// TODO Auto-generated method stub
	    String hql = " delete from CtGoodsAttributeRelation t where t.GId=" + gid;
	    Query query = getSession().createQuery(hql);
	    query.executeUpdate();
	}

	@Override
	public List<CtFreeSampleReg> getFreeSamRegByPage(Page page) {
		String str = "";
	    String hql = " from CtFreeSampleReg t order by t.samId desc";
	    Query query = getSession().createQuery(hql);
		if (page.getCurrentPage() != 0)
		{
		  page.setTotalCount(totalCountForFree(str));
		  query.setFirstResult(page.getFirstIndex());
		  query.setMaxResults(page.getPageSize());
		}
		return query.list();
	}

	private Long totalCountForFree(String str) {
		String hql = "select count(*) from CtFreeSampleReg t where 1=1";
	    if (str != null) {
	      hql = hql + str;
	    }
	    Object count = getSession().createQuery(hql).uniqueResult();
	    return Long.valueOf(count.toString());
	}

	@Override
	public CtFreeSampleReg findRegById(Integer samId) {
		String hql = " from CtFreeSampleReg t where t.samId="+samId;
		Query query = getSession().createQuery(hql);
		return (CtFreeSampleReg) query.list().get(0);
	}

	@Override
	public void updateSamBySamId(Integer samId) {
		String hql = " update CtFreeSampleReg t set t.samReply=1 where t.samId="+samId;
		Query query = getSession().createQuery(hql);
		query.executeUpdate();
	}

	@Override
	public List<CtCashCoupon> getCashCoupon(Page page) {
		String hql = " from CtCashCoupon ccc order by ccc.cashId desc";
		Query query = getSession().createQuery(hql);
		if (page.getCurrentPage() != 0)
		{
		  page.setTotalCount(totalCountCashCoupon(null));
		  query.setFirstResult(page.getFirstIndex());
		  query.setMaxResults(page.getPageSize());
		}
		return query.list();
	}

	private Long totalCountCashCoupon(String str) {
		String hql = "select count(*) from CtCashCoupon ccc order by ccc.cashId desc";
		Query query = getSession().createQuery(hql);
		return (Long) query.uniqueResult();
	}

	@Override
	public List<CtReplace> findReplaceByPage(Page page) {
		String hql = " from CtReplace cr order by cr.reId desc";
		Query query = getSession().createQuery(hql);
		if (page.getCurrentPage() != 0)
		{
		  page.setTotalCount(totalCountCtReplace(null));
		  query.setFirstResult(page.getFirstIndex());
		  query.setMaxResults(page.getPageSize());
		}
		return query.list();
	}

	private Long totalCountCtReplace(String str) {
		String hql = "";
		if(str != null){
			hql = "select count(*) from CtReplace cr where 1=1 and "+str+" order by cr.reId desc";
		} else {
			hql = "select count(*) from CtReplace cr order by cr.reId desc";
		}
		Query query = getSession().createQuery(hql);
		return (Long) query.uniqueResult();
	}

	@Override
	public void saveReplace(CtReplace replace) {
		getSession().merge(replace);
	}

	@Override
	public CtReplace findSearchKeyById(Integer id) {
		String hql = " from CtReplace cr where cr.reId="+id;
		Query query = getSession().createQuery(hql);
		List<CtReplace> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	@Override
	public void deleteReplace(CtReplace replace) {
		getSession().delete(replace);
	}

	@Override
	public List<CtReplace> findReplaceByKeyAndPage(String key, Page page) {
		String str = "";
		str += " cr.re1 like '%"+key+"%' or" +
				" cr.re2 like '%"+key+"%' or cr.re3 like '%"+key+"%' or " +
						"cr.re4 like '%"+key+"%' or cr.re5 like '%"+key+"%' ";
		
		String hql = " from CtReplace cr where 1=1 and "+ str +" order by cr.reId desc";
		Query query = getSession().createQuery(hql);
		if (page.getCurrentPage() != 0)
		{
		  page.setTotalCount(totalCountCtReplace(str));
		  query.setFirstResult(page.getFirstIndex());
		  query.setMaxResults(page.getPageSize());
		}
		return query.list();
	}

	@Override
	public List<CtSearchCollect> findSearchByPage(Page page) {
		String hql = " from CtSearchCollect cs order by cs.isOptimise asc, cs.searchTime desc";
		Query query = getSession().createQuery(hql);
		if (page.getCurrentPage() != 0)
		{
			page.setTotalCount(totalCountSearchCollect(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		List<CtSearchCollect> list = query.list();
		if(list!= null && list.size() > 0){
			return list;
		}
		return null;
	}

	private Long totalCountSearchCollect(String str) {
		String hql = "";
		if(str != null){
			hql = "select count(*) from CtSearchCollect cr where 1=1 and "+str+" ";
		} else {
			hql = "select count(*) from CtSearchCollect";
		}
		Query query = getSession().createQuery(hql);
		return (Long) query.uniqueResult();
	}

	@Override
	public CtSearchCollect findCollectById(Integer coId) {
		String hql = " from CtSearchCollect cs where cs.coId="+coId;
		Query query = getSession().createQuery(hql);
		List<CtSearchCollect> list = query.list();
		if(list!= null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	@Override
	public void updateCollect(CtSearchCollect searchCollect) {
		getSession().merge(searchCollect);
	}

	@Override
	public void deleteCollect(Long m) {
		String hql = " delete from CtSearchCollect cs where cs.coId=" + m;
		Query query = getSession().createQuery(hql);
		query.executeUpdate();
	}

	@Override
	public List<CtGoods> findGoodsByKey(Page page, String keySerach) {
		String str = "";
		if(keySerach != null){
			// '%\%%0402' ESCAPE '\'
			str += " and cg.GName like '%"+keySerach+"%' or cg.GSn like '%"+keySerach+"%' or cg.tname like '%"+keySerach+"%' ";
			str += " and cg.GName like '%"+keySerach.toUpperCase()+"%' ";
		}
		String hql = " from CtGoods cg where 1=1 "+str;
		Query query = getSession().createQuery(hql);
		if (page.getCurrentPage() != 0)
		{
		  page.setTotalCount(totalCountSearchGoods(str));
		  query.setFirstResult(page.getFirstIndex());
		  query.setMaxResults(page.getPageSize());
		}
		List<CtGoods> list = query.list();
		if(list != null && list.size() > 0){
			return list;
		}
		return null;
	}

	private Long totalCountSearchGoods(String str) {
		String hql = "select count(*) from CtGoods cg where 1=1 "+str;
		Query query = getSession().createQuery(hql);
		Long count = (Long) query.uniqueResult();
		return count;
	}
}
