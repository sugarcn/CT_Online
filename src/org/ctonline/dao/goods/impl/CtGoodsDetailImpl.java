package org.ctonline.dao.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsDetailDAO;
import org.ctonline.po.goods.CtGoodsDetail;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtGoodsDetailImpl implements CtGoodsDetailDAO{

	private SessionFactory sessionFactory;
	
	public SessionFactory getsessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	@Override
	public void addDetl(CtGoodsDetail detail) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.openSession();
		session.save(detail);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public List<CtGoodsDetail> queryByGid(Long gid) {
		// TODO Auto-generated method stub
		List<CtGoodsDetail> detailList = null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from CtGoodsDetail  where GId=?");
		query.setParameter(0, gid);
		detailList=query.list();
		colseSession(session);
		return detailList;
	}

	@Override
	public void updateDetl(CtGoodsDetail detail) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.openSession();
		session.update(detail);
		session.beginTransaction().commit();
		colseSession(session);
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
}
