package org.ctonline.dao.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsBrandDAO;
import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtGoodsBrandImpl implements CtGoodsBrandDAO{
	
	private SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Long save(CtGoodsBrand brand) {
		// TODO Auto-generated method stub
		Long id;
		Session session=this.sessionFactory.openSession();
		id =(Long)session.save(brand);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public CtGoodsBrand findById(Long id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		CtGoodsBrand brand = (CtGoodsBrand)session.get(CtGoodsBrand.class, id);
		colseSession(session);
		return brand;//get or load
	}
    
	@SuppressWarnings("unchecked")
	@Override
	public List<CtGoodsBrand> loadAll(Page page) {
		// TODO Auto-generated method stub
		List< CtGoodsBrand> brand=null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from CtGoodsBrand order by BName");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		brand=query.list();
		colseSession(session);
		return brand;
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		int d;
		String hql="delete CtGoodsBrand as u where u.id=?";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		query.setParameter(0, id);
		d= query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
		return d;
	}
	
	
	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtGoodsBrand";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}
	
	@Override
	public void update(CtGoodsBrand brand) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(brand);
		session.beginTransaction().commit();
		colseSession(session);
	}
    
	@SuppressWarnings("unchecked")
	@Override
	public List<CtGoodsBrand> findAll(String keyword,Page page) {
		// TODO Auto-generated method stub
		List< CtGoodsBrand> brand=null;
		String sql="from CtGoodsBrand";
		String str="";
		if (!keyword.equals("请输入关键字") && !keyword.equals("")){
			str+=" where BName like '%"+keyword+"%' ";
		}
		sql+=str;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		brand=query.list();
		colseSession(session);
		return brand;
	}

	@Override
	public List<CtGoodsBrand> queryAll() {
		// TODO Auto-generated method stub
		List< CtGoodsBrand> brand=null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from CtGoodsBrand");
		brand=query.list();
		colseSession(session);
		return brand;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}

}
