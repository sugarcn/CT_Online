package org.ctonline.dao.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtGoodsLinkDAO;
import org.ctonline.po.goods.CtGoodsLink;
import org.ctonline.po.views.ViewGoodsLink;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtGoodsLinkImpl implements CtGoodsLinkDAO{

	private SessionFactory sessionFactory;
	
	public SessionFactory getsessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public void tieLink(CtGoodsLink link) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.openSession();
		session.save(link);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public List<ViewGoodsLink> queryTied(Long gid) {
		// TODO Auto-generated method stub
		List<ViewGoodsLink> link = null;
		Session session=this.sessionFactory.openSession();
		String hql = ("from ViewGoodsLink as vgl where vgl.GId = "+gid +" order by vgl.linkGId");
		Query query= session.createQuery(hql);
		link = query.list();
		colseSession(session);
		return link;
	}

	@Override
	public void unLink(Long gid,Long lid) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		String hql = ("delete CtGoodsLink as u where u.GId=? and u.linkGId=?");
		Query query= session.createQuery(hql);
		query.setParameter(0, gid);
		query.setParameter(1, lid);
		query.executeUpdate();
		colseSession(session);
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
}
