package org.ctonline.dao.goods;

import java.util.List;

import org.ctonline.po.goods.CtGoodsGroup;
import org.ctonline.util.Page;

public interface CtGoodsGroupDAO {
	public Long save(CtGoodsGroup goodsGroup);//保存
	public CtGoodsGroup findById(Long id);//根据id查找
	public List<CtGoodsGroup> loadAll(Page page);//分页找出所有
	public int delete(Long id);//删除一条记录
	public Long totalCount(String str);//统计条数
	public void update(CtGoodsGroup goodsGroup);//更新
	public List<CtGoodsGroup> findAll(String keyword,Page page);//根据条件检索


}
