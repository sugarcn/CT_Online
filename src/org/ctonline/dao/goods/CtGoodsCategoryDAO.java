package org.ctonline.dao.goods;

import java.util.List;

import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.util.Page;

public interface CtGoodsCategoryDAO {
	public Long save(CtGoodsCategory category);//濞ｅ洦绻傞悺锟�	
	public CtGoodsCategory findById(Long id);//闁哄秷顬冨畵涔甦闁哄被鍎叉竟锟�	
	public List<CtGoodsCategory> loadAll(Page page);//闁告帒妫濋妴澶愬箥閹冩瘔闁圭鎷峰﹢锟�	
	public int delete(Long id);//闁告帞濞�▍搴㈢▔閿熻姤钂嬮悹浣规緲缂嶏拷
	public Long totalCount(String str);//缂備胶鍠曢鎼佸级閳╁啯娈�
	public void update(CtGoodsCategory category);//闁哄洤鐡ㄩ弻锟�	
	public List<CtGoodsCategory> searchAll(Long id,Page page);
	public List<CtGoodsCategory> levelquery(Long id);
	public List<CtGoodsCategory> findAll(String keyword,Page page);
}
