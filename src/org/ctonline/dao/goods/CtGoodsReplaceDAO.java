package org.ctonline.dao.goods;

import java.util.List;

import org.ctonline.po.goods.CtGoodsReplace;
import org.ctonline.po.views.ViewGoodsReplace;

public interface CtGoodsReplaceDAO {
	public void tieReplace(CtGoodsReplace replace);//绑定替代商品
	public void unReplace(Long gid,Long rid);
	public List<ViewGoodsReplace> queryRepList(Long gid);
}
