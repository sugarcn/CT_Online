package org.ctonline.dao.goods;

import java.util.List;

import org.ctonline.po.goods.CtGoodsLink;
import org.ctonline.po.views.ViewGoodsLink;

public interface CtGoodsLinkDAO {
	public void tieLink(CtGoodsLink link); //绑定关联商品
	public List<ViewGoodsLink> queryTied(Long gid);
	public void unLink(Long gid,Long lid);
}
