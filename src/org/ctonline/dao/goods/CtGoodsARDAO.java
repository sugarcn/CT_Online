package org.ctonline.dao.goods;

import java.util.List;
import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.goods.CtGoodsAttributeRelation;
import org.ctonline.po.views.ViewGoodsAttribute;

public abstract interface CtGoodsARDAO
  extends BaseHibernateDAO
{
  public abstract Long save(CtGoodsAttributeRelation paramCtGoodsAttributeRelation);
  
  public abstract List<ViewGoodsAttribute> findById(Long paramLong);
}
