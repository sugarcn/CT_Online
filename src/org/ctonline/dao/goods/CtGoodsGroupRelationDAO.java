package org.ctonline.dao.goods;

import java.util.List;

import org.ctonline.po.goods.CtGoodsAttributeRelation;
import org.ctonline.po.goods.CtGoodsGroupRelation;

public interface CtGoodsGroupRelationDAO {
		//保存用户到用户组
		public Long save(CtGoodsGroupRelation groupRelation);
		//根据用户组id查询用户
		public List<CtGoodsGroupRelation>  selectAll(Long UGId);
		//查询用户个数
		public Long totalCount(Long id);
		//删除关联表中重复用户
		public int delete(Long id);
		public CtGoodsAttributeRelation findRelByGidAndAttrId(Long gtid,
				Long long1);
}
