package org.ctonline.dao.goods;

import java.util.List;
import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.goods.CtGoodsImg;

public abstract interface CtGoodsImgDAO
  extends BaseHibernateDAO
{
  public abstract void addImg(CtGoodsImg paramCtGoodsImg);
  
  public abstract List<CtGoodsImg> queryByGid(Long paramLong);
  
  public abstract void updateImg(CtGoodsImg paramCtGoodsImg);
}
