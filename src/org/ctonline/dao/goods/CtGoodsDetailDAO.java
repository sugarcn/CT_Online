package org.ctonline.dao.goods;

import java.util.List;

import org.ctonline.po.goods.CtGoodsDetail;

public interface CtGoodsDetailDAO {
	public void addDetl(CtGoodsDetail detail);//添加详细描述
	public List<CtGoodsDetail> queryByGid(Long gid);
	public void updateDetl(CtGoodsDetail detail);
}
