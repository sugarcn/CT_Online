package org.ctonline.dao.goods;

import java.util.List;

import org.ctonline.po.goods.CtGoodsType;
import org.ctonline.util.Page;

public interface CtGoodsTypeDAO {
	public Long save(CtGoodsType goodsType);//保存
	public CtGoodsType findById(Long id);//根据id查找
	public List<CtGoodsType> loadAll(Page page);//分页找出所有
	public int delete(Long id);//删除一条记录
	public Long totalCount(String str);//统计条数
	public void update(CtGoodsType goodsType);//更新
	public List<CtGoodsType> findAll(String keyword,Page page);//根据条件检索
	public Long getID();
	public List<CtGoodsType> queryAll();

}
