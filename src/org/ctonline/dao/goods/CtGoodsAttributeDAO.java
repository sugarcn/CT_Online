package org.ctonline.dao.goods;

import java.util.List;

import org.ctonline.po.goods.CtGoodsAttribute;
import org.ctonline.po.views.ViewGoodsAttribute;
import org.ctonline.util.Page;

public interface CtGoodsAttributeDAO {
	public Long save(CtGoodsAttribute attribute);//保存
	public CtGoodsAttribute findById(Long id);//根据id查找
	public List<CtGoodsAttribute> loadAll(Page page);//分页找出所有
	public int delete(Long id);//删除一条记录
	public Long totalCount(String str);//统计条数
	public void update(CtGoodsAttribute attribute);//更新
	public List<CtGoodsAttribute> findAll(String keyword,Page page);//根据条件检索
	public List<CtGoodsAttribute> searchAll(Long id,Page page);
	public List<CtGoodsAttribute> queryByTID(Long tid);
	public List<ViewGoodsAttribute> queryByGId(Long gid);
	public void delAttRelation(Long gid);
}
