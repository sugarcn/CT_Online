package org.ctonline.dao.goods;

import java.util.List;

import org.ctonline.po.views.ViewGoodsGroup;

public interface ViewGoodsGroupDAO {
	public List<ViewGoodsGroup> query(String goodsname);
}
