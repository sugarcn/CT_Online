package org.ctonline.dao.goods;

import java.util.List;


import org.ctonline.po.basic.CtCashCoupon;
import org.ctonline.po.basic.CtFreeSampleReg;
import org.ctonline.po.basic.CtReplace;
import org.ctonline.po.goods.CtCateGoods;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGoodsResource;
import org.ctonline.po.goods.CtGoodsType;
import org.ctonline.po.goods.CtGroupPrice;
import org.ctonline.po.goods.CtRangePrice;
import org.ctonline.po.goods.CtSearchCollect;
import org.ctonline.po.views.GoodsDetailView;
import org.ctonline.util.Page;

public interface CtGoodsDAO {
	public Long save(CtGoods goods);//保存
	public CtGoods findById(Long id);//根据id查找
	public List<CtGoods> loadAll(Page page);//分页找出所有
	public int delete(Long id);//删除一条记录
	public Long totalCount(String str);//统计条数
	public void update(CtGoods goods);//更新
	public List<CtGoods> findAll(String keyword,Page page);//根据条件检索
	public Long getID();
	public List<CtGoods> queryAll(String status);
	public List<CtGoodsCategory> queryCategory(Long id);
	public List<CtGoodsBrand> queryBrand();
	public void hide(Long id);//逻辑删除
	public List<GoodsDetailView> queryDetail(Long id);//商品详情
	public void empty();//清空回收站
	public List<CtGoodsResource> queryRes();//取资料表
	public List<CtGoodsType> queryType();//取类型表
	public void tieRes(CtGoodsResource res);//绑定资料
	public List<?>  selectAll(Long GGId);
	public void unTieRes(Long gid,Long rid);
	//根据Gname查询CtGoods
	public List<CtGoods> getGoodsByGname(String Gname);
	//根据Gname查询一个CtGoods
	public Object getOneGoodsByGname(String Gname);
	  public abstract void saveRangePrice(CtRangePrice paramCtRangePrice);
	  
	  public abstract void savegroupPrice(CtGroupPrice paramCtGroupPrice);
	  
	  public abstract void saveCateGoodsInfo(CtCateGoods paramCtCateGoods);
	  
	  public abstract List<CtRangePrice> findRanParByGid(Long paramLong);
	  
	  public abstract void updateForName(String paramString1, String paramString2, String paramString3);
	public CtGoods getGoodsByGnameNoLike(String stringCellValue);
	public void deleteRenPriceByGid(Long gId);
	public CtGoods getGoodsByGSnNoLike(String stringCellValue);//根据SN取出数据
	public void deleteRelationByGid(Long gid);
	public List<CtFreeSampleReg> getFreeSamRegByPage(Page page);
	public CtFreeSampleReg findRegById(Integer samId);
	public void updateSamBySamId(Integer samId);
	public List<CtCashCoupon> getCashCoupon(Page page);
	public List<CtReplace> findReplaceByPage(Page page);
	public void saveReplace(CtReplace replace);
	public CtReplace findSearchKeyById(Integer id);
	public void deleteReplace(CtReplace replace);
	public List<CtReplace> findReplaceByKeyAndPage(String key, Page page);
	public List<CtSearchCollect> findSearchByPage(Page page);
	public CtSearchCollect findCollectById(Integer coId);
	public void updateCollect(CtSearchCollect searchCollect);
	public void deleteCollect(Long m);
	public List<CtGoods> findGoodsByKey(Page page, String keySerach);
}
