package org.ctonline.dao.help;

import java.util.List;
import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.help.CtHelp;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.util.Page;

public abstract interface CtHelpDAO extends BaseHibernateDAO
{
  public abstract CtHelp findById(int paramInt);

  public abstract List<CtHelp> loadAll(Page paramPage);

  public abstract int save(CtHelp paramCtHelp);

  public abstract int delete(int paramInt);

  public abstract Long totalCount(String paramString);

  public abstract void update(CtHelp paramCtHelp);

  public abstract List<CtHelp> loadOne(int paramInt);

  public abstract CtHelp getHelpByHId(int paramInt);

  public abstract List<CtHelp> findAll(String paramString, Page paramPage);

  public abstract List<CtHelpType> queryType();
}