package org.ctonline.dao.help.impl;

import java.util.List;
import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.help.CtHelpTypeDAO;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;

public class CtHelpTypeImpl extends BaseHibernateDAOImpl
  implements CtHelpTypeDAO
{
  public Integer save(CtHelpType helpType)
  {
    Integer id = (Integer)getSession().save(helpType);
    return id;
  }

  public CtHelpType findById(int id)
  {
    CtHelpType helpType = (CtHelpType)getSession().get(CtHelpType.class, Integer.valueOf(id));
    return helpType;
  }

  public List<CtHelpType> loadAll(Page page)
  {
    List helpTypes = null;
    Query query = getSession().createQuery(" from CtHelpType ");
    if (page.getCurrentPage() != 0)
    {
      page.setTotalCount(totalCount(null));
      query.setFirstResult(page.getFirstIndex());
      query.setMaxResults(page.getPageSize());
    }
    helpTypes = query.list();
    return helpTypes;
  }

  public int delete(int id)
  {
    String hql = "delete from CtHelpType as ct where ct.HTypeId=" + id;
    Query query = getSession().createQuery(hql);
    int d = query.executeUpdate();
    return d;
  }

  public Long totalCount(String str)
  {
    String hql = "select count(*) from CtHelpType";
    if (str != null) {
      hql = hql + str;
    }
    Long count = (Long)getSession().createQuery(hql).uniqueResult();
    return count;
  }

  public void update(CtHelpType helpType)
  {
    getSession().update(helpType);
  }

  public List<CtHelpType> findAll(String keyword, Page page)
  {
    List helpType = null;
    String sql = " from CtHelpType ";
    String str = "";
    if ((!keyword.equals("请输入关键字")) && (!keyword.equals(""))) {
      str = str + " where H_TYPE_NAME like '%" + keyword + "%'";
    }
    Query query = getSession().createQuery(sql + str);
    page.setTotalCount(totalCount(str));
    query.setFirstResult(page.getFirstIndex());
    query.setMaxResults(page.getPageSize());
    helpType = query.list();
    return helpType;
  }

  public List<CtHelpType> queryType()
  {
    List type = null;
    Query query = getSession().createQuery("from CtHelpType");
    type = query.list();
    return type;
  }
}