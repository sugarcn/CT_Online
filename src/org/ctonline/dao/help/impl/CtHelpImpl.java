package org.ctonline.dao.help.impl;

import java.util.List;
import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.help.CtHelpDAO;
import org.ctonline.po.help.CtHelp;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;

public class CtHelpImpl extends BaseHibernateDAOImpl
  implements CtHelpDAO
{
  public CtHelp findById(int id)
  {
    CtHelp help = (CtHelp)getSession().get(CtHelp.class, Integer.valueOf(id));
    return help;
  }

  public List<CtHelp> loadAll(Page page)
  {
    List help = null;
    String hql = " from CtHelp order by HTypeId,HId";
    Query query = getSession().createQuery(hql);
    if (page.getCurrentPage() != 0)
    {
      page.setTotalCount(totalCount(null));
      query.setFirstResult(page.getFirstIndex());
      query.setMaxResults(page.getPageSize());
    }
    help = query.list();
    return help;
  }

  public int save(CtHelp help)
  {
    String hql = "insert into Ct_Help(h_id,H_Type_Id,H_Title,H_Desc) values(seq_help.nextval,'" + help.getHTypeId() + "','" + help.getHTitle() + "','" + help.getHDesc() + "')";
    Query query = getSession().createSQLQuery(hql);
    int id = query.executeUpdate();
    return id;
  }

  public Long totalCount(String str)
  {
    String hql = "select count(*) from CtHelp ct ";
    if (str != null) {
      hql = hql + str;
    }
    Long count = (Long)getSession().createQuery(hql).uniqueResult();
    return count;
  }

  public void update(CtHelp help)
  {
    getSession().update(help);
  }

  public int delete(int id)
  {
    String hql = "delete from CtHelp ct where ct.HId=" + id;
    Query query = getSession().createQuery(hql);
    int d = query.executeUpdate();
    return d;
  }

  public CtHelp getHelpByHId(int Uid)
  {
    String hql = "from CtHelp ct where ct.HId=" + Uid;
    CtHelp cthelp = (CtHelp)getSession().createQuery(hql).uniqueResult();
    return cthelp;
  }

  public List<CtHelp> loadOne(int id)
  {
    List ctHelps = null;
    String hql = "from CtHelp ct where ct.HId=" + id;
    Query query = getSession().createQuery(hql);
    ctHelps = query.list();
    return ctHelps;
  }

  public List<CtHelp> findAll(String keyword, Page page)
  {
    List help = null;
    String sql = "from CtHelp";
    String str = "";
    if ((!keyword.equals("请输入关键字")) && (!keyword.equals(""))) {
      str = str + " where H_TITLE like '%" + keyword + "%' ";
    }
    Query query = getSession().createQuery(sql + str);
    page.setTotalCount(totalCount(str));
    query.setFirstResult(page.getFirstIndex());
    query.setMaxResults(page.getPageSize());
    help = query.list();
    return help;
  }

  public List<CtHelpType> queryType()
  {
    List type = null;
    Query query = getSession().createQuery("from CtHelpType");
    type = query.list();
    return type;
  }
}