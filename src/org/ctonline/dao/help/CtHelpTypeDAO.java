package org.ctonline.dao.help;

import java.util.List;
import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.util.Page;

public abstract interface CtHelpTypeDAO extends BaseHibernateDAO
{
  public abstract Integer save(CtHelpType paramCtHelpType);

  public abstract CtHelpType findById(int paramInt);

  public abstract List<CtHelpType> loadAll(Page paramPage);

  public abstract int delete(int paramInt);

  public abstract Long totalCount(String paramString);

  public abstract void update(CtHelpType paramCtHelpType);

  public abstract List<CtHelpType> findAll(String paramString, Page paramPage);

  public abstract List<CtHelpType> queryType();
}