package org.ctonline.dao.role.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.ctonline.dao.role.CtRoleDao;
import org.ctonline.po.basic.CtModule;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.user.CtRole;
import org.ctonline.po.user.CtRoleModule;
import org.ctonline.util.Page;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtRoleDaoImpl implements CtRoleDao {
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
	@Override
	public List<CtRole> loadAll(Page page) {
		// TODO Auto-generated method stub
		List<CtRole> ctRoles = null;
		Session session=this.sessionFactory.openSession();
		Query query = session.createQuery("from CtRole");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		ctRoles = query.list();
		colseSession(session);
		return ctRoles;
	}
	
	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtRole";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public Integer save(CtRole role) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		Integer id = (Integer) session.save(role);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public List<?> getModuleList(Integer id) {
		// TODO Auto-generated method stub
		List<?> modules;
		Session session=this.sessionFactory.openSession();
		String sql="select b.mid,b.name1,b.name2,crm.ROLE_MODULE_ID,crm.MODULE_ID,crm.ROLE_ID,crm.RIGHT " +
				"from ( select cm1.module_name name1, cm2.module_name name2,cm2.module_id mid from ct_module cm1, ct_module cm2 where cm1.module_id=cm2.parent_id ) b " +
				"left join ct_role_module crm on b.mid=crm.module_id and crm.role_id=? order by b.mid";
		Query query = session.createSQLQuery(sql);
		query.setParameter(0, id);
		modules = query.list();
		colseSession(session);
		return modules;
	}

	@Override
	public CtRole getRoleByRoleId(Integer roleId) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		CtRole role = (CtRole) session.get(CtRole.class, roleId);
		colseSession(session);
		return role;
	}

	@Override
	public void updateRole(CtRole cr) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(cr);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public Integer delRole(Integer id) {
		// TODO Auto-generated method stub
		
		
		
			// TODO Auto-generated method stub
			int d=1;
			String hql="delete from CtRoleModule a  where a.roleId=?";
			String hql3="delete from CtManager b  where b.roleId=?";
			String hql4="delete from CtRole cr where cr.roleId=?";
			Session session=this.sessionFactory.openSession();
			Query query= session.createQuery(hql);
			Query query3= session.createQuery(hql3);
			Query query4= session.createQuery(hql4);
			query.setParameter(0, id);
			query3.setParameter(0, id);
			query4.setParameter(0, id);
			query.executeUpdate();
			session.beginTransaction().commit();
			query3.executeUpdate();
			session.beginTransaction().commit();
			query4.executeUpdate();
			session.beginTransaction().commit();
			colseSession(session);
			return d;
		}

	@Override
	public List<CtModule> getModuleList2() {
		// TODO Auto-generated method stub
		List<CtModule> modules;
		Session session=this.sessionFactory.openSession();
		String hql = "from CtModule";
		Query query = session.createQuery(hql);
		modules = query.list();
		colseSession(session);
		return modules;
	}

	@Override
	public void save(CtRoleModule crm) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.save(crm);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public CtRoleModule queryCtRoleModuleByRoleIdAndMoudle(Long moduleId,Long roleId) {
		// TODO Auto-generated method stub
		CtRoleModule cm = new CtRoleModule();
		List<CtRoleModule> roleModules=null;
		Session session=this.sessionFactory.openSession();
		String hql = "from CtRoleModule crm where crm.moduleId="+moduleId+" and crm.roleId="+roleId+"";
		Query query = session.createQuery(hql);
		//query.setParameter(0, moduleId);
		//query.setParameter(1, roleId);
		try {
			roleModules = query.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		colseSession(session);
		if(roleModules.size() <=0){
			cm = null;
			return cm;
		}else {
			cm = roleModules.get(0);
			return cm;
		}
		
	}

	@Override
	public void update(CtRoleModule crm) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(crm);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public List<CtRole> loadAllCtrole() {
		// TODO Auto-generated method stub
		List<CtRole> ctRoles = null;
		Session session=this.sessionFactory.openSession();
		Query query = session.createQuery("from CtRole");
		ctRoles = query.list();
		colseSession(session);
		return ctRoles;
	}

	@Override
	public CtRole getRoleByRoleName() {
		// TODO Auto-generated method stub
		CtRole role;
		Session session=this.sessionFactory.openSession();
		String hql = "from CtRole cr where cr.roleName='客户经理'";
		Query query = session.createQuery(hql);
		role = (CtRole) query.uniqueResult();
		colseSession(session);
		return role;
	}
	
	@Override
	public CtRole findById(Integer id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		CtRole role = (CtRole)session.get(CtRole.class, id);
		colseSession(session);
		return role;//get or load
	}
	
	@Override
	public void update1(CtRole cr) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(cr);
		session.beginTransaction().commit();
		colseSession(session);
	}
}
