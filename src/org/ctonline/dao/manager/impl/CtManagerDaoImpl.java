package org.ctonline.dao.manager.impl;

import java.util.List;

import org.ctonline.dao.manager.CtManagerDao;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.manager.CtManager;
import org.ctonline.po.user.CtRole;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.MD5Util;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtManagerDaoImpl implements CtManagerDao {
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
	@Override
	public List<CtManager> loadAll(Page page) {
		// TODO Auto-generated method stub
		List<CtManager> managers = null;
		Session session=this.sessionFactory.openSession();
		String hql = "from CtManager";
		Query query = session.createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		managers = query.list();
		colseSession(session);
		return managers;
	}
	
	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtManager";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public CtManager getManagerByMManagerId(String id) {
		// TODO Auto-generated method stub
		CtManager cm = new CtManager();
		List<CtManager> managers;
		Session session =this.sessionFactory.openSession();
		String hql = "from CtManager cm where cm.MManagerId=?";
		Query query = session.createQuery(hql);
		query.setParameter(0, id);
		managers = query.list();
		if(managers.size()>0){
			cm = managers.get(0);
		}else {
			cm = null;
		}
		colseSession(session);
		return cm;
	}

	@Override
	public Integer save(CtManager cm) {
		// TODO Auto-generated method stub
		Session session =this.sessionFactory.openSession();
		Integer id = (Integer) session.save(cm);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public CtManager getManagerByMid(Integer Mid) {
		// TODO Auto-generated method stub
		Session session =this.sessionFactory.openSession();
		CtManager cm = (CtManager) session.get(CtManager.class, Mid);
		colseSession(session);
		return cm;
	}

	@Override
	public void updateManager(CtManager cm) {
		// TODO Auto-generated method stub
		Session session =this.sessionFactory.openSession();
		session.update(cm);
		session.beginTransaction().commit();
		colseSession(session);
		
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		String hql="delete CtManager as cm where cm.MId=?";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		query.setParameter(0, id);
		query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public List<CtManager> findAll(String keyword, Page page) {
		List< CtManager> managers=null;
		String sql="from CtManager";
		String str="";
		if (!keyword.equals("请输入你要查询的关键词") && !keyword.equals("")){
			str+=" where MManagerId like '%"+keyword+"%' ";
		}
		//str+=" order by regionID desc";
		sql+=str;
		Session session=this.sessionFactory.openSession();//.openSession();
		Query query= session.createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		managers=query.list();
		colseSession(session);
		return managers;
	}

	@Override
	public CtManager affirm(String uname, String password) throws Exception {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		String passwdorg = "CT"+uname+password;
		String encryptPwd = MD5Util.getEncryptPasswd(passwdorg);
		String hql = "from CtManager cm where cm.MManagerId =? and cm.MPassword=?";
		Query query = session.createQuery(hql);
		query.setParameter(0, uname);
		query.setParameter(1, encryptPwd);
		CtManager ctManager = new CtManager();
		List<CtManager> ctManagers =query.list();
		if(ctManagers.size() <= 0){
			ctManager = null;
		}else {
			ctManager = ctManagers.get(0);
		}
		colseSession(session);
		return ctManager;
	}

	@Override
	public List<CtManager> getCtManagerByRoleId(Integer roleId) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		String hql = "from CtManager cm where cm.roleId=?";
		Query query = session.createQuery(hql);
		query.setParameter(0, roleId);
		List<CtManager> managers = query.list();
		colseSession(session);
		return managers;
	}
	
	@Override
	public void resetPwd(String id,String pwd){
		Session session=this.sessionFactory.openSession();
		String hql = " update CtManager set  MPassword=? where MManagerId=? ";
		Query query = session.createQuery(hql);
		query.setParameter(0, pwd);
		query.setParameter(1, id);		
		query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
	}
}
