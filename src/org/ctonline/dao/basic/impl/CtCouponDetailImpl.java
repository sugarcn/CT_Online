package org.ctonline.dao.basic.impl;

import java.util.List;
import org.ctonline.dao.basic.CtCouponDetailDAO;
import org.ctonline.po.basic.CtCouponDetail;
import org.ctonline.util.Page;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtCouponDetailImpl implements CtCouponDetailDAO {
private SessionFactory sessionFactory;
	
	public SessionFactory getsessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override	
	public void save(CtCouponDetail coupondetail) {
		// TODO Auto-generated method stub
			String guid="select  CREATEGUID from dual ";
			List l;
		//Long id;
		Session session=this.sessionFactory.openSession();
		try{

	        SQLQuery query = session.createSQLQuery(guid);
	        guid = (String) query.uniqueResult();
		//l= session.createQuery(guid).list();//..uniqueResult();
		//guid=(String)l.get(0);
		coupondetail.setCouponNumber(guid);
		
		}catch(Exception e){
			System.out.println("..."+e.getMessage());
		}		
		try{

			//id =(Long)session.save(coupon);
			session.save(coupondetail);
			session.beginTransaction().commit();
			colseSession(session);
			
		}catch (Exception e) {
            System.out.println("......"+e.getMessage());
		}
	}
	
	
	@Override
	public CtCouponDetail findById(Long id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		CtCouponDetail coupon=null;
		try{
			coupon = (CtCouponDetail)session.get(CtCouponDetail.class, id);
		    colseSession(session);
		}catch(Exception e){
			System.out.println("....."+e.getMessage());
		}
		return coupon;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CtCouponDetail> loadAll(Page page) {
		// TODO Auto-generated method stub
		List< CtCouponDetail> resource=null;
		try{
			Session session=this.sessionFactory.openSession();
			Query query= session.createQuery("from CtCouponDetail");
			if (page.getCurrentPage()!=0){
				page.setTotalCount(this.totalCount(null));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
			resource=query.list();
			colseSession(session);
		}catch(Exception e){
			System.out.println("......"+e.getMessage());
		}
		
		return resource;
	}

	
	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		int d=-1;
		String hql="delete CtCouponDetail as u where u.id=?";
		try{
			Session session=this.sessionFactory.openSession();
			Query query= session.createQuery(hql);
			query.setParameter(0, id);
			d= query.executeUpdate();
			session.beginTransaction().commit();
			colseSession(session);
		}catch(Exception e){
			System.out.println("....."+e.getMessage());
		}		
		return d;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		
		String hql="select count(*) from CtCouponDetail t where 1=1 ";
		Long count=0L;
		try{
			if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		}catch(Exception e){
			System.out.println("..."+e.getMessage());
		}		
		return count;
	}

	@Override
	public void update(CtCouponDetail coupondetail) {
		// TODO Auto-generated method stub
		try{
			Session session=this.sessionFactory.openSession();
		session.update(coupondetail);
		session.beginTransaction().commit();
		colseSession(session);
		}catch(Exception e){
			System.out.println("..."+e.getMessage());
		}		
		
	}

	@Override
	public List<CtCouponDetail> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List< CtCouponDetail > resource=null;
		String sql="from CtCouponDetail";
		String str="";
		try{
			if (!keyword.equals("....") && !keyword.equals("")){
				str+=" where COUPON_NAME like '%"+keyword+"%' ";
			}
			//str+=" order by regionID desc";
			sql+=str;
			Session session=this.sessionFactory.openSession();
			Query query= session.createQuery(sql);
			page.setTotalCount(this.totalCount(str));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
			resource=query.list();
			colseSession(session);
		}catch(Exception e){
			System.out.println("....."+e.getMessage());
		}			
		return resource;
	}
	public List<CtCouponDetail> findByCouponId(int id, Page page) {
		// TODO Auto-generated method stub
		List< CtCouponDetail > list=null;
		
		
		try{
			
			Session session=this.sessionFactory.openSession();
			Query query= session.createQuery("from CtCouponDetail t where COUPON_ID=?");
			query.setParameter(0, id);
			if (page.getCurrentPage()!=0){
				page.setTotalCount(this.totalCount(" and t.ctCoupon.couponId="+id));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
			list=query.list();
			colseSession(session);
		}catch(Exception e){
			System.out.println("....."+e.getMessage());
		}			
		return list;
	}

	@Override
	public Long getID() {
		// TODO Auto-generated method stub
		Long id=-1L;
		String sql = "select SEQ_REGION .nextval as id from dual";
		try{
			Session session=this.sessionFactory.openSession();
			id= (Long)session.createQuery(sql).uniqueResult();
			colseSession(session);
		}catch(Exception e){
			System.out.println("......"+e.getMessage());
		}			
		return id;
	}

	@Override
	public List<CtCouponDetail> queryAll() {
		// TODO Auto-generated method stub
		List< CtCouponDetail> resource=null;
		try{
			Session session=this.sessionFactory.openSession();
			Query query= session.createQuery("from CtCouponDetail");
			resource=query.list();
			colseSession(session);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}			
		return resource;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
	@Override
	public String findByCouponIdAndAllIisOk(Integer couponId) {
		Session session = this.sessionFactory.openSession();
		System.out.println("s");
		String count = "";
		String hql = "select count(*) from CtCouponDetail ccd where ccd.ctCoupon.couponId="+couponId+" and ccd.UId is null";
		System.out.println("1");
		long co = (Long) session.createQuery(hql).uniqueResult();
		System.out.println("2");
//		this.sessionFactory.close();
		count = co + "__";
		hql = "select count(*) from CtCouponDetail ccd where ccd.ctCoupon.couponId="+couponId+" and ccd.UId is not null";
		System.out.println(4);
		co = (Long) session.createQuery(hql).uniqueResult();
		System.out.println(5);
		colseSession(session);
		System.out.println(6);
		System.out.println("ok");
		count += co;
		return count;
	}

	@Override
	public Long findUidCount() {
		Session session = this.sessionFactory.openSession();
		String hql = " select count(*) from CtUser cu where cu.UState is not null and cu.UState=1";
		long co = (Long) session.createQuery(hql).uniqueResult();
		colseSession(session);
		return co;
	}



	@Override
	public void updateByCoupIdFaBuAll(long uid ,int id) {
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			String hql;
			hql = "select count(U_ID) from CT_COUPON_DETAIL where U_ID="+uid+" and COUPON_ID="+id;
			SQLQuery sqlQuery = session.createSQLQuery(hql);
			Object oo = sqlQuery.uniqueResult();
			colseSession(session);
			if (oo.toString().equals("0")){
				session = this.sessionFactory.openSession();
				hql = "update CtCouponDetail c set UId="+uid+", status=1, isUse=0 where c.UId is null and c.couponId="+id+" and rownum<="+1+"";
				Query query = session.createQuery(hql);
				query.executeUpdate();
				colseSession(session);
			}
		} catch (HibernateException e) {
			e.printStackTrace();
			colseSession(session);
		}
	}
}
