package org.ctonline.dao.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.CtResourceTypeDAO;
import org.ctonline.po.basic.CtResourceType;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtResourceTypeImpl implements CtResourceTypeDAO{
	
	private SessionFactory sessionFactory;
	
	public SessionFactory getsessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Long save(CtResourceType resourcetype) {
		// TODO Auto-generated method stub
		Long id;
		Session session=this.sessionFactory.openSession();
		id =(Long)session.save(resourcetype);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public CtResourceType findById(Long id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		CtResourceType resourcetype = (CtResourceType)session.get(CtResourceType.class, id);
		colseSession(session);
		return resourcetype;
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		int d;
		String hql="delete CtResourceType as u where u.id=?";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		query.setParameter(0, id);
		d= query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
		return d;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtResourceType";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public void update(CtResourceType resourceType) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(resourceType);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public List<CtResourceType> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List< CtResourceType > resourceType=null;
		String sql="from CtResourceType";
		String str="";
		if (!keyword.equals("请输入你要查询的关键词") && !keyword.equals("")){
			str+=" where R_NAME like '%"+keyword+"%' ";
		}
		//str+=" order by regionID desc";
		sql+=str;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		resourceType=query.list();
		colseSession(session);
		return resourceType;
	}

	@Override
	public Long getID() {
		// TODO Auto-generated method stub
		String sql = "select SEQ_REGION .nextval as id from dual";
		Session session=this.sessionFactory.openSession();
		Long id= (Long)session.createQuery(sql).uniqueResult();
		colseSession(session);
		return id;
	}

	@Override
	public List<CtResourceType> queryAll(Page page) {
		// TODO Auto-generated method stub
		List< CtResourceType> resourceType=null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from CtResourceType cr order by cr.RTId desc");
		if (page!= null && page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		resourceType=query.list();
		colseSession(session);
		return resourceType;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
}
