package org.ctonline.dao.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.CtModuleDAO;
import org.ctonline.po.basic.CtModule;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.views.ViewModuleLeft;
import org.ctonline.po.views.ViewModuleTop;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtModuleImpl implements CtModuleDAO{
	private SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public List<CtModule> searchAll(Integer pid, Page page) {
		List<CtModule> module=null;
		String sql="from CtModule order by moduleId ";
	Session session=this.sessionFactory.openSession();
	Query query= session.createQuery(sql);
	if (page.getCurrentPage()!=0){
		page.setTotalCount(this.totalCount(" where parentId="+pid));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
	}
	module=query.list();
	colseSession(session);
	return module;
	}

	@Override
	public List<CtModule> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
				List<CtModule> module=null;
				String sql="from CtModule";
				String str="";
				if (!keyword.equals("请输入关键字") && !keyword.equals("")){
					str+=" where moduleName like '%"+keyword+"%' ";
				}
				//str+=" order by regionID desc";
				sql+=str;
				Session session=this.sessionFactory.openSession();
				Query query= session.createQuery(sql);
				page.setTotalCount(this.totalCount(str));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
				module=query.list();
				colseSession(session);
				return module;
	}

	@Override
	public List<CtModule> loadAll(Page page) {
		// TODO Auto-generated method stub
				List< CtModule> module=null;
				Session session=this.sessionFactory.openSession();
				Query query= session.createQuery("from CtModule");
				if (page.getCurrentPage()!=0){
					page.setTotalCount(this.totalCount(null));
					query.setFirstResult(page.getFirstIndex());
					query.setMaxResults(page.getPageSize());
				}
				module=query.list();
				colseSession(session);
				return module;
	}

	@Override
	public void save(CtModule module) {
		Session session=this.sessionFactory.openSession();
		session.save(module);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public int delete(int m) {
		// TODO Auto-generated method stub
				int d;
				String hql="delete CtModule as m where m.id=?";
				Session session=this.sessionFactory.openSession();
				Query query= session.createQuery(hql);
				query.setParameter(0, m);
				d= query.executeUpdate();
				session.beginTransaction().commit();
				colseSession(session);
				return d;
	}

	@Override
	public CtModule findById(Integer id) {
		// TODO Auto-generated method stub
				Session session=this.sessionFactory.openSession();
				CtModule module = (CtModule)session.get(CtModule.class, id);
				colseSession(session);
				return module;//get or load
	}

	@Override
	public void update(CtModule module) {
		// TODO Auto-generated method stub
				Session session=this.sessionFactory.openSession();
				session.update(module);
				session.beginTransaction().commit();
				colseSession(session);
	}
	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
				String hql="select count(*) from CtModule";
				if (str !=null)
					hql+=str;
				Session session =this.sessionFactory.openSession();
				Long count= (Long)session.createQuery(hql).uniqueResult();
				colseSession(session);
				return count;
	}
	
	@Override
	public List<ViewModuleTop> getTopModule(Integer roleId) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		List<ViewModuleTop> list;
		String hql = "from ViewModuleTop vmt where vmt.roleId=? order by vmt.moduleId";
		Query query = session.createQuery(hql);
		query.setParameter(0, roleId);
		list = query.list();
		session.flush();
		session.clear();
		colseSession(session);
		return list;
	}

	@Override
	public List<ViewModuleLeft> getLeftModule(Integer roleId, Integer parentId) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		List<ViewModuleLeft> list;
		String hql = "from ViewModuleLeft vml where vml.roleId=? and vml.parentId=? order by vml.moduleId";
		Query query = session.createQuery(hql);
		query.setParameter(0, roleId);
		query.setParameter(1, parentId);
		list = query.list();
		if(list != null){
			for(ViewModuleLeft vml:list){
				Integer mId = vml.getModuleId();
				Integer rId = vml.getRoleId();
				vml.setChilds(getChildResource(mId,rId));
			}
		}
		colseSession(session);
		return list;
	}
	
	//从视图中取出子类别
	public List<ViewModuleLeft> getChildResource(Integer mId,Integer rId){
		Session session=this.sessionFactory.openSession();
		List<ViewModuleLeft> list;
		String hql = "from ViewModuleLeft vml where vml.parentId=? and vml.roleId=? order by vml.moduleId";
		Query query = session.createQuery(hql);
		query.setParameter(0, mId);
		query.setParameter(1, rId);
		list = query.list();
		colseSession(session);
		return list;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
}
