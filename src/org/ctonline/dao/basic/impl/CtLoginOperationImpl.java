package org.ctonline.dao.basic.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.basic.CtLoginOperationDAO;
import org.ctonline.po.basic.CtLoginOperation;
import org.ctonline.po.manager.CtManager;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;

public class CtLoginOperationImpl
  extends BaseHibernateDAOImpl
  implements CtLoginOperationDAO
{
  public void addLog(CtLoginOperation ctLoginOperation)
  {
    getSession().save(ctLoginOperation);
  }

@Override
public Long operation() {
	HttpSession req = ServletActionContext.getRequest().getSession();
	CtManager cm = (CtManager)req.getAttribute("admininfosession");
	if(cm != null && cm.getMId() != null){
		return cm.getMId().longValue();
	}
	return null;
}

@Override
public CtLoginOperation findDescEs(String opContent) {
	String hql = " from CtLoginOperation co where co.logDesc='"+opContent+"' ";
	Query query = getSession().createQuery(hql);
	List<CtLoginOperation> list = query.list();
	if(list != null && list.size() > 0){
		return list.get(0);
	}
	return null;
}

@Override
public List<CtLoginOperation> findAllLoginByPageAndKey(Page page, String key,
		String stime, String etime, String type, String userName) {
	
	String str = "";
	String logType = "";
	if(type.equals("1")){
		str += " and cl.logType=1";
	} else if(type.equals("0")) {
		str += " and cl.logType=0";
	}
	if((stime != null && !stime.equals("")) && (etime != null && !etime.equals(""))){
		str += " and cl.logTime between "+stime+" and "+etime+" ";
	}
	
	
	
	
	if(userName != null && !userName.equals("") && key != null && !key.equals("")){
		str += " and cl.logDesc like '%"+key+"%' or cl.logDesc like '%"+userName+"%' ";
	} else if(userName != null && !userName.equals("")) {
		str += " and cl.logDesc like '%"+key+"%' ";
	} else if(key != null && !key.equals("")){
		str += " and cl.logDesc like '%"+key+"%' or cl.logModule like '%"+key+"%'";
	}
	
	String hql = " from CtLoginOperation cl where 1=1 " + str + " order by cl.logId desc";
	
	Query query = getSession().createQuery(hql);
	
	page.setTotalCount(this.totalCountLog(str));
	query.setFirstResult(page.getFirstIndex());
	query.setMaxResults(page.getPageSize());
	
	List<CtLoginOperation> list = query.list();
	if(list != null && list.size() > 0){
		return list;
	}
	
	return null;
}

private Long totalCountLog(String str) {
	String hql = "select count(*) from CtLoginOperation cl where 1=1 " + str;
	return (Long) getSession().createQuery(hql).uniqueResult();
}

}
