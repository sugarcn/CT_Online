package org.ctonline.dao.basic.impl;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.basic.CtLoginLogDAO;
import org.ctonline.po.basic.CtLoginLog;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;

public class CtLoginLogImpl
  extends BaseHibernateDAOImpl
  implements CtLoginLogDAO
{
  public void save(CtLoginLog ctLoginLog)
  {
    getSession().save(ctLoginLog);
  }

@Override
public List<CtLoginLog> getLogByPage(Page page) {
	String hql = " from CtLoginLog cll order by cll.logId desc";
	Query query = getSession().createQuery(hql);
	if(page.getCurrentPage()!=0){
		page.setTotalCount(this.totalCount(null));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
	}
	return query.list();
}

private Long totalCount(String str) {
	// TODO Auto-generated method stub
	String hql="select count(*) from CtLoginLog";
	if(str != null){
		hql += str;
	}
	Long count = (Long)getSession().createQuery(hql).uniqueResult();
	return count;
}
}
