package org.ctonline.dao.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.CtAdDAO;
import org.ctonline.po.basic.CtAd;
import org.ctonline.po.basic.CtAdType;
import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.po.user.CtAddTicket;
import org.ctonline.system.Logg;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtAdImpl implements CtAdDAO{
	private SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public CtAd findById(int id) {
		// TODO Auto-generated method stub
		Session session =this.sessionFactory.openSession();
		CtAd ad = (CtAd) session.get(CtAd.class, id);
		colseSession(session);
		return ad;
	}

	@Override
	public List<CtAd> loadAll(Page page) {
		// TODO Auto-generated method stub
		List<CtAd> ad = null;
		Session session=this.sessionFactory.openSession();
		String hql = "from CtAd";
		Query query = session.createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		ad = query.list();
		colseSession(session);
		return ad;
	}

	@Logg(operationType="save", operationName="新增广告")
	@Override
	public int save(CtAd ad) {
		// TODO Auto-generated method stub
		Session session = this.getSessionFactory().openSession();
		int id = (Integer) session.save(ad);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtAd ct ";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Logg(operationType="update", operationName="更新广告")
	@Override
	public void update(CtAd ad) {
		// TODO Auto-generated method stub
		Session session = this.getSessionFactory().openSession();
		session.update(ad);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Logg(operationType="delete", operationName="删除广告")
	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		Session session = this.getSessionFactory().openSession();
		String hql = "delete from CtAd ct where ct.adId="+id;
		Query query = session.createQuery(hql);
		int d = query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
		return d;
	}

	@Override
	public CtAd getAdByAdId(int Uid) {
		// TODO Auto-generated method stub
		Session session = this.getSessionFactory().openSession();
		String hql = "from CtAd ct where ct.adId="+Uid;
		CtAd ctAd = (CtAd) session.createQuery(hql).uniqueResult();
		colseSession(session);
		return ctAd;
	}

	@Override
	public List<CtAd> loadOne(int id) {
		// TODO Auto-generated method stub
		List<CtAd> ctAds = null;
		Session session=this.sessionFactory.openSession();
		String hql = "from CtAd ct where ct.adId="+id;
		Query query = session.createQuery(hql);
		ctAds =query.list();
		colseSession(session);
		return ctAds;
	}

	@Override
	public List<CtAd> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List<CtAd> ad = null;
		String sql="from CtAd";
		String str="";
		if (!keyword.equals("请输入关键字") && !keyword.equals("")){
			str+=" where AD_TITLE like '%"+keyword+"%' ";
		}
		Session session = this.sessionFactory.openSession();
		Query query= session.createQuery(sql+str);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		ad=query.list();
		colseSession(session);
		return ad;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
}
