package org.ctonline.dao.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.CtRegionDAO;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtRegionImpl implements CtRegionDAO {
	private SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Long save(CtRegion region) {
		// TODO Auto-generated method stub
		Long id;
		Session session=this.sessionFactory.openSession();
		id =(Long)session.save(region);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public CtRegion findById(Long id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		CtRegion region = (CtRegion)session.get(CtRegion.class, id);
		colseSession(session);
		return region;//get or load
	}

	@Override
	public List<CtRegion> loadAll(Page page) {
		// TODO Auto-generated method stub
		List< CtRegion> region=null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from CtRegion");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		region=query.list();
		colseSession(session);
		return region;
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		int d;
		String hql="delete CtRegion as u where u.id=?";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		query.setParameter(0, id);
		d= query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
		return d;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtRegion";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public void update(CtRegion region) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(region);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public List<CtRegion> findAll(String keyword,Page page) {
		// TODO Auto-generated method stub
		List< CtRegion> region=null;
		String sql="from CtRegion";
		String str="";
		if (!keyword.equals("请输入关键字") && !keyword.equals("")){
			str+=" where regionName like '%"+keyword+"%' ";
		}
		//str+=" order by regionID desc";
		sql+=str;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		region=query.list();
		colseSession(session);
		return region;
	}
	
	
	@Override
	public List<CtRegion> searchAll(Long id,Page page){
		List< CtRegion> region=null;
		String sql="from CtRegion  where parentId="+id+" order by regionName ";
	Session session=this.sessionFactory.openSession();
	Query query= session.createQuery(sql);
	if (page.getCurrentPage()!=0){
		page.setTotalCount(this.totalCount(" where parentId="+id));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
	}
	region=query.list();
	colseSession(session);
	return region;
	}
	
	@Override
	public List<CtRegion> queryById(Long id){
		List< CtRegion> region=null;
		String sql="from CtRegion  where parentId="+id;
	Session session=this.sessionFactory.openSession();
	Query query= session.createQuery(sql);
	region=query.list();
	colseSession(session);
	return region;
		
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
}
