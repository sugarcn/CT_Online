package org.ctonline.dao.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.CtAdTypeDAO;
import org.ctonline.po.basic.CtAdType;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtAdTypeImpl implements CtAdTypeDAO{
	private SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Integer save(CtAdType adType) {
		// TODO Auto-generated method stub
		Integer id;
		Session session=this.sessionFactory.openSession();
		id =(Integer)session.save(adType);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public CtAdType findById(int id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.openSession();
		//CtAdType adType = (CtAdType)session.get(getClass(), id);
		CtAdType adType = (CtAdType)session.get(CtAdType.class, id);
		colseSession(session);
		return adType;
	}

	@Override
	public List<CtAdType> loadAll(Page page) {
		// TODO Auto-generated method stub
		List<CtAdType> adTypes = null;
		Session session=this.sessionFactory.openSession();
		Query query = session.createQuery("from CtAdType");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		adTypes = query.list();
		colseSession(session);
		return adTypes;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		int d;
		String hql="delete from CtAdType as u where u.id=?";
		Session session = this.sessionFactory.openSession();
		Query query = session.createQuery(hql);
		query.setParameter(0, id);
		d = query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
		return  d;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtAdType";
		if(str != null){
			hql += str;
		}
		Session session = this.sessionFactory.openSession();
		Long count = (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public void update(CtAdType adType) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.openSession();
		session.update(adType);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public List<CtAdType> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List<CtAdType> adType = null;
		String sql="from CtAdType";
		String str="";
		if (!keyword.equals("请输入关键字") && !keyword.equals("")){
			str+=" where AD_TYPE_NAME like '%"+keyword+"%' ";
			
		}
		Session session = this.sessionFactory.openSession();
		Query query= session.createQuery(sql+str);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		adType=query.list();
		colseSession(session);
		return adType;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}

	@Override
	public List<CtAdType> findAllType() {
		String hql = " from CtAdType ca order by ca.adTypeId desc";
		Session session = this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		List<CtAdType> list = query.list();
		if(list != null && list.size() > 0){
			colseSession(session);
			return list;
		}
		colseSession(session);
		return null;
	}
}
