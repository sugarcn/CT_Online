package org.ctonline.dao.basic.impl;

import java.util.List;
import org.ctonline.dao.basic.CtCouponDAO;
import org.ctonline.po.basic.CtCashCoupon;
import org.ctonline.po.basic.CtCoupon;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;


public class CtCouponImpl implements CtCouponDAO{
	
	private SessionFactory sessionFactory;
	
	public SessionFactory getsessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override	
	public void save(CtCoupon coupon) {
		// TODO Auto-generated method stub
		//Long id;
		Session session=this.sessionFactory.openSession();
		try{
			//id =(Long)session.save(coupon);
			session.save(coupon);
			session.beginTransaction().commit();
			colseSession(session);
			
		}catch (Exception e) {
            System.out.println("������ݿ���?"+e.getMessage());
		}
	}
	
	
	@Override
	public CtCoupon findById(Long id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		CtCoupon coupon=null;
		try{
			coupon = (CtCoupon) session.createQuery(" from CtCoupon cc where cc.couponId="+id).list().get(0);
		    colseSession(session);
		}catch(Exception e){
			System.out.println("���ID���ҳ��?"+e.getMessage());
		}
		return coupon;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CtCoupon> loadAll(Page page) {
		// TODO Auto-generated method stub
		List< CtCoupon> resource=null;
		try{
			Session session=this.sessionFactory.openSession();
			Query query= session.createQuery("from CtCoupon t order by t.couponId desc");
			if (page.getCurrentPage()!=0){
				page.setTotalCount(this.totalCount(null));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
			resource=query.list();
			colseSession(session);
		}catch(Exception e){
			System.out.println("��ҳ�ҳ����г��?"+e.getMessage());
		}
		
		return resource;
	}
	

	
	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		int d=-1;
		String hql="delete CtCoupon as u where u.id=?";
		try{
			Session session=this.sessionFactory.openSession();
			Query query= session.createQuery(hql);
			query.setParameter(0, (Integer)id);
			d= query.executeUpdate();
			session.beginTransaction().commit();
			colseSession(session);
		}catch(Exception e){
			System.out.println("ɾ���¼���?"+e.getMessage());
		}		
		return d;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtCoupon";
		Long count=0L;
		try{
			if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		}catch(Exception e){
			System.out.println("ͳ�Ƽ�¼������?"+e.getMessage());
		}		
		return count;
	}

	@Override
	public void update(CtCoupon coupon) {
		// TODO Auto-generated method stub
		try{
			Session session=this.sessionFactory.openSession();
		session.update(coupon);
		session.beginTransaction().commit();
		colseSession(session);
		}catch(Exception e){
			System.out.println("�޸ļ�¼���?"+e.getMessage());
		}		
		
	}

	@Override
	public List<CtCoupon> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List< CtCoupon > resource=null;
		String sql="from CtCoupon";
		String str="";
		try{
			if (!keyword.equals("请输入你要查询的关键词") && !keyword.equals("")){
				str+=" where COUPON_NAME like '%"+keyword+"%' ";
			}
			//str+=" order by regionID desc";
			sql+=str;
			Session session=this.sessionFactory.openSession();
			Query query= session.createQuery(sql);
			page.setTotalCount(this.totalCount(str));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
			resource=query.list();
			colseSession(session);
		}catch(Exception e){
			System.out.println("��������������?"+e.getMessage());
		}			
		return resource;
	}

	@Override
	public Long getID() {
		// TODO Auto-generated method stub
		Long id=-1L;
		String sql = "select SEQ_REGION .nextval as id from dual";
		try{
			Session session=this.sessionFactory.openSession();
			id= (Long)session.createQuery(sql).uniqueResult();
			colseSession(session);
		}catch(Exception e){
			System.out.println("�Զ����ID���?"+e.getMessage());
		}			
		return id;
	}

	@Override
	public List<CtCoupon> queryAll() {
		// TODO Auto-generated method stub
		List< CtCoupon> resource=null;
		try{
			Session session=this.sessionFactory.openSession();
			Query query= session.createQuery("from CtCoupon");
			resource=query.list();
			colseSession(session);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}			
		return resource;
	}

	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}

	@Override
	public List<CtCashCoupon> getCashCoupon(Page page) {
		String hql = " from CtCashCoupon ccc order by ccc.cashId desc";
		Session session=this.sessionFactory.openSession();
		Query query = session.createQuery(hql);
		if (page.getCurrentPage() != 0)
		{
		  page.setTotalCount(totalCountCashCoupon(null));
		  query.setFirstResult(page.getFirstIndex());
		  query.setMaxResults(page.getPageSize());
		}
		List<CtCashCoupon> list = query.list();
		colseSession(session);
		if(list != null && list.size() > 0){
			return list;
		}
		return null;
	}

	private Long totalCountCashCoupon(String str) {
		String hql = "select count(*) from CtCashCoupon ccc order by ccc.cashId desc";
		Session session=this.sessionFactory.openSession();
		Query query = session.createQuery(hql);
		Long l = (Long) query.uniqueResult();
		colseSession(session);
		return l;
	}

	@Override
	public void updateCashCoupon(CtCashCoupon cashCoupon) {
		Session session=this.sessionFactory.openSession();
//		String sql = " insert into ct_cash_coupon c values(seq_cash.nextval, '"+cashCoupon.getCashName()+"', '"+cashCoupon.getOrderSn()+"', "+cashCoupon.getCashType()+", '"+cashCoupon.getAmount()+"', '"+cashCoupon.getDiscount()+"', '"+cashCoupon.getStime()+"', '"+cashCoupon.getEtime()+"', '')";
//		Query query = session.createSQLQuery(sql);
//		query.executeUpdate();
		session.merge(cashCoupon);
		session.beginTransaction().commit();
		colseSession(session);
	}

}