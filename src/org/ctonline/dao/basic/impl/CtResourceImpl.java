package org.ctonline.dao.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.CtResourceDAO;
import org.ctonline.po.basic.CtResource;
import org.ctonline.po.basic.CtResourceType;
import org.ctonline.po.views.ViewGoodsResource;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtResourceImpl implements CtResourceDAO{
	
	private SessionFactory sessionFactory;
	
	public SessionFactory getsessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Long save(CtResource resource) {
		// TODO Auto-generated method stub
		Long id;
		Session session=this.sessionFactory.openSession();
		id =(Long)session.save(resource);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public CtResource findById(Long id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		CtResource resource = (CtResource)session.get(CtResource.class, id);
		colseSession(session);
		return resource;
	}

	@Override
	public List<CtResource> loadAll(Page page) {
		// TODO Auto-generated method stub
		List< CtResource> resource=null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from CtResource");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		resource=query.list();
		colseSession(session);
		return resource;
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		int d;
		String hql="delete CtResource as u where u.id=?";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		query.setParameter(0, id);
		d= query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
		return d;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtResource";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public void update(CtResource resource) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(resource);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public List<CtResource> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List< CtResource > resource=null;
		String sql=" from CtResource ";
		String str="";
		if (!keyword.equals("请输入你要查询的关键词") && !keyword.equals("")){
			str+=" where RName like '%"+keyword+"%' ";
		}
		//str+=" order by regionID desc";
		sql+=str;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		resource=query.list();
		colseSession(session);
		return resource;
	}

	@Override
	public Long getID() {
		// TODO Auto-generated method stub
		String sql = "select SEQ_REGION .nextval as id from dual";
		Session session=this.sessionFactory.openSession();
		Long id= (Long)session.createQuery(sql).uniqueResult();
		colseSession(session);
		return id;
	}

	@Override
	public List<CtResource> queryAll(Page page) {
		// TODO Auto-generated method stub
		List< CtResource> resource=null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from CtResource cr order by cr.RSort asc, cr.RId desc");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		resource=query.list();
		colseSession(session);
		return resource;
	}

	@Override
	public List<CtResourceType> queryType() {
		// TODO Auto-generated method stub
		List<CtResourceType> type = null;
		Session session = this.sessionFactory.openSession();
		Query query = session.createQuery("from CtResourceType");
		type = query.list();
		colseSession(session);
		return type;
	}

	@Override
	public List<ViewGoodsResource> queryByGoodsID(Long gid) {
		// TODO Auto-generated method stub
		List<ViewGoodsResource> resList = null;
		Session session = this.sessionFactory.openSession();
		String sql = "from ViewGoodsResource as res where res.GId = "+gid;
		Query query = session.createQuery(sql);
		resList = query.list();
		colseSession(session);
		return resList;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}

	@Override
	public Integer findSortByRTId(Long rTId) {
		String hql = " from CtResource cr where cr.RTId="+rTId +" order by cr.RSort desc";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		query.setFirstResult(0);
		query.setMaxResults(2);
		List<CtResource> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0).getRSort()+1;
		}
		return 1;
	}
}
