package org.ctonline.dao.basic.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.List;

import org.ctonline.dao.basic.CtNoticeDAO;
import org.ctonline.po.basic.CtFilter;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.basic.CtNoticeType;
import org.ctonline.util.Page;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.LobHelper;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtNoticeImpl implements CtNoticeDAO{
	private SessionFactory sessionFactory;
	
	public SessionFactory getSession() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public List<CtNotice> loadAll(Page page) {
		// TODO Auto-generated method stub
		List<CtNotice> notice = null;
		Session session = this.sessionFactory.openSession();
		Query query = session.createQuery("from CtNotice cn order by cn.noId desc");
		if(page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		notice = query.list();
		colseSession(session);
		return notice;
	}
	

	@Override
	public Integer save(CtNotice notice) {
		// TODO Auto-generated method stub
		Integer id;
		Session session=this.sessionFactory.openSession();
		id =(Integer)session.save(notice);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtNotice";
		if(str != null){
			hql += str;
		}
		Session session = this.sessionFactory.openSession();
		Long count = (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}
	public Long totalCountType(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtNoticeType";
		if(str != null){
			hql += str;
		}
		Session session = this.sessionFactory.openSession();
		Long count = (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public CtNotice findById(int id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.openSession();
		//CtAdType adType = (CtAdType)session.get(getClass(), id);
		CtNotice notice = (CtNotice)session.get(CtNotice.class, id);
		colseSession(session);
		return notice;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		int d;
		String hql="delete from CtNotice as u where u.id=?";
		Session session = this.sessionFactory.openSession();
		Query query = session.createQuery(hql);
		query.setParameter(0, id);
		d = query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
		return  d;
	}

	@Override
	public void update(CtNotice notice) {
		// TODO Auto-generated method stub
		try {
			Session session = this.sessionFactory.openSession();
			session.merge(notice);
			session.beginTransaction().commit();
			colseSession(session);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static String ClobToString(Clob clob) throws SQLException, IOException { 

		String reString = ""; 
		Reader is = clob.getCharacterStream();// 得到流 
		BufferedReader br = new BufferedReader(is); 
		String s = br.readLine(); 
		StringBuffer sb = new StringBuffer(); 
		while (s != null) {// 执行循环将字符串全部取出付值给 StringBuffer由StringBuffer转成STRING 
		sb.append(s); 
		s = br.readLine(); 
		} 
		reString = sb.toString(); 
		return reString; 
		}
	@Override
	public List<CtNotice> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List<CtNotice> notice = null;
		String sql="from CtNotice";
		String str="";
		if (!keyword.equals("请输入关键字") && !keyword.equals("")){
			str+=" where NO_TITLE like '%"+keyword+"%' ";
			
		}
		Session session = this.sessionFactory.openSession();
		Query query= session.createQuery(sql+str);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		notice=query.list();
		colseSession(session);
		return notice;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}

	@Override
	public List<CtNoticeType> findTypeByPage(Page page) {
		List<CtNoticeType> noticeList = null;
		Session session = this.sessionFactory.openSession();
		Query query = session.createQuery(" from CtNoticeType cn order by cn.noTypeId asc");
		if(page != null){
			if(page.getCurrentPage()!=0){
				page.setTotalCount(this.totalCountType(null));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
		}
		noticeList = query.list();
		colseSession(session);
		return noticeList;
	}

	@Override
	public CtNoticeType findTypeByTypeId(int id) {
		String hql = " from CtNoticeType c where c.noTypeId=" + id;
		Session session = this.sessionFactory.openSession();
		Query query = session.createQuery(hql);
		List<CtNoticeType> noticeType = query.list();
		if(noticeType != null && noticeType.size() > 0){
			return noticeType.get(0);
		}
		colseSession(session);
		return null;
	}

	@Override
	public void saveNoticeType(CtNoticeType noticeType) {
		Session session = this.sessionFactory.openSession();
		try {
			noticeType = (CtNoticeType) session.merge(noticeType);
			session.beginTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		colseSession(session);
	}

	@Override
	public void updateNoticeType(CtNoticeType noticeType) {
		Session session = this.sessionFactory.openSession();
		String sql = " update CT_NOTICE_TYPE t set t.NO_TYPE_NAME='"+noticeType.getNoTypeName() + "' , t.NO_TYPE_DESC='"+noticeType.getNoTypeDesc()+"' where t.NO_TYPE_ID="+noticeType.getNoTypeId();
		SQLQuery query = session.createSQLQuery(sql);
		query.executeUpdate();
		colseSession(session);
	}

	@Override
	public void deleteNoticeTypeById(int m) {
		Session session = this.sessionFactory.openSession();
		String sql = " delete from CT_NOTICE_TYPE t where t.NO_TYPE_ID="+m;
		SQLQuery query = session.createSQLQuery(sql);
		query.executeUpdate();
		colseSession(session);
	}

	@Override
	public List<CtFilter> findFilterListByPage(Page page) {
		List<CtFilter> noticeList = null;
		Session session = this.sessionFactory.openSession();
		Query query = session.createQuery(" from CtFilter cn order by cn.fid asc");
		if(page != null){
			if(page.getCurrentPage()!=0){
				page.setTotalCount(this.totalCountFilter(null));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
		}
		noticeList = query.list();
		colseSession(session);
		return noticeList;
	}

	private Long totalCountFilter(String str) {
		String hql="select count(*) from CtFilter";
		if(str != null){
			hql += str;
		}
		Session session = this.sessionFactory.openSession();
		Long count = (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public void saveFirlt(CtFilter filter) {
		Session session = this.sessionFactory.openSession();
		session.merge(filter);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public CtFilter findFirltById(int id) {
		String hql = " from CtFilter cf where cf.fid="+id;
		Session session = this.sessionFactory.openSession();
		Query query = session.createQuery(hql);
		List<CtFilter> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	@Override
	public void deleteFilterByFilter(CtFilter filter) {
		Session session = this.sessionFactory.openSession();
		session.delete(filter);
		session.beginTransaction().commit();
		colseSession(session);
	}
}
