package org.ctonline.dao.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.CtRegionDAO;
import org.ctonline.dao.basic.CtUserRankDAO;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.basic.CtUserRank;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtUserRankImpl implements CtUserRankDAO {
	private SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<CtUserRank> loadAll(Page page) {
		// TODO Auto-generated method stub
		List<CtUserRank> userRanks = null;
		Session session=this.sessionFactory.openSession();
		//Session session =sessionFactory.openSession().getCurrentSession();
		Query query = session.createQuery("from CtUserRank");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		userRanks = query.list();
		
		colseSession(session);
		
		return userRanks;
	}
	
	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtUserRank";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public Long save(CtUserRank userRank) {
		// TODO Auto-generated method stub
		Long id;
		Session session=this.sessionFactory.openSession();
		id =(Long)session.save(userRank);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public String getMaxPoint() {
		// TODO Auto-generated method stub
		String hql = "select max(MAX_POINTS) from CtUserRank";
		Session session =this.sessionFactory.openSession();
		String maxPoint = (String)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return maxPoint;
	}

	@Override
	public CtUserRank findById(Long id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		CtUserRank userRank = (CtUserRank) session.get(CtUserRank.class, id);
		colseSession(session);
		return userRank;
	}

	@Override
	public void update(CtUserRank userRank) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(userRank);
		session.beginTransaction().commit();
		colseSession(session);
		
	}

	//鏍规嵁id鍒犻櫎绛夌骇璁板綍
	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		int d;
		String hql="delete CtUserRank as u where u.id=?";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		query.setParameter(0, id);
		d= query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
		return d;
	}

	@Override
	public List<CtUserRank> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List<CtUserRank> userRank = null;
		String sql="from CtUserRank";
		String str="";
		if (!keyword.equals("请输入你要查询的关键词") && !keyword.equals("")){
			str+=" where R_NAME like '%"+keyword+"%' ";
			str+="or MIN_POINTS like '%"+keyword+"%' ";
			str+="or MAX_POINTS like '%"+keyword+"%' ";
		}
		sql+=str;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		userRank=query.list();
		colseSession(session);
		return userRank;
	}

        @Override
        public List<CtUserRank> getRank(){
        	List<CtUserRank> rank=null;
    		Session session=this.sessionFactory.openSession();
    		Query query= session.createQuery("from CtUserRank  order by RName DESC");
            rank = query.list();
            colseSession(session);
        	return rank;
        }
    	private void colseSession(Session session) {
    		session.clear();
    		session.flush();
    		session.close();
    	}
}
