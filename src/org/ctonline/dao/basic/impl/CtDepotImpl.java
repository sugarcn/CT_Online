package org.ctonline.dao.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.CtDepotDAO;
import org.ctonline.po.basic.CtDepot;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtDepotImpl implements CtDepotDAO{
	
private SessionFactory sessionFactory;
	
	public SessionFactory getsessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Integer save(CtDepot depot) {
		// TODO Auto-generated method stub
		Integer id;
		Session session=this.sessionFactory.openSession();
		id =(Integer)session.save(depot);
		session.beginTransaction().commit();
		colseSession(session);
		return id;
	}

	@Override
	public CtDepot findById(Integer id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		CtDepot depot = (CtDepot)session.get(CtDepot.class, id);
		colseSession(session);
		return depot;
	}

	@Override
	public List<CtDepot> loadAll(Page page) {
		// TODO Auto-generated method stub
		List< CtDepot> depot=null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from CtDepot");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		depot=query.list();
		colseSession(session);
		return depot;
	}

	@Override
	public int delete(Integer id) {
		// TODO Auto-generated method stub
		int d;
		String hql="delete CtDepot as u where u.id=?";
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(hql);
		query.setParameter(0, id);
		d= query.executeUpdate();
		session.beginTransaction().commit();
		colseSession(session);
		return d;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtDepot";
		if (str !=null)
			hql+=str;
		Session session =this.sessionFactory.openSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		colseSession(session);
		return count;
	}

	@Override
	public void update(CtDepot depot) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		session.update(depot);
		session.beginTransaction().commit();
		colseSession(session);
	}

	@Override
	public List<CtDepot> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List< CtDepot > depot=null;
		String sql="from CtDepot";
		String str="";
		if (!keyword.equals("请输入你要查询的关键词") && !keyword.equals("")){
			str+=" where depotName like '%"+keyword+"%' ";
		}
		//str+=" order by regionID desc";
		sql+=str;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		depot=query.list();
		colseSession(session);
		return depot;
	}
	
	@Override
	public List<CtDepot> searchAll(Integer id,Page page){
		List< CtDepot> region=null;
		String sql="from CtDepot ";
	Session session=this.sessionFactory.openSession();
	Query query= session.createQuery(sql);
	if (page.getCurrentPage()!=0){
		page.setTotalCount(this.totalCount(null));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
	}
	region=query.list();
	colseSession(session);
	return region;
	}

	@Override
	public List<CtDepot> queryAll() {
		// TODO Auto-generated method stub
		List<CtDepot> depotList = null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from CtDepot");
		depotList=query.list();
		colseSession(session);
		return depotList;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
}
