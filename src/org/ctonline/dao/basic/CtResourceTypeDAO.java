package org.ctonline.dao.basic;

import java.util.List;


import org.ctonline.po.basic.CtResourceType;
import org.ctonline.util.Page;

public interface CtResourceTypeDAO {
	public Long save(CtResourceType resourceType);//淇濆瓨
	public CtResourceType findById(Long id);//鏍规嵁id鏌ユ壘
	public int delete(Long id);//鍒犻櫎涓�潯璁板綍
	public Long totalCount(String str);//缁熻鏉℃暟
	public void update(CtResourceType resourceType);//鏇存柊
	public List<CtResourceType> findAll(String keyword,Page page);//鏍规嵁鏉′欢妫�储
	public Long getID();
	public List<CtResourceType> queryAll(Page page);//鏌ュ叏琛�
	}
