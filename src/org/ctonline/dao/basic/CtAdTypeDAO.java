package org.ctonline.dao.basic;

import java.util.List;

import org.ctonline.po.basic.CtAdType;
import org.ctonline.util.Page;

public interface CtAdTypeDAO {
	public Integer save(CtAdType adType);
	public CtAdType findById(int id);
	public List<CtAdType> loadAll(Page page);
	public int delete(int id);
	public Long totalCount(String str);
	public void update(CtAdType adType);
	public List<CtAdType> findAll(String keyword,Page page);
	public List<CtAdType> findAllType();
}
