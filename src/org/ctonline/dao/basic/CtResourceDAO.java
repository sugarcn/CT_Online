package org.ctonline.dao.basic;

import java.util.List;


import org.ctonline.po.basic.CtResource;
import org.ctonline.po.basic.CtResourceType;
import org.ctonline.po.views.ViewGoodsResource;
import org.ctonline.util.Page;

public interface CtResourceDAO {
	public Long save(CtResource resource);//保存
	public CtResource findById(Long id);//根据id查找
	public List<CtResource> loadAll(Page page);//分页找出所有
	public int delete(Long id);//删除一条记录
	public Long totalCount(String str);//统计条数
	public void update(CtResource resource);//更新
	public List<CtResource> findAll(String keyword,Page page);//根据条件检索
	public Long getID();
	public List<CtResource> queryAll(Page page);//查全表
	public List<CtResourceType> queryType();
	public List<ViewGoodsResource> queryByGoodsID(Long gid);
	public Integer findSortByRTId(Long rTId);
}
