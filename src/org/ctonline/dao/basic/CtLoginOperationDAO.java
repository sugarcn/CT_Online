package org.ctonline.dao.basic;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.basic.CtLoginOperation;
import org.ctonline.util.Page;

public abstract interface CtLoginOperationDAO
  extends BaseHibernateDAO
{
  public abstract void addLog(CtLoginOperation paramCtLoginOperation);

public abstract Long operation();

public abstract CtLoginOperation findDescEs(String opContent);

public abstract List<CtLoginOperation> findAllLoginByPageAndKey(Page page,
		String key, String stime, String etime, String type, String userName);

}
