package org.ctonline.dao.basic;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.basic.CtLoginLog;
import org.ctonline.util.Page;

public abstract interface CtLoginLogDAO
  extends BaseHibernateDAO
{
  public abstract void save(CtLoginLog paramCtLoginLog);

public abstract List<CtLoginLog> getLogByPage(Page page);
}
