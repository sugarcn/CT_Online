package org.ctonline.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class BaseHibernateDAOImpl
{
  SessionFactory sessionFactory;
  
  public SessionFactory getSessionFactory()
  {
    return this.sessionFactory;
  }
  
  public void setSessionFactory(SessionFactory sessionFactory)
  {
    this.sessionFactory = sessionFactory;
  }
  
  public Session begin()
  {
    return null;
  }
  
  public Session getSession()
  {
    return this.sessionFactory.getCurrentSession();
  }
  
  public Session getCurrentSession()
  {
    return this.sessionFactory.getCurrentSession();
  }
  
  public void clossSession() {}
  
  public void commit() {}
}
