package org.ctonline.dao.front_goods.impl;

import java.util.List;

import org.ctonline.dao.front_goods.F_GoodsDAO;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.views.ViewCateNum;
import org.ctonline.po.views.ViewSubcateNum;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class F_GoodsImpl implements F_GoodsDAO {

	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<CtGoodsCategory> queryAllCate() {
		// TODO Auto-generated method stub
		List<CtGoodsCategory> cateList = null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from CtGoodsCategory as cate order by cate.parentId,cate.sortOrder");
		cateList = query.list();
		colseSession(session);
		return cateList;
	}

	@Override
	public List<ViewCateNum> cateCount() {
		// TODO Auto-generated method stub
		List<ViewCateNum> catecount = null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from ViewCateNum");
		catecount = query.list();
		colseSession(session);
		return catecount;
	}

	@Override
	public List<ViewSubcateNum> subcateCount() {
		// TODO Auto-generated method stub
		List<ViewSubcateNum> subcatecount = null;
		Session session=this.sessionFactory.openSession();
		Query query= session.createQuery("from ViewSubcateNum");
		subcatecount = query.list();
		colseSession(session);
		return subcatecount;
	}
	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
}
