package org.ctonline.dao.pay;

import java.util.List;

import org.ctonline.po.basic.CtPaymentWx;
import org.ctonline.util.Page;
import org.hibernate.SessionFactory;

public interface ICtPayDao {

	public abstract SessionFactory getSessionFactory();

	public abstract void setSessionFactory(SessionFactory sessionFactory);

	public abstract List<CtPaymentWx> findAllByPage(Page page);

}