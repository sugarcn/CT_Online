package org.ctonline.dao.pay.impl;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.pay.ICtPayDao;
import org.ctonline.po.basic.CtPaymentWx;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtPayDaoImpl  extends BaseHibernateDAOImpl implements ICtPayDao{
	private SessionFactory sessionFactory;

	@Override
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Override
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<CtPaymentWx> findAllByPage(Page page) {
		String hql = " from CtPaymentWx cpw order by cpw.payDatatime desc";
		Session session = this.sessionFactory.openSession();
		
		Query query =session.createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(session));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		List<CtPaymentWx> list = query.list();
		if(list != null && list.size() > 0){
			session.flush();
			session.close();
			return list;
		}
		session.flush();
		session.close();
		return null;
	}

	private Long totalCount(Session session) {
		String hql = "select count(*) from CtPaymentWx cpw";
		Query query = session.createQuery(hql);
		Long count = (Long) query.uniqueResult();
		return count;
	}
	

}
